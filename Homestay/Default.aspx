﻿<%@ Page Title="CCS Homestay Program" Language="C#" MasterPageFile="~/CCSApps.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Homestay_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <link href="/Homestay/_css/Homestay.css" rel="stylesheet" type="text/css" />

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="bannerH1Text" Runat="Server">
    CCS Homestay Program
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">
    <h2>Welcome to the CCS Homestay Program!</h2>
    <div id="accordion">
        <h3 class="accordion accordion-toggle">Apply for Homestay</h3>
        <div class="panel accordion-content">
            <p><a href="Students/ApplicationForm_L.aspx"><span style="font-weight:bolder;">STUDENT</span> Application Form</a></p>
            <p><a href="/HomestayFamilies/Application/ApplicationForm.aspx"><span style="font-weight:bolder;">HOST FAMILY</span> Application Form</a></p>
        </div>
        <h3 class="accordion accordion-toggle">Current Homestay Student</h3>
        <div class="panel accordion-content">
            <ol>
                <li><a href="https://apps.spokane.edu/CCSOnlineSurvey/Default.aspx?SurveyID=1266&TallyID=1652" target="_blank">Student Satisfaction Survey</a></li>
                <!--
                <li>Tips for Success (and Translations)</li>
                <li><a href="/Homestay/Families/_PDF/MorneauShepell-studentCounseling.pdf" target="_blank">Student Counseling and Support Service-Morneau Shepell 2018</a></li>
                <li><a href="/Homestay/Families/_PDF/2018-19-Lewermark.pdf" target="_blank">Insurance Plan 2018-19</a></li>
                -->
                <li><a href="/Homestay/Students/ListResources.aspx" target="_blank">List of Resources</a></li>
            </ol>
        </div>
        <h3 class="accordion accordion-toggle">Current Homestay Family</h3>
        <div class="panel accordion-content">
             <p><a href="/HomestayFamilies/Default.aspx" target="_blank">List of Resources</a></p>
             <p><a href="/HomestayFamilies/Account/Login.aspx" target="_blank">Current Family Login</a></p>
            <p><a href="https://apps.spokane.edu/CCSOnlineSurvey/Default.aspx?SurveyID=1288&TallyID=1695" target="_blank">Homestay Family Survey</a></p>
        </div>
        <h3 class="accordion accordion-toggle">Contact Information</h3>
        <div class="panel accordion-content">
            <h3>Manager of Immigration and Student Success</h3>
            <p>
                <span style="font-weight:bolder;">Ashley Ding</span><br />
                SFCC: 509-533-3242<br />
                SCC: 509-533-8201<br />
                Cell: 208-821-7626<br />
                <a href="mailto:Ashley.Ding@ccs.spokane.edu">Ashley.Ding@ccs.spokane.edu</a><br />
            </p> 
            <p>
                Spokane Falls Community College<br />
                3410 West Fort George Wright Drive MS 3011<br />
                Spokane, WA 99224-5288
            </p> 
            <p>
                Spokane Community College<br />
                1810 N Green St MS 2151<br />
                Spokane, WA 99224-5399
            </p> 
            <h3>International Homestay Coordinators</h3>
            <p>
                <span style="font-weight:bolder;">Amy Cosgrove</span><br />
                Phone: 509-533-4113<br />
                Email: <a href="mailto:AmyCosgrove@ccs.spokane.edu">AmyCosgrove@ccs.spokane.edu</a>
                </p>
             <p>
                <span style="font-weight:bolder;">Heather Stone</span><br />
                509-533-4113<br />
                Email: <a href="mailto:Heather.Stone@ccs.spokane.edu">Heather.Stone@ccs.spokane.edu</a>
             </p>
        </div>
        <h3 class="accordion accordion-toggle">Homestay Administration</h3>
        <div class="panel accordion-content">
            <ul>
                <li><a href="/Homestay/Admin/Default.aspx">Administration Home Page</a></li>
            </ul>
        </div>

    </div>
    <script type="text/javascript">
        $(document).ready(function ($) {
            $('#accordion').find('.accordion-toggle').click(function () {

                //Expand or collapse this panel
                $(this).next().slideToggle('fast');

                //Hide the other panels
                $(".accordion-content").not($(this).next()).slideUp('fast');

            });
        });
    </script>
    <script>
        var acc = document.getElementsByClassName("accordion");
        var i;

        for (i = 0; i < acc.length; i++) {
            acc[i].addEventListener("click", function () {
                this.classList.toggle("active");
            });
        }
    </script>

</asp:Content>

