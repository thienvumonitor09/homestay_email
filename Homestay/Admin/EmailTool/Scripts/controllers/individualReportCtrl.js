﻿app.controller("individualReportCtrl", function ($scope, $http, $routeParams) {

    console.log($routeParams.idex);
    $scope.individualReport = {};
    $scope.getIndividualEmailReport = function () {
        $http({
            method: "POST",
            url: "EmailToolWS.asmx/GetIndividualEmailReport",
            dataType: 'json',
            data: { reportID: $routeParams.idex },
            headers: { "Content-Type": "application/json" }
        }).then(function (response) {
            //alert(response.data.d);
            $scope.individualReport = JSON.parse(response.data.d);
            /*
            $scope.individualReport.toEmail = $scope.individualReport.toEmail.replace(/[\[\]"]+/g, "");
            $scope.individualReport.toEmail = $scope.individualReport.toEmail.split(",");
            */
            $scope.individualReport.toEmail = JSON.parse($scope.individualReport.toEmail);
            $scope.individualReport.ccEmail = JSON.parse($scope.individualReport.ccEmail);
            //console.log($scope.individualReport.toEmail);
            //console.log(Array.isArray($scope.individualReport.toEmail));
           
            
        }, function (error) {
            alert("error");
        });
    };

    $scope.getIndividualEmailReport();

});