﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Homestay_Admin_Documents : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        String strResultMsg = "";
        String selectedStudent = "0";
        String selectedFamily = "0";
        String strFileName = "";
        String strUploadServer = "";
        String strButtonFace = "";
        String strFileType = "";
        //String strPath = "";

        Homestay hsInfo = new Homestay();
        //Reset fields between loads
        litLetterFile.Text = "";
        litPhotoFile.Text = "";
        litAgreementFile.Text = "";
        litLicenseFile.Text = "";
        litWaiverFile.Text = "";

        if (Request.QueryString["type"] != null)
        {
            strFileType = Request.QueryString["type"].ToString();
        }
        //Set visibility
        switch (strFileType)
        {
            case "student":
                divStudentFiles.Visible = true;
                fldStudent.Visible = true;
                divFamilyFiles.Visible = false;
                fldFamily.Visible = false;
                break;
            case "family":
                divFamilyFiles.Visible = true;
                fldFamily.Visible = true;
                divStudentFiles.Visible = false;
                fldStudent.Visible = false;
                break;
        }
        if (IsPostBack)
        {
            //capture selection
            selectedStudent = ddlStudents.SelectedValue;
            selectedFamily = ddlFamilies.SelectedValue;
            if (Request.Form["btnSubmit"] != null)
            {
                strButtonFace = Request.Form["btnSubmit"].ToString();
            }
            switch (strButtonFace)
            {
                case "Return to Admin Home Page":
                    Response.Redirect("/Homestay/Admin/Default.aspx");
                    break;
            }
            //Display documents with hot links
            Configuration config = WebConfigurationManager.OpenWebConfiguration(Request.ApplicationPath);
            AppSettingsSection appSettings = (AppSettingsSection)config.GetSection("appSettings");
            strUploadServer = "/InternetContent" + appSettings.Settings["HomestayShare"].Value;
            if (selectedStudent != "0" && selectedStudent != "")
            {
                DataTable dt = hsInfo.GetUploadedFiles(Convert.ToInt32(selectedStudent));
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            strFileName = dr["fileName"].ToString();
                            string filePath = dr["filePath"].ToString();
                            string uploadServer = "";
                            if (filePath.Contains("ccs-internet"))
                            {
                                uploadServer += @"http:\\apps.spokane.edu\InternetContent\Homestay\";
                            }
                            else
                            {
                                uploadServer += @"http:\\portal.ccs.spokane.edu\InternetContent\Homestay\";
                            }
                            if (strFileName.Contains("-Letter-"))
                            {
                                //strPath = HttpUtility.UrlEncode(strFileName);
                                //get the file name
                                litLetterFile.Text += "<p><a href=\"" + uploadServer + strFileName + "\" target=\"_blank\">" + strFileName + "</a></p>";
                            }
                            else if (strFileName.Contains("-Photo-"))
                            {
                                //strPath = HttpUtility.UrlEncode(strFileName);
                                //Get the file name
                                litPhotoFile.Text += "<p><a href=\"" + uploadServer + strFileName + "\" target=\"_blank\">" + strFileName + "</a></p>";
                            }
                            else 
                                litAgreementFile.Text += "<p><a href=\"" + uploadServer + strFileName + "\" target=\"_blank\">" + strFileName + "</a></p>";
                        } // end for each loop
                    }// end if count > 0
                }// end if not null
            }// end student
            if (selectedFamily != "0" && selectedFamily != "")
            {
                DataTable dt = hsInfo.GetUploadedFiles(Convert.ToInt32(selectedFamily));
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            strFileName = dr["fileName"].ToString();
                            if (strFileName.Contains("License"))
                            {
                                //get the file name
                                litLicenseFile.Text += "<p><a href=\"" + strUploadServer + strFileName + "\" target=\"_blank\">" + strFileName + "</a></p>";
                            }
                            if (strFileName.Contains("Waiver"))
                            {
                                //get the file name
                                litWaiverFile.Text += "<p><a href=\"" + strUploadServer + strFileName + "\" target=\"_blank\">" + strFileName + "</a></p>";
                            }
                        }// end for each loop
                    }// end row count > 0
                }// end datatable not null
            }// end family
        }// end is postback

        //Load the Drop-Down List
        DataTable dtStudents = hsInfo.GetHomestayStudents("Active", 0);
        ddlStudents.Items.Clear();
        ListItem LI;
        LI = new ListItem();
        LI.Value = "0";
        LI.Text = "-- Select One --";
        ddlStudents.Items.Add(LI);
        if (dtStudents != null)
        {
            if (dtStudents.Rows.Count > 0)
            {
                foreach (DataRow dr in dtStudents.Rows)
                {
                    LI = new ListItem();
                    LI.Value = dr["applicantID"].ToString();
                    LI.Text = dr["familyName"].ToString() + ", " + dr["firstName"].ToString() + " - " + Convert.ToDateTime(dr["DOB"]).ToShortDateString();
                    ddlStudents.Items.Add(LI);
                }
                //Reset the selected student (cleared above)
                if (selectedStudent != "0" && selectedStudent != "") { ddlStudents.SelectedValue = selectedStudent; }
            }
            else { strResultMsg += "Error: dtStudents has no rows.<br />"; }
        }
        else { strResultMsg += "Error: dtStudents is null.<br />"; }

        DataTable dtFamilies = hsInfo.GetHomestayFamilyInfo(0, "", "DDLactive");
        ddlFamilies.Items.Clear();
        LI = new ListItem();
        LI.Value = "0";
        LI.Text = "-- Select One --";
        ddlFamilies.Items.Add(LI);
        if (dtFamilies != null)
        {
            if (dtFamilies.Rows.Count > 0)
            {
                foreach (DataRow dr in dtFamilies.Rows)
                {
                    LI = new ListItem();
                    LI.Value = dr["id"].ToString();
                    LI.Text = dr["homeName"].ToString();
                    ddlFamilies.Items.Add(LI);
                }
                //Reset the selected student (cleared above)
                if (selectedFamily != "0" && selectedFamily != "") { ddlFamilies.SelectedValue = selectedFamily; }
            }
            else { strResultMsg += "Error: dtFamilies has no rows.<br />"; }
        }
        else { strResultMsg += "Error: dtFamilies is null.<br />"; }

        litResultMsg.Text = "<p>" + strResultMsg + hsInfo.strResultMsg + "</p>";
    }
}