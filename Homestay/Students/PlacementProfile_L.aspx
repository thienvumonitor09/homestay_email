﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PlacementProfile_L.aspx.cs" Inherits="Homestay_PlacementProfile_L" %>
<!DOCTYPE html>

<html lang="en-us" >

<head id="Head1" runat="server">
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <title>CCS Homestay Program</title>
    
    <link id="Link1" href="/_css/ccs.css" rel="stylesheet"  type="text/css" />
    <link href="/Homestay/_css/Homestay.css" rel="stylesheet" type="text/css" />  
    <link href="/_css/Forms.css" rel="stylesheet" type="text/css" />         
    <meta http-equiv="X-UA-Compatible" content="IE=9" />        
    <script type="text/javascript" src="/_js/pushToHttps.js"></script>
    <script type="text/javascript" src="/_js/jquery-1.9.1.min.js"></script>
    <link href="/Homestay/_css/Homestay.css" rel="stylesheet" type="text/css" />
    <script src="/_js/jquery.mask.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.dobSibling1').mask('00/00/0000');
            $('.dobSibling2').mask('00/00/0000');
            $('.dobSibling3').mask('00/00/0000');
            $('.dobSibling4').mask('00/00/0000');
        });
    </script>
</head>

<body>
<form id="form1" runat="server">
<h1>CCS Homestay Program</h1>

<h2>Student Profile</h2>
    <asp:Button ID="btnCmdReturnAdminHome" runat="server" Text="Return to Admin Home Page" OnClick="btnCmdReturnAdminHome_Click" />&nbsp&nbsp;
    <!--<asp:Button ID="btnGenerate" runat="server" Text="Export to PDF" OnClick="btnExport_Click" /> -->
    

<asp:Literal ID="litResultMsg" runat="server"></asp:Literal>
    <div class="divError">* indicates required field</div>
<fieldset id="fldParents" runat="server">
<legend>Legal Guardians</legend>
    <table style="width:100%;table-layout:fixed">
        <tr>
            <td>
                <div id="eFFamName" class="divError" runat="server"></div>
    <p>
        <label for="familyNameFather"><span style="color:#DD0000;font-weight:600;"> * </span>Guardian 1's Family  Name</label><br />
        <asp:TextBox ID="familyNameFather" runat="server" Width="300px" BackColor="ControlLight" MaxLength="30"></asp:TextBox>
    </p>
    <div id="eFFirstName" class="divError" runat="server"></div>
    <p>
        <label for="firstNameFather"><span style="color:#DD0000;font-weight:600;"> * </span>Guardian 1's First Name</label><br />
        <asp:TextBox ID="firstNameFather" runat="server" Width="300px" BackColor="ControlLight" MaxLength="30"></asp:TextBox>
    </p>
    <div id="eFathOcc" class="divError" runat="server"></div>
     <p>
        <label for="occupationFather"><span style="color:#DD0000;font-weight:600;"> * </span>Guardian 1's Occupation</label><br />
        <asp:TextBox ID="occupationFather" runat="server" Width="300px" BackColor="ControlLight" MaxLength="30"></asp:TextBox>
        <asp:HiddenField ID="hdnFatherID" runat="server" />
    </p>
            </td>

            <td>
                <div id="eMFamName" class="divError" runat="server"></div>
    <p>
        <label for="familyNameMother">Guardian 2's Family Name</label><br />
        <asp:TextBox ID="familyNameMother" runat="server" Width="300px" BackColor="ControlLight" MaxLength="30"></asp:TextBox>
    </p>
    <div id="eMFirstName" class="divError" runat="server"></div>
    <p>
        <label for="firstNameMother">Guardian 2's First Name</label><br />
        <asp:TextBox ID="firstNameMother" runat="server" Width="300px" BackColor="ControlLight" MaxLength="30"></asp:TextBox>
    </p>
    <div id="eMomOcc" class="divError" runat="server"></div>
    <p>
        <label for="occupationMother">Guardian 2's Occupation</label><br />
        <asp:TextBox ID="occupationMother" runat="server" Width="300px" BackColor="ControlLight" MaxLength="30"></asp:TextBox>
        <asp:HiddenField ID="hdnMotherID" runat="server" />
    </p>
            </td>
        </tr>
    </table>
    
    
    
</fieldset>
<fieldset id="fldSiblings" runat="server">
<legend>Siblings</legend>
    
    <div id="accordion">
    <table style="width:100%;table-layout:fixed">
        
        <tr>
            <td>
                <h3 class="accordion accordion-toggle" >Sibling 1</h3>
    <div class="panel accordion-content" id="sib1">
        <!-- 
    <p>
        <label for="familyNameSibling1">Family Name</label><br />
        <asp:TextBox ID="familyNameSibling1" runat="server" Width="300px" BackColor="ControlLight" MaxLength="30"></asp:TextBox>
    </p>
    <p>
        <label for="firstNameSibling1">First Name</label><br />
        <asp:TextBox ID="firstNameSibling1" runat="server" Width="300px" BackColor="ControlLight" MaxLength="30"></asp:TextBox>
    </p>
            
        <div id="eDOBSib1" class="divError" runat="server"></div>
    <p>
        <label for="dobSibling1">Date of Birth</label> (MM/DD/YYYY)<br />
        <asp:TextBox ID="dobSibling1" runat="server" Width="100px" BackColor="ControlLight" MaxLength="15"></asp:TextBox>
    </p>
            -->
    <p>
        <label for="ageSibling1">Age</label><br />
        <asp:TextBox ID="ageSibling1" runat="server" Width="100px" BackColor="ControlLight" MaxLength="15"></asp:TextBox>
    </p>
    <p>
        <label for="genderSibling1">Gender</label><br />
        <asp:RadioButtonList ID="genderSibling1" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
            <asp:ListItem Value="M">Male&nbsp;&nbsp;</asp:ListItem>
            <asp:ListItem Value="F">Female&nbsp;&nbsp;</asp:ListItem>
            <asp:ListItem Value="O">Other</asp:ListItem>
        </asp:RadioButtonList>
        <asp:HiddenField ID="hdnSibling1ID" runat="server" />
    </p>
   </div>
            </td>
            <td>
                <h3 class="accordion accordion-toggle" >Sibling 2</h3>
   <div class="panel accordion-content" >
      <!--
    <p>
        <label for="familyNameSibling2">Family Name</label><br />
        <asp:TextBox ID="familyNameSibling2" runat="server" Width="300px" BackColor="ControlLight" MaxLength="30"></asp:TextBox>
    </p>
    <p>
        <label for="firstNameSibling2">First Name</label><br />
        <asp:TextBox ID="firstNameSibling2" runat="server" Width="300px" BackColor="ControlLight" MaxLength="30"></asp:TextBox>
    </p>
       
       <div id="eDOBSib2" class="divError" runat="server"></div>
    <p>
        <label for="dobSibling2">Date of Birth</label> (MM/DD/YYYY)<br />
        <asp:TextBox ID="dobSibling2" runat="server" Width="100px" BackColor="ControlLight" MaxLength="15"></asp:TextBox>
    </p>
           -->
       <p>
        <label for="ageSibling2">Age</label><br />
        <asp:TextBox ID="ageSibling2" runat="server" Width="100px" BackColor="ControlLight" MaxLength="15"></asp:TextBox>
    </p>
    <p>
        <label for="genderSibling2">Gender</label><br />
        <asp:RadioButtonList ID="genderSibling2" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
            <asp:ListItem Value="M">Male&nbsp;&nbsp;</asp:ListItem>
            <asp:ListItem Value="F">Female&nbsp;&nbsp;</asp:ListItem>
            <asp:ListItem Value="O">Other</asp:ListItem>
        </asp:RadioButtonList>
        <asp:HiddenField ID="hdnSibling2ID" runat="server" />
    </p>
   </div>

            </td>
            <td>
                <h3 class="accordion accordion-toggle" >Sibling 3</h3>
   <div class="panel accordion-content">
      <!-- 
    <p>
        <label for="familyNameSibling3">Family Name</label><br />
        <asp:TextBox ID="familyNameSibling3" runat="server" Width="300px" BackColor="ControlLight" MaxLength="30"></asp:TextBox>
    </p>
    <p>
        <label for="firstNameSibling3">First Name</label><br />
        <asp:TextBox ID="firstNameSibling3" runat="server" Width="300px" BackColor="ControlLight" MaxLength="30"></asp:TextBox>
    </p>
           
       <div id="eDOBSib3" class="divError" runat="server"></div>
    <p>
        <label for="dobSibling3">Date of Birth</label> (MM/DD/YYYY)<br />
        <asp:TextBox ID="dobSibling3" runat="server" Width="100px" BackColor="ControlLight" MaxLength="15"></asp:TextBox>
    </p>
           -->
       <p>
        <label for="ageSibling3">Age</label><br />
        <asp:TextBox ID="ageSibling3" runat="server" Width="100px" BackColor="ControlLight" MaxLength="15"></asp:TextBox>
    </p>
    <p>
        <label for="genderSibling3">Gender</label><br />
        <asp:RadioButtonList ID="genderSibling3" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
            <asp:ListItem Value="M">Male&nbsp;&nbsp;</asp:ListItem>
            <asp:ListItem Value="F">Female&nbsp;&nbsp;</asp:ListItem>
            <asp:ListItem Value="O">Other</asp:ListItem>
        </asp:RadioButtonList>
        <asp:HiddenField ID="hdnSibling3ID" runat="server" />
    </p>
   </div>
            </td>
            <td>
                <h3 class="accordion accordion-toggle" >Sibling 4</h3>
   <div class="panel accordion-content">
      <!--
    <p>
        <label for="familyNameSibling4">Family Name</label><br />
        <asp:TextBox ID="familyNameSibling4" runat="server" Width="300px" BackColor="ControlLight" MaxLength="30"></asp:TextBox>
    </p>
    <p>
        <label for="familyNameSibling4">First Name</label><br />
        <asp:TextBox ID="firstNameSibling4" runat="server" Width="300px" BackColor="ControlLight" MaxLength="30"></asp:TextBox>
    </p>
           
       <div id="eDOBSib4" class="divError" runat="server"></div>
    <p>
        <label for="dobSibling4">Date of Birth</label> (MM/DD/YYYY)<br />
        <asp:TextBox ID="dobSibling4" runat="server" Width="100px" BackColor="ControlLight" MaxLength="15"></asp:TextBox>
    </p>
           -->
       <p>
        <label for="ageSibling4">Age</label><br />
        <asp:TextBox ID="ageSibling4" runat="server" Width="100px" BackColor="ControlLight" MaxLength="15"></asp:TextBox>
    </p>

    <p>
        <label for="genderSibling4">Gender</label><br />
        <asp:RadioButtonList ID="genderSibling4" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
            <asp:ListItem Value="M">Male&nbsp;&nbsp;</asp:ListItem>
            <asp:ListItem Value="F">Female&nbsp;&nbsp;</asp:ListItem>
            <asp:ListItem Value="O">Other</asp:ListItem>
        </asp:RadioButtonList>
        <asp:HiddenField ID="hdnSibling4ID" runat="server" />
    </p>
    </div>

            </td>
        </tr>
        
    </table>
    
        
    </div>
   

</fieldset>
<fieldset id="fldPreferences" runat="server">
<legend>Preferences</legend>
    
    <p><u>While all student preferences will be considered, CCS cannot guarantee they will all be satisfied.</u></p>
    
    
    <div style="float:left;width:100%;">
        <div style="float:left;width:50%">
            <h3>Home Environment</h3>
            <div id="eSmallChildren" class="divError" runat="server"></div>
            <p>
                <label for="rdoSmallChildren"><span style="color:#DD0000;font-weight:600;"> * </span>I would like to live in a home with:</label><br />
                <asp:RadioButtonList ID="rdoSmallChildren" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
                    <asp:ListItem Value="Young children">Young children&nbsp;&nbsp;</asp:ListItem>
                    <asp:ListItem Value="Older children">Older children&nbsp;&nbsp;</asp:ListItem>
                    <asp:ListItem Value="No children">No children</asp:ListItem>
                    <asp:ListItem Value="Children no preference">No Preference</asp:ListItem>
                </asp:RadioButtonList>
            </p>
            <div id="eTravel" class="divError" runat="server"></div>
            <p>
                <label for="rdoTraveledOutsideCountry"><span style="color:#DD0000;font-weight:600;"> * </span>Have you traveled outside your country before?</label><br />
                <asp:RadioButtonList ID="rdoTraveledOutsideCountry" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
                    <asp:ListItem Value="Traveled outside country">Yes&nbsp;&nbsp;</asp:ListItem>
                    <asp:ListItem Value="Not traveled outside country">No&nbsp;&nbsp;</asp:ListItem>
                </asp:RadioButtonList>
            </p>
            <p>
                <label for="traveledWhere">Which countries?</label><br />
                <asp:TextBox ID="traveledWhere" runat="server" Width="500px" BackColor="ControlLight" MaxLength="100"></asp:TextBox>
            </p>
            <div id="eSmoker" class="divError" runat="server"></div>
            <p>
                <label for="rdoSmoker"><span style="color:#DD0000;font-weight:600;"> * </span>Do you smoke?</label><br /> <span id="divApartment" runat="server">(Most host families do not allow smoking. CCS Homestay team recommends pursuing 
                an apartment if you are a smoker.)</span><br />
                <asp:RadioButtonList ID="rdoSmoker" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
                    <asp:ListItem Value="Smoker">Yes&nbsp;&nbsp;</asp:ListItem>
                    <asp:ListItem Value="Occasional smoker">Sometimes&nbsp;&nbsp;</asp:ListItem>
                    <asp:ListItem Value="Nonsmoker">No&nbsp;&nbsp;</asp:ListItem>
                </asp:RadioButtonList>
            </p>
            <p>
                <label for="smokingHabits">Explain your smoking habits:</label><br />
                <asp:TextBox ID="smokingHabits" runat="server" Width="500px" BackColor="ControlLight" MaxLength="100"></asp:TextBox>
            </p>
            
             <div id="eOtherSmokerOK" class="divError" runat="server"></div>
            <p>
                <label for="rdoOtherSmokerOK"><span style="color:#DD0000;font-weight:600;"> * </span>Is it okay if there are smokers in your homestay family?</label><br />
                <asp:RadioButtonList ID="rdoOtherSmokerOK" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
                    <asp:ListItem Value="Other smoker OK">Yes&nbsp;&nbsp;</asp:ListItem>
                    <asp:ListItem Value="Other smoker not OK">No&nbsp;&nbsp;</asp:ListItem>
                    <asp:ListItem Value="Other smoker no preference">No Preference</asp:ListItem>
                </asp:RadioButtonList>
            </p>
            <div id="eInteraction" class="divError" runat="server"></div>
            <p>
                <label for="rdoInteraction"><span style="color:#DD0000;font-weight:600;"> * </span>How much interaction and conversation with your homestay family do you prefer?</label><br />
                <asp:RadioButtonList ID="rdoInteraction" runat="server" RepeatDirection="Vertical" RepeatLayout="Flow" style="width:100%;">
                    <asp:ListItem Value="Interaction every day">Every Day&nbsp;&nbsp;</asp:ListItem>
                    <asp:ListItem Value="Interaction frequent">Frequent, but not every day</asp:ListItem>
                    <asp:ListItem Value="Interaction minimal">I'm independent and minimal conversation is okay</asp:ListItem>
                </asp:RadioButtonList>
            </p>
            <div id="eHomeEnvironment" class="divError" runat="server"></div>
            <p>
                <label for="rdoHomeEnvironment"><span style="color:#DD0000;font-weight:600;"> * </span>I would like to live in a home that is more:</label><br />
                <asp:RadioButtonList ID="rdoHomeEnvironment" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
                    <asp:ListItem Value="Home environment quiet">Quiet&nbsp;&nbsp;</asp:ListItem>
                    <asp:ListItem Value="Home environment active">Busy and Active&nbsp;&nbsp;</asp:ListItem>
                </asp:RadioButtonList>
            </p>
     
            <p>
                <label for="homeEnvironmentPreferences">Explain:</label><br />
                <asp:TextBox ID="homeEnvironmentPreferences" runat="server" Width="500px" BackColor="ControlLight" MaxLength="100"></asp:TextBox>
            </p>
        </div>


        <div style="float:left;width:50%">
            <h3>Hobbies</h3>
            What hobbies, sports or other activities do you enjoy?<br />
            <asp:CheckBox ID="MusicalInstrument" runat="server" Text="Play a musical instrument" /><br />
            <asp:CheckBox ID="Art" runat="server" Text="Drawing/painting" /><br />
            <asp:CheckBox ID="TeamSports" runat="server" Text="Team sports" /><br />
            <asp:CheckBox ID="IndividualSports" runat="server" Text="Individual sports" /><br />
            <asp:CheckBox ID="ListenMusic" runat="server" Text="Listen to music" /><br />
            <asp:CheckBox ID="Drama" runat="server" Text="Drama" /><br />
            <asp:CheckBox ID="WatchMovies" runat="server" Text="Watch movies" /><br />
            <asp:CheckBox ID="Singing" runat="server" Text="Singing" /><br />
            <asp:CheckBox ID="Shopping" runat="server" Text="Shopping" /><br />
            <asp:CheckBox ID="ReadBooks" runat="server" Text="Read books" /><br />
            <asp:CheckBox ID="Outdoors" runat="server" Text="Hiking/camping/outdoors" /><br />
            <asp:CheckBox ID="Cooking" runat="server" Text="Cooking" /><br />
            <asp:CheckBox ID="Photography" runat="server" Text="Photography" /><br />
            <asp:CheckBox ID="Gaming" runat="server" Text="Gaming" />
            <p>
            <label for="activitiesEnjoyed">Other hobbies/activities you enjoy:</label><br />
            <asp:TextBox ID="activitiesEnjoyed" runat="server" Width="500px" BackColor="ControlLight" MaxLength="100"></asp:TextBox>
            </p>
        </div>
    </div>
    
    
    <br /><br /><br />
   
    
    <div style="float:left;width:100%;">
        
        <div style="float:left;width:50%">
            <h3>Diet</h3>
            <p>
                Please indicate your dietary needs:<br />
                <asp:CheckBox ID="Vegetarian" runat="server" Text="Vegetarian" /><br />
                <asp:CheckBox ID="GlutenFree" runat="server" Text="Gluten free" /><br />
                <asp:CheckBox ID="DairyFree" runat="server" Text="Dairy free" /><br />
                <asp:CheckBox ID="OtherFoodAllergy" runat="server" Text="Other food allergy" /><br />
                <asp:CheckBox ID="Halal" runat="server" Text="Halal" />  (Food prepared as prescribed by Muslim law)<br />
                <asp:CheckBox ID="Kosher" runat="server" Text="Kosher" /><br />
                <asp:CheckBox ID="NoSpecialDiet" runat="server" Text="I have no special dietary needs" /><br />
            </p>
        </div>
        <div style="float:left;width:50%">
            <fieldset id="fldConcerns" runat="server">
            <h3>Other Concerns</h3>
            <p>
                <label for="allergies">Do you have any allergies to food, drugs, animals or anything else?</label><br />
                <asp:TextBox ID="allergies" runat="server" Width="500px" BackColor="ControlLight" MaxLength="100"></asp:TextBox>
            </p>
            <p>
                <label for="healthConditions">Do you have any health conditions that would prevent you from participation in normal and regular physical activities?</label><br />
                <asp:TextBox ID="healthConditions" runat="server" Width="500px" BackColor="ControlLight" MaxLength="100"></asp:TextBox>
            </p>
            <div id="eHealthStatus" class="divError" runat="server"></div>
            <p>
                <label for="rdoHealthStatus"><span style="color:#DD0000;font-weight:600;"> * </span>How would you describe your present condition of health?</label><br />
                <asp:RadioButtonList ID="rdoHealthStatus" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
                    <asp:ListItem Value="Health excellent">Excellent&nbsp;&nbsp;</asp:ListItem>
                    <asp:ListItem Value="Health average">Average&nbsp;&nbsp;</asp:ListItem>
                    <asp:ListItem Value="Health poor">Poor</asp:ListItem>
                </asp:RadioButtonList>
            </p>
    
            <p>
                <label for="rdoDriveCar"><span style="color:#DD0000;font-weight:600;"> * </span>Are you planning to drive a car as soon as you arrive?</label><br />
                <asp:RadioButtonList ID="rdoDriveCar" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
                    <asp:ListItem Value="1">Yes&nbsp;&nbsp;</asp:ListItem>
                    <asp:ListItem Value="0">No&nbsp;&nbsp;</asp:ListItem>
                </asp:RadioButtonList>
            </p>
            <p>
                <label for="anythingElse">Is there anything else you'd like us to know about you?</label><br />
                <asp:TextBox ID="anythingElse" runat="server" Width="500px" BackColor="ControlLight" MaxLength="100"></asp:TextBox>
            </p>

            </fieldset>
        </div>
        
    </div>
    <br />
    
   
    </fieldset>

<div id="divWaiver" runat="server">

<h2></h2>
</div>
<div id="divButtons" runat="server">
    <div id="btnSave" runat="server" style="float:left;">
        <!-- <input type="submit" name="btnSubmit" value="Save"/>&nbsp;&nbsp; -->
        <asp:Button ID="btnCmdSave" runat="server" Text="Save" OnClick="btnCmdSave_Click" />&nbsp;&nbsp;
    </div>
    <div id="divUpdate" runat="server" visible="false" style="float:left;">
      <!--  <input type="submit" name="btnSubmit" id="btnUpdate" value="Save Changes" /> -->
        <asp:Button ID="btnCmdSaveChanges" runat="server" Text="Save Changes" OnClick="btnCmdSave_Click" />
    </div>
    <div id="divNextPage" runat="server" style="float:left;margin-left:30px;">
      <!--  <input type="submit" name="btnSubmit" value="Continue"/>&nbsp;&nbsp; -->
        <asp:Button ID="btnCmdContinue" runat="server" Text="Continue" OnClick="btnCmdContinue_Click" />&nbsp;&nbsp;
        <asp:Button ID="Button1" runat="server" Text="Return to Application page" OnClick="btnCmdBackApplication_Click" />&nbsp;&nbsp;
    </div>
    <div id="btnPrevious" runat="server" style="float:left;margin-left:30px;">
       <!-- <input type="submit" name="btnSubmit" value="Back" /> -->
        <!--<asp:Button ID="btnCmdBack" runat="server" Text="Back" OnClick="btnCmdBack_Click" /> -->
    </div>
</div>
<asp:HiddenField ID="hdnApplicantID" runat="server" />
<script type="text/javascript">
    
    $(function () {
        //To display "+" or "-" for accordion 
        var acc = document.getElementsByClassName("accordion");
        for (var i = 0; i < acc.length; i++) {
            acc[i].addEventListener("click", function () {
                this.classList.toggle("active");
            });
        }
        $('#accordion').find('.accordion-toggle').each(function () {
            //$(this).next().slideToggle('fast');
            //if the first input field has data, it will expand
            if ($(this).next().find("input[type=text]").first().val() != "") {
                $(this).next().slideToggle('fast');
            }

        });
        $('#accordion').find('.accordion-toggle').click(function () {
            //Expand or collapse this panel
            $(this).next().slideToggle('fast');
        });
    });
</script>
    
   

</form>
</body>
</html>
