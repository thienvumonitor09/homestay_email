﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PlacementProfile.aspx.cs" Inherits="Homestay_PlacementProfile" %>
<!DOCTYPE html>

<html lang="en-us" >

<head id="Head1" runat="server">
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <title>CCS Homestay Program</title>
    <link id="Link1" href="/_css/ccs.css" rel="stylesheet"  type="text/css" />
    <link href="/Homestay/_css/Homestay.css" rel="stylesheet" type="text/css" />  
    <link href="/_css/Forms.css" rel="stylesheet" type="text/css" />         
    <meta http-equiv="X-UA-Compatible" content="IE=9" />        
    <script type="text/javascript" src="/_js/pushToHttps.js"></script>
    <script type="text/javascript" src="/_js/jquery-1.9.1.min.js"></script>
    <link href="/Homestay/_css/Homestay.css" rel="stylesheet" type="text/css" />
    <script src="/_js/jquery.mask.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.dobSibling1').mask('00/00/0000');
            $('.dobSibling2').mask('00/00/0000');
            $('.dobSibling3').mask('00/00/0000');
            $('.dobSibling4').mask('00/00/0000');
        });
    </script>
</head>

<body>
<form id="form1" runat="server">
<h1>CCS Homestay Program</h1>

<h2>Student Profile</h2>
<asp:Literal ID="litResultMsg" runat="server"></asp:Literal>
<fieldset id="fldParents" runat="server">
<legend>Parents</legend>
    <p>
        <label for="familyNameFather">Father's Family  Name</label><br />
        <asp:TextBox ID="familyNameFather" runat="server" Width="300px" BackColor="ControlLight" MaxLength="30"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="familyNameFather" runat="server" CssClass="redText" ErrorMessage="Father's family name is required."></asp:RequiredFieldValidator>
    </p>
    <p>
        <label for="firstNameFather">Father's First Name</label><br />
        <asp:TextBox ID="firstNameFather" runat="server" Width="300px" BackColor="ControlLight" MaxLength="30"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="firstNameFather" runat="server" CssClass="redText" ErrorMessage="Father's first name is required."></asp:RequiredFieldValidator>
    </p>
     <p>
        <label for="occupationFather">Father's Occupation</label><br />
        <asp:TextBox ID="occupationFather" runat="server" Width="300px" BackColor="ControlLight" MaxLength="30"></asp:TextBox>
        <asp:HiddenField ID="hdnFatherID" runat="server" />
        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="occupationFather" runat="server" CssClass="redText" ErrorMessage="Father's occupation is required."></asp:RequiredFieldValidator>
    </p>
    <p>
        <label for="familyNameMother">Mother's Family Name</label><br />
        <asp:TextBox ID="familyNameMother" runat="server" Width="300px" BackColor="ControlLight" MaxLength="30"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ControlToValidate="familyNameMother" runat="server" CssClass="redText" ErrorMessage="Mother's family name is required."></asp:RequiredFieldValidator>
    </p>
    <p>
        <label for="firstNameMother">Mother's First Name</label><br />
        <asp:TextBox ID="firstNameMother" runat="server" Width="300px" BackColor="ControlLight" MaxLength="30"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ControlToValidate="firstNameMother" runat="server" CssClass="redText" ErrorMessage="Mother's first name is required."></asp:RequiredFieldValidator>
    </p>
    <p>
        <label for="occupationMother">Mother's Occupation</label><br />
        <asp:TextBox ID="occupationMother" runat="server" Width="300px" BackColor="ControlLight" MaxLength="30"></asp:TextBox>
        <asp:HiddenField ID="hdnMotherID" runat="server" />
        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ControlToValidate="occupationMother" runat="server" CssClass="redText" ErrorMessage="Mother's occupation is required."></asp:RequiredFieldValidator>
    </p>
</fieldset>
<fieldset id="fldSiblings" runat="server">
<legend>Brothers and Sisters</legend>
<div id="accordion">
    <h3 class="accordion accordion-toggle">Sibling 1</h3>
    <div class="panel accordion-content default">
    <p>
        <label for="familyNameSibling1">Family Name</label><br />
        <asp:TextBox ID="familyNameSibling1" runat="server" Width="300px" BackColor="ControlLight" MaxLength="30"></asp:TextBox>
    </p>
    <p>
        <label for="firstNameSibling1">First Name</label><br />
        <asp:TextBox ID="firstNameSibling1" runat="server" Width="300px" BackColor="ControlLight" MaxLength="30"></asp:TextBox>
    </p>
    <p>
        <label for="dobSibling1">Date of Birth</label> (MM/DD/YYYY)<br />
        <asp:TextBox ID="dobSibling1" runat="server" Width="100px" BackColor="ControlLight" MaxLength="15"></asp:TextBox>
    </p>
    <p>
        <label for="genderSibling1">Gender</label><br />
        <asp:RadioButtonList ID="genderSibling1" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
            <asp:ListItem Value="M">Male&nbsp;&nbsp;</asp:ListItem>
            <asp:ListItem Value="F">Female&nbsp;&nbsp;</asp:ListItem>
            <asp:ListItem Value="O">Other</asp:ListItem>
        </asp:RadioButtonList>
        <asp:HiddenField ID="hdnSibling1ID" runat="server" />
    </p>
   </div>
   <h3 class="accordion accordion-toggle">Sibling 2</h3>
   <div class="panel accordion-content">
    <p>
        <label for="familyNameSibling2">Family Name</label><br />
        <asp:TextBox ID="familyNameSibling2" runat="server" Width="300px" BackColor="ControlLight" MaxLength="30"></asp:TextBox>
    </p>
    <p>
        <label for="firstNameSibling2">First Name</label><br />
        <asp:TextBox ID="firstNameSibling2" runat="server" Width="300px" BackColor="ControlLight" MaxLength="30"></asp:TextBox>
    </p>
    <p>
        <label for="dobSibling2">Date of Birth</label> (MM/DD/YYYY)<br />
        <asp:TextBox ID="dobSibling2" runat="server" Width="100px" BackColor="ControlLight" MaxLength="15"></asp:TextBox>
    </p>
    <p>
        <label for="genderSibling2">Gender</label><br />
        <asp:RadioButtonList ID="genderSibling2" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
            <asp:ListItem Value="M">Male&nbsp;&nbsp;</asp:ListItem>
            <asp:ListItem Value="F">Female&nbsp;&nbsp;</asp:ListItem>
            <asp:ListItem Value="O">Other</asp:ListItem>
        </asp:RadioButtonList>
        <asp:HiddenField ID="hdnSibling2ID" runat="server" />
    </p>
   </div>
   <h3 class="accordion accordion-toggle">Sibling 3</h3>
   <div class="panel accordion-content">
    <p>
        <label for="familyNameSibling3">Family Name</label><br />
        <asp:TextBox ID="familyNameSibling3" runat="server" Width="300px" BackColor="ControlLight" MaxLength="30"></asp:TextBox>
    </p>
    <p>
        <label for="firstNameSibling3">First Name</label><br />
        <asp:TextBox ID="firstNameSibling3" runat="server" Width="300px" BackColor="ControlLight" MaxLength="30"></asp:TextBox>
    </p>
    <p>
        <label for="dobSibling3">Date of Birth</label> (MM/DD/YYYY)<br />
        <asp:TextBox ID="dobSibling3" runat="server" Width="100px" BackColor="ControlLight" MaxLength="15"></asp:TextBox>
    </p>
    <p>
        <label for="genderSibling3">Gender</label><br />
        <asp:RadioButtonList ID="genderSibling3" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
            <asp:ListItem Value="M">Male&nbsp;&nbsp;</asp:ListItem>
            <asp:ListItem Value="F">Female&nbsp;&nbsp;</asp:ListItem>
            <asp:ListItem Value="O">Other</asp:ListItem>
        </asp:RadioButtonList>
        <asp:HiddenField ID="hdnSibling3ID" runat="server" />
    </p>
   </div>
   <h3 class="accordion accordion-toggle">Sibling 4</h3>
   <div class="panel accordion-content">
    <p>
        <label for="familyNameSibling4">Family Name</label><br />
        <asp:TextBox ID="familyNameSibling4" runat="server" Width="300px" BackColor="ControlLight" MaxLength="30"></asp:TextBox>
    </p>
    <p>
        <label for="familyNameSibling4">First Name</label><br />
        <asp:TextBox ID="firstNameSibling4" runat="server" Width="300px" BackColor="ControlLight" MaxLength="30"></asp:TextBox>
    </p>
    <p>
        <label for="dobSibling4">Date of Birth</label> (MM/DD/YYYY)<br />
        <asp:TextBox ID="dobSibling4" runat="server" Width="100px" BackColor="ControlLight" MaxLength="15"></asp:TextBox>
    </p>
    <p>
        <label for="genderSibling4">Gender</label><br />
        <asp:RadioButtonList ID="genderSibling4" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
            <asp:ListItem Value="M">Male&nbsp;&nbsp;</asp:ListItem>
            <asp:ListItem Value="F">Female&nbsp;&nbsp;</asp:ListItem>
            <asp:ListItem Value="O">Other</asp:ListItem>
        </asp:RadioButtonList>
        <asp:HiddenField ID="hdnSibling4ID" runat="server" />
    </p>
    </div>
</div>

</fieldset>
<fieldset id="fldPreferences" runat="server">
<legend>Preferences</legend>
    <p><u>While all student preferences will be considered, CCS cannot guarantee they will all be satisfied.</u></p>
    <p>
        <label for="rdoSmallChildren">I would like to live in a home with:</label><br />
        <asp:RadioButtonList ID="rdoSmallChildren" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
            <asp:ListItem Value="Young children">Young children&nbsp;&nbsp;</asp:ListItem>
            <asp:ListItem Value="Older children">Older children&nbsp;&nbsp;</asp:ListItem>
            <asp:ListItem Value="No children">No children</asp:ListItem>
            <asp:ListItem Value="Children no preference">No Preference</asp:ListItem>
        </asp:RadioButtonList>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ControlToValidate="rdoSmallChildren" runat="server" CssClass="redText" ErrorMessage="Please select your home environment preference."></asp:RequiredFieldValidator>
    </p>
    <p>
        <label for="rdoTraveledOutsideCountry">Have you traveled outside your country before?</label><br />
        <asp:RadioButtonList ID="rdoTraveledOutsideCountry" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
            <asp:ListItem Value="Traveled outside country">Yes&nbsp;&nbsp;</asp:ListItem>
            <asp:ListItem Value="Not traveled outside country">No&nbsp;&nbsp;</asp:ListItem>
        </asp:RadioButtonList>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ControlToValidate="occupationMother" runat="server" CssClass="redText" ErrorMessage="Please indicate whether or not you have traveled outside your country."></asp:RequiredFieldValidator>
    </p>
    <p>
        <label for="traveledWhere">Explain:</label><br />
        <asp:TextBox ID="traveledWhere" runat="server" Width="500px" BackColor="ControlLight" MaxLength="100"></asp:TextBox>
    </p>
    <p>
        <label for="rdoSmoker">Do you smoke?</label><br /> <span id="divApartment" runat="server">(Most host families do not allow smoking. CCS Homestay team recommends pursuing 
        an apartment if you are a smoker.)</span><br />
        <asp:RadioButtonList ID="rdoSmoker" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
            <asp:ListItem Value="Smoker">Yes&nbsp;&nbsp;</asp:ListItem>
            <asp:ListItem Value="Occasional smoker">Sometimes&nbsp;&nbsp;</asp:ListItem>
            <asp:ListItem Value="Nonsmoker">No&nbsp;&nbsp;</asp:ListItem>
        </asp:RadioButtonList>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" ControlToValidate="rdoSmoker" runat="server" CssClass="redText" ErrorMessage="Please select your smoking status."></asp:RequiredFieldValidator>
    </p>
    <p>
        <label for="smokingHabits">Explain your smoking habits:</label><br />
        <asp:TextBox ID="smokingHabits" runat="server" Width="500px" BackColor="ControlLight" MaxLength="100"></asp:TextBox>
    </p>
    <p>
        <label for="rdoOtherSmokerOK">Is it okay if there are smokers in your homestay family?</label><br />
        <asp:RadioButtonList ID="rdoOtherSmokerOK" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
            <asp:ListItem Value="Other smoker OK">Yes&nbsp;&nbsp;</asp:ListItem>
            <asp:ListItem Value="Other smoker not OK">No&nbsp;&nbsp;</asp:ListItem>
            <asp:ListItem Value="Other smoker no preference">No Preference</asp:ListItem>
        </asp:RadioButtonList>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator10" ControlToValidate="rdoOtherSmokerOK" runat="server" CssClass="redText" ErrorMessage="Please select your smoking preference."></asp:RequiredFieldValidator>
    </p>
    <p>
        What hobbies, sports or other activities do you enjoy?<br />
        <asp:CheckBox ID="MusicalInstrument" runat="server" Text="Play a musical instrument" /><br />
        <asp:CheckBox ID="Art" runat="server" Text="Drawing/painting" /><br />
        <asp:CheckBox ID="TeamSports" runat="server" Text="Team sports" /><br />
        <asp:CheckBox ID="IndividualSports" runat="server" Text="Individual sports" /><br />
        <asp:CheckBox ID="ListenMusic" runat="server" Text="Listen to music" /><br />
        <asp:CheckBox ID="Drama" runat="server" Text="Drama" /><br />
        <asp:CheckBox ID="WatchMovies" runat="server" Text="Watch movies" /><br />
        <asp:CheckBox ID="Singing" runat="server" Text="Singing" /><br />
        <asp:CheckBox ID="Shopping" runat="server" Text="Shopping" /><br />
        <asp:CheckBox ID="ReadBooks" runat="server" Text="Read books" /><br />
        <asp:CheckBox ID="Outdoors" runat="server" Text="Hiking/camping/outdoors" /><br />
        <asp:CheckBox ID="Cooking" runat="server" Text="Cooking" /><br />
        <asp:CheckBox ID="Photography" runat="server" Text="Photography" /><br />
        <asp:CheckBox ID="Gaming" runat="server" Text="Gaming" />
    </p>
    <p>
        <label for="activitiesEnjoyed">Other hobbies/activities you enjoy:</label><br />
        <asp:TextBox ID="activitiesEnjoyed" runat="server" Width="500px" BackColor="ControlLight" MaxLength="100"></asp:TextBox>
    </p>
    <p>
        <label for="rdoInteraction">How much interaction and conversation with your homestay family do you prefer?</label><br />
        <asp:RadioButtonList ID="rdoInteraction" runat="server" RepeatDirection="Vertical" RepeatLayout="Flow" style="width:100%;">
            <asp:ListItem Value="Interaction every day">Every Day&nbsp;&nbsp;</asp:ListItem>
            <asp:ListItem Value="Interaction frequent">Frequent, but not every day</asp:ListItem>
            <asp:ListItem Value="Interaction minimal">I'm independent and minimal conversation is okay</asp:ListItem>
        </asp:RadioButtonList>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" ControlToValidate="rdoInteraction" runat="server" CssClass="redText" ErrorMessage="Please select your home interaction preference."></asp:RequiredFieldValidator>
    </p>
    <p>
        <label for="rdoHomeEnvironment">I would like to live in a home that is more:</label><br />
        <asp:RadioButtonList ID="rdoHomeEnvironment" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
            <asp:ListItem Value="Home environment quiet">Quiet&nbsp;&nbsp;</asp:ListItem>
            <asp:ListItem Value="Home environment active">Busy and Active&nbsp;&nbsp;</asp:ListItem>
        </asp:RadioButtonList>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" ControlToValidate="rdoHomeEnvironment" runat="server" CssClass="redText" ErrorMessage="Please select your home environment preference."></asp:RequiredFieldValidator>
    </p>
    <p>
        <label for="homeEnvironmentPreferences">Explain:</label><br />
        <asp:TextBox ID="homeEnvironmentPreferences" runat="server" Width="500px" BackColor="ControlLight" MaxLength="100"></asp:TextBox>
    </p>
    <p>
        Please indicate your dietary needs:<br />
        <asp:CheckBox ID="Vegetarian" runat="server" Text="Vegetarian" /><br />
        <asp:CheckBox ID="GlutenFree" runat="server" Text="Gluten free" /><br />
        <asp:CheckBox ID="DairyFree" runat="server" Text="Dairy free" /><br />
        <asp:CheckBox ID="OtherFoodAllergy" runat="server" Text="Other food allergy" /><br />
        <asp:CheckBox ID="Halal" runat="server" Text="Halal" />  (Food prepared as prescribed by Muslim law)<br />
        <asp:CheckBox ID="Kosher" runat="server" Text="Kosher" /><br />
        <asp:CheckBox ID="NoSpecialDiet" runat="server" Text="I have no special dietary needs" /><br />
    </p>
    </fieldset>
<fieldset id="fldConcerns" runat="server">
<legend>Other Concerns</legend>
    <p>
        <label for="allergies">Do you have any allergies to food, drugs, animals or anything else?</label><br />
        <asp:TextBox ID="allergies" runat="server" Width="500px" BackColor="ControlLight" MaxLength="100"></asp:TextBox>
    </p>
    <p>
        <label for="healthConditions">Do you have any health conditions that would prevent you from participation in normal and regular physical activities?</label><br />
        <asp:TextBox ID="healthConditions" runat="server" Width="500px" BackColor="ControlLight" MaxLength="100"></asp:TextBox>
    </p>
    <p>
        <label for="rdoHealthStatus">How would you describe your present condition of health?</label><br />
        <asp:RadioButtonList ID="rdoHealthStatus" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
            <asp:ListItem Value="Health excellent">Excellent&nbsp;&nbsp;</asp:ListItem>
            <asp:ListItem Value="Health average">Average&nbsp;&nbsp;</asp:ListItem>
            <asp:ListItem Value="Health poor">Poor</asp:ListItem>
        </asp:RadioButtonList>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator13" ControlToValidate="rdoHealthStatus" runat="server" CssClass="redText" ErrorMessage="Please indicate your health status."></asp:RequiredFieldValidator>
    </p>
    <p>
        <label for="rdoDriveCar">Are you planning to drive a car as soon as you arrive?</label><br />
        <asp:RadioButtonList ID="rdoDriveCar" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
            <asp:ListItem Value="1">Yes&nbsp;&nbsp;</asp:ListItem>
            <asp:ListItem Value="0">No&nbsp;&nbsp;</asp:ListItem>
        </asp:RadioButtonList>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator14" ControlToValidate="rdoDriveCar" runat="server" CssClass="redText" ErrorMessage="Please select your driving plan."></asp:RequiredFieldValidator>
    </p>
    <p>
        <label for="anythingElse">Is there anything else you'd like us to know about you?</label><br />
        <asp:TextBox ID="anythingElse" runat="server" Width="500px" BackColor="ControlLight" MaxLength="100"></asp:TextBox>
    </p>

</fieldset>
<div id="divWaiver" runat="server">

<h2></h2>
</div>
<div id="divButtons" runat="server">
    <div id="btnSave" runat="server" style="float:left;">
        <input type="submit" name="btnSubmit" value="Save"/>&nbsp;&nbsp;
    </div>
    <div id="divUpdate" runat="server" visible="false" style="float:left;">
        <input type="submit" name="btnSubmit" id="btnUpdate" value="Save Changes" />
    </div>
    <div id="divNextPage" runat="server" style="float:left;margin-left:30px;">
        <input type="submit" name="btnSubmit" value="Continue"/>&nbsp;&nbsp;
    </div>
    <div id="btnPrevious" runat="server" style="float:left;margin-left:30px;">
        <input type="submit" name="btnSubmit" value="Back" />
    </div>
</div>
<asp:HiddenField ID="hdnApplicantID" runat="server" />
<script type="text/javascript">
    $(document).ready(function ($) {
        $('#accordion').find('.accordion-toggle').click(function () {

            //Expand or collapse this panel
            $(this).next().slideToggle('fast');

            //Hide the other panels
            $(".accordion-content").not($(this).next()).slideUp('fast');

        });
    });
</script>
<script>
    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].addEventListener("click", function () {
            this.classList.toggle("active");
        });
    }
</script>
</form>
</body>
</html>
