﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for StudentReport
/// </summary>
public class StudentReport
{
    public string FullName { get; set; }

    public string Status { get; set; }
    public string Gender { get; set; }
    public string Id { get; set; }
    public string Campus { get; set; }
    public string Interest { get; set; }
    public string Age { get; set; }
    public string DOB { get; set; }
    public string Country { get; set; }
    public string Email { get; set; }
    public string CellPhone { get; set; }
    public string QuarterStartDate { get; set; } //added 03-15/2019
    public string Notes { get; set; } //added 03-15/2019
    public string HostFamily { get; set; } //added 03-15/2019
    public string SuccessMeeting { get; set; } //added 03-19/2019
    public string Agreement { get; set; } //added 03-19/2019
    public string LocalAddress { get; set; } //added 03-19-2019

    public StudentReport()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}