USE [CCSInternationalStudent]
GO

/****** Object:  Table [dbo].[EmailReports]    Script Date: 1/10/2019 10:46:02 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[EmailReports](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[fromEmail] [varchar](max) NOT NULL,
	[toEmail] [varchar](max) NOT NULL,
	[ccEmail] [varchar](max) NULL,
	[subject] [varchar](500) NOT NULL,
	[emailContent] [varchar](max) NOT NULL,
	[attachments] [varchar](1000) NULL,
	[timestamp] [datetime] NOT NULL,
 CONSTRAINT [PK_EmailReports] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


