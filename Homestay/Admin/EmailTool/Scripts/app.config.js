﻿app.config(function ($routeProvider) {
    $routeProvider
        .when("/", {
            templateUrl: "views/sendEmail.html",
            controller: 'emailCtrl'
        })
        .when("/confirmation", {
            templateUrl: "views/confirmation.html",
            controller: 'emailCtrl'
        })
        .when("/templates", {
            templateUrl: "views/templates.html",
            controller: 'templateCtrl'
        })
        .when("/reports", {
            templateUrl: "views/reports.html",
            controller: 'reportCtrl'
        })
        .when("/individualReport/:idex", {
            templateUrl: "views/individualReport.html",
            controller: 'individualReportCtrl'
        });
});