﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;

/// <summary>
/// Summary description for CCSOutReach
/// </summary>
public class CCSDropDownLists
{
	public CCSDropDownLists()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    /// <summary>
    /// Runs the queries specified by the other functions.  Original code by Remi Olsen; adapted for OnlineSurvey drop-down lists.
    /// </summary>
    /// <param name="procedure"></param>
    /// <param name="parameters"></param>
    /// <param name="returnvalues"></param>
    /// <returns></returns>
    public ArrayList SPQuery(string procedure, ArrayList parameters, bool returnvalues) {
        // Set up connection
        SqlConnection dbcon = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString_CCSOutreach"].ConnectionString);
        dbcon.Open();

        SqlCommand sp = new SqlCommand(procedure, dbcon);
        sp.CommandType = CommandType.StoredProcedure;
        ArrayList result = new ArrayList();
        ArrayList outparameters = new ArrayList();
        bool returnsresult = false;
        
        // Set parameters
        for (int i = 0; i < parameters.Count; i++) {
            string df    = ((ArrayList)parameters[i])[0].ToString();
            SqlDbType sd = (SqlDbType)((ArrayList)parameters[i])[4];
            int len      = Convert.ToInt16(((ArrayList)parameters[i])[1]);
            string inout = ((ArrayList)parameters[i])[2].ToString();
            string val   = ((ArrayList)parameters[i])[3].ToString();
            sp.Parameters.Add (df, sd, len);
            if (inout == "input") {
                sp.Parameters[df].Value = val;
            }
        }
        
        // Execute and grab data
        if ((returnvalues)) {
            SqlDataReader reader = sp.ExecuteReader();
            while (reader.Read()) {
                ArrayList row = new ArrayList();
                for (int i = 0; i < reader.FieldCount; i++) {
                    row.Add(reader[i]);
                }
                result.Add(row);
            }
            reader.Close();
            reader = null;
        } else {
            ArrayList row = new ArrayList();
            row.Add(sp.ExecuteNonQuery());
            result.Add(row);
        }

        // Clean up
        sp.Dispose();
        sp = null;
         dbcon.Close();
        return result;
    }

    /// <summary>
    /// Returns a zero-based ArrayList with alphabetical list of countries as of 2008
    /// </summary>
    /// <returns></returns>
    public ArrayList populateCountries()
    {
        //CCSOutreach database, original source, is obsolete.  See arrays below.
        //ArrayList ar = SPQuery("GetCountries", new ArrayList(");, true)
        //return ar;
        /* implementation logic
        int selectindex = 0;
        try
        {
            int countcount = 0;
            foreach (ArrayList outar in ar");
            {
                try
                {
                    this.country.Items.Add(outar[0].ToString(");)
                    if (outar[0].ToString("); == "United States of America""); selectindex = countcount;
                    countcount++;

                }
                catch (Exception ex");
                {
                    //Response.Write(p.ErrorMessage(ex.ToString(");");)
                    lblDebug.Text = ex.ToString()
                }
            }
        }
        catch (Exception ex");
        {
            //Response.Write(p.ErrorMessage(ex.ToString(");");)
            lblDebug.Text = ex.ToString()
        }
        finally
        {
            if (!IsPostBack"); this.country.SelectedIndex = selectindex;
        }
        p = null;
        */
        ArrayList ar = new ArrayList();
                ar.Add("United States of America");
                ar.Add("Canada");
                ar.Add("Mexico");
                ar.Add("Afghanistan");
                ar.Add("Albania");
	            ar.Add("Algeria");
	            ar.Add("Andorra");
	            ar.Add("Angola");
	            ar.Add("Antigua and Barbuda");
	            ar.Add("Argentina");
	            ar.Add("Armenia");
	            ar.Add("Australia");
	            ar.Add("Austria");
	            ar.Add("Azerbaijan");
	            ar.Add("Bahamas");
	            ar.Add("Bahrain");
	            ar.Add("Bangladesh");
	            ar.Add("Barbados");
	            ar.Add("Belarus");
	            ar.Add("Belgium");
	            ar.Add("Belize");
	            ar.Add("Benin");
	            ar.Add("Bhutan");
	            ar.Add("Bolivia");
	            ar.Add("Bosnia and Herzegovina");
	            ar.Add("Botswana");
	            ar.Add("Brazil");
	            ar.Add("Brunei Darussalam");
	            ar.Add("Bulgaria");
	            ar.Add("Burkina Faso");
	            ar.Add("Burundi");
	            ar.Add("Cambodia");
	            ar.Add("Cameroon");
	            ar.Add("Cape Verde");
	            ar.Add("Central African Republic");
	            ar.Add("Chad");
	            ar.Add("Chile");
	            ar.Add("China");
	            ar.Add("Colombia");
	            ar.Add("Comoros");
	            ar.Add("Congo");
	            ar.Add("Cook Islands");
	            ar.Add("Costa Rica");
	            ar.Add("C&#244;te d Ivoire");
	            ar.Add("Croatia");
	            ar.Add("Cuba");
	            ar.Add("Cyprus");
	            ar.Add("Czech Republic");
	            ar.Add("Democratic Peoples Republic of Korea");
	            ar.Add("Democratic Republic of the Congo");
	            ar.Add("Denmark");
	            ar.Add("Djibouti");
	            ar.Add("Dominica");
	            ar.Add("Dominican Republic");
	            ar.Add("Ecuador");
	            ar.Add("Egypt");
	            ar.Add("El Salvador");
	            ar.Add("Equatorial Guinea");
	            ar.Add("Eritrea");
	            ar.Add("Estonia");
	            ar.Add("Ethiopia");
	            ar.Add("Fiji");
	            ar.Add("Finland");
	            ar.Add("France");
	            ar.Add("Gabon");
	            ar.Add("Gambia");
	            ar.Add("Georgia");
	            ar.Add("Germany");
	            ar.Add("Ghana");
	            ar.Add("Greece");
	            ar.Add("Grenada");
	            ar.Add("Guatemala");
	            ar.Add("Guinea");
	            ar.Add("Guinea-Bissau");
	            ar.Add("Guyana");
	            ar.Add("Haiti");
	            ar.Add("Honduras");
	            ar.Add("Hungary");
	            ar.Add("Iceland");
	            ar.Add("India");
	            ar.Add("Indonesia");
	            ar.Add("Iran (Islamic Republic of");
	            ar.Add("Iraq");
	            ar.Add("Ireland");
	            ar.Add("Israel");
	            ar.Add("Italy");
	            ar.Add("Jamaica");
	            ar.Add("Japan");
	            ar.Add("Jordan");
	            ar.Add("Kazakhstan");
	            ar.Add("Kenya");
	            ar.Add("Kiribati");
	            ar.Add("Kuwait");
	            ar.Add("Kyrgyzstan");
	            ar.Add("Lao Peoples Democratic Republic");
	            ar.Add("Latvia");
	            ar.Add("Lebanon");
	            ar.Add("Lesotho");
	            ar.Add("Liberia");
	            ar.Add("Libyan Arab Jamahiriya");
	            ar.Add("Lithuania");
	            ar.Add("Luxembourg");
	            ar.Add("Madagascar");
	            ar.Add("Malawi");
	            ar.Add("Malaysia");
	            ar.Add("Maldives");
	            ar.Add("Mali");
	            ar.Add("Malta");
	            ar.Add("Marshall Islands");
	            ar.Add("Mauritania");
	            ar.Add("Mauritius");
	            ar.Add("Micronesia (Federated States of");
	            ar.Add("Moldova");
	            ar.Add("Monaco");
	            ar.Add("Mongolia");
	            ar.Add("Montenegro");
	            ar.Add("Morocco");
	            ar.Add("Mozambique");
	            ar.Add("Myanmar");
	            ar.Add("Namibia");
	            ar.Add("Nauru");
	            ar.Add("Nepal");
	            ar.Add("Netherlands");
	            ar.Add("New Zealand");
	            ar.Add("Nicaragua");
	            ar.Add("Niger");
	            ar.Add("Nigeria");
	            ar.Add("Niue");
	            ar.Add("Norway");
	            ar.Add("Oman");
	            ar.Add("Pakistan");
	            ar.Add("Palau");
	            ar.Add("Panama");
	            ar.Add("Papua New Guinea");
	            ar.Add("Paraguay");
	            ar.Add("Peru");
	            ar.Add("Philippines");
	            ar.Add("Poland");
	            ar.Add("Portugal");
	            ar.Add("Qatar");
	            ar.Add("Republic of Korea");
	            ar.Add("Republic of Macedonia");
	            ar.Add("Romania");
	            ar.Add("Russian Federation");
	            ar.Add("Rwanda");
	            ar.Add("Saint Kitts and Nevis");
	            ar.Add("Saint Lucia");
	            ar.Add("Saint Vincent and the Grenadines");
	            ar.Add("Samoa");
	            ar.Add("San Marino");
	            ar.Add("Sao Tome and Principe");
	            ar.Add("Saudi Arabia");
	            ar.Add("Senegal");
	            ar.Add( "Serbia");
	            ar.Add("Seychelles");
	            ar.Add("Sierra Leone");
	            ar.Add("Singapore");
	            ar.Add("Slovakia");
	            ar.Add("Slovenia");
	            ar.Add("Solomon Islands");
	            ar.Add("Somalia");
	            ar.Add("South Africa");
	            ar.Add("Spain");
	            ar.Add("Sri Lanka");
	            ar.Add("Sudan");
	            ar.Add("Suriname");
	            ar.Add("Swaziland");
	            ar.Add("Sweden");
	            ar.Add("Switzerland");
	            ar.Add("Syrian Arab Republic");
	            ar.Add("Tajikistan");
	            ar.Add("Thailand");
	            ar.Add("Timor-Leste");
	            ar.Add("Togo");
	            ar.Add("Tonga");
	            ar.Add("Trinidad and Tobago");
	            ar.Add("Tunisia");
	            ar.Add("Turkey");
	            ar.Add("Turkmenistan");
	            ar.Add("Tuvalu");
	            ar.Add("Uganda");
	            ar.Add("Ukraine");
	            ar.Add("United Arab Emirates");
	            ar.Add("United Kingdom");
	            ar.Add("United Republic of Tanzania");
	            ar.Add("Uruguay");
	            ar.Add("Uzbekistan");
	            ar.Add("Vanuatu");
	            ar.Add("Venezuela (Bolivarian Republic of");
	            ar.Add("Viet Nam");
	            ar.Add("Yemen");
	            ar.Add("Zambia");
	            ar.Add("Zimbabwe");
                return ar;
    }

    #region populateStates

    /// <summary>
    /// Returns a 51 element, zero-based array list of 50 states plus DC
    /// </summary>
    /// <returns></returns>
    public ArrayList populateStates()
    {
        ArrayList ar = new ArrayList();
        ar.Add("AL");
	    ar.Add("AK");
	    ar.Add("AZ");
	    ar.Add("AR");
	    ar.Add("CA");
	    ar.Add("CO");
	    ar.Add("CT");
	    ar.Add("DE");
	    ar.Add("DC");
	    ar.Add("FL");
	    ar.Add("GA");
	    ar.Add("HI");
	    ar.Add("ID");
	    ar.Add("IL");
	    ar.Add("IN");
	    ar.Add("IA");
	    ar.Add("KS");
	    ar.Add("KY");
	    ar.Add("LA");
	    ar.Add("ME");
	    ar.Add("MD");
	    ar.Add("MA");
	    ar.Add("MI");
	    ar.Add("MN");
	    ar.Add("MS");
	    ar.Add("MO");
	    ar.Add("MT");
	    ar.Add("NE");
	    ar.Add("NV");
	    ar.Add("NH");
	    ar.Add("NJ");
	    ar.Add("NM");
	    ar.Add("NY");
	    ar.Add("NC");
	    ar.Add("ND");
	    ar.Add("OH");
	    ar.Add("OK");
	    ar.Add("OR");
	    ar.Add("PA");
	    ar.Add("RI");
	    ar.Add("SC");
	    ar.Add("SD");
	    ar.Add("TN");
	    ar.Add("TX");
	    ar.Add("UT");
	    ar.Add("VT");
	    ar.Add("VA");
	    ar.Add("WA");
	    ar.Add("WV");
	    ar.Add("WI");
	    ar.Add("WY");
        return ar;
    }
    #endregion
}