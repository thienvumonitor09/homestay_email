﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Homestay_Admin_MatchPreferences2 : System.Web.UI.Page
{
    Homestay hsInfo = new Homestay();
    Quarter_MetaData QMD = new Quarter_MetaData();
    private static Dictionary<string, EntityObject> stuHM;
    private static Dictionary<string, EntityObject> famHM;


    protected void Page_Load(object sender, EventArgs e)
    {
        

        //stuHM = LoadStudentHM();
        //famHM = LoadFamilyHM();
        //litResultMsg.Text = "No of Families have open room: " +  famHM.Count() +"<br/>";

        /*
        foreach (var pair in famHM)
        {
            litResultMsg.Text += " " + pair.Key + "<br/>";
            
            //litResultMsg.Text += " " + pair.Value.ToString();
            
        }
        */
        if (!IsPostBack)
        {
            /** Populate DL for students Using DataBind **/
            DataTable dtStudents = hsInfo.GetHomestayStudents("Active", 0);
            //DataTable dtStudents = hsInfo.GetHomestayStudents("Active", 0);
            dtStudents.Columns.Add("valueField", typeof(string));
            if (dtStudents != null)
            {
                if (dtStudents.Rows.Count > 0)
                {
                    foreach (DataRow dr in dtStudents.Rows)
                    {
                        string applicantIDStr = dr["applicantID"].ToString();
                        dr["valueField"] = dr["familyName"].ToString() + ", "
                                            + dr["firstName"].ToString() + " - " + Convert.ToDateTime(dr["DOB"]).ToShortDateString();
                    }
                }
            }
            ddlStudents.DataSource = dtStudents;
            ddlStudents.DataTextField = "valueField";
            ddlStudents.DataValueField = "applicantID";
            ddlStudents.DataBind();
            ddlStudents.Items.Insert(0, new ListItem("--Select a student--", "")); //Add default value
            //ddlStudents.SelectedValue = "0";
            /** End populating dropdow **/
           
        }
        //Response.Write(stuHM["2401"].Name.ToString());
        //litResultMsg.Text = stuHM["2401"].Name.ToString();
        //litResultMsg.Text = stuHM.Count() +"";
        /*
        string connectionStr = ConfigurationManager.ConnectionStrings["ConnectionString_CCSInternationalStudent"].ToString();
        SqlConnection conn = new SqlConnection(connectionStr);
        string sqlQuery = @"SELECT  [id]
                              ,[applicantID]
                          FROM[CCSInternationalStudent].[dbo].[HomestayStudentInfo]";


        DataTable students = new DataTable();
        SqlCommand cmd = new SqlCommand(sqlQuery, conn);

        try
        {
            conn.Open();

            SqlDataAdapter objDataAdapter = new SqlDataAdapter(cmd);

            objDataAdapter.Fill(students);
            ddlStudents.DataSource = students;
            ddlStudents.DataTextField = "applicantID";
            ddlStudents.DataValueField = "id";
            ddlStudents.DataBind();

        }
        catch (Exception ex)
        {
            Label1.Text += "Error RunGetQuery: " + ex.Message + "<br />SQL: " + sqlQuery + "<br />";
        }
        */

    }



    

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //Reset ddlFamilies
        ddlFamilies.SelectedIndex = -1;
        
        //Add effective quarter
        DataTable dtQuarters = QMD.GetQuarterStart(-3, 15);
        ddlEffectiveQtr1.DataSource = dtQuarters;
        ddlEffectiveQtr1.DataTextField = "textField";
        ddlEffectiveQtr1.DataValueField = "valueField";
        ddlEffectiveQtr1.DataBind();
        ddlEffectiveQtr1.Items.Insert(0, new ListItem("--Select a quarter--", "")); //Add default value

        ddlEffectiveQtr2.DataSource = dtQuarters;
        ddlEffectiveQtr2.DataTextField = "textField";
        ddlEffectiveQtr2.DataValueField = "valueField";
        ddlEffectiveQtr2.DataBind();
        ddlEffectiveQtr2.Items.Insert(0, new ListItem("--Select a quarter--", "")); //Add default value

        ddlEffectiveQtr3.DataSource = dtQuarters;
        ddlEffectiveQtr3.DataTextField = "textField";
        ddlEffectiveQtr3.DataValueField = "valueField";
        ddlEffectiveQtr3.DataBind();
        ddlEffectiveQtr3.Items.Insert(0, new ListItem("--Select a quarter--", "")); //Add default value

        ddlEffectiveQtr4.DataSource = dtQuarters;
        ddlEffectiveQtr4.DataTextField = "textField";
        ddlEffectiveQtr4.DataValueField = "valueField";
        ddlEffectiveQtr4.DataBind();
        ddlEffectiveQtr4.Items.Insert(0, new ListItem("--Select a quarter--", "")); //Add default value

        stuHM = MatchingUtility.LoadStudentHM();
        famHM = MatchingUtility.LoadFamilyHM();

        


        //litResultMsg.Text = "No of Families have open room: " + famHM.Count() + "<br/>";
        litRoomWithOtherFamily.Text = "Number of Families have open room: " +  famHM.Count() +"<br/>";
        //litResultMsg.Text += "Gender: " + rdoGender.SelectedValue + "<br/>";
        //litResultMsg.Text = "HS Preference: " + rdoGender.SelectedValue + "<br/>";
        //litResultMsg.Text += "Smoking: " + rdoSmoking.SelectedValue + "<br/>";

        int selectedStudentID = Convert.ToInt32(ddlStudents.SelectedValue);
        string rdoGenderStr = rdoGender.SelectedValue.ToString();
        string rdoHomestayStr = rdoHomestay.SelectedValue.ToString();
        string rdoSmokingStr = rdoSmoking.SelectedValue.ToString();

        litSelectedStuInfo.Text = "<strong>Selected Student's Info:</strong>" + "<br/>";
        //litResultMsg.Text += "ID: " + stuHM[selectedStudentID + ""].Name.ToString() + "<br/>";
        //litResultMsg.Text += "Smoker: " + stuHM[selectedStudentID + ""].Attributes["Smoker"][0];
        EntityObject stuObject = stuHM[selectedStudentID + ""];
        Dictionary<string, string[]> stuAttributes = stuObject.Attributes;
        string currStudentStatus = stuAttributes["homestayStatus"][0];
        if (!stuAttributes["familyID"][0].Equals("N/A"))
        {
            currStudentStatus += " in room number: " + stuAttributes["roomNumber"][0]
                                + "; home name: " + stuAttributes["homeName"][0] +"<br/>"
                                + "You must withdraw student from the room before moving the student to another room.";
            // Student Placed in room number: 2; home name: UserName, TEST

        }
        
        litCurrStudentStatus.Text = currStudentStatus;


        //litSelectedStuInfo.Text += " entityObj:" + stuObject.ToString() + "</br>";
        string stuCol = stuObject.Attributes["studyWhere"][0].ToLower();
        //litResultMsg.Text += " College:" + stuCol + "<br/>";

        

        litSelectedStuInfo.Text += "applicantID:" + stuAttributes["applicantID"][0] + "<br/>";
        litSelectedStuInfo.Text += "College:" + stuAttributes["college"][0] + "<br/>";
        litSelectedStuInfo.Text += "smoke:" + stuAttributes["smoke"][0] + "<br/>";
        litSelectedStuInfo.Text += "otherSmoke:" + stuAttributes["otherSmoke"][0] + "<br/>";
        litSelectedStuInfo.Text += "gender:" + stuAttributes["gender"][0] + "<br/>";
        litSelectedStuInfo.Text += "homestayOption:" + stuAttributes["homestayOption"][0] + "<br/>";
        litSelectedStuInfo.Text += "dietaryNeeds:" + string.Join(",", stuAttributes["dietaryNeeds"]) + "<br/>";
        litSelectedStuInfo.Text += "hobbies:" + string.Join(",", stuAttributes["hobbies"]) + "<br/>";
        litSelectedStuInfo.Text += "interaction:" + stuAttributes["interaction"][0] + "<br/>";
        litSelectedStuInfo.Text += "homeEnvironment:" + stuAttributes["homeEnvironment"][0] + "<br/>";
        litSelectedStuInfo.Text += "childrenPref:" + stuAttributes["childrenPref"][0] + "<br/>";
        

        /*
        string[] values1 = { };
        
        if (entityObject.Attributes.TryGetValue("Nonsmoker", out values1)) // if Nonsmoker not found, mean that person smoke
        {
            litResultMsg.Text += "Nonsmoker:" + values1[0] + "<br/>";
        }
        else
        {
            litResultMsg.Text += "I Smoke!" + "<br/>";
        }*/

        litResultMsg.Text = "--------- <br/>";
        //litResultMsg.Text += " " + famHM.Count();
        //litRoomWithOtherFamily.Text = selectedStudentID + " " + rdoGenderStr + " " + rdoHomestayStr+ " " + rdoSmokingStr;
        /** Populate DL for students Using DataBind **/
        //string strSQL = "SELECT id, homeName FROM [CCSInternationalStudent].[dbo].[HomestayFamilyInfo]";
        //DataTable dtFamilies = hsInfo.RunGetQuery(strSQL);

        //List of matching level
        var matchingList = new List<Dictionary<string, EntityObject>>();

        /** Filter by college **/
        Dictionary<string, EntityObject> famHMFilteredCollege = MatchingUtility.MatchStuFamCollege(stuCol, famHM);
        litResultMsg.Text += "There are " + famHMFilteredCollege.Count() + " families match with student's college: <br/>";
        litResultMsg.Text += MatchingUtility.PrintHM(famHMFilteredCollege);
        matchingList.Add(famHMFilteredCollege);

        famHM = famHMFilteredCollege;

        /** Filter by Smoking preferences **/
        if (rdoSmoking.SelectedValue.Equals("Include")  || rdoSmoking.SelectedValue.Equals(""))
        {
            Dictionary<string, EntityObject> famHMFilteredSmoking = MatchingUtility.MatchStuFamSmoking(stuObject, famHM);
            litResultMsg.Text += "<br/>There are " + famHMFilteredSmoking.Count() + " families match with student's smoking:<br/>";
            litResultMsg.Text += MatchingUtility.PrintHM(famHMFilteredSmoking);
            matchingList.Add(famHMFilteredSmoking);
        }

        /** Filter by Gender **/
        if (rdoGender.SelectedValue.Equals("Include") || rdoGender.SelectedValue.Equals(""))
        {
            Dictionary<string, EntityObject> famHMFilteredGender = MatchingUtility.MatchStuFamGender(stuObject, famHM);
            litResultMsg.Text += "<br/>There are " + famHMFilteredGender.Count() + " families match with student's gender:<br/>";
            litResultMsg.Text += MatchingUtility.PrintHM(famHMFilteredGender);
            matchingList.Add(famHMFilteredGender);
        }


        /** Filter by homestayOption **/
        if (rdoHomestay.SelectedValue.Equals("Include") || rdoHomestay.SelectedValue.Equals(""))
        {
            Dictionary<string, EntityObject> famHMFilteredHomestayOption = MatchingUtility.MatchStuFamHomestayOption(stuObject, famHM, litResultMsg.Text);
            litResultMsg.Text += "<br/>There are " + famHMFilteredHomestayOption.Count() + " families match with student's hsOption:<br/>";
            litResultMsg.Text += MatchingUtility.PrintHM(famHMFilteredHomestayOption);
            matchingList.Add(famHMFilteredHomestayOption);
        }


        /** Combine all dictionaries **/
        Dictionary<string, EntityObject> famHMFilteredFinal = MatchingUtility.FindIntersection(matchingList);
        litResultMsg.Text += "<br/>There are " + famHMFilteredFinal.Count() + " families match with student:<br/>";
        litResultMsg.Text += matchingList.Count() + "<br/>";

        //Proces final list to add score
        famHMFilteredFinal = MatchingUtility.AddScore(stuObject, famHMFilteredFinal);
        famHM = famHMFilteredFinal;

        litResultMsg.Text += MatchingUtility.PrintHM2(stuObject, famHMFilteredFinal);


        /** Populate DL for students Using DataBind **/
        DataTable dtFamilies = new DataTable();
        dtFamilies.Columns.Add("id", typeof(string));
        dtFamilies.Columns.Add("homeName", typeof(string));
        dtFamilies.Columns.Add("score", typeof(string));
        dtFamilies.Columns.Add("textField", typeof(string));
        foreach (var f in famHMFilteredFinal)
        {
            
            Dictionary<string, string[]> attributes = f.Value.Attributes;
            string formatText = String.Format("{0}, score: {1}%", attributes["homeName"][0].ToString(), attributes["score"][0]);
            dtFamilies.Rows.Add(attributes["id"][0].ToString(), attributes["homeName"][0].ToString(), attributes["score"][0], formatText);
        }

        dtFamilies.DefaultView.Sort = "score desc";
        dtFamilies = dtFamilies.DefaultView.ToTable();

        litFoundFamily.Text = "<strong>There are " + famHMFilteredFinal.Count() + " families that match above criteria found:</strong>" 
                              + "(The result is ranked in order of overall match score)";
        //litResultMsg.Text += dtFamilies.Rows.Count + " families: <br/>";

        //Match with smoking preferences

        ddlFamilies.DataSource = dtFamilies;
        ddlFamilies.DataTextField = "textField";
        ddlFamilies.DataValueField = "id";
        ddlFamilies.DataBind();
        ddlFamilies.Items.Insert(0, new ListItem("--Select a family--", "")); //Add default value
        /** End populating dropdow **/ 
    }



    protected void btnCompare_Click(object sender, EventArgs e)
    {
        DisplayPanel2();
    }

    protected void btnReset_Click(object sender, EventArgs e)
    {
        Response.Redirect("MatchPreferences2.aspx");
    }


   protected void DisplayPanel2()
    {
        //Reset dropdown list for placement
        ddlRm1Placement.SelectedIndex = -1;
        ddlRm2Placement.SelectedIndex = -1;
        ddlRm3Placement.SelectedIndex = -1;
        ddlRm4Placement.SelectedIndex = -1;

        //Reset dropdown list for placement
        ddlEffectiveQtr1.SelectedIndex = -1;
        ddlEffectiveQtr2.SelectedIndex = -1;
        ddlEffectiveQtr3.SelectedIndex = -1;
        ddlEffectiveQtr4.SelectedIndex = -1;

        ddlRm1Placement.Enabled = true;
        ddlRm2Placement.Enabled = true;
        ddlRm3Placement.Enabled = true;
        ddlRm4Placement.Enabled = true;

        //Enable effective quarter
        ddlEffectiveQtr1.Enabled = true;
        ddlEffectiveQtr2.Enabled = true;
        ddlEffectiveQtr3.Enabled = true;
        ddlEffectiveQtr4.Enabled = true;




        StringBuilder sb = new StringBuilder();
        sb.Append("<strong>Family Info</strong>:<br/>");
        string familyIDStr = ddlFamilies.SelectedValue;
        if (familyIDStr.Equals(""))
        {
            return;
        }
        EntityObject famObject = famHM[familyIDStr];
        var famAttributes = famObject.Attributes;

        if (famAttributes["room1Occupancy"][0].Contains("CCS") || famAttributes["room1Occupancy"][0].Contains("No Info"))
        {
            ddlRm1Placement.Enabled = false;
            ddlEffectiveQtr1.Enabled = false;
        }
        if (famAttributes["room2Occupancy"][0].Contains("CCS") || famAttributes["room2Occupancy"][0].Contains("No Info"))
        {
            ddlRm2Placement.Enabled = false;
            ddlEffectiveQtr2.Enabled = false;
        }
        if (famAttributes["room3Occupancy"][0].Contains("CCS") || famAttributes["room3Occupancy"][0].Contains("No Info"))
        {
            ddlRm3Placement.Enabled = false;
            ddlEffectiveQtr3.Enabled = false;
        }
        if (famAttributes["room4Occupancy"][0].Contains("CCS") || famAttributes["room4Occupancy"][0].Contains("No Info"))
        {
            ddlRm4Placement.Enabled = false;
            ddlEffectiveQtr4.Enabled = false;
        }


        sb.Append(MatchingUtility.PrintEnityObject(famObject) + "<br/><br/>");
        //PrintEnityObject(famObject);
        litFamilyInfo.Text = sb.ToString();
        string rm1Status = famAttributes["room1Occupancy"][0];
        if (famAttributes["room1"].Length > 0)
        {
            rm1Status += ", student ID: " + famAttributes["room1"][0]; //applicantID
            ddlRm1Placement.SelectedValue = famAttributes["room1"][1]; //placementType
            ddlEffectiveQtr1.SelectedValue = famAttributes["room1"][2]; //effetiveQuarter
        }

        if (famAttributes["room2"].Length > 0)
        {
            litRm2Status.Text += ", student ID: " + famAttributes["room2"][0];
            ddlRm2Placement.SelectedValue = famAttributes["room2"][1];//placementType
            ddlEffectiveQtr2.SelectedValue = famAttributes["room2"][2]; //effetiveQuarter
        }
        if (famAttributes["room3"].Length > 0)
        {
            litRm3Status.Text += ", student ID: " + famAttributes["room3"][0];
            ddlRm3Placement.SelectedValue = famAttributes["room3"][1];//placementType
            ddlEffectiveQtr3.SelectedValue = famAttributes["room3"][2]; //effetiveQuarter
        }
        if (famAttributes["room4"].Length > 0)
        {
            litRm4Status.Text += ", student ID: " + famAttributes["room4"][0];
            ddlRm4Placement.SelectedValue = famAttributes["room4"][1];//placementType
            ddlEffectiveQtr4.SelectedValue = famAttributes["room4"][2]; //effetiveQuarter
        }

        litRm1Status.Text = rm1Status;
        litRm2Status.Text = famAttributes["room2Occupancy"][0];
        litRm3Status.Text = famAttributes["room3Occupancy"][0];
        litRm4Status.Text = famAttributes["room4Occupancy"][0];

        //Disable placement & effetice quarter when student placed
        int selectedStudentID = Convert.ToInt32(ddlStudents.SelectedValue);
        EntityObject stuObject = stuHM[selectedStudentID + ""];
        Dictionary<string, string[]> stuAttributes = stuObject.Attributes;

        if (famAttributes["room1"].Length > 0)
        {
            if (famAttributes["room1"][0].Equals(selectedStudentID + ""))
            {
                ddlRm1Placement.Enabled = true;
                ddlEffectiveQtr1.Enabled = true;
            }
            else
            {
                ddlRm1Placement.Enabled = false;
                ddlEffectiveQtr1.Enabled = false;
            }

                /*
                if (famAttributes["room1"][0].Equals(selectedStudentID + ""))
                {
                    ddlRm2Placement.Enabled = false;
                    ddlRm3Placement.Enabled = false;
                    ddlRm4Placement.Enabled = false;

                    ddlEffectiveQtr2.Enabled = false;
                    ddlEffectiveQtr3.Enabled = false;
                    ddlEffectiveQtr4.Enabled = false;
                }
                else
                {
                    
                    ddlRm2Placement.Enabled = false;
                    ddlRm3Placement.Enabled = false;
                    ddlRm4Placement.Enabled = false;

                    ddlEffectiveQtr2.Enabled = false;
                    ddlEffectiveQtr3.Enabled = false;
                    ddlEffectiveQtr4.Enabled = false;
                }*/

            }
        /*
        if (famAttributes["room2"].Length > 0 && famAttributes["room2"][0].Equals(selectedStudentID + ""))
        {
            ddlRm1Placement.Enabled = false;
            ddlRm3Placement.Enabled = false;
            ddlRm4Placement.Enabled = false;

            ddlEffectiveQtr1.Enabled = false;
            ddlEffectiveQtr3.Enabled = false;
            ddlEffectiveQtr4.Enabled = false;
        }

        if (famAttributes["room3"].Length > 0 && famAttributes["room3"][0].Equals(selectedStudentID + ""))
        {
            ddlRm1Placement.Enabled = false;
            ddlRm2Placement.Enabled = false;
            ddlRm4Placement.Enabled = false;

            ddlEffectiveQtr1.Enabled = false;
            ddlEffectiveQtr2.Enabled = false;
            ddlEffectiveQtr4.Enabled = false;
        }

        if (famAttributes["room4"].Length > 0 && famAttributes["room4"][0].Equals(selectedStudentID + ""))
        {
            ddlRm1Placement.Enabled = false;
            ddlRm2Placement.Enabled = false;
            ddlRm3Placement.Enabled = false;

            ddlEffectiveQtr1.Enabled = false;
            ddlEffectiveQtr2.Enabled = false;
            ddlEffectiveQtr3.Enabled = false;
        }
        */
        //If student has been placed, disabled all unrelated familes. To change home/room, must withdraw that student first
        if (!stuAttributes["familyID"][0].Equals("N/A") && !stuAttributes["familyID"][0].Equals(familyIDStr))
        {
            //Disable placement DDL
            ddlRm1Placement.Enabled = false;
            ddlRm2Placement.Enabled = false;
            ddlRm3Placement.Enabled = false;
            ddlRm4Placement.Enabled = false;

            //Disable effective quarter
            ddlEffectiveQtr1.Enabled = false;
            ddlEffectiveQtr2.Enabled = false;
            ddlEffectiveQtr3.Enabled = false;
            ddlEffectiveQtr4.Enabled = false;
        }
    }

    protected void btnPlace_Click(object sender, EventArgs e)
    {
        /*
         * To change placementType of a student, there are 3 steps: 
         *  1. Insert into HomestayPlacement tbl
         *  2. Change student statuss
         *  3. Change family status
         */
        //iResult = hsInfo1.InsertUpdateRoomPlacement(Convert.ToInt32(hdnStuID.Value), Convert.ToInt32(hdnFamID.Value), 1, "insert", hdnRm1Placement.Value, " ", hdnHomeName.Value, txtRm1Date.Text);
        int selectedStudentID = Convert.ToInt32(ddlStudents.SelectedValue);
        EntityObject stuObject = stuHM[selectedStudentID + ""];
        Dictionary<string, string[]> stuAttributes = stuObject.Attributes;
        //stuAttributes["familyID"] 
        //string homeName = attributes["homeName"];
        // attributes["familyID"] = new string[] { dtStudentPlacement.Rows[0]["familyID"].ToString() };
        //attributes["homeName"] = new string[] { dtStudentPlacement.Rows[0]["placementName"].ToString() };
        //attributes["roomNumber"] = new string[] { dtStudentPlacement.Rows[0]["roomNumber"].ToString() };
        int selectedFamilyID = Convert.ToInt32(ddlFamilies.SelectedValue);
        EntityObject famObject = famHM[selectedFamilyID + ""];
        Dictionary<string, string[]> famAttributes = famObject.Attributes;
        //check if student place or not

        //Process room1
        string selectedPlacement1 = ddlRm1Placement.SelectedValue;
        string applicantIDStr = ddlStudents.SelectedValue;
        string familyIDStr = ddlFamilies.SelectedValue;

        if (selectedPlacement1.Contains("Placed") || selectedPlacement1.Contains("Tentative"))
        {
            
           
            string roomNumberStr = "1";
            string homeName = famAttributes["homeName"][0];
            //Insert or Update to [HomestayPlacement]
            string effectiveQtr1 = ddlEffectiveQtr1.SelectedValue;
            if (famAttributes["room1"].Length == 0) //Insert new
            {
                litPlace.Text = "insert";
                
                string sqlInsert = @"INSERT INTO [dbo].[HomestayPlacement]
                                           (
                                            [placementName]
                                           ,[applicantID]
                                           ,[familyID]
                                            ,[roomNumber]
                                            ,[placementType]
                                            ,[effectiveQuarter]
                                           )
                                     VALUES( '" + homeName + "'"
                                            + ", " + applicantIDStr 
                                            + ", " + familyIDStr 
                                            + ", " + roomNumberStr
                                            + ", '" + selectedPlacement1 + "'"
                                            + ", '" + effectiveQtr1 + "'"
                                            + ")";
                hsInfo.runSPQuery(sqlInsert, true);
                litPlace.Text = "New record has been inserted!";
               
                famAttributes["room1"] = new string[]
                                        {
                                            applicantIDStr
                                            ,selectedPlacement1
                                            ,effectiveQtr1
                                        };
                                        
            
            }
            else //Update existing
            {
                //Update
                string sqlUpdate = @"UPDATE [CCSInternationalStudent].[dbo].[HomestayPlacement]
                                SET effectiveQuarter = '" + effectiveQtr1 + "'  ,placementType= '" + selectedPlacement1 + "'"
                                    + " WHERE applicantID = " + applicantIDStr + " AND familyID =" + familyIDStr;
                hsInfo.runSPQuery(sqlUpdate, false);
                //update famHM 

                litPlace.Text = "Student has been updated!";

                famAttributes["room1"][1] = selectedPlacement1;
                famAttributes["room1"][2] = effectiveQtr1;
            }



           
            stuAttributes["familyID"][0] = familyIDStr; 
            stuAttributes["homeName"][0] = homeName;  
            stuAttributes["roomNumber"][0] = roomNumberStr;  
           

        }
        else
        {
            
            string sqlUpdate = @"DELETE FROM [CCSInternationalStudent].[dbo].[HomestayPlacement]"
                                + " WHERE applicantID = " + applicantIDStr + " AND familyID =" + familyIDStr;
            hsInfo.runSPQuery(sqlUpdate, false);
            //update famHM 
            famAttributes["room1"] = new string[] {  };

            //update stuHM
            stuAttributes["familyID"] = new string[] { "N/A" };
            stuAttributes["homeName"] = new string[] { "N/A" };
            stuAttributes["roomNumber"] = new string[] { "N/A" };

            //update display
            ddlRm1Placement.SelectedIndex = 0;
            ddlEffectiveQtr1.SelectedIndex = 0;
            litPlace.Text = "Student has been removed!";

            //[HomestayPlacement] tbl
        }

        //2. Change student status
        //Update
        if (!selectedPlacement1.Contains("Select"))
        {
            string sqlUpdateStudentStatus = @"UPDATE [CCSInternationalStudent].[dbo].[HomestayStudentInfo]
                                SET homestayStatus = '" + selectedPlacement1 + "'"
                            + " WHERE applicantID = " + applicantIDStr;
            hsInfo.runSPQuery(sqlUpdateStudentStatus, false);
        }
        

        //3. Change family status: "Open Room(s)" or "Rooms Full"
        int countFull = 0; 
        for (var i = 1; i <= 4; i++)
        {
            string roomStr = "room" + i;
            string roomOccupancyStr = "room" + i + "Occupancy";
            if (famAttributes[roomStr].Length > 0 
                || (famAttributes[roomOccupancyStr][0].Contains("CCS") || famAttributes[roomOccupancyStr][0].Contains("No Info")))
            {
                countFull++;
            }  
        }
        if (countFull == 4)
        {
            litPlace.Text += "Rooms Full!";
        }
        else
        {
            litPlace.Text += "Open Room(s)!";
        }


        //string homeName = famAttributes["homeName"][0];

        //string roomNumber = 
        //litPlace.Text = selectedStudentID.ToString() + " " + selectedFamilyID.ToString() + " " + homeName;


        /*
          attributes[roomNoStr] = new string[] 
                                        {
                                            rowPlacement["applicantID"].ToString().Trim()
                                            ,rowPlacement["placementType"].ToString().Trim()
                                            ,rowPlacement["effectiveQuarter"].ToString().Trim()
                                        };
         */

        /*
          if (dtStudentPlacement != null && dtStudentPlacement.Rows.Count > 0)
            {
                //attributes["familyPlaced"] = new string[] { dtStudentPlacement.Rows[0]["familyID"].ToString() };
                attributes["familyID"] = new string[] { dtStudentPlacement.Rows[0]["familyID"].ToString() };
                attributes["homeName"] = new string[] { dtStudentPlacement.Rows[0]["placementName"].ToString() };
                attributes["roomNumber"] = new string[] { dtStudentPlacement.Rows[0]["roomNumber"].ToString() };
            }
         */
        DisplayPanel2();
    }
    protected void btnUpdateRm1_Click(object sender, EventArgs e)
    {
        /*
         * To change placementType of a student, there are 3 steps: 
         *  1. Insert into HomestayPlacement tbl
         *  2. Change student statuss
         *  3. Change family status
         */
        //iResult = hsInfo1.InsertUpdateRoomPlacement(Convert.ToInt32(hdnStuID.Value), Convert.ToInt32(hdnFamID.Value), 1, "insert", hdnRm1Placement.Value, " ", hdnHomeName.Value, txtRm1Date.Text);
        int selectedStudentID = Convert.ToInt32(ddlStudents.SelectedValue);
        EntityObject stuObject = stuHM[selectedStudentID + ""];
        Dictionary<string, string[]> stuAttributes = stuObject.Attributes;
        //stuAttributes["familyID"] 
        //string homeName = attributes["homeName"];
        // attributes["familyID"] = new string[] { dtStudentPlacement.Rows[0]["familyID"].ToString() };
        //attributes["homeName"] = new string[] { dtStudentPlacement.Rows[0]["placementName"].ToString() };
        //attributes["roomNumber"] = new string[] { dtStudentPlacement.Rows[0]["roomNumber"].ToString() };
        int selectedFamilyID = Convert.ToInt32(ddlFamilies.SelectedValue);
        EntityObject famObject = famHM[selectedFamilyID + ""];
        Dictionary<string, string[]> famAttributes = famObject.Attributes;
        //check if student place or not

        //Process room1
        string selectedPlacement1 = ddlRm1Placement.SelectedValue;
        string applicantIDStr = ddlStudents.SelectedValue;
        string familyIDStr = ddlFamilies.SelectedValue;

        if (selectedPlacement1.Contains("Placed") || selectedPlacement1.Contains("Tentative"))
        {
            string roomNumberStr = "1";
            string homeName = famAttributes["homeName"][0];
            //Insert or Update to [HomestayPlacement]
            string effectiveQtr1 = ddlEffectiveQtr1.SelectedValue;
            if (famAttributes["room1"].Length == 0) //Insert new
            {
                litPlace.Text = "insert";

                string sqlInsert = @"INSERT INTO [dbo].[HomestayPlacement]
                                           (
                                            [placementName]
                                           ,[applicantID]
                                           ,[familyID]
                                            ,[roomNumber]
                                            ,[placementType]
                                            ,[effectiveQuarter]
                                           )
                                     VALUES( '" + homeName + "'"
                                            + ", " + applicantIDStr
                                            + ", " + familyIDStr
                                            + ", " + roomNumberStr
                                            + ", '" + selectedPlacement1 + "'"
                                            + ", '" + effectiveQtr1 + "'"
                                            + ")";
                hsInfo.runSPQuery(sqlInsert, true);
                litPlace.Text = "New record has been inserted!";
              
                famAttributes["room1"] = new string[]
                                       {
                                            applicantIDStr
                                            ,selectedPlacement1
                                            ,effectiveQtr1
                                       };

            }
            else //Update existing
            {
                //Update
                string sqlUpdate = @"UPDATE [CCSInternationalStudent].[dbo].[HomestayPlacement]
                                SET effectiveQuarter = '" + effectiveQtr1 + "'  ,placementType= '" + selectedPlacement1 + "'"
                                    + " WHERE applicantID = " + applicantIDStr + " AND familyID =" + familyIDStr;
                hsInfo.runSPQuery(sqlUpdate, false);
                //update famHM 

                litPlace.Text = "Student has been updated!";
                famAttributes["room1"][1] = selectedPlacement1;
                famAttributes["room1"][2] = effectiveQtr1;
            }

           


            stuAttributes["familyID"] = new string[] { familyIDStr };
            stuAttributes["homeName"] = new string[] { homeName };
            stuAttributes["roomNumber"] = new string[] { roomNumberStr };
            

        }
        else
        {

            string sqlUpdate = @"DELETE FROM [CCSInternationalStudent].[dbo].[HomestayPlacement]"
                                + " WHERE applicantID = " + applicantIDStr + " AND familyID =" + familyIDStr;
            hsInfo.runSPQuery(sqlUpdate, false);
            //update famHM 
            famAttributes["room1"] = new string[] { };

            //update stuHM
            stuAttributes["familyID"] = new string[] { "N/A" };
            stuAttributes["homeName"] = new string[] { "N/A" };
            stuAttributes["roomNumber"] = new string[] { "N/A" };

            //update display
            ddlRm1Placement.SelectedIndex = 0;
            ddlEffectiveQtr1.SelectedIndex = 0;
            litPlace.Text = "Student has been removed!";

            //[HomestayPlacement] tbl
        }

        //2. Change student status
        //Update
        if (!selectedPlacement1.Contains("Select"))
        {
            string sqlUpdateStudentStatus = @"UPDATE [CCSInternationalStudent].[dbo].[HomestayStudentInfo]
                                SET homestayStatus = '" + selectedPlacement1 + "'"
                            + " WHERE applicantID = " + applicantIDStr;
            hsInfo.runSPQuery(sqlUpdateStudentStatus, false);
        }


        //3. Change family status: "Open Room(s)" or "Rooms Full"
        int countFull = 0;
        for (var i = 1; i <= 4; i++)
        {
            string roomStr = "room" + i;
            string roomOccupancyStr = "room" + i + "Occupancy";
            if (famAttributes[roomStr].Length > 0
                || (famAttributes[roomOccupancyStr][0].Contains("CCS") || famAttributes[roomOccupancyStr][0].Contains("No Info")))
            {
                countFull++;
            }
        }
        if (countFull == 4)
        {
            litPlace.Text += "Rooms Full!";
        }
        else
        {
            litPlace.Text += "Open Room(s)!";
        }

        
        DisplayPanel2();
    }
    protected void btnUpdateRm2_Click(object sender, EventArgs e)
    {
        /*
         * To change placementType of a student, there are 3 steps: 
         *  1. Insert into HomestayPlacement tbl
         *  2. Change student statuss
         *  3. Change family status
         */
        int selectedStudentID = Convert.ToInt32(ddlStudents.SelectedValue);
        EntityObject stuObject = stuHM[selectedStudentID + ""];
        Dictionary<string, string[]> stuAttributes = stuObject.Attributes;
       
        int selectedFamilyID = Convert.ToInt32(ddlFamilies.SelectedValue);
        EntityObject famObject = famHM[selectedFamilyID + ""];
        Dictionary<string, string[]> famAttributes = famObject.Attributes;
        //check if student place or not

        //Process room2
        string selectedPlacement1 = ddlRm2Placement.SelectedValue;
        string applicantIDStr = ddlStudents.SelectedValue;
        string familyIDStr = ddlFamilies.SelectedValue;

        if (selectedPlacement1.Contains("Placed") || selectedPlacement1.Contains("Tentative"))
        {
            string roomNumberStr = "2";
            string homeName = famAttributes["homeName"][0];
            //Insert or Update to [HomestayPlacement]
            string effectiveQtr1 = ddlEffectiveQtr2.SelectedValue;
            if (famAttributes["room2"].Length == 0) //Insert new
            {
                litPlace.Text = "insert";

                string sqlInsert = @"INSERT INTO [dbo].[HomestayPlacement]
                                           (
                                            [placementName]
                                           ,[applicantID]
                                           ,[familyID]
                                            ,[roomNumber]
                                            ,[placementType]
                                            ,[effectiveQuarter]
                                           )
                                     VALUES( '" + homeName + "'"
                                            + ", " + applicantIDStr
                                            + ", " + familyIDStr
                                            + ", " + roomNumberStr
                                            + ", '" + selectedPlacement1 + "'"
                                            + ", '" + effectiveQtr1 + "'"
                                            + ")";
                hsInfo.runSPQuery(sqlInsert, true);
                litPlace.Text = "New record has been inserted!";

                famAttributes["room2"] = new string[]
                                       {
                                            applicantIDStr
                                            ,selectedPlacement1
                                            ,effectiveQtr1
                                       };

            }
            else //Update existing
            {
                //Update
                string sqlUpdate = @"UPDATE [CCSInternationalStudent].[dbo].[HomestayPlacement]
                                SET effectiveQuarter = '" + effectiveQtr1 + "'  ,placementType= '" + selectedPlacement1 + "'"
                                    + " WHERE applicantID = " + applicantIDStr + " AND familyID =" + familyIDStr;
                hsInfo.runSPQuery(sqlUpdate, false);
                //update famHM 

                litPlace.Text = "Student has been updated!";
                famAttributes["room2"][1] = selectedPlacement1;
                famAttributes["room2"][2] = effectiveQtr1;
            }




            stuAttributes["familyID"] = new string[] { familyIDStr };
            stuAttributes["homeName"] = new string[] { homeName };
            stuAttributes["roomNumber"] = new string[] { roomNumberStr };


        }
        else
        {

            string sqlUpdate = @"DELETE FROM [CCSInternationalStudent].[dbo].[HomestayPlacement]"
                                + " WHERE applicantID = " + applicantIDStr + " AND familyID =" + familyIDStr;
            hsInfo.runSPQuery(sqlUpdate, false);
            //update famHM 
            famAttributes["room2"] = new string[] { };

            //update stuHM
            stuAttributes["familyID"] = new string[] { "N/A" };
            stuAttributes["homeName"] = new string[] { "N/A" };
            stuAttributes["roomNumber"] = new string[] { "N/A" };

            //update display
            ddlRm1Placement.SelectedIndex = 0;
            ddlEffectiveQtr1.SelectedIndex = 0;
            litPlace.Text = "Student has been removed!";

            //[HomestayPlacement] tbl
        }

        //2. Change student status
        //Update
        if (!selectedPlacement1.Contains("Select"))
        {
            string sqlUpdateStudentStatus = @"UPDATE [CCSInternationalStudent].[dbo].[HomestayStudentInfo]
                                SET homestayStatus = '" + selectedPlacement1 + "'"
                            + " WHERE applicantID = " + applicantIDStr;
            hsInfo.runSPQuery(sqlUpdateStudentStatus, false);
        }


        //3. Change family status: "Open Room(s)" or "Rooms Full"
        int countFull = 0;
        for (var i = 1; i <= 4; i++)
        {
            string roomStr = "room" + i;
            string roomOccupancyStr = "room" + i + "Occupancy";
            if (famAttributes[roomStr].Length > 0
                || (famAttributes[roomOccupancyStr][0].Contains("CCS") || famAttributes[roomOccupancyStr][0].Contains("No Info")))
            {
                countFull++;
            }
        }
        if (countFull == 4)
        {
            litPlace.Text += "Rooms Full!";
        }
        else
        {
            litPlace.Text += "Open Room(s)!";
        }


        DisplayPanel2();
    }

}