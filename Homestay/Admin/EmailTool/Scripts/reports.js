﻿
        var app = angular.module('myApp', ['ngSanitize', 'ngCkeditor', 'ngRoute']);
        app.config(function ($routeProvider) {
            $routeProvider
                .when("/", {
                    templateUrl: "reports.html",
                    controller: 'emailReportsCtrl'
                })
                .when("/individualReport/:idex", {
                    templateUrl: "individualReport.html",
                    controller: 'getIndividualReportCtrl'
                })
                .when("/tomato", {
                    template: "<h1>Tomato</h1><p>Tomatoes contain around 95% water.</p>"
                });
        });
        app.controller("getIndividualReportCtrl", function ($scope, $http, $routeParams) {
            
            console.log($routeParams.idex);
            $scope.individualReport = {};
            $scope.getIndividualEmailReport = function () {
                $http({
                    method: "POST",
                    url: "WebService.asmx/GetIndividualEmailReport",
                    dataType: 'json',
                    data: { reportID: $routeParams.idex },
                    headers: { "Content-Type": "application/json" }
                }).then(function (response) {
                    //alert(response.data.d);
                    $scope.individualReport = JSON.parse(response.data.d);
                }, function (error) {
                    alert("error");
                });
            };

            $scope.getIndividualEmailReport();

        });
        app.controller('emailReportsCtrl', function ($scope, $http, $window, $routeParams) {
            $scope.editorOptions = {
                language: 'en',
                allowedContent: true,
                entities: false,
                uiColor: '#D3D3D3'
            };
            $scope.demo = "here";
            $scope.emailReports = [];
            $scope.getEmailReports = function () {
                $http({
                    method: "POST",
                    url: "WebService.asmx/GetEmailReports",
                    dataType: 'json',
                    data: {},
                    headers: { "Content-Type": "application/json" }
                }).then(function (response) {
                    //alert(response.data);
                    //console.log(response.data.d);
                    $scope.emailReports = JSON.parse(response.data.d);
                    
                    angular.forEach($scope.emailReports, function (value, key) {
                        var toEmailStr = value.toEmail;
                        value.countTo = toEmailStr.split(",").length;
                        var ccEmailStr = value.ccEmail;
                        value.countCC = ccEmailStr.split(",").length;
                    });
                    console.log($scope.emailReports);
                    //alert(response.data.d);
                }, function (error) {
                    alert("error");
                });
            };

            $scope.getEmailReports();
           
        });

