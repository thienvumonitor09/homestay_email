﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

/// <summary>
/// Summary description for HomestayEmailUtility
/// </summary>
public class HomestayEmailUtility
{
    private static readonly string strConnectionLocal = ConfigurationManager.ConnectionStrings["ConnectionString_CCSInternationalStudent"].ToString();

    public HomestayEmailUtility()
    {

    }
    public string getDemo()
    {
        List<Person> persons = new List<Person>();
        DataSet ds = new DataSet();
        SqlConnection conn = new SqlConnection(strConnectionLocal);
        conn.Open();
        string query = "";
        string groupName = "Individual Students";
        switch (groupName)
        {
            case "Individual Students":
                query = @"WITH temp AS(
	                        SELECT applicantID, email
	                        ,ROW_NUMBER() OVER (
                                             PARTITION BY applicantID 
                                             ORDER BY applicantID DESC
         	                           ) AS [ROW NUMBER]
	                        FROM [CCSInternationalStudent].[dbo].[ContactInformation] 	
                        ),
                        temp2 AS(
	                        SELECT applicantID, email FROM temp
	                        WHERE temp.[ROW NUMBER] = 1
                        )

                        SELECT fullName, email
                        FROM temp2 
                        INNER JOIN ApplicantBasicInfo
                        ON applicantID = id
                        ORDER BY fullName ASC";
                break;
            case "Individual Families":
                query = @"SELECT applicantId, (firstname+ ' ' + familyName) as fullName, email, relationship
                          FROM [CCSInternationalStudent].[dbo].[HomestayRelatives]
                          WHERE relationship like '%Primary Contact%'
                          ORDER BY fullName ASC";
                break;
            default:
                persons.Add(new Person("Vu Nguyen", "vu.nguyen@ccs.spokane.edu"));
                //emails.Add("laura.padden@ccs.spokane.edu");
                //Console.WriteLine("Default case");
                break;
        }


        SqlCommand cmd = new SqlCommand(query, conn);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(ds);
        if (ds != null && ds.Tables.Count > 0)
        {
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                persons.Add(new Person(dr["fullName"].ToString(), dr["email"].ToString()));
            }
        }
        conn.Close();
        JavaScriptSerializer js = new JavaScriptSerializer();
        js.MaxJsonLength = Int32.MaxValue;
        //return js.Serialize(templates);

        //emails.Add("thienvumonitor09@gmail.com");
        //emails.Add("laura.padden@ccs.spokane.edu");
        //EmailGroup emailGroup = new EmailGroup(groupName, emails);
        //JavaScriptSerializer js = new JavaScriptSerializer();
        return js.Serialize(persons);
        ////return "ok";
    }
    public class Person
    {
        public string name;
        public string email;


        public Person() { }
        public Person(string name, string email)
        {
            this.name = name;
            this.email = email;
        }
    }
}