﻿<%@ Page Title="Placement Management" EnableEventValidation="false"  Language="C#" MasterPageFile="~/CCSApps.master" AutoEventWireup="true" CodeFile="EditPlacement.aspx.cs" Inherits="Homestay_Admin_Matching_EditPlacement" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    
 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bannerH1Text" Runat="Server">
    Placement Management
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">
   
     <asp:Button ID="btnReset" runat="server" Text="Reset" OnClick="btnReset_Click"  />
      <asp:Button ID="btnMainPage" runat="server" Text="Return to Admin Page" OnClick="btnMainPage_Click" />
      <asp:Button ID="btnPlacement" runat="server" Text="Go to Placement Page" OnClick="btnPlacementPage_Click" />
    <br />
    <asp:Button ID="btnShowAll" runat="server" Text="Show All Records" OnClick="btnShowAll_Click" />
    <br /><br />
      <asp:UpdatePanel ID="UpdatePanel1" runat="server"  >
          <ContentTemplate>
              <div>
                  <asp:Literal id="litSuccessMsg" runat="server" />
            <asp:GridView ID="gvStuFam" runat="server" AutoGenerateColumns="False" CellPadding="6" 
                    OnRowCancelingEdit="gvStuFam_RowCancelingEdit"   
                    OnRowDatabound="gvStuFam_RowDataBound"
                    OnRowEditing="gvStuFam_RowEditing" 
                    OnRowUpdating="gvStuFam_RowUpdating" >  
                    <Columns>  
                <asp:TemplateField>  
                    <ItemTemplate>  
                        <asp:Button ID="btn_Edit" runat="server" Text="Edit" CommandName="Edit" />  
                    </ItemTemplate>  
                    <EditItemTemplate>  
                        <asp:Button ID="btn_Update" runat="server" Text="Update" CommandName="Update" />  
                        <asp:Button ID="btn_Cancel" runat="server" Text="Cancel" CommandName="Cancel"/>  
                    </EditItemTemplate>  
                </asp:TemplateField>  
                <asp:TemplateField HeaderText="id">
                    <ItemTemplate>
                        <asp:Label ID="lbl_id" runat="server"  Text='<%#Eval("id") %>'></asp:Label>
                        
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField HeaderText="Student">  
                    <ItemTemplate>  
                        <asp:Label ID="lblStudentName" runat="server" Text='<%#Eval("StudentName") %>'></asp:Label>  
                    </ItemTemplate>  
                </asp:TemplateField>  
                <asp:TemplateField HeaderText="Student ID">  
                    <ItemTemplate>  
                        <asp:Label ID="lblApplicantID" runat="server" Text='<%#Eval("applicantID") %>'></asp:Label>  
                    </ItemTemplate>  
                </asp:TemplateField> 
                <asp:TemplateField HeaderText="Family">  
                    <ItemTemplate>  
                        <asp:Label ID="lblPlacementName" runat="server" Text='<%#Eval("placementName") %>'></asp:Label>  
                    </ItemTemplate>  
                </asp:TemplateField>  
                <asp:TemplateField HeaderText="Family ID">  
                    <ItemTemplate>  
                        <asp:Label ID="lblFamilyID" runat="server" Text='<%#Eval("familyID") %>'></asp:Label>  
                    </ItemTemplate>  
                </asp:TemplateField> 
                <asp:TemplateField HeaderText="Room Number">  
                    <ItemTemplate>  
                        <asp:Label ID="lbl_roomNumber" runat="server" Text='<%#Eval("roomNumber") %>'></asp:Label>  
                    </ItemTemplate>  
                    
                </asp:TemplateField>  
                <asp:TemplateField HeaderText="Effective Date">  
                    <ItemTemplate>  
                        <asp:Label ID="lbl_effectiveQuarter" runat="server" Text='<%#Eval("effectiveQuarter") %>'></asp:Label>  
                    </ItemTemplate>  
                  
                </asp:TemplateField>  
                <asp:TemplateField HeaderText="End Date">  
                    <ItemTemplate>  
                        <asp:Label ID="lbl_endQuarter" runat="server" Text='<%#Eval("endQuarter") %>'></asp:Label>  
                    </ItemTemplate>  
                   
                </asp:TemplateField>  
                <asp:TemplateField HeaderText="Placement Type">  
                    <ItemTemplate>  
                        <asp:Label ID="lbl_placementType" runat="server" Text='<%#Eval("placementType") %>'></asp:Label>  
                    </ItemTemplate>  
                    <EditItemTemplate>
			            <asp:DropDownList ID="ddlPlacementType" runat="server">
                            <asp:ListItem Value="">--Select a placement type--</asp:ListItem>
                            <asp:ListItem Value="Tentative Placement"></asp:ListItem>
                            <asp:ListItem Value="Student Placed"></asp:ListItem>
                            <asp:ListItem Value="Withdrew"></asp:ListItem>
                            <asp:ListItem Value="Graduated"></asp:ListItem>
                            <asp:ListItem Value="Student Moved Out"></asp:ListItem>
			            </asp:DropDownList>
		            </EditItemTemplate>  
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Action">  
                    <ItemTemplate>  
                        <asp:LinkButton ID="linkBtnStudent" runat="server" Text="View Student form"  OnClientClick='<%# "GotoStudent(" + Eval("applicantID") + ", " + Eval("familyID") + ");" %>'> </asp:LinkButton>
                        <asp:LinkButton ID="LinkBtnFamily" runat="server" Text="View Family form" OnClientClick='<%# "GotoFamily(" +Eval("familyID") + ", " + Eval("applicantID")+ ", " + Eval("id") +");" %>'></asp:LinkButton>
                        
                        <!--<asp:LinkButton ID="LinkButton1" runat="server" Text="Send to Family" OnClientClick='<%# "OpenMailWindow(" +Eval("familyID") + " );" %>'></asp:LinkButton>
                        <asp:LinkButton ID="LinkButton2" runat="server" OnClick="SendToFamily_Click" Text="server"></asp:LinkButton> -->
                    </ItemTemplate>  
                   
                </asp:TemplateField>  
            </Columns>  
                    <HeaderStyle BackColor="#663300" ForeColor="#ffffff"/>  
                    <RowStyle BackColor="#e7ceb6"/>  
                </asp:GridView>  
        </div>
          </ContentTemplate>
      </asp:UpdatePanel>
        
    <script type="text/javascript">
        function GotoStudent(id, familyID)
        {
            javascript:window.open("/Homestay/Admin/Reports/PlacementForm_V4.aspx?studentID=" + id + "&familyID=" + familyID);
        }   
        function GotoFamily(id, studentID, placementID)
        {
            javascript: window.open("/Homestay/Admin/Reports/PlacementForm_Student2.aspx?familyID=" + id + "&studentID=" + studentID + "&placementID=" + placementID);
        } 
        function OpenMailWindow(familyID)
        {
            
            var toEmail = familyID;
            var fromEmail = "from@gmail.com"
            var bodyTemplate = "bodyTemplate";
            var subject = "subject";
            window.open("mailto:" +toEmail +"?cc="+ fromEmail +"&bcc=user@example.com&subject=" + subject + "&body=" + bodyTemplate);
            //return false;
        }
      
    </script>
    </asp:Content>




