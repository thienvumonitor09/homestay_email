﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ApplicationForm_L.aspx.cs" Inherits="Homestay_ApplicationForm_L" Debug="true" %>
<%@ PreviousPageType VirtualPath="PlacementProfile_L.aspx" %> 
<!DOCTYPE html>

<html lang="en-us" >

<head id="Head1" runat="server">
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <title>CCS Homestay Program</title>
    <link id="Link1" href="/_css/ccs.css" rel="stylesheet"  type="text/css" />
    <link href="/Homestay/_css/Homestay.css" rel="stylesheet" type="text/css" />  
    <link href="/_css/Forms.css" rel="stylesheet" type="text/css" />         
    <meta http-equiv="X-UA-Compatible" content="IE=9" />    
    <script type="text/javascript" src="/_js/jquery-1.9.1.min.js"></script>    
    <script type="text/javascript" src="/_js/pushToHttps.js"></script>    
    <script src="/_Upload/iAcceptableFilesOS.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(function () {
            
        });
        function ValidateFileName() {
            var fileName = $('#<%= uplLetterFile.ClientID %>').val();
            var asciiRegExp = /[^ -~]+/;
           
            //console.log(asciiRegExp.test(fileName));
            //$(txtBox).focus();
            if (asciiRegExp.test(fileName)) {
                alert("You have selected a file containing invalid characters. Please rename the file with only english-based characters!");
                return false;
            } 
            //alert(fileName);
            return true;
        }
      function checkFiles() {
        //alert("HELLO WORLD!");
        var blCheckOK = false
        if (document.forms[1].txtLetterFile.value != "") {
          blCheckOK = checkAcceptableFiles(document.forms[1].FileUpload1.value, true)
          if (blCheckOK) {
              if (document.forms[1].txtPhotoFile.value != "") {
                  blCheckOK = checkAcceptableFiles(document.forms[1].txtPhotoFile.value, true)
                  if (blCheckOK) {
                      return true;
                  } else {
                      return false;
                  }
              } 
          } else {
              return false;
          }
        }
        return true;
      }

      function validateRequired() {
          var oForm = document.forms['form1'];
          var arriveValue = oForm.elements['txtArrivalDate'].value;
          if (arriveValue == "") {
              alert("Please enter your estimated arrival date.");
              return false;
          }
      }
    </script>

</head>

<body>
<form id="form1" runat="server">
<h2>Application Form - Students</h2>
    <div id="divErr" runat="server" Visible="false">
    <fieldset id="generalError">
        <legend></legend>
        <p><asp:Literal ID="litErr" runat="server"></asp:Literal></p>
    </fieldset>
    </div>
<div id="divAdmin" runat="server">
    <fieldset id="fldAdmin">
    <legend>Administration Options</legend>
        <ul>
            <li>Select a Homestay student application from the drop-down list to edit.</li>
            <li>Or select <b>Add New Student</b> and use the <b>Personal Information</b> fields below to search for an International Student.</li>
        </ul><br />
        <div>
            <div id="divResetFields" runat="server" style="float:left;margin-right:30px;">
                <p>
                    <!-- <input type="submit" name="btnSubmit" value="Reset Fields" /> -->
                    <asp:Button ID="btnCmdResetFields" runat="server" Text="Reset Fields" OnClick="btnCmdResetFields_Click" />
                </p>
            </div>
            <div id="divHome" runat="server" style="float:left;">
                <p>
                  <!--  <input type="submit" name="btnSubmit" id="btnHome" value="Return to Admin Home Page"/>&nbsp;&nbsp; -->
                    <asp:Button ID="btnCmdReturnAdminHome" runat="server" Text="Return to Admin Home Page" OnClick="btnCmdReturnAdminHome_Click" />&nbsp&nbsp;
                   <!-- <asp:Button ID="btnGenerate" runat="server" Text="Export to PDF" OnClick="btnExport_Click" Visible="true"/> -->
                    <asp:Button ID="Button2" runat="server" Text="Export to PDF" OnClick="btnExport2_Click" Visible="true"/> 
                </p>
            </div>
        </div>
        <div style="clear:both;"><p>&nbsp;</p></div>
        <asp:DropDownList ID="ddlStudents" runat="server" AutoPostBack="true"></asp:DropDownList>
        <div id="divAdminInfo" runat="server">
            <p>
                <label for="txtDateApplied">Application Date</label><br />
                <asp:TextBox ID="txtDateApplied" Enabled="false" runat="server"></asp:TextBox>
            </p>
            <p>
                <label for="txtSID">ctcLink ID</label><br />
                <asp:TextBox ID="txtSID" runat="server"></asp:TextBox>
                <asp:HiddenField ID="hdnSID" runat="server" />
            </p>
            <div id="eSID" class="divError" runat="server"></div>
            <p>
                <label for="rdoWhereStudy">College student plans to attend:</label><br />
                <asp:RadioButtonList ID="rdoWhereStudy" runat="server" RepeatDirection="Vertical" RepeatLayout="Flow" style="width:100%;">
                    <asp:ListItem Value="SCC">Spokane Community College</asp:ListItem>
                    <asp:ListItem Value="SFCC">Spokane Falls Community College</asp:ListItem>
                </asp:RadioButtonList>
            </p> 
           <h5> Notes/Issues</h5>
            <!--
            <p>
                <label for="Subject">Subject: </label> 
                <asp:TextBox ID="subjectID" Enabled="false" runat="server"></asp:TextBox> &nbsp;&nbsp;
                <label for="ActionNeeded">Action Needed: </label>
                <asp:TextBox ID="actionNeededID" Enabled="false" runat="server"></asp:TextBox> &nbsp;&nbsp;
                <label for="DateCreated">Date Created: </label>
                <asp:TextBox ID="dateCreatedID" Enabled="false" runat="server"></asp:TextBox>&nbsp;&nbsp;
                <label for="NoteType">Note Type: </label>
                <asp:TextBox ID="noteTypeID" Enabled="false" runat="server"></asp:TextBox>
            </p>
            <p>
                <label for="Note">Note: </label> <br />
                <asp:TextBox ID="noteID" runat="server" Enabled="false" MaxLength="800" Rows="2" Columns="100" TextMode="MultiLine"></asp:TextBox>
            </p>
            -->
           <asp:GridView ID="gridView" AutoGenerateColumns="False" 
               emptydatatext="N/A" 
               runat="server">
        <HeaderStyle BackColor="#448BC1" Font-Bold="False" ForeColor="White" />
        <AlternatingRowStyle BackColor="AliceBlue"></AlternatingRowStyle>
               <Columns>
                   <asp:BoundField DataField="createDate" HeaderText="Date Created" />
                   <asp:BoundField DataField="homeName" HeaderText="Family" ItemStyle-Width="200px"/>
                 <asp:BoundField DataField="subject" HeaderText="Subject" ItemStyle-Width="200px"/>
                   <asp:BoundField DataField="note" HeaderText="Note" ItemStyle-Width="400px"/>
                   <asp:BoundField DataField="actionNeeded" HeaderText="Action Needed" />
                   <asp:BoundField DataField="actionCompleted" HeaderText="Action Done" />
                   <asp:BoundField DataField="lastModifiedDate" HeaderText="Date Editied" />
            </Columns>
           </asp:GridView>
        </div>
    </fieldset>
</div>
<asp:Literal ID="litResultMsg" runat="server"></asp:Literal>
<asp:HiddenField ID="hdnApplicantID" runat="server" />
<asp:HiddenField ID="hdnAgencyID" runat="server" />
<asp:HiddenField ID="hdnAddMode" runat="server" />
    <asp:HiddenField ID="hdnAgeLevel" runat="server" />
<fieldset id="personalInformation" runat="server">
<legend>Personal Information</legend>
    <asp:Literal ID="litAllRequired" runat="server" Visible="true" Text="<p style='color:#DD0000;'><em>All fields required</em></p>"></asp:Literal>
    <div id="eFamName" class="divError" runat="server"></div>
    <p>
        <label for="txtFamilyName"><span style="color:#DD0000;font-weight:600;"> * </span>Family name</label><br />
        <asp:TextBox Width="300px" ID="txtFamilyName" runat="server" MaxLength="30" ToolTip="Enter Family Name - required"></asp:TextBox>
    </p>
    <div id="eFirstName" class="divError" runat="server"></div>
    <p>
        <label for="txtFirstName"><span style="color:#DD0000;font-weight:600;"> * </span>First Name</label><br />
        <asp:TextBox ID="txtFirstName" runat="server" Width="300px" MaxLength="20" ToolTip="Enter First Name - required"></asp:TextBox>&nbsp;&nbsp
    </p>  
    <div id="eDOB" class="divError" runat="server"></div>
    <p>
        <label for="txtDOB"><span style="color:#DD0000;font-weight:600;"> * </span>Date of Birth</label>&nbsp;&nbsp;(Format: mm/dd/yyyy)<br />
        <asp:TextBox ID="txtDOB" runat="server" Width="300px" MaxLength="30" ToolTip="Enter Date of Birth - required"></asp:TextBox>&nbsp;&nbsp;
    </p>
    <div id="eQtrStart" class="divError" runat="server"></div>
    <p>
        <label for="rdoQuarterStart"><span style="color:#DD0000;font-weight:600;"> * </span>Quarter Start Date</label><br />
        <asp:RadioButtonList ID="rdoQuarterStart" runat="server" RepeatDirection="Vertical" RepeatLayout="Flow" ToolTip="Select Quarter Start Date - required">
        </asp:RadioButtonList>&nbsp;&nbsp;
    </p>
    
    <asp:Literal ID="litDuplicateRecords" runat="server"></asp:Literal>
    <div id="divMorePersonalInfo" runat="server" visible="false">
        <p>
            <label for="rdoGender">Gender</label><br />
            <asp:RadioButtonList ID="rdoGender" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
                <asp:ListItem Value="M">Male&nbsp;&nbsp;</asp:ListItem>
                <asp:ListItem Value="F">Female&nbsp;&nbsp;</asp:ListItem>
                <asp:ListItem Value="O">Other</asp:ListItem>
            </asp:RadioButtonList>
        </p>
        <p>
            <label for="txtCountry">Home Country</label><br />
            <asp:TextBox ID="txtCountry" runat="server" Width="300px" MaxLength="30"></asp:TextBox>
        </p>
        <p>
            <label for="txtPermEmail">Student Email</label><br />
            <asp:TextBox ID="txtPermEmail" Width="300px" runat="server" MaxLength="75"></asp:TextBox>
        </p>
         <div id="eArrivalDate" class="divError" runat="server"></div>
        <p>
            <label for="txtArrivalDate">Estimated date of arrival:</label>&nbsp;&nbsp;(Format: mm/dd/yyyy)<br />
            <asp:TextBox ID="txtArrivalDate" BackColor="ControlLight" runat="server" Width="300px" MaxLength="30"></asp:TextBox>
        </p>
       
    </div>
    <div id="divSearchButton" runat="server">
       <!-- <input type="submit" name="btnSubmit" id="btnSearchOLD" value="Next" /> -->
        <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" />
    </div>
</fieldset>
<fieldset id="recruitingAgent" runat="server">
<legend>Recruiting Agent</legend>
<p>Please verify that the contact information for your agent is up to date:</p>
    <p>
        <label for="txtAgency">Agency:</label><br />
        <asp:TextBox ID="txtAgency" runat="server" Width="300px" BackColor="ControlLight" MaxLength="30"></asp:TextBox>
    </p>
    <p>
        <label for="txtAgentName">Agent Name:</label><br />
        <asp:TextBox ID="txtAgentName" runat="server" Width="300px" BackColor="ControlLight" MaxLength="30"></asp:TextBox>
    </p>
    <p>
        <label for="txtAgentEmail">Agent Email:</label><br />
        <asp:TextBox ID="txtAgentEmail" runat="server" Width="300px" BackColor="ControlLight" MaxLength="30"></asp:TextBox>
    </p>
    <p>
    <label for="txtAgencyAddress" class="fieldTitle">Agency Street Address</label><br />
    <asp:TextBox ID="txtAgencyAddress" runat="server" Width="400px" MaxLength="75"></asp:TextBox>
    </p>
    <table style="width:100%;">
        <tr>
            <td><label for="txtAgencyCity" class="fieldTitle">Agency City</label></td>
            <td><label for="txtAgencyState" class="fieldTitle">Agency State/Province</label></td>
            <td><label for="txtAgencyZip" class="fieldTitle">Agency Zip/Postal Code</label></td>
        </tr>
        <tr>
            <td><asp:TextBox ID="txtAgencyCity" runat="server" Width="250px" MaxLength="30"></asp:TextBox></td>
            <td><asp:TextBox ID="txtAgencyState" Width="250px" runat="server" MaxLength="30"></asp:TextBox></td>
            <td><asp:TextBox ID="txtAgencyZip" Width="100px" runat="server" MaxLength="15"></asp:TextBox></td>
        </tr>
    </table>
        
    <p>
    <label for="txtAgencyCountry" class="fieldTitle">Agency Country</label><br />
    <asp:TextBox ID="txtAgencyCountry" runat="server"></asp:TextBox>
    </p>

    <p>
    <label for="txtAgencyPhone" id="agencyPhone" class="fieldTitle">Agency Phone</label><br />
    <asp:TextBox ID="txtAgencyPhone" Width="150px" runat="server"></asp:TextBox>
    </p>
</fieldset>

<fieldset id="homestay18" runat="server">
<legend>Homestay Options</legend>
    <ul>
        <li>Homestay Application fee is $200 for Homestay Applicants 18 years old or older.</li>
        <li>The Homestay Application Fee will be charged to your student account when you are assigned a Homestay Family.</li>
        <li>Homestay placement is not guaranteed.  However, if an appropriate family is not located, 
            you will not be charged the Homestay Application Fee. </li>
    </ul>
     <div id="eHomeStayOption" class="divError" runat="server"></div>
    <p>
        <label for="rdoHomestayOption"><span style="color:#DD0000;font-weight:600;"> * </span>Choose One:</label><br /> 
        <asp:RadioButtonList ID="rdoHomestayOption" runat="server" RepeatDirection="Vertical" BackColor="ControlLight" RepeatLayout="Flow" style="width:100%;">
            <asp:ListItem Value="With food">$700.00 per month for Homestay with food </asp:ListItem>
            <asp:ListItem Value="Without food">$400.00 per month for Homestay without food</asp:ListItem>
            <asp:ListItem Value="Either">Either is okay</asp:ListItem>
        </asp:RadioButtonList>   
    </p>
   
</fieldset>

<fieldset id="homestay1617" runat="server">

<legend>Homestay Terms</legend>
    <ul>
        <li>Students who are 16 or 17 years old are required to live with a Homestay Family.</li>
        <li>Homestay Application fee is $300 for 16 or 17 year old Homestay Applicants.</li>
        <li>The Homestay Application Fee will be charged to your student account when you are assigned a Homestay Family.</li>
        <li>Full Homestay is $850 per month, including meals.</li>
    </ul>
</fieldset>



<fieldset id="PhotoLetter" runat="server">
<legend>Photo and Letter</legend>
    <p>Please upload a photo and short letter about yourself using the file upload below.  You may return later to your application later to upload files.</p>
    <div id="eLetter"  class="divError" runat="server"></div> 
    <p>
        Letter:  <asp:FileUpload ID="uplLetterFile" runat="server" Size="70" /> <asp:Literal ID="litLetterFile" runat="server"></asp:Literal>
        <asp:HiddenField ID="hdnLetterSubmitted" runat="server" /><br />
    </p>
    <div id="ePhoto"  class="divError" runat="server"></div> 
    <p>
        Photo:  <asp:FileUpload ID="uplPhotoFile" runat="server" Size="70" /> <asp:Literal ID="litPhotoFile" runat="server"></asp:Literal>
        <asp:HiddenField ID="hdnPhotoSubmitted" runat="server" />
    </p>
</fieldset>
<div id="divSave" runat="server" visible="false" style="float:left;margin-right:30px;">
  <!--  <input type="submit" name="btnSubmit" id="btnSaveOLD" value="Save" onclick="validateRequired()" /> -->
    <asp:Button ID="btnCmdSave" runat="server" Text="Save and Upload" OnClick="btnCmdSave_Click" OnClientClick="return ValidateFileName();" />
</div>
<div id="divUpload" runat="server" visible="false" style="float:left;margin-right:30px;">
  <!--  <input type="submit" name="btnSubmit" id="btnUpload" value="Upload" /> -->
    <asp:Button ID="btnCmdUpload" runat="server" Text="Upload" OnClick="btnCmdUpload_Click" />
</div>
<div id="divNextPage" runat="server" style="float:left;margin-right:30px;">
   <!-- <input type="submit" name="btnSubmit" id="btnContinue" value="Continue"/>&nbsp;&nbsp; -->
    <asp:Button ID="btnCmdContinue" runat="server" Text="Continue" OnClick="btnCmdContinue_Click" />&nbsp;&nbsp;

</div>
     <asp:Button ID="Button1" runat="server" Text="Return to Admin Home Page" OnClick="btnCmdReturnAdminHome_Click" />&nbsp&nbsp;

</form>
</body>
</html>
