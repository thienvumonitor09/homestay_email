﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Homestay_Admin_Communication2 : System.Web.UI.Page
{
    String strUploadURL = @"\\ccs-internet\InternetContent\CCS\Homestay\";
    private static String strLogonUser;
    protected void Page_Load(object sender, EventArgs e)
    {
        strLogonUser = Request.ServerVariables["LOGON_USER"];
        //Response.Write(strLogonUser);
    }
    [WebMethod()]
    public static string GetLogonUser()
    {
        return strLogonUser;
    }
}