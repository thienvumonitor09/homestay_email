﻿<%@ Page Title="List of Active Homestay Families" Language="C#" AutoEventWireup="true" MasterPageFile="~/CCSApps.master" CodeFile="Family_ListActive.aspx.cs" Inherits="Homestay_Admin_Reports_Family_ListActive" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css">

<script type="text/javascript"  src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
    <script type="text/javascript"  src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript"  src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript"  src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
    <script type="text/javascript"  src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script type="text/javascript"  src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>

<script type="text/javascript"  src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<!--<script type="text/javascript"  src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script> --> 
    <script type="text/javascript"  src="https://nightly.datatables.net/buttons/js/buttons.html5.min.js"></script>
    <script type="text/javascript"  src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
<style type="text/css">
    p
    {
        padding:2px;
        margin:0px;
    }
    .showHide {  
        cursor: pointer;  
    }  

    td.details-control { 
        background: url('../../images/details_open.png') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('../../images/details_close.png') no-repeat center center;
    }

    td {
      text-align: left;
    }
    tfoot {
        display: table-header-group;
    }
</style>

    <script>
        function format(d) {
            // `d` is the original data object for the row
            var result = "";
            //result += "<div style='margin-left:20px;'>";
            /*
            result += "<strong>Pets:</strong> " + d.PetsInHome + "<br/>";
            result += "<strong>Address: </strong>" + d.StreetAddress + ", " + d.City + ", " + d.State + ", " + d.ZIP +"<br/>"
            result += "<strong>Landline Phone:  </strong>" + d.LandlinePhone + "<br/>";
            result += "<strong>Cell Phone:  </strong>" + d.CellPhone + "<br/>";
            result += "<strong>Email:  </strong>" + d.Email + "<br/>";
            result += "<strong>No of Open Rooms:  </strong>" + d.NoOpenRoom + "<br/>";
            //result = + "</div>";
            return result;
            */
            // `d` is the original data object for the row
           var tbl = $('<table cellpadding="5" cellspacing="0" border="1" style="width:70%;padding-left:50px; margin-left:90px;"/>');
           tbl.append('<tr>' +
                           '<td><strong>Address:</strong></td>' +
                           '<td>' + d.Address + '</td>' +
                        '</tr>');
           tbl.append('<tr>' +
                           '<td><strong>Landline Phone: </strong></td>' +
                           '<td>' + d.LandlinePhone + '</td>' +
                        '</tr>');
           tbl.append('<tr>' +
                           '<td><strong>Students: </strong></td>' +
                           '<td>'+ d.Student +'</td>' +
               '</tr>');
           return tbl;
        }
        $(function () {
            $('#clearFilter').on('click', function (e) {
                var table = $('#studentTable').DataTable();
                table.search('').columns().search('').draw();
            });

            $.ajax({
                type: "POST",
                dataType: "json",
                url: "ReportWS.asmx/GetDatatable_Family",
                beforeSend: function() {
                    $("#loaderDiv").show();
                    $("#mainDiv").hide();
                },
                success: function (data) {
                    $("#loaderDiv").hide();
                    $("#mainDiv").show();
                    console.log(data);
                    //alert(data);
                    var datatableVariable = $('#familyTable').DataTable({
                        data: data,
                        dom: "<'row'<'col-sm-6'l><'col-sm-6'>>" +
                        "<'row'<'col-sm-12'Btr>>" +
                        "<'row'<'col-sm-5'i><'col-sm-7'p>>", //To display buttons
                        //lBfrtip
                        columnDefs: [
                            {
                                //to hide column:PetsInHome, Address , LandlinePhone, Student
                                targets: [1,10,11,12,14], 
                                visible: false,
                                searchable: false
                            },
                            {
                                // for column visibility btn
                                targets: [0,1, 10, 11, 12, 13], 
                                className: 'noVis'
                            }

                        ],
                        buttons: [
                            {
                                extend: 'colvis',
                                columns: ':not(.noVis)'
                            },
                            {
                                extend: 'excelHtml5',
                                filename: "Active_Homestay_Families",
                                //title: "List of Active Homestay Students",
                                exportOptions: {
                                    columns: 'th:not(:first-child)'
                                }
                            },
                        ],
                        columns: [
                            {
                                "className": 'details-control',
                                "orderable": false,
                                "data": null,
                                "defaultContent": ''
                            },
                            { 'data': 'Id' },
                            { 'data': 'HomeName' },
                            { 'data': 'GenderPreference' },
                            { 'data': 'FamilyStatus' },
                            { 'data': 'NoOpenRoom' },
                            { 'data': 'AllowSmoking' },
                            { 'data': 'CollegeQualified' },
                            { 'data': 'TimeSCC' },
                            { 'data': 'TimeSFCC' },
                            { 'data': 'PetsInHome' },
                            { 'data': 'Address' },
                            { 'data': 'LandlinePhone' },
                            { 'data': 'HSOption' },
                            { 'data': 'Student' }
                        ],
                        "order": [[2, 'asc']] //order first column when initialization + to remove sorting icon when first load

                    });
                 
                    
                    
                    //$('#example thead tr').clone(true).appendTo('#example thead');
                    $('#familyTable tfoot th').each(function (index, value) {
                        if (index > 0) {
                            var placeHolderTitle = $('#familyTable thead th').eq($(this).index()).text();
                            $(this).html('<input type="text" class="form-control input input-sm" placeholder = "' + placeHolderTitle + '" />');
                        }

                    });

                   

                    
                    datatableVariable.columns([6]).every(function () {
                        var column = this;
                        var select = $('<select class="form-control"><option value="">----</option></select>')
                            .appendTo($(column.footer()).empty())
                            .on('change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                );

                                column
                                    .search(val ? '^' + val + '$' : '', true, false)
                                    .draw();
                            });

                        column.data().unique().sort().each(function (d, j) {
                            select.append('<option value="' + d + '">' + d + '</option>');
                        });
                    });

                    var i = 0;
                    datatableVariable.columns().every(function () {

                        var column = this;
                        $(this.footer()).find('input').on('keyup change', function () {
                            column.search(this.value).draw();
                        });

                        i++;

                    });
                    // Add event listener for opening and closing details
                    $('#familyTable tbody').on('click', 'td.details-control', function () {

                        var tr = $(this).closest('tr');
                        var row = datatableVariable.row(tr);
                        //
                        if (row.child.isShown()) {

                            // This row is already open - close it
                            row.child.hide();
                            tr.removeClass('shown');
                        }
                        else {
                            // Open this row
                            row.child(format(row.data())).show();

                            tr.addClass('shown');
                        }
                    });
                    
                },
                error: function (request, error) {
                    alert("failed");
                }
            });
});
    </script>
    </asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="bannerH1Text" Runat="Server">
    Homestay Administration
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">
    <h2>List of Active Homestay Families<asp:Literal ID="litType" runat="server"></asp:Literal></h2>
    <div id="divHome" runat="server" style="width:100%; float:left;">
        <asp:Button ID="Button1" runat="server" Text="Return to Main Page" OnClick="linkBtnReport_Click" />&nbsp;&nbsp;
        <asp:Button ID="linkBtnHome" runat="server" Text="Return to Admin Home Page" OnClick="linkBtnHome_Click" />&nbsp;&nbsp;
    </div>
    <div id="loaderDiv">
        <img src="../../images/demo_wait.gif" />
    </div>
    <div id="mainDiv" style="padding: 20px; margin-top: 50px" class="container-fluid">  
        <div class="form-row">
        <div class="form-group col-md-2">
          <button class="btn btn-success" id="clearFilter">Reset</button>
        </div>
        <div class="form-group col-md-4">
            
        </div>
        
    </div> 
    <table id="familyTable" class="display"  style="width:100%">  
                <thead>  
                    <tr> 
                        <th></th>
                         <th>Id</th>  
                        <th>Name</th> 
                       <th>Gender Preference</th> 
                        <th>Status</th> 
                        <th>No. Open Room</th>
                        <th>Allow Smoking?</th>
                        <th>College Qualified</th> 
                        <th>Time to SCC</th> 
                        <th>Time to SFCC</th> 
                        <th>Pets</th> 
                        <th>Address</th> 
                        <th>LandlinePhone</th>
                        <th>HS Option</th> 
                        <th>Student</th> 
                    </tr>  
                </thead>  
                <tfoot>  
                    <tr>  
                        <th></th>  
                       <th>Id</th>  
                         <th>Name</th> 
                       <th>Gender Preference</th> 
                        <th>Status</th> 
                        <th>No Open Room</th>
                        <th>Allow Smoking?</th>
                        <th>College Qualified</th> 
                        <th>Commute Time to SCC</th> 
                        <th>Commute Time to SFCC</th> 
                        <th>Pets</th> 
                        <th>Address</th> 
                        <th>LandlinePhone</th>
                        <th>HS Option</th> 
                        <th>Student</th> 
                    </tr>  
                </tfoot>  
            </table>
    </div>
     
    </asp:Content>