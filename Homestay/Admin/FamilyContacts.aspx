﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CCSApps.master" AutoEventWireup="true" CodeFile="FamilyContacts.aspx.cs" Inherits="Homestay_Admin_UpdateFamily" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <link href="/Homestay/_css/Homestay.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="bannerH1Text" Runat="Server">
    Family Contacts
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">
<h4>Select a Homestay Family</h4>
    <div id="divHome" runat="server" style="float:right;margin-right:30px;">
        <input type="submit" name="btnSubmit" id="btnHome" value="Return to Admin Home Page"/>&nbsp;&nbsp;
    </div>
<div style="padding-bottom:10px;">
<asp:DropDownList ID="ddlFamilies" AutoPostBack="true" runat="server"></asp:DropDownList>
</div>
<asp:Literal ID="litResultMsg" runat="server"></asp:Literal>

<fieldset>
<legend>Family Home</legend>

    <p>
        <label for="homeName">Home Name</label><br />
        <asp:TextBox ID="homeName" runat="server" Width="300px" MaxLength="20"></asp:TextBox>
    </p>
    <p>
        <label for="streetAddress">Street Address</label><br />
        <asp:TextBox ID="streetAddress" runat="server" Width="300px" MaxLength="20"></asp:TextBox>
    </p>
    <div id="eStreetAddress" class="divError" runat="server"></div>
    <p>
        <label for="city">City</label><br />
        <asp:TextBox ID="city" runat="server" Width="300px" MaxLength="20"></asp:TextBox>
    </p>
    <div id="eCity" class="divError" runat="server"></div>
    <p>
        <label for="state">State</label><br />
        <asp:TextBox ID="state" runat="server" Width="300px" MaxLength="20"></asp:TextBox>
    </p>
    <div id="eState" class="divError" runat="server"></div>
    <p>
        <label for="ZIP">ZIP Code</label><br />
        <asp:TextBox ID="ZIP" runat="server" Width="300px" MaxLength="20"></asp:TextBox>
    </p>
    <div id="eZip" class="divError" runat="server"></div>
    <p>
        <label for="landLinePhone">Landline Phone/Primary contact phone</label><br />
        <asp:TextBox ID="landLinePhone" runat="server" Width="300px" MaxLength="20"></asp:TextBox>
    </p>
    <div id="ePhone" class="divError" runat="server"></div>
    <p>
        Please describe the bedrooms you have available for students and the room status.
    </p>
    <div class="accordionDiv">
    
    <h4 class="accordion accordion-toggle">Room 1</h4> 
    <div class="panel accordion-content">
        <p>Level in home: <asp:RadioButtonList ID="rdoRoom1" runat="server" RepeatDirection="Horizontal"><asp:ListItem Value="Main" Text="Main Level" /><asp:ListItem Value="Upper" Text="Upper Level" /><asp:ListItem Value="Lower" Text="Lower Level" /></asp:RadioButtonList>  </p>
        <div id="eRoom1Level" class="divError" runat="server"></div>
        <p>Available dates: <asp:TextBox ID="room1DateOpen" runat="server" /> to <asp:TextBox ID="room1DateClosed" runat="server" /> </p>
        <div id="eRoom1Date" class="divError" runat="server"></div>
        <p>Bathing facilities:  <asp:RadioButtonList ID="rdoRoom1Bathroom" runat="server" RepeatDirection="Horizontal"><asp:ListItem Text="Private Room, Shared Bathroom" Value="Shared" /><asp:ListItem Text="Private Room, Private Bathroom" Value="Private" /></asp:RadioButtonList></p>
        <div id="eRoom1Bathroom" class="divError" runat="server"></div>
        <p>Occupancy: <asp:RadioButtonList ID="rdoRoom1Occupancy" runat="server"><asp:ListItem Value="Open" Text="Available now"></asp:ListItem><asp:ListItem Value="Planning" Text="Open soon, subject to above dates"></asp:ListItem><asp:ListItem Value="CCS" Text="Occupied by CCS Student"></asp:ListItem><asp:ListItem Value="Non-CCS" Text="Occupied by Student from another program"></asp:ListItem></asp:RadioButtonList></p>
        <div id="eRoom1Occupancy" class="divError" runat="server"></div>
     </div>
    <h4 class="accordion accordion-toggle">Room 2</h4> 
        <div class="panel accordion-content">
        <p>Level in home: <asp:RadioButtonList ID="rdoRoom2" runat="server" RepeatDirection="Horizontal"><asp:ListItem Value="Main" Text="Main Level" /><asp:ListItem Value="Upper" Text="Upper Level" /><asp:ListItem Value="Lower" Text="Lower Level" /></asp:RadioButtonList>  </p>
        <p>Available dates: <asp:TextBox ID="room2DateOpen" runat="server" /> to <asp:TextBox ID="room2DateClosed" runat="server" /> </p>
        <div id="eRoom2Date" class="divError" runat="server"></div>
        <p>Bathing facilities:  <asp:RadioButtonList ID="rdoRoom2Bathroom" runat="server" RepeatDirection="Horizontal"><asp:ListItem Text="Private Room, Shared Bathroom" Value="Shared" /><asp:ListItem Text="Private Room, Private Bathroom" Value="Private" /></asp:RadioButtonList></p>
        <p>Occupancy: <asp:RadioButtonList ID="rdoRoom2Occupancy" runat="server"><asp:ListItem Value="Open" Text="Available now"></asp:ListItem><asp:ListItem Value="Planning" Text="Open soon, subject to above dates"></asp:ListItem><asp:ListItem Value="CCS" Text="Occupied by CCS Student"></asp:ListItem><asp:ListItem Value="Non-CCS" Text="Occupied by Student from another program"></asp:ListItem></asp:RadioButtonList></p>
    </div>
    <h4 class="accordion accordion-toggle">Room 3</h4> 
    <div class="panel accordion-content">
    <p>Level in home: <asp:RadioButtonList ID="rdoRoom3" runat="server" RepeatDirection="Horizontal"><asp:ListItem Value="Main" Text="Main Level" /><asp:ListItem Value="Upper" Text="Upper Level" /><asp:ListItem Value="Lower" Text="Lower Level" /></asp:RadioButtonList>  </p>
    <p>Available dates: <asp:TextBox ID="room3DateOpen" runat="server" /> to <asp:TextBox ID="room3DateClosed" runat="server" /> </p>
    <div id="eRoom3Date" class="divError" runat="server"></div>
    <p>Bathing facilities:  <asp:RadioButtonList ID="rdoRoom3Bathroom" runat="server" RepeatDirection="Horizontal"><asp:ListItem Text="Private Room, Shared Bathroom" Value="Shared" /><asp:ListItem Text="Private Room, Private Bathroom" Value="Private" /></asp:RadioButtonList></p>
    <p>Occupancy: <asp:RadioButtonList ID="rdoRoom3Occupancy" runat="server"><asp:ListItem Value="Open" Text="Available now"></asp:ListItem><asp:ListItem Value="Planning" Text="Open soon, subject to above dates"></asp:ListItem><asp:ListItem Value="CCS" Text="Occupied by CCS Student"></asp:ListItem><asp:ListItem Value="Non-CCS" Text="Occupied by Student from another program"></asp:ListItem></asp:RadioButtonList></p>
    </div>
    <h4 class="accordion accordion-toggle">Room 4</h4> 
    <div class="panel accordion-content">
        <p>Level in home: <asp:RadioButtonList ID="rdoRoom4" runat="server" RepeatDirection="Horizontal"><asp:ListItem Value="Main" Text="Main Level" /><asp:ListItem Value="Upper" Text="Upper Level" /><asp:ListItem Value="Lower" Text="Lower Level" /></asp:RadioButtonList>  </p>
        <p>Available dates: <asp:TextBox ID="room4DateOpen" runat="server" /> to <asp:TextBox ID="room4DateClosed" runat="server" /> </p>
        <div id="eRoom4Date" class="divError" runat="server"></div>
        <p>Bathing facilities:  <asp:RadioButtonList ID="rdoRoom4Bathroom" runat="server" RepeatDirection="Horizontal"><asp:ListItem Text="Private Room, Shared Bathroom" Value="Shared" /><asp:ListItem Text="Private Room, Private Bathroom" Value="Private" /></asp:RadioButtonList></p>
        <p>Occupancy: <asp:RadioButtonList ID="rdoRoom4Occupancy" runat="server"><asp:ListItem Value="Open" Text="Available now"></asp:ListItem><asp:ListItem Value="Planning" Text="Open soon, subject to above dates"></asp:ListItem><asp:ListItem Value="CCS" Text="Occupied by CCS Student"></asp:ListItem><asp:ListItem Value="Non-CCS" Text="Occupied by Student from another program"></asp:ListItem></asp:RadioButtonList></p>
    </div>
     </div>
</fieldset>
<fieldset>
<legend>Homeowner's Insurance Policy</legend>
    <p>
        <label for="insuranceCompanyName">Company Name</label><br />
        <asp:TextBox ID="insuranceCompanyName" runat="server" Width="300px" MaxLength="50"></asp:TextBox>
    </p>
    <p>
        <label for="insurancePolicyNumber">Policy Number</label><br />
        <asp:TextBox ID="insurancePolicyNumber" runat="server" Width="300px" MaxLength="50"></asp:TextBox>
    </p>
    <p>
        <label for="insuranceAgentName">Agent Name</label><br />
        <asp:TextBox ID="insuranceAgentName" runat="server" Width="300px" MaxLength="50"></asp:TextBox>
    </p>
    <p>
        <label for="insuranceAgentPhone">Agent Phone Number</label><br />
        <asp:TextBox ID="insuranceAgentPhone" runat="server" Width="300px" MaxLength="20"></asp:TextBox>
    </p>
    <div>
       <!-- <input type="submit" class="button" name="btnSubmit" id="btnUpdateFamilyHome" value="Update Family Home" /> -->
        <asp:Button ID="btnCmdUpdateFamilyHome" runat="server" Text="Update Family Home" OnClick="btnCmdUpdateFamilyHome_Click" />
    </div>
</fieldset>
<fieldset>
<legend>Host Family Contacts</legend>
<div class = "accordionDiv">
    <h3 class="accordion accordion-toggle">Primary Host Family Contact</h3>
    <div class="panel accordion-content">
        <h4>Full Legal Name</h4>
        <p>
            <label for="familyName">Family Name</label><br />
            <asp:TextBox Width="300px" ID="familyName" runat="server" class="required" MaxLength="50"></asp:TextBox>
        </p>
        <p>
            <label for="firstName">First Name</label><br />
            <asp:TextBox ID="firstName" runat="server" Width="300px" MaxLength="30"></asp:TextBox>
        </p>
        <p>
            <label for="middleName">Middle Name</label><br />
            <asp:TextBox ID="middleName" runat="server" Width="300px" MaxLength="20"></asp:TextBox>
        </p>
        <p>
            <label for="rdoGender">Gender</label><br />
            <asp:RadioButtonList ID="rdoGender" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
                <asp:ListItem Value="M">Male&nbsp;&nbsp;</asp:ListItem>
                <asp:ListItem Value="F">Female&nbsp;&nbsp;</asp:ListItem>
                <asp:ListItem Value="O">Other</asp:ListItem>
            </asp:RadioButtonList>

        </p>
        <h4>Personal Information</h4>
        <p>
            <label for="DOB">Date of Birth</label>&nbsp;&nbsp;(Format: mm/dd/yyyy)<br />
            <asp:TextBox ID="DOB" runat="server" Width="300px" MaxLength="30"></asp:TextBox>
        </p>
        <div class="divError" id="ePrimaryDOB" runat="server"></div>
        <p>
            <label for="driversLicenseNumber">Driver's License Number</label><br />
            <asp:TextBox ID="driversLicenseNumber" runat="server" Width="300px" MaxLength="25"></asp:TextBox>
        </p>
        <p>
            <label for="occupation">Occupation</label><br />
            <asp:TextBox ID="occupation" runat="server" Width="300px" MaxLength="50"></asp:TextBox>
        </p>
        <p>
            <label for="cellPhone">Cell Phone</label><br />
            <asp:TextBox ID="cellPhone" runat="server" Width="300px" MaxLength="15"></asp:TextBox>
        </p>
        <p>
            <label for="workPhone">Work Phone</label><br />
            <asp:TextBox ID="workPhone" runat="server" Width="300px" MaxLength="15"></asp:TextBox>
        </p>
        <p>
            <label for="email">Email</label><br />
            <asp:TextBox ID="email" runat="server" Width="300px" MaxLength="75"></asp:TextBox>
        </p>
        <div id="eEmailPrimary" class="divError" runat="server"></div>
        <div>
          <!--  <input type="submit" class="button" name="btnSubmit" id="btnPimaryContact" value="Update Primary Contact" /> -->
            <asp:Button ID="btnCmdUpdatePrimaryContact" runat="server" Text="Update Primary Contact" OnClick="btnCmdUpdatePrimaryContact_Click" />
            <asp:HiddenField ID="hdnPrimaryContactID" runat="server" />
        </div>
   </div>
   <h3 class="accordion accordion-toggle">Secondary Host Family Contact</h3>
   <div class="panel accordion-content">
        <h4>Full Legal Name</h4>
        <p>
            <label for="familyName2">Family Name</label><br />
            <asp:TextBox Width="300px" ID="familyName2" runat="server" class="required" MaxLength="50"></asp:TextBox>
        </p>
        <p>
            <label for="firstName2">First Name</label><br />
            <asp:TextBox ID="firstName2" runat="server" Width="300px" MaxLength="30"></asp:TextBox>
        </p>
        <p>
            <label for="middleName2">Middle Name</label><br />
            <asp:TextBox ID="middleName2" runat="server" Width="300px" MaxLength="20"></asp:TextBox>
        </p>
        <p>
            <label for="rdoGender2">Gender</label><br />
            <asp:RadioButtonList ID="rdoGender2" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
                <asp:ListItem Value="M">Male&nbsp;&nbsp;</asp:ListItem>
                <asp:ListItem Value="F">Female&nbsp;&nbsp;</asp:ListItem>
                <asp:ListItem Value="O">Other</asp:ListItem>
            </asp:RadioButtonList>

        </p>
        <h4>Personal Information</h4>
        <p>
            <label for="DOB2">Date of Birth</label>&nbsp;&nbsp;(Format: mm/dd/yyyy)<br />
            <asp:TextBox ID="DOB2" runat="server" Width="300px" MaxLength="30"></asp:TextBox>
        </p>
       <div id="eDOB2" class="divError" runat="server"></div>
        <p>
            <label for="driversLicenseNumber2">Driver's License Number</label><br />
            <asp:TextBox ID="driversLicenseNumber2" runat="server" Width="300px" MaxLength="25"></asp:TextBox>
        </p>
        <p>
            <label for="occupation2">Occupation</label><br />
            <asp:TextBox ID="occupation2" runat="server" Width="300px" MaxLength="50"></asp:TextBox>
        </p>
        <p>
            <label for="cellPhone2">Cell Phone</label><br />
            <asp:TextBox ID="cellPhone2" runat="server" Width="300px" MaxLength="15"></asp:TextBox>
        </p>
        <p>
            <label for="workPhone2">Work Phone</label><br />
            <asp:TextBox ID="workPhone2" runat="server" Width="300px" MaxLength="15"></asp:TextBox>
        </p>
        <p>
            <label for="email2">Email</label><br />
            <asp:TextBox ID="email2" runat="server" Width="300px" MaxLength="75"></asp:TextBox>
        </p> 
       <div id="eEmail2" class="divError" runat="server"></div>  
        <div>
           <!-- <input type="submit" class="button" name="btnSubmit" id="btnSecondaryContact" value="Update Secondary Contact" /> -->
            <asp:Button ID="btnCmdUpdateSecondaryContact" runat="server" Text="Update Secondary Contact" OnClick="btnCmdUpdateSecondaryContact_Click" />
            <asp:HiddenField ID="hdnSecondaryContactID" runat="server" />
        </div>
    </div>
    <h3 class="accordion accordion-toggle">Other Adult Occupant 1</h3>
    <div class="panel accordion-content">
        <h4>Full Legal Name</h4>
        <p>
            <label for="familyName3">Family Name</label><br />
            <asp:TextBox Width="300px" ID="familyName3" runat="server" class="required" MaxLength="50"></asp:TextBox>
        </p>
        <p>
            <label for="firstName3">First Name</label><br />
            <asp:TextBox ID="firstName3" runat="server" Width="300px" MaxLength="30"></asp:TextBox>
        </p>
        <p>
            <label for="middleName3">Middle Name</label><br />
            <asp:TextBox ID="middleName3" runat="server" Width="300px" MaxLength="20"></asp:TextBox>
        </p>
        <p>
            <label for="rdoGender3">Gender</label><br />
            <asp:RadioButtonList ID="rdoGender3" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
                <asp:ListItem Value="M">Male&nbsp;&nbsp;</asp:ListItem>
                <asp:ListItem Value="F">Female&nbsp;&nbsp;</asp:ListItem>
                <asp:ListItem Value="O">Other</asp:ListItem>
            </asp:RadioButtonList>

        </p>
        <h4>Personal Information</h4>
        <p>
            <label for="DOB3">Date of Birth</label>&nbsp;&nbsp;(Format: mm/dd/yyyy)<br />
            <asp:TextBox ID="DOB3" runat="server" Width="300px" MaxLength="30"></asp:TextBox>
        </p>
        <div id="eDOB3" class="divError" runat="server"></div>
        <p>
            <label for="driversLicenseNumber3">Driver's License Number</label><br />
            <asp:TextBox ID="driversLicenseNumber3" runat="server" Width="300px" MaxLength="25"></asp:TextBox>
        </p>
        <p>
            <label for="occupation3">Occupation</label><br />
            <asp:TextBox ID="occupation3" runat="server" Width="300px" MaxLength="50"></asp:TextBox>
        </p>
        <p>
            <label for="cellPhone3">Cell Phone</label><br />
            <asp:TextBox ID="cellPhone3" runat="server" Width="300px" MaxLength="15"></asp:TextBox>
        </p>
        <p>
            <label for="workPhone3">Work Phone</label><br />
            <asp:TextBox ID="workPhone3" runat="server" Width="300px" MaxLength="15"></asp:TextBox>
        </p>
        <p>
            <label for="email3">Email</label><br />
            <asp:TextBox ID="email3" runat="server" Width="300px" MaxLength="75"></asp:TextBox>
        </p>  
        <div id="eEmail3" class="divError" runat="server"></div>
        <div>
           <!-- <input type="submit" class="button" name="btnSubmit" id="btnOtherAdult1" value="Update Other Adult 1" /> -->
            <asp:Button ID="btnCmdUpdateOtherAdult1" runat="server" Text="Update Other Adult 1" OnClick="btnCmdUpdateOtherAdult1_Click" />
            <asp:HiddenField ID="hdnOtherAdult1ID" runat="server" />
        </div>
    </div>

    <h3 class="accordion accordion-toggle">Other Adult Occupant 2</h3>
    <div class="panel accordion-content">
        <h4>Full Legal Name</h4>
        <p>
            <label for="familyName4">Family Name</label><br />
            <asp:TextBox Width="300px" ID="familyName4" runat="server" class="required" MaxLength="50"></asp:TextBox>
        </p>
        <p>
            <label for="firstName4">First Name</label><br />
            <asp:TextBox ID="firstName4" runat="server" Width="300px" MaxLength="30"></asp:TextBox>
        </p>
        <p>
            <label for="middleName4">Middle Name</label><br />
            <asp:TextBox ID="middleName4" runat="server" Width="300px" MaxLength="20"></asp:TextBox>
        </p>
        <p>
            <label for="rdoGender4">Gender</label><br />
            <asp:RadioButtonList ID="rdoGender4" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
                <asp:ListItem Value="M">Male&nbsp;&nbsp;</asp:ListItem>
                <asp:ListItem Value="F">Female&nbsp;&nbsp;</asp:ListItem>
                <asp:ListItem Value="O">Other</asp:ListItem>
            </asp:RadioButtonList>

        </p>
        <h4>Personal Information</h4>
        <p>
            <label for="DOB4">Date of Birth</label>&nbsp;&nbsp;(Format: mm/dd/yyyy)<br />
            <asp:TextBox ID="DOB4" runat="server" Width="300px" MaxLength="30"></asp:TextBox>
        </p>
        <div id="eDOB4" class="divError" runat="server"></div>
        <p>
            <label for="driversLicenseNumber4">Driver's License Number</label><br />
            <asp:TextBox ID="driversLicenseNumber4" runat="server" Width="300px" MaxLength="25"></asp:TextBox>
        </p>
        <p>
            <label for="occupation4">Occupation</label><br />
            <asp:TextBox ID="occupation4" runat="server" Width="300px" MaxLength="50"></asp:TextBox>
        </p>
        <p>
            <label for="cellPhone4">Cell Phone</label><br />
            <asp:TextBox ID="cellPhone4" runat="server" Width="300px" MaxLength="15"></asp:TextBox>
        </p>
        <p>
            <label for="workPhone4">Work Phone</label><br />
            <asp:TextBox ID="workPhone4" runat="server" Width="300px" MaxLength="15"></asp:TextBox>
        </p>
        <p>
            <label for="email4">Email</label><br />
            <asp:TextBox ID="email4" runat="server" Width="300px" MaxLength="75"></asp:TextBox>
        </p>  
        <div id="eEmail4" class="divError" runat="server"></div>   
        <div>
           <!-- <input type="submit" class="button" name="btnSubmit" id="btnOtherAdult2" value="Update Other Adult 2" /> -->
            <asp:Button ID="btnCmdUpdateOtherAdult2" runat="server" Text="Update Other Adult 2" OnClick="btnCmdUpdateOtherAdult2_Click" />
            <asp:HiddenField ID="hdnOtherAdult2ID" runat="server" />
        </div>
    </div>
    <h3 class="accordion accordion-toggle">Minor Child 1</h3>
    <div class="panel accordion-content">
        <p>
            <label for="familyName5">Family Name</label><br />
            <asp:TextBox ID="familyName5" runat="server" Width="300px" BackColor="ControlLight" MaxLength="30"></asp:TextBox>
        </p>
        <p>
            <label for="firstName5">First Name</label><br />
            <asp:TextBox ID="firstName5" runat="server" Width="300px" BackColor="ControlLight" MaxLength="30"></asp:TextBox>
        </p>
        <p>
            <label for="DOB5">Date Of Birth</label><br />
            <asp:TextBox ID="DOB5" runat="server" Width="100px" BackColor="ControlLight" MaxLength="20"></asp:TextBox>
        </p>
        <div id="eDOB5" class="divError" runat="server"></div>
        <p>
            <label for="rdoGender5">Gender</label><br />
            <asp:RadioButtonList ID="rdoGender5" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
                <asp:ListItem Value="M">Male&nbsp;&nbsp;</asp:ListItem>
                <asp:ListItem Value="F">Female&nbsp;&nbsp;</asp:ListItem>
                <asp:ListItem Value="O">Other</asp:ListItem>
            </asp:RadioButtonList>

        </p>
        <div>
          <!--  <input type="submit" class="button" name="btnSubmit" id="btnMinorChild1" value="Update Minor Child 1" /> -->
            <asp:Button ID="btnCmdUpdateMinorChild1" runat="server" Text="Update Minor Child 1" OnClick="btnCmdUpdateMinorChild1_Click" />
            <asp:HiddenField ID="hdnMinorChild1ID" runat="server" />
        </div>
   </div>
    <h3 class="accordion accordion-toggle">Minor Child 2</h3>
    <div class="panel accordion-content">
        <p>
            <label for="familyName6">Family Name</label><br />
            <asp:TextBox ID="familyName6" runat="server" Width="300px" BackColor="ControlLight" MaxLength="50"></asp:TextBox>
        </p>
        <p>
            <label for="firstName6">First Name</label><br />
            <asp:TextBox ID="firstName6" runat="server" Width="300px" BackColor="ControlLight" MaxLength="30"></asp:TextBox>
        </p>
        <p>
            <label for="DOB6">Date Of Birth</label><br />
            <asp:TextBox ID="DOB6" runat="server" Width="100px" BackColor="ControlLight" MaxLength="20"></asp:TextBox>
        </p>
        <div id="eDOB6" class="divError" runat="server"></div>
        <p>
            <label for="rdoGender6">Gender</label><br />
            <asp:RadioButtonList ID="rdoGender6" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
                <asp:ListItem Value="M">Male&nbsp;&nbsp;</asp:ListItem>
                <asp:ListItem Value="F">Female&nbsp;&nbsp;</asp:ListItem>
                <asp:ListItem Value="O">Other</asp:ListItem>
            </asp:RadioButtonList>
        </p>
        <div>
           <!-- <input type="submit" class="button" name="btnSubmit" id="btnMinorChild2" value="Update Minor Child 2" /> -->
            <asp:Button ID="btnCmdUpdateMinorChild2" runat="server" Text="Update Minor Child 2" OnClick="btnCmdUpdateMinorChild2_Click" />
            <asp:HiddenField ID="hdnMinorChild2ID" runat="server" />
        </div>
    </div>
    <h3 class="accordion accordion-toggle">Minor Child 3</h3>
    <div class="panel accordion-content">
        <p>
            <label for="familyName7">Family Name</label><br />
            <asp:TextBox ID="familyName7" runat="server" Width="300px" BackColor="ControlLight" MaxLength="50"></asp:TextBox>
        </p>
        <p>
            <label for="firstName7">First Name</label><br />
            <asp:TextBox ID="firstName7" runat="server" Width="300px" BackColor="ControlLight" MaxLength="30"></asp:TextBox>
        </p>
        <p>
            <label for="DOB7">Date Of Birth</label><br />
            <asp:TextBox ID="DOB7" runat="server" Width="100px" BackColor="ControlLight" MaxLength="20"></asp:TextBox>
        </p>
        <div id="eDOB7" class="divError" runat="server"></div>
        <p>
            <label for="rdoGender7">Gender</label><br />
            <asp:RadioButtonList ID="rdoGender7" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
                <asp:ListItem Value="M">Male&nbsp;&nbsp;</asp:ListItem>
                <asp:ListItem Value="F">Female&nbsp;&nbsp;</asp:ListItem>
                <asp:ListItem Value="O">Other</asp:ListItem>
            </asp:RadioButtonList>

        </p>
        <div>
          <!--  <input type="submit" class="button" name="btnSubmit" id="btnMinorChild3" value="Update Minor Child 3" /> -->
            <asp:Button ID="btnCmdUpdateMinorChild3" runat="server" Text="Update Minor Child 3" OnClick="btnCmdUpdateMinorChild3_Click" />
            <asp:HiddenField ID="hdnMinorChild3ID" runat="server" />
        </div>
    </div>
    <h3 class="accordion accordion-toggle">Minor Child 4</h3>
    <div class="panel accordion-content">
        <p>
            <label for="familyName8">Family Name</label><br />
            <asp:TextBox ID="familyName8" runat="server" Width="300px" BackColor="ControlLight" MaxLength="50"></asp:TextBox>
        </p>
        <p>
            <label for="firstName8">First Name</label><br />
            <asp:TextBox ID="firstName8" runat="server" Width="300px" BackColor="ControlLight" MaxLength="30"></asp:TextBox>
        </p>
        <p>
            <label for="DOB8">Date Of Birth</label><br />
            <asp:TextBox ID="DOB8" runat="server" Width="100px" BackColor="ControlLight" MaxLength="20"></asp:TextBox>
        </p>
        <div id="eDOB8" class="divError" runat="server"></div>
        <p>
            <label for="rdoGender8">Gender</label><br />
            <asp:RadioButtonList ID="rdoGender8" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
                <asp:ListItem Value="M">Male&nbsp;&nbsp;</asp:ListItem>
                <asp:ListItem Value="F">Female&nbsp;&nbsp;</asp:ListItem>
                <asp:ListItem Value="O">Other</asp:ListItem>
            </asp:RadioButtonList>
        </p>
        <div>
           <!-- <input type="submit" class="button" name="btnSubmit" id="btnMinorChild4" value="Update Minor Child 4" /> -->
            <asp:Button ID="btnCmdUpdateMinorChild4" runat="server" Text="Update Minor Child 4" OnClick="btnCmdUpdateMinorChild4_Click" />
            <asp:HiddenField ID="hdnMinorChild4ID" runat="server" />
        </div>
    </div>

</div>
</fieldset>

<script type="text/javascript">
    $(function () {
        var acc = document.getElementsByClassName("accordion");
        var i;

        for (i = 0; i < acc.length; i++) {
            acc[i].addEventListener("click", function () {
                this.classList.toggle("active");
            });
        }
       
        $('.accordionDiv').find('.accordion-toggle').click(function () {
            //Expand or collapse this panel
            $(this).next().slideToggle('fast');
            //Hide the other panels
            //$(".accordion-content").not($(this).next()).slideUp('fast');
        });
    });
</script>

</asp:Content>


