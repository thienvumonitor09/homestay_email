﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for StudentManager
/// </summary>
public class StudentManager
{
    private Dictionary<string, EntityObject> stuHM;
    Quarter_MetaData QMD = new Quarter_MetaData();
    public StudentManager()
    {
        stuHM = LoadStudentHM();
    }
    public Dictionary<string, EntityObject> GetStuHM()
    {
        return stuHM;
    }
    private Dictionary<string, EntityObject> LoadStudentHM()
    {
        Dictionary<string, EntityObject> stuHM = new Dictionary<string, EntityObject>();
        Homestay hsInfo = new Homestay();
        DataTable dtStudents = hsInfo.GetHomestayStudents("Active", 0);

        foreach (DataRow dr in dtStudents.Rows)
        {
            //Create dictionary
            string applicantIDStr = dr["applicantID"].ToString();
            int applicantIDInt = Convert.ToInt32(dr["applicantID"].ToString());
            Dictionary<string, string[]> attributes = new Dictionary<string, string[]>();
            string sqlQuery = @"SELECT  *
                                FROM [CCSInternationalStudent].[dbo].[SchoolInformation]
                                WHERE applicantID =" + applicantIDStr;


            DataTable dt = hsInfo.RunGetQuery(sqlQuery);
            foreach (DataRow row in dt.Rows)
            {
                foreach (DataColumn column in dt.Columns)
                {
                    string columnName = column.ColumnName;
                    string columnData = row[column].ToString();
                    attributes[columnName] = new string[] { columnData };
                }
            }

            //Add pref
            sqlQuery = @"SELECT  *
                        FROM [CCSInternationalStudent].[dbo].[HomestayPreferenceSelections]
                        WHERE applicantID =" + applicantIDStr;
            DataTable dtPref = hsInfo.RunGetQuery(sqlQuery);
            foreach (DataRow row in dtPref.Rows)
            {
                attributes[row["preferenceID"].ToString()] = new string[] { row["preferenceID"].ToString() };
            }

            //Add dietary needs
            attributes = MatchingUtility.AddDiertaryNeedsAttributes(attributes);

            //Add attribute "smoker", "othersmoker"
            attributes = MatchingUtility.AddAttributes(attributes);

            //Add attribute "college" for stuyWhere
            attributes["college"] = attributes["studyWhere"];

            //Add Gender
            sqlQuery = @"SELECT  *
                        FROM [CCSInternationalStudent].[dbo].[ApplicantBasicInfo]
                        WHERE id =" + applicantIDStr;
            DataTable dtBasicInfo = hsInfo.RunGetQuery(sqlQuery);
           
            
           
            
            foreach (DataRow row in dtBasicInfo.Rows)
            {
                foreach (DataColumn column in dtBasicInfo.Columns)
                {
                    string columnName = column.ColumnName;
                    string columnData = row[column].ToString();
                    attributes[columnName] = new string[] { columnData };
                }
                string genderStr = row["Gender"].ToString();
                //Convert M to Male, F to Female, O to other
                if (genderStr.Equals("M"))
                {
                    genderStr = "Male";
                }
                else if (genderStr.Equals("F"))
                {
                    genderStr = "Female";
                }
                else if (genderStr.Equals("O"))
                {
                    genderStr = "Other";
                }
                attributes["gender"] = new string[] { genderStr };

                string strQuarterStartDate = dr["quarterStartDate"].ToString();
                DateTime dtFirstDayOfClass = new DateTime();
                if (strQuarterStartDate != "")
                {
                    dtFirstDayOfClass = Convert.ToDateTime(strQuarterStartDate);
                }
                else
                {
                    strQuarterStartDate = QMD.NextQtrFirstClass.ToShortDateString();
                    dtFirstDayOfClass = QMD.NextQtrFirstClass;
                }
                DateTime dtLatestDOB = dtFirstDayOfClass.AddYears(-18);
                DateTime dtDOB = Convert.ToDateTime(row["DOB"].ToString());
                bool blUnderAge = false;
                if (dtLatestDOB < dtDOB)
                {
                    blUnderAge = true;
                }
                else
                {
                    blUnderAge = false;
                }
                
                attributes["underAge"] = new string[] { blUnderAge.ToString() };
            }

            //Add homestayOption * homestayStatus from HomestayStudentInfo
            sqlQuery = @"SELECT  *
                        FROM [CCSInternationalStudent].[dbo].[HomestayStudentInfo]
                        WHERE applicantID =" + applicantIDStr;

            DataTable dtHomestayStudentInfo = hsInfo.RunGetQuery(sqlQuery);

            foreach (DataRow row in dtHomestayStudentInfo.Rows)
            {
                foreach (DataColumn column in dtHomestayStudentInfo.Columns)
                {
                    string columnName = column.ColumnName;
                    string columnData = row[column].ToString();
                    //attributes.Add(columnName, new string[] { columnData });
                    attributes[columnName] = new string[] { columnData };
                    string homestayOptionStr = row["homestayOption"].ToString().ToLower();
                    //Convert to yes/no/either for with food/without food/either
                    if (homestayOptionStr.Equals("with food"))
                    {
                        homestayOptionStr = "yes";
                    }
                    else if (homestayOptionStr.Equals("without food"))
                    {
                        homestayOptionStr = "no";
                    }
                    else
                    {
                        homestayOptionStr = "either";
                    }
                    attributes["homestayOption"] = new string[] { homestayOptionStr };
                    attributes["homestayStatus"] = new string[] { row["homestayStatus"].ToString().Trim() };
                }

            }
            if (attributes["underAge"].ToString().Equals("True"))
            {
                attributes["homestayOption2"] = new string[] { "Full-Minor" };
            }
            else
            {
                string homestayOption2 = "Adult";
                if (attributes["homestayOption"].ToString().Equals("no"))
                {
                    homestayOption2 += " without food";
                }
                else if(attributes["homestayOption"].ToString().Equals("yes"))
                {
                    homestayOption2 += " with food";
                }
                else
                {
                    homestayOption2 += " either";
                }
                attributes["homestayOption2"] = new string[] { homestayOption2 };
            }
            

            //Add hobbies
            attributes = MatchingUtility.AddHobbiesAttributes(attributes);

            //Add interaction
            attributes = MatchingUtility.AddInteractionAttributes(attributes);

            //Add homeEnvironment
            attributes = MatchingUtility.AddHomeEnvironmentAttributes(attributes);

            //Add childrenPref
            attributes = MatchingUtility.AddChildrenPrefAttributes(attributes);

            //Add placed family info
            sqlQuery = @"SELECT *
                        FROM [CCSInternationalStudent].[dbo].[HomestayPlacement]
                        WHERE applicantID =" + applicantIDStr;

            DataTable dtStudentPlacement = hsInfo.RunGetQuery(sqlQuery);
            attributes["familyID"] = new string[] { "NA" };
            attributes["homeName"] = new string[] { "NA" };
            attributes["roomNumber"] = new string[] { "NA" };
            if (dtStudentPlacement != null && dtStudentPlacement.Rows.Count > 0)
            {
                //attributes["familyPlaced"] = new string[] { dtStudentPlacement.Rows[0]["familyID"].ToString() };
                attributes["familyID"] = new string[] { dtStudentPlacement.Rows[0]["familyID"].ToString() };
                attributes["homeName"] = new string[] { dtStudentPlacement.Rows[0]["placementName"].ToString() };
                attributes["roomNumber"] = new string[] { dtStudentPlacement.Rows[0]["roomNumber"].ToString() };
            }


            EntityObject entityObject = new EntityObject() { Name = applicantIDStr, Attributes = attributes };
            stuHM.Add(applicantIDStr, entityObject);
        }
        return stuHM;
    }
}