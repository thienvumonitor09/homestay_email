﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Homestay_Admin_Reports_ListActive : System.Web.UI.Page
{
    string strListType = "";
    string strResultMsg = "";
    DataTable dtFamilies;
    DataTable dtStudents;
    DataTable dtStatus;
    DataTable dtCountries;
    protected void Page_Load(object sender, EventArgs e)
    {
        /*
        Homestay hsInfo = new Homestay();

        strListType = Request.QueryString["type"];
        if (strListType == "F")
        {
            litType.Text = "Families";
            divFamilies.Visible = true;
            divStudents.Visible = false;
            dtFamilies = hsInfo.GetHomestayFamilyInfo(0, "", "DDLactive");
        }
        else
        {
            litType.Text = "Students";
            divStudents.Visible = true;
            divFamilies.Visible = false;
            dtStatus = hsInfo.GetHomestayStatusValues("Student");
            dtStudents = hsInfo.GetHomestayStudents("ReportActive", 0);
            dtCountries = hsInfo.GetCountries();
        }

        if (strListType == "F" && dtFamilies != null)
        {
            if (dtFamilies.Rows.Count > 0)
            {
                litList.Text = "";
                foreach (DataRow dr in dtFamilies.Rows)
                {
                    litList.Text += "<li>" + (dr["homeName"].ToString() + "</li>");
                  //  Response.Write(dr["id"].ToString() + "<br />");
                }
            }
            else { strResultMsg += "Error: dtFamilies has no rows.<br />"; }
        }
        else { strResultMsg += "Error: dtFamilies is null.<br />"; }
        DateTime bDate = DateTime.Now;
        string strShortDate = "";
        if(strListType == "S" && dtStudents != null)
        {
            if(dtStatus.Rows.Count > 0)
            {
                ddlStatus.Items.Clear();
                foreach (DataRow drStatus in dtStatus.Rows)
                {
                    ddlStatus.Items.Add(new ListItem(drStatus["statusDescription"].ToString()));
                }
            } else
            {
                ddlStatus.Items.Add("Select One");
            }
            if (dtCountries.Rows.Count > 0)
            {
                ddlCountries.Items.Clear();
                ddlCountries.Items.Add(new ListItem("Select One", ""));
                foreach (DataRow drCountry in dtCountries.Rows)
                {
                    ddlCountries.Items.Add(new ListItem(drCountry["countryName"].ToString()));
                }
            } else
            {
                ddlCountries.Items.Add(new ListItem("Select One"));
            }
            if (dtStudents.Rows.Count > 0)
            {
                litTable.Text = "";
                foreach (DataRow dr in dtStudents.Rows)
                {
                    if (dr["DOB"].ToString().Trim() != "")
                    {
                        bDate = Convert.ToDateTime(dr["DOB"].ToString().Trim());
                        strShortDate = bDate.ToShortDateString();
                    }
                    else
                        strShortDate = ""; ;
                    litTable.Text += "<tr><td>" + dr["familyName"].ToString() + ", " + dr["firstName"].ToString() + " " + dr["middleNames"].ToString() + "</td><td style='align:left;'>" + dr["homestayStatus"].ToString() + "</td><td style='align:left;'>" + dr["Gender"].ToString() + "</td><td style='align:left;'>" + dr["SID"].ToString() + "</td><td style='align:left;'>" + dr["studyWhere"].ToString() + "</td><td style='align:left;'>22</td><td style='align:left;'>" + strShortDate + "</td><td style='align:left;'>" + dr["countryOfCitizenship"].ToString() + "</td><td style='align:left;'>" + dr["Email"].ToString() + "</td><td style='align:left;'>" + dr["Phone"].ToString() + "</td></tr>";
                }
            }
            else { strResultMsg += "Error: dtStudents has no rows.<br />"; }
        }
        else { strResultMsg += "Error: dtStudents is null.<br />"; }
        */
    }

    protected void linkBtnHome_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Homestay/Admin/Default.aspx");
    }

    protected void linkBtnReport_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Homestay/Admin/Reports/ReportsHome.aspx");
    }
}