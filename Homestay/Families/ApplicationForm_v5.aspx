﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ApplicationForm_v5.aspx.cs" Inherits="Homestay_Families_ApplicationForm_v5" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <title>CCS Homestay Program - Family Application</title>
   <!-- <link id="Link1" href="/_css/ccs.css" rel="stylesheet"  type="text/css" /> -->
    <link href="/Homestay/_css/Homestay.css" rel="stylesheet" type="text/css" />  
    
    <meta http-equiv="X-UA-Compatible" content="IE=9" />  
    <script type="text/javascript" src="/_js/jquery-1.9.1.min.js"></script>      
    <script type="text/javascript" src="/_js/pushToHttps.js"></script>   
    <style>
       

        .occupancyClass tr{
            height:40px;
        }
        .occupancyClass tr td:nth-child(2){
            padding-bottom:40px;
        }

    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div id="divSelector" runat="server" style="padding-bottom:10px;">
            <h3>Application Form - Host Families</h3>
            <fieldset>
            <div>
                <div id="divResetFields" runat="server" style="float:left;margin-right:30px;">
                    <p>
                        <asp:Button ID="btnCmdResetFields" runat="server" Text="Reset Fields" />
                        <asp:Button ID="btnGenerate" runat="server" Text="Export to PDF" /> 
                    </p>
                </div>
        
                <div id="div1" runat="server" style="float:left;">
                    <p>
                        <asp:Button ID="btnCmdReturnAdminHome" runat="server" Text="Return to Admin Home Page"  />&nbsp&nbsp;
                    </p>
                </div>
            </div>
            <br />
    

            <h4>Select a Homestay Family</h4>
            <div class="divError">* indicates required field</div>
            <asp:DropDownList ID="ddlFamilies" AutoPostBack="true"  runat="server" OnSelectedIndexChanged="ddlFamilies_SelectedIndexChanged">

            </asp:DropDownList>
            </fieldset>
        </div>
        <!-- Start: Wizard -->
        <asp:Wizard ID="Wizard1" runat="server" 
                HeaderText="ActiveStepChanged Example"
                FinishCompleteButtonText="Save and Continue" 
                SideBarStyle-Width="200px" SideBarStyle-VerticalAlign="Top" 
                SideBarStyle-BackColor="#CCCCCC" 
                NavigationButtonStyle-BackColor="White" 
                NavigationButtonStyle-BorderColor="#CC9966" NavigationButtonStyle-BorderStyle="Solid" NavigationButtonStyle-BorderWidth="1px" NavigationButtonStyle-ForeColor="#990000" SideBarStyle-Height="500px">
            
            
            
                <WizardSteps>
                    <asp:WizardStep runat="server" Title="Family Home">
                        <fieldset id="PersonalInfo" runat="server">
                        <legend>Family Home</legend>
                            <div id="divHomeName" runat="server">
                                <p>
                                    <label for="homeName">Home Name</label><br />
                                    <asp:TextBox ID="homeName" runat="server" Width="300px" MaxLength="20"></asp:TextBox>
                                </p>
                                <p>
                                    <label for="rdoIsActive">Homestay Program Status</label><br />
                                    <asp:RadioButtonList ID="rdoIsActive" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
                                        <asp:ListItem Value="1">Active&nbsp;&nbsp;</asp:ListItem>
                                        <asp:ListItem Value="0">Inactive&nbsp;&nbsp;</asp:ListItem>
                                    </asp:RadioButtonList>
                                </p>
                                <p>
                                    <label for="userName">Account Username for Primary Contact </label><br />
                                    <asp:TextBox ID="userName" runat="server" Width="300px" MaxLength="20"></asp:TextBox>
                                </p>
                                <p>
                                    <label for="userName">Family Status</label><br />
                                    <asp:TextBox ID="familyStatus" runat="server" Width="300px" MaxLength="50"></asp:TextBox>
                                </p>
                            </div>
                            <div style="width:100%; float:left">
                                <div id="eStreetAddress" class="divError" runat="server"></div>
                                <p>
                                    <label for="streetAddress">Street Address</label><span style="color:#DD0000;font-weight:600;"> * </span><br />
                                    <asp:TextBox ID="streetAddress" runat="server" Width="300px" MaxLength="100"></asp:TextBox>
                                </p>
                                <div id="eCity" class="divError" runat="server"></div>
                                <p>        
                                    <label for="city">City</label><span style="color:#DD0000;font-weight:600;"> * </span><br />
                                    <asp:TextBox ID="city" runat="server" Width="300px" MaxLength="20"></asp:TextBox>
                                </p>
                                <div id="eState" class="divError" runat="server"></div>
                                <p>
                                    <label for="state">State</label><span style="color:#DD0000;font-weight:600;"> * </span><br />
                                    <asp:TextBox ID="state" runat="server" Width="40px" MaxLength="2"></asp:TextBox>
                                </p>
                                <div id="eZip" class="divError" runat="server"></div>
                                <p>
                                    <label for="ZIP">ZIP Code</label><span style="color:#DD0000;font-weight:600;"> * </span><br />
                                    <asp:TextBox ID="ZIP" runat="server" Width="100px" MaxLength="10"></asp:TextBox>
                                </p>

                            </div>

                            <div id="divTravelTime" runat="server" visible="true"> 
                                <div id="eTravelErr" class="divError" runat="server"></div>
                                <label for="state">Walking distance to bus stop</label><br />
                                <div style="padding-top: 2px; width:15%; float:left"><asp:TextBox ID="walkingBusStopMinutes" runat="server" Width="50px" MaxLength="5" onblur="convertNum('walkingBusStopMinutes');"></asp:TextBox> Minutes</div>     
                                <div style="padding-top: 2px ; width:15%; float:left"><asp:TextBox ID="walkingBusStopMiles" runat="server" Width="50px" MaxLength="5" onblur="convertNum('walkingBusStopMiles');"></asp:TextBox> Miles</div>
                                <div style="padding-top: 2px; float:left"><asp:TextBox ID="walkingBusStopBlocks" runat="server" Width="50px" MaxLength="5" onblur="convertNum('walkingBusStopBlocks');"></asp:TextBox> Blocks</div>       

        
        
                              <div style="width:100%;float:left">
                                   <div style="width:15%;float:left">
                                    <label for="state">Bus travel to SCC</label><br />
                                <asp:TextBox ID="busMinutesSCC" runat="server" Width="50px" MaxLength="5" onblur="convertNum('busMinutesSCC');"></asp:TextBox> Minutes
                                </div>
                                <div style="width:80%;float:left">
                                    <label for="state">Bus travel to SFCC</label><br />
                                <asp:TextBox ID="busMinutesSFCC" runat="server" Width="50px" MaxLength="5" onblur="convertNum('busMinutesSFCC')"></asp:TextBox> Minutes
                                </div>

             
                                  <asp:Button ID="btnCmdUpdateTravel" runat="server" Text="Update Travel Information"  />
                              </div>
                             <div style="width:100%;float:left">
                                <label for="state">College(s) family qualifies for</label><br />
                                <asp:RadioButtonList ID="collegeQualified" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Value="SCC" Text="SCC"></asp:ListItem>
                                    <asp:ListItem Value="SFCC" Text="SFCC"></asp:ListItem>
                                    <asp:ListItem Value="Either" Text="Either College"></asp:ListItem>
                                </asp:RadioButtonList>
                               </div>
           
                            <div id="eWsp" class="divError" runat="server"></div>
                            <div style="width:100%;float:left">
             <p>
        <label for="wspBackgroundCheck">WSP Background Check date <i>(format mm/dd/yyyy)</i>:</label><br />
            <asp:TextBox ID="wspBackgroundCheck" runat="server" /> 
            </p>
             </div>
    
    
                            <h5>Notes/Issues</h5>
   
                            <asp:GridView ID="gridView" AutoGenerateColumns="False" 
               emptydatatext="N/A" 
               runat="server">
        <HeaderStyle BackColor="#448BC1" Font-Bold="False" ForeColor="White" />
        <AlternatingRowStyle BackColor="AliceBlue"></AlternatingRowStyle>
               <Columns>
                   <asp:BoundField DataField="createDate" HeaderText="Date Created" />
                   <asp:BoundField DataField="studentName" HeaderText="Student" ItemStyle-Width="200px"/>
                 <asp:BoundField DataField="subject" HeaderText="Subject" ItemStyle-Width="200px"/>
                   <asp:BoundField DataField="note" HeaderText="Note" ItemStyle-Width="400px"/>
                   <asp:BoundField DataField="actionNeeded" HeaderText="Action Needed" />
                   <asp:BoundField DataField="actionCompleted" HeaderText="Action Done" />
                   <asp:BoundField DataField="lastModifiedDate" HeaderText="Date Editied" />
            </Columns>
           </asp:GridView>
        
        
        
                            </div> <!--End divTravelTime --> 
    
                    <br />
                        <div id="ePhone" class="divError" runat="server"></div>
                        <label for="landLinePhone">Landline Phone/Primary Contact Phone</label><br />
                        <asp:TextBox ID="landLinePhone" runat="server" Width="300px" MaxLength="20"></asp:TextBox>
    
    
       
                        </fieldset>
                     </asp:WizardStep>
                    <asp:WizardStep ID="WizardStep1" runat="server" Title="Host Family Contacts">
                         <fieldset id="Contacts" runat="server">
<legend>Host Family Contacts</legend>
<p>Please provide the information below for yourself and anyone 18 years or older who resides in your home.</p>
    
<div class = "accordionDiv">
    <h3 class="accordion accordion-toggle">Primary Host Family Contact</h3>
    <div class="panel accordion-content">
        <h4>Full Legal Name</h4>
        <div id="eFamilyName" class="divError" runat="server"></div>
        <p>
            <span style="color:#DD0000;font-weight:600;"> * </span><label for="familyName">Family Name</label><br />
            <asp:TextBox Width="300px" ID="familyName" runat="server" class="required" MaxLength="30"></asp:TextBox>
        </p>
 
        <div id="eFirstName" class="divError" runat="server"></div>
        <p>
            <span style="color:#DD0000;font-weight:600;"> * </span><label for="firstName">First Name</label><br />
            <asp:TextBox ID="firstName" runat="server" Width="300px" MaxLength="20"></asp:TextBox>
        </p>
       <p>
           <label for="middleName">Middle Name (optional)</label><br />
           <asp:TextBox ID="middleName" runat="server" Width="300px" MaxLength="20"></asp:TextBox>
       </p>
        <div id="eGender" class="divError" runat="server"></div>
       <p>
           <span style="color:#DD0000;font-weight:600;"> * </span><label for="rdoGender">Gender</label><br />
            <asp:RadioButtonList ID="rdoGender" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
                <asp:ListItem Value="M">Male&nbsp;&nbsp;</asp:ListItem>
                <asp:ListItem Value="F">Female&nbsp;&nbsp;</asp:ListItem>
                <asp:ListItem Value="O">Other</asp:ListItem>
            </asp:RadioButtonList>
       </p>
           
        <h4>Personal Information</h4>

        <div id="eDOB" class="divError" runat="server"></div>
        <p>
             <span style="color:#DD0000;font-weight:600;"> * </span><label for="DOB">DOB</label>&nbsp;(mm/dd/yyyy)<br />
            <asp:TextBox ID="DOB" runat="server" Width="300px" MaxLength="30"></asp:TextBox>
        </p>
        
        <div id="eLicense" class="divError" runat="server"></div>
        <p>
            <span style="color:#DD0000;font-weight:600;"> * </span><label for="driversLicenseNumber">Driver's License Number</label><br />
            <asp:TextBox ID="driversLicenseNumber" runat="server" Width="300px" MaxLength="20"></asp:TextBox> 

        </p>
        
        <div id="eOccupation" class="divError" runat="server"></div>
        <p>
            <span style="color:#DD0000;font-weight:600;"> * </span><label for="occupation">Occupation/Retired</label><br />
            <asp:TextBox ID="occupation" runat="server" Width="300px" MaxLength="20"></asp:TextBox>
        </p>
       
        
        <p><i><span style="color:#dd0000">Please enter at least one phone number - either cell phone or work phone</span></i>.</p>
        <p>
            <span style="color:#DD0000;font-weight:600;"> * </span><label for="cellPhone">Cell Phone</label><br />
            <asp:TextBox ID="cellPhone" runat="server" Width="300px" MaxLength="20"></asp:TextBox>
        </p>
        <p>
            <label for="workPhone">Work Phone</label><br />
            <asp:TextBox ID="workPhone" runat="server" Width="300px" MaxLength="20"></asp:TextBox>
        </p>
       
        <div id="eEmail" class="divError" runat="server"></div>
        <p>
            <span style="color:#DD0000;font-weight:600;"> * </span><label for="email">Email</label><br />
             <asp:TextBox ID="email" runat="server" Width="300px" MaxLength="50"></asp:TextBox>
            <asp:HiddenField ID="hdnPrimaryID" runat="server" />
        </p>
     
                
   </div>
   <h3 class="accordion accordion-toggle">Secondary Host Family Contact</h3>
   <div class="panel accordion-content">
        <h4>Full Legal Name</h4>
       <p>
           <label for="familyName2">Family Name</label> <br />
            <asp:TextBox Width="300px" ID="familyName2" runat="server" class="required" MaxLength="30"></asp:TextBox>
       </p>

       <p>
           <label for="firstName2">First Name</label><br />
            <asp:TextBox ID="firstName2" runat="server" Width="300px" MaxLength="20"></asp:TextBox>
       </p>

       <p>
           <label for="middleName2">Middle Name</label><br />
           <asp:TextBox ID="middleName2" runat="server" Width="300px" MaxLength="20"></asp:TextBox>
       </p>
    <p>
        <label for="rdoGender2">Gender</label><br />
        <asp:RadioButtonList ID="rdoGender2" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
                <asp:ListItem Value="M">Male&nbsp;&nbsp;</asp:ListItem>
                <asp:ListItem Value="F">Female&nbsp;&nbsp;</asp:ListItem>
                <asp:ListItem Value="O">Other</asp:ListItem>
            </asp:RadioButtonList>
    </p>
    
       
        <h4>Personal Information</h4>
       <div id="eDOB2" class="divError" runat="server"></div> 
       <p>
           <label for="DOB2">DOB</label>(mm/dd/yyyy)<br />
           <asp:TextBox ID="DOB2" runat="server" Width="300px" MaxLength="30"></asp:TextBox>
       </p>
    <p>
        <label for="driversLicenseNumber2">Driver's License Number</label><br />
        <asp:TextBox ID="driversLicenseNumber2" runat="server" Width="300px" MaxLength="20"></asp:TextBox>
    </p>

 <p>
     <label for="occupation2">Occupation/Retired</label><br />
     <asp:TextBox ID="occupation2" runat="server" Width="300px" MaxLength="20"></asp:TextBox>
 </p>
      <p>
          <label for="cellPhone2">Cell Phone</label><br />
          <asp:TextBox ID="cellPhone2" runat="server" Width="300px" MaxLength="20"></asp:TextBox>
      </p>
     <p>
          <label for="workPhone2">Work Phone</label> <br />
         <asp:TextBox ID="workPhone2" runat="server" Width="300px" MaxLength="20"></asp:TextBox>
     </p>
<p>
    <label for="email2">Email</label><br />
    <asp:TextBox ID="email2" runat="server" Width="300px" MaxLength="50"></asp:TextBox>
            <asp:HiddenField ID="hdnSecondaryID" runat="server" />
</p>
     
        
    </div>
    <h3 class="accordion accordion-toggle">Other Adult Occupant 1</h3>
    <div class="panel accordion-content">
        <h4>Full Legal Name</h4>
        <p>
            <label for="familyName3">Family Name</label><br />
            <asp:TextBox Width="300px" ID="familyName3" runat="server" class="required" MaxLength="30"></asp:TextBox>
        </p>
    <p>
         <label for="firstName3">First Name</label><br />
        <asp:TextBox ID="firstName3" runat="server" Width="300px" MaxLength="20"></asp:TextBox>
    </p>
    <p>
        <label for="middleName3">Middle Name</label><br />
        <asp:TextBox ID="middleName3" runat="server" Width="300px" MaxLength="20"></asp:TextBox>
    </p>
        <p>
            <label for="rdoGender3">Gender</label><br />
             <asp:RadioButtonList ID="rdoGender3" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
                <asp:ListItem Value="M">Male&nbsp;&nbsp;</asp:ListItem>
                <asp:ListItem Value="F">Female&nbsp;&nbsp;</asp:ListItem>
                <asp:ListItem Value="O">Other</asp:ListItem>
            </asp:RadioButtonList>
        </p>
    
       
        <h4>Personal Information</h4>
        <div id="eDOB3" class="divError" runat="server"></div>
        <p>
            <label for="DOB3">Date of Birth</label>(mm/dd/yyyy)<br />
            <asp:TextBox ID="DOB3" runat="server" Width="300px" MaxLength="30"></asp:TextBox>
        </p>
     
        <p>
            <label for="driversLicenseNumber3">Driver's License Number</label><br />
            <asp:TextBox ID="driversLicenseNumber3" runat="server" Width="300px" MaxLength="20"></asp:TextBox>
        </p>

  <p>
       <label for="occupation3">Occupation/Retired</label><br />
      <asp:TextBox ID="occupation3" runat="server" Width="300px" MaxLength="20"></asp:TextBox>
  </p>
       
      <p>
          <label for="cellPhone3">Cell Phone</label><br />
           <asp:TextBox ID="cellPhone3" runat="server" Width="300px" MaxLength="20"></asp:TextBox>
      </p>
      <p>
          <label for="workPhone3">Work Phone</label><br />
          <asp:TextBox ID="workPhone3" runat="server" Width="300px" MaxLength="20"></asp:TextBox>
      </p>
   <p>
        <label for="email3">Email</label><br />
        <asp:TextBox ID="email3" runat="server" Width="300px" MaxLength="50"></asp:TextBox>
        <asp:HiddenField ID="hdnAdult1ID" runat="server" />
   </p>

    </div>

    <h3 class="accordion accordion-toggle">Other Adult Occupant 2</h3>
    <div class="panel accordion-content">
        <h4>Full Legal Name</h4>
        <p>
            <label for="familyName4">Family Name</label><br />
            <asp:TextBox Width="300px" ID="familyName4" runat="server" class="required" MaxLength="30"></asp:TextBox>
        </p>
      <p>
          <label for="firstName4">First Name</label><br />
          <asp:TextBox ID="firstName4" runat="server" Width="300px" MaxLength="20"></asp:TextBox>
      </p>
 <p>
     <label for="middleName4">Middle Name</label><br />
     <asp:TextBox ID="middleName4" runat="server" Width="300px" MaxLength="20"></asp:TextBox>
 </p>
   <p>
       <label for="rdoGender4">Gender</label><br />
       <asp:RadioButtonList ID="rdoGender4" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
                <asp:ListItem Value="M">Male&nbsp;&nbsp;</asp:ListItem>
                <asp:ListItem Value="F">Female&nbsp;&nbsp;</asp:ListItem>
                <asp:ListItem Value="O">Other</asp:ListItem>
            </asp:RadioButtonList>
   </p>
      
      
        <h4>Personal Information</h4>
        <div id="eDOB4" class="divError" runat="server"></div>
        <p>
            <label for="DOB4">Date of Birth</label>(mm/dd/yyyy)<br />
            <asp:TextBox ID="DOB4" runat="server" Width="300px" MaxLength="30"></asp:TextBox>
        </p>
        <p>
            <label for="driversLicenseNumber4">Driver's License Number</label><br />
            <asp:TextBox ID="driversLicenseNumber4" runat="server" Width="300px" MaxLength="20"></asp:TextBox>
        </p>
         <p>
             <label for="occupation4">Occupation/Retired</label><br />
             <asp:TextBox ID="occupation4" runat="server" Width="300px" MaxLength="20"></asp:TextBox>
         </p>
      <p>
          <label for="cellPhone4">Cell Phone</label><br />
          <asp:TextBox ID="cellPhone4" runat="server" Width="300px" MaxLength="20"></asp:TextBox>
      </p>
    <p>
        <label for="workPhone4">Work Phone</label><br />
        <asp:TextBox ID="workPhone4" runat="server" Width="300px" MaxLength="20"></asp:TextBox>
    </p>
     <p>
         <label for="email4">Email</label><br />
         <asp:TextBox ID="email4" runat="server" Width="300px" MaxLength="50"></asp:TextBox>
            <asp:HiddenField ID="hdnAdult2ID" runat="server" />
     </p>

          
    </div>
    <h3 class="accordion accordion-toggle">Minor Child 1</h3>
    <div class="panel accordion-content">
        <div style="padding-bottom:5px;width:50%;">
            <div style="width:25%;float:left">
                <label for="familyName5">Family Name</label>
            </div>
            <div>
                <asp:TextBox ID="familyName5" runat="server" Width="300px" MaxLength="30"></asp:TextBox>
            </div>
        </div>
        <div style="padding-bottom:5px;width:50%;">
            <div style="width:25%;float:left">
                <label for="firstName5">First Name</label>
            </div>
            <div>
                <asp:TextBox ID="firstName5" runat="server" Width="300px" MaxLength="30"></asp:TextBox>
            </div>
        </div>
    
   
        <div id="eDOB5" class="divError" runat="server"></div>
        <div style="padding-bottom:5px;width:50%;">
            <div style="width:25%;float:left">
                <label for="DOB5">Date Of Birth</label>
            </div>
            <div>
                <asp:TextBox ID="DOB5" runat="server" Width="300px"  MaxLength="30"></asp:TextBox>
            </div>
        </div>
        <div style="padding-bottom:5px;width:50%;">
            <div style="width:25%;float:left">
                <label for="rdoGender5">Gender</label>
            </div>
            <div>
                <asp:RadioButtonList ID="rdoGender5" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
            <asp:ListItem Value="M">Male&nbsp;&nbsp;</asp:ListItem>
            <asp:ListItem Value="F">Female&nbsp;&nbsp;</asp:ListItem>
            <asp:ListItem Value="O">Other</asp:ListItem>
        </asp:RadioButtonList>
        <asp:HiddenField ID="hdnChild1ID" runat="server" />
            </div>
        </div>
   </div>
    <h3 class="accordion accordion-toggle">Minor Child 2</h3>
    <div class="panel accordion-content">
         <div style="padding-bottom:5px;width:50%;">
            <div style="width:25%;float:left">
                <label for="familyName6">Family Name</label>
            </div>
            <div>
                <asp:TextBox ID="familyName6" runat="server" Width="300px" MaxLength="30"></asp:TextBox>
            </div>
        </div>
        <div style="padding-bottom:5px;width:50%;">
            <div style="width:25%;float:left">
                 <label for="firstName6">First Name</label>
            </div>
            <div>
                <asp:TextBox ID="firstName6" runat="server" Width="300px"  MaxLength="30"></asp:TextBox>
            </div>
        </div>

        <div id="eDOB6" class="divError" runat="server"></div>
        <div style="padding-bottom:5px;width:50%;">
            <div style="width:25%;float:left">
                 <label for="DOB6">Date Of Birth</label>
            </div>
            <div>
                <asp:TextBox ID="DOB6" runat="server" Width="300px"  MaxLength="30"></asp:TextBox>
            </div>
        </div>
        <div style="padding-bottom:5px;width:50%;">
            <div style="width:25%;float:left">
                 <label for="rdoGender6">Gender</label>
            </div>
            <div>
                 <asp:RadioButtonList ID="rdoGender6" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
                    <asp:ListItem Value="M">Male&nbsp;&nbsp;</asp:ListItem>
                    <asp:ListItem Value="F">Female&nbsp;&nbsp;</asp:ListItem>
                    <asp:ListItem Value="O">Other</asp:ListItem>
                </asp:RadioButtonList>
                <asp:HiddenField ID="hdnChild2ID" runat="server" />
            </div>
        </div>
        
          
   
   </div>
    <h3 class="accordion accordion-toggle">Minor Child 3</h3>
    <div class="panel accordion-content">
        <div style="padding-bottom:5px;width:50%;">
            <div style="width:25%;float:left">
                 <label for="familyName7">Family Name</label>
            </div>
            <div>
                <asp:TextBox ID="familyName7" runat="server" Width="300px" MaxLength="30"></asp:TextBox>
            </div>
        </div>

        <div style="padding-bottom:5px;width:50%;">
            <div style="width:25%;float:left">
                 <label for="firstName7">First Name</label>
            </div>
            <div>
                <asp:TextBox ID="firstName7" runat="server" Width="300px" MaxLength="30"></asp:TextBox>
            </div>
        </div>
    
   
        <div id="eDOB7" class="divError" runat="server"></div>
        <div style="padding-bottom:5px;width:50%;">
            <div style="width:25%;float:left">
                 <label for="DOB7">Date Of Birth</label>
            </div>
            <div>
                <asp:TextBox ID="DOB7" runat="server" Width="300px"  MaxLength="30"></asp:TextBox>
            </div>
        </div>

        <div style="padding-bottom:5px;width:50%;">
            <div style="width:25%;float:left">
                 <label for="rdoGender7">Gender</label>
            </div>
            <div>
                <asp:RadioButtonList ID="rdoGender7" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
            <asp:ListItem Value="M">Male&nbsp;&nbsp;</asp:ListItem>
            <asp:ListItem Value="F">Female&nbsp;&nbsp;</asp:ListItem>
            <asp:ListItem Value="O">Other</asp:ListItem>
        </asp:RadioButtonList>
        <asp:HiddenField ID="hdnChild3ID" runat="server" />
            </div>
        </div>
          
    
   </div>
    <h3 class="accordion accordion-toggle">Minor Child 4</h3>
    <div class="panel accordion-content">
        <div style="padding-bottom:5px;width:50%;">
            <div style="width:25%;float:left">
                 <label for="familyName8">Family Name</label>
            </div>
            <div>
                <asp:TextBox ID="familyName8" runat="server" Width="300px" MaxLength="30"></asp:TextBox>
            </div>
        </div>
        <div style="padding-bottom:5px;width:50%;">
            <div style="width:25%;float:left">
                 <label for="firstName8">First Name</label>
            </div>
            <div>
                <asp:TextBox ID="firstName8" runat="server" Width="300px"  MaxLength="30"></asp:TextBox>
            </div>
        </div>
    
    <div id="eDOB8" class="divError" runat="server"></div>
        <div style="padding-bottom:5px;width:50%;">
            <div style="width:25%;float:left">
                 <label for="DOB8">Date Of Birth</label>
            </div>
            <div>
                <asp:TextBox ID="DOB8" runat="server" Width="300px"  MaxLength="30"></asp:TextBox>
            </div>
        </div>

         <div style="padding-bottom:5px;width:50%;">
            <div style="width:25%;float:left">
                 <label for="rdoGender8">Gender</label>
            </div>
            <div>
                <asp:RadioButtonList ID="rdoGender8" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
            <asp:ListItem Value="M">Male&nbsp;&nbsp;</asp:ListItem>
            <asp:ListItem Value="F">Female&nbsp;&nbsp;</asp:ListItem>
            <asp:ListItem Value="O">Other</asp:ListItem>
        </asp:RadioButtonList>
        <asp:HiddenField ID="hdnChild4ID" runat="server" />
            </div>
        </div>
          
    
   </div>
</div> 
    
</fieldset>
                         </asp:WizardStep>

                </WizardSteps>
            

          </asp:Wizard>
        <!-- End: Wizard -->
    </form>
</body>
</html>
<script type="text/javascript">
    $(document).ready(function ($) {
         var acc = document.getElementsByClassName("accordion");
        var i;
        for (i = 0; i < acc.length; i++) {
            acc[i].addEventListener("click", function () {
                this.classList.toggle("active");
            });
        }
        $('.accordionDiv').find('.accordion-toggle').each(function () {
            
            //$(this).next().slideToggle('fast');
            
            //console.log($(this).next().find("input[type=radio]:checked").first().val());
            //console.log($(this).next().find("input[type=text]").first().val());
            //console.log("-----");
            /*On page load, all accordions are closed,check for first input
                open accordion if there is any check value or text entered in textbox
            */
            if (getUrlParameter("ID") == '' || getUrlParameter("ID") == '0') {
                 //$(this).next().slideToggle('fast');
                if ($(this).next().find("input[type=text]").first().val()
                        || $(this).next().find("input[type=radio]:checked").first().val() ) {
                    $(this).next().slideToggle('fast');
                }
            } else {
                if ($(this).next().find("input[type=text]").first().val()
                        || $(this).next().find("input[type=radio]:checked").first().val() ) {
                    $(this).next().slideToggle('fast');
                }
            }
            
            
        });
        $('.accordionDiv').find('.accordion-toggle').click(function () {
            //Expand or collapse this panel
            $(this).next().slideToggle('fast');
            //Hide the other panels
            //$(".accordion-content").not($(this).next()).slideUp('fast');
        });
    });

    function getUrlParameter(name) {
        name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
        var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
        var results = regex.exec(location.search);
        return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
    };
</script>
