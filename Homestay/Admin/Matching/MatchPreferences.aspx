﻿<%@ Page Title="Matching" Language="C#" MasterPageFile="~/CCSApps.master" AutoEventWireup="true" CodeFile="MatchPreferences.aspx.cs" Inherits="Homestay_Admin_MatchPreferences" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <link href = "https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel = "stylesheet">
      <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
     <link href="/Homestay/_css/Matching.css" rel="stylesheet" type="text/css" /> 
 
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="bannerH1Text" Runat="Server">
    Match Preferenes
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">
     <asp:Button ID="btnReset" runat="server" Text="Reset" OnClick="btnReset_Click" />
    
     <asp:Button ID="btnAdminPage" runat="server" Text="Return to Admin Page" OnClick="btnAdminPage_Click"  />
        <asp:Button ID="btnEditPlacement" runat="server" Text="Go to Placement Management Page" OnClick="btnPlacementPage_Click"  />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <br/><i>* required</i>
            <h3>Students</h3>
            <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
            <b>Select a student</b><br />
            <asp:DropDownList ID="ddlStudents" runat="server" AppendDataBoundItems="true" >

            </asp:DropDownList> *
             
            <br /><br />
            <b>Match Criteria</b>
            <div style="width:100%;float:left; margin-bottom:10px; ">
                <div style="float:left;width:200px;">
                    <label for="rdoGender"><strong>Gender</strong></label>
                    <asp:RadioButtonList ID="rdoGender" RepeatDirection="Horizontal" runat="server">
                        <asp:ListItem Value="Include" Selected="True"></asp:ListItem>
                        <asp:ListItem Value="Omit"></asp:ListItem>
                    </asp:RadioButtonList>
                </div>
                <div style="float:left;width:200px;">
                     <label for="rdoHomestay"><strong>Homestay Preference</strong> </label>
                    <asp:RadioButtonList ID="rdoHomestay" RepeatDirection="Horizontal" runat="server">
                        <asp:ListItem Value="Include" Selected="True"></asp:ListItem>
                        <asp:ListItem Value="Omit"></asp:ListItem>
                    </asp:RadioButtonList>
                </div>
                <div style="float:left;width:300px;">
                     <label for="rdoSmoking"><strong>Smoking Habits</strong></label>
                    <asp:RadioButtonList ID="rdoSmoking" RepeatDirection="Horizontal" runat="server">
                        <asp:ListItem Value="Include" Selected="True"></asp:ListItem>
                        <asp:ListItem Value="Omit"></asp:ListItem>
                    </asp:RadioButtonList>
                </div>
            </div>
           
           <br />
            
            <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" OnClientClick="return ValidateSelectStudent();"/>
            <!--Start:  studentStatusID-->
            <div id="studentStatusID" runat="server" >
                <b>Current Status</b>
                 <div class="tooltip"><i class="fas fa-info-circle"></i>
                    <span class="tooltiptext">
                        The Current Status will display all the placements for the selected student. <br />
                    </span>
                </div>
                <br />
                <asp:Literal ID="litCurrStudentStatus" runat="server" Text=""></asp:Literal>
                <asp:GridView ID="GvStudentStatus" AutoGenerateColumns="False" 
                       Width="100%" 
                        cellpadding="5"
                        cellspacing="5"
                       runat="server">
                    <HeaderStyle BackColor="#448BC1" Font-Bold="False" ForeColor="White" />
                    <AlternatingRowStyle BackColor="AliceBlue"></AlternatingRowStyle>
                           <Columns>
                               <asp:BoundField DataField="homeName" HeaderText="Home name"  ItemStyle-Width="20%"  />
                               <asp:BoundField DataField="placementType" HeaderText="Placement Type"  ItemStyle-Width="30%" />
                               <asp:BoundField DataField="roomNumber" HeaderText="Room Number" ItemStyle-Width="10%"/>
                               <asp:BoundField DataField="effectiveQuarter" HeaderText="Effective Date" ItemStyle-Width="20%"/>
                               <asp:BoundField DataField="endQuarter" HeaderText="End Date" ItemStyle-Width="20%"/>
                        </Columns>
                </asp:GridView>
            </div>
        </ContentTemplate>

    </asp:UpdatePanel>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" DynamicLayout="true" AssociatedUpdatePanelID="UpdatePanel1" DisplayAfter="500" >

       <ProgressTemplate>
          <img src="/Homestay/images/demo_wait.gif" />
           Please Wait...
       </ProgressTemplate>
   
    </asp:UpdateProgress>
    <br />
     <asp:UpdateProgress ID="UpdateProgress2" runat="server" DynamicLayout="true" AssociatedUpdatePanelID="UpdatePanel2" DisplayAfter="500" >

       <ProgressTemplate>
          <img src="/Homestay/images/demo_wait.gif" />
           Please Wait...
       </ProgressTemplate>
   
    </asp:UpdateProgress>
    <br />
    
    <asp:UpdatePanel ID="UpdatePanel2" runat="server"  >
        <ContentTemplate>
            
            <!--End:  studentStatusID-->

           <!--Start:  familySelectionID-->
           <div id="familySelectionID" runat="server">
               <h3>Families</h3>
            <asp:Label ID="litRoomWithOtherFamily" runat="server" Text=""></asp:Label>
            <asp:Literal ID="litFoundFamily" runat=server></asp:Literal>   
            <br />
             <div style="margin-bottom:10px;width:40%;float:left;"> 
                  <asp:DropDownList ID="ddlFamilies" runat="server" Width="200px" >
               
                    </asp:DropDownList>
                 <asp:Label ID="litFamilyPlaced" runat="server" Text=""></asp:Label>
                 <asp:Button ID="btnCompare" runat="server" Text="Compare"  OnClientClick="return ValidateSelectFamily();" OnClick="btnCompare_Click"/>
             </div> 
           
                 <!--Start:  familyStatusID-->
                 <div id="familyStatusID" runat="server" visible="false" style="margin-bottom:10px;width:100%;float:left;"> 
                     <b>Room status: </b> 
                     <div class="tooltip"><i class="fas fa-info-circle"></i>
                        <span class="tooltiptext">
                            The room status will display all placements for the selected family. <br />
                        </span>
                    </div>
                     <asp:Label ID="lblFamilyStatus" runat="server" Text="" visible="false"/>
                     <asp:GridView ID="GvAllRoom" AutoGenerateColumns="False" 
                       emptydatatext="N/A"  Width="100%" 
                        cellpadding="5"
                        cellspacing="5"
                       runat="server" >
                    <HeaderStyle BackColor="#448BC1" Font-Bold="true" ForeColor="White" />
                    <AlternatingRowStyle BackColor="AliceBlue"></AlternatingRowStyle>
                           <Columns>
                               <asp:BoundField DataField="room" HeaderText="Room"  ItemStyle-Width="10%" />
                               <asp:BoundField DataField="availableStarting" HeaderText="Available starting"  ItemStyle-Width="40%" />
                               <asp:BoundField DataField="occupancy" HeaderText="Occupancy" ItemStyle-Width="50%" HtmlEncode="False" />
                               
                        </Columns>
                    </asp:GridView>
                 </div> 
                 <!--End:  familyStatusID-->
           </div>
            <!--End:  familySelectionID-->
           
            <!--
            <p>
                
                <b>Notes:</b> <br />
                <i>If the student has been placed:
                    <ul>
                        <li>You can change "Placement" and "Effective Quarter" of the student.</li>
                        <li> You must withdraw student from the room before moving the student to another home/room.</li>
                    </ul>
                </i>
            </p>
            -->
            <!--Start:  studentFamilyPlacementID-->
            <div id="studentFamilyPlacementID" runat="server" visible="false" style="width:100%; float:left">
                <h2>Student-Family Placement</h2>
            <div class="validation-summary-container errormark validation-summary-valid"  ID="divError" runat="server" Visible="false">
                <div class="errormark validation-summary-errors" data-valmsg-summary="true" id="valSummaryPanel">
                    <span>Sign-in unsuccessful.  Please try again.</span>
                    <asp:Literal id="litErrorMsg" runat="server" />
                </div>
            </div>
            <div class="validation-summary-container successmark validation-summary-valid"  ID="divSuccess" runat="server" Visible="false">
                <div class="successmark validation-summary-success" data-valmsg-summary="true" >
                    <span><asp:Literal id="litSuccessMsg" runat="server" /></span>
                </div>
            </div>
            <asp:Label ID="lblMsg" runat="server" ForeColor="Red"></asp:Label>
            <div id="insertDiv" runat="server" Visible="false">
               
            <strong>Place student</strong>
            <div class="tooltip"><i class="fas fa-info-circle"></i>
                    <span class="tooltiptext">
                        You can place the selected student with selected family here.
                    </span>
                </div>
                
            <br />
            <div style="width:200px;float:left;">
                 <b>Room Number*</b> <br />
                <asp:DropDownList ID="ddlRoom" runat="server">
                   
                  </asp:DropDownList>
            </div>
             <div style="width:300px;float:left;">
                 <b>Effective Date *</b> <br />
                 <asp:TextBox ID="txtEffectiveDate" runat="server" CssClass="classTarget"></asp:TextBox>
                
            </div>
            <div style="width:300px;float:left;">
                 <b>End Date</b> <br />
                <asp:TextBox ID="txtEndDate" runat="server" CssClass="classTarget"></asp:TextBox>
                
            </div>
                <div style="width:300px;float:left;">

                 <b>Placement Type</b> <br />
                <asp:DropDownList ID="ddlRmPlacement" runat="server">
                        <asp:ListItem Value="">--Select a placement type--</asp:ListItem>
                        <asp:ListItem Value="Tentative Placement"></asp:ListItem>
                        <asp:ListItem Value="Student Placed"></asp:ListItem>
                  </asp:DropDownList>
            </div>
            <br /><br />
             <div style="width:100%">
                <asp:Button ID="btnPlace" runat="server" Text="Place" OnClick="btnPlace_Click" /> <br />
                 
                  <asp:Label ID="litPlace" runat="server" Text=""></asp:Label>
                 
                  
            </div>

          </div> <!-- End insertDiv -->
            <br /><br />
            <div id="divGvStuFam" runat="server" visible="false">
                <b>Edit Placements</b>
                <div class="tooltip"><i class="fas fa-info-circle"></i>
                    <span class="tooltiptext">
                        You can edit your placements here. <br />
                        You can change Placement Type, Room Number, Effective Date and End Date.
                    </span>
                </div>
                <asp:GridView ID="gvStuFam" runat="server" AutoGenerateColumns="False" CellPadding="6" 
                    OnRowCancelingEdit="gvStuFam_RowCancelingEdit"   
                    OnRowDatabound="gvStuFam_RowDataBound"
                    OnRowEditing="gvStuFam_RowEditing" 
                    OnRowUpdating="gvStuFam_RowUpdating">  
                    <Columns>  
                <asp:TemplateField>  
                    <ItemTemplate>  
                        <asp:Button ID="btn_Edit" runat="server" Text="Edit" CommandName="Edit" />  
                    </ItemTemplate>  
                    <EditItemTemplate>  
                        <asp:Button ID="btn_Update" runat="server" Text="Update" CommandName="Update" />  
                        <asp:Button ID="btn_Cancel" runat="server" Text="Cancel" CommandName="Cancel"/>  
                    </EditItemTemplate>  
                </asp:TemplateField>  
                <asp:TemplateField HeaderText="id">
                    <ItemTemplate>
                        <asp:Label ID="lbl_id" runat="server"  Text='<%#Eval("id") %>'></asp:Label>
                        
                    </ItemTemplate>
                </asp:TemplateField>
                 
                <asp:TemplateField HeaderText="Room Number">  
                    <ItemTemplate>  
                        <asp:Label ID="lbl_roomNumber" runat="server" Text='<%#Eval("roomNumber") %>'></asp:Label>  
                    </ItemTemplate>  
                    <EditItemTemplate>  
                        <asp:DropDownList ID="ddlRoomGv" runat="server">
                            <asp:ListItem Value="">--Select a room--</asp:ListItem>
                            <asp:ListItem Value="1">Room 1</asp:ListItem>
                            <asp:ListItem Value="2">Room 2</asp:ListItem>
                            <asp:ListItem Value="3">Room 3</asp:ListItem>
                            <asp:ListItem Value="4">Room 4</asp:ListItem>
			            </asp:DropDownList>
                    </EditItemTemplate>  
                </asp:TemplateField>  
                <asp:TemplateField HeaderText="Effective Date">  
                    <ItemTemplate>  
                        <asp:Label ID="lbl_effectiveQuarter" runat="server" Text='<%#Eval("effectiveQuarter") %>'></asp:Label>  
                    </ItemTemplate>  
                    <EditItemTemplate>  
                        <asp:TextBox ID="txtEffectiveDateEdit" runat="server" Text='<%#Eval("effectiveQuarter") %>'  CssClass="classTarget"></asp:TextBox>
                    </EditItemTemplate>  
                </asp:TemplateField>  
                <asp:TemplateField HeaderText="End Date">  
                    <ItemTemplate>  
                        <asp:Label ID="lbl_endQuarter" runat="server" Text='<%#Eval("endQuarter") %>'></asp:Label>  
                    </ItemTemplate>  
                    <EditItemTemplate>  
                        <asp:TextBox ID="txtEndDateEdit" runat="server" Text='<%#Eval("endQuarter") %>'  CssClass="classTarget"></asp:TextBox>
                    </EditItemTemplate>  
                </asp:TemplateField>  
                <asp:TemplateField HeaderText="Placement Type">  
                    <ItemTemplate>  
                        <asp:Label ID="lbl_placementType" runat="server" Text='<%#Eval("placementType") %>'></asp:Label>  
                    </ItemTemplate>  
                    <EditItemTemplate>
			            <asp:DropDownList ID="ddlPlacementType" runat="server">
                            <asp:ListItem Value="">--Select a placement type--</asp:ListItem>
                            <asp:ListItem Value="Tentative Placement"></asp:ListItem>
                            <asp:ListItem Value="Student Placed"></asp:ListItem>
                            <asp:ListItem Value="Withdrew"></asp:ListItem>
                            <asp:ListItem Value="Graduated"></asp:ListItem>
                            <asp:ListItem Value="Student Moved Out"></asp:ListItem>
			            </asp:DropDownList>
		            </EditItemTemplate>  
                </asp:TemplateField>
            </Columns>  
                    <HeaderStyle BackColor="#663300" ForeColor="#ffffff"/>  
                    <RowStyle BackColor="#e7ceb6"/>  
                </asp:GridView>  
            </div>
           
                
            
            </div>
            <!--End:  studentFamilyPlacementID-->
            
            
           <br />
            <!--Start: informationID-->
                <div style="width:100%;" id="informationID" runat="server" visible="false">
                <div style="width:70%;float:left;margin-right:10px">
                    <h4>Comparison</h4>
                    <asp:GridView ID="GridView1" AutoGenerateColumns="False" 
                       emptydatatext="N/A"  Width="100%" 
                        cellpadding="5"
                        cellspacing="5"
                       runat="server">
                    <HeaderStyle BackColor="#448BC1" Font-Bold="False" ForeColor="White" />
                    <AlternatingRowStyle BackColor="AliceBlue"></AlternatingRowStyle>
                           <Columns>
                               <asp:BoundField DataField="DisplayText" HeaderText="Field"  ItemStyle-Width="20%" ItemStyle-BackColor="#CCCCCC" />
                               <asp:BoundField DataField="StudentValue" HeaderText="Student"  ItemStyle-Width="40%" />
                               <asp:BoundField DataField="FamilyValue" HeaderText="Family" ItemStyle-Width="40%"/>
                               
                        </Columns>
                    </asp:GridView>
                </div>
                <div style="width:20%;float:left">
                    <h4>Matching score</h4>
                    <asp:GridView ID="GridView2" AutoGenerateColumns="False" 
                               emptydatatext="N/A"  Width="95%" 
                                cellpadding="5"
                                cellspacing="5"
                               runat="server">
                            <HeaderStyle BackColor="#448BC1" Font-Bold="False" ForeColor="White" />
                            <AlternatingRowStyle BackColor="AliceBlue"></AlternatingRowStyle>
                                   <Columns>
                                       <asp:BoundField DataField="Field" HeaderText="Field"  ItemStyle-Width="80%" ItemStyle-BackColor="#CCCCCC" />
                                       <asp:BoundField DataField="ScoreMatch" HeaderText="Percent"   />
                               
                               
                                </Columns>
                  </asp:GridView>
                </div>
            </div>
            <!--End: informationID-->
            
          
            
            
            
            <table style="width: 80%;">
                <tr>
                    <td style=" vertical-align:top">
                        <asp:Literal ID="litSelectedStuInfo" runat=server></asp:Literal>  
                    </td>
                    <td style="vertical-align:top">
                        <asp:Literal ID="litFamilyInfo" runat=server></asp:Literal>  
                    </td>
                </tr>
                
                
            </table>
            <asp:Literal ID="litResultMsg" runat=server></asp:Literal>   
        </ContentTemplate>

    </asp:UpdatePanel>

    

    
    <script type="text/javascript">
        
        function ValidateSelectStudent() {
            var ddl = $('#<%=ddlStudents.ClientID%>');
            var selectedValue = ddl.val();
  
            //alert(selectedValue); //SelVal is the selected Value
            if (selectedValue == 0) {
                 alert("Please select a student to search");
                return false;
            }
            return true;
        }

        function ValidateSelectFamily() {
            var ddl = $('#<%=ddlFamilies.ClientID%>');
            var selectedValue = ddl.val();
  
            //alert(selectedValue); //SelVal is the selected Value
            if (selectedValue == 0) {
                 alert("Please select a family to compare");
                return false;
            }
            return true;
        }

        function ValidatePlacement() {
            var ddlRoom = $('#<%=ddlRoom.ClientID%>');
            var txtEffectiveDate = $('#<%=txtEffectiveDate.ClientID%>');
            var txtEndDate = $('#<%=txtEndDate.ClientID%>');
           
            if (ddlRoom.val() == "") {
                alert("Please select a room to place");
                ddlRoom.focus();
                return false;
            }
            if (txtEffectiveDate.val() == ""  )
            {
                //alert("Please select an effective quarter");
                txtEffectiveDate.focus();
                $('#<%=divError.ClientID%>').show();
                var list = $('<ul/>').appendTo('#valSummaryPanel');
                list.append('<li>Please select a room to place</li>');
                //for (var i = 0; i < 10; i++) {
                    // New <li> elements are created here and added to the <ul> element.
                  //  list.append('<li>something</li>');
                //}
                return false;
            }

           
            
            return true;
         }

        
        $(function () {
            $('[data-toggle="tooltip"]').tooltip(); 
            //Must do this to have datepicker in updatepanel
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
            function EndRequestHandler(sender, args) {
                $('.classTarget').datepicker({ dateFormat: 'mm/dd/yy' });
            }
                //Get data as JSON type from REST API
            /*
            $.ajax({
                type: "POST",
                url: "MatchingWS.asmx/GetDemo",
                contentType: "application/json",
                data: {applicantID : "abc"},
                //dataType: "json"
            })
            .done(function (data) {
                //alert("success");

                console.log(data);
                //displayTable(data);
            })
            .fail(function (xhr) {
                console.log('error', xhr);
            });
            */
        });
        
        
    </script>
    </asp:Content>