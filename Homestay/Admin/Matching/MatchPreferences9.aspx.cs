﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Homestay_Admin_MatchPreferences9 : System.Web.UI.Page
{
    Homestay hsInfo = new Homestay();
    Quarter_MetaData QMD = new Quarter_MetaData();
    PlacingUtility placingUtility = new PlacingUtility();
    private static Dictionary<string, EntityObject> stuHM;
    FamilyManager familyManager = new FamilyManager();
    StudentManager studentManager = new StudentManager();
    private static Dictionary<string, EntityObject> famHM;
    private static DataTable dtDisplay;
    private string connectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSInternationalStudent"].ToString();

    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (!IsPostBack)
        {
            studentStatusID.Visible = false;
            familySelectionID.Visible = false;
            
            /** Populate DL for students Using DataBind **/
            DataTable dtStudents = hsInfo.GetHomestayStudents("Active", 0);
            //DataTable dtStudents = hsInfo.GetHomestayStudents("Active", 0);
            dtStudents.Columns.Add("valueField", typeof(string));
            if (dtStudents != null)
            {
                if (dtStudents.Rows.Count > 0)
                {
                    foreach (DataRow dr in dtStudents.Rows)
                    {
                        string applicantIDStr = dr["applicantID"].ToString();
                        dr["valueField"] = dr["familyName"].ToString() + ", "
                                            + dr["firstName"].ToString() + " - " + Convert.ToDateTime(dr["DOB"]).ToShortDateString();
                    }
                }
            }
            ddlStudents.DataSource = dtStudents;
            ddlStudents.DataTextField = "valueField";
            ddlStudents.DataValueField = "applicantID";
            ddlStudents.DataBind();
            ddlStudents.Items.Insert(0, new ListItem("--Select a student--", "")); //Add default value
            /** End populating dropdow **/
           
        }
        //Response.Write(stuHM["2401"].Name.ToString());
        //litResultMsg.Text = stuHM["2401"].Name.ToString();
        //litResultMsg.Text = stuHM.Count() +"";
        /*
        string connectionStr = ConfigurationManager.ConnectionStrings["ConnectionString_CCSInternationalStudent"].ToString();
        SqlConnection conn = new SqlConnection(connectionStr);
        string sqlQuery = @"SELECT  [id]
                              ,[applicantID]
                          FROM[CCSInternationalStudent].[dbo].[HomestayStudentInfo]";


        DataTable students = new DataTable();
        SqlCommand cmd = new SqlCommand(sqlQuery, conn);

        try
        {
            conn.Open();

            SqlDataAdapter objDataAdapter = new SqlDataAdapter(cmd);

            objDataAdapter.Fill(students);
            ddlStudents.DataSource = students;
            ddlStudents.DataTextField = "applicantID";
            ddlStudents.DataValueField = "id";
            ddlStudents.DataBind();

        }
        catch (Exception ex)
        {
            Label1.Text += "Error RunGetQuery: " + ex.Message + "<br />SQL: " + sqlQuery + "<br />";
        }
        */

    }



    

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        studentStatusID.Visible = true;
        familySelectionID.Visible = true;
        familyStatusID.Visible = false;
        informationID.Visible = false;
        divError.Visible = false;
        divSuccess.Visible = false;
        insertDiv.Visible = false;
        divGvStuFam.Visible = false;
        //gvStuFam.Visible = false;
        studentFamilyPlacementID.Visible = false;


        //Reset ddlFamilies
        ddlFamilies.SelectedIndex = -1;
        //Add effective quarter
        DataTable dtQuarters = QMD.GetQuarterStart(-3, 15);
        
        //DataTable dtQuarters = QMD.GetQuarterStart(-1, 1);
        

        //Reset Placement, Room, EffectiveQtr
        ddlRmPlacement.SelectedIndex = -1;
        ddlRoom.SelectedIndex = -1;
        
        //ddlRmPlacement.Enabled = false;
        //ddlRoom.Enabled = false;
        //ddlEffectiveQtr.Enabled = false;

       
        //stuHM = MatchingUtility.LoadStudentHM();
        stuHM = studentManager.GetStuHM();
        //famHM = MatchingUtility.LoadFamilyHM();
        famHM = familyManager.GetFamilyHM();

       
        //litResultMsg.Text = "No of Families have open room: " + famHM.Count() + "<br/>";
        //litRoomWithOtherFamily.Text = "Number of Families have open room: " +  famHM.Count() +"<br/>";
        //litResultMsg.Text += "Gender: " + rdoGender.SelectedValue + "<br/>";
        //litResultMsg.Text = "HS Preference: " + rdoGender.SelectedValue + "<br/>";
        //litResultMsg.Text += "Smoking: " + rdoSmoking.SelectedValue + "<br/>";

        int selectedStudentID = Convert.ToInt32(ddlStudents.SelectedValue);
        string rdoGenderStr = rdoGender.SelectedValue.ToString();
        string rdoHomestayStr = rdoHomestay.SelectedValue.ToString();
        string rdoSmokingStr = rdoSmoking.SelectedValue.ToString();

        //itSelectedStuInfo.Text = "<strong>Selected Student's Info:</strong>" + "<br/>";
        //litResultMsg.Text += "ID: " + stuHM[selectedStudentID + ""].Name.ToString() + "<br/>";
        //litResultMsg.Text += "Smoker: " + stuHM[selectedStudentID + ""].Attributes["Smoker"][0];
        EntityObject stuObject = stuHM[selectedStudentID + ""];
        Dictionary<string, string[]> stuAttributes = stuObject.Attributes;

        //Display student's current status
        //DisplayCurrStudentStatus(ddlStudents.SelectedValue);
        DisplayGvCurrStudentStatus(ddlStudents.SelectedValue);
        
        //litSelectedStuInfo.Text += " entityObj:" + stuObject.ToString() + "</br>";
        string stuCol = stuObject.Attributes["studyWhere"][0].ToLower();
        //litResultMsg.Text += " College:" + stuCol + "<br/>";

       
        //Popudate Datatable dtDisplay to display "Comparision"
        dtDisplay = new DataTable();
        dtDisplay.Columns.AddRange(new DataColumn[4]
                    {
                        
                        new DataColumn("Field", typeof(string)),
                        new DataColumn("StudentValue", typeof(string)),
                        new DataColumn("FamilyValue", typeof(string)),
                        new DataColumn("DisplayText", typeof(string))
                    });
        dtDisplay.Rows.Add("applicantID", stuAttributes["applicantID"][0], "", "ID");
        dtDisplay.Rows.Add("Name", stuAttributes["familyName"][0]+ " ," + stuAttributes["firstName"][0], "", "Name");
        dtDisplay.Rows.Add("college", stuAttributes["college"][0], "", "College");
        dtDisplay.Rows.Add("smoke", stuAttributes["smoke"][0], "", "Do you smoke?");
        dtDisplay.Rows.Add("otherSmoke", stuAttributes["otherSmoke"][0], "", "Other smokers OK?");
        dtDisplay.Rows.Add("smokingHabits", stuAttributes["smokingHabits"][0], "", "Explain:");
        dtDisplay.Rows.Add("gender", stuAttributes["gender"][0], "", "Gender Preference");
        dtDisplay.Rows.Add("homestayOption", stuAttributes["homestayOption"][0], "", "Homestay Option");
        dtDisplay.Rows.Add("dietaryNeeds", string.Join(", ", stuAttributes["dietaryNeeds"]), "", "Dietary needs");
        dtDisplay.Rows.Add("hobbies", string.Join(", ", stuAttributes["hobbies"]), "", "Hobbies");
        dtDisplay.Rows.Add("activitiesEnjoyed", string.Join(", ", stuAttributes["activitiesEnjoyed"]), "", "Other hobbies/activities enjoyed");
        dtDisplay.Rows.Add("interaction", stuAttributes["interaction"][0], "", "Interaction and Conversation");
        dtDisplay.Rows.Add("homeEnvironment", stuAttributes["homeEnvironment"][0], "", "Home Environment");
        dtDisplay.Rows.Add("homeEnvironmentPreferences", stuAttributes["homeEnvironmentPreferences"][0], "", "Explain");
        dtDisplay.Rows.Add("childrenPref", stuAttributes["childrenPref"][0], "", "Children");
        dtDisplay.Rows.Add("traveledOutsideCountry", stuAttributes["traveledOutsideCountry"][0], "", "Traveled outside U.S. before?");
        string othersStr = String.Format("Allergies:{0}, Health conditions: {1}, Anything else:\n"
                                            , stuAttributes["allergies"][0], stuAttributes["healthConditions"][0], stuAttributes["anythingElse"][0]); 
        dtDisplay.Rows.Add("otherNotes", othersStr, "", "Others");
        //dtDisplay.Rows.Add("familyID", stuAttributes["familyID"][0], "");
        GridView1.DataSource = dtDisplay;
        GridView1.DataBind();




        //litResultMsg.Text = "--------- <br/>";
        //litResultMsg.Text += " " + famHM.Count();
        //litRoomWithOtherFamily.Text = selectedStudentID + " " + rdoGenderStr + " " + rdoHomestayStr+ " " + rdoSmokingStr;
        /** Populate DL for students Using DataBind **/
        //string strSQL = "SELECT id, homeName FROM [CCSInternationalStudent].[dbo].[HomestayFamilyInfo]";
        //DataTable dtFamilies = hsInfo.RunGetQuery(strSQL);

        //List of matching level
        var matchingList = new List<Dictionary<string, EntityObject>>();

        /** Filter by college **/
        Dictionary<string, EntityObject> famHMFilteredCollege = MatchingUtility.MatchStuFamCollege(stuCol, famHM);
        //litResultMsg.Text += "There are " + famHMFilteredCollege.Count() + " families match with student's college: <br/>";
        //litResultMsg.Text += MatchingUtility.PrintHM(famHMFilteredCollege);
        matchingList.Add(famHMFilteredCollege);

        famHM = famHMFilteredCollege;

        /** Filter by Smoking preferences **/
        if (rdoSmoking.SelectedValue.Equals("Include")  || rdoSmoking.SelectedValue.Equals(""))
        {
            Dictionary<string, EntityObject> famHMFilteredSmoking = MatchingUtility.MatchStuFamSmoking(stuObject, famHM);
            //litResultMsg.Text += "<br/>There are " + famHMFilteredSmoking.Count() + " families match with student's smoking:<br/>";
            //litResultMsg.Text += MatchingUtility.PrintHM(famHMFilteredSmoking);
            matchingList.Add(famHMFilteredSmoking);
        }

        /** Filter by Gender **/
        if (rdoGender.SelectedValue.Equals("Include") || rdoGender.SelectedValue.Equals(""))
        {
            Dictionary<string, EntityObject> famHMFilteredGender = MatchingUtility.MatchStuFamGender(stuObject, famHM);
            //litResultMsg.Text += "<br/>There are " + famHMFilteredGender.Count() + " families match with student's gender:<br/>";
            //litResultMsg.Text += MatchingUtility.PrintHM(famHMFilteredGender);
            matchingList.Add(famHMFilteredGender);
        }


        /** Filter by homestayOption **/
        if (rdoHomestay.SelectedValue.Equals("Include") || rdoHomestay.SelectedValue.Equals(""))
        {
            Dictionary<string, EntityObject> famHMFilteredHomestayOption = MatchingUtility.MatchStuFamHomestayOption(stuObject, famHM, litResultMsg.Text);
            //litResultMsg.Text += "<br/>There are " + famHMFilteredHomestayOption.Count() + " families match with student's hsOption:<br/>";
            //litResultMsg.Text += MatchingUtility.PrintHM(famHMFilteredHomestayOption);
            matchingList.Add(famHMFilteredHomestayOption);
        }


        /** Combine all dictionaries **/
        Dictionary<string, EntityObject> famHMFilteredFinal = MatchingUtility.FindIntersection(matchingList);
        //litResultMsg.Text += "<br/>There are " + famHMFilteredFinal.Count() + " families match with student:<br/>";
        //litResultMsg.Text += matchingList.Count() + "<br/>";

        //Proces final list to add score
        famHMFilteredFinal = MatchingUtility.AddScore(stuObject, famHMFilteredFinal);
        famHM = famHMFilteredFinal;

        //litResultMsg.Text += MatchingUtility.PrintHM2(stuObject, famHMFilteredFinal);


        /** Populate DL for students Using DataBind **/
        DataTable dtFamilies = new DataTable();
        dtFamilies.Columns.Add("id", typeof(string));
        dtFamilies.Columns.Add("homeName", typeof(string));
        dtFamilies.Columns.Add("score", typeof(string));
        dtFamilies.Columns.Add("textField", typeof(string));
        foreach (var f in famHMFilteredFinal)
        {
            
            Dictionary<string, string[]> attributes = f.Value.Attributes;
            string formatText = String.Format("{0}, score: {1}%", attributes["homeName"][0].ToString(), attributes["score"][0]);
            dtFamilies.Rows.Add(attributes["id"][0].ToString(), attributes["homeName"][0].ToString(), attributes["score"][0], formatText);
        }

        dtFamilies.DefaultView.Sort = "score desc";
        dtFamilies = dtFamilies.DefaultView.ToTable();

        litFoundFamily.Text = "<strong>There are " + famHMFilteredFinal.Count() + " families that match the criteria above</strong>" 
                              + " (The result is ranked in order of overall match score)";
        //litResultMsg.Text += dtFamilies.Rows.Count + " families: <br/>";

        //Match with smoking preferences

        ddlFamilies.DataSource = dtFamilies;
        ddlFamilies.DataTextField = "textField";
        ddlFamilies.DataValueField = "id";
        ddlFamilies.DataBind();
        ddlFamilies.Items.Insert(0, new ListItem("--Select a family--", "")); //Add default value
                                                                              /** End populating dropdow **/

        //populate room
        DataTable dtRooms = new DataTable();
        dtRooms.Columns.Add("valueField", typeof(string));
        dtRooms.Columns.Add("textField", typeof(string));

        for (var i = 1; i <= 4; i++)
        {
            dtRooms.Rows.Add(i + "", "Room " + i);
        }

        ddlRoom.DataSource = dtRooms;
        ddlRoom.DataTextField = "textField";
        ddlRoom.DataValueField = "valueField";
        ddlRoom.DataBind();
        ddlRoom.Items.Insert(0, new ListItem("--Select a room--", "")); //Add default value
        /*
        var format = System.Globalization.CultureInfo.InvariantCulture.DateTimeFormat;

        Stay[] stays = new Stay[]
        {

            new Stay(DateTime.Parse("1/1/2015 00:00", format), DateTime.Parse("1/1/2015 23:59", format)),
            //new Stay(DateTime.Parse("1/3/2015 00:00", format), DateTime.Parse("1/4/2015 23:59", format)),
            new Stay(DateTime.Parse("1/2/2015 00:00", format),DateTime.MaxValue)
        };
        */
        //Check database, if exist Open-enddate, only can add valid rang
        //If not exist open-endate, can only add after last day of lista
        //lblMsg.Text = DoesNotOverlap(stays)+"";
        //litCurrStudentStatus.Text = DoesNotOverlap(stays) + "";
        //litCurrStudentStatus.Text = DateTime.Parse("11/1/2018 23:59").ToString();
        //litCurrStudentStatus.Text = (GetClassDate("LastDayOfClass", "Winter 2019")).ToString();
    }

   
    /// <summary>Get First or Last day of class
    /// <param name="dateType">"FirstDayOfClass" or "LastDayOfClass".</param>
    /// <param name="quarter">Quarter string. Ex: "Winter 2019".</param>
    /// </summary>
    
    private void DisplayGvCurrStudentStatus(string selectedValue)
    {
        string sqlQueryCurrentStatus = @"SELECT *
                                      FROM [CCSInternationalStudent].[dbo].[HomestayPlacement]
                                      INNER JOIN [CCSInternationalStudent].[dbo].[HomestayFamilyInfo]
                                      ON HomestayPlacement.familyID = HomestayFamilyInfo.id
                                      WHERE applicantID =" + ddlStudents.SelectedValue;

        DataTable dtCurrentStatus = hsInfo.RunGetQuery(sqlQueryCurrentStatus);
        if (dtCurrentStatus == null || dtCurrentStatus.Rows.Count == 0)
        {
            litCurrStudentStatus.Text = "The student has not been placed!";
        }
        else
        {
            litCurrStudentStatus.Text = "";

        }

        GvStudentStatus.DataSource = dtCurrentStatus;
        GvStudentStatus.DataBind();
    }

    protected void btnCompare_Click(object sender, EventArgs e)
    {
        familyStatusID.Visible = true;
        informationID.Visible = true;
        studentFamilyPlacementID.Visible = true;
        insertDiv.Visible = true;
        
        //divGvStuFam.Visible = true;
        //gvStuFam.Visible = true;
        lblMsg.Text = "";
        divError.Visible = false;
        divSuccess.Visible = false;
        DisplayPanel2();
    }

    protected void btnReset_Click(object sender, EventArgs e)
    {
        Response.Redirect("MatchPreferences9.aspx");
    }


   protected void DisplayPanel2()
    {
        //lblMsg.Text = "";

        StringBuilder sb = new StringBuilder();
        //sb.Append("<strong>Family Info</strong>:<br/>");

        string selectedStudentIDStr = ddlStudents.SelectedValue;
        EntityObject stuObject = stuHM[selectedStudentIDStr];
        Dictionary<string, string[]> stuAttributes = stuObject.Attributes;

       
        string familyIDStr = ddlFamilies.SelectedValue;
        /* Start: Dispaly Student-Family Placements*/
        DataTable studentFamilyPlacements = GetPlacementsByStudentFamily(selectedStudentIDStr, familyIDStr);
        if (studentFamilyPlacements == null || studentFamilyPlacements.Rows.Count == 0)
        {
            divGvStuFam.Visible = false;
        }
        else
        {
            divGvStuFam.Visible = true;
        }
        /* End: Dispaly Student-Family Placements*/
        if (familyIDStr.Equals(""))
        {
            return;
        }
        EntityObject famObject = famHM[familyIDStr];
        var famAttributes = famObject.Attributes;

        //Hide/Unhide insertDiv
        string sqlQuery2 = @"SELECT *
                              FROM [CCSInternationalStudent].[dbo].[HomestayPlacement]
                              WHERE applicantID =" + selectedStudentIDStr + " AND familyID= " + familyIDStr;
        DataTable dtsqlQuery1 = hsInfo.RunGetQuery(sqlQuery2);
        if (dtsqlQuery1 == null || dtsqlQuery1.Rows.Count == 0)
        {
            //insertDiv.Visible = true;
        }
        else
        {
            //insertDiv.Visible = false;
        }



        //Display student's current status
        //DisplayCurrStudentStatus(selectedStudentIDStr);
        DisplayGvCurrStudentStatus(selectedStudentIDStr);
        //sb.Append(MatchingUtility.PrintEnityObject(famObject) + "<br/><br/>");
        //PrintEnityObject(famObject);
        litFamilyInfo.Text = sb.ToString();

        //Populate dtAllRoom for GvAllRoom
        string[] rmStatusArr = new string[4];
        for (var i = 1; i <= 4; i++)
        {
            string sqlQuery = @"SELECT *
                              FROM [CCSInternationalStudent].[dbo].[HomestayPlacement]
                              WHERE roomNumber =" + i + " AND familyID= " + familyIDStr;
            DataTable dtPlacements = hsInfo.RunGetQuery(sqlQuery);
            
            foreach (DataRow rowPlacement in dtPlacements.Rows)
            {
                string studentIDStr = rowPlacement["applicantID"].ToString();

                string studentName = stuHM[studentIDStr].Attributes["familyName"][0] +" ,"
                                    + stuHM[studentIDStr].Attributes["firstName"][0];
                string studentGender = stuHM[studentIDStr].Attributes["Gender"][0];
                studentGender = studentGender.Equals("M") ? "Male" : "Female";
                rmStatusArr[i - 1] += String.Format("<b>Student:</b> {0} ({1}), <b>Effective Date:</b> {2}" +
                    "                               , <b>End Date:</b> {3}" +
                    "                               , <b>Placement Type:</b> {4} <br/>"
                                                    , studentName, studentGender
                                                    , rowPlacement["effectiveQuarter"]
                                                    , rowPlacement["endQuarter"]
                                                    , rowPlacement["placementType"]); 
                   
                
            }
           
        }
        
        //display all rooms
        DataTable dtAllRoom = new DataTable();
        dtAllRoom.Columns.AddRange(new DataColumn[3]
                    {

                        new DataColumn("room", typeof(string)),
                        new DataColumn("availableStarting", typeof(string)),
                        new DataColumn("occupancy", typeof(string))
                    });
        for (var i = 1; i <= 4; i++)
        {

            dtAllRoom.Rows.Add("Room " + i, famAttributes["room"+i+"Occupancy"][0], rmStatusArr[i-1]);
        }

        GvAllRoom.DataSource = dtAllRoom;
        GvAllRoom.DataBind();
        //End GVAllRoom
        /*
        string sqlQuery1 = @"SELECT *
                              FROM [CCSInternationalStudent].[dbo].[HomestayPlacement]
                              WHERE applicantID = " + selectedStudentIDStr
                                    + " AND familyID = " + familyIDStr
                                    + " ORDER BY endQuarter asc";
       

        DataTable dtPlacements1 = hsInfo.RunGetQuery(sqlQuery1);
        
       

        gvStuFam.DataSource = dtPlacements1;
        gvStuFam.DataBind();
        */
        ShowData();//to bind data to gvStuFam

        string stuFamilyID = stuAttributes["familyID"][0];
        /*
        //populate room
        DataTable dtRooms = new DataTable();
        dtRooms.Columns.Add("valueField", typeof(string));
        dtRooms.Columns.Add("textField", typeof(string));

        for (var i = 1; i <= 4; i++)
        {
            //Only add room available
            string roomOccupancyStr = famAttributes["room" + i + "Occupancy"][0];
            if (roomOccupancyStr.Equals("No Info") || roomOccupancyStr.Contains("CCS")
                || !GetIsAvailableNow(roomOccupancyStr))
            {

            }
            else
            {
                if (famAttributes["room"+i].Length == 0) //Only add room available and not occupied
                {
                    
                }
                
            }
            dtRooms.Rows.Add(i + "", "Room " + i);

        }

        ddlRoom.DataSource = dtRooms;
        ddlRoom.DataTextField = "textField";
        ddlRoom.DataValueField = "valueField";
        ddlRoom.DataBind();
        ddlRoom.Items.Insert(0, new ListItem("--Select a room--", "")); //Add default value
        */


        //disable room when stuAttributes["familyID"] = "NA" || not "NA" but familyID not match selected family
        if (stuFamilyID.Equals("NA")) // student has not been placed with any family
          //  || (!stuFamilyID.Equals("NA") && !stuFamilyID.Equals(familyIDStr)))
        {
            ddlRmPlacement.Enabled = true;
            ddlRoom.Enabled = true;
            

            ddlRmPlacement.SelectedIndex = -1;
            ddlRoom.SelectedIndex = -1;
            

            for (var i = 1; i <= 4; i++)
            {
                string roomOccupancy = famAttributes["room" + i + "Occupancy"][0];
                if (roomOccupancy.Contains("No Info") || roomOccupancy.Contains("Non-CCS"))
                {
                    //ddlRoom.Items.Remove(ddlRoom.Items.FindByValue(i + ""));
                }
                
                /*
                if (roomOccupancy.Contains("CCS") || roomOccupancy.Equals("No Info") || !GetIsAvailableNow(roomOccupancy))
                {
                   
                }
                */
            }

        }
        else // student has  been placed with a family
        {
            //ddlRoom.Enabled = false;//because you must withdraw before change room
            //If stuFamilyID = familyIDStr, display and populate preselected value
            if (stuFamilyID.Equals(familyIDStr))
            {
                //litRm1Status.Text = "second";
               
                string roomNumberStr = stuAttributes["roomNumber"][0];
                ddlRoom.SelectedValue = roomNumberStr;
                //ddlRmPlacement.SelectedValue = famAttributes["room" + roomNumberStr][1];
                //ddlEffectiveQtr.SelectedValue = famAttributes["room" + roomNumberStr][2];

               
                ddlRmPlacement.Enabled = true;
                
            }
            else
            {
                ddlRmPlacement.SelectedIndex = -1;
                ddlRoom.SelectedIndex = -1;
                
                //litRm1Status.Text = "last";
                //ddlRmPlacement.Enabled = false;
                //ddlRoom.Enabled = false;
                //ddlEffectiveQtr.Enabled = false;
            }
            

        }
        //litRm1Status.Text = stuFamilyID + " " + familyIDStr + stuFamilyID.Equals(familyIDStr);
        //dtDisplay.Rows[0]["FamilyValue"] = "123";
        
        foreach (DataRow dr in dtDisplay.Rows)
        {
            string attr = dr["Field"].ToString();
            try
            {
                dr["FamilyValue"] = String.Join(", ", famAttributes[attr]);
            }
            catch (Exception ex)
            {
                dr["FamilyValue"] = "Key not found";
                switch (attr)
                {
                    case "applicantID":
                        dr["FamilyValue"] = String.Join(", ", famAttributes["id"]);
                        break;
                    case "childrenPref":
                        dr["FamilyValue"] = String.Join(", ", famAttributes["children"]);
                        break;
                    case "smokingHabits":
                        dr["FamilyValue"] = String.Join(", ", famAttributes["smokingGuidelines"]);
                        break;
                    case "homeEnvironmentPreferences":
                        dr["FamilyValue"] = String.Join(", ", famAttributes["homeEnvironment"]);
                        break;
                    case "activitiesEnjoyed":
                        dr["FamilyValue"] = String.Join(", ", famAttributes["otherHobbies"]);
                        break;
                    case "traveledOutsideCountry":
                        dr["FamilyValue"] = String.Join(", ", famAttributes["travelOutsideUS"]);
                        break;
                    case "otherNotes":
                        string familyOtherNotes = String.Format("Pets in home: {0}, Additional Expectations: {1}"
                                                , famAttributes["petsInHome"][0], famAttributes["additionalExpectations"][0]) ;
                        dr["FamilyValue"] = familyOtherNotes;
                        break;
                    case "Name":
                        dr["FamilyValue"] = String.Join(", ", famAttributes["homeName"]);
                        break;
                }
            }
            
            //string columnName = column.ColumnName;
            //string columnData = row[column].ToString();
            //attributes.Add(columnName, new string[] { columnData });
        }
        GridView1.DataSource = dtDisplay;
        GridView1.DataBind();


        DataTable dtMatching = new DataTable();
        dtMatching.Columns.AddRange(new DataColumn[2]
                    {

                        new DataColumn("Field", typeof(string)),
                        new DataColumn("ScoreMatch", typeof(string))
                    });
        dtMatching.Rows.Add("% children match", famAttributes["childrenScore"][0]);
        dtMatching.Rows.Add("% hobbies match", famAttributes["hobbiesScore"][0]);
        dtMatching.Rows.Add("% dietaryNeeds match", famAttributes["dietaryScore"][0]);
        dtMatching.Rows.Add("% interaction match", famAttributes["interactionScore"][0]);
        dtMatching.Rows.Add("% homeEnvironment match", famAttributes["homeEnvironmentScore"][0]);
        dtMatching.Rows.Add("% overall match", famAttributes["score"][0]);

        GridView2.DataSource = dtMatching;
        GridView2.DataBind();

        lblFamilyStatus.Text = famAttributes["familyStatus"][0];

        ddlRmPlacement.SelectedIndex = -1;
        ddlRoom.SelectedIndex = -1;
       

        

    }

    void btn_Click(object sender, EventArgs e)
    {
        //lit.Text = string.Format("Button {0} was pressed.", ((Button)sender).ID);
    }

    private bool GetIsAvailableNow(string roomStatus)
    {
        DataTable dtQuarters = QMD.GetQuarterStart(-3, 15);
        foreach (DataRow r in dtQuarters.Rows)
        {
            if (r["valueField"].ToString().Equals(roomStatus))
            {
                string availableDate = r["textField"].ToString();
                availableDate = availableDate.Substring(availableDate.IndexOf(" ")).Trim();
                DateTime effectiveDate = Convert.ToDateTime(availableDate);
                bool availableNow = DateTime.Compare(effectiveDate, DateTime.Now) <= 0;
                //litRm1Status.Text = availableDate + availableNow;
                return availableNow;
            }
        }
        return false;
    }

    private void DisplayCurrStudentStatus(string selectedStudentIDStr)
    {
        string currStudentStatus = "";
       /*
        EntityObject stuObject = stuHM[selectedStudentIDStr];
        var stuAttributes = stuObject.Attributes;
        string currStudentStatus = stuAttributes["homestayStatus"][0] + "<br/> ";
        if (!stuAttributes["familyID"][0].Equals("NA"))
        {
            string familyIDStr = stuAttributes["familyID"][0];
            EntityObject famObject = famHM[familyIDStr];
            var famAttributes = famObject.Attributes;
            string roomNumberStr = stuAttributes["roomNumber"][0];
            string effectiveQuarterStr = "";
            string placementTypeStr = "";
            string endQuarterStr = "";
            string sqlQuery = @"SELECT *
                              FROM [CCSInternationalStudent].[dbo].[HomestayPlacement]
                              WHERE roomNumber =" + roomNumberStr + " AND familyID= " + familyIDStr + " AND applicantID = " + selectedStudentIDStr;
            DataTable dtPlacements = hsInfo.RunGetQuery(sqlQuery);
            if(dtPlacements!=null && dtPlacements.Rows.Count > 0)
            {
                endQuarterStr = dtPlacements.Rows[0]["endQuarter"].ToString();
            }
            try
            {
                effectiveQuarterStr = famAttributes["room" + roomNumberStr][2];
                placementTypeStr = famAttributes["room" + roomNumberStr][1];
            }
            catch (Exception e)
            {

            }
            currStudentStatus += "Room number: " + roomNumberStr + "<br/>"
                                + "Effective Quarter: " + effectiveQuarterStr +"<br/>"
                                + "End Quarter: " + endQuarterStr + "<br/>"
                                + "Home name: " + stuAttributes["homeName"][0] + "<br/>";
            // Student Placed in room number: 2; home name: UserName, TEST
        }
        */
        string sqlQuery = @"SELECT *
                              FROM [CCSInternationalStudent].[dbo].[HomestayPlacement]
                              WHERE applicantID =" + selectedStudentIDStr;
        
        DataTable dtPlacements = hsInfo.RunGetQuery(sqlQuery);
        if (dtPlacements == null || dtPlacements.Rows.Count == 0)
        {
            currStudentStatus = "From HSStudentInfo";
        }
        else
        {
            
            //From placement table
            foreach (DataRow rowPlacement in dtPlacements.Rows)
            {
                //string 
                currStudentStatus += " familyID: " + rowPlacement["familyID"]
                                    + " ,placementType: " + rowPlacement["placementType"]
                                    + " ,homeName: " + "homeName"
                                    + " ,roomNumber" + rowPlacement["roomNumber"]
                                    + " ,effectiveQuarter: " + rowPlacement["effectiveQuarter"]
                                    + " ,endQuarter: " + rowPlacement["endQuarter"]
                                    + "<br/>";
            }
                
        }
        //foreach (DataRow rowPlacement in dtPlacements.Rows)
        //{
           // rmStatusArr[i - 1] += " studentID:" + rowPlacement["applicantID"] + ", familyID: " + rowPlacement["familyID"]
              //              + " effectiveQuarter: " + rowPlacement["effectiveQuarter"] + " endQuarter: " + rowPlacement["endQuarter"] + "  ";
       // }

        litCurrStudentStatus.Text = currStudentStatus;

        

    }

    protected void btnPlace_Click(object sender, EventArgs e)
    {
        divError.Visible = false;
        divSuccess.Visible = false;
        //Validate Select required room and effective quarter
        if (String.IsNullOrEmpty(ddlRoom.SelectedValue) || String.IsNullOrEmpty(txtEffectiveDate.Text))
        {
            divError.Visible = true;
            ddlRoom.Focus();
            string liStr = "";
            
            if (String.IsNullOrEmpty(txtEffectiveDate.Text))
            {
                liStr += "<li>Effective Date is required</li>";
                txtEffectiveDate.Focus();
            }
            if (String.IsNullOrEmpty(ddlRoom.SelectedValue))
            {
                liStr += "<li>Room number is required.</li>";
                ddlRoom.Focus();
            }
            litErrorMsg.Text = "<ul>" + liStr + "</ul>";
            return;
        }
       
        int selectedFamilyID = Convert.ToInt32(ddlFamilies.SelectedValue);
        EntityObject famObject = famHM[selectedFamilyID + ""];
        Dictionary<string, string[]> famAttributes = famObject.Attributes;

        string selectedStudentStr = ddlStudents.SelectedValue;
        string selectedFamilyStr = ddlFamilies.SelectedValue;

        string selectedPlacement = ddlRmPlacement.SelectedValue;
       
        string selectedRoomNumber = ddlRoom.SelectedValue;
        string selectedEffectiveQuater = txtEffectiveDate.Text;
        string selectedEndQuarter = txtEndDate.Text;

        string effectiveDate = txtEffectiveDate.Text;
        string endDate = txtEndDate.Text;

        //applicantID, familyID, roomNumber, effectiveQuarter, endQuarter
        string sqlQuery = @"SELECT *
                              FROM [CCSInternationalStudent].[dbo].[HomestayPlacement]
                              WHERE applicantID = " + selectedStudentStr
                                    + " AND familyID = " + selectedFamilyStr
                                    + " AND roomNumber = " + selectedRoomNumber
                                    + " AND effectiveQuarter = '" + selectedEffectiveQuater + "'"
		                            + " AND endQuarter = '" + selectedEndQuarter + "'";
        DataTable dtPlacements = hsInfo.RunGetQuery(sqlQuery);
        

        //If "Place"
        if (dtPlacements == null || dtPlacements.Rows.Count == 0)
        {
            //Check room "CCS" or "Non-CCS"
            if (famAttributes["room" + selectedRoomNumber + "Occupancy"][0].Contains("CCS"))
            {
                //divError.Visible = false;
                litErrorMsg.Text = DisplayErrorMsg("Selected room is not available. Please select another room");
                //lblMsg.Text = "Selected room is not available. Please select another room";
                ddlRoom.Focus();
                return;
            }
            //If endQuarter < effectiveQuarter, cannot add
            
            if (!placingUtility.validDateRange(txtEffectiveDate.Text, txtEndDate.Text))
            {

                //lblMsg.Text = "End Quarter cannot be before Effective Quarter";
                txtEndDate.Focus();
                StringBuilder sb = new StringBuilder();

                //divError.Visible = true;
                litErrorMsg.Text = DisplayErrorMsg("End Date cannot be before Effective Date");
                //litErrorMsg.Text = "<ul><li>End Quarter cannot be before Effective Quarter</li></ul>";
                return;
            }

            

            //Check overlap
            //If overlap, cannot add

            if (!placingUtility.ValidateOverlapByDate(selectedFamilyStr, selectedRoomNumber, effectiveDate, endDate))
            {
                //lblMsg.Text = "The record you tried to insert will overlap existing records. Please select re-select or remove existing record to add new record";
                litErrorMsg.Text = DisplayErrorMsg("The record you tried to insert will overlap existing records. Please select re-select or remove existing record to add new record");
                ddlRoom.Focus();
                return;
            }

            /*
            //display list date
            List<Stay> stays = GetStaysByFamilyRoom(selectedFamilyStr, selectedRoomNumber);
            stays.Add(new Stay()
                        {
                            Start = GetClassDate("FirstDayOfClass", selectedEffectiveQuater),
                            End = GetClassDate("LastDayOfClass", selectedEndQuarter),
                        }
                    );
                    
            

            if (!DoesNotOverlap(stays))
            {
                lblMsg.Text = "overlap";
                ddlRoom.Focus();
                return;
            }
           
            */


            //Check overlap by same student: place same student in overlap period
            
            var stays = new List<Stay>();
            sqlQuery = @"SELECT *
                              FROM [CCSInternationalStudent].[dbo].[HomestayPlacement]
                              WHERE  applicantID = " + selectedStudentStr;
            DataTable dtPlacements1 = hsInfo.RunGetQuery(sqlQuery);
            if (dtPlacements1 != null && dtPlacements1.Rows.Count > 0)
            {
                foreach (DataRow dr in dtPlacements1.Rows)
                {
                    //DateTime start = placingUtility.GetClassDate("FirstDayOfClass", dr["effectiveQuarter"].ToString());
                    //DateTime end = placingUtility.GetClassDate("LastDayOfClass", dr["endQuarter"].ToString());
                    DateTime start = placingUtility.GetDateTimeByType("effectiveDate", dr["effectiveQuarter"].ToString());
                    DateTime end = placingUtility.GetDateTimeByType("endDate", dr["endQuarter"].ToString());
                    stays.Add(new Stay(start, end));
                }
            }
            stays.Add(new Stay()
                    {
                        Start = placingUtility.GetDateTimeByType("effectiveDate", effectiveDate),
                        End = placingUtility.GetDateTimeByType("endDate", endDate),
                //Start = placingUtility.GetClassDate("FirstDayOfClass", selectedEffectiveQuater),
                //End = placingUtility.GetClassDate("LastDayOfClass", selectedEndQuarter),
                    });



            if (!placingUtility.DoesNotOverlap(stays))
            {
                //lblMsg.Text = "The student you are trying to place has been placed within the period. ";
                litErrorMsg.Text = DisplayErrorMsg("The student you are trying to place has been placed within that period." +
                                    " Please check the Current Status or Family status. Then select another Effective Date and End Date so that they do not overlap exisiting stays.");
                ddlRoom.Focus();
                return;
            }

            //Check gender overlap. Only one gender can be added in a home. If selected date range overlap existing placements but different gender, cannot add. 
            string sqlQueryAllPlacements = @"SELECT *
                                          FROM [CCSInternationalStudent].[dbo].[HomestayPlacement]
                                          WHERE familyID = " + selectedFamilyStr;
            DataTable dtAllPlacements = hsInfo.RunGetQuery(sqlQueryAllPlacements);

            if (dtAllPlacements != null && dtAllPlacements.Rows.Count > 0)
            {
                //Get start date, end date selected by user
                DateTime selectEffectiveDate = GetDateTimeFromString(effectiveDate);
                DateTime selectEndDateDt = GetDateTimeFromString(endDate);
                string selectedStudentGender = stuHM[selectedStudentStr].Attributes["Gender"][0];
                foreach (DataRow drAllPlacements in dtAllPlacements.Rows)
                {
                    
                    string studentIDInFamily = drAllPlacements["applicantID"].ToString();
                    string studentGenderInFamily = stuHM[studentIDInFamily].Attributes["Gender"][0];

                    //Get startDate, endDate for each row
                    DateTime effectiveDateDr = GetDateTimeFromString(drAllPlacements["effectiveQuarter"].ToString());
                    DateTime endDateDr = GetDateTimeFromString(drAllPlacements["endQuarter"].ToString());
                    if (placingUtility.IsOverlappedPeriods(selectEffectiveDate, selectEndDateDt, effectiveDateDr, endDateDr))
                    {
                        
                        if (!selectedStudentGender.Equals(studentGenderInFamily))
                        {
                            litErrorMsg.Text = DisplayErrorMsg("There is only one gender can be placed in a room. " +
                                                                "The date range you placed the student only allow " + studentGenderInFamily
                                                                + " . Please select another date range or place another student.");
                            txtEffectiveDate.Focus();
                            return;
                        }
                    }
                }
            }


            //Begin Insert
            string homeName = famAttributes["homeName"][0];
            string sqlInsert = @"INSERT INTO [dbo].[HomestayPlacement]
                                (
                                    [placementName]
                                    ,[applicantID]
                                    ,[familyID]
                                    ,[roomNumber]
                                    ,[placementType]
                                    ,[effectiveQuarter]
                                    ,[endQuarter]
                                )
                                VALUES
                                (
                                    @PlacementName
                                    ,@ApplicantID
                                    ,@FamilyID
                                    ,@RoomNumber
                                    ,@PlacementType
                                    ,@EffectiveQuarter
                                    ,@EndQuarter
                                )";
            SqlConnection conn = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand(sqlInsert, conn);
            // set values to parameters from textboxes
            cmd.Parameters.AddWithValue("@PlacementName", homeName);
            cmd.Parameters.AddWithValue("@ApplicantID", selectedStudentStr);
            cmd.Parameters.AddWithValue("@FamilyID", selectedFamilyStr);
            cmd.Parameters.AddWithValue("@RoomNumber", selectedRoomNumber);
            cmd.Parameters.AddWithValue("@PlacementType", selectedPlacement);
            cmd.Parameters.AddWithValue("@EffectiveQuarter", effectiveDate);
            cmd.Parameters.AddWithValue("@EndQuarter", endDate);

            try
            {
                conn.Open();
                cmd.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                
            }
            finally
            {
                conn.Close();
            }

            //hsInfo.runSPQuery(sqlInsert, true);
            //lblMsg.Text = "New record has been inserted!";
            litSuccessMsg.Text = DisplaySuccessMsg("New record has been inserted!");
        }
        else
        {
            //lblMsg.Text = "The record exists.";
            litErrorMsg.Text = DisplayErrorMsg("There is an existing record with same room number, effective quarter and end quarter. Please make another selection!");
            ddlRoom.Focus();
            return;
        }
       


        DisplayPanel2();  
    }

    //Convert dateTimeStr to DateTime; if empty string, return DateTime.MaxValue
    private DateTime GetDateTimeFromString(string dateTimeStr)
    {
        DateTime dt;
        dt = DateTime.TryParse(dateTimeStr, out dt) ? dt : DateTime.MaxValue;
        return dt;
    }

    private string DisplaySuccessMsg(string successMsg)
    {
        divSuccess.Visible = true;
        return successMsg;
    }

    private string DisplayErrorMsg(string errorMsg)
    {
        divError.Visible = true;
        return "<ul><li>"+ errorMsg + "</li></ul>";
    }

    private DataTable GetPlacementsByStudentFamily(string studentID, string familyID)
    {
        string sqlQuery1 = @"SELECT *
                              FROM [CCSInternationalStudent].[dbo].[HomestayPlacement]
                              WHERE applicantID = " + studentID
                                   + " AND familyID = " + familyID
                                   + " ORDER BY effectiveQuarter asc";


        DataTable dtPlacements = hsInfo.RunGetQuery(sqlQuery1);
        return dtPlacements;
    }

    protected void ShowData()
    {
       
        string selectedStudentIDStr = ddlStudents.SelectedValue;
        string familyIDStr = ddlFamilies.SelectedValue;

        gvStuFam.DataSource = GetPlacementsByStudentFamily(selectedStudentIDStr, familyIDStr);
        gvStuFam.DataBind();
    }

    protected void gvStuFam_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvStuFam.EditIndex = e.NewEditIndex;
        ShowData();
        


    }
    protected void gvStuFam_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        divError.Visible = false;
        divSuccess.Visible = false;
       
        //Finding the controls from Gridview for the row which is going to update  
        Label id = (Label)gvStuFam.Rows[e.RowIndex].FindControl("lbl_ID");
        string idStr = id.Text;
        DropDownList ddlPlacementType = (DropDownList)gvStuFam.Rows[e.RowIndex].FindControl("ddlPlacementType");
        DropDownList ddlRoomGv = (DropDownList)gvStuFam.Rows[e.RowIndex].FindControl("ddlRoomGv");
        //DropDownList ddlEffectiveQuarter = (DropDownList)gvStuFam.Rows[e.RowIndex].FindControl("ddlEffectiveQuarter");
        //DropDownList ddlEndQuarter = (DropDownList)gvStuFam.Rows[e.RowIndex].FindControl("ddlEndQuarter");
        TextBox txtEffectiveDateEdit = (TextBox)gvStuFam.Rows[e.RowIndex].FindControl("txtEffectiveDateEdit");
        TextBox txtEndDateEdit = (TextBox)gvStuFam.Rows[e.RowIndex].FindControl("txtEndDateEdit");

        int selectedFamilyID = Convert.ToInt32(ddlFamilies.SelectedValue);
        EntityObject famObject = famHM[selectedFamilyID + ""];
        Dictionary<string, string[]> famAttributes = famObject.Attributes;

        string selectedStudentStr = ddlStudents.SelectedValue;
        string selectedFamilyStr = ddlFamilies.SelectedValue;

        string selectedPlacement = ddlPlacementType.SelectedValue;
        string selectedRoomNumber = ddlRoomGv.SelectedValue;
        //string selectedEffectiveQuater = ddlEffectiveQuarter.SelectedValue;
        //string selectedEndQuarter = ddlEndQuarter.SelectedValue;
        string effectiveDate = txtEffectiveDateEdit.Text;
        string endDate = txtEndDateEdit.Text;
        //TextBox city = GridView1.Rows[e.RowIndex].FindControl("txt_City") as TextBox;


        if (String.IsNullOrEmpty(selectedPlacement)|| selectedPlacement.Contains("Place"))
        {
            //Check room "CCS" or "Non-CCS"
            if (famAttributes["room" + selectedRoomNumber + "Occupancy"][0].Contains("CCS"))
            {
                //lblMsg.Text = "Selected room is not available. Please select another room";
                litErrorMsg.Text = DisplayErrorMsg("Selected room is not available. Please select another room");
                ddlRoomGv.Focus();
                return;
            }
            //Check effectiveQuarter <= endQuarter
            //If endQuarter < effectiveQuarter, cannot add
            if (!placingUtility.validDateRange(txtEffectiveDateEdit.Text, txtEndDateEdit.Text))
            {
                //lblMsg.Text = "End Quarter cannot be before Effective Quarter";
                litErrorMsg.Text = DisplayErrorMsg("End Date cannot be before Effective Date");
                txtEndDateEdit.Focus();
                return;
            }

            //Check overlap

            var stays = placingUtility.GetStaysByFamilyRoomExcludeID(selectedFamilyStr, selectedRoomNumber, idStr);

            stays.Add(new Stay()
            {
                Start = placingUtility.GetDateTimeByType("effectiveDate", effectiveDate),
                End = placingUtility.GetDateTimeByType("endDate", endDate),
                //Start = placingUtility.GetClassDate("FirstDayOfClass", selectedEffectiveQuater),
                //End = placingUtility.GetClassDate("LastDayOfClass", selectedEndQuarter),
            }
                );
            if (!placingUtility.DoesNotOverlap(stays))
            {
                //lblMsg.Text = "The record you tried to insert will overlap existing records. Please select re-select or remove existing record to add new record";
                litErrorMsg.Text = DisplayErrorMsg("The record you tried to insert will overlap existing records. Please re-select or remove existing records to add new record");
                //ddlEffectiveQuarter.Focus();
                txtEffectiveDateEdit.Focus();
                return;
            }


            //Check overlap by same student: place same student in overlap period
            var stays1 = new List<Stay>();
            string sqlQuery = @"SELECT *
                              FROM [CCSInternationalStudent].[dbo].[HomestayPlacement]
                              WHERE  applicantID = " + selectedStudentStr
                               +" AND id <> " + idStr; ;
            DataTable dtPlacements1 = hsInfo.RunGetQuery(sqlQuery);
            if (dtPlacements1 != null && dtPlacements1.Rows.Count > 0)
            {
                foreach (DataRow dr in dtPlacements1.Rows)
                {
                    DateTime start = placingUtility.GetDateTimeByType("effectiveDate", dr["effectiveQuarter"].ToString());
                    DateTime end = placingUtility.GetDateTimeByType("endDate", dr["endQuarter"].ToString());
                    //DateTime start = placingUtility.GetClassDate("FirstDayOfClass", dr["effectiveQuarter"].ToString());
                    //DateTime end = placingUtility.GetClassDate("LastDayOfClass", dr["endQuarter"].ToString());
                    stays1.Add(new Stay(start, end));
                }
            }
            stays1.Add(new Stay()
            {
                Start = placingUtility.GetDateTimeByType("effectiveDate", effectiveDate),
                End = placingUtility.GetDateTimeByType("endDate", endDate)
            });



            if (!placingUtility.DoesNotOverlap(stays1))
            {
                //lblMsg.Text = "The student you are trying to place has been placed within the period. ";
                litErrorMsg.Text = DisplayErrorMsg("The student you are trying to place has been placed within that period." +
                                    " Please check the Current Status or Family status. Then select another Effective Quarter and End Quarter so that they do not overlap exisiting stays.");
                ddlRoomGv.Focus();
                return;
            }

            //Check gender overlap. Only one gender can be added in a home. If selected date range overlap existing placements but different gender, cannot add. 
            string sqlQueryAllPlacements = @"SELECT *
                                          FROM [CCSInternationalStudent].[dbo].[HomestayPlacement]
                                          WHERE familyID = " + selectedFamilyStr;
            DataTable dtAllPlacements = hsInfo.RunGetQuery(sqlQueryAllPlacements);

            if (dtAllPlacements != null && dtAllPlacements.Rows.Count > 0)
            {
                //Get start date, end date selected by user
                DateTime selectEffectiveDate = GetDateTimeFromString(effectiveDate);
                DateTime selectEndDateDt = GetDateTimeFromString(endDate);
                string selectedStudentGender = stuHM[selectedStudentStr].Attributes["Gender"][0];
                foreach (DataRow drAllPlacements in dtAllPlacements.Rows)
                {

                    string studentIDInFamily = drAllPlacements["applicantID"].ToString();
                    string studentGenderInFamily = stuHM[studentIDInFamily].Attributes["Gender"][0];

                    //Get startDate, endDate for each row
                    DateTime effectiveDateDr = GetDateTimeFromString(drAllPlacements["effectiveQuarter"].ToString());
                    DateTime endDateDr = GetDateTimeFromString(drAllPlacements["endQuarter"].ToString());
                    if (placingUtility.IsOverlappedPeriods(selectEffectiveDate, selectEndDateDt, effectiveDateDr, endDateDr))
                    {

                        if (!selectedStudentGender.Equals(studentGenderInFamily))
                        {
                            litErrorMsg.Text = DisplayErrorMsg("There is only one gender can be placed in a room. " +
                                                                "The date range you placed the student only allow " + studentGenderInFamily
                                                                + " . Please select another date range or place another student.");
                            txtEffectiveDateEdit.Focus();
                            return;
                        }
                    }
                }
            }

            //Update
            string sqlUpdate = @"UPDATE [CCSInternationalStudent].[dbo].[HomestayPlacement]
                                SET placementType = '" + ddlPlacementType.SelectedValue + "'"
                                + "  ,roomNumber = '" + ddlRoomGv.SelectedValue + "'"
                                + "  ,effectiveQuarter= '" + effectiveDate + "'"
                                + "  ,endQuarter = '" + endDate + "'"
                                + " WHERE id = " + idStr;
            hsInfo.runSPQuery(sqlUpdate, false);
            //update famHM 

            //lblMsg.Text = "Student has been updated!";

            litSuccessMsg.Text = DisplaySuccessMsg("Student has been updated!");
        }
        else
        {
           
            string sqlDelete = @"DELETE FROM [CCSInternationalStudent].[dbo].[HomestayPlacement]"
                               + " WHERE id = " + idStr;
            hsInfo.runSPQuery(sqlDelete, false);
            //lblMsg.Text = "Record has been removed!";
            litSuccessMsg.Text = DisplaySuccessMsg("Record has been successfully removed!");
        }

        
        //Setting the EditIndex property to -1 to cancel the Edit mode in Gridview  
        gvStuFam.EditIndex = -1;
        //Call ShowData method for displaying updated data  
        ShowData();
        DisplayPanel2();
    }
    protected void gvStuFam_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvStuFam.EditIndex = -1;
        ShowData();
    }

    protected void gvStuFam_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        gvStuFam.Columns[1].Visible = false;
        //DropDownList ddlPlacementType = (DropDownList)e.Row.FindControl("ddlPlacementType");
        //ddlPlacementType.SelectedValue = "Withdrew";
        //ShowData();
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if ((e.Row.RowState & DataControlRowState.Edit) > 0)
            {
                //placementType
                DropDownList ddlPlacementType = (DropDownList)e.Row.FindControl("ddlPlacementType");

               

                DataRowView dr = e.Row.DataItem as DataRowView;
                ddlPlacementType.SelectedValue = dr["placementType"].ToString();

                //roomNumber
                DropDownList ddlRoomGv = (DropDownList)e.Row.FindControl("ddlRoomGv");
                ddlRoomGv.SelectedValue = dr["roomNumber"].ToString();

                /*
                //effectiveQuarter
                DropDownList ddlEffectiveQuarter = (DropDownList)e.Row.FindControl("ddlEffectiveQuarter");
                DataTable dtQuarters = QMD.GetQuarterStart(-3, 15);
                //DataTable dtQuarters = QMD.GetQuarterStart(-1, 1);
                ddlEffectiveQuarter.DataSource = dtQuarters;
                ddlEffectiveQuarter.DataTextField = "textField";
                ddlEffectiveQuarter.DataValueField = "valueField";
                ddlEffectiveQuarter.DataBind();
                ddlEffectiveQuarter.Items.Insert(0, new ListItem("--Select a quarter--", "")); //Add default value


                ddlEffectiveQuarter.SelectedValue = dr["effectiveQuarter"].ToString();

                //endQuarter
                DropDownList ddlEndQuarter = (DropDownList)e.Row.FindControl("ddlEndQuarter");

                //DataTable dtQuarters = QMD.GetQuarterStart(-1, 1);
                ddlEndQuarter.DataSource = dtQuarters;
                ddlEndQuarter.DataTextField = "textField";
                ddlEndQuarter.DataValueField = "valueField";
                ddlEndQuarter.DataBind();
                ddlEndQuarter.Items.Insert(0, new ListItem("--Select a quarter--", "")); //Add default value


                ddlEndQuarter.SelectedValue = dr["endQuarter"].ToString();
                */

            }
        }
    }



    protected void btnAdminPage_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Homestay/Admin/Default.aspx");
    }

    protected void btnPlacementPage_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Homestay/Admin/Matching/EditPlacement.aspx");
    }

    
}