﻿<%@ Page Title="Administration Home Page" Language="C#" MasterPageFile="~/CCSApps.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Homestay_Admin_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="bannerH1Text" Runat="Server">
Homestay Program 
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">
    <h2>Administration Tools</h2>
    <h3>Students</h3>
    <ol>
        <li><a href="/Homestay/Admin/Default.aspx"></a>Student Dashboard</li>
        <li><a href="/Homestay/Students/ApplicationForm_L.aspx">Student Application</a> - Administrative View - Update ctcLink ID</li>
        <li><a href="/Homestay/Admin/StudentContacts.aspx">Student Contacts</a></li>
        <li><a href="/Homestay/Admin/Documents.aspx?type=student">Uploaded Documents</a></li>
        <li><a href="/Homestay/Admin/Notes.aspx">Issues and Notes</a> - Update Student Status</li>
        <li><a href="/Homestay/Admin/StatusMaintenance.aspx">Status Maintenance</a></li>
        <li><a href="/Homestay/Admin/Activation.aspx">Activate/De-activate</a></li>
    </ol>
    <h3>Families</h3>
    <ol>
        <li><a href="/Homestay/Admin/Default.aspx"></a>Family Dashboard</li>
        <li><a href="/Homestay/Families/ApplicationForm2.aspx">Family Application</a> - Administrative View</li>
        <li><a href="/Homestay/Admin/FamilyContacts.aspx">Family Contacts</a></li>
        <li><a href="/Homestay/Admin/Documents.aspx?type=family">Uploaded Documents</a></li>
        <li><a href="/Homestay/Admin/Notes.aspx">Issues and Notes</a> - Update Family Status</li>
        <li><a href="/HomestayFamilies/Admin/ManageAccounts.aspx">Manage Family User Accounts</a></li>
        <li><a href="/Homestay/Admin/StatusMaintenance.aspx">Status Maintenance</a></li>
        <li><a href="/Homestay/Admin/Activation.aspx">Activate/De-activate</a></li>
        
    </ol>
    <h3>Placement</h3>
    <ol>
        <li><a href="/Homestay/Admin/Default.aspx"></a>Placement Dashboard</li>
        <li><a href="/Homestay/Admin/Matching/MatchPreferences.aspx">Match Student/Family Preferences</a></li>
        <li><a href="/Homestay/Admin/Matching/editplacement.aspx">Student/Family Placement</a></li>
        <li><a href="/Homestay/Admin/Default.aspx"></a>Ad Hoc Searches and Reports</li>
        <li><a href="/Homestay/Admin/Default.aspx"></a>Generate Documents</li>
    </ol>
    <h3>Admin Tools</h3>
    <ol>
        <li><a href="/Homestay/Admin/EmailTool/Communication.aspx">Email Tool</a></li>
        <li><a href="/Homestay/Admin/Reports/ReportsHome.aspx">Reports and Forms</a></li>
    </ol>



</asp:Content>

