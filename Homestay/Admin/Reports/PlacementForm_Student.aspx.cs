﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Homestay_Admin_Reports_PlacementForm_Student : System.Web.UI.Page
{
    string strResultMsg = "";
    String selectedStudent = "0";
    DateTime dob;
    String arrivalDate;
    String shortDate = "";
    Homestay hsInfo = new Homestay();
    DataTable dtDisplay;
    HomestayUtility hsUtility = new HomestayUtility();
    protected void Page_Load(object sender, EventArgs e)
    {
        //Get studentID from querystring
        int intFamilyID = GetQueryStringValue("familyID");
        dtDisplay = GetDisplayDt();
        //Populate ddlStudent
        if (!IsPostBack)
        {
            DataTable dtFamilies = hsInfo.GetHomestayFamilyInfo(0, "", "DDLactive");
            
            ddlFamily.DataSource = dtFamilies;
            ddlFamily.DataTextField = "homeName";
            ddlFamily.DataValueField = "id";
            ddlFamily.DataBind();
            ddlFamily.Items.Insert(0, new ListItem("--Select a family--", "0")); //Add default value
            litResutlMsg.Text = strResultMsg;

            BindDetailView();
            //Precheck for smoking policy
            //SmokingPolicy
            string otherSmokingSelection = "";
            DataTable dtFamPref = hsInfo.GetHomestayPreferenceSelections(0, intFamilyID);
            if (dtFamPref != null && dtFamPref.Rows.Count > 0)
            {
                foreach (DataRow drFamPref in dtFamPref.Rows)
                {
                    if (drFamPref["fieldID"].ToString().Contains("rdoOtherSmokerOK"))
                    {
                        otherSmokingSelection = drFamPref["fieldValue"].ToString();
                        if (otherSmokingSelection.Contains("preference"))
                        {
                            otherSmokingSelection = "NoPreference";
                        }
                        else if (otherSmokingSelection.Contains("not OK"))
                        {
                            otherSmokingSelection = "No";
                        }
                        else
                        {
                            otherSmokingSelection = "Yes";
                        }
                    }
                }
            }
            CheckBoxList checkBoxSmoking = (CheckBoxList)dvPlacement.FindControl("CheckboxSmoking");
            foreach (ListItem item in checkBoxSmoking.Items)
            {
                if (otherSmokingSelection.Equals(item.Value))
                {
                    item.Selected = true;
                }
            }

            ddlFamily.SelectedValue = intFamilyID + "";

            //Populate email
            int intStudentID = GetQueryStringValue("studentID");
            DataTable dtStudent = hsInfo.GetOneInternationalStudent("", intStudentID, "", "", "", "");
            
            if (dtStudent != null && dtStudent.Rows.Count > 0)
            {
                DataRow drStudent = dtStudent.Rows[0];
                txtTo.Text = drStudent["Email"].ToString().Trim();
            }
        }// NOT IsPostback
    }

    private DataTable GetDisplayDt()
    {
        //int intFamilyID = 0;
        //intFamilyID = (int.TryParse(ddlFamily.SelectedValue, out intFamilyID)) ? intFamilyID : 0; 
        int intFamilyID = GetQueryStringValue("familyID");
        int intStudentID = GetQueryStringValue("studentID");
        int intPlacementID = GetQueryStringValue("placementID");
        

        DataTable dtDisplay = new DataTable();
        dtDisplay.Columns.AddRange(new DataColumn[13]
                    {

                        new DataColumn("Student", typeof(string)),
                        new DataColumn("DOB", typeof(string)),
                        new DataColumn("Country", typeof(string)),
                        new DataColumn("EffectiveDate", typeof(string)),
                        new DataColumn("EndDate", typeof(string)),
                        new DataColumn("HomestayFamily", typeof(string)),
                        new DataColumn("Address", typeof(string)),
                        new DataColumn("PhoneNumber", typeof(string)),
                        new DataColumn("Email", typeof(string)),
                        new DataColumn("Hobbies", typeof(string)),
                        new DataColumn("Transportation", typeof(string)),
                        new DataColumn("SmokingPolicy", typeof(string)),
                        new DataColumn("AirportPickup", typeof(string))
                    });
        DataRow displayRow = dtDisplay.NewRow();

        //Student, DOB, Country
        DataTable dtStudent = hsInfo.GetOneInternationalStudent("", intStudentID, "", "", "", "");
        if (dtStudent != null && dtStudent.Rows.Count > 0)
        {
            DataRow drStudent = dtStudent.Rows[0];
            displayRow["Student"] = drStudent["familyName"].ToString().Trim() + ", " + drStudent["firstName"].ToString().Trim();
            displayRow["DOB"] = hsUtility.GetShortDateString(drStudent["DOB"].ToString().Trim());
            displayRow["Country"] = drStudent["Country"].ToString();
        }
        //Effetive Date & EndDate
        string sqlQuery = @"SELECT *
                              FROM [CCSInternationalStudent].[dbo].[HomestayPlacement]
                              where id =" + intPlacementID;
        DataTable dtPlacement = hsInfo.RunGetQuery(sqlQuery);
        if (dtPlacement != null && dtPlacement.Rows.Count > 0)
        {
            displayRow["EffectiveDate"] = dtPlacement.Rows[0]["effectiveQuarter"].ToString().Trim();
            displayRow["EndDate"] = dtPlacement.Rows[0]["endQuarter"].ToString().Trim();
        }
        //Homestay Family and Children
        string familyMembers = "";
        string emails = "";
        DataTable dtRelatives = hsInfo.GetHomestayRelatives(0, intFamilyID);
        if (dtRelatives != null && dtRelatives.Rows.Count > 0)
        {
            foreach (DataRow drRelative in dtRelatives.Rows)
            {
                string relationship = drRelative["relationship"].ToString().Trim();
                familyMembers += drRelative["familyName"].ToString().Trim() + ", " + drRelative["firstName"].ToString().Trim() + "<br/>";
                if (relationship.Contains("Primary") || relationship.Contains("Secondary"))
                {
                    emails += drRelative["email"].ToString().Trim() + "<br/>";
                }

            }
        }
        displayRow["HomestayFamily"] = familyMembers;

        //Emails
        displayRow["email"] = emails;

        //Address
        string address = "";
        string phoneNumber = "";
        string transportation = "";
        DataTable dtFamily = hsInfo.GetHomestayFamilyInfo(intFamilyID, "", "ID");
        
        if (dtFamily != null && dtFamily.Rows.Count > 0)
        {
            DataRow drFamily = dtFamily.Rows[0];
            address = String.Format("{0}, {1}, {2} {3}"
                                    , drFamily["streetAddress"].ToString().Trim()
                                    , drFamily["city"].ToString().Trim()
                                    , drFamily["state"].ToString().Trim()
                                    , drFamily["ZIP"].ToString().Trim()
                            );  
            phoneNumber = drFamily["landlinePhone"].ToString().Trim();
            phoneNumber = drFamily["landlinePhone"].ToString().Trim();
            transportation = String.Format("<b>SFCC:</b> {0} minute walk to the bus; {1} minute ride to SFCC <br/> " +
                                            "<b>SCC:</b> {0} minute walk to the bus; {2} minute ride to SCC"
                                            , drFamily["walkingBusStopMinutes"].ToString().Trim()
                                            , drFamily["busMinutesSFCC"].ToString().Trim()
                                            , drFamily["busMinutesSCC"].ToString().Trim());
        }
        displayRow["Address"] = address;

        //PhoneNumber
        displayRow["PhoneNumber"] = phoneNumber;

        
        //Hobbies
        displayRow["Hobbies"] = string.Join(", ", hsUtility.GetPreferenceSelectionsByType("hobbies", "family", intFamilyID).ToArray());
        //Transportation
        displayRow["Transportation"] = transportation;
       
        //displayRow["SmokingPolicy"] = otherSmokingSelection;
        //AirportPickup
        displayRow["AirportPickup"] = @"The family will pick you up at the airport. 
                                        Meet the family in the baggage claim area. Be sure your flight terminates at the GEG-Spokane Airport. 
                                        It is helpful to check with your family about travel dates before you buy your ticket. 
                                        <b>Please send your travel itinerary to: InternationalHomestay@ccs.spokane.edu</b> and to your family. 
                                        Please be sure to carry your family's phone number and email address with you on your flight. 
                                        If you arrive and cannot find your family at the airport, call: Ashley at 208-821-7626 or Amy at 509-844-5150 or Heather at 509-389-7368.";


        dtDisplay.Rows.Add(displayRow);
        return dtDisplay;
    }
    private void BindDetailView()
    {
        //Bind dtDisplay to dvPlacement
        dvPlacement.DataSource = dtDisplay;
        dvPlacement.DataBind();

    }
    private string GetShortDateString(string dateTimeStr)
    {
        DateTime arrivalDate;
        return DateTime.TryParse(dateTimeStr, out arrivalDate) ? arrivalDate.ToShortDateString() : "n/a";
    }
    protected void linkBtnHome_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Homestay/Admin/Default.aspx");
    }

    
   
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        Response.Redirect("./PlacementForm_Student.aspx?familyID=" + ddlFamily.SelectedValue);
        GetDisplayDt();
        BindDetailView();
    }

    private string GetHobbies()
    {
        int intFamilyID = ParseInt(ddlFamily.SelectedValue);
        string strHobbies = "";
        DataTable dtPref = hsInfo.GetHomestayPreferenceSelections(0, intFamilyID);
        if (dtPref != null && dtPref.Rows.Count > 0)
        {
            string[] hobbies = { "MusicalInstrument", "Art" ,"TeamSports", "IndividualSports" ,"ListenMusic","Drama","WatchMovies",
                                "Singing","Shopping","ReadBooks","Outdoors","Cooking", "Photography", "Gaming"};
            foreach (DataRow dtFamPrefRow in dtPref.Rows)
            {
                string fieldIDStr = dtFamPrefRow["fieldID"].ToString().Trim();
                string fieldValueStr = dtFamPrefRow["fieldValue"].ToString().Trim();
                if (Array.Exists(hobbies, element => element == fieldValueStr))
                {
                    strHobbies += dtFamPrefRow["Preference"].ToString().Trim() + ", ";
                }
            }

            DataTable dtFamilyInfo = hsInfo.GetHomestayFamilyInfo(intFamilyID, "", "ID");
            if (dtFamilyInfo != null && dtFamilyInfo.Rows.Count > 0)
            {
                string otherHobbies = dtFamilyInfo.Rows[0]["otherHobbies"].ToString().Trim();
                if (!String.IsNullOrEmpty(otherHobbies))
                {
                    strHobbies += "; " + otherHobbies;
                }
            }
            strHobbies = strHobbies.Trim();
            if (!String.IsNullOrEmpty(strHobbies) && strHobbies[strHobbies.Length - 1] == ',')
            {
                strHobbies = strHobbies.Substring(0, strHobbies.Length - 1);
            }
        }
        return strHobbies;
    }

    private int ParseInt(string value)
    {
        int result;
        return int.TryParse(value, out result) ? result : 0;
    }
    public int GetQueryStringValue(string queryString)
    {
        int id = 0;
        if ((Request.QueryString[queryString] != null && Request.QueryString[queryString].ToString() != "0"))
        {
            id = hsUtility.ParseInt(Request.QueryString[queryString]);
        }
        return id;
    }

    protected void SendMail_Click(object sender, EventArgs e)
    {
        //public string SendEmailMsg(string strTo, string strFrom, string strSubject, bool blIsHTML, string strBody, string strCCEmailAddresses)
        Utility utility = new Utility();
        string body = GetGridviewData(dvPlacement);
        string subject = "Information about your International Homestay Family";
        string result = "";
        int intStudentID = GetQueryStringValue("studentID");
        //Student, DOB, Country
        /*
        DataTable dtStudent = hsInfo.GetOneInternationalStudent("", intStudentID, "", "", "", "");
        string toEmail = "";
        if (dtStudent != null && dtStudent.Rows.Count > 0)
        {
            DataRow drStudent = dtStudent.Rows[0];
            toEmail = drStudent["Email"].ToString().Trim();
        }
        */
        string[] toEmailList = txtTo.Text.Split(';');
        foreach (string toEmail in toEmailList)
        {
            result = utility.SendEmailMsg(toEmail, "Internationalhomestay@ccs.spokane.edu", subject, true, body, txtCC.Text);
        }

        if (result.Contains("Success"))
        {
            litResultMsg.Text = "An Email has been sent to the student.";
        }

    }

    // This Method is used to render gridview control
    public string GetGridviewData(DetailsView gv)
    {
        DetailsView gv2 = gv;
        StringBuilder strBuilder = new StringBuilder();
        StringWriter strWriter = new StringWriter(strBuilder);
        HtmlTextWriter htw = new HtmlTextWriter(strWriter);
        gv2.RenderControl(htw);
        return strBuilder.ToString();
    }

    //This function to prevent exception when render page
    public override void VerifyRenderingInServerForm(Control control)
    {
        return;
    }

}