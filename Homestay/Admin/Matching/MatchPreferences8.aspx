﻿<%@ Page Title="Matching" Language="C#" MasterPageFile="~/CCSApps.master" AutoEventWireup="true" CodeFile="MatchPreferences8.aspx.cs" Inherits="Homestay_Admin_MatchPreferences8" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <style>
    

    

    .validation-summary-container {
        display: none;
        border-style: solid;
        border-width: 1px;
        border-radius: 3px;
        margin: 10px;
        padding-left: 10px,;
    }

    .errormark.validation-summary-container {
        background-color: #fdd6d7;
        border-color: #ff0000;
    }

    .validation-summary-errors.errormark::before {
        background: url("../../images/iconMessages.png") 0px -48px;
        content: '';
        width: 24px;
        height: 24px;
        position: absolute;
        overflow: hidden;
    }

    .validation-summary-errors.errormark > span {
        padding-left: 32px;
        display: none;
    }

    .validation-summary-errors.errormark > UL {
        padding-left: 50px;
    }

    

    .validation-summary-container.errormark {
        display: block;
        border-style: solid;
        border-width: 1px;
        border-radius: 3px;
        margin: 10px;
        padding-left: 10px,;
    }

   


    .validation-summary-container > DIV {
        padding-top: 8px;
        padding-left: 8px;
    }

    
    .successmark.validation-summary-container {
       
        background-color: #90b60d;
        border-color: white;
    }
   
    /*For success */

    .validation-summary-success.successmark::before {
        background: url("../../images/iconMessages.png") 0px -24px;
        content: '';
        width: 24px;
        height: 24px;
        position: absolute;
        overflow: hidden;
        
    }
    
    .validation-summary-success.successmark > span {
        color:white;
        padding-left: 32px;
        margin-bottom : 10px;
        font-size:18px;
        display: block;
    }

    .validation-summary-success.successmark > UL {
        padding-left: 50px;
    }

    
    .validation-summary-container.successmark {
        display: block;
        border-style: solid;
        border-width: 1px;
        border-radius: 3px;
        margin: 10px;
        padding-left: 10px;
    }
    
        

   
}
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="bannerH1Text" Runat="Server">
    Match Preferenes
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">
     <asp:Button ID="btnReset" runat="server" Text="Reset" OnClick="btnReset_Click" />
    
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <br/><i>* required</i>
            <h3>Students</h3>
            <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
            <b>Select a student</b><br />
            <asp:DropDownList ID="ddlStudents" runat="server" AppendDataBoundItems="true" 
                AutoPostBack="true">

            </asp:DropDownList> *
             
            <br /><br />
            <b>Match Criteria</b>
            <div style="width:100%;float:left; margin-bottom:10px; ">
                <div style="float:left;width:200px;">
                    <label for="rdoGender"><strong>Gender</strong></label>
                    <asp:RadioButtonList ID="rdoGender" RepeatDirection="Horizontal" runat="server">
                        <asp:ListItem Value="Include" Selected="True"></asp:ListItem>
                        <asp:ListItem Value="Omit"></asp:ListItem>
                    </asp:RadioButtonList>
                </div>
                <div style="float:left;width:200px;">
                     <label for="rdoHomestay"><strong>Homestay Preference</strong> </label>
                    <asp:RadioButtonList ID="rdoHomestay" RepeatDirection="Horizontal" runat="server">
                        <asp:ListItem Value="Include" Selected="True"></asp:ListItem>
                        <asp:ListItem Value="Omit"></asp:ListItem>
                    </asp:RadioButtonList>
                </div>
                <div style="float:left;width:300px;">
                     <label for="rdoSmoking"><strong>Smoking Habits</strong></label>
                    <asp:RadioButtonList ID="rdoSmoking" RepeatDirection="Horizontal" runat="server">
                        <asp:ListItem Value="Include" Selected="True"></asp:ListItem>
                        <asp:ListItem Value="Omit"></asp:ListItem>
                    </asp:RadioButtonList>
                </div>
            </div>
           
           <br />
            
            <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" OnClientClick="return ValidateSelectStudent();"/>
        </ContentTemplate>

       

    </asp:UpdatePanel>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" DynamicLayout="true" AssociatedUpdatePanelID="UpdatePanel1" DisplayAfter="500" >

       <ProgressTemplate>
          <img src="../../images/demo_wait.gif" />
           Please Wait...
       </ProgressTemplate>
   
    </asp:UpdateProgress>
    <br />
    
    
    <asp:UpdatePanel ID="UpdatePanel2" runat="server"  >
        <ContentTemplate>
            <!--Start:  studentStatusID-->
            <div id="studentStatusID" runat="server" >
                <b>Current Status</b><br />
                <asp:Literal ID="litCurrStudentStatus" runat="server" Text=""></asp:Literal>
                <asp:GridView ID="GvStudentStatus" AutoGenerateColumns="False" 
                       Width="100%" 
                        cellpadding="5"
                        cellspacing="5"
                       runat="server">
                    <HeaderStyle BackColor="#448BC1" Font-Bold="False" ForeColor="White" />
                    <AlternatingRowStyle BackColor="AliceBlue"></AlternatingRowStyle>
                           <Columns>
                               <asp:BoundField DataField="homeName" HeaderText="Home name"  ItemStyle-Width="20%"  />
                               <asp:BoundField DataField="placementType" HeaderText="Placement Type"  ItemStyle-Width="30%" />
                               <asp:BoundField DataField="roomNumber" HeaderText="Room Number" ItemStyle-Width="10%"/>
                               <asp:BoundField DataField="effectiveQuarter" HeaderText="Effective Quarter" ItemStyle-Width="20%"/>
                               <asp:BoundField DataField="endQuarter" HeaderText="End Quarter" ItemStyle-Width="20%"/>
                        </Columns>
                </asp:GridView>
            </div>
            <!--End:  studentStatusID-->

           <!--Start:  familySelectionID-->
           <div id="familySelectionID" runat="server">
               <h3>Families</h3>
            <asp:Label ID="litRoomWithOtherFamily" runat="server" Text=""></asp:Label>
            <asp:Literal ID="litFoundFamily" runat=server></asp:Literal>   
            <br />
             <div style="margin-bottom:10px;width:40%;float:left;"> 
                  <asp:DropDownList ID="ddlFamilies" runat="server" Width="200px" >
               
                    </asp:DropDownList>
                 <asp:Label ID="litFamilyPlaced" runat="server" Text=""></asp:Label>
                 <asp:Button ID="btnCompare" runat="server" Text="Compare"  OnClientClick="return ValidateSelectFamily();" OnClick="btnCompare_Click"/>
             </div> 
           
                 <!--Start:  familyStatusID-->
                 <div id="familyStatusID" runat="server" visible="false" style="margin-bottom:10px;width:100%;float:left;"> 
                     <b>Room status: </b> <asp:Label ID="lblFamilyStatus" runat="server" Text="" visible="false"/>
                     <asp:GridView ID="GvAllRoom" AutoGenerateColumns="False" 
                       emptydatatext="N/A"  Width="100%" 
                        cellpadding="5"
                        cellspacing="5"
                       runat="server" >
                    <HeaderStyle BackColor="#448BC1" Font-Bold="true" ForeColor="White" />
                    <AlternatingRowStyle BackColor="AliceBlue"></AlternatingRowStyle>
                           <Columns>
                               <asp:BoundField DataField="room" HeaderText="Room"  ItemStyle-Width="10%" />
                               <asp:BoundField DataField="availableStarting" HeaderText="Available starting"  ItemStyle-Width="40%" />
                               <asp:BoundField DataField="occupancy" HeaderText="Occupancy" ItemStyle-Width="50%" HtmlEncode="False" />
                               
                        </Columns>
                    </asp:GridView>
                 </div> 
                 <!--End:  familyStatusID-->
           </div>
            <!--End:  familySelectionID-->
           
            <!--
            <p>
                
                <b>Notes:</b> <br />
                <i>If the student has been placed:
                    <ul>
                        <li>You can change "Placement" and "Effective Quarter" of the student.</li>
                        <li> You must withdraw student from the room before moving the student to another home/room.</li>
                    </ul>
                </i>
            </p>
            -->
            <!--Start:  studentFamilyPlacementID-->
            <div id="studentFamilyPlacementID" runat="server" visible="false" style="width:100%; float:left">
                <h2>Student-Family Placement</h2>
            <div class="validation-summary-container errormark validation-summary-valid"  ID="divError" runat="server" Visible="false">
                <div class="errormark validation-summary-errors" data-valmsg-summary="true" id="valSummaryPanel">
                    <span>Sign-in unsuccessful.  Please try again.</span>
                    <asp:Literal id="litErrorMsg" runat="server" />
                </div>
            </div>
            <div class="validation-summary-container successmark validation-summary-valid"  ID="divSuccess" runat="server" Visible="false">
                <div class="successmark validation-summary-success" data-valmsg-summary="true" >
                    <span><asp:Literal id="litSuccessMsg" runat="server" /></span>
                </div>
            </div>
            <asp:Label ID="lblMsg" runat="server" ForeColor="Red"></asp:Label>
            <div id="insertDiv" runat="server" Visible="false">
               
            <strong>Place student</strong><br />
            <i>You can place a student here:</i><br/>
                
            
            <div style="width:200px;float:left;">
                 <b>Room Number*</b> <br />
                <asp:DropDownList ID="ddlRoom" runat="server">
                   
                  </asp:DropDownList>
            </div>
             <div style="width:300px;float:left;">
                 <b>Effective Quarter *</b> <br />
                <asp:DropDownList ID="ddlEffectiveQtr" runat="server">
                        
                  </asp:DropDownList>
            </div>
            <div style="width:300px;float:left;">
                 <b>End Quarter</b> <br />
                <asp:DropDownList ID="ddlEndQtr" runat="server">
                        
                  </asp:DropDownList>
            </div>
                <div style="width:300px;float:left;">

                 <b>Placement Type</b> <br />
                <asp:DropDownList ID="ddlRmPlacement" runat="server">
                        <asp:ListItem Value="">--Select a placement type--</asp:ListItem>
                        <asp:ListItem Value="Tentative Placement"></asp:ListItem>
                        <asp:ListItem Value="Student Placed"></asp:ListItem>
                  </asp:DropDownList>
            </div>
            <br /><br />
             <div style="width:100%">
                <asp:Button ID="btnPlace" runat="server" Text="Place" OnClick="btnPlace_Click" /> <br />
                 
                  <asp:Label ID="litPlace" runat="server" Text=""></asp:Label>
                 
                  
            </div>

          </div> <!-- End insertDiv -->
            <br /><br />
            <asp:GridView ID="gvStuFam" runat="server" AutoGenerateColumns="False" CellPadding="6" 
                OnRowCancelingEdit="gvStuFam_RowCancelingEdit"   
                OnRowDatabound="gvStuFam_RowDataBound"
                OnRowEditing="gvStuFam_RowEditing" 
                OnRowUpdating="gvStuFam_RowUpdating">  
            <Columns>  
                <asp:TemplateField>  
                    <ItemTemplate>  
                        <asp:Button ID="btn_Edit" runat="server" Text="Edit" CommandName="Edit" />  
                    </ItemTemplate>  
                    <EditItemTemplate>  
                        <asp:Button ID="btn_Update" runat="server" Text="Update" CommandName="Update" />  
                        <asp:Button ID="btn_Cancel" runat="server" Text="Cancel" CommandName="Cancel"/>  
                    </EditItemTemplate>  
                </asp:TemplateField>  
                <asp:TemplateField HeaderText="id">
                    <ItemTemplate>
                        <asp:Label ID="lbl_id" runat="server"  Text='<%#Eval("id") %>'></asp:Label>
                        
                    </ItemTemplate>
                </asp:TemplateField>
                 
                <asp:TemplateField HeaderText="Room Number">  
                    <ItemTemplate>  
                        <asp:Label ID="lbl_roomNumber" runat="server" Text='<%#Eval("roomNumber") %>'></asp:Label>  
                    </ItemTemplate>  
                    <EditItemTemplate>  
                        <asp:DropDownList ID="ddlRoomGv" runat="server">
                            <asp:ListItem Value="">--Select a room--</asp:ListItem>
                            <asp:ListItem Value="1">Room 1</asp:ListItem>
                            <asp:ListItem Value="2">Room 2</asp:ListItem>
                            <asp:ListItem Value="3">Room 3</asp:ListItem>
                            <asp:ListItem Value="4">Room 4</asp:ListItem>
			            </asp:DropDownList>
                    </EditItemTemplate>  
                </asp:TemplateField>  
                <asp:TemplateField HeaderText="Effective Quarter">  
                    <ItemTemplate>  
                        <asp:Label ID="lbl_effectiveQuarter" runat="server" Text='<%#Eval("effectiveQuarter") %>'></asp:Label>  
                    </ItemTemplate>  
                    <EditItemTemplate>  
                        <asp:DropDownList ID="ddlEffectiveQuarter" runat="server">

                        </asp:DropDownList>
                    </EditItemTemplate>  
                </asp:TemplateField>  
                <asp:TemplateField HeaderText="End Quarter">  
                    <ItemTemplate>  
                        <asp:Label ID="lbl_endQuarter" runat="server" Text='<%#Eval("endQuarter") %>'></asp:Label>  
                    </ItemTemplate>  
                    <EditItemTemplate>  
                        <asp:DropDownList ID="ddlEndQuarter" runat="server">

                        </asp:DropDownList>
                    </EditItemTemplate>  
                </asp:TemplateField>  
                <asp:TemplateField HeaderText="Placement Type">  
                    <ItemTemplate>  
                        <asp:Label ID="lbl_placementType" runat="server" Text='<%#Eval("placementType") %>'></asp:Label>  
                    </ItemTemplate>  
                    <EditItemTemplate>
			            <asp:DropDownList ID="ddlPlacementType" runat="server">
                            <asp:ListItem Value="">--Select a placement type--</asp:ListItem>
                            <asp:ListItem Value="Tentative Placement"></asp:ListItem>
                            <asp:ListItem Value="Student Placed"></asp:ListItem>
                            <asp:ListItem Value="Withdrew"></asp:ListItem>
                            <asp:ListItem Value="Graduated"></asp:ListItem>
                            <asp:ListItem Value="Student Moved Out"></asp:ListItem>
			            </asp:DropDownList>
		            </EditItemTemplate>  
                </asp:TemplateField>
            </Columns>  
            <HeaderStyle BackColor="#663300" ForeColor="#ffffff"/>  
            <RowStyle BackColor="#e7ceb6"/>  
        </asp:GridView>  
            </div>
            <!--End:  studentFamilyPlacementID-->
            
            
           <br />
            <!--Start: informationID-->
                <div style="width:100%;" id="informationID" runat="server" visible="false">
                <div style="width:70%;float:left;margin-right:10px">
                    <h4>Comparison</h4>
                    <asp:GridView ID="GridView1" AutoGenerateColumns="False" 
                       emptydatatext="N/A"  Width="100%" 
                        cellpadding="5"
                        cellspacing="5"
                       runat="server">
                    <HeaderStyle BackColor="#448BC1" Font-Bold="False" ForeColor="White" />
                    <AlternatingRowStyle BackColor="AliceBlue"></AlternatingRowStyle>
                           <Columns>
                               <asp:BoundField DataField="DisplayText" HeaderText="Field"  ItemStyle-Width="20%" ItemStyle-BackColor="#CCCCCC" />
                               <asp:BoundField DataField="StudentValue" HeaderText="Student"  ItemStyle-Width="40%" />
                               <asp:BoundField DataField="FamilyValue" HeaderText="Family" ItemStyle-Width="40%"/>
                               
                        </Columns>
                    </asp:GridView>
                </div>
                <div style="width:20%;float:left">
                    <h4>Matching score</h4>
                    <asp:GridView ID="GridView2" AutoGenerateColumns="False" 
                               emptydatatext="N/A"  Width="95%" 
                                cellpadding="5"
                                cellspacing="5"
                               runat="server">
                            <HeaderStyle BackColor="#448BC1" Font-Bold="False" ForeColor="White" />
                            <AlternatingRowStyle BackColor="AliceBlue"></AlternatingRowStyle>
                                   <Columns>
                                       <asp:BoundField DataField="Field" HeaderText="Field"  ItemStyle-Width="80%" ItemStyle-BackColor="#CCCCCC" />
                                       <asp:BoundField DataField="ScoreMatch" HeaderText="Percent"   />
                               
                               
                                </Columns>
                  </asp:GridView>
                </div>
            </div>
            <!--End: informationID-->
            
          
            
            
            
            <table style="width: 80%;">
                <tr>
                    <td style=" vertical-align:top">
                        <asp:Literal ID="litSelectedStuInfo" runat=server></asp:Literal>  
                    </td>
                    <td style="vertical-align:top">
                        <asp:Literal ID="litFamilyInfo" runat=server></asp:Literal>  
                    </td>
                </tr>
                
                
            </table>
            <asp:Literal ID="litResultMsg" runat=server></asp:Literal>   
        </ContentTemplate>

    </asp:UpdatePanel>

    

    
    <script type="text/javascript">
        
        function ValidateSelectStudent() {
            var ddl = $('#<%=ddlStudents.ClientID%>');
            var selectedValue = ddl.val();
  
            //alert(selectedValue); //SelVal is the selected Value
            if (selectedValue == 0) {
                 alert("Please select a student to search");
                return false;
            }
            return true;
        }

        function ValidateSelectFamily() {
            var ddl = $('#<%=ddlFamilies.ClientID%>');
            var selectedValue = ddl.val();
  
            //alert(selectedValue); //SelVal is the selected Value
            if (selectedValue == 0) {
                 alert("Please select a family to compare");
                return false;
            }
            return true;
        }

        function ValidatePlacement() {
            var ddlRoom = $('#<%=ddlRoom.ClientID%>');
            var ddlEffectiveQtr = $('#<%=ddlEffectiveQtr.ClientID%>');
            var ddlEndQtr = $('#<%=ddlEndQtr.ClientID%>');
           
            if (ddlRoom.val() == "") {
                alert("Please select a room to place");
                ddlRoom.focus();
                return false;
            }
            if (ddlEffectiveQtr.val() == ""  )
            {
                //alert("Please select an effective quarter");
                ddlEffectiveQtr.focus();
                $('#<%=divError.ClientID%>').show();
                var list = $('<ul/>').appendTo('#valSummaryPanel');
                list.append('<li>Please select a room to place</li>');
                //for (var i = 0; i < 10; i++) {
                    // New <li> elements are created here and added to the <ul> element.
                  //  list.append('<li>something</li>');
                //}
                return false;
            }

           
            
            return true;
         }

        
        $(function () {
            
            
            //alert("hello");
                //Get data as JSON type from REST API
            /*
            $.ajax({
                type: "POST",
                url: "MatchingWS.asmx/GetDemo",
                contentType: "application/json",
                data: {applicantID : "abc"},
                //dataType: "json"
            })
            .done(function (data) {
                //alert("success");

                console.log(data);
                //displayTable(data);
            })
            .fail(function (xhr) {
                console.log('error', xhr);
            });
            */
        });
        
        
    </script>
    </asp:Content>