﻿using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Homestay_Families_PlacementProfile : System.Web.UI.Page
{
    String strButtonFace = "";
    String strAppPart = "";
    String strResultMsg = "";
    Int32 intResult = 0;
    Int32 intFamilyID = 0;
    String FamilyID = "";
    String strFamilyStatus = "App Submitted";
    String appStatus = "";
    Boolean blWaiverSubmitted = false;
    Boolean blEmailSent = false;
    String strUploadURL = @"\\ccs-internet\InternetContent\CCS\Homestay\";
    String strFileName = "";
    String strUploadServer = "";
    String strHomeName = "";
    String strPrintPage = "false";
    String strUserName = "";
    String allHomestayOptions = "";
    Boolean blIsAdmin = false;

/*
    String TraveledWhere = "";
    String SmokingHabits = "";
    String HomeEnvironmentPreferences = "";
    String ActivitiesEnjoyed = "";
    String AnythingElse = "";
    String HomestayOption = "";
    String GenderPreference = "";
*/
    public static String intlEmail = "globalprograms@ccs.spokane.edu";
    string sendTo = "";
    string sentFrom = "Internationalhomestay@ccs.spokane.edu";
    string strCCaddresses = "Internationalhomestay@ccs.spokane.edu;laura.padden@ccs.spokane.edu";
    string strSubject = "International Homestay Family Application Submission";
    string strEmailResult = "";
    
    Homestay hsInfo = new Homestay();

    protected void Page_Load(object sender, EventArgs e)
    {

        litMissingUploads.Text = "";
        litPageTitle.Text = "Family Profile";
        if (Request.Form["btnSubmit"] != null)
        {
            strButtonFace = Request.Form["btnSubmit"].ToString();
        }
        if (Request.Form["part"] != null)
        {
            strAppPart = Request.Form["part"].ToString();
            if (strAppPart == "3") { strButtonFace = "Done"; }
        }
        if (Request.QueryString["UID"] != null)
        {
            strUserName = Request.QueryString["UID"].ToString();
        }
        if (Request.ServerVariables["LOGON_USER"] != null)
        {
            //set admin status for visibility control
            blIsAdmin = true;
        }
        if (Request.QueryString["admin"] != null)
        {
            blIsAdmin = Convert.ToBoolean(Request.QueryString["admin"].ToString());
        }
        if (blIsAdmin)
        {
            divHome.Visible = true;
        }
        //set up page for printing
        if (Request.QueryString["print"] != null)
        {
            divWaiver.Visible = true;
            divConfirmation.Visible = false;
            btnDone.Visible = false;
            btnSave.Visible = false;
            btnPrevious.Visible = false;
            fldProfile.Visible = false;
            strPrintPage = "true";
            divPrint.Visible = true;
            divPrintWaiver.Visible = false;
            divNextPage.Visible = false;
            divUpload.Visible = false;
            divHome.Visible = false;
            divCheckbox.Visible = false;
            hdnAppStatus.Value = "Printable Waiver";
        }
        //track app status
        appStatus = hdnAppStatus.Value;
        if (strButtonFace != "") { appStatus = strButtonFace; }
        if (appStatus == "") { appStatus = "default"; }
        hdnAppStatus.Value = appStatus;
        //strResultMsg += "Button Face: " + strButtonFace + "; App Status: " + appStatus + "; Print? " + strPrintPage + "<br />";

        if (Request.QueryString["ID"] != null)
        {
            //page load
            FamilyID = Request.QueryString["ID"].ToString();
            hdnFamilyID.Value = FamilyID;
        }
        //Set default visibility
        if (appStatus == "default")
        {
            divConfirmation.Visible = false;
            btnDone.Visible = false;
            divNextPage.Visible = true;
            btnPrevious.Visible = true;
            fldProfile.Visible = true;
            divWaiver.Visible = false;
            if (blIsAdmin)
                divHome.Visible = true;
            else
                divHome.Visible = false;
        }
        if (FamilyID == "")
        {
            //after postback
            FamilyID = hdnFamilyID.Value;
        }
        //strResultMsg += "Family ID: " + FamilyID + "<br />";
        if (FamilyID != "0" && FamilyID != "" && FamilyID != null)
        {
            intFamilyID = Convert.ToInt32(FamilyID);
        }
        
        //strResultMsg += "Button Face: " + strButtonFace + "<br />";
        if (!IsPostBack)
        {
            //Display data
            DataTable dtFamilyInfo = hsInfo.GetHomestayFamilyInfo(intFamilyID, "", "ID");
            if (dtFamilyInfo != null)
            {
                if (dtFamilyInfo.Rows.Count > 0)
                {
                    traveledWhere.Text = dtFamilyInfo.Rows[0]["travelOutsideUS"].ToString();
                    smokingHabits.Text = dtFamilyInfo.Rows[0]["smokerInHome"].ToString();
                    activitiesEnjoyed.Text = dtFamilyInfo.Rows[0]["otherHobbies"].ToString();
                    homeEnvironmentPreferences.Text = dtFamilyInfo.Rows[0]["homeEnvironment"].ToString();
                    anythingElse.Text = dtFamilyInfo.Rows[0]["additionalPreferences"].ToString();
                    //rdoHomestayOption.SelectedValue = dtFamilyInfo.Rows[0]["homestayOption"].ToString();
                    string[] optionchoices = dtFamilyInfo.Rows[0]["homestayOption"].ToString().Split('|');
                    foreach (var substring in optionchoices)
                    {
                        switch (substring)
                        {
                            case "Full18":
                                Full18.Checked = true;
                                break;
                            case "Shared18":
                                Shared18.Checked = true;
                                break;
                            case "FullUA":
                                FullUA.Checked = true;
                                break;
                            case "NoPreference":
                                NoPreference.Checked = true;
                                break;
                        }
                    }
                    string[] genderChoices = dtFamilyInfo.Rows[0]["genderPreference"].ToString().Split('|');
                    foreach (var substring in genderChoices)
                    {
                        switch (substring)
                        {
                            case "Male":
                                Male.Checked = true;
                                break;
                            case "Female":
                                Female.Checked = true;
                                break;
                            case "Other":
                                Other.Checked = true;
                                break;
                        }
                    }
                    blWaiverSubmitted = Convert.ToBoolean(dtFamilyInfo.Rows[0]["waiverSubmitted"]);
                    chkAcknowledgeUnderstand.Checked = Convert.ToBoolean(dtFamilyInfo.Rows[0]["understandAgree"]);
                    strHomeName = dtFamilyInfo.Rows[0]["homeName"].ToString();
                    hdnHomeName.Value = strHomeName;
                    hdnWaiverSubmitted.Value = blWaiverSubmitted.ToString();
                    if (blWaiverSubmitted)
                    {
                        Configuration config = WebConfigurationManager.OpenWebConfiguration(Request.ApplicationPath);
                        AppSettingsSection appSettings = (AppSettingsSection)config.GetSection("appSettings");
                        strUploadServer = "/InternetContent" + appSettings.Settings["HomestayShare"].Value;
                        litWaiverFile.Text = "";
                        DataTable dt = hsInfo.GetUploadedFiles(intFamilyID);
                        if (dt != null)
                        {
                            if (dt.Rows.Count > 0)
                            {
                                foreach (DataRow dr in dt.Rows)
                                {
                                    strFileName = dr["fileName"].ToString().Trim();
                                    //strFileName = strFileName.Replace(",", "-");
                                    //strFileName = strFileName.Replace("'", "''");
                                    if (strFileName.Contains("Waiver"))
                                    {
                                        //get the file name
                                        litWaiverFile.Text += "<br /><b>Waiver submitted:</b> " + "<a href=\"" + strUploadServer + strFileName + "\" target=\"_blank\">" + strFileName + "</a>"; ;
                                    }
                                }
                            }
                        }
                    }// end waiver submitted
                    // set emailSent boolean
                    blEmailSent = Convert.ToBoolean(dtFamilyInfo.Rows[0]["submittedEmailSent"].ToString());
                    hdnMailSent.Value = blEmailSent.ToString();
                }
            }// have records returned - Family Info
            if (intFamilyID != 0)
            {
                DataTable dtFamPref = hsInfo.GetHomestayPreferenceSelections(0, intFamilyID);
                if (dtFamPref != null)
                {
                    if (dtFamPref.Rows.Count > 0)
                    {
                        foreach (DataRow dtFamPrefRow in dtFamPref.Rows)
                        {
                            String objID = dtFamPrefRow["fieldID"].ToString().Trim();
                            String objValue = dtFamPrefRow["fieldValue"].ToString().Trim();
                            //strResultMsg += "Object ID; " + objID + " Value: " + objValue + "<br />"; 
                            if (objID.Substring(0, 3).Contains("rdo"))
                            {
                                //Radio Button
                                //declare specific type of control
                                try
                                {
                                    //RadioButtonList myRDO = (RadioButtonList)Page.Master.FindControl("MainContent").FindControl("fldProfile").FindControl(objID);
                                    RadioButtonList myRDO = (RadioButtonList)FindControl("fldProfile").FindControl(objID);
                                    if (myRDO != null)
                                    {
                                        //set value
                                        myRDO.SelectedValue = objValue;
                                    }
                                }
                                catch
                                {
                                    strResultMsg += "Radio button list " + objID + " with a value of " + objValue + " is null.<br />"; 
                                }
                            }
                            else
                            {
                                //Checkbox
                                //declare specific type of control
                                try
                                {
                                    //CheckBox myControl = (CheckBox)Page.Master.FindControl("MainContent").FindControl("fldProfile").FindControl(objID);
                                    CheckBox myControl = (CheckBox)FindControl("fldProfile").FindControl(objID);
                                    if (myControl != null)
                                    {
                                        //set value
                                        myControl.Checked = true;
                                    }
                                }
                                catch
                                {
                                    //strResultMsg += "Checkbox " + objID + " with a value of " + objValue + " is null.<br />"; 
                                }
                            }
                        }
                    }
                    else { 
                        //strResultMsg += "Preference Selections returned no rows.<br />"; 
                    }
                }
                else { strResultMsg += "Preference Selections table is null<br />"; }
            }// Family ID > 0
            litResultMsg.Text = "<p>" + strResultMsg + hsInfo.strResultMsg + "</p>";
        }// end !IsPostback

    }
    protected void btnExportAll_Click(object sender, EventArgs e)
    {
        HomestayUtility hsUtility = new HomestayUtility();
        hsUtility.ExportPdf();
    }
        #region Export PDF  
    protected void btnExport_Click(object sender, EventArgs e)
    {
        try
        {
            Document pdfDoc = new Document(PageSize.A4, 25, 10, 25, 10);
            PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            /*
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            form1.RenderControl(hw);
            StringReader sr = new StringReader(sw.ToString());
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            */
            pdfDoc.Open();
            pdfWriter.CloseStream = false;

            //Prepare data
            BaseFont bf = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
            Font font1 = new Font(bf, 16, Font.NORMAL);
            Font font2 = new Font(bf, 14, Font.NORMAL);
            Paragraph Text = new Paragraph("Family Profile",font1);
            pdfDoc.Add(Text);
            Text = new Paragraph("Homestay Preferences", font2);
            pdfDoc.Add(Text);

            int familiyID = Convert.ToInt32(Request.QueryString["ID"].ToString());
            DataTable dtFamily = hsInfo.GetHomestayFamilyInfo(familiyID, "", "ID");
            DataTable dtFamPref = hsInfo.GetHomestayPreferenceSelections(0, familiyID);
            Text = new Paragraph("Homestay Program :" + dtFamily.Rows[0]["homestayOption"].ToString() + "\n");
            pdfDoc.Add(Text);

            Text = new Paragraph("Preference for Gender of Student(s) :" + dtFamily.Rows[0]["genderPreference"].ToString() + "\n");
            pdfDoc.Add(Text);

            Text = new Paragraph("Personal Preferences and Activities", font2);
            pdfDoc.Add(Text);
            Text = new Paragraph("Home Environment", font2);
            pdfDoc.Add(Text);
            
            Dictionary<string, string> hash = new Dictionary<string, string>();
            List list = new List(List.UNORDERED, 10f);
            list.SetListSymbol("\u2022");
            list.IndentationLeft = 30f;
            foreach (DataRow dtFamPrefRow in dtFamPref.Rows)
            {
                //Text = new Paragraph(dtFamPrefRow["fieldID"].ToString().Trim() +" " + dtFamPrefRow["fieldValue"].ToString().Trim());
                //pdfDoc.Add(Text);
                hash.Add(dtFamPrefRow["fieldID"].ToString().Trim(), dtFamPrefRow["fieldValue"].ToString().Trim());
            }
            string item;
            string travelOutside;
            if (hash.TryGetValue("rdoTraveledOutsideCountry",out travelOutside))
            {
                item = "Have you traveled outside the U.S. before? " + (travelOutside.Contains("Not") ? "No" : "Yes");
                list.Add(item);
                item = "Which countries? " + dtFamily.Rows[0]["travelOutsideUS"].ToString();
                list.Add(item);
            }

            
            string smokeSelection;
            if (hash.TryGetValue("rdoSmoker", out smokeSelection))
            {
                if (hash["rdoSmoker"].Contains("Non"))
                {
                    smokeSelection = "No";
                }
                else if (hash["rdoSmoker"].Contains("Occasional"))
                {
                    smokeSelection = "Sometimes";
                }
                else
                {
                    smokeSelection = "Yes";
                }
                item = "Does anyone living in your home smoke? " + smokeSelection;
                list.Add(item);
            }


            item = "Explain: " + dtFamily.Rows[0]["smokerInHome"].ToString();
            list.Add(item);
            //item = "Is it okay if your homestay student smokes? " + (hash["rdoOtherSmokerOK"].Contains("Not") ? "No" : "Yes");
            //list.Add(item);

            //item = "How much interaction and conversation with your student do you hope to have each day/week? " + hash["rdoInteraction"];
            //pdfDoc.Add(Text);
            //list.Add(item);
            //item = "I would describe my home as more: " + hash["rdoHomeEnvironment"];
            //pdfDoc.Add(Text);
            //list.Add(item);
            item = "Explain: " + dtFamily.Rows[0]["homeEnvironment"].ToString();
            //pdfDoc.Add(Text);
            //list.Add(item);
            pdfDoc.Add(list);

            #region Hobbies
            Text = new Paragraph("Hobbies", font2);
            pdfDoc.Add(Text);
            list = new List(List.UNORDERED, 10f);
            list.SetListSymbol("\u2022");
            list.IndentationLeft = 30f;

            string[] hobbies = { "MusicalInstrument", "Art" ,"TeamSports", "IndividualSports" ,"ListenMusic","Drama","WatchMovies",
                                "Singing","Shopping","ReadBooks","Outdoors","Cooking", "Photography", "Gaming"};
            foreach (DataRow dtFamPrefRow in dtFamPref.Rows)
            {
                //Text = new Paragraph(dtFamPrefRow["fieldID"].ToString().Trim() +" " + dtFamPrefRow["fieldValue"].ToString().Trim());
                //pdfDoc.Add(Text);
                string fieldIDStr = dtFamPrefRow["fieldID"].ToString().Trim();
                string fieldValueStr = dtFamPrefRow["fieldValue"].ToString().Trim();
                if (Array.Exists(hobbies, element => element == fieldValueStr))
                {
                    list.Add(dtFamPrefRow["Preference"].ToString().Trim());
                }  
            }

            item = "Other hobbies/activities you enjoy: " + dtFamily.Rows[0]["otherHobbies"].ToString();
            list.Add(item);
            pdfDoc.Add(list);
            #endregion

            #region Diet
            Text = new Paragraph("Diet", font2);
            pdfDoc.Add(Text);
            string[] diets = { "Vegetarian", "GlutenFree", "DairyFree", "OtherFoodAllergy", "Halal", "Kosher", "NoSpecialDiet" };
            list = new List(List.UNORDERED, 10f);
            list.SetListSymbol("\u2022");
            list.IndentationLeft = 30f;
            foreach (DataRow dtFamPrefRow in dtFamPref.Rows)
            {
                //Text = new Paragraph(dtFamPrefRow["fieldID"].ToString().Trim() +" " + dtFamPrefRow["fieldValue"].ToString().Trim());
                //pdfDoc.Add(Text);
                string fieldIDStr = dtFamPrefRow["fieldID"].ToString().Trim();
                string fieldValueStr = dtFamPrefRow["fieldValue"].ToString().Trim();
                if (Array.Exists(diets, element => element == fieldValueStr))
                {
                    list.Add(dtFamPrefRow["Preference"].ToString().Trim());
                }
            }
            pdfDoc.Add(list);

            Text = new Paragraph("\nIs there anything else you'd like us to know about your family? " + dtFamily.Rows[0]["additionalPreferences"].ToString() + "\n");
            pdfDoc.Add(Text);
            #endregion
            //pdfDoc.Add(new Phrase(this.rdoGenderPreference.Text.Trim()));

            //PdfFormField radiogroup = PdfFormField.CreateRadioButton(pdfWriter, true);
            //radiogroup.FieldName = "gsex";
            Text = new Paragraph("\nI acknowledge that I have read this waiver and release of liability. I understand what is required of me as a Homestay Host Family :" 
                                    + ( dtFamily.Rows[0]["understandAgree"].ToString() == "True" ? "Checked" : "Have not checked") + "\n");
            pdfDoc.Add(Text);

            #region Document Upload
            DataTable dt = hsInfo.GetUploadedFiles(intFamilyID);
            Text = new Paragraph("Document Upload:\n", font2);
            pdfDoc.Add(Text);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        strFileName = dr["fileName"].ToString();
                        if (strFileName.Contains("Waiver"))
                        {
                            //get the file name
                            //litWaiverFile.Text += "<br /><b>Waiver submitted:</b> " + "<a href=\"" + strUploadServer + strFileName + "\" target=\"_blank\">" + strFileName + "</a>";
                            Text = new Paragraph(strFileName + "\n");
                            pdfDoc.Add(Text);
                        }
                    }
                }
            }
            #endregion
            /*
            PdfPTable table = new PdfPTable(3);
            PdfPCell cell = new PdfPCell(new Phrase("Header spanning 3 columns"));
            cell.Colspan = 3;
            cell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            table.AddCell(cell);
            table.AddCell("Col 1 Row 1");
            table.AddCell("Col 2 Row 1");
            table.AddCell("Col 3 Row 1");
            table.AddCell("Col 1 Row 2");
            table.AddCell("Col 2 Row 2");
            table.AddCell("Col 3 Row 2");
            pdfDoc.Add(table);
            */
            string homeNameStr = dtFamily.Rows[0]["homeName"].ToString().Trim().Replace(",", "-"); //file name cannot contains "," therefore must replace with "-"
            string fileName = homeNameStr + " Family Profile";
            pdfDoc.Close();
            Response.Buffer = true;
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=" + fileName + ".pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Write(pdfDoc);
            Response.End();
        }
        catch (Exception ex)
        {
            Response.Write("error" + ex.Message);
        }
    }
    #endregion
    protected void btnCmdSaveAndContinue_Click(object sender, EventArgs e)
    {
        // validate form fields
        /* if (rdoHomestayOption.SelectedIndex == -1)
        {
            eProgramPref.InnerHtml = "Please select a Program preference.";
            rdoHomestayOption.Focus();
            return;
        }
        else
            eProgramPref.InnerHtml = "" */
        //Validate homestay program check
        if (!Full18.Checked && !FullUA.Checked && !Shared18.Checked && !NoPreference.Checked)
        {
            eProgramPref.InnerHtml = "Please select a Program preference.";
            Full18.Focus();
            return;
        }

        //Validate gender preference check
        if (!Male.Checked && !Female.Checked && !Other.Checked )
        {
            eGenderPref.InnerHtml = "Please select a your gender preference.";
            Male.Focus();
            return;
        }

        else
            eProgramPref.InnerHtml = "";
       
        if (rdoTraveledOutsideCountry.SelectedIndex == -1)
        {
            eTravel.InnerHtml = "Pliease indicate if you have traveled outside the U.S. before.";
            rdoTraveledOutsideCountry.Focus();
            return;
        }
        else
            eTravel.InnerHtml = "";
        if (rdoSmoker.SelectedIndex == -1)
        {
            eSmokers.InnerHtml = "Please indicate if anyone in your home smokes.";
            rdoSmoker.Focus();
            return;
        }
        else
            eSmokers.InnerHtml = "";
        if (rdoOtherSmokerOK.SelectedIndex == -1)
        {
            eStudentSmokeOK.InnerHtml = "Please indicate your preference if the homestay student smokes or not.";
            rdoOtherSmokerOK.Focus();
            return;
        }
        else
            eStudentSmokeOK.InnerHtml = "";
        if (rdoInteraction.SelectedIndex == -1)
        {
            eInteraction.InnerHtml = "Please indicate the desired level of interaction with your student.";
            rdoInteraction.Focus();
            return;
        }
        else
            eInteraction.InnerHtml = "";
        if (rdoHomeEnvironment.SelectedIndex == -1)
        {
            eEnvironment.InnerHtml = "Please indicate how you would describe your home.";
            rdoHomeEnvironment.Focus();
            return;
        }
        else
            eEnvironment.InnerHtml = "";
        
        // process submission
        String TraveledWhere = traveledWhere.Text.Replace("'", "''");
        String SmokingHabits = smokingHabits.Text.Replace("'", "''");
        String HomeEnvironmentPreferences = homeEnvironmentPreferences.Text.Replace("'", "''");
        String ActivitiesEnjoyed = activitiesEnjoyed.Text.Replace("'", "''");
        String AnythingElse = anythingElse.Text.Replace("'", "''");
        //String HomestayOption = rdoHomestayOption.SelectedValue;
        String HomestayOption = "";
        if (Full18.Checked)
        {
            HomestayOption = "Full18";
        }
            
        if (Shared18.Checked ) { 
            HomestayOption += "|Shared18";
        }
        
        if (FullUA.Checked )
        {
            HomestayOption += "|FullUA";
        }
        
        if (NoPreference.Checked )
        {
            HomestayOption += "|NoPreference";
        }
        //Remove leading | of GenderPreference
        if (HomestayOption.StartsWith("|"))
        {
            HomestayOption = HomestayOption.Remove(0, 1);
        }


        //Get GenderPreference
        String GenderPreference = "";
        if (Male.Checked)
        {
            GenderPreference = "Male";
        }

        if (Female.Checked)
        {
            GenderPreference += "|Female";
        }
       
        if (Other.Checked)
        {
            GenderPreference += "|Other";
        }
        //Remove leading | of GenderPreference
        if (GenderPreference.StartsWith("|"))
        {
            GenderPreference = GenderPreference.Remove(0, 1);
        }
        //       Response.Write("gender: " + GenderPreference + "; option: " + HomestayOption + "; travel: " + TraveledWhere + "<br />");
        //Update HomestayFamilyInfo table
        // intResult
        string strMsg = hsInfo.UpdateP2HomestayFamilyInfo(intFamilyID, TraveledWhere, SmokingHabits, ActivitiesEnjoyed, HomeEnvironmentPreferences, AnythingElse, HomestayOption, GenderPreference);
//        Response.Write("result: " + strMsg + "<br />");
        //Delete all previous selections; then re-add new ones
        intResult = hsInfo.DeleteHomestayPreferenceSelections(0, intFamilyID);

        //Insert into HomestayPreferenceSelections table
        String SmokersOK = rdoOtherSmokerOK.SelectedValue;
        if (SmokersOK != "")
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections(SmokersOK, 0, intFamilyID);
        }
        String StudentSmokes = rdoSmoker.SelectedValue;
        if (StudentSmokes != "")
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections(StudentSmokes, 0, intFamilyID);
        }
        String selectedTraveledOutsideCountry = rdoTraveledOutsideCountry.SelectedValue;
        if (selectedTraveledOutsideCountry != "")
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections(selectedTraveledOutsideCountry, 0, intFamilyID);
        }
        if (MusicalInstrument.Checked)
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("MusicalInstrument", 0, intFamilyID);
        }
        if (Art.Checked)
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("Art", 0, intFamilyID);
        }
        if (TeamSports.Checked)
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("TeamSports", 0, intFamilyID);
        }
        if (IndividualSports.Checked)
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("IndividualSports", 0, intFamilyID);
        }
        if (ListenMusic.Checked)
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("ListenMusic", 0, intFamilyID);
        }
        if (Drama.Checked)
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("Drama", 0, intFamilyID);
        }
        if (WatchMovies.Checked)
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("WatchMovies", 0, intFamilyID);
        }
        if (Singing.Checked)
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("Singing", 0, intFamilyID);
        }
        if (Shopping.Checked)
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("Shopping", 0, intFamilyID);
        }
        if (ReadBooks.Checked)
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("ReadBooks", 0, intFamilyID);
        }
        if (Outdoors.Checked)
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("Outdoors", 0, intFamilyID);
        }
        if (Cooking.Checked)
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("Cooking", 0, intFamilyID);
        }
        if (Photography.Checked)
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("Photography", 0, intFamilyID);
        }
        if (Gaming.Checked)
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("Gaming", 0, intFamilyID);
        }
        String selectedInteraction = rdoInteraction.SelectedValue;
        if (selectedInteraction != "")
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections(selectedInteraction, 0, intFamilyID);
        }
        String selectedHomeEnvironment = rdoHomeEnvironment.SelectedValue;
        if (selectedHomeEnvironment != "")
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections(selectedHomeEnvironment, 0, intFamilyID);
        }
        if (Vegetarian.Checked)
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("Vegetarian", 0, intFamilyID);
        }
        if (GlutenFree.Checked)
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("GlutenFree", 0, intFamilyID);
        }
        if (DairyFree.Checked)
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("DairyFree", 0, intFamilyID);
        }
        if (OtherFoodAllergy.Checked)
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("OtherFoodAllergy", 0, intFamilyID);
        }
        if (Halal.Checked)
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("Halal", 0, intFamilyID);
        }
        if (Kosher.Checked)
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("Kosher", 0, intFamilyID);
        }
        if (NoSpecialDiet.Checked)
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("NoSpecialDiet", 0, intFamilyID);
        }
        
        //process submission
        divConfirmation.Visible = false;
        btnDone.Visible = true;
        btnSave.Visible = false;
        btnPrevious.Visible = false;
        fldProfile.Visible = false;
        divWaiver.Visible = true;
        divPrintWaiver.Visible = true;
        litPageTitle.Text = "Sign Form";
        if (blIsAdmin)
            divHome.Visible = true;
        else
            divHome.Visible = false;
         
    }// end btn Save and Continue

    protected void btnCmdSave_Click(object sender, EventArgs e)
    {
        if (!chkAcknowledgeUnderstand.Checked)
        {
            eAcknowledge.InnerHtml = "Please acknowledge you understand what is required of a Homestay Host Family.";
            divWaiver.Visible = true;
            fldProfile.Visible = false;
            divConfirmation.Visible = false;
            btnSave.Visible = false;
            btnDone.Visible = true;
            divNextPage.Visible = false;
            btnPrevious.Visible = true;
            divPrintWaiver.Visible = true;
            if (blIsAdmin)
                divHome.Visible = true;
            else
                divHome.Visible = false;
            return;
        }
        else
            eAcknowledge.InnerHtml = "";
        //Upload waiver
        if (uplWaiverFile.HasFile)
        {
            
            string uploadWaiverFileName = uplWaiverFile.FileName;
            uploadWaiverFileName = uploadWaiverFileName.Replace("'", "");
            uploadWaiverFileName = uploadWaiverFileName.Replace(",", "-");
            strHomeName = hdnHomeName.Value;
            strFileName = strHomeName + "-" + intFamilyID + "-Waiver-" + uploadWaiverFileName;
            uplWaiverFile.SaveAs(strUploadURL + strFileName);
            strResultMsg += "Your file, <span style=\"color:red;\">" + strFileName + "</span>, was successfully uploaded!<br />";
            //Insert record in UploadedFilesInformation table
            intResult = hsInfo.InsertUploadedFilesInformation(intFamilyID, strHomeName, strFileName, strUploadURL);
            blWaiverSubmitted = true;
            intResult = hsInfo.UpdateWaiverSubmittedBitflagHomestayFamilyInfo(intFamilyID, 1);
        }
        else if (hdnWaiverSubmitted.Value.ToLower() == "true")
        {
            //previous submission exists
            blWaiverSubmitted = true;
        }

        //Process acknowledgement - already validated that it is checked
        intResult = hsInfo.UpdateUnderstandAgreeHomestayFamilyInfo(intFamilyID, 1);
        //app now complete; update status
        intResult = hsInfo.UpdateFamilyStatus(intFamilyID, strFamilyStatus);
        // generate email to family
        Utility uEmail = new Utility();
        String strRelationship = "";
        string familyNamePrimary = "";
        string firstNamePrimary = "";
        string middleNamePrimary = "";
        string genderPrimary = "";
        string emailPrimary = "";
        string IDprimary = "";
        string familyNameSecond = "";
        string firstNameSecond = "";
        string middleNameSecond = "";
        string genderSecond = "";
        string emailSecond = "";
        string IDsecond = "";
        string strBody = "";

        //get the primary contact basic information (name, email address) for the confirmation 
        DataTable dtRelatives = hsInfo.GetHomestayRelatives(0, intFamilyID);
        if (dtRelatives != null)
        {
            if (dtRelatives.Rows.Count > 0)
            {
                foreach (DataRow dtRow in dtRelatives.Rows)
                {
                    strRelationship = dtRow["relationship"].ToString().Trim();
                    //strResultMsg += "Relationship: " + strRelationship + "<br />";
                    switch (strRelationship)
                    {
                        case "Primary Contact":
                            familyNamePrimary = dtRow["familyName"].ToString().Trim();
                            firstNamePrimary = dtRow["firstName"].ToString().Trim();
                            middleNamePrimary = dtRow["middleName"].ToString().Trim();
                            genderPrimary = dtRow["gender"].ToString().Trim();
                         //   DOB.Text = Convert.ToDateTime(dtRow["DOB"]).ToShortDateString();
                         //   driversLicenseNumber.Text = dtRow["driversLicenseNumber"].ToString().Trim();
                         //   occupation.Text = dtRow["occupation"].ToString().Trim();
                         //   cellPhone.Text = dtRow["cellPhone"].ToString().Trim();
                        //    workPhone.Text = dtRow["workPhone"].ToString().Trim();
                            emailPrimary = dtRow["email"].ToString().Trim();
                            IDprimary = dtRow["id"].ToString();
                            break;
                        case "Secondary Contact":
                            familyNameSecond = dtRow["familyName"].ToString().Trim();
                            firstNameSecond = dtRow["firstName"].ToString().Trim();
                            middleNameSecond = dtRow["middleName"].ToString().Trim();
                            genderSecond = dtRow["gender"].ToString().Trim();
                     //       DOB2.Text = Convert.ToDateTime(dtRow["DOB"]).ToShortDateString();
                      //      driversLicenseNumber2.Text = dtRow["driversLicenseNumber"].ToString().Trim();
                      //      occupation2.Text = dtRow["occupation"].ToString().Trim();
                      //      cellPhone2.Text = dtRow["cellPhone"].ToString().Trim();
                      //      workPhone2.Text = dtRow["workPhone"].ToString().Trim();
                            emailSecond = dtRow["email"].ToString().Trim();
                            IDsecond = dtRow["id"].ToString();
                            break;
                        default:
                            break;
                    }// end switch
                }// end foreach
            }// end if rowcount > 0
            else { strResultMsg += "No HomestayRelatives records returned.<br />"; }
        }
        else { strResultMsg += "No HomestayRelatives records returned.<br />"; }

        Boolean blHdnEmailSent = Convert.ToBoolean(hdnMailSent.Value);
        // see if they have uploaded any files, if not let them know in both email and confirmation page what is missing.

        Boolean blWaiver = false;
        Boolean blLicense = false;
        DataTable dt = hsInfo.GetUploadedFiles(intFamilyID);
        if (dt != null)
        {
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    strFileName = dr["fileName"].ToString();
                    if (strFileName.Contains("-Waiver-"))
                        blWaiver = true;
                    else if (strFileName.Contains("-License-"))
                        blLicense = true;
                }// end foreach
            }// end row count > 0
        }// end dt not null
        string strStart = "<p><span style='color: #DD0000;'>All forms must be signed, scanned and uploaded into your application or emailed to <a href='mailto:Internationalhomestay@ccs.spokane.edu'>Internationalhomestay@ccs.spokane.edu</a>.<br />";
        if (!blLicense && litMissingUploads.Text.Trim().Length > 1)
            litMissingUploads.Text += "Our records indicate no driver licenses have been uploaded.  Please upload or email the scanned drivers licenses for all adults in your household over 18 years of age.";
        else if(!blLicense && litMissingUploads.Text.Trim().Length == 0)
            litMissingUploads.Text = strStart + "Our records indicate no driver licenses have been uploaded.  Please upload or email the scanned documents for all adults in your household over 18 years of age.";
        if (!blWaiver && litMissingUploads.Text.Trim().Length > 1)
            litMissingUploads.Text += "<br />Our records show that you need to upload a scanned copy of the 'Waiver and Release of Liability' form.";
        else if(!blWaiver && litMissingUploads.Text.Trim().Length == 0)
            litMissingUploads.Text = strStart + "Our records show that you need to upload a scanned copy of the 'Waiver and Release of Liability' form.";
        if(litMissingUploads.Text.Trim().Length > 1)
            litMissingUploads.Text += "</p>";

        if (emailPrimary != "" && !blHdnEmailSent)
        {
            sendTo = emailPrimary;
            
            //prepare the body of the email
            strBody = "";
            strBody = "<p>Dear " + firstNamePrimary + " " + familyNamePrimary + ",</p>";
            strBody += "<p>Thank you for your interest in our Homestay Program.  In order to complete your application, the Homestay Office will need scans ";
            strBody += "or photos of the driver’s licenses for each person in your home over the age of 18.  ";
            if (!blLicense)
            {
                strBody += "<b>Our records indicate no licenses have been uploaded.  Please either log into your ";
                strBody += "family account in the <a href='http://apps.spokane.edu/Homestay/'>Homestay Family application</a>, navigate to the correct page and use the 'Document Upload' button to find and upload your scanned file, ";
                strBody += "or email the file to: <a href='mailto:internationalhomestay@ccs.spokane.edu'>internationalhomestay@ccs.spokane.edu</a> as ";
                strBody += "an attachment</b>.</p>";
            } else
            {
                strBody += "<b>Our records indicate you have uploaded at least one driver's license.  If there are others in your home over the age of 18, be sure to ";
                strBody += "include scans of their driver's license as well</b>.</p>";
            }
            
            if (!blWaiver)
            {
                strBody += "<p><b>Our records indicate that you need to upload a scanned copy of the 'Waiver and Release of Liability' form.  Please either log into your ";
                strBody += "family account in the <a href='http://apps.spokane.edu/Homestay/'>Homestay Family application</a>, navigate to the correct page and use the 'Document Upload' button to find and upload your scanned file, ";
                strBody += "or email the file to: <a href='mailto:internationalhomestay@ccs.spokane.edu'>internationalhomestay@ccs.spokane.edu</a> as ";
                strBody += "an attachment</b>.</p>";
            }

            strBody += "<p>When the review of your application is complete, ";
            strBody += "you will be contacted for a home visit to view the rooms available and to answer any questions regarding our program. </p>";
            strBody += "<p>Please keep track of your username, password, and login information, as you will be returning to this website to maintain your Homestay Family Profile. </p>";
            strBody += "<p>Please feel free to learn more about how the program works by viewing our website:  ";
            strBody += "<a href='https://sfcc.spokane.edu/Become-a-Student/I-am-an-International-Student/After-I-Have-Been-Admitted/Housing-Options/Become-a-Homestay-Family' target='_blank'>Become a Homestay Family</a></p>";
            strBody += "<p>If you have questions, feel free to call the CCS Homestay Office at:  509-533-4113.</p>";
            //  strBody += "";
            //  strBody += "";
            strEmailResult = uEmail.SendEmailMsg(sendTo, sentFrom, strSubject, true, strBody, strCCaddresses);

            if (strEmailResult == "Success")
            {
                string strResult = "";
                // update the emailSent field in the HomestayFamilyInfo table
                //  intResult = hsInfo.UpdateMailSentFamily(intFamilyID, 1);
                strResult = hsInfo.updateMailSentFamily(intFamilyID, 1);
                // Response.Write("MailUpdate Result: " + strResult + "<br />");
                //Display Confirmation
                fldProfile.Visible = false;
                divConfirmation.Visible = true;
                divWaiver.Visible = false;
                divButtons.Visible = false;
                litPageTitle.Text = "Confirmation";
                if (blIsAdmin)
                {
                    divButtons.Visible = true;
                    btnSave.Visible = false;
                    btnDone.Visible = false;
                    divNextPage.Visible = false;
                    btnPrevious.Visible = false;
                    divPrintWaiver.Visible = false;
                    divHome.Visible = true;
                }
                else
                    divHome.Visible = false;
            }
            else
            {
                // error sending email so let user know
                litResultMsg.Text = strEmailResult;
                //return;
            }
        }// end need to send email and have address

        
        //Display Confirmation
        fldProfile.Visible = false;
        divConfirmation.Visible = true;
        divWaiver.Visible = false;
        divButtons.Visible = false;
        litPageTitle.Text = "Confirmation";
        if (blIsAdmin)
        {
            divButtons.Visible = true;
            btnSave.Visible = false;
            btnDone.Visible = false;
            divNextPage.Visible = false;
            btnPrevious.Visible = false;
            divPrintWaiver.Visible = false;
            divHome.Visible = true;
        }
        else
            divHome.Visible = false;
         
    }

    protected void btnCmdBack_Click(object sender, EventArgs e)
    {
        if(blIsAdmin)
            Response.Redirect("ApplicationForm.aspx?ID=" + intFamilyID);
        else
            Response.Redirect("/Homestay/Families/ApplicationForm.aspx?ID=" + intFamilyID + "&UID="+strUserName+"&admin="+blIsAdmin);

    }

    protected void btnCmdPrintWaiver_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Homestay/Families/PlacementProfile.aspx?ID=" + intFamilyID + "&print=true");
    }
    protected void btnCmdReturnHome_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Homestay/Admin/Default.aspx");
    }
    protected void btnCmdContinue_Click(object sender, EventArgs e)
    {
        divConfirmation.Visible = false;
        btnDone.Visible = true;
        btnSave.Visible = false;
        divNextPage.Visible = false;
        btnPrevious.Visible = true;
        fldProfile.Visible = false;
        divWaiver.Visible = true;
        divPrintWaiver.Visible = true;
        litPageTitle.Text = "Sign Form";
        if (blIsAdmin)
            divHome.Visible = true;
        else
            divHome.Visible = false;
    }
}