﻿<%@ Page Title="Homestay Family" EnableEventValidation="false" Language="C#" AutoEventWireup="true" MasterPageFile="~/CCSApps.master" CodeFile="PlacementForm_Student.aspx.cs" Inherits="Homestay_Admin_Reports_PlacementForm_Student" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
<style type="text/css">
    p
    {
        padding:2px;
        margin:0px;
    }
    .editClass{
        width:80%;
    }
    unbold{
        font-weight:normal;
    }
  
</style>
    <link href="/Homestay/_css/Print.css" rel="stylesheet" type="text/css" media="print" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="bannerH1Text" Runat="Server">
    Homestay Administration
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">
<div style="width:100%;text-align:center;margin-bottom:50px;">
    <div style="width:20%;float:left">
        <img src="/Homestay/images/ccs-logo.jpg" style="height: 104px; width: 211px" />
    </div>
    <div style="width:80%">
        <p style="font-size:30px;font-weight:bold">Information about your International Homestay Family</p>       
    </div>
    
   <br /> 

</div>
    
<div id="divHome" class="noPrint" runat="server" style="float:right;margin-right:30px;">
    <asp:Button  ID="btnHome" Text="Return to Admin Home Page" runat="server" class="ButtonAdmin" OnClick="linkBtnHome_Click"/>&nbsp;&nbsp;
</div>
    <asp:Literal ID="litResutlMsg" runat="server"></asp:Literal>
    
    <div id="divStudentdd" class="noPrint" runat="server" visible="true">
        <h2>Select a Family</h2>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:DropDownList ID="ddlFamily" runat="server"  AutoPostBack="true" style="margin-top:5px;"></asp:DropDownList>
                <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" CssClass="noPrint" />
             </ContentTemplate>
            
        </asp:UpdatePanel>
       
        
    </div>

    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <p aria-autocomplete="both">
                <asp:DetailsView ID="dvPlacement" runat="server" 
        AutoGenerateRows="False" CellPadding="10" Width="100%" Font-Size="Large" >
         <FieldHeaderStyle BackColor="#E4E4E4"  Font-Bold="True" Height="50px" Width="30%" />
         <RowStyle BorderStyle="Solid" BorderWidth="2px" />
        <Fields>
            <asp:TemplateField HeaderText="Student">
                <ItemTemplate>
                    <asp:Label ID="lblCampus" Text='<%# Bind("Student") %>' runat="server" ></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Date Of Birth">
                <ItemTemplate>
                    <asp:Label ID="lblQuarterOfArrival" Text='<%# Bind("DOB") %>' runat="server" ></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Country of Citizenship">
                <ItemTemplate>
                    <asp:Label ID="lblOrientationStartDate" Text='<%# Bind("Country") %>' runat="server"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Effective Date">
                <ItemTemplate>
                    <asp:Label ID="lblEffectiveDate" Text='<%# Bind("EffectiveDate") %>' runat="server"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="End Date">
                <ItemTemplate>
                    <asp:Label ID="lblEndDate" Text='<%# Bind("EndDate") %>' runat="server"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Homestay Family and Children">
                <ItemTemplate>
                    <asp:Label ID="lblQuarterStartDate" Text='<%# Bind("HomestayFamily") %>' runat="server" ></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Address">
                <ItemTemplate>
                    <asp:Label ID="lblStudentName" Text='<%# Bind("Address") %>' runat="server" ></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Phone numbers">
                <ItemTemplate>
                    <asp:Label ID="lblStudentGender" Text='<%# Bind("PhoneNumber") %>' runat="server" ></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Emails <br/> <p style='font-weight:normal;'><i>Please email your homestay family to say hello before you arrive!</i></p>">
                <ItemTemplate>
                    <asp:Label ID="lblDOB" Text='<%# Bind("Email") %>' runat="server" ></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Transportation">
                <ItemTemplate>
                    <asp:Label ID="lblCountry" Text='<%# Bind("Transportation") %>' runat="server" ></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Hobbies and Interests">
                <ItemTemplate>
                    <asp:Label ID="lblHobbies" Text='<%# Bind("Hobbies") %>' runat="server" ></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Smoking policy <p style='font-weight:normal;'><i>(note that students in Washington, under age 18, are not allowed to smoke or possess tobacco products)</i></p>">
                <ItemTemplate>
                    <asp:Label ID="lblProgramOfStudy" Text='<%# Bind("SmokingPolicy") %>' runat="server" ></asp:Label>
                    <asp:CheckBoxList  ID="CheckboxSmoking" runat="server">
                        <asp:ListItem Text="No smoking while a resident in this home" Value="No" />
                        <asp:ListItem Text="Smoking outside or away from home only" Value="NoPreference" />
                        <asp:ListItem Text="Smoking permitted at this home" Value="Yes" />
                    </asp:CheckBoxList>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Airport pickup <p style='font-weight:normal;'><i>If student’s arrival at the Spokane airport (GEG) is after 9 pm, the student is expected to stay in one of our suggested hotels until the next day, at which time, the homestay family will pick the student up. If a student under the age of 18 has no other choice but to arrive at the Spokane airport after 9pm, special arrangements must be made.</i></p>">
                <ItemTemplate>
                    <asp:Label ID="lblEmail" Text='<%# Bind("AirportPickup") %>' runat="server" ></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            
           
            
            
        </Fields>
        
    </asp:DetailsView>
            </p>
            <div class="noPrint">
                 From: <br /><asp:TextBox ID="txtFrom" runat="server" Text="Internationalhomestay@ccs.spokane.edu" Width="500px" Enabled="false"/> <br />
                 To: <br /><asp:TextBox ID="txtTo" runat="server" Width="500px"/> <br />
                 CC: <br /><asp:TextBox ID="txtCC" runat="server" Width="500px"/> <br />
            </div>
           
             <asp:button id="btnSendMailClient" Cssclass="noPrint" runat="server" Onclick="SendMail_Click"  
                  Text="Send Mail"/>
            <br />
            <asp:Literal ID="litResultMsg" runat="server"/>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" DynamicLayout="true" AssociatedUpdatePanelID="UpdatePanel2" DisplayAfter="500" >

       <ProgressTemplate>
          <img src="/Homestay/images/demo_wait.gif" />
           Please Wait...
       </ProgressTemplate>
   
    </asp:UpdateProgress>

    
    <p><input type="button" name="btnPrint" class="noPrint" onclick="javascript:window.print();" value="Print" /></p>
</asp:Content>
