﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;

/// <summary>
/// Summary description for ReportWS
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
 [System.Web.Script.Services.ScriptService]
public class ReportWS : System.Web.Services.WebService
{
    Quarter_MetaData QMD = new Quarter_MetaData();
    private static Homestay hsInfo = new Homestay();
    public ReportWS()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

   

    [WebMethod]
    public void GetDataTable()
    {
        
        DataTable dtStudents  = hsInfo.GetHomestayStudents("ReportActive", 0);
        var reports = new List<StudentReport>();
        if (dtStudents != null && dtStudents.Rows.Count > 0)
        {  
            foreach (DataRow dr in dtStudents.Rows)
            {
                DateTime bDate = Convert.ToDateTime(dr["DOB"].ToString().Trim());
                var applicantID = Convert.ToInt32(dr["applicantID"].ToString().Trim());
                DataTable dtInfo = hsInfo.GetOneInternationalStudent("", applicantID, "", "", "", "");
                var strCollege = "";
                var localPhone = "";
                var studyWhat = "";
                var localAddress = "N/A";
                if (dtInfo != null && dtInfo.Rows.Count > 0){
                    strCollege = dtInfo.Rows[0]["studyWhere"].ToString();
                    localPhone = dtInfo.Rows[0]["LocalPhone"].ToString();
                    studyWhat = dtInfo.Rows[0]["studyWhat"].ToString();
                    if (!String.IsNullOrEmpty(dtInfo.Rows[0]["LocalAddress"].ToString()))
                    {
                        localAddress = dtInfo.Rows[0]["LocalAddress"].ToString() + ", " + dtInfo.Rows[0]["LocalCity"].ToString() + ", " + dtInfo.Rows[0]["LocalState"].ToString() + ", " + dtInfo.Rows[0]["LocalZip"].ToString();
                    }
                }

                //Get Notes
                string notes = "";
                DataTable dtNotes = hsInfo.GetHomestayNotesExtended("Student", "" + applicantID, "0", "0", "0", "", "", "");
                if (dtNotes != null && dtNotes.Rows.Count > 0)
                {
                    foreach (DataRow drNotes in dtNotes.Rows)
                    {
                        string detailNote = String.Format("{0})"
                                                    + " <b>Note:</b> {1},"
                                                    + " <b>Date Created:</b> {2},"
                                                    + " <b>Family:</b> {3},"
                                                    + " <b>Action Needed:</b> {4},"
                                                    + " <b>Actin Done:</b> {5} <br/>"
                                                    , dtNotes.Rows.IndexOf(drNotes) + 1
                                                    , drNotes["Note"].ToString()
                                                    , Convert.ToDateTime(drNotes["createDate"].ToString().Trim()).ToShortDateString()
                                                    , drNotes["homeName"].ToString()
                                                    , drNotes["actionNeeded"].ToString()
                                                    , drNotes["actionCompleted"].ToString()
                                                    );
                        notes += detailNote;
                    }
                }

                //Get HostFamily for current quarter
                //Get current quarter
                DataTable dtQMD = QMD.GetRangeRows_Quarter_MetaData_Base(0, 0);
                string currentQuarter = dtQMD.Rows[0]["Name"].ToString() + " " + Convert.ToDateTime(dtQMD.Rows[0]["FirstDayOfClass"]).Year.ToString();
                //string hostFamily
                string homeName = GetCurrentFamilyName(applicantID);

      

                string hostFamily = homeName;

                var report = new StudentReport
                {
                    FullName = dr["familyName"].ToString() + ", " + dr["firstName"].ToString() + " " + dr["middleNames"].ToString(),
                    Status = dr["homestayStatus"].ToString(),
                    Gender = dr["Gender"].ToString(),
                    Id = dr["SID"].ToString(),
                    Campus = strCollege,
                    Age = Calculate(bDate).ToString(),
                    DOB = bDate.ToShortDateString(),
                    Country = dr["countryOfCitizenship"].ToString(),
                    Email = dr["Email"].ToString(),
                    CellPhone = localPhone,
                    Interest = studyWhat,
                    QuarterStartDate = Convert.ToDateTime(dr["quarterStartDate"].ToString().Trim()).ToShortDateString(),
                    Notes = notes,
                    HostFamily = hostFamily,
                    SuccessMeeting = "No", 
                    Agreement = "Fall 2019", 
                    LocalAddress = localAddress
                };
                reports.Add(report);
            }
                
            
        }
                                                                                                                                                                                                                                                                                                                                                                                                                     
        var js = new JavaScriptSerializer();
        Context.Response.Write(js.Serialize(reports));
    }

    //Get the name of family that student with applicantID is being placed with now. 
    //It will look at effectiveQuarter and endQuarter to decide whether the student is being placed now.
    private string GetCurrentFamilyName(int applicantID)
    {
        string sqlQuery = @"SELECT *
                                      FROM [CCSInternationalStudent].[dbo].[HomestayPlacement]
                                      where applicantID =" + applicantID;
        Homestay hs = new Homestay();
        DataTable dtPlacements = hs.RunGetQuery(sqlQuery);

        string homeName = "N/A";
        if(dtPlacements != null && dtPlacements.Rows.Count > 0)
        {
            foreach (DataRow drPlacement in dtPlacements.Rows)
            {
                string effectiveQuarter = drPlacement["effectiveQuarter"].ToString();
                DateTime firstDate = Convert.ToDateTime(GetDateByQuarter(effectiveQuarter, "StartDate"));


                string endQuarter = drPlacement["endQuarter"].ToString();
                string lastDate = GetDateByQuarter(endQuarter, "EndDate");
                if (String.IsNullOrEmpty(endQuarter))
                {
                    //Only check the startDate
                    if (DateTime.Compare(DateTime.Now, firstDate) == 0
                        || DateTime.Compare(DateTime.Now, firstDate) > 0)
                    {
                        homeName = drPlacement["placementName"].ToString();
                    }
                }
                else
                {

                    //check both start and end
                    DateTime endDate = Convert.ToDateTime(GetDateByQuarter(endQuarter, "EndDate"));
                    if ((DateTime.Compare(DateTime.Now, firstDate) == 0 || DateTime.Compare(DateTime.Now, firstDate) > 0)
                        && (DateTime.Compare(DateTime.Now, endDate) == 0 || DateTime.Compare(DateTime.Now, endDate) < 0))
                    {
                        homeName = drPlacement["placementName"].ToString();
                    }
                }
            }
        }
        
        return homeName;
    }

    //dateType = "FirstDayOfClass" or "LastDayOfClass"
    private string GetDateByQuarter(string quarter, string dateType)
    {
        if (String.IsNullOrEmpty(quarter))
        {
            return null; 
        }
        DataTable dtQMD = QMD.GetRangeRows_Quarter_MetaData_Base(-3, 15);
        string currentQuarter = dtQMD.Rows[0]["Name"].ToString() + " " + Convert.ToDateTime(dtQMD.Rows[0]["FirstDayOfClass"]).Year.ToString();
        if (dtQMD != null && dtQMD.Rows.Count > 0)
        {
            foreach(DataRow dr in dtQMD.Rows)
            {
                string drQuarter = dr["Name"].ToString() + " " + Convert.ToDateTime(dr["FirstDayOfClass"]).Year.ToString();
                if (quarter.Equals(drQuarter))
                {
                    return Convert.ToDateTime(dr[dateType].ToString()).ToString();
                }
            }
        }
        return null;
    }

    [WebMethod]
    public void GetDatatable_Family()
    {
        Homestay hsInfo = new Homestay();
        DataTable dtFamilies = hsInfo.GetHomestayFamilyInfo(0, "", "DDLactive");
        var familyReports = new List<FamilyReport>();
        if (dtFamilies != null)
        {
            if (dtFamilies.Rows.Count > 0)
            {
                foreach (DataRow dr in dtFamilies.Rows)
                {
                    Int32 intFamilyID = Convert.ToInt32(dr["id"].ToString());
                    DataTable dtFamilyInfo = hsInfo.GetHomestayFamilyInfo(intFamilyID, "", "ID");
                    DataTable dtRelatives = hsInfo.GetHomestayRelatives(0, intFamilyID);

                    
                    
                    if (dtFamilyInfo != null && dtFamilyInfo.Rows.Count > 0)
                    {
                        DataRow drFamilyInfo = dtFamilyInfo.Rows[0];
                        //Get TimeSCC and TimeSFCC
                        int busMinutesSFCC = drFamilyInfo.IsNull("busMinutesSFCC") ? 0 : Convert.ToInt32(dtFamilyInfo.Rows[0]["busMinutesSFCC"].ToString().Trim());
                        int busMinutesSCC = drFamilyInfo.IsNull("busMinutesSCC") ? 0 : Convert.ToInt32(dtFamilyInfo.Rows[0]["busMinutesSCC"].ToString().Trim());
                        int walkingBusStopMinutes = drFamilyInfo.IsNull("walkingBusStopMinutes") ? 0 : Convert.ToInt32(dtFamilyInfo.Rows[0]["walkingBusStopMinutes"].ToString().Trim());
                        //Get Address
                        string address = String.Format("{0}, {1}, {2}, {3}"
                                                        , drFamilyInfo["StreetAddress"].ToString()
                                                        , drFamilyInfo["City"].ToString()
                                                        , drFamilyInfo["State"].ToString().Trim()
                                                        , drFamilyInfo["ZIP"].ToString()
                                                        );
                        //Get HSOption
                        string hsOption = drFamilyInfo["homestayOption"].ToString();

                        //Get Students living with the family
                        
                        string students = GetStudentsLivingWithFamily(intFamilyID);
                        var familyReport = new FamilyReport
                        {
                            Id = dr["id"].ToString(),
                            HomeName = dr["homeName"].ToString(),
                            GenderPreference = drFamilyInfo["genderPreference"].ToString(),
                            FamilyStatus = drFamilyInfo["familyStatus"].ToString(),
                            CollegeQualified = drFamilyInfo["collegeQualified"].ToString(),
                            TimeSCC = busMinutesSCC.ToString(),
                            TimeSFCC = busMinutesSCC.ToString(),
                            PetsInHome = drFamilyInfo["petsInHome"].ToString(),
                            Address = address,
                            LandlinePhone = drFamilyInfo["landlinePhone"].ToString(),
                            NoOpenRoom = GetNoOpenRoom(intFamilyID),
                            AllowSmoking = GetAllowSmoking(intFamilyID),
                            HSOption = hsOption,
                            Student = students

                        };
                        familyReports.Add(familyReport);
                    }   
                }
                
            }
        }
        var js = new JavaScriptSerializer();
        Context.Response.Write(js.Serialize(familyReports));
    }

    private string GetStudentsLivingWithFamily(int intFamilyID)
    {
        string sqlQuery = @"SELECT *
                            FROM [CCSInternationalStudent].[dbo].[HomestayPlacement]
                            WHERE familyID =" + intFamilyID;
        Homestay hs = new Homestay();
        DataTable dtPlacements = hs.RunGetQuery(sqlQuery);
        string result = "";
        if (dtPlacements != null && dtPlacements.Rows.Count > 0) { }
        {
            foreach (DataRow drPlacement in dtPlacements.Rows)
            {
                int applicantID = Convert.ToInt32(drPlacement["applicantID"].ToString());
                DataTable dtInfo = hsInfo.GetOneInternationalStudent("", applicantID, "", "", "", "");
                DataRow drInfo = dtInfo.Rows[0];
                result += String.Format("Student: {0}, Room Number: {1}, Effective Quarter: {2}, End Quarter: {3} <br/>"
                                , drInfo["familyName"].ToString() + ", " + drInfo["firstName"].ToString()
                                , drPlacement["roomNumber"].ToString()
                                , drPlacement["effectiveQuarter"].ToString()
                                , drPlacement["endQuarter"].ToString()
                                );
            }
            
           
        }
        result = String.IsNullOrEmpty(result) ? "no students" : result;

        return result;
    }

    private static int Calculate(DateTime dateOfBirth)
    {
        int age = 0;
        age = DateTime.Now.Year - dateOfBirth.Year;
        if (DateTime.Now.DayOfYear < dateOfBirth.DayOfYear)
            age = age - 1;

        return age;
    }

    private static string GetNoOpenRoom(int intFamilyID)
    {
        DataTable dtFamily = hsInfo.GetHomestayFamilyInfo(intFamilyID, "", "ID");


        var occupancyCols = dtFamily.Columns.Cast<DataColumn>().Where(c => c.ColumnName.Contains("Occupancy"));
        int countOpenRoom = 0;
        foreach (DataColumn dt in occupancyCols)
        {
            if (dtFamily.Rows[0][dt.ColumnName].ToString().Contains("Open"))
            {
                countOpenRoom++;
            }
        }

        return countOpenRoom.ToString();
    }

    private static string GetAllowSmoking(int intFamilyID)
    {
        DataTable dtFamPref = hsInfo.GetHomestayPreferenceSelections(0, intFamilyID);
        var result = dtFamPref.AsEnumerable().FirstOrDefault(myRow => myRow.Field<String>("Preference").Contains("Other smoker"));
        if (result != null)
        {
            if (result["Preference"].ToString().Contains("not"))
            {
                return "No";
            }
            else
            {
                return "Yes";
            }
        }

        return "N/A";
    }
   
    

    
}
