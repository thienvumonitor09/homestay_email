﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CCSApps.master" AutoEventWireup="true" CodeFile="MatchPreferences.aspx.cs" Inherits="Homestay_Admin_MatchPreferences" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="bannerH1Text" Runat="Server">
    Match Preferenes
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">
    <div style="float:left;width:70%;">
        <asp:Button ID="btnCmdReset" runat="server" Text="Reset Fields" OnClick="btnCmdReset_Click" />&nbsp;&nbsp;<asp:Literal ID="litResultMsg" runat=server></asp:Literal>      
        </div>
    <div id="divHome" runat="server" style="float:right;margin-right:30px;">
        <input type="submit" name="btnSubmit" id="btnHome" value="Return to Admin Home Page"/>&nbsp;&nbsp;
    </div>
    <div style="margin-bottom:10px;clear:both;width:25%;float:left;">
        <h3>Select a Student</h3>
        <asp:DropDownList ID="ddlStudents" runat="server" autopostback="true"></asp:DropDownList>
    </div>
    <div style="float:left;width:75%;margin-bottom:10px;">
        <h3>Current Status</h3>
        <asp:Literal ID="litCurrStudentStatus" runat="server" Text="&nbsp;"></asp:Literal>
    </div>
    <h3>Level of Matching</h3>
    <div style="float:left;width:25%;margin-bottom:10px;">
        <p> <label for="rdoGender">Gender</label>
            <asp:RadioButtonList ID="rdoGender" RepeatDirection="Horizontal" runat="server">
                <asp:ListItem Value="Include"></asp:ListItem>
                <asp:ListItem Value="Omit"></asp:ListItem>
            </asp:RadioButtonList>
        </p>
    </div>
    <div style="float:left;width:25%;margin-bottom:10px;">
        <p> <label for="rdoHomestay">Homestay Preference</label>
            <asp:RadioButtonList ID="rdoHomestay" RepeatDirection="Horizontal" runat="server">
                <asp:ListItem Value="Include"></asp:ListItem>
                <asp:ListItem Value="Omit"></asp:ListItem>
            </asp:RadioButtonList>
        </p>
    </div>
    <div style="float:left;width:25%;margin-bottom:10px;">
        <p> <label for="rdoCollege">College</label>
            <asp:RadioButtonList ID="rdoCollege" RepeatDirection="Horizontal" runat="server">
                <asp:ListItem Value="SCC"></asp:ListItem>
                <asp:ListItem Value="SFCC"></asp:ListItem>
            </asp:RadioButtonList>
        </p>
    </div>
    <div style="float:left;width:25%;margin-bottom:10px;">
        <p> <label for="rdoSmoking">Smoking Habits</label>
            <asp:RadioButtonList ID="rdoSmoking" RepeatDirection="Horizontal" runat="server">
                <asp:ListItem Value="Include"></asp:ListItem>
                <asp:ListItem Value="Omit"></asp:ListItem>
            </asp:RadioButtonList>
        </p>
    </div>
    <p>
        <input type="submit" name="btnSubmit" value="Search" />
    </p>
    <asp:HiddenField ID="hdnGender" runat="server" />
    <asp:HiddenField ID="hdnOption" runat="server" />
    <asp:HiddenField ID="hdnAge" runat="server" />
    <asp:HiddenField ID="hdnSmoke" runat="server" />
    <asp:HiddenField ID="hdnStuID" runat="server" />
    <asp:HiddenField ID="hdnStuRoom" runat="server" />
    <asp:HiddenField ID="hdnStuFamily" runat="server" />
    <asp:HiddenField ID="hdnFamID" runat="server" />
    <asp:HiddenField ID="hdnHomeName" runat="server" />
    <asp:HiddenField ID="hdnRm1Placement" runat="server" />
    <asp:HiddenField ID="hdnRm2Placement" runat="server" />
    <asp:HiddenField ID="hdnRm3Placement" runat="server" />
    <asp:HiddenField ID="hdnRm4Placement" runat="server" />

    <h3>Families that Match</h3>
    <div id="divFamilies" runat="server" style="margin-bottom:10px;width:49%;float:left;">
        <asp:DropDownList ID="ddlFamilies" runat="server" autopostback="true"></asp:DropDownList>&nbsp;&nbsp;&nbsp;&nbsp;<input type="submit" name="btnSubmit" value="Compare" /><br />
        <asp:Label ID="litRoomWithOtherFamily" runat="server" Text=""></asp:Label>
    </div>
     <div id="divRoomPlacement" runat="server" style="margin-bottom:10px;width:49%;float:left;">
        <asp:Table ID="roomStatus" runat="server" Width="90%" BorderWidth="0px">
            <asp:TableHeaderRow>
                <asp:TableCell Width="10%"><b>Room</b></asp:TableCell>
                <asp:TableCell Width="20%"><b>Status</b></asp:TableCell>
                <asp:TableCell Width="20%"><b>Placement</b></asp:TableCell>
                <asp:TableCell Width="15%"><b>Effective Date</b></asp:TableCell>
            </asp:TableHeaderRow>
            <asp:TableRow>
                <asp:TableCell Width="10%">Room 1</asp:TableCell>
                <asp:TableCell ID="rm1Status" Width="20%"><asp:Literal ID="litRm1Status" runat="server"></asp:Literal></asp:TableCell>
                <asp:TableCell ID="rm1Placement" Width="20%">
                    <asp:DropDownList ID="ddRm1Placement" runat="server" onchange="newData('MainContent_ddRm1Placement')">
                    <asp:ListItem Value="Select one"></asp:ListItem>
                        <asp:ListItem Value="Not Available"></asp:ListItem>
                        <asp:ListItem Value="Open"></asp:ListItem>
                        <asp:ListItem Value="Tentative Placement"></asp:ListItem>
                        <asp:ListItem Value="Student Placed"></asp:ListItem>
                        <asp:ListItem Value="Withdrew"></asp:ListItem>
                        <asp:ListItem Value="Graduated"></asp:ListItem>
                        <asp:ListItem Value="Student Moved Out"></asp:ListItem>
                  </asp:DropDownList>
                </asp:TableCell>
                <asp:TableCell ID="rm1EffectiveDate" Width="15%"><asp:TextBox ID="txtRm1Date" runat="server" onchange="isDate(this.value, '/')"></asp:TextBox>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell Width="10%">Room 2</asp:TableCell>
                <asp:TableCell ID="rm2Status" Width="20%"><asp:Literal ID="litRm2Status" runat="server"></asp:Literal></asp:TableCell>

                <asp:TableCell ID="rm2Placement" Width="20%">
                    <asp:DropDownList ID="ddRm2Placement" runat="server" onchange="newData('MainContent_ddRm2Placement')">
                        <asp:ListItem Value="Select one"></asp:ListItem>
                        <asp:ListItem Value="Not Available"></asp:ListItem>
                        <asp:ListItem Value="Open"></asp:ListItem>
                        <asp:ListItem Value="Tentative Placement"></asp:ListItem>
                        <asp:ListItem Value="Student Placed"></asp:ListItem>
                        <asp:ListItem Value="Withdrew"></asp:ListItem>
                        <asp:ListItem Value="Graduated"></asp:ListItem>
                        <asp:ListItem Value="Student Moved Out"></asp:ListItem>
                  </asp:DropDownList>
                </asp:TableCell>
                <asp:TableCell ID="rm2EffectiveDate" Width="15%"><asp:TextBox ID="txtRm2Date" runat="server" onchange="isDate(this.value)"></asp:TextBox>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell Width="10%">Room 3</asp:TableCell>
                <asp:TableCell ID="rm3Status" Width="20%"><asp:Literal ID="litRm3Status" runat="server"></asp:Literal></asp:TableCell>

                <asp:TableCell ID="rm3Placement" Width="20%">
                    <asp:DropDownList ID="ddRm3Placement" runat="server" onchange="newData('MainContent_ddRm3Placement')">
                    <asp:ListItem Value="Select one"></asp:ListItem>
                        <asp:ListItem Value="Not Available"></asp:ListItem>
                        <asp:ListItem Value="Open"></asp:ListItem>
                        <asp:ListItem Value="Tentative Placement"></asp:ListItem>
                        <asp:ListItem Value="Student Placed"></asp:ListItem>
                        <asp:ListItem Value="Withdrew"></asp:ListItem>
                        <asp:ListItem Value="Graduated"></asp:ListItem>
                        <asp:ListItem Value="Student Moved Out"></asp:ListItem>
                  </asp:DropDownList>
                </asp:TableCell>
                <asp:TableCell ID="rm3EffectiveDate" Width="15%"><asp:TextBox ID="txtRm3Date" runat="server" onchange="isDate(this.value)"></asp:TextBox>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell Width="10%">Room 4</asp:TableCell>
                <asp:TableCell ID="rm4Status" Width="20%"><asp:Literal ID="litRm4Status" runat="server"></asp:Literal></asp:TableCell>

                <asp:TableCell ID="rm4Placement" Width="20%">
                    <asp:DropDownList ID="ddRm4Placement" runat="server" onchange="newData('MainContent_ddRm4Placement')">
                    <asp:ListItem Value="Select one"></asp:ListItem>
                        <asp:ListItem Value="Not Available"></asp:ListItem>
                        <asp:ListItem Value="Open"></asp:ListItem>
                        <asp:ListItem Value="Tentative Placement"></asp:ListItem>
                        <asp:ListItem Value="Student Placed"></asp:ListItem>
                        <asp:ListItem Value="Withdrew"></asp:ListItem>
                        <asp:ListItem Value="Graduated"></asp:ListItem>
                        <asp:ListItem Value="Student Moved Out"></asp:ListItem>
                  </asp:DropDownList>
                </asp:TableCell>
                <asp:TableCell ID="rm4EffectiveDate" Width="15%"><asp:TextBox ID="txtRm4Date" runat="server" onchange="isDate(this.value)"></asp:TextBox>
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>

        
    </div>
    <p>
        <span id="compareBtn" style="float:left;width:49%;">&nbsp;</span>
        <span id="placeBtn" style="width:25%"><input type="submit" name="btnSubmit" value="Place Student with selected family" /></span>
        <asp:Label ID="lblBadDate" runat="server" Text="" style="float:right;margin-right:50px;"></asp:Label>
    </p>


    <div id="studentProfile" runat="server" style="float:left;width:49%;margin-bottom:10px;">
        <h3>Student Profile</h3>
        <p>
            <label for="studentNotes">Notes</label><br />
            <asp:TextBox ID="studentNotes" runat="server" TextMode="MultiLine" Rows="5" Columns="40"></asp:TextBox>
        </p>
        <div style="height:275px;">
            <h4>Smoking Habits</h4>
                <p>
                <label for="rdoSmokerStu">Do you smoke?</label><br /> 
                <asp:RadioButtonList ID="rdoSmokerStu" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
                    <asp:ListItem Value="Smoker">Yes&nbsp;&nbsp;</asp:ListItem>
                    <asp:ListItem Value="Occasional smoker">Sometimes&nbsp;&nbsp;</asp:ListItem>
                    <asp:ListItem Value="Nonsmoker">No&nbsp;&nbsp;</asp:ListItem>
                </asp:RadioButtonList>
            </p>
            <p>
                <label for="rdoOtherSmokerOKStu">Other smokers OK?</label><br />
                <asp:RadioButtonList ID="rdoOtherSmokerOKStu" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
                    <asp:ListItem Value="Other smoker OK">Yes&nbsp;&nbsp;</asp:ListItem>
                    <asp:ListItem Value="Other smoker not OK">No&nbsp;&nbsp;</asp:ListItem>
                    <asp:ListItem Value="Other smoker no preference">No Preference</asp:ListItem>
                </asp:RadioButtonList>
            </p>
            <p>
                <label for="smokingHabitsStu">Explain:</label><br />
                <asp:TextBox ID="smokingHabitsStu" runat="server" Width="98%" MaxLength="100"></asp:TextBox>
            </p>
        </div>
        <div style="height:250px;">
            <h4>Home Environment</h4>
            <p>
                <label for="rdoInteractionStu">Interaction and Conversation</label><br />
                <asp:RadioButtonList ID="rdoInteractionStu" runat="server" RepeatDirection="Vertical" RepeatLayout="Flow" style="width:100%;">
                    <asp:ListItem Value="Interaction every day">Every Day</asp:ListItem>
                    <asp:ListItem Value="Interaction frequent">Frequent, but not every day</asp:ListItem>
                    <asp:ListItem Value="Interaction minimal">Minimal conversation is okay</asp:ListItem>
                </asp:RadioButtonList>
            </p>
            <p>
                <label for="rdoHomeEnvironmentStu">HomeEnvironment</label><br />
                <asp:RadioButtonList ID="rdoHomeEnvironmentStu" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
                    <asp:ListItem Value="Home environment quiet">Quiet&nbsp;&nbsp;</asp:ListItem>
                    <asp:ListItem Value="Home busy and active">Busy and Active&nbsp;&nbsp;</asp:ListItem>
                </asp:RadioButtonList>
            </p>
            <p>
                <label for="homeEnvironmentPreferencesStu">Explain:</label><br />
                <asp:TextBox ID="homeEnvironmentPreferencesStu" runat="server" Width="98%" MaxLength="100"></asp:TextBox>
            </p>
        </div>
        <div style="height:155px;padding-top:20px;">
            <h4>Children</h4>
            <p>
                <label for="rdoSmallChildren">Preference</label><br />
                <asp:RadioButtonList ID="rdoSmallChildrenStu" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
                    <asp:ListItem Value="Young children">Young children&nbsp;&nbsp;</asp:ListItem>
                    <asp:ListItem Value="Older children">Older children&nbsp;&nbsp;</asp:ListItem>
                    <asp:ListItem Value="No children">No children</asp:ListItem>
                    <asp:ListItem Value="Children no preference">No Preference</asp:ListItem>
                </asp:RadioButtonList>
            </p>
        </div>
        <div style="height:350px;">
            <h4>Lifestyle</h4>
            <b>Dietary needs:</b><br />
            <asp:CheckBox ID="VegetarianStu" runat="server" Text="Vegetarian" /><br />
            <asp:CheckBox ID="GlutenFreeStu" runat="server" Text="Gluten free" /><br />
            <asp:CheckBox ID="DairyFreeStu" runat="server" Text="Dairy free" /><br />
            <asp:CheckBox ID="OtherFoodAllergyStu" runat="server" Text="Other food allergy" /><br />
            <asp:CheckBox ID="HalalStu" runat="server" Text="Halal" />  (Food prepared as prescribed by Muslim law)<br />
            <asp:CheckBox ID="KosherStu" runat="server" Text="Kosher" /><br />
            <asp:CheckBox ID="NoSpecialDietStu" runat="server" Text="I have no special dietary needs" /><br />
            <p>
                <label for="rdoTraveledOutsideCountryStu">Have you traveled outside the U.S. before?</label><br />
                <asp:RadioButtonList ID="rdoTraveledOutsideCountryStu" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
                    <asp:ListItem Value="Traveled outside country">Yes&nbsp;&nbsp;</asp:ListItem>
                    <asp:ListItem Value="Not traveled outside country">No&nbsp;&nbsp;</asp:ListItem>
                </asp:RadioButtonList>
            </p>
            <p>
                <label for="traveledWhere">Explain:</label><br />
                <asp:TextBox ID="traveledWhereStu" runat="server" Width="98%" MaxLength="100"></asp:TextBox>
            </p>
        </div>
        <div style="height:400px;">
            <p><b>Hobbies and activities:</b><br />
            <asp:CheckBox ID="MusicalInstrumentStu" runat="server" Text="Play a musical instrument" /><br />
            <asp:CheckBox ID="ArtStu" runat="server" Text="Drawing/painting" /><br />
            <asp:CheckBox ID="TeamSportsStu" runat="server" Text="Team sports" /><br />
            <asp:CheckBox ID="IndividualSportsStu" runat="server" Text="Individual sports" /><br />
            <asp:CheckBox ID="ListenMusicStu" runat="server" Text="Listen to music" /><br />
            <asp:CheckBox ID="DramaStu" runat="server" Text="Drama" /><br />
            <asp:CheckBox ID="WatchMoviesStu" runat="server" Text="Watch movies" /><br />
            <asp:CheckBox ID="SingingStu" runat="server" Text="Singing" /><br />
            <asp:CheckBox ID="ShoppingStu" runat="server" Text="Shopping" /><br />
            <asp:CheckBox ID="ReadBooksStu" runat="server" Text="Read books" /><br />
            <asp:CheckBox ID="OutdoorsStu" runat="server" Text="Hiking/camping/outdoors" /><br />
            <asp:CheckBox ID="CookingStu" runat="server" Text="Cooking" /><br />
            <asp:CheckBox ID="PhotographyStu" runat="server" Text="Photography" /><br />
            <asp:CheckBox ID="GamingStu" runat="server" Text="Gaming" />
            </p>
        </div>
        <p>
            <label for="activitiesEnjoyedStu">Other hobbies/activities you enjoy:</label><br />
            <asp:TextBox ID="activitiesEnjoyedStu" runat="server" Width="98%" MaxLength="100"></asp:TextBox>
        </p>
        <p>
            <label for="allergies">Do you have any allergies to food, drugs, animals or anything else?</label><br />
            <asp:TextBox ID="allergiesStu" runat="server" Width="98%" MaxLength="100"></asp:TextBox>
        </p>
        <p>
            <label for="healthConditions">Do you have any health conditions that would prevent you from participation in normal and regular physical activities?</label><br />
            <asp:TextBox ID="healthConditionsStu" runat="server" Width="98%" MaxLength="100"></asp:TextBox>
        </p>
        <p>
            <label for="anythingElseStu">Is there anything else you'd like us to know about your family?</label><br />
            <asp:TextBox ID="anythingElseStu" runat="server" Width="98%" MaxLength="300"></asp:TextBox>
        </p>
    </div>
        
    <div id="divFamilyProfile" runat="server" style="float:left;width:49%;margin-bottom:10px;">
        <h3>Family Profile</h3>
        <p>
            <label for="familyNotes">Notes</label><br />
            <asp:TextBox ID="familyNotes" runat="server" TextMode="MultiLine" Rows="5" Columns="40"></asp:TextBox>
        </p>
        <div style="height:275px;">
            <h4>Smoking Habits</h4>
                <p>
                <label for="rdoSmoker">Do you smoke?</label><br /> 
                <asp:RadioButtonList ID="rdoSmoker" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
                    <asp:ListItem Value="Smoker">Yes&nbsp;&nbsp;</asp:ListItem>
                    <asp:ListItem Value="Occasional smoker">Sometimes&nbsp;&nbsp;</asp:ListItem>
                    <asp:ListItem Value="Nonsmoker">No&nbsp;&nbsp;</asp:ListItem>
                </asp:RadioButtonList>
            </p>
            <p>
                <label for="rdoOtherSmokerOK">Other smokers OK?</label><br />
                <asp:RadioButtonList ID="rdoOtherSmokerOK" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
                    <asp:ListItem Value="Other smoker OK">Yes&nbsp;&nbsp;</asp:ListItem>
                    <asp:ListItem Value="Other smoker not OK">No&nbsp;&nbsp;</asp:ListItem>
                    <asp:ListItem Value="Other smoker no preference">No Preference</asp:ListItem>
                </asp:RadioButtonList>
            </p>
            <p>
                <label for="smokingHabits">Explain:</label><br />
                <asp:TextBox ID="smokingHabits" runat="server" Width="98%" MaxLength="100"></asp:TextBox>
            </p>
            <p>
                <label for="smokingHabits">Smoking Guidelines</label><br />
                <asp:TextBox ID="smokingGuidelines" runat="server" Width="98%" MaxLength="500"></asp:TextBox>
            </p>
        </div>
        <div style="height:250px;">
            <h4>Home Environment</h4>
            <p>
                <label for="rdoInteraction">Interaction and Conversation</label><br />
                <asp:RadioButtonList ID="rdoInteraction" runat="server" RepeatDirection="Vertical" RepeatLayout="Flow" style="width:100%;">
                    <asp:ListItem Value="Interaction every day">Every Day</asp:ListItem>
                    <asp:ListItem Value="Interaction frequent">Frequent, but not every day</asp:ListItem>
                    <asp:ListItem Value="Interaction minimal">Minimal conversation is okay</asp:ListItem>
                </asp:RadioButtonList>
            </p>
            <p>
                <label for="rdoHomeEnvironment">HomeEnvironment</label><br />
                <asp:RadioButtonList ID="rdoHomeEnvironment" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
                    <asp:ListItem Value="Home quiet">Quiet&nbsp;&nbsp;</asp:ListItem>
                    <asp:ListItem Value="Home busy and active">Busy and Active&nbsp;&nbsp;</asp:ListItem>
                </asp:RadioButtonList>
            </p>
            <p>
                <label for="homeEnvironmentPreferences">Explain:</label><br />
                <asp:TextBox ID="homeEnvironmentPreferences" runat="server" Width="98%" MaxLength="100"></asp:TextBox>
            </p>
        </div>
        <div style="height:155px;padding-top:20px;">
            <h4>Minor children in home</h4>
            <p>
                <asp:TextBox ID="SmallChildrenInHome" runat="server" Width="98%"  Rows="3" Columns="60" TextMode="MultiLine"></asp:TextBox>
            </p>
        </div>
        <div style="height:350px;">
            <h4>Lifestyle</h4>
            <b>Willing to accommodate:</b><br />
            <asp:CheckBox ID="Vegetarian" runat="server" Text="Vegetarian" /><br />
            <asp:CheckBox ID="GlutenFree" runat="server" Text="Gluten free" /><br />
            <asp:CheckBox ID="DairyFree" runat="server" Text="Dairy free" /><br />
            <asp:CheckBox ID="OtherFoodAllergy" runat="server" Text="Other food allergy" /><br />
            <asp:CheckBox ID="Halal" runat="server" Text="Halal" />  (Food prepared as prescribed by Muslim law)<br />
            <asp:CheckBox ID="Kosher" runat="server" Text="Kosher" /><br />
            <asp:CheckBox ID="NoSpecialDiet" runat="server" Text="I prefer a student with no special dietary needs" /><br />
            <p>
                <label for="rdoTraveledOutsideCountry">Have you traveled outside the U.S. before?</label><br />
                <asp:RadioButtonList ID="rdoTraveledOutsideCountry" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
                    <asp:ListItem Value="Traveled outside country">Yes&nbsp;&nbsp;</asp:ListItem>
                    <asp:ListItem Value="Not traveled outside country">No&nbsp;&nbsp;</asp:ListItem>
                </asp:RadioButtonList>
            </p>
             <p>
                <label for="traveledWhere">Explain:</label><br />
                <asp:TextBox ID="traveledWhere" runat="server" Width="98%" MaxLength="100"></asp:TextBox>
            </p>
        </div>
        <div style="height:400px;">
           <p><b>Hobbies and activities:</b><br />
            <asp:CheckBox ID="MusicalInstrument" runat="server" Text="Play a musical instrument" /><br />
            <asp:CheckBox ID="Art" runat="server" Text="Drawing/painting" /><br />
            <asp:CheckBox ID="TeamSports" runat="server" Text="Team sports" /><br />
            <asp:CheckBox ID="IndividualSports" runat="server" Text="Individual sports" /><br />
            <asp:CheckBox ID="ListenMusic" runat="server" Text="Listen to music" /><br />
            <asp:CheckBox ID="Drama" runat="server" Text="Drama" /><br />
            <asp:CheckBox ID="WatchMovies" runat="server" Text="Watch movies" /><br />
            <asp:CheckBox ID="Singing" runat="server" Text="Singing" /><br />
            <asp:CheckBox ID="Shopping" runat="server" Text="Shopping" /><br />
            <asp:CheckBox ID="ReadBooks" runat="server" Text="Read books" /><br />
            <asp:CheckBox ID="Outdoors" runat="server" Text="Hiking/camping/outdoors" /><br />
            <asp:CheckBox ID="Cooking" runat="server" Text="Cooking" /><br />
            <asp:CheckBox ID="Photography" runat="server" Text="Photography" /><br />
            <asp:CheckBox ID="Gaming" runat="server" Text="Gaming" />
        </p>
        </div>
        <p>
            <label for="activitiesEnjoyed">Other hobbies/activities you enjoy:</label><br />
            <asp:TextBox ID="activitiesEnjoyed" runat="server" Width="98%" MaxLength="100"></asp:TextBox>
        </p>
        <p>
            <label for="petsInHome">Do you have pets? What type and what areas of your home are they free to occupy?</label><br />
            <asp:TextBox ID="petsInHome" runat="server" MaxLength="100" Rows="3" Columns="40" TextMode="MultiLine"></asp:TextBox>
        </p>
        <p>
            <label for="anythingElse">Is there anything else you'd like us to know about your family?</label><br />
            <asp:TextBox ID="anythingElse" runat="server" Width="98%" MaxLength="300"></asp:TextBox>
        </p>
    </div>
        <script type="text/javascript">
    function newData(itemID) {
        txtSubString = "txt";
        var newString;
        var myObj = document.getElementById(itemID);
        if (itemID.includes(txtSubString)) {
            newString = itemID.replace("txt", "hdn");
        } else {
            newString = itemID.replace("dd", "hdn");
        }
        var hdnField = document.getElementById(newString);
        hdnField.value = myObj.value;
            }

            //Value parameter - required. All other parameters are optional.
function isDate(value, sepVal, dayIdx, monthIdx, yearIdx) {
    var blankMsg = document.getElementById("MainContent_lblBadDate");
    blankMsg.innerHTML = "";
    try {
        //Change the below values to determine which format of date you wish to check. It is set to dd/mm/yyyy by default.
        var MonthIndex = monthIdx !== undefined ? monthIdx : 0;
        var DayIndex = dayIdx !== undefined ? dayIdx : 1; 
        var YearIndex = yearIdx !== undefined ? yearIdx : 2;
        value = value.replace(/-/g, "/").replace(/\./g, "/"); 
        var SplitValue = value.split("/");
        var OK = true;
        
        if (OK && !(SplitValue[MonthIndex].length == 1 || SplitValue[MonthIndex].length == 2)) {
            OK = false;
        }
        if (!(SplitValue[DayIndex].length == 1 || SplitValue[DayIndex].length == 2)) {
            OK = false;
        }
        if (OK && SplitValue[YearIndex].length != 4) {
            OK = false;
        }
        if (OK) {
            var Day = parseInt(SplitValue[DayIndex], 10);
            var Month = parseInt(SplitValue[MonthIndex], 10);
            var Year = parseInt(SplitValue[YearIndex], 10);
 
            if (OK = ((Year > 1900) && (Year < new Date().getFullYear()))) {
                if (OK = (Month <= 12 && Month > 0)) {

                    var LeapYear = (((Year % 4) == 0) && ((Year % 100) != 0) || ((Year % 400) == 0));   
                    
                    if(OK = Day > 0)
                    {
                        if (Month == 2) {  
                            OK = LeapYear ? Day <= 29 : Day <= 28;
                        } 
                        else {
                            if ((Month == 4) || (Month == 6) || (Month == 9) || (Month == 11)) {
                                OK = Day <= 30;
                            }
                            else {
                                OK = Day <= 31;
                            }
                        }
                    }
                }
            }
        }
        return OK;
    }
    catch (e) {
        var falseMsg = document.getElementById("MainContent_lblBadDate");
        falseMsg.innerHTML = "<span style='color:#DD0000'>Please enter a date in correct format = mm/dd/yyyy</span>.";
        //return false;
    }
} 
</script>
</asp:Content>


