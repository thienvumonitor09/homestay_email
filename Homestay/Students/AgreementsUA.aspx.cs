﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Security.Principal;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Homestay_Students_Agreements : System.Web.UI.Page
{
    String strAppPart = "";
    String applicantID = "";
    bool blNoButtons = false;
    Int32 intApplicantID = 0;
    String strResultMsg = "";
    Int32 intResult = 0;
    String strUploadURL = @"\\ccs-internet\InternetContent\CCS\Homestay\";
    String strFileName = "";
    String strLastName = "";
    String strFirstName = "";
    Boolean blWaiveLiabilityParentUASubmitted = false;
    Boolean blCCSConductUASubmitted = false;
    Boolean blNoticesWaiverParentUASubmitted = false;
    DataTable dtStudent;
    DataTable dtHomestayStudent;
    
    Boolean blIsAdmin = false;
    Homestay hsInfo = new Homestay();

    protected void Page_Load(object sender, EventArgs e)
    {
        String strButtonFace = "";
        String strLogonUser = Request.ServerVariables["LOGON_USER"];    

        if (strLogonUser != "" && strLogonUser != null)
        {
            //If user has not logged out of Family Portal, forms authentication considers the person logged in
            if (strLogonUser.Contains("ccs\\"))
            {
                blIsAdmin = true;
            }
        }
        

        if (Request.QueryString["ID"] != null)
        {
            //page load
            applicantID = Request.QueryString["ID"].ToString();
            hdnApplicantID.Value = applicantID;
        }
        if (applicantID == "")
        {
            //after postback
            applicantID = hdnApplicantID.Value;
        }
        if (applicantID != "" && applicantID != null && applicantID != "0")
        {
            intApplicantID = Convert.ToInt32(applicantID);
        }
        if (intApplicantID != 0)
        {
            dtStudent = hsInfo.GetOneInternationalStudent("", intApplicantID, "", "", "", "");
            if (dtStudent != null)
            {
                if (dtStudent.Rows.Count > 0)
                {
                    strFirstName = dtStudent.Rows[0]["firstName"].ToString();
                    strLastName = dtStudent.Rows[0]["familyName"].ToString();
                }
            }
            dtHomestayStudent = hsInfo.GetOneHomestayStudent(intApplicantID);
            if (dtHomestayStudent != null)
            {
                if (dtHomestayStudent.Rows.Count > 0)
                {
                    blWaiveLiabilityParentUASubmitted = Convert.ToBoolean(dtHomestayStudent.Rows[0]["WaiveLiabilityParentUASubmitted"]);
                    blCCSConductUASubmitted = Convert.ToBoolean(dtHomestayStudent.Rows[0]["CCSConductUASubmitted"]);
                    blNoticesWaiverParentUASubmitted = Convert.ToBoolean(dtHomestayStudent.Rows[0]["NoticesWaiverParentUASubmitted"]);
                }
            }
        }
        if (blWaiveLiabilityParentUASubmitted || blCCSConductUASubmitted || blNoticesWaiverParentUASubmitted)
        {
            //get the files already uploaded and display
            Configuration config = WebConfigurationManager.OpenWebConfiguration(Request.ApplicationPath);
            AppSettingsSection appSettings = (AppSettingsSection)config.GetSection("appSettings");
            String strUploadServer = "/InternetContent" + appSettings.Settings["HomestayShare"].Value;
            litWaiveLiabilityParentUAFile.Text = "";
            litCCSConductUAFile.Text = "";
            litNoticesWaiverParentUAFile.Text = "";
            DataTable dt = hsInfo.GetUploadedFiles(intApplicantID);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        strFileName = dr["fileName"].ToString();
                        if (strFileName.Contains("WaiveLiabilityParentUA"))
                        {
                            litWaiveLiabilityParentUAFile.Text += "<br /><b>Waiver of Liability (Parents) submitted:</b> " + "<a href=\"" + strUploadServer + strFileName + "\" target=\"_blank\">" + strFileName + "</a>";
                        }
                        if (strFileName.Contains("CCSConductUA"))
                        {
                            litCCSConductUAFile.Text += "<br /><b>CCS Conduct Agreement submitted:</b> " + "<a href=\"" + strUploadServer + strFileName + "\" target=\"_blank\">" + strFileName + "</a>";
                        }
                        if (strFileName.Contains("NoticesWaiverParentUA"))
                        {
                            litNoticesWaiverParentUAFile.Text += "<br /><b>Notices and Waiver of Liability (Parents) submitted:</b> " + "<a href=\"" + strUploadServer + strFileName + "\" target=\"_blank\">" + strFileName + "</a>";
                        }
                    }
                }
            }
        }

        if (Request.Form["btnSubmit"] != null)
        {
            strButtonFace = Request.Form["btnSubmit"].ToString();
        }
        if (Request.QueryString["part"] != null)
        {
            strAppPart = Request.QueryString["part"].ToString();
            switch (strAppPart) {
                case "2":
                    strButtonFace = "Continue Part 2";
                    break;
                case "3":
                    strButtonFace = "Continue Part 3";
                    break;
            }
            blNoButtons = true;
        }

        switch (strButtonFace)
        {
            case "Continue Part 2":
                //process submission
                strResultMsg += "WaiveLiabilityParentUA Checked? " + chkWaiveLiabilityParentUA.Checked + "<br />";
                if (chkWaiveLiabilityParentUA.Checked)
                {
                    intResult = hsInfo.UpdateUnderstandAgreeHomestayStudentInfo(intApplicantID, 1, "WaiveLiabilityParentUA");
                }
                //Upload the Waive Liability Parent UA agreement
                if (uplWaiveLiabilityParentUAFile.HasFile)
                {
                    strFileName = strLastName + "-" + intApplicantID + "-WaiveLiabilityParentUA-" + uplWaiveLiabilityParentUAFile.FileName;
                    uplCCSConductUAFile.SaveAs(strUploadURL + strFileName);
                    strResultMsg += "Your agreement, <span style=\"color:red;\">" + strFileName + "</span>, was successfully uploaded!<br />";
                    //Insert record in UploadedFilesInformation table
                    intResult = hsInfo.InsertUploadedFilesInformation(intApplicantID, strFirstName + " " + strLastName, strFileName, strUploadURL);
                    blWaiveLiabilityParentUASubmitted = true;
                    //Update status of submission
                    intResult = hsInfo.UpdateAgreementSubmittedHomestayStudentInfo(intApplicantID, 1, "WaiveLiabilityParentUASubmitted");
                }
                else if (hdnWaiveLiabilityParentUASubmitted.Value.ToLower() == "true")
                {
                    //previous submission exists
                    blWaiveLiabilityParentUASubmitted = true;
                }
                //Visibility
                if (!blCCSConductUASubmitted) { divUploadCCSConductUA.Visible = true; }
                divUploadWaiveLiabilityParentUA.Visible = false;
                divConfirmation.Visible = false;
                btnDone.Visible = false;
                btnContinue2.Visible = false;
                btnContinue3.Visible = true;
                btnPrevious.Visible = false;
                div1.Visible = false;
                div2.Visible = true;
                div3.Visible = false;
                break;
            case "Continue Part 3":
                //process submission
                strResultMsg += "CCSConductUA Checked? " + chkCCSConductUA.Checked + "<br />";
                if (chkCCSConductUA.Checked)
                {
                    intResult = hsInfo.UpdateUnderstandAgreeHomestayStudentInfo(intApplicantID, 1, "CCSConductUA");
                }
                //Upload the Waive Liability Parent UA agreement
                if (uplCCSConductUAFile.HasFile)
                {
                    strFileName = strLastName + "-" + intApplicantID + "-CCSConductUA-" + uplCCSConductUAFile.FileName;
                    uplCCSConductUAFile.SaveAs(strUploadURL + strFileName);
                    strResultMsg += "Your agreement, <span style=\"color:red;\">" + strFileName + "</span>, was successfully uploaded!<br />";
                    //Insert record in UploadedFilesInformation table
                    intResult = hsInfo.InsertUploadedFilesInformation(intApplicantID, strFirstName + " " + strLastName, strFileName, strUploadURL);
                    blCCSConductUASubmitted = true;
                    //Update status of submission
                    intResult = hsInfo.UpdateAgreementSubmittedHomestayStudentInfo(intApplicantID, 1, "CCSConductUASubmitted");
                }
                else if (hdnCCSConductUASubmitted.Value.ToLower() == "true")
                {
                    //previous submission exists
                    blCCSConductUASubmitted = true;
                }
                //Visibility
                if (!blNoticesWaiverParentUASubmitted) { divUploadNoticesWaiverParentUA.Visible = true; }
                divUploadCCSConductUA.Visible = false;
                divConfirmation.Visible = false;
                btnDone.Visible = true;
                btnContinue2.Visible = false;
                btnContinue3.Visible = false;
                btnPrevious.Visible = false;
                div1.Visible = false;
                div2.Visible = false;
                div3.Visible = true;
                break;
            case "Save":
                //process submission
                strResultMsg += "NoticesWaiverParentUA Checked? " + chkNoticesWaiverParentUA.Checked + "<br />";
                if (chkCCSConductUA.Checked)
                {
                    intResult = hsInfo.UpdateUnderstandAgreeHomestayStudentInfo(intApplicantID, 1, "NoticesWaiverParentUA");
                }
                //Upload the Waive Liability Parent UA agreement
                if (uplNoticesWaiverParentUAFile.HasFile)
                {
                    strFileName = strLastName + "-" + intApplicantID + "-NoticesWaiverParentUA-" + uplNoticesWaiverParentUAFile.FileName;
                    uplNoticesWaiverParentUAFile.SaveAs(strUploadURL + strFileName);
                    strResultMsg += "Your agreement, <span style=\"color:red;\">" + strFileName + "</span>, was successfully uploaded!<br />";
                    //Insert record in UploadedFilesInformation table
                    intResult = hsInfo.InsertUploadedFilesInformation(intApplicantID, strFirstName + " " + strLastName, strFileName, strUploadURL);
                    blNoticesWaiverParentUASubmitted = true;
                    //Update status of submission
                    intResult = hsInfo.UpdateAgreementSubmittedHomestayStudentInfo(intApplicantID, 1, "NoticesWaiverParentUASubmitted");
                }
                else if (hdnNoticesWaiverParentUASubmitted.Value.ToLower() == "true")
                {
                    //previous submission exists
                    blNoticesWaiverParentUASubmitted = true;
                }
                divUploadCCSConductUA.Visible = false;
                divNoticesWaiverParentUA.Visible = false;
                divConfirmation.Visible = true;
                div1.Visible = false;
                div2.Visible = false;
                div3.Visible = false;
                divButtons.Visible = false;
                divInstructions.Visible = false;
                break;
            case "Back":
                Response.Redirect("PlacementProfile_L.aspx");
                break;
            case "Save Notices and Waiver":
                if (uplNoticesWaiverParentUAFile.HasFile)
                {
                    strFileName = strLastName + "-" + intApplicantID + "-NoticesWaiverParentUA-" + uplNoticesWaiverParentUAFile.FileName;
                    uplNoticesWaiverParentUAFile.SaveAs(strUploadURL + strFileName);
                    strResultMsg += "Your agreement, <span style=\"color:red;\">" + strFileName + "</span>, was successfully uploaded!<br />";
                    //Insert record in UploadedFilesInformation table
                    intResult = hsInfo.InsertUploadedFilesInformation(intApplicantID, strFirstName + " " + strLastName, strFileName, strUploadURL);
                    blNoticesWaiverParentUASubmitted = true;
                    //Update status of submission
                    intResult = hsInfo.UpdateAgreementSubmittedHomestayStudentInfo(intApplicantID, 1, "NoticesWaiverParentUASubmitted");
                }
                break;
            case "Save Parent Agreement":
                if (uplWaiveLiabilityParentUAFile.HasFile)
                {
                    strFileName = strLastName + "-" + intApplicantID + "-WaiveLiabilityParentUA-" + uplWaiveLiabilityParentUAFile.FileName;
                    uplCCSConductUAFile.SaveAs(strUploadURL + strFileName);
                    strResultMsg += "Your agreement, <span style=\"color:red;\">" + strFileName + "</span>, was successfully uploaded!<br />";
                    //Insert record in UploadedFilesInformation table
                    intResult = hsInfo.InsertUploadedFilesInformation(intApplicantID, strFirstName + " " + strLastName, strFileName, strUploadURL);
                    blWaiveLiabilityParentUASubmitted = true;
                    //Update status of submission
                    intResult = hsInfo.UpdateAgreementSubmittedHomestayStudentInfo(intApplicantID, 1, "WaiveLiabilityParentUASubmitted");
                }
                break;
            case "Save Student Agreement":
                if (uplCCSConductUAFile.HasFile)
                {
                    strFileName = strLastName + "-" + intApplicantID + "-CCSConductUA-" + uplCCSConductUAFile.FileName;
                    uplCCSConductUAFile.SaveAs(strUploadURL + strFileName);
                    strResultMsg += "Your agreement, <span style=\"color:red;\">" + strFileName + "</span>, was successfully uploaded!<br />";
                    //Insert record in UploadedFilesInformation table
                    intResult = hsInfo.InsertUploadedFilesInformation(intApplicantID, strFirstName + " " + strLastName, strFileName, strUploadURL);
                    blCCSConductUASubmitted = true;
                    //Update status of submission
                    intResult = hsInfo.UpdateAgreementSubmittedHomestayStudentInfo(intApplicantID, 1, "CCSConductUASubmitted");
                }
                break;
            default:
                //Visibility control for initial page load
                if (!blWaiveLiabilityParentUASubmitted) { divUploadWaiveLiabilityParentUA.Visible = true; }
                divConfirmation.Visible = false;
                divButtons.Visible = true;
                div1.Visible = true;
                div2.Visible = false;
                div3.Visible = false;
                btnDone.Visible = false;
                btnContinue2.Visible = true;
                btnContinue3.Visible = false;
                break;
        }
        if (blNoButtons) { divButtons.Visible = false; }
        if (blIsAdmin) { divHomepage.Visible = true; }
        if (intApplicantID != 0)
        {
            //After processing, set checkboxes
            dtHomestayStudent = hsInfo.GetOneHomestayStudent(intApplicantID);
            if (dtHomestayStudent != null)
            {
                if (dtHomestayStudent.Rows.Count > 0)
                {
                    //Set checkboxes
                    chkWaiveLiabilityParentUA.Checked = Convert.ToBoolean(dtHomestayStudent.Rows[0]["agreeWaiveLiabilityParentUA"]); ;
                    chkCCSConductUA.Checked = Convert.ToBoolean(dtHomestayStudent.Rows[0]["agreeCCSConductUA"]); ;
                    chkNoticesWaiverParentUA.Checked = Convert.ToBoolean(dtHomestayStudent.Rows[0]["agreeNoticesWaiverParentUA"]); ;
                }
            }
        }
        litResultMsg.Text = "<p>" + strResultMsg + hsInfo.strResultMsg + "</p>";
    }
    protected void btnUploadWaiveLiabParentUA_Click(object sender, EventArgs e)
    {
        if (uplNoticesWaiverParentUAFile.HasFile)
        {
            Response.Write("in upload");
            strFileName = strLastName + "-" + intApplicantID + "-NoticesWaiverParentUA-" + uplNoticesWaiverParentUAFile.FileName;
            uplNoticesWaiverParentUAFile.SaveAs(strUploadURL + strFileName);
            strResultMsg += "Your agreement, <span style=\"color:red;\">" + strFileName + "</span>, was successfully uploaded!<br />";
            //Insert record in UploadedFilesInformation table
            intResult = hsInfo.InsertUploadedFilesInformation(intApplicantID, strFirstName + " " + strLastName, strFileName, strUploadURL);
            blNoticesWaiverParentUASubmitted = true;
            //Update status of submission
            intResult = hsInfo.UpdateAgreementSubmittedHomestayStudentInfo(intApplicantID, 1, "NoticesWaiverParentUASubmitted");
            Response.Write("result: " + intResult.ToString() + "<br />");
        }
    }// end btn
}