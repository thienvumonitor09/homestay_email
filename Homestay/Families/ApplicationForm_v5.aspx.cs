﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Homestay_Families_ApplicationForm_v5 : System.Web.UI.Page
{
    string strUserName = "";
    bool blIsAdmin = false;
    Homestay hsInfo = new Homestay();
    Quarter_MetaData QMD = new Quarter_MetaData();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["UID"] != null)
        {
            strUserName = Request.QueryString["UID"].ToString();
        }
        else if (Request.ServerVariables["LOGON_USER"] != null)
        {
            //set admin status for visibility control
            blIsAdmin = true;
            //Response.Write("set true<br/>");
        }

        if (!IsPostBack)
        {
            

            DataTable dtFamilies = hsInfo.GetHomestayFamilyInfo(0, "", "DDLactive");
            if (dtFamilies != null)
            {
                if (dtFamilies.Rows.Count > 0)
                {
                    //ddlFamilies.Items.Clear();
                    System.Web.UI.WebControls.ListItem LI;
                    LI = new System.Web.UI.WebControls.ListItem();
                    LI.Value = "0";
                    LI.Text = "-- Add New Family --";
                    ddlFamilies.Items.Add(LI);
                    foreach (DataRow dr in dtFamilies.Rows)
                    {
                        LI = new System.Web.UI.WebControls.ListItem();
                        LI.Value = dr["id"].ToString();
                        LI.Text = dr["homeName"].ToString();
                        ddlFamilies.Items.Add(LI);
                    }
                }
                //else { strResultMsg += "Error: dtFamilies has no rows.<br />"; }
            }
            //else { strResultMsg += "Error: dtFamilies is null.<br />"; }
            if (Request.QueryString["ID"] != null && Request.QueryString["ID"].ToString() != "0")
            {

                ddlFamilies.SelectedValue = Request.QueryString["ID"].ToString();
            }

            //Populate
            string selectedFamily = ddlFamilies.SelectedValue;
            int intFamilyID = Convert.ToInt32(selectedFamily);
            DataTable dtFamily = hsInfo.GetHomestayFamilyInfo(intFamilyID, "", "ID");
            if (dtFamily != null && dtFamily.Rows.Count > 0)
            {
                homeName.Text = dtFamily.Rows[0]["homeName"].ToString().Trim();
                userName.Text = dtFamily.Rows[0]["userName"].ToString().Trim();
                familyStatus.Text = dtFamily.Rows[0]["familyStatus"].ToString().Trim();
                streetAddress.Text = dtFamily.Rows[0]["streetAddress"].ToString().Trim();
                city.Text = dtFamily.Rows[0]["city"].ToString().Trim();
                state.Text = dtFamily.Rows[0]["state"].ToString().Trim();
                ZIP.Text = dtFamily.Rows[0]["ZIP"].ToString().Trim();
                walkingBusStopMinutes.Text = dtFamily.Rows[0]["walkingBusStopMinutes"].ToString().Trim();
                walkingBusStopMiles.Text = dtFamily.Rows[0]["walkingBusStopMiles"].ToString().Trim();
                walkingBusStopBlocks.Text = dtFamily.Rows[0]["walkingBusStopBlocks"].ToString().Trim();
                busMinutesSCC.Text = dtFamily.Rows[0]["busMinutesSCC"].ToString().Trim();
                busMinutesSFCC.Text = dtFamily.Rows[0]["busMinutesSFCC"].ToString().Trim();
                if (dtFamily.Rows[0]["collegeQualified"].ToString().Trim() == "")
                {
                    collegeQualified.SelectedIndex = -1;
                }
                else
                {
                    collegeQualified.SelectedValue = dtFamily.Rows[0]["collegeQualified"].ToString().Trim();
                }
                if (!Convert.IsDBNull(dtFamily.Rows[0]["wspBackgroundCheck"]))
                {
                    wspBackgroundCheck.Text = Convert.ToDateTime(dtFamily.Rows[0]["wspBackgroundCheck"]).ToShortDateString();
                }
                landLinePhone.Text = dtFamily.Rows[0]["landlinePhone"].ToString().Trim();
            }
        }
        
    }

    protected void ddlFamilies_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (blIsAdmin)
        {
            Response.Redirect("ApplicationForm_v5.aspx?ID=" + ddlFamilies.SelectedValue);
        }
    }
}