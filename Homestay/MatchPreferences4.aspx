﻿<%@ Page Title="Matching" Language="C#" MasterPageFile="~/CCSApps.master" AutoEventWireup="true" CodeFile="MatchPreferences4.aspx.cs" Inherits="Homestay_Admin_MatchPreferences4" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="bannerH1Text" Runat="Server">
    Match Preferenes
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">
     <asp:Button ID="btnReset" runat="server" Text="Reset" OnClick="btnReset_Click" />
    
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <h3>Select a Student</h3>
            <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
            
            <asp:DropDownList ID="ddlStudents" runat="server" AppendDataBoundItems="true" 
                AutoPostBack="true">

            </asp:DropDownList>
             
            <br />
            <h3>Level of Matching</h3>
            <div style="width:100%;float:left; margin-bottom:10px">
                <div style="float:left;width:10%;">
                    <label for="rdoGender"><strong>Gender</strong></label>
                    <asp:RadioButtonList ID="rdoGender" RepeatDirection="Horizontal" runat="server">
                        <asp:ListItem Value="Include" Selected="True"></asp:ListItem>
                        <asp:ListItem Value="Omit"></asp:ListItem>
                    </asp:RadioButtonList>
                </div>
                <div style="float:left;width:10%;">
                     <label for="rdoHomestay"><strong>Homestay Preference</strong> </label>
                    <asp:RadioButtonList ID="rdoHomestay" RepeatDirection="Horizontal" runat="server">
                        <asp:ListItem Value="Include" Selected="True"></asp:ListItem>
                        <asp:ListItem Value="Omit"></asp:ListItem>
                    </asp:RadioButtonList>
                </div>
                <div style="float:left;width:80%;">
                     <label for="rdoSmoking"><strong>Smoking Habits</strong></label>
                    <asp:RadioButtonList ID="rdoSmoking" RepeatDirection="Horizontal" runat="server">
                        <asp:ListItem Value="Include" Selected="True"></asp:ListItem>
                        <asp:ListItem Value="Omit"></asp:ListItem>
                    </asp:RadioButtonList>
                </div>
            </div>
           
           <br />
            
            <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" OnClientClick="return ValidateSelectStudent();"/>
        </ContentTemplate>

       

    </asp:UpdatePanel>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" DynamicLayout="true" AssociatedUpdatePanelID="UpdatePanel1" DisplayAfter="500" >

       <ProgressTemplate>
          <img src="../images/demo_wait.gif" />
           Please Wait...
       </ProgressTemplate>
   
    </asp:UpdateProgress>
    <br />
    
    
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
           <h3>Current Status</h3>
            <asp:Literal ID="litCurrStudentStatus" runat="server" Text=""></asp:Literal>
           <h3>Families that Match</h3>
            <asp:Label ID="litRoomWithOtherFamily" runat="server" Text=""></asp:Label>
            <asp:Literal ID="litFoundFamily" runat=server></asp:Literal>   
            <br />
             <div style="margin-bottom:10px;width:40%;float:left;"> 
                  <asp:DropDownList ID="ddlFamilies" runat="server" Width="200px" >
               
                    </asp:DropDownList>
                 <asp:Label ID="litFamilyPlaced" runat="server" Text=""></asp:Label>
                 <asp:Button ID="btnCompare" runat="server" Text="Compare"  OnClientClick="return ValidateSelectFamily();" OnClick="btnCompare_Click"/>
             </div> 
           
             <div id="divRoomPlacement" runat="server" style="margin-bottom:10px;width:100%;float:left;">
                <asp:Table ID="roomStatus" runat="server" Width="90%" BorderWidth="0px">
            <asp:TableHeaderRow>
                <asp:TableCell Width="10%"><b>Room</b></asp:TableCell>
                <asp:TableCell Width="20%"><b>Status</b></asp:TableCell>
               
            </asp:TableHeaderRow>
            <asp:TableRow>
                <asp:TableCell Width="10%">Room 1</asp:TableCell>
                <asp:TableCell ID="rm1Status" Width="20%"><asp:Literal ID="litRm1Status" runat="server"></asp:Literal></asp:TableCell>
                
                
                
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell Width="10%">Room 2</asp:TableCell>
                <asp:TableCell ID="rm2Status" Width="20%"><asp:Literal ID="litRm2Status" runat="server"></asp:Literal></asp:TableCell>

               
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell Width="10%">Room 3</asp:TableCell>
                <asp:TableCell ID="rm3Status" Width="20%"><asp:Literal ID="litRm3Status" runat="server"></asp:Literal></asp:TableCell>

                
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell Width="10%">Room 4</asp:TableCell>
                <asp:TableCell ID="rm4Status" Width="20%"><asp:Literal ID="litRm4Status" runat="server"></asp:Literal></asp:TableCell>

                
            </asp:TableRow>
        </asp:Table>

        
            </div>
            <p>
                
                <b>Notes:</b> <br />
                <i>If the student has been placed, admin must withdraw the student before placing to another room. <br />
                Admin can change "Placement" and "Effective Quarter" of the student. <br/>
                You must withdraw student from the room before moving the student to another room. <br/>
                </i>
            </p>
            <div style="width:30%;float:left;">
                 <b>Placement</b> <br />
                <asp:DropDownList ID="ddlRmPlacement" runat="server">
                        <asp:ListItem Value="">--Select a placement type--</asp:ListItem>
                        <asp:ListItem Value="Tentative Placement"></asp:ListItem>
                        <asp:ListItem Value="Student Placed"></asp:ListItem>
                        <asp:ListItem Value="Withdrew"></asp:ListItem>
                        <asp:ListItem Value="Graduated"></asp:ListItem>
                        <asp:ListItem Value="Student Moved Out"></asp:ListItem>
                  </asp:DropDownList>
            </div>
            <p></p>
            <div style="width:30%;float:left;">
                 <b>Room</b> <br />
                <asp:DropDownList ID="ddlRoom" runat="server">
                   
                  </asp:DropDownList>
            </div>
             <div style="width:40%;float:left;">
                 <b>Effective Quarter</b> <br />
                <asp:DropDownList ID="ddlEffectiveQtr" runat="server">
                        
                  </asp:DropDownList>
            </div>
            <br />
             <div style="width:20%">
                <asp:Button ID="btnUpdate" runat="server" Text="Update" OnClick="btnUpdate_Click"/> <br />
                 <asp:Label ID="lbMsg" runat="server" ForeColor="Red"></asp:Label><br /> <br />
                  <asp:Label ID="litPlace" runat="server" Text=""></asp:Label>
                 
                  
            </div>
            <br />
             
            
             
             <br /><br />
            <div style="width:100%;">
                <div style="width:70%;float:left;margin-right:10px">
                    <h4>Comparison</h4>
                    <asp:GridView ID="GridView1" AutoGenerateColumns="False" 
                       emptydatatext="N/A"  Width="100%" 
                        cellpadding="5"
                        cellspacing="5"
                       runat="server">
                    <HeaderStyle BackColor="#448BC1" Font-Bold="False" ForeColor="White" />
                    <AlternatingRowStyle BackColor="AliceBlue"></AlternatingRowStyle>
                           <Columns>
                               <asp:BoundField DataField="DisplayText" HeaderText="Field"  ItemStyle-Width="20%" ItemStyle-BackColor="#CCCCCC" />
                               <asp:BoundField DataField="StudentValue" HeaderText="Student"  ItemStyle-Width="40%" />
                               <asp:BoundField DataField="FamilyValue" HeaderText="Family" ItemStyle-Width="40%"/>
                               
                        </Columns>
                    </asp:GridView>
                </div>
                <div style="width:20%;float:left">
                    <h4>Matching score</h4>
                    <asp:GridView ID="GridView2" AutoGenerateColumns="False" 
                               emptydatatext="N/A"  Width="95%" 
                                cellpadding="5"
                                cellspacing="5"
                               runat="server">
                            <HeaderStyle BackColor="#448BC1" Font-Bold="False" ForeColor="White" />
                            <AlternatingRowStyle BackColor="AliceBlue"></AlternatingRowStyle>
                                   <Columns>
                                       <asp:BoundField DataField="Field" HeaderText="Field"  ItemStyle-Width="80%" ItemStyle-BackColor="#CCCCCC" />
                                       <asp:BoundField DataField="ScoreMatch" HeaderText="Percent"   />
                               
                               
                                </Columns>
                  </asp:GridView>
                </div>
            </div>
          
            
            
            
            <table style="width: 80%;">
                <tr>
                    <td style=" vertical-align:top">
                        <asp:Literal ID="litSelectedStuInfo" runat=server></asp:Literal>  
                    </td>
                    <td style="vertical-align:top">
                        <asp:Literal ID="litFamilyInfo" runat=server></asp:Literal>  
                    </td>
                </tr>
                
                
            </table>
            <asp:Literal ID="litResultMsg" runat=server></asp:Literal>   
        </ContentTemplate>

    </asp:UpdatePanel>

    

    
    <script type="text/javascript">
        function ValidateSelectStudent() {
            var ddl = $('#<%=ddlStudents.ClientID%>');
            var selectedValue = ddl.val();
  
            //alert(selectedValue); //SelVal is the selected Value
            if (selectedValue == 0) {
                 alert("Please select a student to search");
                return false;
            }
            return true;
        }

         function ValidateSelectFamily() {
            var ddl = $('#<%=ddlFamilies.ClientID%>');
            var selectedValue = ddl.val();
  
            //alert(selectedValue); //SelVal is the selected Value
            if (selectedValue == 0) {
                 alert("Please select a family to compare");
                return false;
            }
            return true;
        }

        
        $(function () {
            
        });
        
        
    </script>
    </asp:Content>