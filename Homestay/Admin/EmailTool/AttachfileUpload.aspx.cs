﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Homestay_Admin_AttachfileUpload : System.Web.UI.Page
{
    private static String  strUploadURL = @"\\ccs-internet\InternetContent\CCS\Homestay\EmailAttachments\";
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void UploadButton_Click(object sender, EventArgs e)
    {
        if (FileUploadControl.HasFile)
        {
            try
            {
                string filename = Path.GetFileName(FileUploadControl.FileName);
                //Response.Write(Server.MapPath("./UploadFiles"));
                //FileUploadControl.SaveAs(Server.MapPath("./UploadFiles/") + filename);
                FileUploadControl.SaveAs(strUploadURL + filename);
                
                //StatusLabel.Text = "Upload status: File uploaded!";
            }
            catch (Exception ex)
            {
                //StatusLabel.Text = "Upload status: The file could not be uploaded. The following error occured: " + ex.Message;
            }
        }
    }

    [WebMethod()]
    public static string DeleteFile(string fileName)
    {
        //var filePath = HttpContext.Current.Server.MapPath("./UploadFiles/")+fileName;
        var filePath = strUploadURL + fileName;
        //var filePath = HttpContext.Current.Server.MapPath("./UploadFiles/" + postedFile.FileName);
        File.Delete(filePath);
        return filePath;
    }
}