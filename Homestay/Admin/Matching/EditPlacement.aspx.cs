﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Homestay_Admin_Matching_EditPlacement : System.Web.UI.Page
{
    Homestay hsInfo = new Homestay();
    HomestayUtility hsUtility = new HomestayUtility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        { //do something 
            ShowData();
        }
        
    }
    
    protected void ShowData()
    {
        gvStuFam.DataSource = GetPlacements();
        gvStuFam.DataBind();
    }
    protected void gvStuFam_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvStuFam.EditIndex = e.NewEditIndex;
        ShowData();
    }
    protected void gvStuFam_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        //Finding the controls from Gridview for the row which is going to update  
        Label id = (Label)gvStuFam.Rows[e.RowIndex].FindControl("lbl_ID");
        string idStr = id.Text;
        DropDownList ddlPlacementType = (DropDownList)gvStuFam.Rows[e.RowIndex].FindControl("ddlPlacementType");
        string selectedPlacement = ddlPlacementType.SelectedValue;
        if (String.IsNullOrEmpty(selectedPlacement) || selectedPlacement.Contains("Place"))
        {
            //Update
            string sqlUpdate = @"UPDATE [CCSInternationalStudent].[dbo].[HomestayPlacement]
                                SET placementType = '" + ddlPlacementType.SelectedValue + "'"
                                + " WHERE id = " + idStr;
            hsInfo.runSPQuery(sqlUpdate, false);
            litSuccessMsg.Text = "Student has been updated!";
        }
        else
        {
            string sqlDelete = @"DELETE FROM [CCSInternationalStudent].[dbo].[HomestayPlacement]"
                               + " WHERE id = " + idStr;
            hsInfo.runSPQuery(sqlDelete, false);
            //lblMsg.Text = "Record has been removed!";
            litSuccessMsg.Text = "Record has been successfully removed!";
        }
        gvStuFam.EditIndex = -1;
        ShowData();
    }
    protected void gvStuFam_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvStuFam.EditIndex = -1;
        ShowData();
    }
    protected void gvStuFam_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        gvStuFam.Columns[1].Visible = false; //hide id column
        gvStuFam.Columns[3].Visible = false; //hide student id column
        gvStuFam.Columns[5].Visible = false; //hide family id column

        //Used to group by student
        /*
        for (int i = gvStuFam.Rows.Count - 1; i > 0; i--)
        {
            GridViewRow row = gvStuFam.Rows[i];
            GridViewRow previousRow = gvStuFam.Rows[i - 1];
            for (int j = 0; j < row.Cells.Count; j++)
            {
                if (row.Cells[j].Text == previousRow.Cells[j].Text)
                {
                    if (previousRow.Cells[j].RowSpan == 0)
                    {
                        if (row.Cells[j].RowSpan == 0)
                        {
                            previousRow.Cells[j].RowSpan += 2;
                        }
                        else
                        {
                            previousRow.Cells[j].RowSpan = row.Cells[j].RowSpan + 1;
                        }
                        row.Cells[j].Visible = false;
                    }
                }
            }
        }
        */
        
    }

    private DataTable GetPlacements()
    {
        string sqlQuery = @"SELECT a.id, placementName, applicantID, familyID, roomNumber,placementType,effectiveQuarter, endQuarter, (familyName + ', ' + firstName) AS StudentName
                                FROM [CCSInternationalStudent].[dbo].[HomestayPlacement] a
                                LEFT JOIN [CCSInternationalStudent].[dbo].[ApplicantBasicInfo] b
                                ON a.applicantID = b.id
                                ORDER BY applicantID ASC";
        int studentID = 0;
        if ((Request.QueryString["studentID"] != null))
        {
            studentID = hsUtility.ParseInt(Request.QueryString["studentID"]);
            sqlQuery = @"SELECT a.id, placementName, applicantID, familyID, roomNumber,placementType,effectiveQuarter, endQuarter, (familyName + ', ' + firstName) AS StudentName
                                FROM [CCSInternationalStudent].[dbo].[HomestayPlacement] a
                                LEFT JOIN [CCSInternationalStudent].[dbo].[ApplicantBasicInfo] b
                                ON a.applicantID = b.id
                                where applicantID=" + studentID
                                + " ORDER BY applicantID ASC";
        }
        int familyID = 0;
        if ((Request.QueryString["familyID"] != null))
        {
            familyID = hsUtility.ParseInt(Request.QueryString["familyID"]);
            sqlQuery = @"SELECT a.id, placementName, applicantID, familyID, roomNumber,placementType,effectiveQuarter, endQuarter, (familyName + ', ' + firstName) AS StudentName
                                FROM [CCSInternationalStudent].[dbo].[HomestayPlacement] a
                                LEFT JOIN [CCSInternationalStudent].[dbo].[ApplicantBasicInfo] b
                                ON a.applicantID = b.id
                                where applicantID=" + studentID + " AND familyID=" + familyID
                                + " ORDER BY applicantID ASC";
        }
        


        DataTable dtPlacements = hsInfo.RunGetQuery(sqlQuery);
        return dtPlacements;
    }

   

    protected void btnReset_Click(object sender, EventArgs e)
    {
        Response.Redirect("editplacement.aspx");
    }

    protected void btnMainPage_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Homestay/Admin/Default.aspx");
    }

    protected void btnPlacementPage_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Homestay/Admin/Matching/MatchPreferences.aspx");
    }

    

    protected void GotoFamily_Click(object sender, EventArgs e)
    {
        GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;
        string rowNumber = (grdrow.FindControl("lbl_id") as Label).Text;
        //Response.Redirect("123.com?"+ rowNumber);
    }

    protected void GotoStudent_Click(object sender, EventArgs e)
    {
        GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;
        string studentID = (grdrow.FindControl("lblApplicantID") as Label).Text;
        
        //Response.Redirect("/Homestay/Admin/Reports/PlacementForm_V.aspx?ID=" + studentID);
    }

    protected void SendToFamily_Click(object sender, EventArgs e)
    {
        //public string SendEmailMsg(string strTo, string strFrom, string strSubject, bool blIsHTML, string strBody, string strCCEmailAddresses)
        Utility utility = new Utility();
        string body = GetGridviewData(gvStuFam);

        //utility.SendEmailMsg("vu.nguyen@ccs.spokane.edu", "vu.nguyen@ccs.spokane.edu", "subject", true, body, "");
    }
    // This Method is used to render gridview control
    public string GetGridviewData(GridView gv)
    {
        StringBuilder strBuilder = new StringBuilder();
        StringWriter strWriter = new StringWriter(strBuilder);
        HtmlTextWriter htw = new HtmlTextWriter(strWriter);
        gv.RenderControl(htw);
        return strBuilder.ToString();
    }

    //This function to prevent exception when render page
    public override void VerifyRenderingInServerForm(Control control)
    {
        return;
    }
    protected void btnShowAll_Click(object sender, EventArgs e)
    {
        Response.Redirect("./editplacement.aspx");
    }
    

}