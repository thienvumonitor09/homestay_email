﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Placement
/// </summary>
public class Placement
{
    public string ApplicantID { get; set; }
    public string FamilyID { get; set; }
    public string RoomNumber { get; set; }
    public string PlacementType { get; set; }
    public string EffectiveQuarter { get; set; }
    public string EndQuarter { get; set; }
    public Placement()
    {
        
        //
        // TODO: Add constructor logic here
        //
    }
}