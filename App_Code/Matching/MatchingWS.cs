﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;

/// <summary>
/// Summary description for MatchingWS
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class MatchingWS : System.Web.Services.WebService
{
    Homestay hsInfo = new Homestay();

    public MatchingWS()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public string HelloWorld()
    {
        return "Hello World";
    }

    [WebMethod]
    public string[] GetDemo()
    {
        JavaScriptSerializer js = new JavaScriptSerializer();
        js.MaxJsonLength = Int32.MaxValue;
        //return js.Serialize(new string[] { "1", "2" });
        return new string[] { "1","2"};
    }

    [WebMethod]
    public List<Placement> GetPlacement(string applicantID)
    {
        List<Placement> result = new List<Placement>();
        string sqlQuery = @"SELECT *
                              FROM [CCSInternationalStudent].[dbo].[HomestayPlacement]";
        DataTable dtPlacements = hsInfo.RunGetQuery(sqlQuery);
        foreach (DataRow rowPlacement in dtPlacements.Rows)
        {
            Placement placement = new Placement()
            {

                ApplicantID = rowPlacement["familyID"].ToString(),

                FamilyID = rowPlacement["familyID"].ToString(),
                RoomNumber = rowPlacement["roomNumber"].ToString(),
                PlacementType = rowPlacement["placementType"].ToString(),
                EffectiveQuarter = rowPlacement["effectiveQuarter"].ToString(),
                EndQuarter = rowPlacement["endQuarter"].ToString()
            };
            result.Add(placement);
        }
        return result;
    }

}
