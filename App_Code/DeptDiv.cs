﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for DeptDiv
/// </summary>
public class DeptDiv
{
	public DeptDiv()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public DataTable GetDivisions(String strInstitution)
    {
        DataSet ds = new DataSet();
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCommand = new SqlCommand();
        SqlDataAdapter objDataAdapter;
        String strCommandText = "SELECT DISTINCT Division FROM vw_StaffDirectory ";
        switch(strInstitution)
        {
            case "District":
                strCommandText += "WHERE Domain = 'DISTRICT' ORDER BY Division";
                break;
            case "SCC":
                strCommandText += "WHERE Domain = 'SCC' OR (Domain = 'IEL' AND Department <> 'Pullman Center') ORDER BY Division";
                break;
            case "SFCC":
                strCommandText += "WHERE Domain = 'SFCC' ORDER BY Division";
                break;
            default:
                strCommandText += "ORDER BY Division";
                break;
        }
        try
        {
            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSGen"].ToString();
            objConn.Open();
            objCommand.CommandText = strCommandText;
            objCommand.CommandType = CommandType.Text;
            objCommand.Connection = objConn;
            objDataAdapter = new SqlDataAdapter(objCommand);
            objDataAdapter.Fill(ds);
        }
        finally
        {
            objConn.Close();
            objConn.Dispose();
            objCommand.Dispose();
        }
        return ds.Tables[0];
    }

    public DataTable GetDepartments(String strDivision)
    {
        DataSet ds = new DataSet();
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCommand = new SqlCommand();
        SqlDataAdapter objDataAdapter;
        String strCommandText = "SELECT DISTINCT department FROM vw_StaffDirectory WHERE Division = '" + strDivision + "'";
        try
        {
            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSGen"].ToString();
            objConn.Open();
            objCommand.CommandText = strCommandText;
            objCommand.CommandType = CommandType.Text;
            objCommand.Connection = objConn;
            objDataAdapter = new SqlDataAdapter(objCommand);
            objDataAdapter.Fill(ds);
        }
        finally
        {
            objConn.Close();
            objConn.Dispose();
            objCommand.Dispose();
        }
        return ds.Tables[0];
    }
}