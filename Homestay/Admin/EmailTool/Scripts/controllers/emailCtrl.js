﻿app.controller('emailCtrl', function ($scope, $http, $location, fileUploadService, $window) {
    /*
    $scope.editorOptions = {
        language: 'en',
        allowedContent: true,
        entities: false,
        uiColor: '#D3D3D3'
    };
    */
    $scope.backAdmin = function () {
        $window.location.href = "http://10.171.1.202:5175/Homestay/Admin/Default.aspx"; //You should have http here.
    };
    $('[data-toggle="tooltip"]').tooltip();
    $scope.uploadFile = function () {

        var file = $scope.myFile;
        console.log('file is ');
        console.dir(file);
        var uploadUrl = "/EmailToolWS.asmx/UploadFile"; //Url of EmailToolWS/api/server

        var fileFormData = new FormData();
        fileFormData.append('file', file);
        $http({
            method: "POST",
            url: "EmailToolWS.asmx/UploadFile",
            //dataType: 'json',
            data: fileFormData,
            headers: { "Content-Type": undefined },
            transformRequest: angular.identity
        }).then(function (response) {
            //alert(JSON.stringify(response.data));
            //alert(response.data);
        });
    };

    $scope.content_two = "hello";
    $scope.emailObj = {};

    //$scope.emailObj.FromEmail = "vu.nguyen@ccs.spokane.edu";   
    //$scope.emailObj.ToEmail = "vu.nguyen@ccs.spokane.edu";  
    $scope.emailObj.ToEmail = "";

    $scope.emailObj.Subject = "";
    $scope.emailObj.Body = "content";

    //Load groups for email lists from json file
    $http.get("./Scripts/groups.json").then(function (response) {
        $scope.groups = response.data;
    });

    $scope.updateEmails = function () {
        
        //var emailStr = angular.copy($scope.emails); //deep copy to prevent $scope.emails be changed
        //emailStr = emailStr.replace(/(^,)|(,$)/g, "");
        //$scope.emails = regex.Replace($scope.emails, "");
        //$scope.emails = emailStr.split(",");
        //console.log($scope.emails);
    };


    $scope.ccEmails = [];
    $scope.updateCCEmails = function () {
        //$scope.ccEmails = $scope.ccEmails.split(",");
    };

    $scope.emailTemplates = {};


    $scope.getEmailsByGroup = function () {
        $scope.searchName = "";
        if ($scope.group == "Individual Students" || $scope.group == "Individual Families") {

            $scope.selectedEmail = [];
           
            $('#individualEmail').modal('show');
            $http({
                method: "POST",
                url: "EmailToolWS.asmx/GetEmailGroup2",
                dataType: 'json',
                data: { groupName: $scope.group },
                headers: { "Content-Type": "application/json" }
            }).then(function (response) {
                var res = JSON.parse(response.data.d);
                //console.log(res);
                angular.forEach(res, function (value, key) {
                    $scope.selectedEmail.push({ name: value.name, email: value.email, selected: false });
                });
                //console.log($scope.selectedEmail);
                //$scope.selectedEmail = JSON.parse(response.data.d);
            }, function (error) {
                alert("Error.");
            });


            if ($scope.group == "Individual Students") {
                $("#modalTitle").text("Students' Email");
            } else {
                $("#modalTitle").text("Families' Email");
            }

        } else {
            $http({
                method: "POST",
                url: "EmailToolWS.asmx/GetEmailGroup",
                dataType: 'json',
                data: { groupName: $scope.group },
                headers: { "Content-Type": "application/json" }
            }).then(function (response) {
                //alert(response.data);
                console.log(response.data);
                //$scope.emailTemplates = JSON.parse(response.data.d);
                //console.log($scope.emailTemplates);
                $scope.emails = JSON.parse(response.data.d);

            }, function (error) {
                alert("error");
            });
            //$scope.emails = $scope.group;
        }
    };
    $scope.saveSelectedEmails = function () {
        $scope.emails = [];
        angular.forEach($scope.selectedEmail, function (value, key) {
            if (value.selected) {
                console.log(value.name);
                $scope.emails.push(value.email);
            }

            //$scope.attached.push({ name: value, selected: false });
        });
        //$('#attachFileModalID').modal('hide');
    }

    $scope.addEmail = function () {
        $scope.emailObj.ToEmail.split(";").forEach(function (element) {
            if (element != "" && $scope.emails.indexOf(element) == -1) {
                $scope.emails.push(element);
            }
            console.log($scope.emails);
        });

    }

    $scope.emailSignature = '<p><em>Community Colleges of Spokane</em>'
        + '<em> Global Education Homestay Program </em><br/>'
        + "<em><a href='mailto: internationalhomestay@ccs.spokane.edu'>internationalhomestay@ccs.spokane.edu</a></em><br /"
        + "<em>509-533-4113</em></p>";

    $scope.getEmailTemplates = function () {
        $http({
            method: "POST",
            url: "EmailToolWS.asmx/GetTemplates",
            dataType: 'json',
            data: {},
            headers: { "Content-Type": "application/json" }
        }).then(function (response) {
            //alert(response.data);
            //console.log(response.data);
            //response.data.d.push({ "templateID": "0", "templateName": "--Select--","templateContent":"" });
            $scope.emailTemplates = JSON.parse(response.data.d);
            //$scope.emailTemplates.unshift({ "templateID": "0", "templateName": "--Select--", "templateContent": "" });
            console.log($scope.emailTemplates);
        }, function (error) {
            alert("error");
        });
    };

    $scope.getEmailTemplates();
    $scope.template2 = "template2";
    //console.log($scope.emailObj);

    $scope.addSignature = function () {
        $scope.selectedTemplate.templateContent += ($scope.senderName + $scope.emailSignature);
    }
    //$scope.attachments = ["Homestay-Change-Form.pdf", "Moving-Out-Form.pdf"];
    $scope.attachments = [];
    $scope.attachFileModal = function () {
        $scope.getAllFiles();
        $('#attachFileModalID').modal('show');
        //CrudFactory.attachModal();
    };

    $scope.getAllFiles = function () {
        //alert("retrieving files");
        $http({
            method: "POST",
            url: "EmailToolWS.asmx/GetAttachments",
            dataType: 'json',
            data: {},
            headers: { "Content-Type": "application/json" }
        }).then(function (response) {
            //alert(JSON.stringify(response.data.d));
            var attachmentsObj = JSON.parse(response.data.d);
            //$scope.attachments = JSON.parse(response.data.d);
            $scope.attached = [];
            angular.forEach(attachmentsObj, function (value, key) {
                //console.log(value);
                $scope.attached.push({ name: value, selected: false });
            });
        }, function (error) {
            alert("error");
        });
    };
    $scope.saveAttachment = function () {
        //console.log("ok");
        $scope.attachments = [];
        angular.forEach($scope.attached, function (value, key) {
            if (value.selected) {
                console.log(value.name);
                $scope.attachments.push(value.name);
            }

            //$scope.attached.push({ name: value, selected: false });
        });
        $('#attachFileModalID').modal('hide');
    }
    $scope.attachFile = function () {
        $scope.attachments.push("Homestay-Change-Form.pdf");
    };
    $scope.saveEmail = function () {
        //alert("saving");
        $http({
            method: "POST",
            url: "EmailToolWS.asmx/SaveReports",
            dataType: 'json',
            data: {
                emailObj: JSON.stringify($scope.emailObj),
                selectedTemplate: $scope.selectedTemplate.templateContent,
                emails: JSON.stringify($scope.emails),
                ccEmails: JSON.stringify($scope.ccEmails),
                attachmentNames: JSON.stringify($scope.attachments)
            },
            headers: { "Content-Type": "application/json" }
        }).then(function (response) {
            //alert("saved");
            //$scope.selectedEmail = JSON.parse(response.data.d);
        }, function (error) {
            alert("error");
        });
    };
    $scope.send = function () {
        //alert($scope.selectedTemplate.templateContent);
        //var file = $scope.myFile;
        //var fileFormData = new FormData();
        //fileFormData.append('file', file);
        //console.dir(file);
        alert("sending");
        $scope.processEmail();
        $http({
            method: "POST",
            url: "EmailToolWS.asmx/SendEmailService",
            dataType: 'json',
            data: {
                emailObj: JSON.stringify($scope.emailObj),
                selectedTemplate: $scope.selectedTemplate.templateContent,
                emails: JSON.stringify($scope.emails),
                ccEmails: JSON.stringify($scope.ccEmails),
                attachmentNames: JSON.stringify($scope.attachments)
            },
            //transformRequest: angular.identity,
            headers: { "Content-Type": "application/json" }
        }).then(function (response) {
            //alert(JSON.stringify(response.data));
            //alert(response.data.d);
            //console.log("success");
            $scope.saveEmail();
            //alert(response.data.d);
            alert("Your email has been sent successfully!");
            $location.path('/confirmation');

        }, function (error) {
            alert("An error has occured while sending email.");
        });
    };
    $scope.processEmail = function ()
    {   
        var regex = /(^,)|(,$)/g; //remove any trailing commas 
        var emailStr = $scope.emails; //deep copy to prevent $scope.emails be changed
        $scope.emails = $scope.emails.replace(regex, "").split(",");
        //$scope.emails = regex.Replace($scope.emails, "");
        //$scope.emails = $scope.emails.split(",");

        //Process CC emails
        $scope.ccEmails = $scope.ccEmails.replace(regex, "").split(",");
        //console.log($scope.emails);
    };
    $scope.getLogonUser = function () {
        $http({
            method: "POST",
            url: "Communication.aspx/GetLogonUser",
            dataType: 'json',
            data: {},
            headers: { "Content-Type": "application/json" }
        }).then(function (response) {
            var strLogonUser = (response.data.d);
            console.log(strLogonUser);
            //alert(response.data);
            var n = strLogonUser.lastIndexOf("\\"); //find index of \ because logon user has format ccs\vu.nguyen
            console.log(strLogonUser.substr(n + 1) + "@ccs.spokane.edu");

            //Get log in email from username
            var userName = strLogonUser.substr(n + 1);
            $scope.emailObj.FromEmail = userName + "@ccs.spokane.edu";

            //Get Sender full name
            var senderName = "";
            userName.split(".").forEach(function (element) { senderName += element.charAt(0).toUpperCase() + element.substr(1) + " "; });
            $scope.senderName = "<p><strong>" + senderName + "</strong></p>";
        }, function (error) {
            alert("error");
        });
    };
    $scope.getLogonUser();
    /*
    $scope.getTemplates = function () {
        $http({
            method: "GET",
            url: "./Scripts/template.json",
            dataType: 'json',
            headers: { "Content-Type": "application/json" }
        }).then(function (response) {
            console.log(response.data)
        }, function (error) {
            alert("error");
        });
    };
    */

    //$scope.emailObj.Body = "hello";

});