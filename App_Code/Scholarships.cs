﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Scholarships
/// </summary>
public class Scholarships
{
	public Scholarships()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public string GetScholarships(string strSQL, bool blShowDetail, bool blIsAdmin)
    {
        string strWebAddress = "";
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCommand = new SqlCommand();
        StringBuilder objBuilder = new StringBuilder();
        SqlDataReader objReader;
        try
        {
            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSScholarships"].ToString();
            objConn.Open();
            objCommand.CommandText = strSQL;
            objCommand.CommandType = CommandType.Text;
            objCommand.Connection = objConn;
            objReader = objCommand.ExecuteReader();
            bool blHasRows = objReader.HasRows;
            if (objReader.HasRows)
            {
                if (!blShowDetail) 
                {
                    objBuilder.Append("	<table class=\"ccs-table GreyBorder\" cellpadding=\"2\" cellspacing=\"0\" width=\"100%\">" + System.Environment.NewLine);
                    if (blIsAdmin)
                    {
                        objBuilder.Append("	<tr><th class=\"GreyBorder GreyBackground\">College</th><th class=\"GreyBorder GreyBackground\">Title</th><th class=\"GreyBorder GreyBackground\">Agency</th>" + System.Environment.NewLine);
                    }
                }
                while (objReader.Read())
                {
                    string strScholarshipTitle = "";
                    if (!objReader.IsDBNull(objReader.GetOrdinal("ScholarshipTitle")))
                    {
                        strScholarshipTitle = objReader["ScholarshipTitle"].ToString();
                        //objBuilder.Append("<h2>" + objReader["ScholarshipTitle"].ToString() + "</h2>") ;
                    }
                    if (blShowDetail)
                    {
                        objBuilder.Append("	<table class=\"ccs-table GreyBorder\" cellpadding=\"2\" cellspacing=\"0\" width=\"100%\">" + System.Environment.NewLine);
                        objBuilder.Append(" <tr class=\"TblBGColor\"><td colspan=\"2\"><b>" + strScholarshipTitle + "&nbsp;</b></td></tr>" + System.Environment.NewLine);
                    } else if(!blIsAdmin) {
                        objBuilder.Append(" <tr class=\"TblBGColor\"><td><input type=\"checkbox\" name=\"chkRecord\" value=\"" + objReader["ScholarshipNumber"] + "\"> Add to Cart</td><td><a target=\"_blank\" href=\"Detail.aspx?ID=" + objReader["ScholarshipNumber"] + "\">" + strScholarshipTitle + "</a></td></tr>" + System.Environment.NewLine);
                    } else if(blIsAdmin) {
                        objBuilder.Append(" <tr><td class=\"GreyBorder\">" + objReader["College"] + "</td><td class=\"GreyBorder\"><a target=\"_blank\" href=\"Edit.aspx?ID=" + objReader["ScholarshipNumber"] + "\">" + strScholarshipTitle + "</a></td><td class=\"GreyBorder\">" + objReader["ContactAgency"] + "</td></tr>" + System.Environment.NewLine);
                    }
                    if (!blIsAdmin)
                    {
                        if (!objReader.IsDBNull(objReader.GetOrdinal("ScholarshipPurpose"))) { if (objReader["ScholarshipPurpose"].ToString().Length > 0) { objBuilder.Append("	<tr><td class=\"GreyBorder\" width=\"20%\"><b>Purpose:</b></td><td class=\"GreyBorder\">" + objReader["ScholarshipPurpose"] + "&nbsp;</td></tr>" + System.Environment.NewLine); } }
                        else { objBuilder.Append("	<tr><td class=\"GreyBorder\" width=\"20%\"><b>Eligibility:</b></td><td class=\"GreyBorder\">" + objReader["Eligibility"] + "&nbsp;</td></tr>" + System.Environment.NewLine); }
                        if (blShowDetail && !objReader.IsDBNull(objReader.GetOrdinal("ScholarshipPurpose"))) { objBuilder.Append("	<tr><td width=\"20%\"><b>Eligibility:</b></td><td class=\"GreyBorder\">" + objReader["Eligibility"] + "&nbsp;</td></tr>" + System.Environment.NewLine); }
                        if (!objReader.IsDBNull(objReader.GetOrdinal("ApplicationDeadline"))) { objBuilder.Append("	<tr><td class=\"GreyBorder\" width=\"20%\"><b>Deadline:</b></td><td class=\"GreyBorder\">" + objReader["ApplicationDeadline"] + "&nbsp;</td></tr>" + System.Environment.NewLine); }
                        if (!objReader.IsDBNull(objReader.GetOrdinal("WebAddress")))
                        {
                            strWebAddress = objReader["WebAddress"].ToString();
                            if (strWebAddress.Length > 4)
                            {
                                if (strWebAddress.Substring(0, 4) != "http") { strWebAddress = "http://" + strWebAddress; }
                            }
                            string strAgency = "Click to Apply";
                            if (!objReader.IsDBNull(objReader.GetOrdinal("ContactAgency"))) { strAgency = objReader["ContactAgency"].ToString(); }
                            objBuilder.Append("	<tr><td class=\"GreyBorder\" width=\"20%\"><b>Website:</b></td><td class=\"GreyBorder\"><a target=\"_blank\" href=" + strWebAddress + ">" + strAgency + "</a>&nbsp;</td></tr>" + System.Environment.NewLine);
                        }
                    }
                    if (blShowDetail)
                    {
                        if (!objReader.IsDBNull(objReader.GetOrdinal("Amount"))) {  if (objReader["Amount"].ToString().Length > 0) { objBuilder.Append("	<tr><td class=\"GreyBorder\" width=\"20%\"><b>Amount:</b></td><td class=\"GreyBorder\">" + objReader["Amount"] + "&nbsp;</td></tr>" + System.Environment.NewLine); } }
                        if (!objReader.IsDBNull(objReader.GetOrdinal("Notes"))) {  if (objReader["Notes"].ToString().Length > 0) { objBuilder.Append("	<tr><td class=\"GreyBorder\" width=\"20%\"><b>Additional Information:</b></td><td class=\"GreyBorder\">" + objReader["Notes"] + "&nbsp;</td></tr>" + System.Environment.NewLine); } }
                        if (!objReader.IsDBNull(objReader.GetOrdinal("Limitations"))) {  if (objReader["Limitations"].ToString().Length > 0) { objBuilder.Append("	<tr><td class=\"GreyBorder\" width=\"20%\"><b>Limitations:</b></td><td class=\"GreyBorder\">" + objReader["Limitations"] + "&nbsp;</td></tr>" + System.Environment.NewLine); } }
                        if (!objReader.IsDBNull(objReader.GetOrdinal("Duration"))) { if (objReader["Duration"].ToString().Length > 0) { objBuilder.Append("	<tr><td class=\"GreyBorder\" width=\"20%\"><b>Duration:</b></td><td class=\"GreyBorder\">" + objReader["Duration"] + "&nbsp;</td></tr>" + System.Environment.NewLine); } }
                        if (!objReader.IsDBNull(objReader.GetOrdinal("NumberAwarded"))) {  if (objReader["NumberAwarded"].ToString().Length > 0) { objBuilder.Append("	<tr><td class=\"GreyBorder\" width=\"20%\"><b>Number Awarded:</b></td><td class=\"GreyBorder\">" + objReader["NumberAwarded"] + "&nbsp;</td></tr>" + System.Environment.NewLine); } }
                        //OTHER DETAILS SECTION
                        objBuilder.Append("	<tr><td class=\"GreyBorder\" valign=\"top\"><b>Other Details</b></td><td class=\"GreyBorder\">");
                        objBuilder.Append("Scholarship No. <b>" + objReader["ScholarshipNumber"] + "</b><br />");
                        if (Convert.ToBoolean(objReader["DisabilityBased"])) { objBuilder.Append("Disability Based<br />"); }
                        if (!objReader.IsDBNull(objReader.GetOrdinal("DisabilityType"))) { if (objReader["Gender"].ToString().Length > 0) { objBuilder.Append("Disability Type: " + objReader["DisabilityType"].ToString() + "<br />"); } }
                        if (Convert.ToBoolean(objReader["FinancialNeed"])) { objBuilder.Append("Financial Need Based<br />"); }
                        if (Convert.ToBoolean(objReader["EthnicityBased"])) { objBuilder.Append("Ethnicity Based<br />"); }
                        if (Convert.ToBoolean(objReader["AfricanAmerican"])) { objBuilder.Append("Offered to African Americans<br />"); }
                        if (Convert.ToBoolean(objReader["AsianAmerican"])) { objBuilder.Append("Offered to Asian Americans<br />"); }
                        if (Convert.ToBoolean(objReader["Hispanic"])) { objBuilder.Append("Offered to Hispanics<br />"); }
                        if (Convert.ToBoolean(objReader["NativeAmerican"])) { objBuilder.Append("Offered to Native Americans<br />"); }
                        if (Convert.ToBoolean(objReader["Military"])) { objBuilder.Append("Military Only<br />"); }
                        if (!objReader.IsDBNull(objReader.GetOrdinal("Gender"))) { if (objReader["Gender"].ToString().Length > 0) { objBuilder.Append(objReader["Gender"] + " Only<br />"); } }
                        if (Convert.ToBoolean(objReader["SpecificSchool"])) { objBuilder.Append("Specific School"); }
                        if (!objReader.IsDBNull(objReader.GetOrdinal("SchoolName"))) { if (objReader["SchoolName"].ToString().Length > 0) { objBuilder.Append(": " + objReader["SchoolName"] + "<br />"); } }
                        if (!objReader.IsDBNull(objReader.GetOrdinal("CollegeMajor"))) { if (objReader["CollegeMajor"].ToString().Length > 0) { objBuilder.Append("Area of Specialization: " + objReader["CollegeMajor"] + "<br />"); } }
                        if (Convert.ToBoolean(objReader["CommunityServiceRequired"])) { objBuilder.Append("Community Service Requirement<br />"); }
                        if (Convert.ToBoolean(objReader["EssayRequired"])) { objBuilder.Append("Essay Required<br />"); }
                        if (Convert.ToBoolean(objReader["GPARequired"])) { objBuilder.Append("Required GPA<br />"); }
                        if (!objReader.IsDBNull(objReader.GetOrdinal("GPACutoff"))) { if (objReader["GPACutoff"].ToString().Length > 0) { objBuilder.Append("Cutoff GPA: " + objReader["GPACutoff"] + "<br />"); } }
                        if (Convert.ToBoolean(objReader["LetterOfRecommendation"])) { objBuilder.Append("Letter of Recommendation Required<br />"); }
                        if (Convert.ToBoolean(objReader["ReligiousAffiliation"])) { objBuilder.Append("Religious Affiliation<br />"); }
                        if (Convert.ToBoolean(objReader["TranscriptRequired"])) { objBuilder.Append("Transcript Required<br />"); }
                        objBuilder.Append("</td></tr>");
                        //CONTACT INFORMATION SECTION
                        if (!objReader.IsDBNull(objReader.GetOrdinal("ContactAgency")) || !objReader.IsDBNull(objReader.GetOrdinal("ContactPhone")) || !objReader.IsDBNull(objReader.GetOrdinal("ContactEmailAddress")) || !objReader.IsDBNull(objReader.GetOrdinal("ContactAddress1")))
                        {
                            objBuilder.Append("	<tr><td class=\"GreyBorder GreyBGColor\" colspan=\"2\"><b>Contact Information</b></td></tr>" + System.Environment.NewLine);
                        }
                        if (!objReader.IsDBNull(objReader.GetOrdinal("ContactAgency"))) { if (objReader["ContactAgency"].ToString().Length > 0) { objBuilder.Append("	<tr><td class=\"GreyBorder\" width=\"20%\"><b>Agency:</b></td><td class=\"GreyBorder\">" + objReader["ContactAgency"] + "&nbsp;</td></tr>" + System.Environment.NewLine); } }
                        if (!objReader.IsDBNull(objReader.GetOrdinal("ContactName"))) { if (objReader["ContactName"].ToString().Length > 0) { objBuilder.Append("	<tr><td class=\"GreyBorder\" width=\"20%\"><b>Name:</b></td><td class=\"GreyBorder\">" + objReader["ContactName"] + "&nbsp;</td></tr>" + System.Environment.NewLine); } }
                        if (!objReader.IsDBNull(objReader.GetOrdinal("ContactPhone")))
                        {
                            if (objReader["ContactPhone"].ToString().Length > 0) { objBuilder.Append("	<tr><td class=\"GreyBorder\" width=\"20%\"><b>Phone:</b></td><td class=\"GreyBorder\">" + objReader["ContactPhone"]); }
                            if (!objReader.IsDBNull(objReader.GetOrdinal("PhoneExtension"))) { if (objReader["PhoneExtension"].ToString().Length > 0) {   objBuilder.Append(" Ext. " + objReader["PhoneExtension"]); } }
                            objBuilder.Append("</td></tr>" + System.Environment.NewLine);
                        }
                        if (!objReader.IsDBNull(objReader.GetOrdinal("ContactTollFreePhone"))) { if (objReader["ContactTollFreePhone"].ToString().Length > 0) {   objBuilder.Append("	<tr><td class=\"GreyBorder\" width=\"20%\"><b>Toll-free Phone:</b></td><td class=\"GreyBorder\">" + objReader["ContactTollFreePhone"] + "&nbsp;</td></tr>" + System.Environment.NewLine); } }
                        if (!objReader.IsDBNull(objReader.GetOrdinal("ContactFax"))) { if (objReader["ContactFax"].ToString().Length > 0) {   objBuilder.Append("	<tr><td class=\"GreyBorder\" width=\"20%\"><b>Fax Number:</b></td><td class=\"GreyBorder\">" + objReader["ContactFax"] + "&nbsp;</td></tr>" + System.Environment.NewLine); } }
                        if (!objReader.IsDBNull(objReader.GetOrdinal("ContactEmailAddress"))) { if (objReader["ContactEmailAddress"].ToString().Length > 0) { objBuilder.Append("	<tr><td class=\"GreyBorder\" width=\"20%\"><b>Email Address:</b></td><td class=\"GreyBorder\"><a href=\"mailto:" + objReader["ContactEmailAddress"] + "\">" + objReader["ContactEmailAddress"] + "</a>&nbsp;</td></tr>" + System.Environment.NewLine); } }
                        if (!objReader.IsDBNull(objReader.GetOrdinal("ContactAddress1")) || !objReader.IsDBNull(objReader.GetOrdinal("ContactAddress2")))
                        {
                            objBuilder.Append("	<tr><td class=\"GreyBorder\" width=\"20%\"><b>Mailing Address:</b></td><td class=\"GreyBorder\">");
                            if (!objReader.IsDBNull(objReader.GetOrdinal("ContactAddress1"))) { if (objReader["ContactAddress1"].ToString().Length > 0) { objBuilder.Append(objReader["ContactAddress1"] + "&nbsp;<br />" + System.Environment.NewLine); } }
                            if (!objReader.IsDBNull(objReader.GetOrdinal("ContactAddress2"))) { if (objReader["ContactAddress2"].ToString().Length > 0) { objBuilder.Append("	" + objReader["ContactAddress2"] + "&nbsp;<br />" + System.Environment.NewLine); } }
                            if (!objReader.IsDBNull(objReader.GetOrdinal("ContactCity"))) { if (objReader["ContactCity"].ToString().Length > 0) { objBuilder.Append("	" + objReader["ContactCity"] + ", " + objReader["ContactState"] + "&nbsp;&nbsp;" + objReader["ContactZIP"]); } }
                            objBuilder.Append("&nbsp;</td></tr>" + System.Environment.NewLine);
                        }
                        objBuilder.Append("	</table> <br />" + System.Environment.NewLine); 
                   }
                }
                if (!blShowDetail) { objBuilder.Append("	</table>" + System.Environment.NewLine); }
                return objBuilder.ToString();
            }
            else
            {
                string strResult = "<p>No record found.</p>";
                return strResult;
            }
        }
        catch (Exception ex)
        {
            string strResult = ex.Message;
            return strResult;
        }
        finally
        {
            objConn.Close();
            objConn.Dispose();
            objCommand.Dispose();
        }
    }

    public string ProcessScholarshipCheckboxes(string strRequestObject)
    {
        string[] strArrDetail = strRequestObject.Split(',');
        Int32 intUBound = 0;
        string strSQL = "";
        Int32 i = 0;
        if (strRequestObject.Contains(",")) 
        {
            intUBound = strArrDetail.GetUpperBound(0);
            for( i = 0; i <= intUBound; i++)
            {
                strSQL += "ScholarshipNumber = " + strArrDetail[i] + " OR ";
            }
            if (strSQL.Substring((strSQL.Length - 4),4) == " OR ")
            {
                strSQL = strSQL.Substring(0, (strSQL.Length - 4)); 
            }
        }
        else if (strRequestObject != "")
        {
            strSQL = strSQL + "ScholarshipNumber = " + strRequestObject + " ";
        }
        return strSQL;
    }

    public DataTable GetScholarshipData(Int32 intScholarshipNumber)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCommand = new SqlCommand();
        SqlParameter param = new SqlParameter();
        SqlDataAdapter objDataAdapter;
        DataSet ds = new DataSet();
        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSScholarships"].ToString();
        objConn.Open();
        objCommand.CommandText = "usp_CCSScholarships_Select";
        objCommand.CommandType = CommandType.StoredProcedure;
        objCommand.Connection = objConn;

        param.ParameterName = "@ScholarshipNumber";
        param.SqlDbType = SqlDbType.Int;
        param.Value = intScholarshipNumber;
        objCommand.Parameters.Add(param);
        objDataAdapter = new SqlDataAdapter(objCommand);
        try
        {
            objDataAdapter.Fill(ds);
        }
        catch
        {
            // do nothing
        }
        finally
        {
            objConn.Close();
            objConn.Dispose();
            objCommand.Dispose();
        }
        return ds.Tables[0];
    }

    public Int32 InsertScholarship(string ScholarshipTitle,string ScholarshipPurpose,string Eligibility,string Limitations,string Amount,string Duration,string ContactName,string ContactAgency,
           string ContactPhone,string ContactTollFreePhone,string ContactFax,string ContactAddress1,string ContactAddress2,string ContactCity,string ContactState,string ContactZIP,string ContactEmailAddress,
           string ApplicationDeadline,string NumberAwarded,string WebAddress,string LevelOfStudy,string Notes, Int32 EssayRequired,Int32 TranscriptRequired,Int32 LetterOfRecommendation,Int32 GenderBased,
           Int32 EthnicityBased,Int32 AfricanAmerican,Int32 AsianAmerican,Int32 Hispanic,Int32 NativeAmerican,Int32 SpecificSchool,Int32 ReligiousAffiliation,Int32 DisabilityBased,Int32 Military,
           Int32 GPARequired,Int32 FinancialNeed,Int32 CommunityServiceRequired,string UpdatedBy,string SchoolName,string DisabilityType,string Gender,string PhoneExtension,string GPACutoff,
           string Program, string College)
    {
        Int32 intResult = 0;
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCommand = new SqlCommand();
        string _strSQL = "usp_CCSScholarships_Insert '" + ScholarshipTitle + "', '" + ScholarshipPurpose + "', '" + Eligibility + "', '" + Limitations + "', '" + Amount + "', '" + Duration + "', '" + ContactName + "', '" + ContactAgency +
           "', '" + ContactPhone + "', '" + ContactTollFreePhone + "', '" + ContactFax + "', '" + ContactAddress1 + "', '" + ContactAddress2 + "', '" + ContactCity + "', '" + ContactState + "', '" + ContactZIP + "', '" + ContactEmailAddress +
           "', '" + ApplicationDeadline + "', '" + NumberAwarded + "', '" + WebAddress + "', '" + LevelOfStudy + "', '" + Notes + "', " + EssayRequired + ", " + TranscriptRequired + ", " + LetterOfRecommendation + ", " + GenderBased +
           ", " + EthnicityBased + ", " + AfricanAmerican + ", " + AsianAmerican + ", " + Hispanic + ", " + NativeAmerican + ", " + SpecificSchool + ", " + ReligiousAffiliation + ", " + DisabilityBased + ", " + Military +
           ", " + GPARequired + ", " + FinancialNeed + ", " + CommunityServiceRequired + ", '" + System.DateTime.Today.ToShortDateString() + "', '" + UpdatedBy + "', '" + SchoolName + "', '" + DisabilityType + "', '" + Gender + "', '" + PhoneExtension + "', '" + GPACutoff +
           "', '" + Program + "', 0, '" + System.DateTime.Today.ToShortDateString() + "', '" + College + "'";
        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSScholarships"].ToString();
        try
        {
            objConn.Open();
        }
        catch
        {
            //do nothing
            return 99;
        }
        objCommand.CommandText = _strSQL;
        objCommand.CommandType = CommandType.Text;
        objCommand.Connection = objConn;


        try
        {
            intResult = (Int32)objCommand.ExecuteScalar();

        }
        catch
        {
            //do nothing
            return intResult;
        }
        finally
        {
            objConn.Close();
        }
        objConn.Dispose();
        objCommand.Dispose();
        return intResult;
    }

    public string UpdateScholarship(Int32 intScholarshipNumber, string ScholarshipTitle, string ScholarshipPurpose, string Eligibility, string Limitations, string Amount, string Duration, string ContactName, string ContactAgency,
           string ContactPhone, string ContactTollFreePhone, string ContactFax, string ContactAddress1, string ContactAddress2, string ContactCity, string ContactState, string ContactZIP, string ContactEmailAddress,
           string ApplicationDeadline, string NumberAwarded, string WebAddress, string LevelOfStudy, string Notes, Int32 EssayRequired, Int32 TranscriptRequired, Int32 LetterOfRecommendation, Int32 GenderBased,
           Int32 EthnicityBased, Int32 AfricanAmerican, Int32 AsianAmerican, Int32 Hispanic, Int32 NativeAmerican, Int32 SpecificSchool, Int32 ReligiousAffiliation, Int32 DisabilityBased, Int32 Military,
           Int32 GPARequired, Int32 FinancialNeed, Int32 CommunityServiceRequired, string UpdatedBy, string SchoolName, string DisabilityType, string Gender, string PhoneExtension, string GPACutoff,
           string Program, Int32 Expired, string College)
    {
        string strSuccess = "";
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCommand = new SqlCommand();
        string _strSQL = "usp_CCSScholarships_Update " + intScholarshipNumber + ", '" + ScholarshipTitle + "', '" + ScholarshipPurpose + "', '" + Eligibility + "', '" + Limitations + "', '" + Amount + "', '" + Duration + "', '" + ContactName + "', '" + ContactAgency +
           "', '" + ContactPhone + "', '" + ContactTollFreePhone + "', '" + ContactFax + "', '" + ContactAddress1 + "', '" + ContactAddress2 + "', '" + ContactCity + "', '" + ContactState + "', '" + ContactZIP + "', '" + ContactEmailAddress +
           "', '" + ApplicationDeadline + "', '" + NumberAwarded + "', '" + WebAddress + "', '" + LevelOfStudy + "', '" + Notes + "', " + EssayRequired + ", " + TranscriptRequired + ", " + LetterOfRecommendation + ", " + GenderBased +
           ", " + EthnicityBased + ", " + AfricanAmerican + ", " + AsianAmerican + ", " + Hispanic + ", " + NativeAmerican + ", " + SpecificSchool + ", " + ReligiousAffiliation + ", " + DisabilityBased + ", " + Military +
           ", " + GPARequired + ", " + FinancialNeed + ", " + CommunityServiceRequired + ", '" + System.DateTime.Today.ToShortDateString() + "', '" + UpdatedBy + "', '" + SchoolName + "', '" + DisabilityType + "', '" + Gender + "', '" + PhoneExtension + "', '" + GPACutoff +
           "', '" + Program + "', " +  Expired + ", '" + College + "'";
        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSScholarships"].ToString();
        objCommand.CommandText = _strSQL;
        objCommand.CommandType = CommandType.Text;
        objCommand.Connection = objConn;
        try
        {
            objConn.Open();
            objCommand.ExecuteNonQuery();
        }
        catch (Exception ex)
        {
            //do nothing
            strSuccess = ex.Message + "<br /> SQL: " + _strSQL + "<br />";
        }
        finally
        {
            objConn.Close();
        }
        objConn.Dispose();
        objCommand.Dispose();
        return strSuccess;
    }
    public string DeleteScholarship(Int32 intScholarshipNumber)
    {
        string strSuccess = "";
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCommand = new SqlCommand();
        string _strSQL = "usp_CCSScholarships_Delete " + intScholarshipNumber;
        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSScholarships"].ToString();
        objCommand.CommandText = _strSQL;
        objCommand.CommandType = CommandType.Text;
        objCommand.Connection = objConn;
        try
        {
            objConn.Open();
            objCommand.ExecuteNonQuery();
        }
        catch (Exception ex)
        {
            //do nothing
            strSuccess = ex.Message + "<br /> SQL: " + _strSQL + "<br />";
        }
        finally
        {
            objConn.Close();
        }
        objConn.Dispose();
        objCommand.Dispose();

        return strSuccess;
    }
}