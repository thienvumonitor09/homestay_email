﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Security.Principal;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;

public partial class Homestay_Students_AgreementsUAp1 : System.Web.UI.Page
{
    String strAppPart = "";
    String applicantID = "";
    bool blNoButtons = false;
    Int32 intApplicantID = 0;
    String strResultMsg = "";
    Int32 intResult = 0;
    String strUploadURL = @"\\ccs-internet\InternetContent\CCS\Homestay\";
    String strFileName = "";
    String strLastName = "";
    String strFirstName = "";
    String strWaiveLiabilityParentUA = "";
    String strCCSConductUA = "";
    String strNoticesWaiverParentUA = "";
    Boolean blWaiveLiabilityParentUASubmitted = false;
    Boolean blCCSConductUASubmitted = false;
    Boolean blNoticesWaiverParentUASubmitted = false;
    Boolean blEmailSent = false;
    DataTable dtStudent;
    DataTable dtHomestayStudent;
    DataTable dtEmailSent;

    public static String intlEmail = "globalprograms@ccs.spokane.edu";
    string sendTo = "";
    string sentFrom = "Internationalhomestay@ccs.spokane.edu";
    string strCCaddresses = "Internationalhomestay@ccs.spokane.edu;amy.cosgrove@ccs.spokane.edu;aadil.refaey@ccs.spokane.edu;laura.padden@ccs.spokane.edu";
    string strSubject = "International Homestay Student Application Submission";
    string strEmailResult = "";
    string strBody = "";

    Boolean blIsAdmin = false;
    Homestay hsInfo = new Homestay();
    

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            hdnPartNo.Value = "1";//set current part no
        }
           
        String strLogonUser = Request.ServerVariables["LOGON_USER"];

        if (strLogonUser != "" && strLogonUser != null)
        {
            //If user has not logged out of Family Portal, forms authentication considers the person logged in
            if (strLogonUser.Contains("ccs\\"))
            {
                blIsAdmin = true;
            }
        }


        if (Request.QueryString["ID"] != null)
        {
            //page load
            applicantID = Request.QueryString["ID"].ToString();
            hdnApplicantID.Value = applicantID;
        }
        if (applicantID == "")
        {
            //after postback
            applicantID = hdnApplicantID.Value;
        }
        if (applicantID != "" && applicantID != null && applicantID != "0")
        {
            intApplicantID = Convert.ToInt32(applicantID);
        }
        if (intApplicantID != 0)
        {
            dtStudent = hsInfo.GetOneInternationalStudent("", intApplicantID, "", "", "", "");
            if (dtStudent != null)
            {
                if (dtStudent.Rows.Count > 0)
                {
                    strFirstName = dtStudent.Rows[0]["firstName"].ToString();
                    strLastName = dtStudent.Rows[0]["familyName"].ToString();
                    sendTo = dtStudent.Rows[0]["Email"].ToString();
                }
            }
            //Add agent's email
            DataTable dtContact = hsInfo.GetOneContact(3, intApplicantID);
            if (dtContact != null)
            {
                if (dtContact.Rows.Count > 0)
                {

                    string strAgentEmail = dtContact.Rows[0]["Email"].ToString();
                    strCCaddresses += ";" + strAgentEmail;
                }
            }
        }
        getFileInfo();
    }

    #region Export to PDF
    protected void btnExport_Click(object sender, EventArgs e)
    {
        Document pdfDoc = new Document(PageSize.A4, 25, 10, 25, 10);
        PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
        pdfDoc.Open();
        BaseFont bf = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
        //Font font1 = new Font(bf, 16, Font.NORMAL, Color.BLUE);
        Font font3 = new Font(bf, 15, Font.BOLD);
        Paragraph Text = new Paragraph("Agreements", font3);
        pdfDoc.Add(Text);
        #region Get File Names
        string waiveLiabilityParentUAStr = "";
        string CCSConductUAStr = "";
        string NoticesWaiverParentUAStr = "";
        DataTable dt = hsInfo.GetUploadedFiles(intApplicantID);
        if (dt != null)
        {
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {

                    string fileNameStr = dr["fileName"].ToString();
                    //Text = new Paragraph(fileNameStr);
                    //pdfDoc.Add(Text);
                    if (fileNameStr.Contains("WaiveLiabilityParentUA"))
                    {
                        waiveLiabilityParentUAStr += fileNameStr +"\n";
                    }
                    if (fileNameStr.Contains("CCSConductUA"))
                    {
                        CCSConductUAStr += fileNameStr + "\n";
                    }
                    if (fileNameStr.Contains("NoticesWaiverParentUA"))
                    {
                        NoticesWaiverParentUAStr += fileNameStr + "\n";
                    }
                }
            }
        }
        #endregion

        if (dtHomestayStudent != null)
        {
            if (dtHomestayStudent.Rows.Count > 0)
            {
                #region Waiver of Liability (Parents)
                Text = new Paragraph("Waiver of Liability (Parents)", font3);
                pdfDoc.Add(Text);
                Text = new Paragraph("I understand that my parents or guardians must print and sign this agreement. to complete my application process."
                                    + ((dtHomestayStudent.Rows[0]["agreeWaiveLiabilityParentUA"].ToString() == "True") ? "Checked" : "Not checked"));
                pdfDoc.Add(Text);
               
                Text = new Paragraph("Document Upload", font3);
                pdfDoc.Add(Text);
                Text = new Paragraph(waiveLiabilityParentUAStr);
                pdfDoc.Add(Text);
                #endregion

                #region CCS Conduct Agreement
                Text = new Paragraph("Waiver of Liability (Parents)", font3);
                pdfDoc.Add(Text);
                Text = new Paragraph("I acknowledge that I must print AND SIGN this agreement."
                                    + ((dtHomestayStudent.Rows[0]["agreeCCSConductUA"].ToString() == "True") ? "Checked" : "Not checked"));
                pdfDoc.Add(Text);

                Text = new Paragraph("Document Upload", font3);
                pdfDoc.Add(Text);
                Text = new Paragraph(CCSConductUAStr);
                pdfDoc.Add(Text);
                #endregion

                #region Notices and Waiver of Liability
                Text = new Paragraph("Notices and Waivers", font3);
                pdfDoc.Add(Text);
                Text = new Paragraph("I understand what is required of me during my stay with a Homestay Host Family. I understand I must print, sign, and submit all agreements to the International Student Office."
                                    + ((dtHomestayStudent.Rows[0]["agreeNoticesWaiverParentUA"].ToString() == "True") ? "Checked" : "Not checked"));
                pdfDoc.Add(Text);

                Text = new Paragraph("Document Upload", font3);
                pdfDoc.Add(Text);
                Text = new Paragraph(NoticesWaiverParentUAStr);
                pdfDoc.Add(Text);
                #endregion

            }
        }
        
        
        pdfWriter.CloseStream = false;
        pdfDoc.Close();
        string exportFileName = strLastName + "-" + strFirstName + "-" + "Agreements";
        Response.Buffer = true;
        Response.ContentType = "application/pdf";
        Response.AddHeader("content-disposition", "attachment;filename=" + exportFileName + ".pdf");
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.Write(pdfDoc);
        Response.End();
    }
#endregion

    protected void btnUploadWaiveLiabParentUA_Click(object sender, EventArgs e)
    {
        //Response.Write("here");
        eWaiveLiabilityParentUAFile.InnerText = "";
        if (uplWaiveLiabilityParentUAFile.HasFile)
        {
            if (Utility.ContainsNonEnglishChars(uplWaiveLiabilityParentUAFile.FileName))
            {
                //Response.Write(uplWaiveLiabilityParentUAFile.FileName);
                eWaiveLiabilityParentUAFile.InnerText = "You have selected a file containing invalid characters. Please rename the file with only English characters!";
                
                return;
            }

            // Response.Write("id: " + intApplicantID.ToString() + "<br />");
            strFileName = strLastName + "-" + intApplicantID + "-WaiveLiabilityParentUA-" + uplWaiveLiabilityParentUAFile.FileName;
            uplWaiveLiabilityParentUAFile.SaveAs(strUploadURL + strFileName);
            strResultMsg += "Your agreement, <span style=\"color:red;\">" + strFileName + "</span>, was successfully uploaded!<br />";
            //Insert record in UploadedFilesInformation table
            intResult = hsInfo.InsertUploadedFilesInformation(intApplicantID, strFirstName + " " + strLastName, strFileName, strUploadURL);
            blNoticesWaiverParentUASubmitted = true;
            //Update status of submission
            intResult = hsInfo.UpdateAgreementSubmittedHomestayStudentInfo(intApplicantID, 1, "WaiveLiabilityParentUA");
          //  Response.Write("result: " + intResult.ToString() + "<br />");
        }
        else
        {
            // no file found
        }
        getFileInfo();
        //set visibility
        divUploadWaiveLiabilityParentUA.Visible = true;

        divUploadCCSConductUA.Visible = false;
        divConfirmation.Visible = false;
        btnDone.Visible = false;
        btnContinue2.Visible = true;
        btnContinue3.Visible = false;
        btnPrevious.Visible = true;
        div1.Visible = true;
        div2.Visible = false;
        div3.Visible = false;
        btnCmdParentAgreement.Focus();
    }
    protected void btnCmdSave_Click(object sender, EventArgs e)
    {
        //process submission
        strResultMsg += "NoticesWaiverParentUA Checked? " + chkNoticesWaiverParentUA.Checked + "<br />";

        if (!chkNoticesWaiverParentUA.Checked)
        {
            eNoticesAndWaiver.InnerHtml = "You must check the box to indicate you understand that you must print, sign, and return all agreements to the International Student Office.";
            eNoticesAndWaiver.Focus();
            hdnNoticesWaiverParentUASubmitted.Value = "false";
  //          Response.Write("NOT CHECKED<Br />");
            return;
        }
        else  // process submission
        {
            intResult = hsInfo.UpdateUnderstandAgreeHomestayStudentInfo(intApplicantID, 1, "NoticesWaiverParentUA");

            //Upload the Waive Liability Parent UA agreement
            if (uplNoticesWaiverParentUAFile.HasFile)
            {
                strFileName = strLastName + "-" + intApplicantID + "-NoticesWaiverParentUA-" + uplNoticesWaiverParentUAFile.FileName;
                uplNoticesWaiverParentUAFile.SaveAs(strUploadURL + strFileName);
                strResultMsg += "Your agreement, <span style=\"color:red;\">" + strFileName + "</span>, was successfully uploaded!<br />";
                //Insert record in UploadedFilesInformation table
                intResult = hsInfo.InsertUploadedFilesInformation(intApplicantID, strFirstName + " " + strLastName, strFileName, strUploadURL);
                blNoticesWaiverParentUASubmitted = true;
                //Update status of submission
                intResult = hsInfo.UpdateAgreementSubmittedHomestayStudentInfo(intApplicantID, 1, "NoticesWaiverParentUA");
            }
            else if (hdnNoticesWaiverParentUASubmitted.Value.ToLower() == "true")
            {
                //previous submission exists
                blNoticesWaiverParentUASubmitted = true;
            }
            divUploadCCSConductUA.Visible = false;
            divNoticesWaiverParentUA.Visible = false;
            divConfirmation.Visible = true;
            div1.Visible = false;
            div2.Visible = false;
            div3.Visible = false;
            divButtons.Visible = false;
            divInstructions.Visible = false;
        }// end box is checked
    }
    protected void btnCmdContinuePart2_Click(object sender, EventArgs e)
    {
        hdnPartNo.Value = "2";//set current part no
        if (!chkWaiveLiabilityParentUA.Checked)
        {
            echkWaiveParentUA.InnerHtml = "You must check the box to indicate you understand that your parents or guardians much print and sign this agreement";
            chkWaiveLiabilityParentUA.Focus();
            return;
        } 
        else        //process submission
        {
            intResult = hsInfo.UpdateUnderstandAgreeHomestayStudentInfo(intApplicantID, 1, "WaiveLiabilityParentUA");
        }
        //Upload the Waive Liability Parent UA agreement
        if (uplWaiveLiabilityParentUAFile.HasFile)
        {
            strFileName = strLastName + "-" + intApplicantID + "-WaiveLiabilityParentUA-" + uplWaiveLiabilityParentUAFile.FileName;
            uplWaiveLiabilityParentUAFile.SaveAs(strUploadURL + strFileName);
            strResultMsg += "Your agreement, <span style=\"color:red;\">" + strFileName + "</span>, was successfully uploaded!<br />";
            //Insert record in UploadedFilesInformation table
            intResult = hsInfo.InsertUploadedFilesInformation(intApplicantID, strFirstName + " " + strLastName, strFileName, strUploadURL);
            blWaiveLiabilityParentUASubmitted = true;
            //Update status of submission
            intResult = hsInfo.UpdateAgreementSubmittedHomestayStudentInfo(intApplicantID, 1, "WaiveLiabilityParentUA");
        }
        else if (hdnWaiveLiabilityParentUASubmitted.Value.ToLower() == "true")
        {
            //previous submission exists
            blWaiveLiabilityParentUASubmitted = true;
        }
        //Visibility
        if (!blCCSConductUASubmitted) { divUploadCCSConductUA.Visible = true; }
        divUploadWaiveLiabilityParentUA.Visible = false;
        divUploadCCSConductUA.Visible = true;
        divConfirmation.Visible = false;
        btnDone.Visible = false;
        btnContinue2.Visible = false;
        btnPrevious.Visible = true;
        div1.Visible = false;
        div2.Visible = true;
        div3.Visible = false;
        divUploadNoticesWaiverParentUA.Visible = false;
        btnContinue3.Visible = true;
    }

    protected void btnCmdContinuePart3_Click(object sender, EventArgs e)
    {
        hdnPartNo.Value = "3";//set current part no
        //process submission
        strResultMsg += "CCSConductUA Checked? " + chkCCSConductUA.Checked + "<br />";
        if (!chkCCSConductUA.Checked)
        {
            eStudentAgreement.InnerHtml = "You must check the box to indicate you understand that you must print and sign this agreement";
            chkCCSConductUA.Focus();
            return;
        }
        else        //process submission
        {
            intResult = hsInfo.UpdateUnderstandAgreeHomestayStudentInfo(intApplicantID, 1, "CCSConductUA");
        }
        //Upload the Waive Liability Parent UA agreement
        if (uplCCSConductUAFile.HasFile)
        {
            strFileName = strLastName + "-" + intApplicantID + "-CCSConductUA-" + uplCCSConductUAFile.FileName;
            uplCCSConductUAFile.SaveAs(strUploadURL + strFileName);
            strResultMsg += "Your agreement, <span style=\"color:red;\">" + strFileName + "</span>, was successfully uploaded!<br />";
            //Insert record in UploadedFilesInformation table
            intResult = hsInfo.InsertUploadedFilesInformation(intApplicantID, strFirstName + " " + strLastName, strFileName, strUploadURL);
            blCCSConductUASubmitted = true;
            //Update status of submission
            intResult = hsInfo.UpdateAgreementSubmittedHomestayStudentInfo(intApplicantID, 1, "CCSConductUA");
        }
        else if (hdnCCSConductUASubmitted.Value.ToLower() == "true")
        {
            //previous submission exists
            blCCSConductUASubmitted = true;
        }
        //Visibility
       // if (!blNoticesWaiverParentUASubmitted) { divUploadNoticesWaiverParentUA.Visible = true; }
        divUploadNoticesWaiverParentUA.Visible = true;
        divUploadCCSConductUA.Visible = false;
        divConfirmation.Visible = false;
        btnDone.Visible = false;
        btnContinue2.Visible = false;
        btnContinue3.Visible = false;
        btnPrevious.Visible = true;
        div1.Visible = false;
        div2.Visible = false;
        div3.Visible = true;
    }
    protected void btnCmdParentAgreement_Click(object sender, EventArgs e)
    {
        if (!chkWaiveLiabilityParentUA.Checked)
        {
            echkWaiveParentUA.InnerHtml = "You must check the box to indicate you understand that you must print and sign the Student Agreement agreement";
            chkWaiveLiabilityParentUA.Focus();
            return;
        }
        else
        {
            intResult = hsInfo.UpdateAgreementSubmittedHomestayStudentInfo(intApplicantID, 1, "agreeWaiveLiabilityParentUA");
        }
        if (uplWaiveLiabilityParentUAFile.HasFile)
        {
            
            strFileName = strLastName + "-" + intApplicantID + "-WaiveLiabilityParentUA-" + uplWaiveLiabilityParentUAFile.FileName;
            uplWaiveLiabilityParentUAFile.SaveAs(strUploadURL + strFileName);
            strResultMsg += "Your agreement, <span style=\"color:red;\">" + strFileName + "</span>, was successfully uploaded!<br />";
            //Insert record in UploadedFilesInformation table
            intResult = hsInfo.InsertUploadedFilesInformation(intApplicantID, strFirstName + " " + strLastName, strFileName, strUploadURL);
            blWaiveLiabilityParentUASubmitted = true;
            //Update status of submission
            intResult = hsInfo.UpdateAgreementSubmittedHomestayStudentInfo(intApplicantID, 1, "WaiveLiabilityParentUA");
        }
        //set Visibility
        divUploadWaiveLiabilityParentUA.Visible = false;
        btnContinue2.Visible = false;
        div1.Visible = false;
        div2.Visible = true;
        divUploadCCSConductUA.Visible = true;
        btnContinue3.Visible = true;
    }
    protected void btnCmdStudentAgreement_Click(object sender, EventArgs e)
    {

        if (!chkCCSConductUA.Checked)
        {
            eStudentAgreement.InnerHtml = "You must check the box to indicate you understand and agree";
            chkCCSConductUA.Focus();
            return;
        }
        else
        {
            // process the submission
            intResult = hsInfo.UpdateAgreementSubmittedHomestayStudentInfo(intApplicantID, 1, "agreeCCSConductUA");
        }
        if (uplCCSConductUAFile.HasFile)
        {
            strFileName = strLastName + "-" + intApplicantID + "-CCSConductUA-" + uplCCSConductUAFile.FileName;
            uplCCSConductUAFile.SaveAs(strUploadURL + strFileName);
            strResultMsg += "Your agreement, <span style=\"color:red;\">" + strFileName + "</span>, was successfully uploaded!<br />";
            //Insert record in UploadedFilesInformation table
            intResult = hsInfo.InsertUploadedFilesInformation(intApplicantID, strFirstName + " " + strLastName, strFileName, strUploadURL);
            blCCSConductUASubmitted = true;
            //Update status of submission
            intResult = hsInfo.UpdateAgreementSubmittedHomestayStudentInfo(intApplicantID, 1, "CCSConductUA");
        }
        else if (hdnCCSConductUASubmitted.Value.ToLower() == "true")
        {
            //previous submission exists
            blCCSConductUASubmitted = true;
        }
        divNoticesWaiverParentUA.Visible = true;
        divConfirmation.Visible = false;
        div1.Visible = false;
        div2.Visible = false;
        div3.Visible = true;
        divButtons.Visible = true;
        divInstructions.Visible = true;
        divUploadWaiveLiabilityParentUA.Visible = false;
        divUploadCCSConductUA.Visible = false;
        btnDone.Visible = false;
        btnContinue2.Visible = false;
        btnContinue3.Visible = false;
        divUploadNoticesWaiverParentUA.Visible = true;
        btnPrevious.Visible = true;
    }

    protected void btnCmdSaveNoticesAndWaiver_Click(object sender, EventArgs e)
    {
        //process submission
        strResultMsg += "NoticesWaiverParentUA Checked? " + chkNoticesWaiverParentUA.Checked + "<br />";

        if (!chkNoticesWaiverParentUA.Checked)
        {
            eNoticesAndWaiver.InnerHtml = "You must check the box to indicate you understand that you must print, sign, and return all agreements to the International Student Office.";
            chkNoticesWaiverParentUA.Focus();
            return;
        } else  
        {
            // process submission
            intResult = hsInfo.UpdateUnderstandAgreeHomestayStudentInfo(intApplicantID, 1, "NoticesWaiverParentUA");
        }
        //Upload the Waive Liability Parent UA agreement
        if (uplNoticesWaiverParentUAFile.HasFile)
        {
            strFileName = strLastName + "-" + intApplicantID + "-NoticesWaiverParentUA-" + uplNoticesWaiverParentUAFile.FileName;
            uplNoticesWaiverParentUAFile.SaveAs(strUploadURL + strFileName);
            strResultMsg += "Your agreement, <span style=\"color:red;\">" + strFileName + "</span>, was successfully uploaded!<br />";
            //Insert record in UploadedFilesInformation table
            intResult = hsInfo.InsertUploadedFilesInformation(intApplicantID, strFirstName + " " + strLastName, strFileName, strUploadURL);
            blNoticesWaiverParentUASubmitted = true;
            //Update status of submission
            intResult = hsInfo.UpdateAgreementSubmittedHomestayStudentInfo(intApplicantID, 1, "NoticesWaiverParentUA");
        }
        else if (hdnNoticesWaiverParentUASubmitted.Value.ToLower() == "true")
        {
            //previous submission exists
            blNoticesWaiverParentUASubmitted = true;
        }
        // see if email already sent
        /*
        int EmailSentValue;
        try
        {
            EmailSentValue = hsInfo.GetEmailSent(intApplicantID, "Student");
            
            Response.Write("value returned: " + EmailSentValue.ToString() + "<br />");
            blEmailSent = Convert.ToBoolean(EmailSentValue);
        }
        catch (Exception ex)
        {
            Response.Write("error: " + ex.Message + "<br />" + ex.StackTrace);
        }
         */
        /*       
               if (dtEmailSent != null)
               {
                   Response.Write("not null<br />");
                   if (dtEmailSent.Rows.Count > 0)
                   {
                       Response.Write("value returned: " + dtEmailSent.Rows[0]["submittedEmailSent"].ToString() + "<br />");
                       blEmailSent = Convert.ToBoolean(dtEmailSent.Rows[0]["submittedEmailSent"].ToString());
                       Response.Write("converted to boolean email value: " + blEmailSent.ToString() + "<br />");
                   }
               }// not null
         */
        // verify upload of files
        string strMissingUploads = "";
        blWaiveLiabilityParentUASubmitted = false;
        blNoticesWaiverParentUASubmitted = false;
        blCCSConductUASubmitted = false;
        DataTable dt = hsInfo.GetUploadedFiles(intApplicantID);
        if (dt != null)
        {
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    strFileName = dr["fileName"].ToString();
                    if (strFileName.Contains("-CCSConductUA-"))
                        blCCSConductUASubmitted = true;
                    else if (strFileName.Contains("-WaiveLiabilityParentUA-"))
                        blWaiveLiabilityParentUASubmitted = true;  
                    else if (strFileName.Contains("-NoticesWaiverParentUA-"))
                        blNoticesWaiverParentUASubmitted = true;


                }// end foreach
            }// end row count > 0
        }// end dt not null
        // if flags end up still false, populate the missing files string (strMissingUploads)
        if(!blCCSConductUASubmitted)
            strMissingUploads += "Student Agreement form";
        if(!blWaiveLiabilityParentUASubmitted)
        {
            if (strMissingUploads.Length > 1)
                strMissingUploads += ", the student's Parents/Guardian Agreement form";
            else
                strMissingUploads = "Student's Parents/Guardian Agreement form";
        }
        if(!blNoticesWaiverParentUASubmitted)
            if (strMissingUploads.Length > 1)
                strMissingUploads += "; the Notice and Waivers form ";
            else
                strMissingUploads = "Notice and Waivers form ";
        if (!blCCSConductUASubmitted || !blNoticesWaiverParentUASubmitted || !blNoticesWaiverParentUASubmitted)
            litMissingUploads.Text = "<p><span style='color: #DD0000;'>All forms must be signed, scanned and uploaded into your application or emailed to <a href='mailto:Internationalhomestay@ccs.spokane.edu'>Internationalhomestay@ccs.spokane.edu</a>. The " + strMissingUploads + " need to be uploaded</span>.</p>";
        else
            litMissingUploads.Text = "";
        // generate the email to the student
        Utility uEmail = new Utility();
        //Response.Write("current submitted email value: " + blEmailSent.ToString() + "<br />");

        // Display confirmation
        divUploadCCSConductUA.Visible = false;
        divNoticesWaiverParentUA.Visible = false;
        divConfirmation.Visible = true;
        div1.Visible = false;
        div2.Visible = false;
        div3.Visible = false;
        divButtons.Visible = false;
        divInstructions.Visible = false;

        if (!blEmailSent)
        {
            if (sendTo != "" && !blEmailSent)
            {
                //sendTo = "vu.nguyen@ccs.spokane.edu";
                // prepare the body of the email
                strBody = "";
                strBody = "<p>Dear " + strFirstName + " " + strLastName + ", </p>";
                strBody += "<p>Thank you for applying to the CCS Homestay Program!  We will review your application and begin the process of matching a family to your profile.  ";
                strBody += "Please feel free to learn more about the Homestay Program at our website:<br />";
                strBody += "<a href='https://sfcc.spokane.edu/Become-a-Student/I-am-an-International-Student/After-I-Have-Been-Admitted/Housing-Options/Homestay' target='_blank'>Homestay Program information</a></p>";
                strBody += "<p>Please take note of the following details and recommendations:<br />";
                strBody += "<ul><li>Student applications must be submitted 30 days prior to arrival.</li>";
                if (!blCCSConductUASubmitted || !blNoticesWaiverParentUASubmitted || !blNoticesWaiverParentUASubmitted)
                {
                    strBody += "<li>All forms must be signed, scanned and uploaded into your application or emailed to <a href='mailto:Internationalhomestay@ccs.spokane.edu'>Internationalhomestay@ccs.spokane.edu</a>. The " + strMissingUploads + "need to be uploaded.</li>";
                    // add message to confirmation web display
                    litMissingUploads.Text = "<p><span style='color: #DD0000;'>All forms must be signed, scanned and uploaded into your application or emailed to <a href='mailto:Internationalhomestay@ccs.spokane.edu'>Internationalhomestay@ccs.spokane.edu</a>. The " + strMissingUploads + " need to be uploaded</span>.</p>";
                }
                strBody += "<li>Housing Placements will be released to the agent/student after the student visa is approved.</li>";
                strBody += "<li>Minors (students ages 16-17) are REQUIRED to be in Homestay.</li>";
                strBody += "<li>Housing is not guaranteed for students over the age of 18. The application fee will be refunded if no suitable family is available.</li>";
                strBody += "<li>Preferences indicated on the Homestay Application will guide placements, but preferences cannot be guaranteed. </li>";
                strBody += "<li>After placement is confirmed, please contact your host family right away by email. They are looking forward to connecting with you and making arrangements for your arrival!  Please ask your host family about arrival dates prior to booking airline tickets to Spokane. They will be meeting you at the airport, but would like to coordinate a suitable date.  </li>";
                strBody += "<li>Once your airline flight has been arranged, please send a copy of your flight itinerary to the Homestay Office:  InternationalHomestay@ccs.spokane.edu so that our staff can prepare arrangements with your host family for airport pick-up. Please ensure that flights terminate before 9:00 p.m. in Spokane.</li>";
                strBody += "<li>We look forward to meeting you at the Homestay Success Meeting!  This mandatory meeting will take place about one week after school starts.  Please be sure you attend so that we can help you develop a positive experience in your new homestay family.</li></ul></p>";
                strBody += "<p>Please contact the Community Colleges of Spokane - International Homestay Program at the following email address if you have further questions:<br />";
                strBody += "<a href='mailto:Internationalhomestay@ccs.spokane.edu'>Internationalhomestay@ccs.spokane.edu</a></p>";
     /* RESTORE WHEN NO LONGER MANUALLY ENTERING APPLICATIONS!!! */
                   strEmailResult = uEmail.SendEmailMsg(sendTo, sentFrom, strSubject, true, strBody, strCCaddresses);

                if (strEmailResult == "Success")
                {
                    try
                    {
                        //update email sent bit flag
     /* RESTORE WHEN NO LONGER MANUALLY ENTERING APPLICATIONS!!! */
                        intResult = hsInfo.UpdateMailSentStudent(intApplicantID, 1);
                    }
                    catch (Exception crap)
                    {
                        Response.Write("error: " + crap.Message + "<br />" + crap.StackTrace);
                    }
                    // Display confirmation

                    divUploadCCSConductUA.Visible = false;
                    divNoticesWaiverParentUA.Visible = false;
                    divConfirmation.Visible = true;
                    div1.Visible = false;
                    div2.Visible = false;
                    div3.Visible = false;
                    divButtons.Visible = false;
                    divInstructions.Visible = false;
                }
                else
                {
                    // error sending email so let user know
                    litResultMsg.Text = strEmailResult;
                    //return;
                }
            }
            else if (sendTo == "" && !blEmailSent) // no send to email address and SubmittedEmailSent = false so want to send
            {
                //sendTo = "Internationalhomestay@ccs.spokane.edu";
                sendTo = "laura.padden@ccs.spokane.edu";
                strSubject = "Problem sending homestay student applicaiton form submission email";
                strBody = "";
                strBody = "<p>Submission for " + strFirstName + " " + strLastName + " contained no email address. </p>";
                strEmailResult = uEmail.SendEmailMsg(sendTo, sentFrom, strSubject, true, strBody, strCCaddresses);

                if (strEmailResult == "Success")
                {
                    // Display confirmation
                    divUploadCCSConductUA.Visible = false;
                    divNoticesWaiverParentUA.Visible = false;
                    divConfirmation.Visible = true;
                    div1.Visible = false;
                    div2.Visible = false;
                    div3.Visible = false;
                    divButtons.Visible = false;
                    divInstructions.Visible = false;
                }
                else
                {
                    // error sending email so let user know
                    litResultMsg.Text = strEmailResult;
                    //return;
                }
            }// end no email address supplied
        } else {
            // email already sent just go onto confirmation page
            // include messages if uploads not complete
            // Display confirmation
            divUploadCCSConductUA.Visible = false;
            divNoticesWaiverParentUA.Visible = false;
            divConfirmation.Visible = true;
            div1.Visible = false;
            div2.Visible = false;
            div3.Visible = false;
            divButtons.Visible = false;
            divInstructions.Visible = false;
        }


        
    }

    protected void btnCmdBack_Click(object sender, EventArgs e)
    {
        if (hdnPartNo.Value.Equals("3"))
        {
            btnCmdContinuePart2_Click(sender, e);
        }
        else if (hdnPartNo.Value.Equals("2"))
        {
            Response.Redirect("AgreementsUAp1.aspx?ID=" + applicantID);
        }
        else if (hdnPartNo.Value.Equals("1"))
        {
            Response.Redirect("PlacementProfile_L.aspx?ID=" + applicantID + "&UA=t");
        }
    }

    protected void btnCmdUploadCCSConductUA_Click(object sender, EventArgs e)
    {
        eCCSConductUAFile.InnerText = "";
        if (uplCCSConductUAFile.HasFile)
        {
            if (Utility.ContainsNonEnglishChars(uplCCSConductUAFile.FileName))
            {
                //Response.Write(uplWaiveLiabilityParentUAFile.FileName);
                eCCSConductUAFile.InnerText = "You have selected a file containing invalid characters. Please rename the file with only English characters!";
                return;
            }
            strFileName = strLastName + "-" + intApplicantID + "-CCSConductUA-" + uplCCSConductUAFile.FileName;
            uplCCSConductUAFile.SaveAs(strUploadURL + strFileName);
            strResultMsg += "Your agreement, <span style=\"color:red;\">" + strFileName + "</span>, was successfully uploaded!<br />";
            //Insert record in UploadedFilesInformation table
            intResult = hsInfo.InsertUploadedFilesInformation(intApplicantID, strFirstName + " " + strLastName, strFileName, strUploadURL);
            blCCSConductUASubmitted = true;
            //Update status of submission
            intResult = hsInfo.UpdateAgreementSubmittedHomestayStudentInfo(intApplicantID, 1, "CCSConductUA");
        }
        else if (hdnCCSConductUASubmitted.Value.ToLower() == "true")
        {
            //previous submission exists
            blCCSConductUASubmitted = true;
        }
        getFileInfo();
        //set visibility - don't go anywhere
        divUploadCCSConductUA.Visible = true;
        btnContinue3.Visible = true;
        btnPrevious.Visible = true;
        div2.Visible = true;
        div1.Visible = false;
        div3.Visible = false;
        divConfirmation.Visible = false;
        divUploadWaiveLiabilityParentUA.Visible = false;
        btnDone.Visible = false;
        btnContinue2.Visible = false;
        btnCmdStudentAgreement.Focus();
    }// end btnCmdUploadCCSConductUA

    protected void btnCmdUploadNotices_Click(object sender, EventArgs e)
    {
        eNoticesWaiverParentUAFile.InnerText = "";
        if (uplNoticesWaiverParentUAFile.HasFile)
        {
            if (Utility.ContainsNonEnglishChars(uplNoticesWaiverParentUAFile.FileName))
            {
                //Response.Write(uplWaiveLiabilityParentUAFile.FileName);
                eNoticesWaiverParentUAFile.InnerText = "You have selected a file containing invalid characters. Please rename the file with only English characters!";
                return;
            }
            strFileName = strLastName + "-" + intApplicantID + "-NoticesWaiverParentUA-" + uplNoticesWaiverParentUAFile.FileName;
            uplNoticesWaiverParentUAFile.SaveAs(strUploadURL + strFileName);
            strResultMsg += "Your agreement, <span style=\"color:red;\">" + strFileName + "</span>, was successfully uploaded!<br />";
            //Insert record in UploadedFilesInformation table
            intResult = hsInfo.InsertUploadedFilesInformation(intApplicantID, strFirstName + " " + strLastName, strFileName, strUploadURL);
            blNoticesWaiverParentUASubmitted = true;
            //Update status of submission
            intResult = hsInfo.UpdateAgreementSubmittedHomestayStudentInfo(intApplicantID, 1, "NoticesWaiverParentUA");
        }

        getFileInfo();
        btnCmdSaveNoticesAndWaiver.Focus();
    }

    void getFileInfo()
    {
        dtHomestayStudent = hsInfo.GetOneHomestayStudent(intApplicantID);
                if (dtHomestayStudent != null)
                {
                    if (dtHomestayStudent.Rows.Count > 0)
                    {
                        blWaiveLiabilityParentUASubmitted = Convert.ToBoolean(dtHomestayStudent.Rows[0]["WaiveLiabilityParentUASubmitted"]);
                        blCCSConductUASubmitted = Convert.ToBoolean(dtHomestayStudent.Rows[0]["CCSConductUASubmitted"]);
                        blNoticesWaiverParentUASubmitted = Convert.ToBoolean(dtHomestayStudent.Rows[0]["NoticesWaiverParentUASubmitted"]);
                        if(Convert.ToBoolean(dtHomestayStudent.Rows[0]["agreeWaiveLiabilityParentUA"]))
                            chkWaiveLiabilityParentUA.Checked = true;
                        if (Convert.ToBoolean(dtHomestayStudent.Rows[0]["agreeCCSConductUA"]))
                            chkCCSConductUA.Checked = true;
                        if (Convert.ToBoolean(dtHomestayStudent.Rows[0]["agreeNoticesWaiverParentUA"]))
                            chkNoticesWaiverParentUA.Checked = true;
                        blEmailSent = Convert.ToBoolean(dtHomestayStudent.Rows[0]["SubmittedEmailSent"]);
                    }
                }
            
            if (blWaiveLiabilityParentUASubmitted || blCCSConductUASubmitted || blNoticesWaiverParentUASubmitted)
            {
                //get the files already uploaded and display
                Configuration config = WebConfigurationManager.OpenWebConfiguration(Request.ApplicationPath);
                AppSettingsSection appSettings = (AppSettingsSection)config.GetSection("appSettings");
                String strUploadServer = "/InternetContent" + appSettings.Settings["HomestayShare"].Value;
                litWaiveLiabilityParentUAFile.Text = "";
                litCCSConductUAFile.Text = "";
                litNoticesWaiverParentUAFile.Text = "";
                DataTable dt = hsInfo.GetUploadedFiles(intApplicantID);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            strFileName = dr["fileName"].ToString();
                        string filePath = dr["filePath"].ToString();
                        string uploadServer = "";
                        if (filePath.Contains("ccs-internet"))
                        {
                            uploadServer += @"http:\\apps.spokane.edu\InternetContent\Homestay\";
                        }
                        else
                        {
                            uploadServer += @"http:\\portal.ccs.spokane.edu\InternetContent\Homestay\";
                        }
                        if (strFileName.Contains("WaiveLiabilityParentUA"))
                            {
                                litWaiveLiabilityParentUAFile.Text += "<br /><b>Waiver of Liability (Parents) submitted:</b> " + "<a href=\"" + uploadServer + strFileName + "\" target=\"_blank\">" + strFileName + "</a>";
                                strWaiveLiabilityParentUA = strFileName.Trim();
                            }
                            if (strFileName.Contains("CCSConductUA"))
                            {
                                litCCSConductUAFile.Text += "<br /><b>CCS Conduct Agreement submitted:</b> " + "<a href=\"" + uploadServer + strFileName + "\" target=\"_blank\">" + strFileName + "</a>";
                                strCCSConductUA = strFileName.Trim();
                            }
                            if (strFileName.Contains("NoticesWaiverParentUA"))
                            {
                                litNoticesWaiverParentUAFile.Text += "<br /><b>Notices and Waiver of Liability (Parents) submitted:</b> " + "<a href=\"" + uploadServer + strFileName + "\" target=\"_blank\">" + strFileName + "</a>";
                                strNoticesWaiverParentUA = strFileName.Trim();
                            }
                        }
                    }
                }
            }// end some or all files submitted
    }// end get file info
    protected void btnCmdReturnAdminHome_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Homestay/Admin");
    }
}// end class

