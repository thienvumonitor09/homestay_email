﻿<%@ Page Title="Matching" Language="C#" MasterPageFile="~/CCSApps.master" AutoEventWireup="true" CodeFile="MatchPreferences2.aspx.cs" Inherits="Homestay_Admin_MatchPreferences2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="bannerH1Text" Runat="Server">
    Match Preferenes
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">
     <asp:Button ID="btnReset" runat="server" Text="Reset" OnClick="btnReset_Click" />
    
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <h3>Select a Student</h3>
            <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
            
            <asp:DropDownList ID="ddlStudents" runat="server" AppendDataBoundItems="true" 
                AutoPostBack="true">

            </asp:DropDownList>
             
            <br />
            <h3>Level of Matching</h3>
            <div style="width:100%;float:left; margin-bottom:10px">
                <div style="float:left;width:10%;">
                    <label for="rdoGender"><strong>Gender</strong></label>
                    <asp:RadioButtonList ID="rdoGender" RepeatDirection="Horizontal" runat="server">
                        <asp:ListItem Value="Include" Selected="True"></asp:ListItem>
                        <asp:ListItem Value="Omit"></asp:ListItem>
                    </asp:RadioButtonList>
                </div>
                <div style="float:left;width:10%;">
                     <label for="rdoHomestay"><strong>Preference</strong> </label>
                    <asp:RadioButtonList ID="rdoHomestay" RepeatDirection="Horizontal" runat="server">
                        <asp:ListItem Value="Include" Selected="True"></asp:ListItem>
                        <asp:ListItem Value="Omit"></asp:ListItem>
                    </asp:RadioButtonList>
                </div>
                <div style="float:left;width:80%;">
                     <label for="rdoSmoking"><strong>Smoking Habits</strong></label>
                    <asp:RadioButtonList ID="rdoSmoking" RepeatDirection="Horizontal" runat="server">
                        <asp:ListItem Value="Include" Selected="True"></asp:ListItem>
                        <asp:ListItem Value="Omit"></asp:ListItem>
                    </asp:RadioButtonList>
                </div>
            </div>
           
           <br />
            
            <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" OnClientClick="return ValidateSelectStudent();"/>
        </ContentTemplate>

       

    </asp:UpdatePanel>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" DynamicLayout="true" AssociatedUpdatePanelID="UpdatePanel1" DisplayAfter="500" >

       <ProgressTemplate>
          <img src="../images/demo_wait.gif" />
           Please Wait...
       </ProgressTemplate>
   
    </asp:UpdateProgress>
    <br />
    
    
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
           <h3>Current Status</h3>
            <asp:Literal ID="litCurrStudentStatus" runat="server" Text=""></asp:Literal>
           <h3>Families that Match</h3>
            <asp:Label ID="litRoomWithOtherFamily" runat="server" Text=""></asp:Label>
            <asp:Literal ID="litFoundFamily" runat=server></asp:Literal>   
            <br />
             <div style="margin-bottom:10px;width:40%;float:left;"> 
                  <asp:DropDownList ID="ddlFamilies" runat="server" Width="200px" >
               
                    </asp:DropDownList>
                 <asp:Label ID="litFamilyPlaced" runat="server" Text=""></asp:Label>
                 <asp:Button ID="btnCompare" runat="server" Text="Compare"  OnClientClick="return ValidateSelectFamily();" OnClick="btnCompare_Click"/>
             </div> 
           
             <div id="divRoomPlacement" runat="server" style="margin-bottom:10px;width:70%;float:left;">
                <asp:Table ID="roomStatus" runat="server" Width="90%" BorderWidth="0px">
            <asp:TableHeaderRow>
                <asp:TableCell Width="10%"><b>Room</b></asp:TableCell>
                <asp:TableCell Width="20%"><b>Status</b></asp:TableCell>
                <asp:TableCell Width="20%"><b>Placement</b></asp:TableCell>
                <asp:TableCell Width="15%"><b>Effective Quarter</b></asp:TableCell>
                <asp:TableCell Width="15%"><b>Action</b></asp:TableCell>
            </asp:TableHeaderRow>
            <asp:TableRow>
                <asp:TableCell Width="10%">Room 1</asp:TableCell>
                <asp:TableCell ID="rm1Status" Width="20%"><asp:Literal ID="litRm1Status" runat="server"></asp:Literal></asp:TableCell>
                <asp:TableCell ID="rm1Placement" Width="20%">
                    <asp:DropDownList ID="ddlRm1Placement" runat="server">
                    <asp:ListItem Value="--Select one--"></asp:ListItem>
                       
                        <asp:ListItem Value="Tentative Placement">Tentative Placement</asp:ListItem>
                        <asp:ListItem Value="Student Placed">Student Placed</asp:ListItem>
                        <asp:ListItem Value="Withdrew">Withdrew</asp:ListItem>
                        <asp:ListItem Value="Graduated">Graduated</asp:ListItem>
                        <asp:ListItem Value="Student Moved Out">Student Moved Out</asp:ListItem>
                  </asp:DropDownList>
                </asp:TableCell>
                <asp:TableCell ID="rm1EffectiveDate" Width="15%">
                     <asp:DropDownList ID="ddlEffectiveQtr1"  runat="server" ></asp:DropDownList>
                </asp:TableCell>
                <asp:TableCell Width="15%">
                    <asp:Button ID="btnUpdateRm1" runat="server" Text="Update Room 1" OnClick="btnUpdateRm1_Click"/>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell Width="10%">Room 2</asp:TableCell>
                <asp:TableCell ID="rm2Status" Width="20%"><asp:Literal ID="litRm2Status" runat="server"></asp:Literal></asp:TableCell>

                <asp:TableCell ID="rm2Placement" Width="20%">
                    <asp:DropDownList ID="ddlRm2Placement" runat="server">
                        <asp:ListItem Value="--Select one--"></asp:ListItem>
                        
                        <asp:ListItem Value="Tentative Placement"></asp:ListItem>
                        <asp:ListItem Value="Student Placed"></asp:ListItem>
                        <asp:ListItem Value="Withdrew"></asp:ListItem>
                        <asp:ListItem Value="Graduated"></asp:ListItem>
                        <asp:ListItem Value="Student Moved Out"></asp:ListItem>
                  </asp:DropDownList>
                </asp:TableCell>
                <asp:TableCell ID="rm2EffectiveDate" Width="15%">
                    <asp:DropDownList ID="ddlEffectiveQtr2"  runat="server" ></asp:DropDownList>
                </asp:TableCell>
                <asp:TableCell Width="15%">
                    <asp:Button ID="btnUpdateRm2" runat="server" Text="Update Room 2" OnClick="btnUpdateRm2_Click"/>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell Width="10%">Room 3</asp:TableCell>
                <asp:TableCell ID="rm3Status" Width="20%"><asp:Literal ID="litRm3Status" runat="server"></asp:Literal></asp:TableCell>

                <asp:TableCell ID="rm3Placement" Width="20%">
                    <asp:DropDownList ID="ddlRm3Placement" runat="server">
                    <asp:ListItem Value="--Select one--"></asp:ListItem>
                       
                        <asp:ListItem Value="Tentative Placement"></asp:ListItem>
                        <asp:ListItem Value="Student Placed"></asp:ListItem>
                        <asp:ListItem Value="Withdrew"></asp:ListItem>
                        <asp:ListItem Value="Graduated"></asp:ListItem>
                        <asp:ListItem Value="Student Moved Out"></asp:ListItem>
                  </asp:DropDownList>
                </asp:TableCell>
                <asp:TableCell ID="rm3EffectiveDate" Width="15%">
                    <asp:DropDownList ID="ddlEffectiveQtr3"  runat="server" ></asp:DropDownList>
                </asp:TableCell>
                <asp:TableCell Width="15%"><b>Action</b></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell Width="10%">Room 4</asp:TableCell>
                <asp:TableCell ID="rm4Status" Width="20%"><asp:Literal ID="litRm4Status" runat="server"></asp:Literal></asp:TableCell>

                <asp:TableCell ID="rm4Placement" Width="20%">
                    <asp:DropDownList ID="ddlRm4Placement" runat="server">
                    <asp:ListItem Value="--Select one--"></asp:ListItem>
                       
                        <asp:ListItem Value="Tentative Placement"></asp:ListItem>
                        <asp:ListItem Value="Student Placed"></asp:ListItem>
                        <asp:ListItem Value="Withdrew"></asp:ListItem>
                        <asp:ListItem Value="Graduated"></asp:ListItem>
                        <asp:ListItem Value="Student Moved Out"></asp:ListItem>
                  </asp:DropDownList>
                </asp:TableCell>
                <asp:TableCell ID="rm4EffectiveDate" Width="15%">
                    <asp:DropDownList ID="ddlEffectiveQtr4"  runat="server" ></asp:DropDownList>
                </asp:TableCell>
                <asp:TableCell Width="15%"><b>Action</b></asp:TableCell>
            </asp:TableRow>
        </asp:Table>

        
            </div>
             <div style="width:20%">
                <asp:Button ID="btnPlace" runat="server" Text="Place Student with selected family" OnClick="btnPlace_Click" />
                  <asp:Label ID="litPlace" runat="server" Text=""></asp:Label>
            </div>
            <br />
             
             <br /><br />
          
             <div style="width:100%; float:right;">
                 <div style="width:50%; float:left;">
                    
                 </div>
                 <div style="width:50%; float:left;">
                    
                 </div>
             </div>
            <table style="width: 80%;">
                <tr>
                    <td style=" vertical-align:top">
                        <asp:Literal ID="litSelectedStuInfo" runat=server></asp:Literal>  
                    </td>
                    <td style="vertical-align:top">
                        <asp:Literal ID="litFamilyInfo" runat=server></asp:Literal>  
                    </td>
                </tr>
                
                
            </table>
            <asp:Literal ID="litResultMsg" runat=server></asp:Literal>   
        </ContentTemplate>

    </asp:UpdatePanel>

    

    
    <script type="text/javascript">
        function ValidateSelectStudent() {
            var ddl = $('#<%=ddlStudents.ClientID%>');
            var selectedValue = ddl.val();
  
            //alert(selectedValue); //SelVal is the selected Value
            if (selectedValue == 0) {
                 alert("Please select a student to search");
                return false;
            }
            return true;
        }

         function ValidateSelectFamily() {
            var ddl = $('#<%=ddlFamilies.ClientID%>');
            var selectedValue = ddl.val();
  
            //alert(selectedValue); //SelVal is the selected Value
            if (selectedValue == 0) {
                 alert("Please select a family to compare");
                return false;
            }
            return true;
        }
        $(function () {
            
            $('#<%=ddlRm1Placement.ClientID %>').change(function() {
                alert( "Handler for .change() called." );
            });
        });
        
        
    </script>
    </asp:Content>