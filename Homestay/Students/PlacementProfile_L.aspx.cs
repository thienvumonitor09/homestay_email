﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Homestay_PlacementProfile_L : System.Web.UI.Page
{
    String strButtonFace = "";
    String applicantID = "";
    String strResultMsg = "";
    Boolean blUnderAge = false;
    Boolean blAddMode = true;
    //Preferences
    Int32 intResult = 0;
    String strBlank = "";
    Int32 intApplicantID = 0;
    String strRelationship = "";
    
    Boolean blIsAdmin = false;
    Boolean isValidDate = false;
    DateTime outDate;               // used in try/catch to validate dob on form
    Boolean dateSuccess = false;    // used in try/catch to validate dob on form

    string strTravel;

    Homestay hsInfo = new Homestay();
    protected void Page_Load(object sender, EventArgs e)
    {
        String strLogonUser = Request.ServerVariables["LOGON_USER"];
        if (strLogonUser != "" && strLogonUser != null) { blIsAdmin = true; }

        if (Request.Form["btnSubmit"] != null)
        {
            strButtonFace = Request.Form["btnSubmit"].ToString();
        }
        if (Request.QueryString["ID"] != null)
        {
            //page load
            applicantID = Request.QueryString["ID"].ToString();
            hdnApplicantID.Value = applicantID;
        }
        if (applicantID == "")
        {
            //after postback
            applicantID = hdnApplicantID.Value;
        }
        //strResultMsg += "applicantID: " + applicantID + "<br />";
        if (applicantID != "0" && applicantID != "" && applicantID != null)
        {
            intApplicantID = Convert.ToInt32(applicantID);
        }
        if (intApplicantID <= 0)
        {
            strResultMsg += "Unable to Update Homestay Student Info: applicant ID not set.<br />";
            /*
            switch (strButtonFace)
            {
                case "Save":
                case "Save Changes":
                    
                    break;
                case "Continue":
                    if (blUnderAge)
                    {
                        Response.Redirect("AgreementsUA.aspx?ID=" + hdnApplicantID.Value);
                    }
                    else
                    {
                        Response.Redirect("Agreements18.aspx?ID=" + hdnApplicantID.Value);
                    }
                    break;
                case "Back":
                    Response.Redirect("ApplicationForm.aspx?ID=" + hdnApplicantID.Value);
                    break;
            }
             */
            
        }

        if (!IsPostBack)
        {
            //LOAD PAGE WITH EXISTING RECORD, IF ANY
            DataTable dtInfo = hsInfo.GetOneHomestayStudent(intApplicantID);
            if (dtInfo != null)
            {
                if (dtInfo.Rows.Count > 0)
                {
                    traveledWhere.Text = dtInfo.Rows[0]["traveledOutsideCountry"].ToString().Trim();
                    smokingHabits.Text = dtInfo.Rows[0]["smokingHabits"].ToString().Trim();
                    activitiesEnjoyed.Text = dtInfo.Rows[0]["activitiesEnjoyed"].ToString().Trim();
                    allergies.Text = dtInfo.Rows[0]["allergies"].ToString().Trim();
                    homeEnvironmentPreferences.Text = dtInfo.Rows[0]["homeEnvironmentPreferences"].ToString().Trim();
                    healthConditions.Text = dtInfo.Rows[0]["healthConditions"].ToString().Trim();
                    rdoHealthStatus.SelectedValue = dtInfo.Rows[0]["healthStatus"].ToString().Trim();
                    Boolean blDriveCar = Convert.ToBoolean(dtInfo.Rows[0]["driveCar"]);
                    if (blDriveCar)
                    {
                        rdoDriveCar.SelectedValue = "1";
                    }
                    else
                    {
                        rdoDriveCar.SelectedValue = "0";
                    }
                    anythingElse.Text = dtInfo.Rows[0]["anythingElse"].ToString().Trim();
                }
            }
            DataTable dtRelatives = hsInfo.GetHomestayRelatives(intApplicantID, 0);
            if (dtRelatives != null)
            {
                if (dtRelatives.Rows.Count > 0)
                {
                    foreach (DataRow dtRow in dtRelatives.Rows)
                    {
                        blAddMode = false;
                        strRelationship = dtRow["relationship"].ToString().Trim();
                        //strResultMsg += "Relationship: " + strRelationship + "<br />";
                        switch (strRelationship)
                        {
                            case "Father":
                                familyNameFather.Text = dtRow["familyName"].ToString().Trim();
                                firstNameFather.Text = dtRow["firstName"].ToString().Trim();
                                occupationFather.Text = dtRow["occupation"].ToString().Trim();
                                hdnFatherID.Value = dtRow["id"].ToString();
                                break;
                            case "Mother":
                                familyNameMother.Text = dtRow["familyName"].ToString().Trim();
                                firstNameMother.Text = dtRow["firstName"].ToString().Trim();
                                occupationMother.Text = dtRow["occupation"].ToString().Trim();
                                hdnMotherID.Value = dtRow["id"].ToString();
                                break;
                            case "Sibling 1":
                                familyNameSibling1.Text = dtRow["familyName"].ToString().Trim();
                                firstNameSibling1.Text = dtRow["firstName"].ToString().Trim();
                                dobSibling1.Text = Convert.ToDateTime(dtRow["DOB"]).ToShortDateString();
                                genderSibling1.Text = dtRow["gender"].ToString().Trim();
                                hdnSibling1ID.Value = dtRow["id"].ToString();
                                ageSibling1.Text = "";
                                if (!Convert.IsDBNull(dtRow["age"]))
                                {
                                    ageSibling1.Text = dtRow["age"].ToString();
                                }
                                break;
                            case "Sibling 2":
                                familyNameSibling2.Text = dtRow["familyName"].ToString().Trim();
                                firstNameSibling2.Text = dtRow["firstName"].ToString().Trim();
                                dobSibling2.Text = Convert.ToDateTime(dtRow["DOB"]).ToShortDateString();
                                genderSibling2.Text = dtRow["gender"].ToString().Trim();
                                hdnSibling2ID.Value = dtRow["id"].ToString();
                                ageSibling2.Text = "";
                                if (!Convert.IsDBNull(dtRow["age"]))
                                {
                                    ageSibling2.Text = dtRow["age"].ToString();
                                }
                                break;
                            case "Sibling 3":
                                familyNameSibling3.Text = dtRow["familyName"].ToString().Trim();
                                firstNameSibling3.Text = dtRow["firstName"].ToString().Trim();
                                dobSibling3.Text = Convert.ToDateTime(dtRow["DOB"]).ToShortDateString();
                                genderSibling3.Text = dtRow["gender"].ToString().Trim();
                                hdnSibling3ID.Value = dtRow["id"].ToString();
                                ageSibling3.Text = "";
                                if (!Convert.IsDBNull(dtRow["age"]))
                                {
                                    ageSibling3.Text = dtRow["age"].ToString();
                                }
                                break;
                            case "Sibling 4":
                                familyNameSibling4.Text = dtRow["familyName"].ToString().Trim();
                                firstNameSibling4.Text = dtRow["firstName"].ToString().Trim();
                                dobSibling4.Text = Convert.ToDateTime(dtRow["DOB"]).ToShortDateString();
                                genderSibling4.Text = dtRow["gender"].ToString().Trim();
                                hdnSibling4ID.Value = dtRow["id"].ToString();
                                ageSibling4.Text = "";
                                if (!Convert.IsDBNull(dtRow["age"]))
                                {
                                    ageSibling4.Text = dtRow["age"].ToString();
                                }
                                break;
                        }
                    }
                }
                else if (blIsAdmin) { strResultMsg += "No HomestayRelatives records returned.<br />"; }
            }
            else if (blIsAdmin) { strResultMsg += "No HomestayRelatives records returned.<br />"; }

            //Get preferences
            DataTable dtPref = hsInfo.GetHomestayPreferenceSelections(intApplicantID, 0);
            if (dtPref != null)
            {
                if (dtPref.Rows.Count > 0)
                {
                    foreach (DataRow dtPrefRow in dtPref.Rows)
                    {
                        String objID = dtPrefRow["fieldID"].ToString().Trim();
                        String objValue = dtPrefRow["fieldValue"].ToString().Trim();
                        if (objID.Substring(0, 3).Contains("rdo"))
                        {
                            //Radio Button
                            //declare specific type of control
                            //RadioButtonList myRDO = (RadioButtonList)Page.Master.FindControl("MainContent").FindControl("fldPreferences").FindControl(objID);
                            RadioButtonList myRDO = (RadioButtonList)FindControl("fldPreferences").FindControl(objID);
                            if (myRDO != null)
                            {
                                //set value
                                myRDO.SelectedValue = objValue;
                            }
                            else if (blIsAdmin) { strResultMsg += "Radio button list " + objID + " with a value of " + objValue + " is null.<br />"; }
                        }
                        else
                        {
                            //Checkbox
                            //declare specific type of control
                            //CheckBox myControl = (CheckBox)Page.Master.FindControl("MainContent").FindControl("fldPreferences").FindControl(objID);
                            CheckBox myControl = (CheckBox)FindControl("fldPreferences").FindControl(objID);
                            if (myControl != null)
                            {
                                //set value
                                myControl.Checked = true;
                            }
                            else if (blIsAdmin) { strResultMsg += "Checkbox " + objID + " with a value of " + objValue + " is null.<br />"; }
                        }
                    }
                }
                else if (blIsAdmin) { strResultMsg += "Preference Selections returned no rows.<br />"; }
            }
            else if (blIsAdmin) { strResultMsg += "Preference Selections table is null<br />"; }
        }// end not a postback

        //Verify age
        DataTable dtStudent = hsInfo.GetHomestayStudents("ID", intApplicantID);
        if (dtStudent != null && dtStudent.Rows.Count > 0)
        {
            //clear any values from previous selection
            DateTime dtDOB = Convert.ToDateTime(dtStudent.Rows[0]["DOB"]);
            DateTime dtFirstDayOfClass = Convert.ToDateTime(dtStudent.Rows[0]["quarterStartDate"]);
            DateTime dtLatestDOB = dtFirstDayOfClass.AddYears(-18);
            if (dtLatestDOB < dtDOB)
            {
                blUnderAge = true;
                divApartment.Visible = false;
            }
        }
        
        //Visibility settings
        if (blAddMode)
        {
            divUpdate.Visible = false;
            btnSave.Visible = true;
            if (!blIsAdmin)
            {
                divNextPage.Visible = false;
                btnPrevious.Visible = false;
            }
        }
        else
        {
            if (blIsAdmin)
            {
                divUpdate.Visible = true;
                btnPrevious.Visible = true;
            }
            btnSave.Visible = false;
            divNextPage.Visible = true;
        }
        litResultMsg.Text = "<p>" + strResultMsg + hsInfo.strResultMsg + "</p>";
    }

#region Export to PDF
    protected void btnExport_Click(object sender, EventArgs e)
    {
        Document pdfDoc = new Document(PageSize.A4, 25, 10, 25, 10);
        PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
        pdfDoc.Open();
        BaseFont bf = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
        //Font font1 = new Font(bf, 16, Font.NORMAL, Color.BLUE);
        Font font3 = new Font(bf, 15, Font.BOLD);
        Paragraph Text = new Paragraph("Student Profile", font3);
        pdfDoc.Add(Text);
        string familyNameStr = "";
        string firstNameStr = "";
        DataTable dtRelatives = hsInfo.GetHomestayRelatives(intApplicantID, 0);
        if (dtRelatives != null)
        {
            if (dtRelatives.Rows.Count > 0)
            {
                foreach (DataRow dtRow in dtRelatives.Rows)
                {
                    strRelationship = dtRow["relationship"].ToString().Trim();
                    Text = new Paragraph(strRelationship, font3);
                    pdfDoc.Add(Text);
                    familyNameStr = dtRow["familyName"].ToString();
                    Text = new Paragraph("Family Name: "  + dtRow["familyName"].ToString());
                    pdfDoc.Add(Text);
                    firstNameStr = dtRow["firstName"].ToString();
                    Text = new Paragraph("First Name: " + dtRow["firstName"].ToString());
                    pdfDoc.Add(Text);
                    

                    switch (strRelationship)
                    {
                        case "Father": case "Mother":
                            Text = new Paragraph("Occupation: " + dtRow["occupation"].ToString());
                            pdfDoc.Add(Text);
                            break;
                        case "Sibling 1": case "Sibling 2": case "Sibling 3": case "Sibling 4":
                            Text = new Paragraph("Date of Birth: " + Convert.ToDateTime(dtRow["DOB"]).ToShortDateString());
                            pdfDoc.Add(Text);
                            Text = new Paragraph("Gender: " + dtRow["gender"].ToString());
                            pdfDoc.Add(Text);
                            break;
                    }
                }
            }
        }

        Text = new Paragraph("Preferences", font3);
        pdfDoc.Add(Text);

        #region Home Environment
        Text = new Paragraph("Home Environment:", font3);
        pdfDoc.Add(Text);
        DataTable dtPref = hsInfo.GetHomestayPreferenceSelections(intApplicantID, 0);
        DataTable dtInfo = hsInfo.GetOneHomestayStudent(intApplicantID);
        bool dtIndoExist = false;
        if (dtInfo != null)
        {
            if (dtInfo.Rows.Count > 0)
            {
                dtIndoExist = true;
            }
        }
        if (dtPref != null)
        {
            if (dtPref.Rows.Count > 0)
            {
                DataRow[] foundRow = null;

                //rdoSmallChildren
                foundRow = dtPref.Select("fieldID = 'rdoSmallChildren'");
                Text = new Paragraph("I would like to live in a home with: " 
                                + foundRow[0]["fieldValue"].ToString());
                pdfDoc.Add(Text);

                //rdoTraveledOutsideCountry
                foundRow = dtPref.Select("fieldID = 'rdoTraveledOutsideCountry'");
                Text = new Paragraph("Have you traveled outside your country before? "
                                    +  foundRow[0]["fieldValue"].ToString() ) ;
                pdfDoc.Add(Text);
                if (dtIndoExist)
                {
                    Text = new Paragraph("Which countries? " + dtInfo.Rows[0]["traveledOutsideCountry"].ToString());
                    pdfDoc.Add(Text);
                }

                //rdoSmoker
                foundRow = dtPref.Select("fieldID = 'rdoSmoker'");
                Text = new Paragraph("Do you smoke? "
                                    + (foundRow[0]["fieldValue"].ToString().Contains("Not") ? "No" : "Yes"));
                pdfDoc.Add(Text);
                if (dtIndoExist)
                {
                    Text = new Paragraph("Explain your smoking habits:  " + dtInfo.Rows[0]["smokingHabits"].ToString());
                    pdfDoc.Add(Text);
                }

                //rdoOtherSmokerOK
                foundRow = dtPref.Select("fieldID = 'rdoOtherSmokerOK'");
                Text = new Paragraph("Is it okay if there are smokers in your homestay family ? "
                                    + foundRow[0]["fieldValue"].ToString());
                pdfDoc.Add(Text);

                //rdoInteraction
                foundRow = dtPref.Select("fieldID = 'rdoInteraction'");
                Text = new Paragraph("How much interaction and conversation with your homestay family do you prefer? "
                                    + foundRow[0]["fieldValue"].ToString());
                pdfDoc.Add(Text);

                //rdoInteraction
                foundRow = dtPref.Select("fieldID = 'rdoHomeEnvironment'");
                Text = new Paragraph("I would like to live in a home that is more: "
                                    + foundRow[0]["fieldValue"].ToString());
                pdfDoc.Add(Text);
                if (dtIndoExist)
                {
                    Text = new Paragraph("Explain:  " + dtInfo.Rows[0]["homeEnvironmentPreferences"].ToString());
                    pdfDoc.Add(Text);
                }
            }
        }
#endregion

        #region Hobbies
        Text = new Paragraph("Hobbies", font3);
        pdfDoc.Add(Text);
        List list = new List(List.UNORDERED, 10f);
        list.SetListSymbol("\u2022");
        list.IndentationLeft = 30f;

        string[] hobbies = { "MusicalInstrument", "Art" ,"TeamSports", "IndividualSports" ,"ListenMusic","Drama","WatchMovies",
                                "Singing","Shopping","ReadBooks","Outdoors","Cooking", "Photography", "Gaming"};
        foreach (DataRow dtFamPrefRow in dtPref.Rows)
        {
            //Text = new Paragraph(dtFamPrefRow["fieldID"].ToString().Trim() +" " + dtFamPrefRow["fieldValue"].ToString().Trim());
            //pdfDoc.Add(Text);
            string fieldIDStr = dtFamPrefRow["fieldID"].ToString().Trim();
            string fieldValueStr = dtFamPrefRow["fieldValue"].ToString().Trim();
            if (Array.Exists(hobbies, element => element == fieldValueStr))
            {
                list.Add(dtFamPrefRow["Preference"].ToString().Trim());
            }
        }

        list.Add("Other hobbies/activities you enjoy: " + dtInfo.Rows[0]["activitiesEnjoyed"].ToString());
        pdfDoc.Add(list);
        #endregion

        #region Diet
        Text = new Paragraph("Diet", font3);
        pdfDoc.Add(Text);
        string[] diets = { "Vegetarian", "GlutenFree", "DairyFree", "OtherFoodAllergy", "Halal", "Kosher", "NoSpecialDiet" };
        list = new List(List.UNORDERED, 10f);
        list.SetListSymbol("\u2022");
        list.IndentationLeft = 30f;
        foreach (DataRow dtFamPrefRow in dtPref.Rows)
        {
            //Text = new Paragraph(dtFamPrefRow["fieldID"].ToString().Trim() +" " + dtFamPrefRow["fieldValue"].ToString().Trim());
            //pdfDoc.Add(Text);
            string fieldIDStr = dtFamPrefRow["fieldID"].ToString().Trim();
            string fieldValueStr = dtFamPrefRow["fieldValue"].ToString().Trim();
            if (Array.Exists(diets, element => element == fieldValueStr))
            {
                list.Add(dtFamPrefRow["Preference"].ToString().Trim());
            }
        }
        pdfDoc.Add(list);
        #endregion

        #region Other Concerns
        Text = new Paragraph("Other Concerns", font3);
        pdfDoc.Add(Text);

        Text = new Paragraph("Do you have any allergies to food, drugs, animals or anything else?");
        pdfDoc.Add(Text);
        Text = new Paragraph(dtInfo.Rows[0]["allergies"].ToString());
        pdfDoc.Add(Text);

        Text = new Paragraph("Do you have any health conditions that would prevent you from participation in normal and regular physical activities?");
        pdfDoc.Add(Text);
        Text = new Paragraph(dtInfo.Rows[0]["healthConditions"].ToString());
        pdfDoc.Add(Text);

        Text = new Paragraph("How would you describe your present condition of health? "
                            + dtInfo.Rows[0]["healthStatus"].ToString());
        pdfDoc.Add(Text);

        Text = new Paragraph("Are you planning to drive a car as soon as you arrive? "
                            + (dtInfo.Rows[0]["driveCar"].ToString().Equals("True") ? "Yes" : "No" ));
        pdfDoc.Add(Text);

        Text = new Paragraph("Is there anything else you'd like us to know about you?");
        pdfDoc.Add(Text);
        Text = new Paragraph(dtInfo.Rows[0]["anythingElse"].ToString());
        pdfDoc.Add(Text);
        #endregion

        pdfWriter.CloseStream = false;
        pdfDoc.Close();
        //string exportFileName = familyNameStr + "-" + firstNameStr + "-" + "Student Profile";
        string exportFileName = "Student Profile";
        Response.Buffer = true;
        Response.ContentType = "application/pdf";
        Response.AddHeader("content-disposition", "attachment;filename=" + exportFileName + ".pdf");
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.Write(pdfDoc);
        Response.End();
    }
#endregion
    protected void btnCmdSave_Click(object sender, EventArgs e)
    {
        if (familyNameFather.Text.Trim() == "")
        {
            eFFamName.InnerHtml = "Please enter your father's family name.";
            familyNameFather.Focus();
            return;
        }
        else
            eFFamName.InnerHtml = "";
        if (firstNameFather.Text.Trim() == "")
        {
            eFFirstName.InnerHtml = "Please enter your father's first name.";
            firstNameFather.Focus();
            return;
        }
        else
            eFFirstName.InnerHtml = "";
        if (occupationFather.Text.Trim() == "")
        {
            eFathOcc.InnerHtml = "Please enter your father's occupation.";
            occupationFather.Focus();
            return;
        }
        else
        {
            eFathOcc.InnerHtml = "";
        }
        /*
        if (familyNameMother.Text.Trim() == "")
        {
            eMFamName.InnerHtml = "Please enter your mother's family name.";
            familyNameMother.Focus();
            return;
        }
        else
        {
            eMFamName.InnerHtml = "";
        }
            
        if (firstNameMother.Text.Trim() == "")
        {
            eMFirstName.InnerHtml = "Please enter your mother's first name.";
            firstNameMother.Focus();
            return;
        }
        else
        {
            eMFirstName.InnerHtml = "";
        }
            
        if (occupationMother.Text.Trim() == "")
        {
            eMomOcc.InnerHtml = "Please enter your mother's occupation.";
            occupationMother.Focus();
            return;
        }
        else
        {
            eMomOcc.InnerHtml = "";
        }
        */

        // check sibling date of birth if entered
        if (dobSibling1.Text.Trim() != "")
        {
            isValidDate = checkDate(dobSibling1);
            if(!isValidDate)
            {
                eDOBSib1.InnerHtml = "Please enter a valid date of birth for Sibling 1.";
                litResultMsg.Text = "<span style='color:#dd0000'>Please enter a valid date of birth for Sibling 1 information</span>.";
                return;
            } else
            {
                eDOBSib1.InnerHtml = "";
                litResultMsg.Text = "";
            }
        }// entry in Sibling 1 DOB
        if (dobSibling2.Text.Trim() != "")
        {
            isValidDate = checkDate(dobSibling2);
            if (!isValidDate)
            {
                eDOBSib2.InnerHtml = "Please enter a valid date of birth for Sibling 2.";
                litResultMsg.Text = "<span style='color:#dd0000'>Please enter a valid date of birth for Sibling 2 information</span>.";
                return;
            }
            else
            {
                eDOBSib2.InnerHtml = "";
                litResultMsg.Text = "";
            }
        }// entry in Sibling 2 DOB
        if (dobSibling3.Text.Trim() != "")
        {
            isValidDate = checkDate(dobSibling3);
            if (!isValidDate)
            {
                eDOBSib3.InnerHtml = "Please enter a valid date of birth for Sibling 3.";
                litResultMsg.Text = "<span style='color:#dd0000'>Please enter a valid date of birth for Sibling 3 information</span>.";
                return;
            }
            else
            {
                eDOBSib3.InnerHtml = "";
                litResultMsg.Text = "";
            }
        }// entry in Sibling 3 DOB
        if (dobSibling4.Text.Trim() != "")
        {
            isValidDate = checkDate(dobSibling4);
            if (!isValidDate)
            {
                eDOBSib4.InnerHtml = "Please enter a valid date of birth for Sibling 4.";
                litResultMsg.Text = "<span style='color:#dd0000'>Please enter a valid date of birth for Sibling 4 information</span>.";
                return;
            }
            else
            {
                eDOBSib4.InnerHtml = "";
                litResultMsg.Text = "";
            }
        }// entry in Sibling 4 DOB

        if (rdoSmallChildren.SelectedIndex == -1)
        {
            eSmallChildren.InnerHtml = "Please indicate your preferences about children in the home.";
            rdoSmallChildren.Focus();
            return;
        }
        else
            eSmallChildren.InnerHtml = "";
        if (rdoTraveledOutsideCountry.SelectedIndex == -1)
        {
            eTravel.InnerHtml = "Please indicate if you have traveled outside your country before.";
            rdoTraveledOutsideCountry.Focus();
            return;
        }
        else
            eTravel.InnerHtml = "";
        if (rdoSmoker.SelectedIndex == -1)
        {
            eSmoker.InnerHtml = "Please indicate if you are a smoker or not.";
            rdoSmoker.Focus();
            return;
        }
        else
            eSmoker.InnerHtml = "";
        if (rdoOtherSmokerOK.SelectedIndex == -1)
        {
            eOtherSmokerOK.InnerHtml = "Please indicate your preference about other smokers in the home.";
            rdoOtherSmokerOK.Focus();
            return;
        }
        else
            eOtherSmokerOK.InnerHtml = "";
        if (rdoInteraction.SelectedIndex == -1)
        {
            eInteraction.InnerHtml = "Please indicate how much interaction with the host family you prefer.";
            rdoInteraction.Focus();
            return;
        }
        else
            eInteraction.InnerHtml = "";
        if (rdoHomeEnvironment.SelectedIndex == -1)
        {
            eHomeEnvironment.InnerHtml = "Please indicate how busy you prefer the home to be.";
            rdoHomeEnvironment.Focus();
            return;
        }
        else
            eHomeEnvironment.InnerHtml = "";
        if (rdoHealthStatus.SelectedIndex == -1)
        {
            eHealthStatus.InnerHtml = "Please indicate  your health status.";
            rdoHealthStatus.Focus();
            return;
        }
        else
            eHealthStatus.InnerHtml = "";

        
        //process submission
        //Insert HomestayRelatives table
        //Int32 applicantID, Int32 familyID, String familyName, String firstName, String occupation, String DOB, String gender,
        //String relationship, String email, String cellPhone, String workPhone, String driversLicenseNumber, String middleName
        if (firstNameFather.Text != "")
        {
            String FamilyNameFather = familyNameFather.Text.Replace("'", "''");
            String FirstNameFather = firstNameFather.Text.Replace("'", "''");
            String OccupationFather = occupationFather.Text.Replace("'", "''");
            String FatherID = hdnFatherID.Value;
            
            if (FatherID != "" && FatherID != "0")
            {
                Int32 intFatherID = Convert.ToInt32(FatherID);
                intResult = hsInfo.UpdateHomestayRelatives(intFatherID, FamilyNameFather, FirstNameFather, OccupationFather, "",0 ,"M", "Father", "", "", "", "", "");
            } else if (FamilyNameFather != "" && FirstNameFather != "")
                intResult = hsInfo.InsertHomestayRelatives(intApplicantID, 0, FamilyNameFather, FirstNameFather, OccupationFather, strBlank, 0,"M", "Father", strBlank, strBlank, strBlank, strBlank, strBlank);
        }
        //if (firstNameMother.Text != "")
        //{
            String FamilyNameMother = familyNameMother.Text.Replace("'", "''");
            String FirstNameMother = firstNameMother.Text.Replace("'", "''");
            String OccupationMother = occupationMother.Text.Replace("'", "''");
            String MotherID = hdnMotherID.Value;
            
            if (MotherID != "" && MotherID != "0")
            {
                Int32 intMotherID = Convert.ToInt32(MotherID);
                intResult = hsInfo.UpdateHomestayRelatives(intMotherID, FamilyNameMother, FirstNameMother, OccupationMother, "",0, "F", "Mother", strBlank, strBlank, strBlank, strBlank, strBlank);
            } else if(FamilyNameMother != "" && FirstNameMother != "")
                intResult = hsInfo.InsertHomestayRelatives(intApplicantID, 0, FamilyNameMother, FirstNameMother, OccupationMother, strBlank,0 ,"F", "Mother", strBlank, strBlank, strBlank, strBlank, strBlank);
        //}
        if (ageSibling1.Text != "")
        {
            String FamilyNameSibling1 = familyNameSibling1.Text.Replace("'", "''");
            String FirstNameSibling1 = firstNameSibling1.Text.Replace("'", "''");
            String DOBSibling1 = dobSibling1.Text.Replace("'", "''");
            //String AgeSibling1 = ageSibling1.Text.Replace("'", "''");
            Int32 AgeSibling1 = Convert.ToInt32(ageSibling1.Text);
            String GenderSibling1 = genderSibling1.Text;
            String Sibling1ID = hdnSibling1ID.Value;

            if (Sibling1ID != "" && Sibling1ID != "0")
            {
                Int32 intSibling1ID = Convert.ToInt32(Sibling1ID);
                intResult = hsInfo.UpdateHomestayRelatives(intSibling1ID, FamilyNameSibling1, FirstNameSibling1, strBlank, DOBSibling1, AgeSibling1, GenderSibling1, "Sibling 1", strBlank, strBlank, strBlank, strBlank, strBlank);
            }
            else 
            {
                //insert new relative
                intResult = hsInfo.InsertHomestayRelatives(intApplicantID, 0, FamilyNameSibling1, FirstNameSibling1, strBlank, DOBSibling1,1, GenderSibling1, "Sibling 1", strBlank, strBlank, strBlank, strBlank, strBlank);
            }
        }
        if (ageSibling2.Text != "")
        {
            String FamilyNameSibling2 = familyNameSibling2.Text.Replace("'", "''");
            String FirstNameSibling2 = firstNameSibling2.Text.Replace("'", "''");
            String DOBSibling2 = dobSibling2.Text.Replace("'", "''");
            Int32 AgeSibling2 = Convert.ToInt32(ageSibling2.Text);
            String GenderSibling2 = genderSibling2.Text;
            String Sibling2ID = hdnSibling2ID.Value;

            if (Sibling2ID != "" && Sibling2ID != "0")
            {
                Int32 intSibling2ID = Convert.ToInt32(Sibling2ID);
                intResult = hsInfo.UpdateHomestayRelatives(intSibling2ID, FamilyNameSibling2, FirstNameSibling2, strBlank, DOBSibling2, AgeSibling2, GenderSibling2, "Sibling 2", strBlank, strBlank, strBlank, strBlank, strBlank);
            }
            else 
            {
                intResult = hsInfo.InsertHomestayRelatives(intApplicantID, 0, FamilyNameSibling2, FirstNameSibling2, strBlank, DOBSibling2, AgeSibling2, GenderSibling2, "Sibling 2", strBlank, strBlank, strBlank, strBlank, strBlank);
            }
        }
        if (ageSibling3.Text != "")
        {
            String FamilyNameSibling3 = familyNameSibling3.Text.Replace("'", "''");
            String FirstNameSibling3 = firstNameSibling3.Text.Replace("'", "''");
            String DOBSibling3 = dobSibling3.Text.Replace("'", "''");
            Int32 AgeSibling3 = Convert.ToInt32(ageSibling3.Text);
            String GenderSibling3 = genderSibling3.Text;
            String Sibling3ID = hdnSibling3ID.Value;
            
            if (Sibling3ID != "" && Sibling3ID != "0")
            {
                Int32 intSibling3ID = Convert.ToInt32(Sibling3ID);
                intResult = hsInfo.UpdateHomestayRelatives(intSibling3ID, FamilyNameSibling3, FirstNameSibling3, strBlank, DOBSibling3, AgeSibling3, GenderSibling3, "Sibling 3", strBlank, strBlank, strBlank, strBlank, strBlank);
            }
            else 
            {
                intResult = hsInfo.InsertHomestayRelatives(intApplicantID, 0, strBlank, strBlank, strBlank, DOBSibling3, AgeSibling3, GenderSibling3, "Sibling 3", strBlank, strBlank, strBlank, strBlank, strBlank);
            }
        }
        if (ageSibling4.Text != "")
        {
            String FamilyNameSibling4 = familyNameSibling4.Text.Replace("'", "''");
            String FirstNameSibling4 = firstNameSibling4.Text.Replace("'", "''");
            String DOBSibling4 = dobSibling4.Text.Replace("'", "''");
            Int32 AgeSibling4 = Convert.ToInt32(ageSibling4.Text);
            String GenderSibling4 = genderSibling4.Text;
            String Sibling4ID = hdnSibling4ID.Value;
            
            if (Sibling4ID != "" && Sibling4ID != "0")
            {
                Int32 intSibling4ID = Convert.ToInt32(Sibling4ID);
                intResult = hsInfo.UpdateHomestayRelatives(intSibling4ID, FamilyNameSibling4, FirstNameSibling4, strBlank, DOBSibling4, AgeSibling4, GenderSibling4, "Sibling 4", strBlank, strBlank, strBlank, strBlank, strBlank);
            }
            else 
            {
                intResult = hsInfo.InsertHomestayRelatives(intApplicantID, 0, FamilyNameSibling4, FirstNameSibling4, strBlank, DOBSibling4, AgeSibling4, GenderSibling4, "Sibling 4", strBlank, strBlank, strBlank, strBlank, strBlank);
            }
        }

        //Insert HomestayStudentInfo table
        String TraveledWhere = traveledWhere.Text.Replace("'", "''");
        String SmokingHabits = smokingHabits.Text.Replace("'", "''");
        String ActivitiesEnjoyed = activitiesEnjoyed.Text.Replace("'", "''");
        String Allergies = allergies.Text.Replace("'", "''");
        String HomeEnvironmentPreferences = homeEnvironmentPreferences.Text.Replace("'", "''");
        String HealthConditions = healthConditions.Text.Replace("'", "''");
        String selectedHealthStatus = rdoHealthStatus.SelectedValue;
        String selectedDriveCar = rdoDriveCar.SelectedValue;
        if (selectedDriveCar == "") { selectedDriveCar = "0"; }
        String AnythingElse = anythingElse.Text.Replace("'", "''");
        intResult = hsInfo.UpdateP2HomestayStudentInfo(intApplicantID, TraveledWhere, SmokingHabits, ActivitiesEnjoyed, HomeEnvironmentPreferences, Allergies,
            HealthConditions, selectedHealthStatus, Convert.ToInt32(selectedDriveCar), AnythingElse);

        //Delete all previous selections; then re-add new ones
        intResult = hsInfo.DeleteHomestayPreferenceSelections(intApplicantID, 0);

        //Insert into HomestayPreferenceSelections table
        String SmokersOK = rdoOtherSmokerOK.SelectedValue;
        if (SmokersOK != "")
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections(SmokersOK, intApplicantID, 0);
        }
        String StudentSmokes = rdoSmoker.SelectedValue;
        if (StudentSmokes != "")
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections(StudentSmokes, intApplicantID, 0);
        }
        String selectedSmallChildren = rdoSmallChildren.SelectedValue;
        if (selectedSmallChildren != "")
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections(selectedSmallChildren, intApplicantID, 0);
        }
        String selectedTraveledOutsideCountry = rdoTraveledOutsideCountry.SelectedValue;
        if (selectedTraveledOutsideCountry != "")
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections(selectedTraveledOutsideCountry, intApplicantID, 0);
        }
        if (MusicalInstrument.Checked)
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("MusicalInstrument", intApplicantID, 0);
        }
        if (Art.Checked)
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("Art", intApplicantID, 0);
        }
        if (TeamSports.Checked)
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("TeamSports", intApplicantID, 0);
        }
        if (IndividualSports.Checked)
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("IndividualSports", intApplicantID, 0);
        }
        if (ListenMusic.Checked)
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("ListenMusic", intApplicantID, 0);
        }
        if (Drama.Checked)
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("Drama", intApplicantID, 0);
        }
        if (WatchMovies.Checked)
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("WatchMovies", intApplicantID, 0);
        }
        if (Singing.Checked)
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("Singing", intApplicantID, 0);
        }
        if (Shopping.Checked)
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("Shopping", intApplicantID, 0);
        }
        if (ReadBooks.Checked)
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("ReadBooks", intApplicantID, 0);
        }
        if (Outdoors.Checked)
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("Outdoors", intApplicantID, 0);
        }
        if (Cooking.Checked)
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("Cooking", intApplicantID, 0);
        }
        if (Photography.Checked)
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("Photography", intApplicantID, 0);
        }
        if (Gaming.Checked)
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("Gaming", intApplicantID, 0);
        }
        String selectedInteraction = rdoInteraction.SelectedValue;
        if (selectedInteraction != "")
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections(selectedInteraction, intApplicantID, 0);
        }
        String selectedHomeEnvironment = rdoHomeEnvironment.SelectedValue;
     //   Response.Write("rdoHomeEnviro.SelectedValue: " + selectedHomeEnvironment + "<br />");
        if (selectedHomeEnvironment != "")
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections(selectedHomeEnvironment, intApplicantID, 0);
       //     Response.Write("result: " + intResult.ToString() + "<br /");
        }
        if (Vegetarian.Checked)
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("Vegetarian", intApplicantID, 0);
        }
        if (GlutenFree.Checked)
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("GlutenFree", intApplicantID, 0);
        }
        if (DairyFree.Checked)
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("DairyFree", intApplicantID, 0);
        }
        if (OtherFoodAllergy.Checked)
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("OtherFoodAllergy", intApplicantID, 0);
        }
        if (Halal.Checked)
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("Halal", intApplicantID, 0);
        }
        if (Kosher.Checked)
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("Kosher", intApplicantID, 0);
        }
        if (NoSpecialDiet.Checked)
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("NoSpecialDiet", intApplicantID, 0);
        }
        //Then navigate to next page
        if (blUnderAge)
        {
            Response.Redirect("AgreementsUAp1.aspx?ID=" + hdnApplicantID.Value);
        }
        else
        {
            Response.Redirect("Agreements18.aspx?ID=" + hdnApplicantID.Value);
        } 
    }
    protected void btnCmdBack_Click(object sender, EventArgs e)
    {
        if(Request.QueryString["UA"] == null)
            Response.Redirect("ApplicationForm_L.aspx?ID=" + hdnApplicantID.Value +"&UA=f");
        else if(Request.QueryString["UA"] == "f")
            Response.Redirect("ApplicationForm_L.aspx?ID=" + hdnApplicantID.Value + "&UA=f");
        else
            Response.Redirect("ApplicationForm_L.aspx?ID=" + hdnApplicantID.Value + "&UA=t");
    }

    protected void btnCmdBackApplication_Click(object sender, EventArgs e)
    {
        Response.Redirect("ApplicationForm_L.aspx");
    }

    protected void btnCmdContinue_Click(object sender, EventArgs e)
    {
        if (blUnderAge)
        {
            Response.Redirect("AgreementsUAp1.aspx?ID=" + hdnApplicantID.Value);
        }
        else
        {
            Response.Redirect("Agreements18.aspx?ID=" + hdnApplicantID.Value);
        }
    }

    Boolean checkDate(TextBox txtBox)
    {
        dateSuccess = DateTime.TryParse(txtBox.Text.Trim(), out outDate);
        if (dateSuccess)
            txtBox.Text = outDate.ToShortDateString();
        else
            txtBox.Focus();
        return dateSuccess;
    }
    protected void btnCmdReturnAdminHome_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Homestay/Admin");
    }
}



