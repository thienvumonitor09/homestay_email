﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Homestay_Admin_MatchPreferences3 : System.Web.UI.Page
{
    Homestay hsInfo = new Homestay();
    private Dictionary<string, EntityObject> stuHM = new Dictionary<string, EntityObject>();
    private Dictionary<string, EntityObject> famHM = new Dictionary<string, EntityObject>();


    protected void Page_Load(object sender, EventArgs e)
    {
        stuHM = LoadStudentHM();
        famHM = LoadFamilyHM();
        litResultMsg.Text = "No of Families have open room: " + famHM.Count() + "<br/>";

        /*
        foreach (var pair in famHM)
        {
            litResultMsg.Text += " " + pair.Key + "<br/>";
            
            //litResultMsg.Text += " " + pair.Value.ToString();
            
        }
        */
        if (!IsPostBack)
        {
            /** Populate DL for students Using DataBind **/
            DataTable dtStudents = hsInfo.GetHomestayStudents("Active", 0);
            //DataTable dtStudents = hsInfo.GetHomestayStudents("Active", 0);
            dtStudents.Columns.Add("valueField", typeof(string));
            if (dtStudents != null)
            {
                if (dtStudents.Rows.Count > 0)
                {
                    foreach (DataRow dr in dtStudents.Rows)
                    {
                        string applicantIDStr = dr["applicantID"].ToString();
                        dr["valueField"] = dr["familyName"].ToString() + ", "
                                            + dr["firstName"].ToString() + " - " + Convert.ToDateTime(dr["DOB"]).ToShortDateString();
                    }
                }
            }
            ddlStudents.DataSource = dtStudents;
            ddlStudents.DataTextField = "valueField";
            ddlStudents.DataValueField = "applicantID";
            ddlStudents.DataBind();
            ddlStudents.Items.Insert(0, new ListItem("--Select a student--", "0")); //Add default value
                                                                                    //ddlStudents.SelectedValue = "0";
                                                                                    /** End populating dropdow **/

        }
        //Response.Write(stuHM["2401"].Name.ToString());
        //litResultMsg.Text = stuHM["2401"].Name.ToString();
        //litResultMsg.Text = stuHM.Count() +"";
        /*
        string connectionStr = ConfigurationManager.ConnectionStrings["ConnectionString_CCSInternationalStudent"].ToString();
        SqlConnection conn = new SqlConnection(connectionStr);
        string sqlQuery = @"SELECT  [id]
                              ,[applicantID]
                          FROM[CCSInternationalStudent].[dbo].[HomestayStudentInfo]";


        DataTable students = new DataTable();
        SqlCommand cmd = new SqlCommand(sqlQuery, conn);

        try
        {
            conn.Open();

            SqlDataAdapter objDataAdapter = new SqlDataAdapter(cmd);

            objDataAdapter.Fill(students);
            ddlStudents.DataSource = students;
            ddlStudents.DataTextField = "applicantID";
            ddlStudents.DataValueField = "id";
            ddlStudents.DataBind();

        }
        catch (Exception ex)
        {
            Label1.Text += "Error RunGetQuery: " + ex.Message + "<br />SQL: " + sqlQuery + "<br />";
        }
        */

    }



    protected void ddlStudents_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dtStudentChoice = hsInfo.GetStudentStudyInfo(Convert.ToInt32(ddlStudents.SelectedValue));
        if (dtStudentChoice != null)
        {
            if (dtStudentChoice.Rows.Count > 0)
            {
                lblCollege.Text = dtStudentChoice.Rows[0]["studyWhere"].ToString();
                litCurrStudentStatus.Text = dtStudentChoice.Rows[0]["homestayStatus"].ToString();
            }
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //litResultMsg.Text += "Gender: " + rdoGender.SelectedValue + "<br/>";
        //litResultMsg.Text = "HS Preference: " + rdoGender.SelectedValue + "<br/>";
        //litResultMsg.Text += "Smoking: " + rdoSmoking.SelectedValue + "<br/>";

        int selectedStudentID = Convert.ToInt32(ddlStudents.SelectedValue);
        string rdoGenderStr = rdoGender.SelectedValue.ToString();
        string rdoHomestayStr = rdoHomestay.SelectedValue.ToString();
        string rdoSmokingStr = rdoSmoking.SelectedValue.ToString();

        litResultMsg.Text += "Selected Student's Info:" + "<br/>";
        //litResultMsg.Text += "ID: " + stuHM[selectedStudentID + ""].Name.ToString() + "<br/>";
        //litResultMsg.Text += "Smoker: " + stuHM[selectedStudentID + ""].Attributes["Smoker"][0];
        EntityObject stuObject = stuHM[selectedStudentID + ""];
        //litResultMsg.Text += " entityObj:" + stuObject.ToString() + "</br>";
        string stuCol = stuObject.Attributes["studyWhere"][0].ToLower();
        //litResultMsg.Text += " College:" + stuCol + "<br/>";

        Dictionary<string, string[]> stuAttributes = stuObject.Attributes;

        litResultMsg.Text += "applicantID:" + stuAttributes["applicantID"][0] + ", ";
        litResultMsg.Text += "College:" + stuAttributes["college"][0] + ", ";
        litResultMsg.Text += "smoke:" + stuAttributes["smoke"][0] + ", ";
        litResultMsg.Text += "otherSmoke:" + stuAttributes["otherSmoke"][0] + ", ";
        litResultMsg.Text += "gender:" + stuAttributes["gender"][0] + ", ";
        litResultMsg.Text += "homestayOption:" + stuAttributes["homestayOption"][0] + ", ";
        litResultMsg.Text += "dietaryNeeds:" + string.Join(",", stuAttributes["dietaryNeeds"]) + ", ";
        litResultMsg.Text += "hobbies:" + string.Join(",", stuAttributes["hobbies"]) + ", ";
        litResultMsg.Text += "interaction:" + stuAttributes["interaction"][0] + ", ";
        litResultMsg.Text += "homeEnvironment:" + stuAttributes["homeEnvironment"][0] + "<br/>";

        /*
        string[] values1 = { };
        
        if (entityObject.Attributes.TryGetValue("Nonsmoker", out values1)) // if Nonsmoker not found, mean that person smoke
        {
            litResultMsg.Text += "Nonsmoker:" + values1[0] + "<br/>";
        }
        else
        {
            litResultMsg.Text += "I Smoke!" + "<br/>";
        }*/

        litResultMsg.Text += "--------- <br/>";
        //litResultMsg.Text += " " + famHM.Count();
        //litRoomWithOtherFamily.Text = selectedStudentID + " " + rdoGenderStr + " " + rdoHomestayStr+ " " + rdoSmokingStr;
        /** Populate DL for students Using DataBind **/
        //string strSQL = "SELECT id, homeName FROM [CCSInternationalStudent].[dbo].[HomestayFamilyInfo]";
        //DataTable dtFamilies = hsInfo.RunGetQuery(strSQL);

        //List of matching level
        var matchingList = new List<Dictionary<string, EntityObject>>();

        /** Filter by college **/
        Dictionary<string, EntityObject> famHMFilteredCollege = MatchStuFamCollege(stuCol, famHM);
        litResultMsg.Text += "There are " + famHMFilteredCollege.Count() + " families match with student's college: <br/>";
        //litResultMsg.Text += PrintHM(famHMFilteredCollege);
        matchingList.Add(famHMFilteredCollege);

        famHM = famHMFilteredCollege;

        /** Filter by Smoking preferences **/
        if (rdoSmoking.SelectedValue.Equals("Include") || rdoSmoking.SelectedValue.Equals(""))
        {
            Dictionary<string, EntityObject> famHMFilteredSmoking = MatchStuFamSmoking(stuObject, famHM);
            litResultMsg.Text += "<br/>There are " + famHMFilteredSmoking.Count() + " families match with student's smoking:<br/>";
            //litResultMsg.Text += PrintHM(famHMFilteredSmoking);
            matchingList.Add(famHMFilteredSmoking);
        }

        /** Filter by Gender **/
        if (rdoGender.SelectedValue.Equals("Include") || rdoGender.SelectedValue.Equals(""))
        {
            Dictionary<string, EntityObject> famHMFilteredGender = MatchStuFamGender(stuObject, famHM);
            litResultMsg.Text += "<br/>There are " + famHMFilteredGender.Count() + " families match with student's gender:<br/>";
            //litResultMsg.Text += PrintHM(famHMFilteredGender);
            matchingList.Add(famHMFilteredGender);
        }


        /** Filter by homestayOption **/
        if (rdoHomestay.SelectedValue.Equals("Include") || rdoHomestay.SelectedValue.Equals(""))
        {
            Dictionary<string, EntityObject> famHMFilteredHomestayOption = MatchStuFamHomestayOption(stuObject, famHM, litResultMsg.Text);
            litResultMsg.Text += "<br/>There are " + famHMFilteredHomestayOption.Count() + " families match with student's hsOption:<br/>";
            //litResultMsg.Text += PrintHM(famHMFilteredHomestayOption);
            matchingList.Add(famHMFilteredHomestayOption);
        }


        /** Combine all dictionaries **/
        Dictionary<string, EntityObject> famHMFilteredFinal = FindIntersection(matchingList);
        litResultMsg.Text += "<br/>There are " + famHMFilteredFinal.Count() + " families match with student:<br/>";
        litResultMsg.Text += matchingList.Count() + "<br/>";

        //Proces final list to add score
        famHMFilteredFinal = AddScore(stuObject, famHMFilteredFinal);


        litResultMsg.Text += PrintHM2(stuObject, famHMFilteredFinal);


        /** Populate DL for students Using DataBind **/
        DataTable dtFamilies = new DataTable();
        dtFamilies.Columns.Add("id", typeof(string));
        dtFamilies.Columns.Add("homeName", typeof(string));
        foreach (var f in famHMFilteredFinal)
        {

            Dictionary<string, string[]> attributes = f.Value.Attributes;
            dtFamilies.Rows.Add(attributes["id"][0].ToString(), attributes["homeName"][0].ToString());
        }
        litFoundFamily.Text = "There are " + famHMFilteredFinal.Count() + " families found";
        //litResultMsg.Text += dtFamilies.Rows.Count + " families: <br/>";

        //Match with smoking preferences

        ddlFamilies.DataSource = dtFamilies;
        ddlFamilies.DataTextField = "homeName";
        ddlFamilies.DataValueField = "id";
        ddlFamilies.DataBind();
        ddlFamilies.Items.Insert(0, new ListItem("--Select a family--", "0")); //Add default value
                                                                               /** End populating dropdow **/

    }



    private static Dictionary<string, EntityObject> FindIntersection(List<Dictionary<string, EntityObject>> matchingList)
    {
        Dictionary<string, EntityObject> result = new Dictionary<string, EntityObject>();
        if (matchingList.Count() == 1)
        {
            return matchingList[0];
        }
        Dictionary<string, EntityObject> firstHM = matchingList[0];

        for (var i = 1; i < matchingList.Count(); i++)
        {
            Dictionary<string, EntityObject> anotherHM = matchingList[i];
            result = firstHM.Where(x => anotherHM.ContainsKey(x.Key))
                         .ToDictionary(x => x.Key, x => x.Value);
            //update firstHM for finding intersect
            firstHM = result;
        }
        return result;
    }


    private static string PrintHM(Dictionary<string, EntityObject> hm)
    {
        StringBuilder sb = new StringBuilder();

        foreach (var f in hm)
        {
            //sb.Append("main ID:" + f.Key + ", ");
            Dictionary<string, string[]> attributes = f.Value.Attributes;

            //sb.Append(f.Value.ToString());
            sb.Append("id:" + attributes["id"][0] + ", ");
            sb.Append("College:" + attributes["college"][0] + ", ");
            sb.Append("smoke:" + attributes["smoke"][0] + ", ");
            sb.Append("otherSmoke:" + attributes["otherSmoke"][0] + ", ");
            sb.Append("gender:" + attributes["gender"][0] + ", ");
            sb.Append("homestayOption:" + attributes["homestayOption"][0] + ", ");
            sb.Append("homestayOption1:" + attributes["homestayOption1"][0] + ", ");
            sb.Append("dietaryNeeds:" + string.Join(",", attributes["dietaryNeeds"]) + ", ");
            sb.Append("hobbies:" + string.Join(",", attributes["hobbies"]) + ", ");
            sb.Append("interaction:" + attributes["interaction"][0] + ", ");
            sb.Append("homeEnvironment:" + attributes["homeEnvironment"][0] + "<br/>");
        }
        return sb.ToString();

    }

    private static Dictionary<string, EntityObject> AddScore(EntityObject stuObj, Dictionary<string, EntityObject> hm)
    {
        Dictionary<string, string[]> stuAttributes = stuObj.Attributes;
        string[] stuHobbies = stuAttributes["hobbies"];
        string[] stuDietary = stuAttributes["dietaryNeeds"];
        string[] stuInteraction = stuAttributes["interaction"];
        string[] stuHomeEnvironment = stuAttributes["homeEnvironment"];

        foreach (var f in hm)
        {
            //sb.Append("main ID:" + f.Key + ", ");
            Dictionary<string, string[]> famAttributes = f.Value.Attributes;

            double score = 0;
            //Match hobbies
            string[] famHobbies = famAttributes["hobbies"];
            double percentHobbiesMatch = GetPercentMatch(stuHobbies, famHobbies);
            famAttributes["hobbiesScore"] = new string[] { percentHobbiesMatch + "" };

            score += percentHobbiesMatch * 20 / 100;

            //Match dietary needs
            string[] famDietary = famAttributes["dietaryNeeds"];
            double percentDietaryMatch = 0;


            if (stuDietary.Length == 0 || stuDietary[0].Equals("NoSpecialDiet"))//if the student does not have any dietary needs, 100% match with family
            {
                percentDietaryMatch = 100;
                //Console.WriteLine("100% match b/c student does not have any dietary needs.");
            }
            else
            {
                if (famDietary[0].Equals("NoSpecialDiet")) //if the student has dietary needs & family cannot provide, 0% match
                {
                    percentDietaryMatch = 0;
                    //Console.WriteLine("0% match b/c family cannot provide dietary needs");
                }
                else //if the student has dietary needs & family has selected from dietary list, start matching from 2 lists
                {
                    percentDietaryMatch = GetPercentMatch(stuDietary, famDietary);
                    //Console.WriteLine("% dietary needs: {0}% ", percentDietaryMatch);
                }
            }
            famAttributes["dietaryScore"] = new string[] { percentDietaryMatch + "" };
            score += percentDietaryMatch * 10 / 100;

            //Match Interaction
            string[] famInteraction = famAttributes["interaction"];
            double percentInteractionMatch = GetPercentMatch(stuInteraction, famInteraction);
            famAttributes["interactionScore"] = new string[] { percentInteractionMatch + "" };
            score += percentInteractionMatch * 25 / 100;

            //Match homeEnvironment
            string[] famHomeEnvironment = famAttributes["homeEnvironment"];
            double percentHomeEnvironmentMatch = GetPercentMatch(stuHomeEnvironment, famHomeEnvironment);
            famAttributes["homeEnvironmentScore"] = new string[] { percentHomeEnvironmentMatch + "" };
            score += percentHomeEnvironmentMatch * 25 / 100;

            famAttributes["score"] = new string[] { score + "" };
        }
        return hm;

    }
    private static string PrintHM2(EntityObject stuObj, Dictionary<string, EntityObject> hm)
    {
        /*
        Dictionary<string, string[]> stuAttributes = stuObj.Attributes;
        string[] stuHobbies = stuAttributes["hobbies"];
        string[] stuDietary = stuAttributes["dietaryNeeds"];
        string[] stuInteraction = stuAttributes["interaction"];
        string[] stuHomeEnvironment = stuAttributes["homeEnvironment"];
        */
        StringBuilder sb = new StringBuilder();
        foreach (var f in hm)
        {
            //sb.Append("main ID:" + f.Key + ", ");
            Dictionary<string, string[]> famAttributes = f.Value.Attributes;
            /*
            double score = 0;
            //Match hobbies
            string[] famHobbies = famAttributes["hobbies"];
            double percentHobbiesMatch = GetPercentMatch(stuHobbies, famHobbies);


            score += percentHobbiesMatch * 20 / 100;

            //Match dietary needs
            string[] famDietary = famAttributes["dietaryNeeds"];
            double percentDietaryMatch = 0;
            
            
            if (stuDietary.Length == 0 || stuDietary[0].Equals("NoSpecialDiet"))//if the student does not have any dietary needs, 100% match with family
            {
                percentDietaryMatch = 100;
                //Console.WriteLine("100% match b/c student does not have any dietary needs.");
            }
            else
            {
                if (famDietary[0].Equals("NoSpecialDiet")) //if the student has dietary needs & family cannot provide, 0% match
                {
                    percentDietaryMatch = 0;
                    //Console.WriteLine("0% match b/c family cannot provide dietary needs");
                }
                else //if the student has dietary needs & family has selected from dietary list, start matching from 2 lists
                {
                    percentDietaryMatch = GetPercentMatch(stuDietary, famDietary);
                    //Console.WriteLine("% dietary needs: {0}% ", percentDietaryMatch);
                }
            }
            score += percentDietaryMatch * 10 / 100;

            //Match Interaction
            string[] famInteraction = famAttributes["interaction"];
            double percentInteractionMatch = GetPercentMatch(stuInteraction, famInteraction);
            score += percentInteractionMatch * 25 / 100;

            //Match homeEnvironment
            string[] famHomeEnvironment = famAttributes["homeEnvironment"];
            
            double percentHomeEnvironmentMatch = GetPercentMatch(stuHomeEnvironment, famHomeEnvironment);
            
            score += percentHomeEnvironmentMatch * 25 / 100;
            */

            //sb.Append(f.Value.ToString());
            sb.Append("id:" + famAttributes["id"][0] + ", ");
            sb.Append("College:" + famAttributes["college"][0] + ", ");
            sb.Append("smoke:" + famAttributes["smoke"][0] + ", ");
            sb.Append("otherSmoke:" + famAttributes["otherSmoke"][0] + ", ");
            sb.Append("gender:" + famAttributes["gender"][0] + ", ");
            sb.Append("homestayOption:" + famAttributes["homestayOption"][0] + ", ");
            sb.Append("homestayOption1:" + famAttributes["homestayOption1"][0] + ", ");
            sb.Append("dietaryNeeds:" + string.Join(",", famAttributes["dietaryNeeds"]) + ", ");
            sb.Append("hobbies:" + string.Join(",", famAttributes["hobbies"]) + ", ");
            sb.Append("interaction:" + famAttributes["interaction"][0] + ", ");
            sb.Append("score:" + famAttributes["score"][0] + ", ");
            sb.Append("homeEnvironment: " + famAttributes["homeEnvironment"][0] + "<br/>");
            sb.Append("% hobbies match: " + famAttributes["hobbiesScore"][0] + "<br/>");
            sb.Append("% dietaryNeeds match: " + famAttributes["dietaryScore"][0] + "<br/>");
            sb.Append("% interaction match: " + famAttributes["interactionScore"][0] + "<br/>");
            sb.Append("% homeEnvironment match: " + famAttributes["homeEnvironmentScore"][0] + "<br/>");
            sb.Append("% overal match: " + famAttributes["score"][0] + "<br/><br/>");
        }
        return sb.ToString();

    }
    private static Dictionary<string, EntityObject> MatchStuFamCollege(string stuCol, Dictionary<string, EntityObject> famHM)
    {
        Dictionary<string, EntityObject> famHMFiltered = new Dictionary<string, EntityObject>();
        foreach (var f in famHM)
        {
            string famCol = f.Value.Attributes["college"][0].ToLower();
            if (stuCol.Equals(famCol) || famCol.Equals("either")) //"either" will match with scc/sfcc{
            {
                famHMFiltered.Add(f.Key, f.Value);
            }
        }
        return famHMFiltered;
    }

    private static Dictionary<string, EntityObject> MatchStuFamSmoking(EntityObject stuObj, Dictionary<string, EntityObject> famHM)
    {
        Dictionary<string, EntityObject> resultHM = new Dictionary<string, EntityObject>();
        Dictionary<string, string[]> stuAttributes = stuObj.Attributes;
        string studentSmoke = stuAttributes["smoke"][0];
        string studentOtherSmoke = stuAttributes["otherSmoke"][0];


        foreach (var f in famHM)
        {
            string familyName = f.Key;
            Dictionary<string, string[]> famAttributes = f.Value.Attributes;
            string familySmoke = famAttributes["smoke"][0];
            string familyOtherSmoke = famAttributes["otherSmoke"][0];

            if (GetStuFamSmokingMatched(studentSmoke, studentOtherSmoke, familySmoke, familyOtherSmoke))
            {
                resultHM.Add(f.Key, f.Value);
            }
        }
        return resultHM;
    }
    private static Dictionary<string, EntityObject> MatchStuFamGender(EntityObject stuObj, Dictionary<string, EntityObject> famHM)
    {
        Dictionary<string, EntityObject> resultHM = new Dictionary<string, EntityObject>();
        Dictionary<string, string[]> stuAttributes = stuObj.Attributes;
        string studentGender = stuAttributes["gender"][0].ToLower();


        foreach (var f in famHM)
        {
            bool isMatched = false;
            string familyName = f.Key;
            Dictionary<string, string[]> famAttributes = f.Value.Attributes;
            string familyGender = famAttributes["gender"][0].ToLower();


            if (familyGender.Equals("combination"))
            {
                isMatched = true;
            }
            else
            {
                if (familyGender.Equals("male"))
                {
                    if (studentGender.Equals("male") || studentGender.Equals("other"))
                    {
                        isMatched = true;
                    }
                }
                else if (familyGender.Equals("female"))
                {
                    if (studentGender.Equals("female") || studentGender.Equals("other"))
                    {
                        isMatched = true;
                    }
                }
                else if (familyGender.Equals("either"))
                {
                    //Check majorirty of room for gender
                    isMatched = true;
                }
            }
            if (isMatched)
            {
                resultHM.Add(f.Key, f.Value);
            }
        }
        return resultHM;
    }

    private static Dictionary<string, EntityObject> MatchStuFamHomestayOption(EntityObject stuObj, Dictionary<string, EntityObject> famHM, string stext)
    {
        Dictionary<string, EntityObject> resultHM = new Dictionary<string, EntityObject>();
        Dictionary<string, string[]> stuAttributes = stuObj.Attributes;
        string stuHomestayOption = stuAttributes["homestayOption"][0].ToLower();
        //If student does not have option, all family are satisfied
        if (stuHomestayOption.Equals("no") || stuHomestayOption.Equals("either"))
        {
            return famHM;
        }

        //If student does have option, but select "no preference" for dietary needs, all "yes" family are satisfied
        if (stuAttributes.ContainsKey("NoSpecialDiet"))
        {
            //return famHM;
        }

        string[] studentdietaryNeeds = stuAttributes["dietaryNeeds"];


        //If student has option (yes), and select some diet needs, all fam's dieratary needs must match with stu's needs
        foreach (var f in famHM)
        {
            //Check if family select "NoSpecialDiet", which means cannot accomodate student's diet needs, exclude that family
            Dictionary<string, string[]> famAttributes = f.Value.Attributes;
            string famHomestayOption = famAttributes["homestayOption1"][0].ToLower();

            //If family does not provide food, exclude
            if (famHomestayOption.Equals("no"))
            {
                //exclude this family
            }
            else
            {
                if (stuAttributes.ContainsKey("NoSpecialDiet"))
                {
                    resultHM.Add(f.Key, f.Value);
                }
                else
                {
                    if (famAttributes.ContainsKey("NoSpecialDiet")) //if student have dietary but fam cannot provide
                    {
                        //Exclude this family
                    }
                    else
                    {
                        string[] familydietaryNeeds = famAttributes["dietaryNeeds"];
                        if (studentdietaryNeeds.Length == 0)
                        {
                            return famHM;
                        }
                        else
                        {
                            if (familydietaryNeeds.Length == 0)
                            {
                                //If student has some needs, family has not selected any, return empty
                                return resultHM;
                            }
                            else
                            {
                                bool isSubset = studentdietaryNeeds.All(elem => familydietaryNeeds.Contains(elem));
                                if (isSubset)
                                {
                                    resultHM.Add(f.Key, f.Value);
                                }
                            }
                        }
                    }
                }

            }
        }
        return resultHM;
    }
    private static bool GetStuFamSmokingMatched(string studentSmoke, string studentOtherSmoke, string familySmoke, string familyOtherSmoke)
    {
        //bool matched = false;
        if (studentSmoke.Equals("yes") && studentOtherSmoke.Equals("yes") && familyOtherSmoke.Equals("yes"))
        {
            return true;
        }
        else if (studentSmoke.Equals("yes") && studentOtherSmoke.Equals("no") && familySmoke.Equals("no") && familyOtherSmoke.Equals("yes"))
        {
            return true;
        }
        else if (studentSmoke.Equals("no") && studentOtherSmoke.Equals("yes"))
        {
            return true;
        }
        else if (studentSmoke.Equals("no") && studentOtherSmoke.Equals("no") && familySmoke.Equals("no") && familyOtherSmoke.Equals("no"))
        {
            return true;
        }
        return false; ;
    }

    private static Dictionary<string, EntityObject> LoadStudentHM()
    {
        Dictionary<string, EntityObject> stuHM = new Dictionary<string, EntityObject>();
        Homestay hsInfo = new Homestay();
        DataTable dtStudents = hsInfo.GetHomestayStudents("Active", 0);



        //attributes.Add("gender", new string[] { "male" });
        //attributes.Add("college", new string[] { "scc" });
        //attributes.Add("smoke", new string[] { "yes" });
        //attributes.Add("otherSmoke", new string[] { "yes" });

        foreach (DataRow dr in dtStudents.Rows)
        {
            //Create dictionary
            string applicantIDStr = dr["applicantID"].ToString();
            int applicantIDInt = Convert.ToInt32(dr["applicantID"].ToString());
            Dictionary<string, string[]> attributes = new Dictionary<string, string[]>();
            string sqlQuery = @"SELECT  *
                                FROM [CCSInternationalStudent].[dbo].[SchoolInformation]
                                WHERE applicantID =" + applicantIDStr;


            DataTable dt = hsInfo.RunGetQuery(sqlQuery);
            foreach (DataRow row in dt.Rows)
            {
                foreach (DataColumn column in dt.Columns)
                {
                    string columnName = column.ColumnName;
                    string columnData = row[column].ToString();
                    attributes.Add(columnName, new string[] { columnData });
                }
            }

            //Add pref
            sqlQuery = @"SELECT  *
                        FROM [CCSInternationalStudent].[dbo].[HomestayPreferenceSelections]
                        WHERE applicantID =" + applicantIDStr;
            DataTable dtPref = hsInfo.RunGetQuery(sqlQuery);
            foreach (DataRow row in dtPref.Rows)
            {
                attributes[row["preferenceID"].ToString()] = new string[] { row["preferenceID"].ToString() };
            }

            //Add dietary needs
            attributes = AddDiertaryNeedsAttributes(attributes);

            //Add attribute "smoker", "othersmoker"
            attributes = AddAttributes(attributes);

            //Add attribute "college" for stuyWhere
            attributes["college"] = attributes["studyWhere"];

            //Add Gender
            sqlQuery = @"SELECT  *
                        FROM [CCSInternationalStudent].[dbo].[ApplicantBasicInfo]
                        WHERE id =" + applicantIDStr;
            DataTable dtBasicInfo = hsInfo.RunGetQuery(sqlQuery);
            foreach (DataRow row in dtBasicInfo.Rows)
            {
                string genderStr = row["Gender"].ToString();
                //Convert M to Male, F to Female, O to other
                if (genderStr.Equals("M"))
                {
                    genderStr = "Male";
                }
                else if (genderStr.Equals("F"))
                {
                    genderStr = "Female";
                }
                else if (genderStr.Equals("O"))
                {
                    genderStr = "Other";
                }
                attributes["gender"] = new string[] { genderStr };
            }

            //Add homestayOption from HomestayStudentInfo
            sqlQuery = @"SELECT  *
                        FROM [CCSInternationalStudent].[dbo].[HomestayStudentInfo]
                        WHERE applicantID =" + applicantIDStr;

            DataTable dtHomestayStudentInfo = hsInfo.RunGetQuery(sqlQuery);

            foreach (DataRow row in dtHomestayStudentInfo.Rows)
            {
                string homestayOptionStr = row["homestayOption"].ToString().ToLower();
                //Convert to yes/no/either for with food/without food/either
                if (homestayOptionStr.Equals("with food"))
                {
                    homestayOptionStr = "yes";
                }
                else if (homestayOptionStr.Equals("without food"))
                {
                    homestayOptionStr = "no";
                }
                else
                {
                    homestayOptionStr = "either";
                }
                attributes["homestayOption"] = new string[] { homestayOptionStr };
            }


            //Add hobbies
            attributes = AddHobbiesAttributes(attributes);

            //Add interaction
            attributes = AddInteractionAttributes(attributes);

            //Add homeEnvironment
            attributes = AddHomeEnvironmentAttributes(attributes);

            EntityObject entityObject = new EntityObject() { Name = applicantIDStr, Attributes = attributes };
            stuHM.Add(applicantIDStr, entityObject);
        }
        return stuHM;
    }

    private static Dictionary<string, EntityObject> LoadFamilyHM()
    {
        Dictionary<string, EntityObject> famHM = new Dictionary<string, EntityObject>();
        Homestay hsInfo = new Homestay();

        /*
        Dictionary<string, string[]> attributes = new Dictionary<string, string[]>();
        attributes.Add("gender", new string[] { "male" });
        attributes.Add("college", new string[] { "scc" });
        attributes.Add("smoke", new string[] { "yes" });
        attributes.Add("otherSmoke", new string[] { "yes" });
        */

        string connectionStr = ConfigurationManager.ConnectionStrings["ConnectionString_CCSInternationalStudent"].ToString();
        SqlConnection conn = new SqlConnection(connectionStr);
        string sqlQuery = @"SELECT *
                          FROM[CCSInternationalStudent].[dbo].[HomestayFamilyInfo]";


        DataTable dtFamilies = hsInfo.RunGetQuery(sqlQuery);
        foreach (DataRow row in dtFamilies.Rows)
        {
            string idStr = row["id"].ToString();
            Dictionary<string, string[]> attributes = new Dictionary<string, string[]>();
            foreach (DataColumn column in dtFamilies.Columns)
            {
                string columnName = column.ColumnName;
                string columnData = row[column].ToString();
                /*
                if (columnName.ToLower().Contains("college"))
                {
                    columnName = "college"; //family col name is "collegeQualified", must convert to "college" to match with student
                }
                */
                attributes.Add(columnName, new string[] { columnData });
            }
            //Add College : to match collegeQualified to college
            attributes["college"] = attributes["collegeQualified"];

            //replace homestayOption with yes/no
            if (attributes["homestayOption"][0].Equals("") || attributes["homestayOption"][0].Contains("Full"))
            {
                attributes["homestayOption1"] = new string[] { "yes" };
            }
            else
            {
                attributes["homestayOption1"] = new string[] { "no" };
            }
            //Add pref
            sqlQuery = @"SELECT  *
                        FROM [CCSInternationalStudent].[dbo].[HomestayPreferenceSelections]
                        WHERE familyID =" + idStr;
            DataTable dtPref = hsInfo.RunGetQuery(sqlQuery);
            foreach (DataRow rowPref in dtPref.Rows)
            {
                attributes[rowPref["preferenceID"].ToString()] = new string[] { rowPref["preferenceID"].ToString() };
            }

            //Add dietary needs
            attributes = AddDiertaryNeedsAttributes(attributes);


            //Add attribute "smoker", "othersmoker"
            attributes = AddAttributes(attributes);

            //Add Gender
            attributes["gender"] = attributes["genderPreference"];

            //Add hobbies
            attributes = AddHobbiesAttributes(attributes);

            //Add interaction
            attributes = AddInteractionAttributes(attributes);

            //Add homeEnvironment
            attributes = AddHomeEnvironmentAttributes(attributes);


            EntityObject entityObject = new EntityObject() { Name = idStr, Attributes = attributes };
            famHM.Add(idStr, entityObject);
        }

        //Detect smoker/nonsmoker and Add new attributes 


        //Filter based on open rooms. If all 4 rooms are occupied, exlcude that room
        Dictionary<string, EntityObject> filteredFamilies = new Dictionary<string, EntityObject>();
        string[] rooms = { "room1Occupancy", "room2Occupancy", "room3Occupancy", "room4Occupancy" };
        foreach (var f in famHM)
        {
            Dictionary<string, string[]> attributes = f.Value.Attributes;
            int count = 0;
            foreach (var r in rooms)
            {
                if (attributes[r][0].ToLower().Contains("ccs")) //for ccs and non-ccs
                {
                    count++;
                }

            }
            Console.WriteLine(count);
            if (count < 4)
            {
                filteredFamilies.Add(f.Key, new EntityObject { Name = f.Key, Attributes = attributes });
            }

        }
        return filteredFamilies;


        //return famHM;
    }

    private static Dictionary<string, string[]> AddHomeEnvironmentAttributes(Dictionary<string, string[]> attributes)
    {
        //Add homeEnvironment
        if (attributes.Keys.Any(key => key.ToLower().Contains("home"))) //This condition to make sures to match for "home" environment
        {
            if (attributes.Keys.Any(key => key.Contains("quiet")))
            {
                attributes["homeEnvironment"] = new string[] { "quiet" };
            }
            else if (attributes.Keys.Any(key => key.Contains("active")))
            {
                attributes["homeEnvironment"] = new string[] { "active" };
            }
            else //If family have not selected, default "quiet"
            {
                attributes["homeEnvironment"] = new string[] { "quiet" };
            }
        }



        return attributes;
    }

    private static Dictionary<string, string[]> AddInteractionAttributes(Dictionary<string, string[]> attributes)
    {
        //Add interaction
        if (attributes.ContainsKey("Interaction every day"))
        {
            attributes["interaction"] = new string[] { "everyday" };
        }
        else if (attributes.ContainsKey("Interaction frequent"))
        {
            attributes["interaction"] = new string[] { "frequent" };
        }
        else if (attributes.ContainsKey("Interaction minimal"))
        {
            attributes["interaction"] = new string[] { "minimal" };
        }
        else //If family have not selected, set to "minimal"
        {
            attributes["interaction"] = new string[] { "minimal" };
        }
        return attributes;
    }

    private static Dictionary<string, string[]> AddPrefAttributes(Dictionary<string, string[]> attributes)
    {
        //Add hobbies
        List<string> stuHobbies = new List<string>();
        Homestay hsInfo = new Homestay();
        string sqlQuery = @"SELECT fieldID
                      FROM [CCSInternationalStudent].[dbo].[HomestayPreferences]
                      where id >= 7 AND id <= 33";
        DataTable dtHobbies = hsInfo.RunGetQuery(sqlQuery);
        foreach (DataRow row in dtHobbies.Rows)
        {
            string hobbieS = row["fieldID"].ToString();
            if (attributes.ContainsKey(hobbieS))
            {
                stuHobbies.Add(hobbieS);
            }
        }

        attributes["hobbies"] = stuHobbies.ToArray();
        return attributes;
    }

    private static Dictionary<string, string[]> AddHobbiesAttributes(Dictionary<string, string[]> attributes)
    {
        //Add hobbies
        List<string> stuHobbies = new List<string>();
        Homestay hsInfo = new Homestay();
        string sqlQuery = @"SELECT fieldID
                      FROM [CCSInternationalStudent].[dbo].[HomestayPreferences]
                      where id >= 7 AND id <= 33";
        DataTable dtHobbies = hsInfo.RunGetQuery(sqlQuery);
        foreach (DataRow row in dtHobbies.Rows)
        {
            string hobbieS = row["fieldID"].ToString();
            if (attributes.ContainsKey(hobbieS))
            {
                stuHobbies.Add(hobbieS);
            }
        }

        attributes["hobbies"] = stuHobbies.ToArray();
        return attributes;
    }
    private static Dictionary<string, string[]> AddDiertaryNeedsAttributes(Dictionary<string, string[]> attributes)
    {
        //Add dietary needs
        if (attributes.ContainsKey("NoSpecialDiet"))
        {
            attributes["dietaryNeeds"] = attributes["NoSpecialDiet"];
        }
        else
        {
            attributes["dietaryNeeds"] = new string[] { "TBD" };
            string[] dietPreferences = new string[] { "Vegetarian", "GlutenFree", "DairyFree", "Halal", "Kosher" };
            //Find student's dietary needs
            List<string> stuDietaryNeeds = new List<string>();

            foreach (string s in dietPreferences)
            {
                if (attributes.ContainsKey(s))
                {
                    stuDietaryNeeds.Add(s);
                }
            }
            attributes["dietaryNeeds"] = stuDietaryNeeds.ToArray();
        }
        return attributes;
    }

    private static Dictionary<string, string[]> AddAttributes(Dictionary<string, string[]> attributes)
    {
        /*
             * Add new attribute "smoke" to detect if the family is smoker or nonsmoker. !Nonsmoker = smoker
             */
        if (attributes.ContainsKey("Nonsmoker"))
        {
            attributes["smoke"] = new string[] { "no" };
        }
        else
        {
            attributes["smoke"] = new string[] { "yes" };
        }

        /*
         * All value for other smoke: Other smoker no preference, Other smoker not OK,Other smoker OK
         * !(Other smoker OK) = (Other smoker no preference, Other smoker OK)
         * Add new attribute "otherSmoke" to detect if the family ok if other smoke
         */
        if (attributes.ContainsKey("Other smoker not OK"))
        {
            attributes["otherSmoke"] = new string[] { "no" };
        }
        else
        {
            attributes["otherSmoke"] = new string[] { "yes" };
        }
        return attributes;
    }

    private static double GetPercentMatch(string[] stuAttributes, string[] famAttributes)
    {
        var attributeIntersect = stuAttributes.Intersect(famAttributes);
        int noAttributeIntersect = attributeIntersect.Count();
        if (stuAttributes.Length == 0)
        {
            return 100; //If student does not have any attributes, match is 100%
        }
        return Math.Round((double)noAttributeIntersect / stuAttributes.Length, 4) * 100;
    }

    protected void btnCompare_Click(object sender, EventArgs e)
    {
        litResultMsg.Text += "hello";
    }
}