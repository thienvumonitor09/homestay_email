﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CCSApps.master" AutoEventWireup="true" CodeFile="AttachfileUpload.aspx.cs" Inherits="Homestay_Admin_AttachfileUpload" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
     <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular-sanitize.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" /> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ng-ckeditor/0.2.1/ng-ckeditor.min.js"></script>
    <script src="https://cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/open-iconic/1.1.1/font/css/open-iconic-bootstrap.min.css"  />
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

  
    <script type="text/javascript">
        var app = angular.module('myApp', ['ngSanitize', 'ngCkeditor']);
        /* Factory */
        app.factory('CrudFactory', function () {
            var factory = {};
            factory.editTemplate = function () {
                editTemplate();
            }
            factory.addTemplateModal = function () {
                addTemplateModal();
            }
           
            return factory;
        }); 
        app.controller('templateAdminCtrl', function ($scope, $http, $window, CrudFactory) {
            
            $scope.getAllFiles = function () {
                //alert("retrieving files");
                $http({
                    method: "POST",
                    url: "WebService.asmx/GetAttachments",
                    dataType: 'json',
                    data: {},
                    headers: { "Content-Type": "application/json" }
                }).then(function (response) {
                    //alert(JSON.stringify(response.data.d));
                    var attachmentsObj = JSON.parse(response.data.d);
                    $scope.attachments = JSON.parse(response.data.d);
                    $scope.attached = [];
                    angular.forEach(attachmentsObj, function (value, key) {
                        //console.log(value);
                        $scope.attached.push({ name: value, selected: false });
                    });
                    console.log($scope.attached);
                }, function (error) {
                    alert("error");
                });
            };
            $scope.getAllFiles();

            $scope.deleteFile = function (index) {
                //var fileName = $scope.attachments[index]
                //alert($scope.attachments[index]);
                var r = confirm("Are you sure to delete the file " + $scope.attachments[index] + "?");
                if (r == false) {
                    return;
                } else {
                    $http({
                        method: "POST",
                        url: "AttachfileUpload.aspx/DeleteFile",
                        dataType: 'json',
                        data: { fileName: $scope.attachments[index] },
                        headers: { "Content-Type": "application/json" }
                    }).then(function (response) {
                        console.log(JSON.stringify(response.data.d));
                        //$window.location.reload();
                        $scope.getAllFiles();
                    }, function (error) {
                        alert("error");
                    });
                } 
            };

        });

    </script>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bannerH1Text" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">
    
<div ng-app="myApp" ng-controller="templateAdminCtrl" class="container">
    <h1>Manage Files</h1>
    <asp:FileUpload id="FileUploadControl" runat="server" /><br />
    <asp:Button runat="server" id="UploadButton" text="Upload" onclick="UploadButton_Click" CssClass="btn btn-primary"/> 
        <div class="row">
        <div class="col-sm-8" >
            
           
        </div>
    
        
        <div class="col-sm-4" >
             <input type="search" class="form-control" placeholder="Search File Name..." ng-model="searchName"/>
        </div>
  </div>
       

     <table class="table table-hover">
  <thead>
    <tr>
     <!-- <th scope="col">#</th> -->
        <th scope="col">#</th>
      <th scope="col">File Name</th>
      <th scope="col">Action</th>
      
    </tr>
  </thead>
  <tbody>
    <tr ng-repeat="x in attachments | filter :searchName">
      
        <td>{{$index+1}}</td>
      <td>
          <a href="/InternetContent/Homestay/EmailAttachments/{{x}}" target="_blank">{{x}}</a>
    </td>
      <td>
          
           <button type="button" class="btn btn-danger" ng-click="deleteFile($index)"> 
          <span class="oi oi-x" title="remove" aria-hidden="true"></span>
          </button>
      </td>
     
    </tr>
   
  </tbody>
</table>
 

</div>
    
   
</asp:Content>

