﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CCSApps.master" AutoEventWireup="true" CodeFile="Communication.aspx.cs" Inherits="Homestay_Admin_Communication" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
     <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular-sanitize.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" /> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ng-ckeditor/0.2.1/ng-ckeditor.min.js"></script>
    <!--<script src="https://cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script> -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/open-iconic/1.1.1/font/css/open-iconic-bootstrap.min.css"  />
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="Scripts/ckeditor/ckeditor.js"></script> 
    
    <script type="text/javascript">
        var app = angular.module('myApp', ['ngSanitize', 'ngCkeditor']);
       
        app.directive('ckEditor', function () {
            return {
                require: '?ngModel',
                link: function (scope, elm, attr, ngModel) {
                    var ck = CKEDITOR.replace(elm[0]);
                    if (!ngModel) return;
                    ck.on('instanceReady', function () {
                        ck.setData(ngModel.$viewValue);
                    });
                    function updateModel() {
                        scope.$apply(function () {
                            ngModel.$setViewValue(ck.getData());
                        });
                    }
                    ck.on('change', updateModel);
                    ck.on('key', updateModel);
                    ck.on('dataReady', updateModel);

                    ngModel.$render = function (value) {
                        ck.setData(ngModel.$viewValue);
                    };
                }
            };
        });
        app.controller('templateAdminCtrl', function ($scope, $http, $window) {
            $scope.getEmailTemplates = function () {
                $http({
                    method: "POST",
                    url: "WebService.asmx/GetTemplates",
                    dataType: 'json',
                    data: {},
                    headers: { "Content-Type": "application/json" }
                }).then(function (response) {
                    //alert(response.data);
                    console.log(response.data);
                    $scope.emailTemplates = JSON.parse(response.data.d);
                    console.log($scope.emailTemplates);
                }, function (error) {
                    alert("error");
                });
            };

            $scope.getEmailTemplates();
            //$scope.IsVisible = false;
            $scope.selectedTemplate = {};
            $scope.isAdd = false;
            $scope.isEdit = false;

            $scope.ShowHideEdit = function (templateID) {
                $scope.isAdd = false;
                $scope.isEdit = true;
                $('#myModal').modal('show');
                $("#modalTitle").text("Edit Template");
                //CrudFactory.editTemplate();
                //alert("hello" + templateID);
                $scope.selectedTemplate = $scope.emailTemplates[templateID];
                //$scope.IsVisible = true;
            };
           
            $scope.deleteTemplate = function (templateIndex) {
                //alert("remove" + templateIndex);
                var r = confirm("Are you sure to delete the template " + $scope.emailTemplates[templateIndex].templateName +"?");
                if (r == false) {
                    return;
                } else {
                    $http({
                        method: "POST",
                        url: "WebService.asmx/DeleteTemplate",
                        dataType: 'json',
                        data: { templateID: $scope.emailTemplates[templateIndex].templateID },
                        headers: { "Content-Type": "application/json" }
                    }).then(function (response) {
                        //alert(JSON.stringify(response.data.d));
                        //console.log($scope.emailTemplates);
                        $window.location.reload();
                    }, function (error) {
                        alert("error");
                    });
                }
            };
            $scope.saveTemplate = function () {
                alert("saving");
                $http({
                    method: "POST",
                    url: "WebService.asmx/SaveTemplate",
                    dataType: 'json',
                    data: { templateJS: JSON.stringify($scope.selectedTemplate) },
                    headers: { "Content-Type": "application/json" }
                }).then(function (response) {
                    //alert(JSON.stringify(response.data.d));
                    //console.log($scope.emailTemplates);
                    alert("SAVED!");
                    
                }, function (error) {
                    alert("error");
                });
            };

            $scope.resetTemplate = function () {
                //alert("reset");
                //MathService.multiply();
            };
            $scope.addTemplateForm = function () {
                $scope.isAdd = true;
                $scope.isEdit = false;
                $scope.selectedTemplate = {};
                $('#myModal').modal('show');
                $("#modalTitle").text("Add Template");
                //CrudFactory.addTemplateModal();
            }
            $scope.addTemplate = function () {
                //alert("addTemplate");
                $http({
                    method: "POST",
                    url: "WebService.asmx/AddTemplate",
                    dataType: 'json',
                    data: { templateJS: JSON.stringify($scope.selectedTemplate) },
                    headers: { "Content-Type": "application/json" }
                }).then(function (response) {
                    //alert(JSON.stringify(response.data.d));
                    //console.log($scope.emailTemplates);
                    alert("New Template was added");
                    $scope.isSave = false;
                    $window.location.reload();
                }, function (error) {
                    alert("error");
                });
            };
        });

    </script>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bannerH1Text" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">

    <div ng-app="myApp" ng-controller="templateAdminCtrl" class="container">
            <a href="../Default.aspx">Admin page</a> &nbsp; &nbsp;&nbsp;
            <a href="Communication.aspx">Main Email Tool</a> &nbsp; &nbsp;&nbsp;
            <a href="TemplateAdmin.aspx">Templates</a> &nbsp; &nbsp;&nbsp;
            <a href="Reports.aspx">Reports</a>
    
 <!-- Modal for Edit/Add -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog" style="width:90%">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" id="modalTitle"></h4>
        </div>
        <div class="modal-body">
            <!-- main -->
          <div>
            <div class="form-group">
            <label for="Template">Template Name:</label>
                <input type="Text" class="form-control" ng-model="selectedTemplate.templateName">
            </div>
            <div class="form-group">
              <label for="comment">Content Preview:</label>
         
                <!-- <textarea ckeditor="editorOptions" ng-model="selectedTemplate.templateContent"></textarea> -->
                 <textarea ng-model="selectedTemplate.templateContent" data-ck-editor></textarea>
            </div>
            <!-- {{selectedTemplate.templateName}}{{selectedTemplate.templateContent}} -->
             
        </div>  
            <!-- End main -->
        </div>
        <div class="modal-footer">
            <button  type="button" class="btn btn-primary" ng-click="saveTemplate()"  ng-show="isEdit">Save</button> 
            <button  type="button" class="btn btn-primary" ng-click="addTemplate()" ng-show="isAdd">Add</button> 
           <!-- <button  type="button" class="btn btn-primary" ng-click="resetTemplate()" >Reset</button>  -->
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
    <!-- End Modal for Edit/Add -->
<h1>Manage Templates</h1>
<div class="row">
        <div class="col-sm-4" >
            <input type="search" class="form-control" placeholder="Search Template Name..." ng-model="searchName"/>
        </div>
    
        <div class="col-sm-4" ></div>
        <div class="col-sm-4" >
            <button type="button" class="btn btn-success pull-right" ng-click="addTemplateForm()"> 
                <span class="oi oi-plus" title="addTemplate" aria-hidden="true"></span> Add new template
          </button>
        </div>
  </div>
       
 <table class="table table-hover">
  <thead>
    <tr>
     <!-- <th scope="col">#</th> -->
        <th scope="col">#</th>
      <th scope="col">Template Name</th>
      <th scope="col">Action</th>
      
    </tr>
  </thead>
  <tbody>
    <tr ng-repeat="x in emailTemplates | filter :searchName">
      <!-- <th scope="row">{{x.templateID}}</th> -->
        <td><a href="" ng-click="ShowHideEdit($index)"> {{$index+1}} </a></td>
      <td ng-model="templateName"><a href="" ng-click="ShowHideEdit($index)">{{x.templateName}}</a></td>
      <td>
          <button type="button" class="btn btn-primary" ng-click="ShowHideEdit($index)">
          <span class="oi oi-pencil" title="edit" aria-hidden="true"></span>
          </button>
           <button type="button" class="btn btn-danger" ng-click="deleteTemplate($index)"> 
          <span class="oi oi-x" title="remove" aria-hidden="true"></span>
          </button>
      </td>
     
    </tr>
   
  </tbody>
</table>
        <!--
        <div ng-show="IsVisible">
            {{selectedTemplate.templateID}} <br />
            <div class="form-group">
            <label for="Template">Template Name:</label>
                <input type="Text" class="form-control" ng-model="selectedTemplate.templateName">
            </div>
            <div class="form-group">
              <label for="comment">Content Preview:</label>
         
                <textarea ckeditor="editorOptions" ng-model="selectedTemplate.templateContent"></textarea> 
            </div>
            {{selectedTemplate.templateName}}{{selectedTemplate.templateContent}}
             <button  type="button" class="btn btn-primary" ng-click="saveTemplate()">Save</button> 
            <button  type="button" class="btn btn-primary" ng-click="addTemplate()" >Add</button> 
            <button  type="button" class="btn btn-primary" ng-click="resetTemplate()" >Reset</button> <br/>
        </div>  
        -->
</div>
    
   
</asp:Content>

