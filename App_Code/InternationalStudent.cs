﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Configuration;
using System.Net.Mail;

/// <summary>
/// Summary description for InternationalStudent
/// </summary>
public class InternationalStudent
{
    // contact info
    string addrName = "";
    string agentName = "";
    string cellP = "";
    string addrRelation = "";
    string sqlAddr;
    char speakE = '0';
    string strBlank = "";
    int intPlaceHold = 0;
    decimal decPlaceHold = 0.0M;
    DateTime dtePlaceHold = DateTime.MinValue;


    // for the international student db (on SQL2)
    SqlConnection objCon = new SqlConnection();
    SqlCommand objCommand = new SqlCommand();
    SqlDataAdapter objDataADapter;

    // for the other db - iCatalog (on dist17-sql3 --> moved to sql2 on 2/28/2017)
    SqlConnection objConCatalog = new SqlConnection();
    SqlCommand objCommandCatalog = new SqlCommand();
    SqlDataAdapter objDataAdapterCatalog;
    private static readonly string connectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSInternationalStudent"].ToString();


    public InternationalStudent()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public DataTable GetIntlStudentCountries()
    {
        DataSet ds = new DataSet();
        //objCon.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSInternationalStudent"].ToString();
        objCon.ConnectionString = connectionString;
        objCon.Open();
        objCommand.CommandText = "SELECT id,countryName FROM CCSInternationalStudent.dbo.CountryNames ORDER by countryName";
        objCommand.CommandType = CommandType.Text;
        objCommand.Connection = objCon;

        objDataADapter = new SqlDataAdapter(objCommand);
        try
        {
            objDataADapter.Fill(ds);
        }
        catch
        {
            // do nothing
        }
        finally
        {
            objCon.Close();
            objCon.Dispose();
            objCommand.Dispose();
        }
        return ds.Tables[0];
    }

    public DataTable GetPage1(string fbID)
    {
        DataSet dsP1 = new DataSet();
        //objCon.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSInternationalStudent"].ToString();
        objCon.ConnectionString = connectionString;
        objCon.Open();
        objCommand.CommandText = String.Format("SELECT familyName, firstName, middleNames, DOB, Gender, studentStatus, countryOfBirth, " +
            "countryOfCitizenship, nativeLanguage, englishAbility, visaType, numberDependents, Spouse, Children " +
            "FROM CCSInternationalStudent.dbo.ApplicantBasicInfo WHERE FBid = '{0}' ORDER BY id desc", fbID);
        objCommand.CommandType = CommandType.Text;
        objCommand.Connection = objCon;
        objDataADapter = new SqlDataAdapter(objCommand);
        try
        {
            objDataADapter.Fill(dsP1);
        }
        catch
        {
            // do nothing
        }
        finally
        {
            objCon.Close();
            objCon.Dispose();
            objCommand.Dispose();
        }
        return dsP1.Tables[0];
    }

    public DataTable GetPage2(string fbID)
    {
        DataSet dsP2 = new DataSet();
        //objCon.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSInternationalStudent"].ToString();
        objCon.ConnectionString = connectionString;
        objCon.Open();

        objCommand.CommandText = String.Format("SELECT BI.inUS, BI.parentDiff, BI.useAgency, BI.sendI_20, BI.USRelative, C.applicantID, C.addressCode, " +
            "C.addresseeName, C.agencyName, C.Addr, C.City, C.StateProv, C.Zip, C.Country, C.Phone, C.CellPhone, C.Email, C.Relationship, speakEnglish " +
            "FROM CCSInternationalStudent.dbo.ContactInformation AS C INNER JOIN " +
            "CCSInternationalStudent.dbo.ApplicantBasicInfo AS BI ON BI.id = C.applicantID " +
            "WHERE C.FBid = '{0}' ORDER BY C.addressCode", fbID);
        objCommand.CommandType = CommandType.Text;
        objCommand.Connection = objCon;
        objDataADapter = new SqlDataAdapter(objCommand);
        try
        {
            objDataADapter.Fill(dsP2);
        }
        catch
        {
            // do nothing
        }
        finally
        {
            objCon.Close();
            objCon.Dispose();
            objCommand.Dispose();
        }
        return dsP2.Tables[0];
    }

    public DataTable GetPage3(string fbID)
    {
        DataSet dsP3 = new DataSet();
        //objCon.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSInternationalStudent"].ToString();
        objCon.ConnectionString = connectionString;
       objCon.Open();
        // ******************************* COMMAND TEXT NEEDS TO FIT NEW PAGE 3 *******************************************
        objCommand.CommandText = String.Format("SELECT BI.specialAccommodations, BI.specialAccommodationDetail, " +
            "BI.healthIns, SI.FBid, SI.applicantID,SI.hsName, SI.hsGrad, SI.hsGradDate, SI.toeflSchoolCode, SI.toeflScore, SI.toeflTestDate, " +
            "SI.ieltsScore, SI.ieltsTestDate, SI.studiesBegin, SI.studyWhat, SI.studyWhere, SI.studyMajor " +
            "FROM CCSInternationalStudent.dbo.SchoolInformation AS SI INNER JOIN " +
            "CCSInternationalStudent.dbo.ApplicantBasicInfo AS BI ON BI.id = SI.applicantID " +
            "WHERE SI.FBid = '{0}'", fbID);
        objCommand.CommandType = CommandType.Text;
        objCommand.Connection = objCon;
        objDataADapter = new SqlDataAdapter(objCommand);
        try
        {
            objDataADapter.Fill(dsP3);
        }
        catch
        {
            // do nothing
        }
        finally
        {
            objCon.Close();
            objCon.Dispose();
            objCommand.Dispose();
        }
        return dsP3.Tables[0];
    }

    public DataTable GetPage4(string fbID)
    {
        DataSet dsP4 = new DataSet();
        //objCon.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSInternationalStudent"].ToString();
        objCon.ConnectionString = connectionString;
        objCon.Open();
        // ******************************* COMMAND TEXT NEEDS TO FIT NEW PAGE 4 *******************************************
        objCommand.CommandText = String.Format("SELECT BI.releaseInformation, BI.allTrue, SI.FBid, SI.applicantID, " +
            "SI.payHow, SI.payHowOther, SI.payOption, SI.housingFindOwn, SI.ownHousingAddress, SI.homeStay, SI.apartment, " +
            "SI.homeStayFamType, SI.homeStayBadFood, SI.homeStayBadFoodDetail, SI.homeStayCookOwn, SI.homeStaySmoke, SI.homeStayReligion " +
            "FROM CCSInternationalStudent.dbo.SchoolInformation AS SI INNER JOIN " +
            "CCSInternationalStudent.dbo.ApplicantBasicInfo AS BI ON BI.id = SI.applicantID " +
            "WHERE SI.FBid = '{0}'", fbID);
        objCommand.CommandType = CommandType.Text;
        objCommand.Connection = objCon;
        objDataADapter = new SqlDataAdapter(objCommand);
        try
        {
            objDataADapter.Fill(dsP4);
        }
        catch
        {
            // do nothing
        }
        finally
        {
            objCon.Close();
            objCon.Dispose();
            objCommand.Dispose();
        }
        return dsP4.Tables[0];
    }

    public DataTable GetStudentInfoAll(int qID)
    {
        DataSet dsBasic = new DataSet();
        //objCon.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSInternationalStudent"].ToString();
        objCon.ConnectionString = connectionString;
        objCon.Open();
        objCommand.CommandText = String.Format("SELECT * FROM CCSInternationalStudent.dbo.ApplicantBasicInfo WHERE id = {0} ORDER BY id desc", qID);
        objCommand.CommandType = CommandType.Text;
        objCommand.Connection = objCon;
        objDataADapter = new SqlDataAdapter(objCommand);
        try
        {
            objDataADapter.Fill(dsBasic);
        }
        catch
        {
            // do nothing
        }
        finally
        {
            objCon.Close();
            objCon.Dispose();
            objCommand.Dispose();
        }
        return dsBasic.Tables[0];
    }

    public DataTable GetExportData()
    {
        DataSet dsExport = new DataSet();
        //objCon.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSInternationalStudent"].ToString();
        objCon.ConnectionString = connectionString;
        objCon.Open();
        string sqlText;
        DateTime currDate = DateTime.Now;
        DateTime dte120 = currDate.AddDays(-120);
        /*
        objCommand.CommandText = String.Format("SELECT AB.preferredName, AB.firstName, AB.middleNames, AB.familyName,AB.DOB,AB.Gender,AB.studentStatus, " +
            "AB.countryOfBirth,AB.countryOfCitizenship,AB.englishAbility,AB.visaType,AB.numberDependents,AB.sendI_20, " +
            "AB.specialAccommodations,AB.specialAccommodationDetail,AB.healthIns,AB.releaseInformation,AB.howHearCCS,CI.Addr,CI.City,CI.StateProv, " +
            "CI.Zip,CI.Country,CI.Phone,CI.Email,SI.hsName,SI.hsGrad,SI.hsGradDate,SI.toeflSchoolCode,SI.toeflScore,SI.toeflTestDate, " +
            "SI.ieltsScore,SI.ieltsTestDate,SI.studiesBegin,SI.studyWhat,SI.studyWhere,SI.studyMajor,SI.payHow,SI.payHowOther, " +
            "SI.payOption,SI.housingFindOwn " +
            "FROM CCSInternationalStudent.dbo.ApplicantBasicInfo AS AB INNER JOIN " +
            "CCSInternationalStudent.dbo.ContactInformation AS CI ON CI.applicantID = AB.id AND CI.addressCode = {0} INNER JOIN " +
            "CCSInternationalStudent.dbo.SchoolInformation AS SI ON SI.applicantID = AB.id " +
            "ORDER BY AB.familyName", 1);
         */
        objCommand.CommandText = String.Format("SELECT AB.dateSubmitted, AB.preferredName, AB.firstName, AB.middleNames, AB.familyName,AB.DOB,AB.Gender,AB.studentStatus, " +
            "AB.countryOfBirth,AB.countryOfCitizenship,AB.englishAbility,AB.visaType,AB.numberDependents, " +
            "AB.healthIns,AB.releaseInformation,AB.howHearCCS,CI.Addr,CI.City,CI.StateProv, " +
            "CI.Zip,CI.Country,CI.Phone,CI.Email,SI.hsName,SI.hsGrad,SI.hsGradDate, " +
            "SI.studiesBegin,SI.studyWhat,SI.studyWhere,SI.studyMajor,SI.payHow,SI.payHowOther, " +
            "SI.payOption " +
            "FROM CCSInternationalStudent.dbo.ApplicantBasicInfo AS AB INNER JOIN " +
            "CCSInternationalStudent.dbo.ContactInformation AS CI ON CI.applicantID = AB.id AND CI.addressCode = {0} INNER JOIN " +
            "CCSInternationalStudent.dbo.SchoolInformation AS SI ON SI.applicantID = AB.id " +
            "WHERE AB.dateSubmitted > '{1}' " + 
            "ORDER BY AB.dateSubmitted desc, AB.familyName", 1, dte120.ToShortDateString());
        objCommand.CommandType = CommandType.Text;
        objCommand.Connection = objCon;
        objDataADapter = new SqlDataAdapter(objCommand);
        /*
        sqlText = String.Format("SELECT AB.preferredName, AB.firstName, AB.middleNames, AB.familyName,AB.DOB,AB.Gender,AB.studentStatus, " +
            "AB.countryOfBirth,AB.countryOfCitizenship,AB.englishAbility,AB.visaType,AB.numberDependents,AB.sendI_20, " +
            "AB.specialAccommodations,AB.specialAccommodationDetail,AB.healthIns,AB.releaseInformation,AB.howHearCCS,CI.Addr,CI.City,CI.StateProv, " +
            "CI.Zip,CI.Country,CI.Phone,CI.Email,SI.hsName,SI.hsGrad,SI.hsGradDate,SI.toeflSchoolCode,SI.toeflScore,SI.toeflTestDate, " +
            "SI.ieltsScore,SI.ieltsTestDate,SI.studiesBegin,SI.studyWhat,SI.studyWhere,SI.studyMajor,SI.payHow,SI.payHowOther, " +
            "SI.payOption,SI.housingFindOwn " +
            "FROM CCSInternationalStudent.dbo.ApplicantBasicInfo AS AB INNER JOIN " +
            "CCSInternationalStudent.dbo.ContactInformation AS CI ON CI.applicantID = AB.id AND CI.addressCode = {0} INNER JOIN " +
            "CCSInternationalStudent.dbo.SchoolInformation AS SI ON SI.applicantID = AB.id " +
            "ORDER BY AB.familyName", 1);
        */
        sqlText = String.Format("SELECT AB.dateSubmitted, AB.preferredName, AB.firstName, AB.middleNames, AB.familyName,AB.DOB,AB.Gender,AB.studentStatus, " +
            "AB.countryOfBirth,AB.countryOfCitizenship,AB.englishAbility,AB.visaType,AB.numberDependents, " +
            "AB.healthIns,AB.releaseInformation,AB.howHearCCS,CI.Addr,CI.City,CI.StateProv, " +
            "CI.Zip,CI.Country,CI.Phone,CI.Email,SI.hsName,SI.hsGrad,SI.hsGradDate, " +
            "SI.studiesBegin,SI.studyWhat,SI.studyWhere,SI.studyMajor,SI.payHow,SI.payHowOther, " +
            "SI.payOption " +
            "FROM CCSInternationalStudent.dbo.ApplicantBasicInfo AS AB INNER JOIN " +
            "CCSInternationalStudent.dbo.ContactInformation AS CI ON CI.applicantID = AB.id AND CI.addressCode = {0} INNER JOIN " +
            "CCSInternationalStudent.dbo.SchoolInformation AS SI ON SI.applicantID = AB.id " +
            "WHERE AB.dateSubmitted > '{1}' " +
            "ORDER BY AB.dateSubmitted desc, AB.familyName", 1, dte120.ToShortDateString());
        try
        {
            objDataADapter.Fill(dsExport);
        }
        catch (Exception ex)
        {
            // do nothing
           // return ex.Message + ". " + ex.StackTrace;

        }
        finally
        {
            objCon.Close();
            objCon.Dispose();
            objCommand.Dispose();
        }
        return dsExport.Tables[0];
        //return sqlText;
    }

    public DataTable GetExportDataReDo()
    {
        DataSet dsExport = new DataSet();
        //objCon.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSInternationalStudent"].ToString();
        objCon.ConnectionString = connectionString;
        objCon.Open();
        string sqlText;
        objCommand.CommandText = String.Format("SELECT AB.preferredName, AB.firstName, AB.middleNames, AB.familyName,AB.DOB,AB.Gender,AB.maritalStatus,AB.studentStatus, " +
            "AB.countryOfBirth,AB.countryOfCitizenship,AB.visaType,AB.numberDependents, " +
            "AB.healthIns,AB.releaseInformation,AB.howHearCCS,CI.Addr,CI.City,CI.StateProv, " +
            "CI.Zip,CI.Country,CI.Phone,CI.Email,SI.hsName,SI.hsGrad,SI.hsGradDate,AB.englishAbility, " +
            "SI.studiesBegin,SI.studyWhat,SI.trSchools,SI.studyWhere,SI.studyMajor,SI.payHow,SI.payHowOther,SI.payOption " +
            "FROM CCSInternationalStudent.dbo.ApplicantBasicInfo AS AB INNER JOIN " +
            "CCSInternationalStudent.dbo.ContactInformation AS CI ON CI.applicantID = AB.id AND CI.addressCode = 1 INNER JOIN " +
            "CCSInternationalStudent.dbo.SchoolInformation AS SI ON SI.applicantID = AB.id " +
            "ORDER BY AB.familyName", 1);
        objCommand.CommandType = CommandType.Text;
        objCommand.Connection = objCon;
        objDataADapter = new SqlDataAdapter(objCommand);
        sqlText = String.Format("SELECT AB.preferredName, AB.firstName, AB.middleNames, AB.familyName,AB.DOB,AB.Gender,AB.studentStatus, " +
            "AB.countryOfBirth,AB.countryOfCitizenship,AB.englishAbility,AB.visaType,AB.numberDependents,AB.sendI_20, " +
            "AB.specialAccommodations,AB.specialAccommodationDetail,AB.healthIns,AB.releaseInformation,AB.howHearCCS,CI.Addr,CI.City,CI.StateProv, " +
            "CI.Zip,CI.Country,CI.Phone,CI.Email,SI.hsName,SI.hsGrad,SI.hsGradDate,SI.toeflSchoolCode,SI.toeflScore,SI.toeflTestDate, " +
            "SI.ieltsScore,SI.ieltsTestDate,SI.studiesBegin,SI.studyWhat,SI.studyWhere,SI.studyMajor,SI.payHow,SI.payHowOther, " +
            "SI.payOption,SI.housingFindOwn " +
            "FROM CCSInternationalStudent.dbo.ApplicantBasicInfo AS AB INNER JOIN " +
            "CCSInternationalStudent.dbo.ContactInformation AS CI ON CI.applicantID = AB.id AND CI.addressCode = {0} INNER JOIN " +
            "CCSInternationalStudent.dbo.SchoolInformation AS SI ON SI.applicantID = AB.id " +
            "ORDER BY AB.familyName", 1);
        try
        {
            objDataADapter.Fill(dsExport);
        }
        catch (Exception ex)
        {
            // do nothing
            // return ex.Message + ". " + ex.StackTrace;

        }
        finally
        {
            objCon.Close();
            objCon.Dispose();
            objCommand.Dispose();
        }
        return dsExport.Tables[0];
        //return sqlText;
    }

    public DataTable GetFinancial(int qID)
    {
        DataSet dsFinancial = new DataSet();
        //objCon.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSInternationalStudent"].ToString();
        objCon.ConnectionString = connectionString;
        objCon.Open();
        objCommand.CommandText = String.Format("SELECT payHow, PayHowOther, payOption FROM CCSInternationalStudent.dbo.SchoolInformation WHERE applicantID = {0}", qID);
        objCommand.CommandType = CommandType.Text;
        objCommand.Connection = objCon;
        objDataADapter = new SqlDataAdapter(objCommand);
        try
        {
            objDataADapter.Fill(dsFinancial);
        }
        catch
        {
            // do nothing
        }
        finally
        {
            objCon.Close();
            objCon.Dispose();
            objCommand.Dispose();
        }
        return dsFinancial.Tables[0];
    }

    public DataTable GetHousing(int qID)
    {
        DataSet dsHousing = new DataSet();
        //objCon.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSInternationalStudent"].ToString();
        objCon.ConnectionString = connectionString;
        objCon.Open();
        objCommand.CommandText = String.Format("SELECT housingFindOwn, ownHousingAddress, homeStay, apartment, homeStayFamType, " +
            "homeStayBadFood, homeStayBadFoodDetail, homeStayCookOwn, homeStaySmoke, homeStayReligion " +
            "FROM CCSInternationalStudent.dbo.SchoolInformation WHERE applicantID = {0}", qID);
        objCommand.CommandType = CommandType.Text;
        objCommand.Connection = objCon;
        objDataADapter = new SqlDataAdapter(objCommand);
        try
        {
            objDataADapter.Fill(dsHousing);
        }
        catch
        {
            // do nothing
        }
        finally
        {
            objCon.Close();
            objCon.Dispose();
            objCommand.Dispose();
        }
        return dsHousing.Tables[0];
    }

    public DataTable GetSchool(int qID)
    {
        DataSet dsSchool = new DataSet();
        //objCon.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSInternationalStudent"].ToString();
        objCon.ConnectionString = connectionString;
        objCon.Open();
        objCommand.CommandText = String.Format("SELECT hsName, hsGrad, hsGradDate, toeflSchoolCode, toeflScore, toeflTestDate, " +
            "ieltsScore, ieltsTestDate, studiesBegin, studyWhat, studyWhere, studyMajor, payHow, payHowOther, payOption " +
            "trSchools, studyMajorOther, hscp " +
            "FROM CCSInternationalStudent.dbo.SchoolInformation WHERE applicantID = {0}", qID);
        objCommand.CommandType = CommandType.Text;
        objCommand.Connection = objCon;
        objDataADapter = new SqlDataAdapter(objCommand);
        try
        {
            objDataADapter.Fill(dsSchool);
        }
        catch
        {
            // do nothing
        }
        finally
        {
            objCon.Close();
            objCon.Dispose();
            objCommand.Dispose();
        }
        return dsSchool.Tables[0];
    }

    public DataTable GetContacts(int qID, int addressCd)
    {
        DataSet dsContacts = new DataSet();
        //objCon.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSInternationalStudent"].ToString();
        objCon.ConnectionString = connectionString;
        objCon.Open();
        objCommand.CommandText = String.Format("SELECT * FROM CCSInternationalStudent.dbo.ContactInformation WHERE applicantID = {0} AND addressCode = {1}", qID, addressCd);
        objCommand.CommandType = CommandType.Text;
        objCommand.Connection = objCon;
        objDataADapter = new SqlDataAdapter(objCommand);
        try
        {
            objDataADapter.Fill(dsContacts);
        }
        catch (Exception ex)
        {
            // do nothing
        }
        finally
        {
            objCon.Close();
            objCon.Dispose();
            objCommand.Dispose();
        }
        return dsContacts.Tables[0];
    }

    public DataTable getMajors()
    {
        DataSet ds = new DataSet();
        objConCatalog.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_iCatalog"].ToString();
        objConCatalog.Open();
        objCommandCatalog.CommandText = "SELECT DISTINCT ProgramTitle FROM CCSiCatalog_Legacy.dbo.Program " +
        "WHERE ProgramBeginSTRM <= (SELECT MIN(Yrq) FROM CCSiCatalog_Legacy.dbo.SMYrq WHERE TenDayYrq > GETDATE() AND LastDayYrq >= GETDATE()) " +
        "AND (ProgramEndSTRM = 'Z999' " +
        "OR ProgramEndSTRM >= (SELECT MIN(Yrq) FROM CCSiCatalog_Legacy.dbo.SMYrq WHERE TenDayYrq > GETDATE() AND LastDayYrq >= GETDATE())) " +
        "AND PublishedProgram = 1 " +
        "ORDER BY ProgramTitle";
        objCommandCatalog.CommandType = CommandType.Text;
        objCommandCatalog.Connection = objConCatalog;

        objDataAdapterCatalog = new SqlDataAdapter(objCommandCatalog);
        try
        {
            objDataAdapterCatalog.Fill(ds);
        }
        catch
        {
            // do nothing
        }
        finally
        {
            objConCatalog.Close();
            objConCatalog.Dispose();
            objCommandCatalog.Dispose();
        }
        return ds.Tables[0];
    }

    public string InsertStudentAppPage1(string FBid, string famName, string firstName, string midNames, DateTime dob, string gender, string studentStatus, string countryOfBirth,
        string countryOfCitizenship, string englishAbility, string visaType, int numberDependents, string spouse, string numberChildren)
    {
        string sql;
        string result = "";
        int recordID = -1;

        if (midNames.Length > 0)
            addrName = firstName + " " + midNames + " " + famName;
        else
            addrName = firstName + " " + famName;

        SqlConnection objCon = new SqlConnection();
        SqlCommand objCommand = new SqlCommand();
        sql = "uspInsertPage1 '" + FBid + "', '" + famName + "', '" + firstName + "', '" + midNames + "', '" + addrName + "', '" + dob + "', '" + gender + "', '" + studentStatus;
        sql += "', '" + countryOfBirth + "', '" + countryOfCitizenship + "', '" + englishAbility + "', '" + visaType;
        sql += "', " + numberDependents + ", '" + spouse + "', '" + numberChildren + "', '" + recordID + "'";
        // prepare the address information
        sqlAddr = "";

        try
        {
            //objCon.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSInternationalStudent"].ToString();
            objCon.ConnectionString = connectionString;
            objCon.Open();
            objCommand.Connection = objCon;
            objCommand.CommandText = sql;
            objCommand.CommandType = CommandType.Text;
            //  objCommand.ExecuteNonQuery();
            //  recordID = Convert.ToInt32(objCommand.Parameters["@newID"].Value);
            recordID = Convert.ToInt32(objCommand.ExecuteScalar());
            result = recordID.ToString();
        }
        catch (Exception expn)
        {
            result = "sql: " + sql + "<br />" + "sqlAddr: " + sqlAddr + "<br />" + expn.Message + "<br />Stack trace: " + expn.StackTrace + "<br />";
        }
        finally
        {
            objCon.Close();
            objCon.Dispose();
            objCommand.Dispose();
        }

        return result;
    }
/*
    public string InsertStudentAppPage2(string FBid, int applicantID, int addressCode, string addresseeName, string agencyName, string Addr, string City, string StateProv, string Zip,
       string Country, string Phone, string CellPhone, string FAX, string Email, string Relationship, int speakEngligh)
    {
        string sql;
        string result = "";
        int recordID = -1;

        SqlConnection objCon = new SqlConnection();
        SqlCommand objCommand = new SqlCommand();
        sql = "uspInsertContactFB '" + FBid + "', " + applicantID + ", " + addressCode + ", '" + addresseeName + "', '" + agencyName + "', '" + Addr + "', '" + City + "', '" + StateProv;
        sql += "', '" + Zip + "', '" + Country + "', '" + Phone + "', '" + CellPhone + "', '" + FAX;
        sql += "', '" + Email + "', '" + Relationship + "', " + speakEngligh + "'";
        // prepare the address information
        sqlAddr = "";

        try
        {
            objCon.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSInternationalStudent"].ToString();
            objCon.Open();
            objCommand.Connection = objCon;
            objCommand.CommandText = sql;
            objCommand.CommandType = CommandType.Text;
            //  objCommand.ExecuteNonQuery();
            //  recordID = Convert.ToInt32(objCommand.Parameters["@newID"].Value);
            recordID = Convert.ToInt32(objCommand.ExecuteScalar());
            result = recordID.ToString();
        }
        catch (Exception expn)
        {
            result = "sql: " + sql + "<br />" + "sqlAddr: " + sqlAddr + "<br />" + expn.Message + "<br />Stack trace: " + expn.StackTrace + "<br />";
        }
        finally
        {
            objCon.Close();
            objCon.Dispose();
            objCommand.Dispose();
        }

        return result;
    }
*/
    public string updateStudentAppP1(string FBid, string famName, string firstName, string midNames, DateTime dob, string gender, string studentStatus, string countryOfBirth,
        string countryOfCitizenship, string englishAbility, string visaType, int numberDependents, string spouse, string numberChildren)
    {
        string sql;
        string result = "";

        if (midNames.Length > 0)
            addrName = firstName + " " + midNames + " " + famName;
        else
            addrName = firstName + " " + famName;

        SqlConnection objCon = new SqlConnection();
        SqlCommand objCommand = new SqlCommand();
        sql = "uspUpdatePage1 '" + FBid + "', '" + famName + "', '" + firstName + "', '" + midNames + "', '" + addrName + "', '" + dob + "', '" + gender + "', '" + studentStatus;
        sql += "', '" + countryOfBirth + "', '" + countryOfCitizenship + "', '" + englishAbility + "', '" + visaType;
        sql += "', " + numberDependents + ", '" + spouse + "', '" + numberChildren + "'";

        try
        {
            //objCon.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSInternationalStudent"].ToString();
            objCon.ConnectionString = connectionString;
            objCon.Open();
            objCommand.Connection = objCon;
            objCommand.CommandText = sql;
            objCommand.CommandType = CommandType.Text;
            objCommand.ExecuteNonQuery();
        }
        catch (Exception expn)
        {
            result = "sql: " + sql + "<br />" + expn.Message + "<br />Stack trace: " + expn.StackTrace + "<br />";
        }
        finally
        {
            objCon.Close();
            objCon.Dispose();
            objCommand.Dispose();
        }
        //return "OK update page 1";
        //return sql;
        return result;
    }

    public string InsertStudentApplicationData(string famName, string firstName, string midNames, DateTime dob, string gender, string studentStatus, string countryOfBirth,
        string countryOfCitizenship, string englishAbility, string visaType, int numberDependents, string spouse, string numberChildren, int liveHere,
        int useAgency, string i20, int medN, string medNDetail, string healthIns,
        char releaseInfo, char allTrue, string howHear, DateTime dateSubmit)

    {
        string sql;
        string result = "";
        int recordID = -1;
 //       int tempInt = 0;

        if (midNames.Length > 0)
            addrName = firstName + " " + midNames + " " + famName;
        else
            addrName = firstName + " " + famName;

        SqlConnection objCon = new SqlConnection();
        SqlCommand objCommand = new SqlCommand();
        sql = "uspInsertApplicationData '" + famName + "', '" + firstName + "', '" + midNames + "', '" + addrName + "', '" + dob + "', '" + gender + "', '" + studentStatus;
        sql += "', '" + countryOfBirth + "', '" + countryOfCitizenship + "', '" + englishAbility + "', '" + visaType;
        sql += "', " + numberDependents + ", '" + spouse + "', '" + numberChildren + "', " + liveHere + ", " + useAgency + ", '" + i20;
        sql += "', " + medN + ", '" + medNDetail + "', '" + healthIns + "', '" + releaseInfo + "', '" + allTrue + "', '" + howHear + "', '" + dateSubmit + "', '" + recordID + "'";

 //       result = sql;
       
        try
        {
            //objCon.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSInternationalStudent"].ToString();
            objCon.ConnectionString = connectionString;
            objCon.Open();
            objCommand.Connection = objCon;
            objCommand.CommandText = sql;
            objCommand.CommandType = CommandType.Text;
          //  objCommand.ExecuteNonQuery();
          //  recordID = Convert.ToInt32(objCommand.Parameters["@newID"].Value);
            recordID = Convert.ToInt32(objCommand.ExecuteScalar());
            result = recordID.ToString();
        }
        catch (Exception expn)
        {
            result = "sql: " + sql + "<br />" + expn.Message + "<br />Stack trace: " + expn.StackTrace + "<br />";
        }
        finally
        {
            objCon.Close();
            objCon.Dispose();
            objCommand.Dispose();
        }

        return result;
    } // end InsertStudentApplicationData

    public string InsertStudentApplicationDataREDO(string famName, string firstName, string midNames, DateTime dob, string gender, string studentStatus, string countryOfBirth,
        string countryOfCitizenship, string englishAbility, string visaType, int numberDependents, string spouse, string numberChildren, int liveHere,
        int useAgency, string healthIns,
        char releaseInfo, char allTrue, string howHear, DateTime dateSubmit, string mStatus, string preferredName)
    {
        string sql;
        string result = "";
        int recordID = -1;
        //       int tempInt = 0;

        if (midNames.Length > 0)
            addrName = firstName + " " + midNames + " " + famName;
        else
            addrName = firstName + " " + famName;

        SqlConnection objCon = new SqlConnection();
        SqlCommand objCommand = new SqlCommand();
        sql = "uspInsertApplicationDataCtcLink '" + famName + "', '" + firstName + "', '" + midNames + "', '" + addrName + "', '" + dob + "', '" + gender + "', '" + studentStatus;
        sql += "', '" + countryOfBirth + "', '" + countryOfCitizenship + "', '" + englishAbility + "', '" + visaType;
        sql += "', " + numberDependents + ", '" + spouse + "', '" + numberChildren + "', " + liveHere + ", " + useAgency + ", '" + strBlank;
        sql += "', " + intPlaceHold + ", '" + strBlank + "', '" + healthIns + "', '" + releaseInfo + "', '" + allTrue + "', '" + howHear + "', '" + dateSubmit + "', '" + mStatus + "', '" + preferredName + "', '"  + recordID + "'";

        //       result = sql;

        try
        {
            //objCon.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSInternationalStudent"].ToString();
            objCon.ConnectionString = connectionString;
            objCon.Open();
            objCommand.Connection = objCon;
            objCommand.CommandText = sql;
            objCommand.CommandType = CommandType.Text;
            //  objCommand.ExecuteNonQuery();
            //  recordID = Convert.ToInt32(objCommand.Parameters["@newID"].Value);
            recordID = Convert.ToInt32(objCommand.ExecuteScalar());
            result = recordID.ToString();
        }
        catch (Exception expn)
        {
            result = "sql: " + sql + "<br />" + expn.Message + "<br />Stack trace: " + expn.StackTrace + "<br />";
        }
        finally
        {
            objCon.Close();
            objCon.Dispose();
            objCommand.Dispose();
        }

        return result;
    } // end InsertStudentApplicationDataREDO

    public string InsertContactInfo(int appID, int addrCode, string contactName, string agencyName, string addr, string city, string stProv, string zip,
        string country, string phone, string cellPhone, string email, string relationship, int speakE)
    {
        SqlConnection objCon = new SqlConnection();
        SqlCommand objCommand = new SqlCommand();

        try
        {
            //objCon.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSInternationalStudent"].ToString();
            objCon.ConnectionString = connectionString;
            objCon.Open();
            objCommand.Connection = objCon;

            sqlAddr = "uspInsertContactInfo " + appID + ", " + addrCode + ", '" + contactName + "', '" + agencyName + "', '" + addr + "', '" + city;
            sqlAddr += "', '" + stProv + "', '" + zip + "', '" + country + "', '" + phone + "', '" + cellPhone + "', '" + email + "', '" + relationship + "', '" + speakE + "'";

            objCommand.CommandText = sqlAddr;
            objCommand.CommandType = CommandType.Text;
            objCommand.ExecuteNonQuery();

        }
        catch (Exception e)
        {
            return "sqlAddr: " + sqlAddr + "<br />" + e.Message + "<br />Stack trace: " + e.StackTrace + "<br />";
        }
        finally
        {
            objCon.Close();
            objCon.Dispose();
            objCommand.Dispose();
        }
        return "OK contact info";
    }

    public string UpdateUseAgencyFlag(int appID, int useAgencyFlag)
    {
        SqlConnection objCon = new SqlConnection();
        SqlCommand objCommand = new SqlCommand();
        int recordID;
        string result;
        string sql = "uspUpdateUseAgencyFlag " + appID + ", " + useAgencyFlag;
        try
        {
            //objCon.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSInternationalStudent"].ToString();
            objCon.ConnectionString = connectionString;
            objCon.Open();
            objCommand.Connection = objCon;
            objCommand.CommandText = sql;
            objCommand.CommandType = CommandType.Text;
           // objCommand.ExecuteNonQuery();
            recordID = Convert.ToInt32(objCommand.ExecuteScalar());
            result = recordID.ToString();

        }
        catch (Exception e)
        {
            return "sql: " + sql + "<br />" + e.Message + "<br />Stack trace: " + e.StackTrace + "<br />";
        }
        finally
        {
            objCon.Close();
            objCon.Dispose();
            objCommand.Dispose();
        }
        return result;
    }

    public string InsertContactInfoFB(string FBid, int appID, int addrCode, string contactName, string agencyName, string addr, string city, string stProv, string zip,
        string country, string phone, string cellPhone, string email, string relationship, int speakE)
    {
        SqlConnection objCon = new SqlConnection();
        SqlCommand objCommand = new SqlCommand();

        try
        {
            //objCon.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSInternationalStudent"].ToString();
            objCon.ConnectionString = connectionString;
            objCon.Open();
            objCommand.Connection = objCon;

            sqlAddr = "uspInsertContactInfoFB " + FBid + "," + appID + ", " + addrCode + ", '" + contactName + "', '" + agencyName + "', '" + addr + "', '" + city;
            sqlAddr += "', '" + stProv + "', '" + zip + "', '" + country + "', '" + phone + "', '" + cellPhone + "', '" + email + "', '" + relationship + "', '" + speakE + "'";

            objCommand.CommandText = sqlAddr;
            objCommand.CommandType = CommandType.Text;
            objCommand.ExecuteNonQuery();

        }
        catch (Exception e)
        {
            return "sqlAddr: " + sqlAddr + "<br />" + e.Message + "<br />Stack trace: " + e.StackTrace + "<br />";
        }
        finally
        {
            objCon.Close();
            objCon.Dispose();
            objCommand.Dispose();
        }
        return "OK contact info";
    }

    public string UpdateSchool(int appID, string strCollege)
    {
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString_CCSInternationalStudent"].ToString());
        string sqlUpdate = @" UPDATE [CCSInternationalStudent].[dbo].[SchoolInformation] 
                                    SET [studyWhere] = @studyWhere 
                                    WHERE applicantID =" + appID;
        SqlCommand cmd = new SqlCommand(sqlUpdate, conn);
        cmd.Parameters.AddWithValue("@studyWhere", strCollege);
        try
        {
            conn.Open();
            cmd.ExecuteNonQuery();
        }
        catch (SqlException ex)
        {
            Console.WriteLine(ex.ToString());
        }
        finally
        {
            conn.Close();
        }
        return "ok";
    }
    public string UpdateSID(int appID, string SID)
    {
        SqlConnection objCon = new SqlConnection();
        SqlCommand objCommand = new SqlCommand();

        try
        {
            //objCon.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSInternationalStudent"].ToString();
            objCon.ConnectionString = connectionString;
            objCon.Open();
            objCommand.Connection = objCon;

            sqlAddr = "uspUpdateSID " + appID + ", '" + SID + "'";

            objCommand.CommandText = sqlAddr;
            objCommand.CommandType = CommandType.Text;
            objCommand.ExecuteNonQuery();

        }
        catch (Exception e)
        {
            return "sqlAddr: " + sqlAddr + "<br />" + e.Message + "<br />Stack trace: " + e.StackTrace + "<br />";
        }
        finally
        {
            objCon.Close();
            objCon.Dispose();
            objCommand.Dispose();
        }
        return "OK SID info";
    }

    public string UpdateContactInfoFB(string FBid, int appID, int addrCode, string contactName, string agencyName, string addr, string city, string stProv, string zip,
        string country, string phone, string cellPhone, string email, string relationship, int speakE)
    {
        SqlConnection objCon = new SqlConnection();
        SqlCommand objCommand = new SqlCommand();

        try
        {
            //objCon.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSInternationalStudent"].ToString();
            objCon.ConnectionString = connectionString;
            objCon.Open();
            objCommand.Connection = objCon;

            sqlAddr = "uspUpdateContactInfoFB '" + FBid + "', " + appID + ", " + addrCode + ", '" + contactName + "', '" + agencyName + "', '" + addr + "', '" + city;
            sqlAddr += "', '" + stProv + "', '" + zip + "', '" + country + "', '" + phone + "', '" + cellPhone + "', '" + email + "', '" + relationship + "', '" + speakE + "'";

            objCommand.CommandText = sqlAddr;
            objCommand.CommandType = CommandType.Text;
            objCommand.ExecuteNonQuery();

        }
        catch (Exception e)
        {
            return "Error UpdateContactInfoFB:  SQL: " + sqlAddr + "<br />" + e.Message + "<br />Stack trace: " + e.StackTrace + "<br />";
        }
        finally
        {
            objCon.Close();
            objCon.Dispose();
            objCommand.Dispose();
        }
        return "OK contact info";
    }

    /// <summary>
    /// Used by Homestay program to update student's permanent, parents, agent, and emergency contact information
    /// Requires ContactInformation id to identify record to update
    /// </summary>
    /// <param name="id"></param>
    /// <param name="appID"></param>
    /// <param name="addrCode"></param>
    /// <param name="contactName"></param>
    /// <param name="agencyName"></param>
    /// <param name="addr"></param>
    /// <param name="city"></param>
    /// <param name="stProv"></param>
    /// <param name="zip"></param>
    /// <param name="country"></param>
    /// <param name="phone"></param>
    /// <param name="cellPhone"></param>
    /// <param name="email"></param>
    /// <param name="relationship"></param>
    /// <param name="speakE"></param>
    /// <returns>Success/Error message</returns>
    public string UpdateContactInfo(int id, string contactName, string agencyName, string addr, string city, string stProv, string zip,
    string country, string phone, string cellPhone, string email, string relationship, char speakE)
    {
        SqlConnection objCon = new SqlConnection();
        SqlCommand objCommand = new SqlCommand();

        try
        {
            //objCon.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSInternationalStudent"].ToString();
            objCon.ConnectionString = connectionString;
            objCon.Open();
            objCommand.Connection = objCon;

            sqlAddr = "uspUpdateContactInfo " + id + ", '" + contactName + "', '" + agencyName + "', '" + addr + "', '" + city;
            sqlAddr += "', '" + stProv + "', '" + zip + "', '" + country + "', '" + phone + "', '" + cellPhone + "', '" + email + "', '" + relationship + "', " + speakE;

            objCommand.CommandText = sqlAddr;
            objCommand.CommandType = CommandType.Text;
            objCommand.ExecuteNonQuery();

        }
        catch (Exception e)
        {
            return "Error UpdateContactInfo - SQL: " + sqlAddr + "<br />Error message: " + e.Message + "<br />Stack trace: " + e.StackTrace + "<br />";
        }
        finally
        {
            objCon.Close();
            objCon.Dispose();
            objCommand.Dispose();
        }
        //return "Success UpdateContactInfo:  SQL: " + sqlAddr + "<br />";
        return "OK contact info";
    }

    /// <summary>
    /// Used by Homestay program to update student's local contact info
    /// Requires ContactInformation id to identify record to update
    /// </summary>
    /// <param name="id"></param>
    /// <param name="localAddress"></param>
    /// <param name="localCity"></param>
    /// <param name="localState"></param>
    /// <param name="localZip"></param>
    /// <param name="localPhone"></param>
    /// <param name="email"></param>
    /// <returns>Success/Error message</returns>
    public string UpdateLocalContactInfo(int id, string localAddress, string localCity, string localState, 
                                            string localZip,string localPhone, string email)
    {
        SqlConnection objCon = new SqlConnection(connectionString);
        try
        {
            objCon.Open();
            
            SqlCommand objCommand = new SqlCommand("usp_UpdateLocalContactInfo", objCon);
            objCommand.CommandType = CommandType.StoredProcedure;
            // set values to parameters 
            objCommand.Parameters.AddWithValue("@LocalAddress", localAddress);
            objCommand.Parameters.AddWithValue("@LocalCity", localCity);
            objCommand.Parameters.AddWithValue("@LocalState", localState);
            objCommand.Parameters.AddWithValue("@LocalZip", localZip);
            objCommand.Parameters.AddWithValue("@LocalPhone", localPhone);
            objCommand.Parameters.AddWithValue("@Email", email);
            objCommand.Parameters.AddWithValue("@id", id);

            objCommand.ExecuteNonQuery();
        }
        catch (Exception e)
        {
            return "Error UpdateContactInfo - SQL: " + sqlAddr + "<br />Error message: " + e.Message + "<br />Stack trace: " + e.StackTrace + "<br />";
        }
        finally
        {
            objCon.Close();
            objCon.Dispose();
            objCommand.Dispose();
        }

            return "OK contact info";
        }

        public string updateP2Basic(string FBid, int appID, int inUS, int parentDiff, int useA, int usRelative, string sI20)
        {
            SqlConnection objCon = new SqlConnection();
            SqlCommand objCommand = new SqlCommand();

            try
            {
                //objCon.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSInternationalStudent"].ToString();
                objCon.ConnectionString = connectionString;
                objCon.Open();
                objCommand.Connection = objCon;
    /******************* NEEDS TO REFLECT EXPANDED PAGE 2 INFO - INCLUDES ALL CONTACTS AND I-20 ***********************************/
            sqlAddr = "uspUpdateP2BasicFB '" + FBid + "'," + appID + ", " + inUS + ", " + parentDiff + ", " + useA + ", " + usRelative + ", '" + sI20 + "'";

            objCommand.CommandText = sqlAddr;
            objCommand.CommandType = CommandType.Text;
            objCommand.ExecuteNonQuery();

        }
        catch (Exception e)
        {
            return "sqlAddr: " + sqlAddr + "<br />" + e.Message + "<br />Stack trace: " + e.StackTrace + "<br />";
        }
        finally
        {
            objCon.Close();
            objCon.Dispose();
            objCommand.Dispose();
        }
        return "OK contact info";
    }

    public string insertSchoolInfo(int appID, string hsName, int hsGrad, DateTime hsGradDate, string toeflSchool, int toeflScore, DateTime toeflDate,
        decimal ieltsScore, DateTime ieltsDate, string studiesStart, string studyWhat, string studyWhere, string studyMajor,
        string payHow, string payHowOther, string payOption, int findOwn, string ownAddress, int homeStay, int apart, string familyType,
        int badFood, string badFoodDetail, string homeStayCook, int smoke, string religion)
    {
        SqlConnection objCon = new SqlConnection();
        SqlCommand objCommand = new SqlCommand();

        try
        {
            //objCon.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSInternationalStudent"].ToString();
            objCon.ConnectionString = connectionString;
            objCon.Open();
            objCommand.Connection = objCon;

            sqlAddr = "uspInsertSchoolInfo " + appID + ", '" + hsName + "', " + hsGrad + ", '" + hsGradDate + "', '" + toeflSchool + "', " + toeflScore + ", '" + toeflDate;
            sqlAddr += "', " + ieltsScore + ", '" + ieltsDate + "', '" + studiesStart + "', '" + studyWhat + "', '" + studyWhere + "', '" + studyMajor;
            sqlAddr += "', '" + payHow + "', '" + payHowOther + "', '" + payOption + "', " + findOwn + ", '" + ownAddress + "', " + homeStay + ", " + apart + ", '" + familyType;
            sqlAddr += "', " + badFood + ", '" + badFoodDetail + "', '" + homeStayCook + "', " + smoke + ", '" + religion + "'";
            
            objCommand.CommandText = sqlAddr;
            objCommand.CommandType = CommandType.Text;
            objCommand.ExecuteNonQuery();
            
        }
        catch (Exception e)
        {
            return "sqlAddr: " + sqlAddr + "<br />" + e.Message + "<br />Stack trace: " + e.StackTrace + "<br />";
        }
        finally
        {
            objCon.Close();
            objCon.Dispose();
            objCommand.Dispose();
        }
        return sqlAddr;
    } //end InsertSchoolInfo

    public string insertSchoolInfoREDO(int appID, string hsName, int hsGrad, DateTime hsGradDate, 
        string studiesStart, string studyWhat, string studyWhere, string studyMajor, string studyOtherMajor,
        string payHow, string payHowOther, string payOption, string trSchools, int hscp)
    {
        SqlConnection objCon = new SqlConnection();
        SqlCommand objCommand = new SqlCommand();

        try
        {
            //objCon.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSInternationalStudent"].ToString();
            objCon.ConnectionString = connectionString;
            objCon.Open();
            objCommand.Connection = objCon;

            sqlAddr = "uspInsertSchoolInfoNEW " + appID + ", '" + hsName + "', " + hsGrad + ", '" + hsGradDate + "', '" + strBlank + "', " + intPlaceHold + ", '" + dtePlaceHold;
            sqlAddr += "', " + decPlaceHold + ", '" + dtePlaceHold + "', '" + studiesStart + "', '" + studyWhat + "', '" + studyWhere + "', '" + studyMajor;
            sqlAddr += "', '" + payHow + "', '" + payHowOther + "', '" + payOption + "', " + intPlaceHold + ", '" + strBlank + "', " + intPlaceHold + ", " + intPlaceHold + ", '" + strBlank;
            sqlAddr += "', " + intPlaceHold + ", '" + strBlank + "', '" + strBlank + "', " + intPlaceHold + ", '" + strBlank + "', '" + trSchools + "', '" + studyOtherMajor + "', '" + hscp + "'";

            objCommand.CommandText = sqlAddr;
            objCommand.CommandType = CommandType.Text;
            objCommand.ExecuteNonQuery();

        }
        catch (Exception e)
        {
            return "sqlAddr: " + sqlAddr + "<br />" + e.Message + "<br />Stack trace: " + e.StackTrace + "<br />";
        }
        finally
        {
            objCon.Close();
            objCon.Dispose();
            objCommand.Dispose();
        }
        return sqlAddr;
    }// end InsertSchoolInfoREDO

    public string updateP3Basic(string FBid, int appID, int specialNeeds, string specialDetail, string healthIns)
    {
        SqlConnection objCon = new SqlConnection();
        SqlCommand objCommand = new SqlCommand();

        try
        {
            //objCon.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSInternationalStudent"].ToString();
            objCon.ConnectionString = connectionString;
            objCon.Open();
            objCommand.Connection = objCon;
            /******************* NEEDS TO REFLECT PAGE 3 INFO  ***********************************/
            sqlAddr = "uspUpdateP3BasicFB '" + FBid + "'," + appID + ", " + specialNeeds + ", '" + specialDetail + "', '" + healthIns + "'";

            objCommand.CommandText = sqlAddr;
            objCommand.CommandType = CommandType.Text;
            objCommand.ExecuteNonQuery();

        }
        catch (Exception e)
        {
            return "sqlAddr: " + sqlAddr + "<br />" + e.Message + "<br />Stack trace: " + e.StackTrace + "<br />";
        }
        finally
        {
            objCon.Close();
            objCon.Dispose();
            objCommand.Dispose();
        }
        return "OK contact info";
    }

    public string updatePage3School(string FBid, int appID, string hsName, int hsGrad, DateTime gradDate, string schoolCD, int toeflScore, DateTime toeflDate, decimal ieltsScore, 
        DateTime eiltsDate, string start, string what, string where, string major)
    {
        SqlConnection objCon = new SqlConnection();
        SqlCommand objCommand = new SqlCommand();

        try
        {
            //objCon.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSInternationalStudent"].ToString();
            objCon.ConnectionString = connectionString;
            objCon.Open();
            objCommand.Connection = objCon;
            /******************* NEEDS TO REFLECT PAGE 3 INFO  ***********************************/
            sqlAddr = "uspUpdateP3SchoolFB '" + FBid + "'," + appID + ", '" + hsName + "', " + hsGrad + ", '" + gradDate.ToShortDateString() + "', '" + schoolCD + "', " + toeflScore + ", '" + toeflDate.ToShortDateString();
            sqlAddr += "', " + ieltsScore + ", '" + eiltsDate.ToShortDateString() + "', '" + start + "', '" + what + "', '" + where + "', '" + major + "'";

            objCommand.CommandText = sqlAddr;
            objCommand.CommandType = CommandType.Text;
            objCommand.ExecuteNonQuery();

        }
        catch (Exception e)
        {
            return "sqlAddr: " + sqlAddr + "<br />" + e.Message + "<br />Stack trace: " + e.StackTrace + "<br />";
        }
        finally
        {
            objCon.Close();
            objCon.Dispose();
            objCommand.Dispose();
        }
        return "OK school update";
        //return "sqlAddr: " + sqlAddr + "<br />";
    }

    public string insertPage3School(string FBid, int appID, string hsName, int hsGrad, DateTime gradDate, string schoolCD, int toeflScore, DateTime toeflDate, decimal ieltsScore,
        DateTime eiltsDate, string start, string what, string where, string major, string time)
    {
        SqlConnection objCon = new SqlConnection();
        SqlCommand objCommand = new SqlCommand();

        try
        {
            //objCon.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSInternationalStudent"].ToString();
            objCon.ConnectionString = connectionString;
            objCon.Open();
            objCommand.Connection = objCon;
            /******************* NEEDS TO REFLECT PAGE 3 INFO  ***********************************/
            sqlAddr = "uspInsertP3SchoolFB '" + FBid + "'," + appID + ", '" + hsName + "', " + hsGrad + ", '" + gradDate.ToShortDateString() + "', '" + schoolCD + "', " + toeflScore + ", '" + toeflDate.ToShortDateString();
            sqlAddr += "', " + ieltsScore + ", '" + eiltsDate.ToShortDateString() + "', '" + start + "', '" + what + "', '" + where + "', '" + major + "', '" + time + "'";

            objCommand.CommandText = sqlAddr;
            objCommand.CommandType = CommandType.Text;
            objCommand.ExecuteNonQuery();

        }
        catch (Exception e)
        {
            return "sqlAddr: " + sqlAddr + "<br />" + e.Message + "<br />Stack trace: " + e.StackTrace + "<br />";
        }
        finally
        {
            objCon.Close();
            objCon.Dispose();
            objCommand.Dispose();
        }
        return "OK school insert";
        //return "sqlAddr: " + sqlAddr + "<br />";
    }

    public string updateP4Basic(string FBid, int appID, char releaseInfo, char allTrue)
    {
        SqlConnection objCon = new SqlConnection();
        SqlCommand objCommand = new SqlCommand();

        try
        {
            //objCon.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSInternationalStudent"].ToString();
            objCon.ConnectionString = connectionString;
            objCon.Open();
            objCommand.Connection = objCon;
            /******************* NEEDS TO REFLECT PAGE 4 INFO  ***********************************/
            sqlAddr = "uspUpdateP4BasicFB '" + FBid + "', " + appID + ", '" + releaseInfo + "', '" + allTrue + "'";

            objCommand.CommandText = sqlAddr;
            objCommand.CommandType = CommandType.Text;
            objCommand.ExecuteNonQuery();

        }
        catch (Exception e)
        {
            return "sqlAddr: " + sqlAddr + "<br />" + e.Message + "<br />Stack trace: " + e.StackTrace + "<br />";
        }
        finally
        {
            objCon.Close();
            objCon.Dispose();
            objCommand.Dispose();
        }
        return "OK p4 info";
    }

    public string updatePage4School(string FBid, int appID, string payHow, string payHowOther, string payOption, int findOwn, string ownAddr, int homeStay,
        int apt, string famType, int badFood, string badFoodDetail, string cookOwn, int doSmoke, string religion)
    {
        SqlConnection objCon = new SqlConnection();
        SqlCommand objCommand = new SqlCommand();

        try
        {
            //objCon.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSInternationalStudent"].ToString();
            objCon.ConnectionString = connectionString;
            objCon.Open();
            objCommand.Connection = objCon;
            /******************* NEEDS TO REFLECT PAGE 4 INFO  ***********************************/
            sqlAddr = "uspUpdateP4SchoolFB '" + FBid + "'," + appID + ", '" + payHow + "', '" + payHowOther + "', '" + payOption + "', " + findOwn + ", '" + ownAddr + "', " + homeStay;
            sqlAddr += ", " + apt + ", '" + famType + "', " + badFood + ", '" + badFoodDetail + "', '" + cookOwn + "', " + doSmoke + ", '" + religion + "'";

            objCommand.CommandText = sqlAddr;
            objCommand.CommandType = CommandType.Text;
            objCommand.ExecuteNonQuery();

        }
        catch (Exception e)
        {
            return "sqlAddr: " + sqlAddr + "<br />" + e.Message + "<br />Stack trace: " + e.StackTrace + "<br />";
        }
        finally
        {
            objCon.Close();
            objCon.Dispose();
            objCommand.Dispose();
        }
        //return "OK school update";
        return "sqlAddr: " + sqlAddr + "<br />";
    }
}