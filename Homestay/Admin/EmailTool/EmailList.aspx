﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CCSApps.master" AutoEventWireup="true" CodeFile="EmailList.aspx.cs" Inherits="Homestay_Admin_EmailTool_EmailList" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="X-UA-Compatible" content="IE=11" />
     <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular-sanitize.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" /> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ng-ckeditor/0.2.1/ng-ckeditor.min.js"></script>
    <!--<script src="https://cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script> -->
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular-route.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular-cookies.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/open-iconic/1.1.1/font/css/open-iconic-bootstrap.min.css"  />
 
    <script type="text/javascript" src="Scripts/ckeditor/ckeditor.js"></script> 
    <!-- css for icon -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> 

    <script>
        function editTemplate() {
            var g = $('#<%=Label2.ClientID%>').html();
            alert(g);
            return "edit";
        }
        

    </script>

    <script type="text/javascript">
        var app = angular.module('myApp', ['ngSanitize', 'ngCkeditor']);
        /* Factory */
        app.factory('CrudFactory', function () {
            var factory = {};
            factory.editTemplate = function () {
                editTemplate();
            }
            

            return factory;
        }); 
        
        app.controller('listAdminCtrl', function ($scope, $http, $window, CrudFactory) {
            //$scope.demo = "here";
            $scope.demo = CrudFactory.editTemplate();
        });

    </script>
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bannerH1Text" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">
    <div ng-app="myApp" ng-controller="listAdminCtrl" class="container">
        {{demo}}

    </div>
    <asp:Label id="Label2" Text="Label Control"  runat="server"/>
  <!-- <asp:Label id="Label1" Text="Label Control"  runat="server"/> -->
  

</div>

    

</asp:Content>

