﻿<%@ Page Title="List of Active Homestay Students" EnableEventValidation="false" Language="C#" AutoEventWireup="true" MasterPageFile="~/CCSApps.master" CodeFile="Student_ListActive.aspx.cs" Inherits="Homestay_Admin_Reports_Student_ListActive" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">

    
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="bannerH1Text" Runat="Server">
    Homestay Administration
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">
    <h2>List of Active Homestay Students</h2>
    <asp:Button ID="btnAdmin" runat="server" Text="Return to Admin Page" OnClick="btnAdmin_Click" />&nbsp;&nbsp;
    <asp:Button ID="btnMain" runat="server" Text="Return to Main Page" OnClick="btnMain_Click" />&nbsp;&nbsp;
    <asp:Button ID="btnExportExcel" runat="server" Text="Export to Excel" OnClick = "ExportToExcel"/>
    <asp:Button ID="Button1" runat="server" Text="Export to PDF" OnClick = "ExportToPDF"/>
    <br /><br />
    Age: 
    <asp:DropDownList ID="ddlAge" runat="server"
        OnSelectedIndexChanged = "AgeChanged"
            AutoPostBack = "true"
        AppendDataBoundItems = "true">
        <asp:ListItem Text = "All" Value = "All"></asp:ListItem>
        <asp:ListItem Text = "Under 18" Value = "Under"></asp:ListItem>
        <asp:ListItem Text = "18+" Value = "18"></asp:ListItem>
    </asp:DropDownList>
    <br />
    <asp:Literal ID="litStudentCount" runat="server"></asp:Literal>
    <asp:GridView ID="gvStudents" runat="server" AutoGenerateColumns="False"
        OnSelectedIndexChanged="OnSelectedIndexChanged"
        onsorting="gvStudents_Sorting"
        AllowSorting="True"   CellPadding="6" Width="100%"
        onrowdatabound="gvStudents_RowDataBound">
        <Columns>  
           <asp:TemplateField HeaderText="id" Visible="false">
                <ItemTemplate>
                    <asp:Label ID="lblid" runat="server"  Text='<%#Eval("id") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Full Name" SortExpression="FullName">
                <ItemTemplate>
                    <asp:Label ID="lblFullName" runat="server"  Text='<%#Eval("FullName") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Gender" SortExpression="Gender">
                <ItemTemplate>
                    <asp:Label ID="lblGender" runat="server"  Text='<%#Eval("Gender") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Campus" SortExpression="Campus">
                <ItemTemplate>
                    <asp:Label ID="lblCampus" runat="server"  Text='<%#Eval("Campus") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Age" SortExpression="Age">
               
                <ItemTemplate>
                    <asp:Label ID="lblAge" runat="server"  Text='<%#Eval("Age") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="DOB">
                <ItemTemplate>
                    <asp:Label ID="lblDOB" runat="server"  Text='<%#Eval("DOB") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
             <asp:TemplateField HeaderText="Country">
                <ItemTemplate>
                    <asp:Label ID="lblCountry" runat="server"  Text='<%#Eval("Country") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Placement Status" SortExpression="PlacementStatus">
                <ItemTemplate>
                    <asp:HyperLink runat="server"  ID="lblPlacementStatus" Text='<%#Eval("PlacementStatus") %>' 
                        NavigateUrl='<%# "/Homestay/Admin/Matching/editplacement.aspx?studentID=" + Eval("id") %>' />
                </ItemTemplate>
                
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Host Family">
                <ItemTemplate>
                    <asp:Repeater ID="rptHostFamily" runat="server"  OnItemDataBound="rptHostFamily_ItemDataBound">
                        <ItemTemplate>
                            <asp:HyperLink ID="lnkHostFamily" Text='<%#Bind("placementName") %>'  runat="server" />
                            <br />
                        </ItemTemplate>
                    </asp:Repeater>
                    <asp:Label ID="lblHostFamily" runat="server"  Text='<%#Eval("HostFamily") %>' NavigateUrl="#"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Success Meeting">
                <ItemTemplate>
                    <asp:Label ID="lblMeeting" runat="server"  Text='<%#Eval("Meeting") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Agreement">
                <ItemTemplate>
                    <asp:Label ID="lblAgreement" runat="server"  Text='<%#Eval("Agreement") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:ButtonField Text="Select" CommandName="Select" />
        </Columns>
        <HeaderStyle BackColor="#663300" ForeColor="#ffffff"/>  
        <RowStyle BackColor="#e7ceb6"/>  
    </asp:GridView>
    <br />
<h2>Selected Student</h2>
<br />
<br />
    <asp:DetailsView ID="dvStudent" runat="server" AutoGenerateRows="false" 
        OnItemUpdating="DetailsViewExample_ItemUpdating"
        OnModeChanging="DetailsViewExample_ModeChanging"
        OnItemCommand="DetailsViewExample_ItemCommand1"
        OnDataBound="DetailsViewExample_DataBound" Width="60%" CellPadding="10">
        <FieldHeaderStyle BackColor="#E4E4E4"  Font-Bold="True" Height="50px" Width="25%" />
         <RowStyle BorderStyle="Solid" BorderWidth="2px" />
        <Fields>
            <asp:TemplateField HeaderText="id" Visible="false">
                <ItemTemplate>
                    <asp:Label ID="lblid" Text='<%# Bind("id") %>' runat="server" ></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="ctcID">
                <ItemTemplate>
                    <asp:Label ID="lblctcID" Text='<%# Bind("ctcID") %>' runat="server" ></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Email">
                <ItemTemplate>
                    <asp:Label ID="lblEmail" Text='<%# Bind("Email") %>' runat="server" ></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Cell Phone">
                <ItemTemplate>
                    <asp:Label ID="lblCellPhone" Text='<%# Bind("CellPhone") %>' runat="server" ></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Mailing Address(Local Address)">
                <ItemTemplate>
                    <asp:Label ID="lblMailingAddress" Text='<%# Bind("MailingAddress") %>' runat="server" ></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="studyWhat">
                <ItemTemplate>
                    <asp:Label ID="lblStudyWhat" Text='<%# Bind("studyWhat") %>' runat="server" ></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="studyMajor">
                <ItemTemplate>
                    <asp:Label ID="lblStudyMajor" Text='<%# Bind("studyMajor") %>' runat="server" ></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Program of Study">
                <ItemTemplate>
                    <asp:Label ID="lblProgram" Text='<%# Bind("Program") %>' runat="server" ></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:CheckBoxList  ID="cblProgram" runat="server">
                        <asp:ListItem Text="IELP" Value="IELP" />
                        <asp:ListItem Text="ACA" Value="ACA" />
                        <asp:ListItem Text="HSCP" Value="HSCP" />
                    </asp:CheckBoxList>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Major">
                <ItemTemplate>
                    <asp:Label ID="lblMajor" Text='<%# Bind("Major") %>' runat="server" ></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txtMajor" Text='<%# Bind("Major") %>' runat="server" Width="500px"></asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Notes">
                <ItemTemplate>
                    <asp:Label ID="lblNotes" Text='<%# Bind("Notes") %>' runat="server" ></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Success Meeting">
                <ItemTemplate>
                    <asp:Label ID="lblMeeting" Text='<%# Bind("Meeting") %>' runat="server" ></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:RadioButtonList ID="rblMeeting" runat="server">
                        <asp:ListItem Value="Yes"></asp:ListItem>
                        <asp:ListItem Value="No"></asp:ListItem>
                    </asp:RadioButtonList>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Agreement">
                <ItemTemplate>
                    <asp:Label ID="lblAgreement" Text='<%# Bind("Agreement") %>' runat="server" ></asp:Label>
                </ItemTemplate>
                
                <EditItemTemplate>
                    <asp:DropDownList ID="ddlQuarter" runat="server">
                        <asp:ListItem Value="0" Text="--Select a quarter--"></asp:ListItem>
                        <asp:ListItem Value="Fall"></asp:ListItem>
                        <asp:ListItem Value="Winter"></asp:ListItem>
                        <asp:ListItem Value="Spring"></asp:ListItem>
                        <asp:ListItem Value="Summer"></asp:ListItem>
                    </asp:DropDownList>
                    <asp:TextBox ID="txtYear" runat="server" placeholder="Year(YYYY)"></asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField>
             <asp:CommandField ShowEditButton="true"  ItemStyle-CssClass="noPrint" />
        </Fields>
       
    </asp:DetailsView>
    <asp:Label ID="lblError" ForeColor="#CC3300" runat="server" ></asp:Label>
    <asp:GridView ID="GridView1" 
    RowStyle-BackColor="#A1DCF2" AlternatingRowStyle-BackColor="White" AlternatingRowStyle-ForeColor="#000"
    runat="server" AutoGenerateColumns="false" AllowPaging="true">
        <Columns>
            <asp:BoundField DataField="FullName" HeaderText="Full Name" ItemStyle-Width="150px" />
            <asp:BoundField DataField="Gender" HeaderText="Gender" ItemStyle-Width="100px" />
            <asp:BoundField DataField="Campus" HeaderText="Campus" ItemStyle-Width="100px" />
            <asp:BoundField DataField="Age" HeaderText="Age" ItemStyle-Width="50px" />
            <asp:BoundField DataField="DOB" HeaderText="DOB" ItemStyle-Width="100px" />
            <asp:BoundField DataField="Country" HeaderText="Country" ItemStyle-Width="100px" />
            <asp:BoundField DataField="PlacementStatus" HeaderText="PlacementStatus" ItemStyle-Width="100px" />
            <asp:BoundField DataField="HostFamily" HeaderText="HostFamily" ItemStyle-Width="200px" />
            <asp:BoundField DataField="Meeting" HeaderText="Meeting" ItemStyle-Width="50px" />
            <asp:BoundField DataField="Agreement" HeaderText="Agreement" ItemStyle-Width="100px" />
            <asp:BoundField DataField="Program" HeaderText="Program" ItemStyle-Width="150px" />
            <asp:BoundField DataField="Major" HeaderText="Major" ItemStyle-Width="200px" />
            
        </Columns>
    </asp:GridView>
</asp:Content>
