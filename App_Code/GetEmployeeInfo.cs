﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.DirectoryServices;
using System.Web;

/// <summary>
/// Summary description for GetEmployeeInfo
/// </summary>
public class GetEmployeeInfo
{
    public string FullName;
    public string FirstName;
    public string LastName;
    public string EmailAddress;
    public string PhoneNumber;
    public string SystemID;
    public string AccountName;
    public string Institution;
    public string EmployeeType;
    public string Department;
    public string MailStop;
    public string EmployeeTitle;
    public string Division;
    public string InstitutionStreet;
    public string InstitutionCity;
    public string InstitutionState;
    public string InstitutionZIP;
    public string ErrorMessage;
    private DirectoryEntry entry;
    private DirectorySearcher Dsearch;
    private SearchResultCollection results;


    public GetEmployeeInfo(string strAccountName, string strSystemID, string strColCode, string strLookupType)
	{
        switch (strLookupType)
        {
            case "PPMS":
                //Get Legacy data
                GetEmployeeSMSInfo(strSystemID);
                break;
            case "PS":
                //get PeopleSoft data
                GetEmployeePSInfo(strSystemID);
                break;
            case "AD":
                //Active Directory contact information
                GetEmployeeADInfo(strAccountName, strSystemID);
                break;
            case "EmployeeInfo":
                //Active Directory contact information
                GetEmployeeStudentAndEmployeeInfo(strAccountName);
                break;

        }
    }

    private void GetEmployeeStudentAndEmployeeInfo(string strAccountName)
    {
        //DEPENDS UPON Sudent and Employee Info
        string strSQL = "usp_GetEmployeeInfo"; 
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCommand = new SqlCommand();
        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_StudentAndEmployeeInfo"].ToString();
        //try
        //{
            objConn.Open();
            objCommand.CommandText = strSQL;
            objCommand.CommandType = CommandType.StoredProcedure;
            objCommand.Connection = objConn;

            SqlParameter objParam = new SqlParameter();
            objParam.ParameterName = "@ADLOGIN";
            objParam.Size = 200;
            objParam.SqlDbType = SqlDbType.Char;

            objParam.Value = strAccountName;
            //objParam.Value = "jeremy.anderson@ccs.spokane.edu";
            objCommand.Parameters.Add(objParam);

            SqlDataReader reader = objCommand.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    SystemID = Convert.ToString(reader.GetString(0));
                    FirstName = Convert.ToString(reader.GetString(1));
                    LastName = Convert.ToString(reader.GetString(2));
                    EmailAddress = Convert.ToString(reader.GetString(3));
                    String holderVal;
                    holderVal = Convert.ToString(reader.GetString(4));
                    Institution = GetInstitution(holderVal);
                }
            }
            else
            {
                Console.WriteLine("No rows found.");
            }
            reader.Close();

        //}

        //catch (Exception ex)
        //{
        //    ErrorMessage = ex.Message;
        //}
        //finally
        //{
        //    objConn.Close();
        //    objCommand.Dispose();
        //}

    }
    private void GetEmployeeSMSInfo(string strSystemID)
    {
        //DEPENDS UPON PPMS DATA - Used in Tally Quiz, Print Results, type quiz
        string strSQL = "CCS_td_SidEmpQuery"; //CCSReplNLink on Dist17 - SQL3
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCommand = new SqlCommand();
        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSReplNLink"].ToString();
        try
        {
            objConn.Open();
            objCommand.CommandText = strSQL;
            objCommand.CommandType = CommandType.StoredProcedure;
            objCommand.Connection = objConn;

            SqlParameter objParam = new SqlParameter();
            objParam.ParameterName = "@pSID";
            objParam.Size = 10;
            objParam.SqlDbType = SqlDbType.Char;
            objParam.Value = strSystemID;
            objCommand.Parameters.Add(objParam);

            objParam = new SqlParameter();
            objParam.ParameterName = "@pName";
            objParam.SqlDbType = SqlDbType.VarChar;
            objParam.Size = 30;
            objParam.Direction = ParameterDirection.Output;
            objCommand.Parameters.Add(objParam);

            objParam = new SqlParameter();
            objParam.ParameterName = "@pTyp";
            objParam.SqlDbType = SqlDbType.Char;
            objParam.Size = 2;
            objParam.Direction = ParameterDirection.Output;
            objCommand.Parameters.Add(objParam);

            objParam = new SqlParameter();
            objParam.ParameterName = "@pDept";
            objParam.SqlDbType = SqlDbType.Char;
            objParam.Size = 6;
            objParam.Direction = ParameterDirection.Output;
            objCommand.Parameters.Add(objParam);

            objParam = new SqlParameter();
            objParam.ParameterName = "@pMstop";
            objParam.SqlDbType = SqlDbType.VarChar;
            objParam.Size = 8;
            objCommand.Parameters.Add(objParam);
            objParam.Direction = ParameterDirection.Output;

            objParam = new SqlParameter();
            objParam.ParameterName = "@pPh";
            objParam.SqlDbType = SqlDbType.VarChar;
            objParam.Size = 10;
            objParam.Direction = ParameterDirection.Output;
            objCommand.Parameters.Add(objParam);

            objParam = new SqlParameter();
            objParam.ParameterName = "@pCampus";
            objParam.SqlDbType = SqlDbType.Char;
            objParam.Size = 4;
            objParam.Direction = ParameterDirection.Output;
            objCommand.Parameters.Add(objParam);

            objParam = new SqlParameter();
            objParam.ParameterName = "@pDeptTtl";
            objParam.SqlDbType = SqlDbType.VarChar;
            objParam.Size = 30;
            objParam.Direction = ParameterDirection.Output;
            objCommand.Parameters.Add(objParam);

            objCommand.ExecuteReader();
            FullName = Convert.ToString(objCommand.Parameters["@pName"].Value);
            EmployeeType = Convert.ToString(objCommand.Parameters["@pTyp"].Value);
            Department = Convert.ToString(objCommand.Parameters["@pDept"].Value);
            MailStop = Convert.ToString(objCommand.Parameters["@pMstop"].Value);
            PhoneNumber = Convert.ToString(objCommand.Parameters["@pPh"].Value);
            Institution = Convert.ToString(objCommand.Parameters["@pCampus"].Value);
            EmployeeTitle = Convert.ToString(objCommand.Parameters["@pDeptTtl"].Value);
        }
        catch (Exception ex)
        {
            ErrorMessage = ex.Message;
        }
        finally
        {
            objConn.Close();
            objCommand.Dispose();
        }
    }

    private void GetEmployeePSInfo(string strSystemID)
    {
        string strSQL = "usp_GetEmpInfoByEmplID"; //CCSGen_ctcLink_ODS on SQL2
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCommand = new SqlCommand();
        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSGen_ctcLink_ODS"].ToString();
        try
        {
            objConn.Open();
            objCommand.CommandText = strSQL;
            objCommand.CommandType = CommandType.StoredProcedure;
            objCommand.Connection = objConn;

            SqlParameter objParam = new SqlParameter();
            objParam.ParameterName = "@pEmplID";
            objParam.Size = 9;
            objParam.SqlDbType = SqlDbType.VarChar;
            objParam.Value = strSystemID;
            objCommand.Parameters.Add(objParam);

            objParam = new SqlParameter();
            objParam.ParameterName = "@pEmpName";
            objParam.SqlDbType = SqlDbType.VarChar;
            objParam.Size = 30;
            objParam.Direction = ParameterDirection.Output;
            objCommand.Parameters.Add(objParam);

            objParam = new SqlParameter();
            objParam.ParameterName = "@pEmpType";
            objParam.SqlDbType = SqlDbType.VarChar;
            objParam.Size = 3;
            objParam.Direction = ParameterDirection.Output;
            objCommand.Parameters.Add(objParam);

            objParam = new SqlParameter();
            objParam.ParameterName = "@pDeptName";
            objParam.SqlDbType = SqlDbType.Char;
            objParam.Size = 6;
            objParam.Direction = ParameterDirection.Output;
            objCommand.Parameters.Add(objParam);

            objCommand.ExecuteReader();
            FullName = Convert.ToString(objCommand.Parameters["@pEmpName"].Value);
            EmployeeType = Convert.ToString(objCommand.Parameters["@pEmpType"].Value);
            Department = Convert.ToString(objCommand.Parameters["@pDeptName"].Value);
        }
        catch (Exception ex)
        {
            ErrorMessage = ex.Message;
        }
        finally
        {
            objConn.Close();
            objCommand.Dispose();
        }
    }

    private void GetEmployeeADInfo(string strAccountName, string strSystemID)
    {
        //simplify existing AD searches
        entry = new DirectoryEntry(ConfigurationManager.ConnectionStrings["LDAP_CCS"].ConnectionString);
        Dsearch = new DirectorySearcher(entry);
        //Determine search filter
        if (strSystemID != "")
        {
    			Dsearch.Filter = "(employeeID=" + strSystemID + ")";
        }
        else if(strAccountName != "")
        {
            Dsearch.Filter = "(sAMAccountName=" + strAccountName + ")";
        }

        Dsearch.SearchScope = SearchScope.Subtree;
        Dsearch.PropertiesToLoad.Add("sAMAccountName");
        Dsearch.PropertiesToLoad.Add("cn");
        Dsearch.PropertiesToLoad.Add("title");
        Dsearch.PropertiesToLoad.Add("physicalDeliveryOfficeName");
        Dsearch.PropertiesToLoad.Add("employeeID");
        Dsearch.PropertiesToLoad.Add("givenname");
        Dsearch.PropertiesToLoad.Add("company");
        Dsearch.PropertiesToLoad.Add("sn");
        Dsearch.PropertiesToLoad.Add("mail");
        Dsearch.PropertiesToLoad.Add("telephonenumber");
        Dsearch.PropertiesToLoad.Add("department");
        Dsearch.PropertiesToLoad.Add("description");
        Dsearch.PropertiesToLoad.Add("memberof");
        Dsearch.PropertiesToLoad.Add("pwdlastset");
        Dsearch.PropertiesToLoad.Add("accountexpires");
        Dsearch.PropertiesToLoad.Add("accountexpirationstatus");
        Dsearch.PropertiesToLoad.Add("extensionAttribute4"); // Mail Stop
        Dsearch.PropertiesToLoad.Add("streetAddress"); // Street
        Dsearch.PropertiesToLoad.Add("l"); // City
        Dsearch.PropertiesToLoad.Add("st"); // State
        Dsearch.PropertiesToLoad.Add("postalCode"); // ZIP

        try
        {
            results = Dsearch.FindAll();
            // 2B. Pull in results
            if (results != null && results.Count > 0)
            {
                foreach (SearchResult result in results)
                {
                    //string searchpath = result.Path;
                    ResultPropertyCollection rpc = result.Properties;
                    foreach (string property in rpc.PropertyNames)
                    {
                        foreach (object value in rpc[property])
                        {
                            switch (property)
                            {
                                case "samaccountname":
                                    AccountName = value.ToString();
                                    break;
                                case "employeeid":
                                    SystemID = value.ToString();
                                    break;
                                case "givenname":
                                    FirstName = value.ToString();
                                    break;
                                case "sn":
                                    LastName = value.ToString();
                                    break;
                                case "cn":
                                    FullName = value.ToString();
                                    break;
                                case "company":
                                    Institution = value.ToString();
                                    break;
                                case "title":
                                    EmployeeTitle = value.ToString();
                                    break;
                                case "physicalDeliveryOfficeName":
                                    InstitutionStreet = value.ToString();
                                    break;
                                case "mail":
                                    EmailAddress = value.ToString();
                                    break;
                                case "department":
                                    Department = value.ToString();
                                    break;
                                case "description":
                                    Division = value.ToString();
                                    break;
                                case "telephonenumber":
                                    PhoneNumber = value.ToString();
                                    break;
                                case "extensionattribute4":
                                    MailStop = value.ToString();
                                    break;
                                case "streetaddress":
                                    InstitutionStreet = value.ToString();
                                    break;
                                case "l":
                                    InstitutionCity = value.ToString();
                                    break;
                                case "st":
                                    InstitutionState = value.ToString();
                                    break;
                                case "postalcode":
                                    InstitutionZIP = value.ToString();
                                    break;
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ErrorMessage = ex.Message;
        }
        finally
        {
            Dsearch.Dispose();
            entry.Close();
            entry.Dispose();
        }

    }
    private string GetInstitution(string strColCode)
    {
        string strCollege = "";
        if (strColCode == "SCC")
        {
            strCollege = "Spokane Community College";
        }
        else if (strColCode == "SFCC")
        {
            strCollege = "Spokane Falls Community College";
        }
        else if (strColCode == "DISTRICT")
        {
            strCollege = "Community College of Spokane";
        }
        return strCollege;
    }
}