﻿<%@ Page Title="List of Active Homestay Families" EnableEventValidation="false" Language="C#" AutoEventWireup="true" MasterPageFile="~/CCSApps.master" CodeFile="Family_ListActive2.aspx.cs" Inherits="Homestay_Admin_Reports_Family_ListActive2" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">

    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bannerH1Text" Runat="Server">
    Homestay Administration
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">
    <h2>List of Active Homestay Families</h2>
     <asp:Button ID="btnAdmin" runat="server" Text="Return to Admin Page" OnClick="btnAdmin_Click" />&nbsp;&nbsp;
    <asp:Button ID="btnMain" runat="server" Text="Return to Main Page" OnClick="btnMain_Click" />&nbsp;&nbsp;
    <asp:Button ID="btnExportExcel" runat="server" Text="Export to Excel" OnClick = "ExportToExcel"/>
    <asp:Button ID="Button1" runat="server" Text="Export to PDF" OnClick = "ExportToPDF"/>
    <br /><br />
    <asp:GridView ID="gvFamilies" runat="server" AutoGenerateColumns="False" CellPadding="6" Width="100%"
        OnPageIndexChanging="OnPageIndexChanging"
        AllowSorting="True"  
        onsorting="gvFamilies_Sorting"
        OnSelectedIndexChanged="OnSelectedIndexChanged" AllowPaging="True"
        onrowdatabound="gvFamilies_RowDataBound">
        <Columns>  
            <asp:TemplateField HeaderText="id">
                <ItemTemplate>
                    <asp:Label ID="lblid" runat="server"  Text='<%#Eval("id") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Full Name" SortExpression="FullName">
                <ItemTemplate>
                    <asp:Label ID="lblFullName" runat="server"  Text='<%#Eval("FullName") %>' ></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
             <asp:TemplateField HeaderText="Gender Preference">
                <ItemTemplate>
                    <asp:Label ID="lblGenderPreference" runat="server"  Text='<%#Eval("GenderPreference") %>' ></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Allow Smoking" >
                <ItemTemplate>
                    <asp:Label ID="lblAllowSmoking" runat="server"  Text='<%#Eval("AllowSmoking") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="College Qualified" >
                <ItemTemplate>
                    <asp:Label ID="lblCollegeQualified" runat="server"  Text='<%#Eval("CollegeQualified") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Time to SCC(minutes)" >
                <ItemTemplate>
                    <asp:Label ID="lblTimeSCC" runat="server"  Text='<%#Eval("TimeSCC") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Time to SFCC(minutes)" >
                <ItemTemplate>
                    <asp:Label ID="lblTimeSFCC" runat="server"  Text='<%#Eval("TimeSFCC") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="HS Option" >
                <ItemTemplate>
                    <asp:Label ID="lblHSOption" runat="server"  Text='<%#Eval("HSOption") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Student(s) Placed">
                <ItemTemplate>
                    <asp:Repeater ID="rptStudents" runat="server"  OnItemDataBound="rptStudents_ItemDataBound">
                        <ItemTemplate>
                            <asp:HyperLink ID="lnkStudents"  runat="server" />
                            <br />
                        </ItemTemplate>
                    </asp:Repeater>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:ButtonField Text="Select" CommandName="Select" />
        </Columns>
        <HeaderStyle BackColor="#663300" ForeColor="#ffffff"/>  
        <RowStyle BackColor="#e7ceb6"/>  
    </asp:GridView>
    <br />
    <h2>Selected Family</h2>
    <asp:DetailsView ID="dvFamily" runat="server" AutoGenerateRows="false" CellPadding="10">
         <FieldHeaderStyle BackColor="#E4E4E4"  Font-Bold="True" Height="50px" Width="150px" />
         <RowStyle BorderStyle="Solid" BorderWidth="2px" />
        <Fields>
            <asp:TemplateField HeaderText="Address">
                <ItemTemplate>
                    <asp:Label ID="lblAddress" Text='<%# Bind("Address") %>' runat="server" ></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="LandlinePhone">
                <ItemTemplate>
                    <asp:Label ID="lblid" Text='<%# Bind("LandlinePhone") %>' runat="server" ></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Students">
                <ItemTemplate>
                    <asp:Label ID="lblStudents" Text='<%# Bind("Students") %>' runat="server" ></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
        </Fields>
    </asp:DetailsView>
    <br />
    <asp:GridView ID="GridView1" 
    RowStyle-BackColor="#A1DCF2" AlternatingRowStyle-BackColor="White" AlternatingRowStyle-ForeColor="#000"
    runat="server" AutoGenerateColumns="false" AllowPaging="true" Width="100%">
        <Columns>
            <asp:BoundField DataField="FullName" HeaderText="Full Name" ItemStyle-Width="150px" />
            <asp:BoundField DataField="GenderPreference" HeaderText="Gender Preference" ItemStyle-Width="150px" />
            <asp:BoundField DataField="AllowSmoking" HeaderText="Allow Smoking" ItemStyle-Width="100px" />
            <asp:BoundField DataField="CollegeQualified" HeaderText="College Qualified" ItemStyle-Width="150px" />
            <asp:BoundField DataField="TimeSCC" HeaderText="Time To SCC" ItemStyle-Width="50px" />
            <asp:BoundField DataField="TimeSFCC" HeaderText="TimeTo SFCC" ItemStyle-Width="100px" />
            <asp:BoundField DataField="HSOption" HeaderText="HS Option" ItemStyle-Width="150px" />
            <asp:BoundField DataField="Students" HeaderText="Students" ItemStyle-Width="200px" />
            <asp:BoundField DataField="Address" HeaderText="Address" ItemStyle-Width="300px" />
            <asp:BoundField DataField="LandlinePhone" HeaderText="Landline Phone" ItemStyle-Width="100px" />
        </Columns>
    </asp:GridView>
</asp:Content>