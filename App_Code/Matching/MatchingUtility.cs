﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;

/// <summary>
/// Summary description for Utility
/// </summary>
public class MatchingUtility
{
    public MatchingUtility()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public static Dictionary<string, string[]> AddHomeEnvironmentAttributes(Dictionary<string, string[]> attributes)
    {
        //Add homeEnvironment
        if (attributes.Keys.Any(key => key.ToLower().Contains("home"))) //This condition to make sures to match for "home" environment
        {
            if (attributes.Keys.Any(key => key.Contains("quiet")))
            {
                attributes["homeEnvironment"] = new string[] { "quiet" };
            }
            else if (attributes.Keys.Any(key => key.Contains("active")))
            {
                attributes["homeEnvironment"] = new string[] { "active" };
            }
            else //If family have not selected, default "quiet"
            {
                attributes["homeEnvironment"] = new string[] { "quiet" };
            }
        }
        return attributes;
    }

    public static Dictionary<string, string[]> AddInteractionAttributes(Dictionary<string, string[]> attributes)
    {
        //Add interaction
        if (attributes.ContainsKey("Interaction every day"))
        {
            attributes["interaction"] = new string[] { "everyday" };
        }
        else if (attributes.ContainsKey("Interaction frequent"))
        {
            attributes["interaction"] = new string[] { "frequent" };
        }
        else if (attributes.ContainsKey("Interaction minimal"))
        {
            attributes["interaction"] = new string[] { "minimal" };
        }
        else //If family have not selected, set to "minimal"
        {
            attributes["interaction"] = new string[] { "minimal" };
        }
        return attributes;
    }

    public static Dictionary<string, string[]> AddPrefAttributes(Dictionary<string, string[]> attributes)
    {
        //Add hobbies
        List<string> stuHobbies = new List<string>();
        Homestay hsInfo = new Homestay();
        string sqlQuery = @"SELECT fieldID
                      FROM [CCSInternationalStudent].[dbo].[HomestayPreferences]
                      where id >= 7 AND id <= 33";
        DataTable dtHobbies = hsInfo.RunGetQuery(sqlQuery);
        foreach (DataRow row in dtHobbies.Rows)
        {
            string hobbieS = row["fieldID"].ToString();
            if (attributes.ContainsKey(hobbieS))
            {
                stuHobbies.Add(hobbieS);
            }
        }

        attributes["hobbies"] = stuHobbies.ToArray();
        return attributes;
    }

    public static Dictionary<string, string[]> AddHobbiesAttributes(Dictionary<string, string[]> attributes)
    {
        //Add hobbies
        List<string> stuHobbies = new List<string>();
        Homestay hsInfo = new Homestay();
        string sqlQuery = @"SELECT fieldID
                      FROM [CCSInternationalStudent].[dbo].[HomestayPreferences]
                      where id >= 7 AND id <= 33";
        DataTable dtHobbies = hsInfo.RunGetQuery(sqlQuery);
        foreach (DataRow row in dtHobbies.Rows)
        {
            string hobbieS = row["fieldID"].ToString();
            if (attributes.ContainsKey(hobbieS))
            {
                stuHobbies.Add(hobbieS);
            }
        }

        attributes["hobbies"] = stuHobbies.ToArray();
        return attributes;
    }

    public static Dictionary<string, string[]> AddDiertaryNeedsAttributes(Dictionary<string, string[]> attributes)
    {
        //Add dietary needs
        if (attributes.ContainsKey("NoSpecialDiet"))
        {
            attributes["dietaryNeeds"] = attributes["NoSpecialDiet"];
        }
        else
        {
            attributes["dietaryNeeds"] = new string[] { "TBD" };
            string[] dietPreferences = new string[] { "Vegetarian", "GlutenFree", "DairyFree", "Halal", "Kosher" };
            //Find student's dietary needs
            List<string> stuDietaryNeeds = new List<string>();

            foreach (string s in dietPreferences)
            {
                if (attributes.ContainsKey(s))
                {
                    stuDietaryNeeds.Add(s);
                }
            }
            attributes["dietaryNeeds"] = stuDietaryNeeds.ToArray();
        }
        return attributes;
    }

    public static Dictionary<string, string[]> AddAttributes(Dictionary<string, string[]> attributes)
    {
        // Add new attribute "smoke" to detect if the family is smoker or nonsmoker. !Nonsmoker = smoker

        if (attributes.ContainsKey("Nonsmoker"))
        {
            attributes["smoke"] = new string[] { "no" };
        }
        else
        {
            attributes["smoke"] = new string[] { "yes" };
        }


        //All value for other smoke: Other smoker no preference, Other smoker not OK,Other smoker OK
        //!(Other smoker OK) = (Other smoker no preference, Other smoker OK)
        //Add new attribute "otherSmoke" to detect if the family ok if other smoke

        if (attributes.ContainsKey("Other smoker not OK"))
        {
            attributes["otherSmoke"] = new string[] { "no" };
        }
        else
        {
            attributes["otherSmoke"] = new string[] { "yes" };
        }
        return attributes;
    }

    public static Dictionary<string, string[]> AddChildrenPrefAttributes(Dictionary<string, string[]> attributes)
    {
        //Add childrenPref
        if (attributes.ContainsKey("Young children"))
        {
            attributes["childrenPref"] = new string[] { "young" };
        }
        else if (attributes.ContainsKey("Older children"))
        {
            attributes["childrenPref"] = new string[] { "old" };
        }
        else if (attributes.ContainsKey("No children"))
        {
            attributes["childrenPref"] = new string[] { "no" };
        }
        else
        {
            attributes["childrenPref"] = new string[] { "none" }; // none means "no preference"
        }
        return attributes;
    }

    public static double GetPercentMatch(string[] stuAttributes, string[] famAttributes)
    {
        var attributeIntersect = stuAttributes.Intersect(famAttributes);
        int noAttributeIntersect = attributeIntersect.Count();
        if (stuAttributes.Length == 0)
        {
            return 100; //If student does not have any attributes, match is 100%
        }
        return Math.Round((double)noAttributeIntersect / stuAttributes.Length, 4) * 100;
    }

    public static string PrintEnityObject(EntityObject entityObject)
    {

        StringBuilder sb = new StringBuilder();

        Dictionary<string, string[]> famAttributes = entityObject.Attributes;

        //sb.Append(f.Value.ToString());
        sb.Append("id:" + famAttributes["id"][0] + "<br/>");
        sb.Append("College:" + famAttributes["college"][0] + "<br/>");
        sb.Append("smoke:" + famAttributes["smoke"][0] + "<br/>");
        sb.Append("otherSmoke:" + famAttributes["otherSmoke"][0] + "<br/>");
        sb.Append("gender:" + famAttributes["gender"][0] + "<br/>");
        //sb.Append("homestayOption:" + famAttributes["homestayOption"][0] + "<br/>");
        sb.Append("homestayOption1:" + famAttributes["homestayOption1"][0] + "<br/>");
        sb.Append("dietaryNeeds:" + string.Join(",", famAttributes["dietaryNeeds"]) + "<br/>");
        sb.Append("hobbies:" + string.Join(",", famAttributes["hobbies"]) + "<br/>");
        sb.Append("interaction:" + famAttributes["interaction"][0] + "<br/>");
        sb.Append("homeEnvironment: " + famAttributes["homeEnvironment"][0] + "<br/>");
        sb.Append("children:" + string.Join(",", famAttributes["children"]) + "<br/>");
        sb.Append("room1OccupancyM:" + famAttributes["room1OccupancyM"][0] + "<br/>");
        sb.Append("room2OccupancyM:" + famAttributes["room2OccupancyM"][0] + "<br/>");
        sb.Append("room3OccupancyM:" + famAttributes["room3OccupancyM"][0] + "<br/>");
        sb.Append("room4OccupancyM:" + famAttributes["room4OccupancyM"][0] + "<br/>");

        sb.Append("% hobbies match: " + famAttributes["hobbiesScore"][0] + "<br/>");
        sb.Append("% dietaryNeeds match: " + famAttributes["dietaryScore"][0] + "<br/>");
        sb.Append("% interaction match: " + famAttributes["interactionScore"][0] + "<br/>");
        sb.Append("% homeEnvironment match: " + famAttributes["homeEnvironmentScore"][0] + "<br/>");
        sb.Append("% children match: " + famAttributes["childrenScore"][0] + "<br/>");
        sb.Append("% overall match: " + famAttributes["score"][0] + "<br/><br/>");

        //display 4 room
        for (var i = 1; i <= 4; i++)
        {
            string roomStr = "room" + i;
            if (famAttributes[roomStr].Length > 0)
            {
                sb.Append(roomStr + ": studentID:" + famAttributes[roomStr][0]);
                sb.Append(", effectiveQtr:" + famAttributes[roomStr][2]);
                sb.Append(", placementType: " + famAttributes[roomStr][1] + "<br/>");

            }
            else
            {
                sb.Append(roomStr+ ": " + "N/A" + "<br/>");
            }
        }
        sb.Append("familyStatus: " + famAttributes["familyStatus"][0] + "<br/>");
        return sb.ToString();


    }

    public static Dictionary<string, EntityObject> LoadStudentHM()
    {
        Dictionary<string, EntityObject> stuHM = new Dictionary<string, EntityObject>();
        Homestay hsInfo = new Homestay();
        DataTable dtStudents = hsInfo.GetHomestayStudents("Active", 0);



        //attributes.Add("gender", new string[] { "male" });
        //attributes.Add("college", new string[] { "scc" });
        //attributes.Add("smoke", new string[] { "yes" });
        //attributes.Add("otherSmoke", new string[] { "yes" });

        foreach (DataRow dr in dtStudents.Rows)
        {
            //Create dictionary
            string applicantIDStr = dr["applicantID"].ToString();
            int applicantIDInt = Convert.ToInt32(dr["applicantID"].ToString());
            Dictionary<string, string[]> attributes = new Dictionary<string, string[]>();
            string sqlQuery = @"SELECT  *
                                FROM [CCSInternationalStudent].[dbo].[SchoolInformation]
                                WHERE applicantID =" + applicantIDStr;


            DataTable dt = hsInfo.RunGetQuery(sqlQuery);
            foreach (DataRow row in dt.Rows)
            {
                foreach (DataColumn column in dt.Columns)
                {
                    string columnName = column.ColumnName;
                    string columnData = row[column].ToString();
                    attributes.Add(columnName, new string[] { columnData });
                }
            }

            //Add pref
            sqlQuery = @"SELECT  *
                        FROM [CCSInternationalStudent].[dbo].[HomestayPreferenceSelections]
                        WHERE applicantID =" + applicantIDStr;
            DataTable dtPref = hsInfo.RunGetQuery(sqlQuery);
            foreach (DataRow row in dtPref.Rows)
            {
                attributes[row["preferenceID"].ToString()] = new string[] { row["preferenceID"].ToString() };
            }

            //Add dietary needs
            attributes = MatchingUtility.AddDiertaryNeedsAttributes(attributes);

            //Add attribute "smoker", "othersmoker"
            attributes = MatchingUtility.AddAttributes(attributes);

            //Add attribute "college" for stuyWhere
            attributes["college"] = attributes["studyWhere"];

            //Add Gender
            sqlQuery = @"SELECT  *
                        FROM [CCSInternationalStudent].[dbo].[ApplicantBasicInfo]
                        WHERE id =" + applicantIDStr;
            DataTable dtBasicInfo = hsInfo.RunGetQuery(sqlQuery);
            foreach (DataRow row in dtBasicInfo.Rows)
            {
                string genderStr = row["Gender"].ToString();
                //Convert M to Male, F to Female, O to other
                if (genderStr.Equals("M"))
                {
                    genderStr = "Male";
                }
                else if (genderStr.Equals("F"))
                {
                    genderStr = "Female";
                }
                else if (genderStr.Equals("O"))
                {
                    genderStr = "Other";
                }
                attributes["gender"] = new string[] { genderStr };
            }

            //Add homestayOption * homestayStatus from HomestayStudentInfo
            sqlQuery = @"SELECT  *
                        FROM [CCSInternationalStudent].[dbo].[HomestayStudentInfo]
                        WHERE applicantID =" + applicantIDStr;

            DataTable dtHomestayStudentInfo = hsInfo.RunGetQuery(sqlQuery);

            foreach (DataRow row in dtHomestayStudentInfo.Rows)
            {
                foreach (DataColumn column in dtHomestayStudentInfo.Columns)
                {
                    string columnName = column.ColumnName;
                    string columnData = row[column].ToString();
                    //attributes.Add(columnName, new string[] { columnData });
                    attributes[columnName] = new string[] { columnData };
                    string homestayOptionStr = row["homestayOption"].ToString().ToLower();
                    //Convert to yes/no/either for with food/without food/either
                    if (homestayOptionStr.Equals("with food"))
                    {
                        homestayOptionStr = "yes";
                    }
                    else if (homestayOptionStr.Equals("without food"))
                    {
                        homestayOptionStr = "no";
                    }
                    else
                    {
                        homestayOptionStr = "either";
                    }
                    attributes["homestayOption"] = new string[] { homestayOptionStr };
                    attributes["homestayStatus"] = new string[] { row["homestayStatus"].ToString().Trim() };
                }
                
            }


            //Add hobbies
            attributes = MatchingUtility.AddHobbiesAttributes(attributes);

            //Add interaction
            attributes = MatchingUtility.AddInteractionAttributes(attributes);

            //Add homeEnvironment
            attributes = MatchingUtility.AddHomeEnvironmentAttributes(attributes);

            //Add childrenPref
            attributes = MatchingUtility.AddChildrenPrefAttributes(attributes);

            //Add placed family info
            sqlQuery = @"SELECT *
                        FROM [CCSInternationalStudent].[dbo].[HomestayPlacement]
                        WHERE applicantID =" + applicantIDStr;

            DataTable dtStudentPlacement = hsInfo.RunGetQuery(sqlQuery);
            attributes["familyID"] = new string[] { "NA" };
            attributes["homeName"] = new string[] { "NA" };
            attributes["roomNumber"] = new string[] { "NA" };
            if (dtStudentPlacement != null && dtStudentPlacement.Rows.Count > 0)
            {
                //attributes["familyPlaced"] = new string[] { dtStudentPlacement.Rows[0]["familyID"].ToString() };
                attributes["familyID"] = new string[] { dtStudentPlacement.Rows[0]["familyID"].ToString() };
                attributes["homeName"] = new string[] { dtStudentPlacement.Rows[0]["placementName"].ToString() };
                attributes["roomNumber"] = new string[] { dtStudentPlacement.Rows[0]["roomNumber"].ToString() };
            }

           
            EntityObject entityObject = new EntityObject() { Name = applicantIDStr, Attributes = attributes };
            stuHM.Add(applicantIDStr, entityObject);
        }
        return stuHM;
    }

    public static Dictionary<string, EntityObject> LoadFamilyHM()
    {
        Dictionary<string, EntityObject> famHM = new Dictionary<string, EntityObject>();
        Homestay hsInfo = new Homestay();

        /*
        Dictionary<string, string[]> attributes = new Dictionary<string, string[]>();
        attributes.Add("gender", new string[] { "male" });
        attributes.Add("college", new string[] { "scc" });
        attributes.Add("smoke", new string[] { "yes" });
        attributes.Add("otherSmoke", new string[] { "yes" });
        */

        string connectionStr = ConfigurationManager.ConnectionStrings["ConnectionString_CCSInternationalStudent"].ToString();
        SqlConnection conn = new SqlConnection(connectionStr);
        string sqlQuery = @"SELECT *
                          FROM[CCSInternationalStudent].[dbo].[HomestayFamilyInfo]";


        DataTable dtFamilies = hsInfo.RunGetQuery(sqlQuery);
        foreach (DataRow row in dtFamilies.Rows)
        {
            string idStr = row["id"].ToString();
            Dictionary<string, string[]> attributes = new Dictionary<string, string[]>();
            foreach (DataColumn column in dtFamilies.Columns)
            {
                string columnName = column.ColumnName.Trim();
                string columnData = row[column].ToString().Trim();
                //To replace empty/null value for room occupancy with "No Info"
                if (columnName.ToLower().Contains("occupancy"))
                {
                    if(columnData.Length == 0)
                    {
                        columnData = "No Info";
                    }
                }
                //attributes.Add(columnName, new string[] { columnData });
                attributes[columnName] = new string[] { columnData };//Using this way instead of Add() to avoid exception if exist
            }
            //Add College : to match collegeQualified to college
            attributes["college"] = attributes["collegeQualified"];

            //replace homestayOption with yes/no
            if (attributes["homestayOption"][0].Equals("") || attributes["homestayOption"][0].Contains("Full"))
            {
                attributes["homestayOption1"] = new string[] { "yes" };
            }
            else
            {
                attributes["homestayOption1"] = new string[] { "no" };
            }
            //Add pref
            sqlQuery = @"SELECT  *
                        FROM [CCSInternationalStudent].[dbo].[HomestayPreferenceSelections]
                        WHERE familyID =" + idStr;
            DataTable dtPref = hsInfo.RunGetQuery(sqlQuery);
            foreach (DataRow rowPref in dtPref.Rows)
            {
                attributes[rowPref["preferenceID"].ToString()] = new string[] { rowPref["preferenceID"].ToString() };
            }

            //Add relatives
            sqlQuery = @"SELECT DOB
                        FROM [CCSInternationalStudent].[dbo].[HomestayRelatives]
                        WHERE (relationship LIKE '%child%' OR relationship LIKE '%adult%') AND familyID =" + idStr;
            DataTable dtRevalitves = hsInfo.RunGetQuery(sqlQuery);
            List<string> childList = new List<string>();
            foreach (DataRow rowRelatives in dtRevalitves.Rows)
            {
                DateTime today = DateTime.Today;
                DateTime dob = Convert.ToDateTime(rowRelatives["DOB"]);
                int age = today.Year - dob.Year;

                if (dob > today.AddYears(-age))
                {
                    age--;
                }
                if (age < 5)
                {
                    childList.Add("young");
                }
                else
                {
                    childList.Add("old");
                }

            }
            //attributes["children"] = dtRevalitves.AsEnumerable().Select(s => s.Field<DateTime>("DOB").ToString()).ToArray<string>();
            attributes["children"] = childList.ToArray();

            //Add dietary needs
            attributes = MatchingUtility.AddDiertaryNeedsAttributes(attributes);


            //Add attribute "smoker", "othersmoker"
            attributes = MatchingUtility.AddAttributes(attributes);

            //Add Gender
            attributes["gender"] = attributes["genderPreference"];

            //Add hobbies
            attributes = MatchingUtility.AddHobbiesAttributes(attributes);

            //Add interaction
            attributes = MatchingUtility.AddInteractionAttributes(attributes);

            //Add homeEnvironment
            attributes = MatchingUtility.AddHomeEnvironmentAttributes(attributes);

            //Add rooms
            string roomType = "Occupancy";
            for (var i = 1; i <= 4; i++)
            {
                var keyStr = "room" + i + roomType;
                var keyStrM = keyStr + "M";
                if (attributes[keyStr].Length == 0 || attributes[keyStr][0].Trim().Equals(""))
                {
                    attributes[keyStrM] = new string[] { "N/A" };
                }
                else
                {
                    attributes[keyStrM] = attributes[keyStr];
                }
            }

            //Add placed student info
            //to access values for room1, access by index by this order "studentID" , "placementType"
            //Create empty attributes for 4 rooms
            for (var i = 1; i <= 4; i++)
            {
                attributes["room"+i] = new string[]{};
            }
            sqlQuery = @"SELECT *
                       FROM [CCSInternationalStudent].[dbo].[HomestayPlacement]
                       WHERE familyID =" + idStr;
            DataTable dtPlacements = hsInfo.RunGetQuery(sqlQuery);
            foreach (DataRow rowPlacement in dtPlacements.Rows)
            {
                string roomNoStr = "room" + rowPlacement["roomNumber"].ToString().Trim();
                attributes[roomNoStr] = new string[] 
                                        {
                                            rowPlacement["applicantID"].ToString().Trim()
                                            ,rowPlacement["placementType"].ToString().Trim()
                                            ,rowPlacement["effectiveQuarter"].ToString().Trim()
                                        };
            }

            //attributes["room1"] = new string[] {"studentID" , "placementType" };
            //Add familyStatus
            int countOccupiedRoom = 0;
            string updatedFamilyStatus = "";
            for (var i = 1; i <= 4; i++)
            {
                if (attributes["room" + i].Length > 0)
                {
                    countOccupiedRoom++;
                }
            }
            if (countOccupiedRoom == 0)
            {
                updatedFamilyStatus = "Open room(s)";
            }
            else if (countOccupiedRoom == 4)
            {
                updatedFamilyStatus = "Rooms Full";
            }
            else
            {
                updatedFamilyStatus = "Open Room-Student Placed";
            }
            //update famHM
            attributes["familyStatus"] = new string[] { updatedFamilyStatus };

            EntityObject entityObject = new EntityObject() { Name = idStr, Attributes = attributes };
            famHM.Add(idStr, entityObject);
        }

        //Detect smoker/nonsmoker and Add new attributes 


        //Filter based on open rooms. If all 4 rooms are occupied, exlcude that room
        Dictionary<string, EntityObject> filteredFamilies = new Dictionary<string, EntityObject>();
        string[] rooms = { "room1Occupancy", "room2Occupancy", "room3Occupancy", "room4Occupancy" };
        foreach (var f in famHM)
        {
            Dictionary<string, string[]> attributes = f.Value.Attributes;
            int count = 0;
            foreach (var r in rooms)
            {
                if (attributes[r][0].ToLower().Contains("ccs")) //for ccs and non-ccs
                {
                    count++;
                }

            }
            
            if (count < 4)
            {
                filteredFamilies.Add(f.Key, new EntityObject { Name = f.Key, Attributes = attributes });
            }

        }
        return filteredFamilies;


        //return famHM;
    }

    public static Dictionary<string, EntityObject> MatchStuFamCollege(string stuCol, Dictionary<string, EntityObject> famHM)
    {
        Dictionary<string, EntityObject> famHMFiltered = new Dictionary<string, EntityObject>();
        foreach (var f in famHM)
        {
            string famCol = f.Value.Attributes["college"][0].ToLower();
            if (stuCol.Equals(famCol) || famCol.Equals("either")) //"either" will match with scc/sfcc{
            {
                famHMFiltered.Add(f.Key, f.Value);
            }
        }
        return famHMFiltered;
    }

    public static Dictionary<string, EntityObject> MatchStuFamSmoking(EntityObject stuObj, Dictionary<string, EntityObject> famHM)
    {
        Dictionary<string, EntityObject> resultHM = new Dictionary<string, EntityObject>();
        Dictionary<string, string[]> stuAttributes = stuObj.Attributes;
        string studentSmoke = stuAttributes["smoke"][0];
        string studentOtherSmoke = stuAttributes["otherSmoke"][0];


        foreach (var f in famHM)
        {
            string familyName = f.Key;
            Dictionary<string, string[]> famAttributes = f.Value.Attributes;
            string familySmoke = famAttributes["smoke"][0];
            string familyOtherSmoke = famAttributes["otherSmoke"][0];

            if (GetStuFamSmokingMatched(studentSmoke, studentOtherSmoke, familySmoke, familyOtherSmoke))
            {
                resultHM.Add(f.Key, f.Value);
            }
        }
        return resultHM;
    }
    public static Dictionary<string, EntityObject> MatchStuFamGender(EntityObject stuObj, Dictionary<string, EntityObject> famHM)
    {
        Dictionary<string, EntityObject> resultHM = new Dictionary<string, EntityObject>();
        Dictionary<string, string[]> stuAttributes = stuObj.Attributes;
        string studentGender = stuAttributes["gender"][0].ToLower();


        foreach (var f in famHM)
        {
            bool isMatched = false;
            string familyName = f.Key;
            Dictionary<string, string[]> famAttributes = f.Value.Attributes;
            string familyGender = famAttributes["gender"][0].ToLower();


            if (familyGender.Contains(studentGender))
            {
                isMatched = true;
            }

            if (isMatched)
            {
                resultHM.Add(f.Key, f.Value);
            }
        }
        return resultHM;
    }

    public static Dictionary<string, EntityObject> MatchStuFamHomestayOption(EntityObject stuObj, Dictionary<string, EntityObject> famHM, string stext)
    {
        Dictionary<string, EntityObject> resultHM = new Dictionary<string, EntityObject>();
        Dictionary<string, string[]> stuAttributes = stuObj.Attributes;
        string stuHomestayOption = stuAttributes["homestayOption"][0].ToLower();
        //If student does not have option, all family are satisfied
        if (stuHomestayOption.Equals("no") || stuHomestayOption.Equals("either"))
        {
            return famHM;
        }

        //If student does have option, but select "no preference" for dietary needs, all "yes" family are satisfied
        if (stuAttributes.ContainsKey("NoSpecialDiet"))
        {
            //return famHM;
        }

        string[] studentdietaryNeeds = stuAttributes["dietaryNeeds"];


        //If student has option (yes), and select some diet needs, all fam's dieratary needs must match with stu's needs
        foreach (var f in famHM)
        {
            //Check if family select "NoSpecialDiet", which means cannot accomodate student's diet needs, exclude that family
            Dictionary<string, string[]> famAttributes = f.Value.Attributes;
            string famHomestayOption = famAttributes["homestayOption1"][0].ToLower();

            //If family does not provide food, exclude
            if (famHomestayOption.Equals("no"))
            {
                //exclude this family
            }
            else
            {
                if (stuAttributes.ContainsKey("NoSpecialDiet"))
                {
                    resultHM.Add(f.Key, f.Value);
                }
                else
                {
                    if (famAttributes.ContainsKey("NoSpecialDiet")) //if student have dietary but fam cannot provide
                    {
                        //Exclude this family
                    }
                    else
                    {
                        string[] familydietaryNeeds = famAttributes["dietaryNeeds"];
                        if (studentdietaryNeeds.Length == 0)
                        {
                            return famHM;
                        }
                        else
                        {
                            if (familydietaryNeeds.Length == 0)
                            {
                                //If student has some needs, family has not selected any, return empty
                                return resultHM;
                            }
                            else
                            {
                                bool isSubset = studentdietaryNeeds.All(elem => familydietaryNeeds.Contains(elem));
                                if (isSubset)
                                {
                                    resultHM.Add(f.Key, f.Value);
                                }
                            }
                        }
                    }
                }

            }
        }
        return resultHM;
    }
    private static bool GetStuFamSmokingMatched(string studentSmoke, string studentOtherSmoke, string familySmoke, string familyOtherSmoke)
    {
        //bool matched = false;
        if (studentSmoke.Equals("yes") && studentOtherSmoke.Equals("yes") && familyOtherSmoke.Equals("yes"))
        {
            return true;
        }
        else if (studentSmoke.Equals("yes") && studentOtherSmoke.Equals("no") && familySmoke.Equals("no") && familyOtherSmoke.Equals("yes"))
        {
            return true;
        }
        else if (studentSmoke.Equals("no") && studentOtherSmoke.Equals("yes"))
        {
            return true;
        }
        else if (studentSmoke.Equals("no") && studentOtherSmoke.Equals("no") && familySmoke.Equals("no") && familyOtherSmoke.Equals("no"))
        {
            return true;
        }
        return false; ;
    }

    public static string PrintHM2(EntityObject stuObj, Dictionary<string, EntityObject> hm)
    {
        /*
        Dictionary<string, string[]> stuAttributes = stuObj.Attributes;
        string[] stuHobbies = stuAttributes["hobbies"];
        string[] stuDietary = stuAttributes["dietaryNeeds"];
        string[] stuInteraction = stuAttributes["interaction"];
        string[] stuHomeEnvironment = stuAttributes["homeEnvironment"];
        */
        StringBuilder sb = new StringBuilder();
        foreach (var f in hm)
        {
            //sb.Append("main ID:" + f.Key + ", ");
            Dictionary<string, string[]> famAttributes = f.Value.Attributes;


            //sb.Append(f.Value.ToString());
            sb.Append("id:" + famAttributes["id"][0] + ", ");
            sb.Append("College:" + famAttributes["college"][0] + ", ");
            sb.Append("smoke:" + famAttributes["smoke"][0] + ", ");
            sb.Append("otherSmoke:" + famAttributes["otherSmoke"][0] + ", ");
            sb.Append("gender:" + famAttributes["gender"][0] + ", ");
            sb.Append("homestayOption:" + famAttributes["homestayOption"][0] + ", ");
            sb.Append("homestayOption1:" + famAttributes["homestayOption1"][0] + ", ");
            sb.Append("dietaryNeeds:" + string.Join(",", famAttributes["dietaryNeeds"]) + ", ");
            sb.Append("hobbies:" + string.Join(",", famAttributes["hobbies"]) + ", ");
            sb.Append("interaction:" + famAttributes["interaction"][0] + ", ");
            sb.Append("score:" + famAttributes["score"][0] + ", ");
            sb.Append("homeEnvironment: " + famAttributes["homeEnvironment"][0] + "<br/>");
            sb.Append("% hobbies match: " + famAttributes["hobbiesScore"][0] + "<br/>");
            sb.Append("% dietaryNeeds match: " + famAttributes["dietaryScore"][0] + "<br/>");
            sb.Append("% interaction match: " + famAttributes["interactionScore"][0] + "<br/>");
            sb.Append("% homeEnvironment match: " + famAttributes["homeEnvironmentScore"][0] + "<br/>");
            sb.Append("% overall match: " + famAttributes["score"][0] + "<br/><br/>");
            
            
        }
        return sb.ToString();

    }


    public static Dictionary<string, EntityObject> FindIntersection(List<Dictionary<string, EntityObject>> matchingList)
    {
        Dictionary<string, EntityObject> result = new Dictionary<string, EntityObject>();
        if (matchingList.Count() == 1)
        {
            return matchingList[0];
        }
        Dictionary<string, EntityObject> firstHM = matchingList[0];

        for (var i = 1; i < matchingList.Count(); i++)
        {
            Dictionary<string, EntityObject> anotherHM = matchingList[i];
            result = firstHM.Where(x => anotherHM.ContainsKey(x.Key))
                         .ToDictionary(x => x.Key, x => x.Value);
            //update firstHM for finding intersect
            firstHM = result;
        }
        return result;
    }


    public static string PrintHM(Dictionary<string, EntityObject> hm)
    {
        StringBuilder sb = new StringBuilder();

        foreach (var f in hm)
        {
            //sb.Append("main ID:" + f.Key + ", ");
            Dictionary<string, string[]> attributes = f.Value.Attributes;

            //sb.Append(f.Value.ToString());
            sb.Append("id:" + attributes["id"][0] + ", ");
            sb.Append("College:" + attributes["college"][0] + ", ");
            sb.Append("smoke:" + attributes["smoke"][0] + ", ");
            sb.Append("otherSmoke:" + attributes["otherSmoke"][0] + ", ");
            sb.Append("gender:" + attributes["gender"][0] + ", ");
            sb.Append("homestayOption:" + attributes["homestayOption"][0] + ", ");
            sb.Append("homestayOption1:" + attributes["homestayOption1"][0] + ", ");
            sb.Append("dietaryNeeds:" + string.Join(",", attributes["dietaryNeeds"]) + ", ");
            sb.Append("hobbies:" + string.Join(",", attributes["hobbies"]) + ", ");
            sb.Append("interaction:" + attributes["interaction"][0] + ", ");
            sb.Append("homeEnvironment:" + attributes["homeEnvironment"][0] + ", ");
            sb.Append("room1OccupancyM:" + attributes["room1OccupancyM"][0] + ", ");
            sb.Append("room2OccupancyM:" + attributes["room2OccupancyM"][0] + ", ");
            sb.Append("room3OccupancyM:" + attributes["room3OccupancyM"][0] + ", ");
            sb.Append("room4OccupancyM:" + attributes["room4OccupancyM"][0] + "<br/>");
        }
        return sb.ToString();

    }

    public static Dictionary<string, EntityObject> AddScore(EntityObject stuObj, Dictionary<string, EntityObject> hm)
    {
        Dictionary<string, string[]> stuAttributes = stuObj.Attributes;
        string[] stuHobbies = stuAttributes["hobbies"];
        string[] stuDietary = stuAttributes["dietaryNeeds"];
        string[] stuInteraction = stuAttributes["interaction"];
        string[] stuHomeEnvironment = stuAttributes["homeEnvironment"];
        string[] stuChildren = stuAttributes["childrenPref"];
        foreach (var f in hm)
        {
            //sb.Append("main ID:" + f.Key + ", ");
            Dictionary<string, string[]> famAttributes = f.Value.Attributes;

            double score = 0;
            //Match Children
            string[] famChildren = famAttributes["children"];

            string stuChildrenSelection = stuChildren[0];
            double percentChildrenMatch = 0;
            if (stuChildrenSelection.Equals("no"))
            {
                if (famChildren.Count() > 0) //If family has children, 0%
                {
                    percentChildrenMatch = 0;
                }
                else
                {
                    percentChildrenMatch = 100;
                }
            }
            else if (stuChildrenSelection.Equals("none")) //no preference
            {
                percentChildrenMatch = 100;
            }
            else if (stuChildrenSelection.Equals("young") || stuChildrenSelection.Equals("old")) // percent of selection in family. Exp: stu select "young", in family has 2 young, 2 old, means percent = 50%
            {
                if (famChildren.Count() == 0)
                {
                    percentChildrenMatch = 0;
                }
                else
                {
                    //Count number of selection in family
                    int countSelection = famChildren.Count(e => e == stuChildrenSelection);

                    percentChildrenMatch = Math.Round((double)countSelection / famChildren.Count(), 2) * 100;
                }

            }
            famAttributes["childrenScore"] = new string[] { percentChildrenMatch + "" };
            score += percentChildrenMatch * 20 / 100;

            //Match hobbies
            string[] famHobbies = famAttributes["hobbies"];
            double percentHobbiesMatch = MatchingUtility.GetPercentMatch(stuHobbies, famHobbies);
            famAttributes["hobbiesScore"] = new string[] { percentHobbiesMatch + "" };

            score += percentHobbiesMatch * 20 / 100;

            //Match dietary needs
            string[] famDietary = famAttributes["dietaryNeeds"];
            double percentDietaryMatch = 0;


            if (stuDietary.Length == 0 || stuDietary[0].Equals("NoSpecialDiet"))//if the student does not have any dietary needs, 100% match with family
            {
                percentDietaryMatch = 100;
            }
            else
            {

                if (famDietary.Length == 0 || famDietary[0].Equals("NoSpecialDiet")) //if the student has dietary needs & family cannot provide, 0% match
                {
                    percentDietaryMatch = 0;
                }
                else //if the student has dietary needs & family has selected from dietary list, start matching from 2 lists
                {
                    percentDietaryMatch = MatchingUtility.GetPercentMatch(stuDietary, famDietary);
                }
            }
            famAttributes["dietaryScore"] = new string[] { percentDietaryMatch + "" };
            score += percentDietaryMatch * 10 / 100;

            //Match Interaction
            string[] famInteraction = famAttributes["interaction"];
            double percentInteractionMatch = MatchingUtility.GetPercentMatch(stuInteraction, famInteraction);
            famAttributes["interactionScore"] = new string[] { percentInteractionMatch + "" };
            score += percentInteractionMatch * 25 / 100;

            //Match homeEnvironment
            string[] famHomeEnvironment = famAttributes["homeEnvironment"];
            double percentHomeEnvironmentMatch = MatchingUtility.GetPercentMatch(stuHomeEnvironment, famHomeEnvironment);
            famAttributes["homeEnvironmentScore"] = new string[] { percentHomeEnvironmentMatch + "" };
            score += percentHomeEnvironmentMatch * 25 / 100;

            famAttributes["score"] = new string[] { score + "" };
        }
        return hm;

    }

    public bool overlapPeriods(DateTime startA, DateTime endA, DateTime startB, DateTime endB)
    {
        return DateTime.Compare(startA, endB) < 0 && DateTime.Compare(startB, endA) < 0;
        //return tStartA < tEndB && tStartB < tEndA;
    }
}