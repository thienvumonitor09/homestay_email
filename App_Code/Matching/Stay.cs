﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Stay
/// </summary>
public class Stay
{
    public DateTime Start { get; set; }
    public DateTime End { get; set; }

    public Stay()
    {

    }
    public Stay(DateTime start, DateTime end)
    {
        this.Start = start;
        this.End = end;
    }

    public Stay(string start, string end)
    {
        this.Start = DateTime.Parse(start);
        try
        {
            this.End = DateTime.Parse(end);
        }
        catch (FormatException e)
        {
            this.End = DateTime.MaxValue;
        }
    }

    public override string ToString()
    {
        return Start.ToString() + " " + End.ToString() + " \n";
    }
}
    