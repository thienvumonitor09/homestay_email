﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Homestay_Families_ApplicationForm_V2 : System.Web.UI.Page
{
    string strResultMsg = "";
    Homestay hsInfo = new Homestay();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            // populate the family drop - down
            DataTable dtFamilies = hsInfo.GetHomestayFamilyInfo(0, "", "DDLactive");
            if (dtFamilies != null)
            {
                if (dtFamilies.Rows.Count > 0)
                {
                    //ddlFamilies.Items.Clear();
                    System.Web.UI.WebControls.ListItem LI;
                    LI = new System.Web.UI.WebControls.ListItem();
                    LI.Value = "0";
                    LI.Text = "-- Add New Family --";
                    ddlFamilies.Items.Add(LI);
                    foreach (DataRow dr in dtFamilies.Rows)
                    {
                        LI = new System.Web.UI.WebControls.ListItem();
                        LI.Value = dr["id"].ToString();
                        LI.Text = dr["homeName"].ToString();
                        ddlFamilies.Items.Add(LI);
                    }
                }
                else { strResultMsg += "Error: dtFamilies has no rows.<br />"; }
                if (Request.QueryString["ID"] != null && Request.QueryString["ID"].ToString() != "0")
                {
                    
                    ddlFamilies.SelectedValue = Request.QueryString["ID"].ToString();
                }
            }
        }
    }



    protected void Wizard1_FinishButtonClick(object sender, WizardNavigationEventArgs e)
    {
        Response.Redirect("PlacementProfile.aspx?ID=" + 39);
    }
}