﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Homestay_Admin_Reports_PlacementForm_V3 : System.Web.UI.Page
{
    string strResultMsg = "";
    String selectedStudent = "0";
    DateTime dob;
    String arrivalDate;
    String shortDate = "";
    Homestay hsInfo = new Homestay();
    DataTable dtDisplay;
    HomestayUtility hsUtility = new HomestayUtility();
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {

        
        //Get studentID from querystring
        int intStudentID = GetQueryStringValue("studentID");
        dtDisplay = GetDisplayDt();
        //Populate ddlStudent
        if (!IsPostBack)
        {
           

        }// NOT IsPostback
       
        //litArea.Text = ConvertDataTableToHTML(dtDisplay);
        //ddlStudent.SelectedValue = intStudentID+"";
        //GetDisplayDt();
        //BindDetailView();
        //selectedStudent = ddlStudent.SelectedValue;

        //DataTable dtStudentExtended2 = hsInfo.GetHomestayStudents("ID", Convert.ToInt32("0" + ddlStudent.SelectedValue));



    }

    private static string GetShortDateString(string dateTimeStr)
    {
        DateTime arrivalDate;
        return DateTime.TryParse(dateTimeStr, out arrivalDate) ? arrivalDate.ToShortDateString() : "n/a";
    }
    protected void linkBtnHome_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Homestay/Admin/Default.aspx");
    }

    
    private DataTable GetDisplayDt()
    {
        int intStudentID = GetQueryStringValue("studentID");
        //int intStudentID = hsUtility.ParseInt(ddlStudent.SelectedValue);
        DataTable dtDisplay = new DataTable();
        dtDisplay.Columns.AddRange(new DataColumn[13]
                    {

                        new DataColumn("Campus", typeof(string)),
                        new DataColumn("QuarterOfArrival", typeof(string)),
                        new DataColumn("OrientationStartDate", typeof(string)),
                        new DataColumn("QuarterStartDate", typeof(string)),
                        new DataColumn("StudentName", typeof(string)),
                        new DataColumn("StudentGender", typeof(string)),
                        new DataColumn("DOB", typeof(string)),
                        new DataColumn("Country", typeof(string)),
                        new DataColumn("ProgramOfStudy", typeof(string)),
                        new DataColumn("Email", typeof(string)),
                        new DataColumn("Hobbies", typeof(string)),
                        new DataColumn("ArrivalInfo", typeof(string)),
                        new DataColumn("UrgentCare", typeof(string))
                    });
        DataRow displayRow = dtDisplay.NewRow();
        DataTable dtStudentExtended = hsInfo.GetHomestayStudents("ID", intStudentID); 
        //OrientationStartDate
        //displayRow["OrientationStartDate"] = "demo";
        if (dtStudentExtended != null && dtStudentExtended.Rows.Count > 0)
        {
            DataRow drStudent = dtStudentExtended.Rows[0];
            string sqlQuerySchoolInfo = @"SELECT *
                                  FROM [CCSInternationalStudent].[dbo].[SchoolInformation]
                                  where applicantID = " + intStudentID;
            DataTable dtSchoolInfo = hsInfo.RunGetQuery(sqlQuerySchoolInfo);
            //DataTable dtStudentMore = hsInfo.GetStudentStudyInfo(Convert.ToInt32( ddlStudent.SelectedValue));
            if (dtSchoolInfo != null && dtSchoolInfo.Rows.Count > 0)
            {
                DataRow drSchoolInfo = dtSchoolInfo.Rows[0];
                //Campus
                string campus = drSchoolInfo["studyWhere"].ToString();
                if (campus.Contains("SCC"))
                {
                    campus = "Spokane Community College - SCC";
                }
                else if (campus.Contains("SFCC"))
                {
                    campus = "Spokane Falls Community College - SFCC";
                }
                else if (campus.Contains("Pullman"))
                {
                    campus = "Pullman";
                }
                else
                {
                    campus = "Other";
                }
                displayRow["Campus"] = campus;

                //ProgramOfStudy
                displayRow["ProgramOfStudy"] = drSchoolInfo["studyWhat"].ToString().Trim().Replace("''", "'")
                                            + " - " + drSchoolInfo["studyMajor"].ToString().Trim().Replace("''", "'")
                                            + drSchoolInfo["studyMajorOther"].ToString().Trim().Replace("''", "'");
            }

            //QuarterOfArrival
            displayRow["QuarterOfArrival"] = GetShortDateString(drStudent["arrivalDate"].ToString());

            //StudentName
            displayRow["StudentName"] = drStudent["firstName"].ToString().Trim().Replace("''", "'")
                                        + " " + drStudent["middleNames"].ToString().Trim().Replace("''", "'")
                                        + " " + drStudent["familyName"].ToString().Trim().Replace("''", "'");
            //Gender
            string gender = drStudent["Gender"].ToString();
            if (gender.Equals("M"))
            {
                gender = "Male";
            }
            else if (gender.Equals("F"))
            {
                gender = "Female";
            }
            else
            {
                gender = "Other";
            }
            displayRow["StudentGender"] = gender;

            
            //DOB
            displayRow["DOB"] = GetShortDateString(drStudent["DOB"].ToString());

            //Country
            displayRow["Country"] = drStudent["countryOfCitizenship"].ToString();

            //Email
            DataTable dtInfo = hsInfo.GetOneInternationalStudent("", intStudentID, "", "", "", "");
            if (dtInfo != null && dtInfo.Rows.Count > 0)
            {
                displayRow["Email"] = dtInfo.Rows[0]["Email"].ToString();
            }
            //Hobbies
            
            List<string> hobbiesList = hsUtility.GetPreferenceSelectionsByType("hobbies","student",intStudentID);
            displayRow["Hobbies"] = string.Join(", ", hobbiesList.ToArray());

            //UrgentCare
            displayRow["UrgentCare"] = "$20 co-pay";

        }

        

        dtDisplay.Rows.Add(displayRow);
        return dtDisplay;
    }

    private static DataTable GetDisplayDt2()
    {
        int intStudentID = 99;
        //int intStudentID = hsUtility.ParseInt(ddlStudent.SelectedValue);
        DataTable dtDisplay = new DataTable();
        dtDisplay.Columns.AddRange(new DataColumn[13]
                    {

                        new DataColumn("Campus", typeof(string)),
                        new DataColumn("QuarterOfArrival", typeof(string)),
                        new DataColumn("OrientationStartDate", typeof(string)),
                        new DataColumn("QuarterStartDate", typeof(string)),
                        new DataColumn("StudentName", typeof(string)),
                        new DataColumn("StudentGender", typeof(string)),
                        new DataColumn("DOB", typeof(string)),
                        new DataColumn("Country", typeof(string)),
                        new DataColumn("ProgramOfStudy", typeof(string)),
                        new DataColumn("Email", typeof(string)),
                        new DataColumn("Hobbies", typeof(string)),
                        new DataColumn("ArrivalInfo", typeof(string)),
                        new DataColumn("UrgentCare", typeof(string))
                    });
        DataRow displayRow = dtDisplay.NewRow();
        Homestay hsInfo = new Homestay();
        DataTable dtStudentExtended = hsInfo.GetHomestayStudents("ID", intStudentID);
        //OrientationStartDate
        //displayRow["OrientationStartDate"] = "demo";
        if (dtStudentExtended != null && dtStudentExtended.Rows.Count > 0)
        {
            DataRow drStudent = dtStudentExtended.Rows[0];
            string sqlQuerySchoolInfo = @"SELECT *
                                  FROM [CCSInternationalStudent].[dbo].[SchoolInformation]
                                  where applicantID = " + intStudentID;
            DataTable dtSchoolInfo = hsInfo.RunGetQuery(sqlQuerySchoolInfo);
            //DataTable dtStudentMore = hsInfo.GetStudentStudyInfo(Convert.ToInt32( ddlStudent.SelectedValue));
            if (dtSchoolInfo != null && dtSchoolInfo.Rows.Count > 0)
            {
                DataRow drSchoolInfo = dtSchoolInfo.Rows[0];
                //Campus
                string campus = drSchoolInfo["studyWhere"].ToString();
                if (campus.Contains("SCC"))
                {
                    campus = "Spokane Community College - SCC";
                }
                else if (campus.Contains("SFCC"))
                {
                    campus = "Spokane Falls Community College - SFCC";
                }
                else if (campus.Contains("Pullman"))
                {
                    campus = "Pullman";
                }
                else
                {
                    campus = "Other";
                }
                displayRow["Campus"] = campus;

                //ProgramOfStudy
                displayRow["ProgramOfStudy"] = drSchoolInfo["studyWhat"].ToString().Trim().Replace("''", "'")
                                            + " - " + drSchoolInfo["studyMajor"].ToString().Trim().Replace("''", "'")
                                            + drSchoolInfo["studyMajorOther"].ToString().Trim().Replace("''", "'");
            }

            //QuarterOfArrival
            displayRow["QuarterOfArrival"] = GetShortDateString(drStudent["arrivalDate"].ToString());

            //StudentName
            displayRow["StudentName"] = drStudent["firstName"].ToString().Trim().Replace("''", "'")
                                        + " " + drStudent["middleNames"].ToString().Trim().Replace("''", "'")
                                        + " " + drStudent["familyName"].ToString().Trim().Replace("''", "'");
            //Gender
            string gender = drStudent["Gender"].ToString();
            if (gender.Equals("M"))
            {
                gender = "Male";
            }
            else if (gender.Equals("F"))
            {
                gender = "Female";
            }
            else
            {
                gender = "Other";
            }
            displayRow["StudentGender"] = gender;


            //DOB
            displayRow["DOB"] = GetShortDateString(drStudent["DOB"].ToString());

            //Country
            displayRow["Country"] = drStudent["countryOfCitizenship"].ToString();

            //Email
            DataTable dtInfo = hsInfo.GetOneInternationalStudent("", intStudentID, "", "", "", "");
            if (dtInfo != null && dtInfo.Rows.Count > 0)
            {
                displayRow["Email"] = dtInfo.Rows[0]["Email"].ToString();
            }
            //Hobbies
            HomestayUtility hsUtility = new HomestayUtility();
            List<string> hobbiesList = hsUtility.GetPreferenceSelectionsByType("hobbies", "student", intStudentID);
            displayRow["Hobbies"] = string.Join(", ", hobbiesList.ToArray());

            //UrgentCare
            displayRow["UrgentCare"] = "$20 co-pay";

        }



        dtDisplay.Rows.Add(displayRow);
        return dtDisplay;
    }






    protected void Button1_Click(object sender, EventArgs e)
    {
        Process.Start(String.Format(
             "mailto:{0}?subject={1}&cc={2}&bcc={3}&body={4}",
              "", "", "", "", ""));
    }


    /*
    // This Method is used to render gridview control
    public string GetGridviewData(DetailsView gv)
    {
        DetailsView gv2 = gv;

        StringBuilder strBuilder = new StringBuilder();
        StringWriter strWriter = new StringWriter(strBuilder);
        HtmlTextWriter htw = new HtmlTextWriter(strWriter);
        gv2.Rows[13].Visible = false; //Hide commandfield
        gv2.RenderControl(htw);
        gv2.Rows[13].Visible = true; //Unhide commandfield
        return strBuilder.ToString();
    }

    //This function to prevent exception when render page
    public override void VerifyRenderingInServerForm(Control control)
    {
        return;
    }
    */
    protected void SendMail_Click(object sender, EventArgs e)
    {
        //public string SendEmailMsg(string strTo, string strFrom, string strSubject, bool blIsHTML, string strBody, string strCCEmailAddresses)


        //string body = GetGridviewData(dvPlacement);
        //body += lblInfo.Text;

        int intFamilyID = GetQueryStringValue("familyID");
        string result = "";


        //result = utility.SendEmailMsg(toEmail, "Internationalhomestay@ccs.spokane.edu", "New Student Placement", true, body, txtCC.Text);
        string body = ""; 
            //litArea.Text;
        
        result = utility.SendEmailMsg("vu.nguyen@ccs.spokane.edu", "Internationalhomestay@ccs.spokane.edu", "New Student Placement", true, body, "");
        if (result.Contains("Success"))
        {
            //litResultMsg.Text = "An Email has been sent to Primary Contact of the Family.";
        }


    }
    public int GetQueryStringValue(string queryString)
    {
        int id = 0;
        if ((Request.QueryString[queryString] != null && Request.QueryString[queryString].ToString() != "0"))
        {
            id = hsUtility.ParseInt(Request.QueryString[queryString]);
        }
        return id;
    }

   
    public static string ConvertDataTableToHTML(DataTable dt)
    {
        string html = @"<table cellspacing=""0"" cellpadding =""10"" rules =""all"" border =""1"" id =""MainContent_dvPlacement"" style =""font - size:Large; width: 100 %; border - collapse:collapse; "" > ";
        //add header row
        html += "<tbody>";
       
        
        //add rows
        for (int i = 0; i < dt.Columns.Count; i++)
        {
            html += "<tr>";
            html += @"<td style=""background - color:#E4E4E4;font-weight:bold;height:50px;width:30%;"">" + dt.Columns[i].ColumnName + "</td>";
            html += "<td>" + dt.Rows[0][i].ToString() + "</td>";

            html += "</tr>";
        }
        html += "</tbody>";
        html += "</table>";
        return html;
    }

    [WebMethod]
    public static string GetString()
    {
        string table = ConvertDataTableToHTML(GetDisplayDt2());
        return table;
    }
}