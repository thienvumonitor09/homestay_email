﻿<%@ Page Title="List of Active Homestay Students" Language="C#" AutoEventWireup="true" MasterPageFile="~/CCSApps.master" CodeFile="ListActive.aspx.cs" Inherits="Homestay_Admin_Reports_ListActive" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
<style type="text/css">
    p
    {
        padding:2px;
        margin:0px;
    }
    .showHide {  
        cursor: pointer;  
    }  

td.details-control {
    background: url('../../images/details_open.png') no-repeat center center;
    cursor: pointer;
}
tr.shown td.details-control {
    background: url('../../images/details_close.png') no-repeat center center;
}

td {
  text-align: left;
}
tfoot {
    display: table-header-group;
}
</style>
    
   
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css"> 
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
   
   <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.bootstrap.min.css"> 
 <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css"> 

    <!--<script type="text/javascript"  src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script> -->
    <!--<script type="text/javascript"  src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.bootstrap.min.js"></script> -->
   

<script type="text/javascript"  src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript"  src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript"  src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript"  src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
    <script type="text/javascript"  src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script type="text/javascript"  src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>

<script type="text/javascript"  src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<!--<script type="text/javascript"  src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script> -->
    <script type="text/javascript"  src="https://nightly.datatables.net/buttons/js/buttons.html5.min.js"></script>
    <script type="text/javascript"  src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
   <script>
       function format(d) {
           // `d` is the original data object for the row
           var tbl = $('<table cellpadding="5" cellspacing="0" border="1" style="width:70%;padding-left:50px; margin-left:90px;"/>');
           tbl.append('<tr>' +
                           '<td><strong>ctcID:</strong></td>' +
                           '<td>' + d.Id + '</td>' +
                        '</tr>');
           tbl.append('<tr>' +
                           '<td><strong>Email: </strong></td>' +
                           '<td>' + d.Email + '</td>' +
                       '</tr>');
           tbl.append('<tr>' +
                           '<td><strong>Cell Phone (Local number): </strong></td>' +
                           '<td>' + d.CellPhone + '</td>' +
                        '</tr>');
           tbl.append('<tr>' +
                           '<td><strong> Mailing address : </strong></td>' +
                           '<td>' + d.LocalAddress + '</td>' +
               '</tr>');
           tbl.append('<tr>' +
                           '<td><strong> Success Meeting : </strong></td>' +
                           '<td>' + d.SuccessMeeting + '</td>' +
               '</tr>');
           tbl.append('<tr>' +
                           '<td><strong> Agreement : </strong></td>' +
                           '<td>' + d.Agreement + '</td>' +
                      '</tr>' );
           tbl.append('<tr>' +
                           '<td><strong> Notes : </strong></td>' +
                           '<td>' + d.Notes + '</td>' +
                      '</tr>' );
           return tbl;
           /*
           return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px; margin-left:90px;">' +
               '<tr>' +
                   '<td><strong>ctcID:</strong></td>' +
                   '<td>' + d.Id + '</td>' +
               '</tr>' +
               '<tr>' +
                   '<td><strong>Email: </strong></td>' +
                   '<td>' + d.Email + '</td>' +
               '</tr>' +
               '<tr>' +
               '<td><strong>Cell Phone (Local number): </strong></td>' +
               '<td>' + d.CellPhone + '</td>' +
               '</tr>' +
               '<tr>' +
                   '<td><strong> Mailing address : </strong></td>' +
                   '<td>And any further details here (images etc)...</td>' +
               '</tr>' +
               '</table>';
           */
       }
       $.fn.dataTable.ext.search.push(
           function (settings, data, dataIndex) {
               var filterVal = parseInt($('#ageFilterID').val(), 10);
               var age = parseFloat(data[5]) || 0; // use data for the age column
               if (isNaN(filterVal)){
                   return true;
               } else {
                   if (filterVal == 18){
                       if (age > 17){
                           return true;
                       }
                   } else if (filterVal == 17){
                       if (age <= 17) {
                           return true;
                       }
                   }
                   return false;
               }
               return false;
               
           }
       );

       $(function () {
           
           $('#clearFilter').on('click', function (e) {
               var table = $('#studentTable').DataTable();
               table.search('').columns().search('').draw();
           });

           /*
           var buttonCommon = {
               exportOptions: {
                   columns: 'th:not(:first-child)',
                   format: {
                       body: function (data, row, column, node) {
                           // Strip $ from salary column to make it numeric
                           return column === 5 ?
                               data.replace(/[$,]/g, '') :
                               data;
                       }
                   }
               }
           };
           */
           $.ajax({
               type: "POST",
               dataType: "json",
               url: "ReportWS.asmx/GetDataTable",
                beforeSend: function() {
                    $("#loaderDiv").show();
                    $("#mainDiv").hide();
                },
               success: function (data) {
                   $("#loaderDiv").hide();
                    $("#mainDiv").show();
                   var datatableVariable = $('#studentTable').DataTable({
                       data: data,
                       /* Results in:
                        <div class="top">
                            {Button}
                          {information}
                        </div>
                        {processing}
                        {table}
                        <div class="bottom">
                          {filter}
                          {length}
                          {pagination}
                        </div>
                        <div class="clear"></div>
                    */
                       dom: "<'row'<'col-sm-6'l><'col-sm-6'>>" +
                       "<'row'<'col-sm-12'Btr>>" +
                       "<'row'<'col-sm-5'i><'col-sm-7'p>>", //To display buttons
                       //lBfrtip
                       columnDefs: [
                           {
                               targets: [4, 8,9,10,13,15,16,17],
                               visible: false,
                               searchable: false
                           },
                           {
                               targets: [0,4,8,9,10,13],
                               className: 'noVis'
                           },
                           
                           { "width": "8%", "targets": 3 }, //Width for Campus column
                           { "width": "5%", "targets": 5 }, //Age
                           { "width": "10%", "targets": 6 }, //DOB
                           { "width": "10%", "targets": 7 } //country

                       ],

                       buttons: [
                           {
                               extend: 'colvis',
                               columns: ':not(.noVis)',
                               //className: 'btn btn-default pull-left'
                           },
                           {
                               extend: 'excel',
                               filename: "Active_Homestay_Students",
                               title: "List of Active Homestay Students",
                               exportOptions: {
                                   columns: 'th:not(:first-child)'
                               },
                               //className: 'btn btn-default pull-left'
                           },
                           /*
                            {
                                extend: 'pdfHtml5',
                                filename: "Active_Homestay_Students"
                           }*/
                       ],
                       columns: [
                           {
                               "className": 'details-control',
                               "orderable": false,
                               "data": null,
                               "defaultContent": ''
                           },
                           { 'data': 'FullName' },
                           
                           { 'data': 'Gender' },
                           { 'data': 'Campus' },
                           { 'data': 'Interest' },
                           { 'data': 'Age' },
                           { 'data': 'DOB' },
                           { 'data': 'Country' },
                           { 'data': 'Id' },
                           { 'data': 'Email' },
                           { 'data': 'CellPhone' },
                           { 'data': 'QuarterStartDate' },
                           { 'data': 'Status' },
                           { 'data': 'Notes' },
                           { 'data': 'HostFamily' },
                           { 'data': 'SuccessMeeting' },
                           { 'data': 'Agreement' },
                           { 'data': 'LocalAddress' }
                       ],
                       "order": [[1, 'asc']] //order first column when initialization + to remove sorting icon when first load
                      
                   });

                 
              
                   //$('#example thead tr').clone(true).appendTo('#example thead');
                   $('#studentTable tfoot th').each(function (index, value) {
                       if (index > 0){
                           var placeHolderTitle = $('#studentTable thead th').eq($(this).index()).text();
                           $(this).html('<input type="text" class="form-control input input-sm" placeholder = "' + placeHolderTitle + '" />');
                       }
                       
                   });
                   
                   $('#ageFilterID').on('change', function () {
                       //datatableVariable.columns(6).search(this.value).draw();
                       datatableVariable.draw();
                   });

                   
                   datatableVariable.columns([1, 2, 3, 4, 5]).every(function () {
                       var column = this;
                       var select = $('<select class="form-control"><option value="">----</option></select>')
                           .appendTo($(column.footer()).empty())
                           .on('change', function () {
                               var val = $.fn.dataTable.util.escapeRegex(
                                   $(this).val()
                               );

                               column
                                   .search(val ? '^' + val + '$' : '', true, false)
                                   .draw();
                           });

                       column.data().unique().sort().each(function (d, j) {
                           select.append('<option value="' + d + '">' + d + '</option>');
                       });
                       
                       
                   });
                   
                   
                   var i = 0;
                   datatableVariable.columns().every(function () {
                       //if (i != 3 || i != 0){
                           var column = this;
                           $(this.footer()).find('input').on('keyup change', function () {
                               column.search(this.value).draw();
                           });
                       //}
                       i++; 
                   });
                   
                   $('.showHide').on('click', function () {
                       var tableColumn = datatableVariable.column($(this).attr('data-columnindex'));
                       tableColumn.visible(!tableColumn.visible());
                   });
                   
                   
                   // Add event listener for opening and closing details
                   $('#studentTable tbody').on('click', 'td.details-control', function () {
                       
                       var tr = $(this).closest('tr');
                       var row = datatableVariable.row(tr);
                       //
                       if (row.child.isShown()) {
                           
                           // This row is already open - close it
                           row.child.hide();
                           tr.removeClass('shown');
                       }
                       else {
                           // Open this row
                           row.child(format(row.data())).show();
                           
                           tr.addClass('shown');
                       }
                   });
               },
               error: function (request, error) {
                   alert("failed");
               }
           });
           
           

       });  
       
   </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="bannerH1Text" Runat="Server">
    Homestay Administration
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">
    <h2>List of Active Homestay Students<asp:Literal ID="litType" runat="server"></asp:Literal></h2>
    <div id="divHome" runat="server" style="width:100%;float:left">
        <asp:Button ID="Button1" runat="server" Text="Return to Main Page" OnClick="linkBtnReport_Click" />&nbsp;&nbsp;
        <asp:Button ID="linkBtnHome" runat="server" Text="Return to Admin Home Page" OnClick="linkBtnHome_Click" />&nbsp;&nbsp;
    </div>

    <div id="loaderDiv">
        <img src="../../images/demo_wait.gif" />
    </div>
    
    <div id="mainDiv" style="padding: 20px; margin-top: 50px" class="container-fluid">  
        
        
        <!--
    <div>  
                <b class="label label-danger" style="padding: 8.5px">Click to Show or Hide Column:</b>  
                <div class="btn-group btn-group-sm">  
                   
                    <a class="showHide btn btn-primary" data-columnindex="1">Full Name</a>  
                    <a class="showHide btn btn-primary" data-columnindex="2">Status</a>  
                    <a class="showHide btn btn-primary" data-columnindex="3">Gender</a>  
                    <a class="showHide btn btn-primary" data-columnindex="4">Campus</a>  
                    <a class="showHide btn btn-primary" data-columnindex="5">Interest</a>  
                    <a class="showHide btn btn-primary" data-columnindex="6">Age</a>  
                    <a class="showHide btn btn-primary" data-columnindex="7">DOB</a>  
                    <a class="showHide btn btn-primary" data-columnindex="8">Country</a>  
                </div>  
            </div>  
        -->
       
           
    <div class="form-row">
        <div class="form-group col-md-2">
          <select class="form-control" id="ageFilterID">
                <option value="">Select age</option>
                <option value="18">18+</option>
                <option value="17">Under 18</option>
            </select>
        </div>
        <div class="form-group col-md-4">
            <button class="btn btn-success" id="clearFilter">Reset</button>
        </div>
        
    </div>     
        <div id="studentTable_wrapper"></div>
    <table id="studentTable" class="table table-striped table-bordered"  style="width:100%">  
                <thead>  
                    <tr>  
                        <th></th>
                        <th>Full Name</th>  
                        <th>Gender</th>  
                          <th>Campus</th>  
                        <th>Interest</th>  
                        <th>Age</th>  
                        <th>DOB</th> 
                       <th>Country</th>
                        <th>Id</th>
                        <th>Email</th>
                        <th>CellPhone</th>
                        <th>Starting Quarter</th>  
                        <th>Status</th>  
                        <th>Notes</th>  
                        <th>Host Family(for current period)</th>  
                        <th>Success Meeting</th>  
                        <th>Agreement</th>  
                        <th>LocalAddress</th>  
                    </tr>  
                </thead>  
                <tfoot>  
                    <tr>  
                        <th></th>
                       <th>Full Name</th>  
                        <th>Gender</th>  
                         <th>Campus</th>
                        <th>Interest</th>  
                        <th>Age</th> 
                        <th>DOB</th>
                        <th>Country</th>
                        <th>Id</th>
                        <th>Email</th>
                        <th>CellPhone</th>
                        <th>QuarterStartDate</th> 
                        <th>Status</th> 
                        <th>Notes</th>  
                        <th>HostFamily</th>  
                        <th>Success Meeting</th>  
                        <th>Agreement</th> 
                        <th>LocalAddress</th>  
                    </tr>  
                </tfoot>  
            </table>  

    </div>
    <!--
<div id="divFamilies" runat="server" Visible="false">    
<asp:Literal ID="litResultMsg" runat="server"></asp:Literal>
    <ul>
        <asp:Literal ID="litList" runat="server"></asp:Literal>
    </ul>
</div>
    
<div id="divStudents" runat="server" Visible="false">
    <div><h2>Filters</h2></div>
    <div style="float:left;margin-right:30px;width:15%;"><h4 style="margin-top:-20px;">Age</h4></div><div style="float:left;margin-right:30px;width:15%;"><h4 style="margin-top:-20px;">Status</h4></div><div style="float:left;margin-right:30px;width:25%;"><h4 style="margin-top:-20px;">Country</h4></div><div style="float:left;margin-right:30px;width:15%;"><h4 style="margin-top:-20px;">Campus</h4></div><div style="float:left;margin-right:30px;width:15%;"><h4 style="margin-top:-20px;">Family</h4></div>
   <div style="float:left;width:15%;margin-bottom:10px;margin-top:-10px;margin-right:30px;"><asp:DropDownList ID="ddlAge" runat="server">
       <asp:ListItem Value="18" Text="18+"></asp:ListItem>
       <asp:ListItem Value="UA" Text="Under 18"></asp:ListItem>
     </asp:DropDownList></div><div style="float:left;width:15%;margin-bottom:10px;margin-top:-10px;margin-right:30px;"><asp:DropDownList ID="ddlStatus" runat="server"></asp:DropDownList></div>
     <div style="float:left;width:15%;margin-bottom:10px;margin-top:-10px;margin-right:30px;">
         <asp:DropDownList ID="ddlCountries" runat="server"></asp:DropDownList></div>
    <div style="float:left;width:95%;margin-bottom:20px;"><asp:Button ID="btnCmdFilter" runat="server" Text="Filter" /></div>
    <table style="width:100%;">
        <tr>
            <th style="text-align:left;">NAME</th><th style="text-align:left;">Status</th><th style="text-align:left;">Gender</th><th style="text-align:left;">ID</th><th style="text-align:left;">Campus</th><th style="text-align:left;">age</th><th style="text-align:left;">DOB</th><th style="text-align:left;">Country</th><th style="text-align:left;">Email</th><th style="text-align:left;">Phone</th>
        </tr>
        <asp:Literal ID="litTable" runat="server"></asp:Literal>
    </table>
</div>
    -->
</asp:Content>
