﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MatchPreferences3.aspx.cs" Inherits="Homestay_Admin_MatchPreferences3" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript" src="/_js/jquery-1.9.1.min.js"></script>      
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <h3>Select a Student</h3>
            <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
            <br />
            <asp:DropDownList ID="ddlStudents" runat="server" AppendDataBoundItems="true" 
                OnSelectedIndexChanged="ddlStudents_SelectedIndexChanged"
                AutoPostBack="true">

            </asp:DropDownList>
             <h3>Current Status</h3>
            <asp:Literal ID="litCurrStudentStatus" runat="server" Text=""></asp:Literal>
            <br />
            College: <asp:Label ID="lblCollege" runat="server" Text=""></asp:Label>
            <p> 
                <label for="rdoGender">Gender</label>
                <asp:RadioButtonList ID="rdoGender" RepeatDirection="Horizontal" runat="server">
                    <asp:ListItem Value="Include" Selected="True"></asp:ListItem>
                    <asp:ListItem Value="Omit"></asp:ListItem>
                </asp:RadioButtonList>
            </p>
            <p> 
                <label for="rdoHomestay">Homestay Preference</label>
                <asp:RadioButtonList ID="rdoHomestay" RepeatDirection="Horizontal" runat="server">
                    <asp:ListItem Value="Include" Selected="True"></asp:ListItem>
                    <asp:ListItem Value="Omit"></asp:ListItem>
                </asp:RadioButtonList>
            </p>
             <p> 
                <label for="rdoSmoking">Smoking Habits</label>
                <asp:RadioButtonList ID="rdoSmoking" RepeatDirection="Horizontal" runat="server">
                    <asp:ListItem Value="Include" Selected="True"></asp:ListItem>
                    <asp:ListItem Value="Omit"></asp:ListItem>
                </asp:RadioButtonList>
            </p>
        </ContentTemplate>

        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSearch" />
        </Triggers>

    </asp:UpdatePanel>
    <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" OnClientClick="return ValidateSelectStudent();"/>
    <br />

    <h3>Level of Matching</h3>
    
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <h3>Families that Match</h3>
            <asp:Literal ID="litFoundFamily" runat=server></asp:Literal>   
            <br />
              <asp:Label ID="litRoomWithOtherFamily" runat="server" Text=""></asp:Label>
            <asp:DropDownList ID="ddlFamilies" runat="server" >
               
            </asp:DropDownList>
             
            <asp:Button ID="btnCompare" runat="server" Text="Compare"  OnClientClick="return ValidateSelectFamily();" OnClick="btnCompare_Click"/>
            <br />
            <asp:Literal ID="litResultMsg" runat=server></asp:Literal>    
            
           
    
        </ContentTemplate>

    </asp:UpdatePanel>
        </div>
    </form>
     <script type="text/javascript">
        function ValidateSelectStudent() {
            var ddl = $('#<%=ddlStudents.ClientID%>');
            var selectedValue = ddl.val();
  
            //alert(selectedValue); //SelVal is the selected Value
            if (selectedValue == 0) {
                 alert("Please select a student to search");
                return false;
            }
            return true;
        }

         function ValidateSelectFamily() {
            var ddl = $('#<%=ddlFamilies.ClientID%>');
            var selectedValue = ddl.val();
  
            //alert(selectedValue); //SelVal is the selected Value
            if (selectedValue == 0) {
                 alert("Please select a family to compare");
                return false;
            }
            return true;
        }
        $(function () {
            
        });
    </script>
</body>
</html>
