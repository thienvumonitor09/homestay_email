﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

/// <summary>
/// Summary description for EntityObject
/// </summary>
public class EntityObject
{
    
    public string Name { get; set; }

    public Dictionary<string, string[]> Attributes { get; set; }
    public EntityObject()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public override string ToString()
    {
        StringBuilder sb = new StringBuilder();
        foreach (var pair in Attributes)
        {
            sb.Append(pair.Key + ":");
            sb.Append(string.Join(",", pair.Value) + "\n");
        }
        return sb.ToString();
    }
}