﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Security.Principal;


/// <summary>
/// Summary description for Global
/// </summary>
public class Global : System.Web.HttpApplication
{
    void Application_Error(object sender, EventArgs e)
    {
        bool blSuppressEmail = false;
        // Code that runs when an unhandled error occurs
        HttpContext ctx = HttpContext.Current;

        Exception exception = ctx.Server.GetLastError();
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        // Get the path of the page
        sb.Append("More information may be available in the system log on the target server.<br /><br /><b>Error in Path:</b> " + Request.Path);
        // Get the QueryString along with the Virtual Path
        sb.Append("<br /><br /><b>Error Raw Url: </b>" + Request.RawUrl);
        // Create an Exception object from the Last error that occurred on the server
        Exception myError = Server.GetLastError();
        // Get the error message
        sb.Append("<br /><br /><b>Error Message:</b> " + myError.Message);
        if (myError.Message.Contains("does not exist")) { blSuppressEmail = true; }
        // Source of the message
        sb.Append("<br /><br /><b>Error Source:</b> " + myError.Source);
        // Stack Trace of the error
        sb.Append("<br /><br /><b>Error Stack Trace:</b> " + myError.StackTrace);
        // Method where the error occurred
        sb.Append("<br /><br /><b>Error TargetSite:</b> " + Request.ServerVariables["SERVER_NAME"]);
        sb.Append("<br /><br /><b>IP:</b> " + Request.ServerVariables["REMOTE_ADDR"] + "<br /><br />" + DateTime.Now.ToString() +
            "<br /><br />" + myError.InnerException + "<br /><br />" + myError.Data + "<br /><br />" +
            "<b>Client Information:</b> " + HttpContext.Current.Request.Browser.Browser + "<br /><br />");
        String temp = "<b>Header Key Value Pairs</b><br />";
        foreach (string st in HttpContext.Current.Request.Headers.AllKeys)
        {
            foreach (string st2 in HttpContext.Current.Request.Headers.GetValues(st))
            {
                temp += "  " + st + " = " + st2 + "<br />";
            }
        }
        sb.Append(temp + "<br /><br />");
        temp = "<b>Form Key Value Pairs:</b><br />";
        foreach (string st in HttpContext.Current.Request.Form.AllKeys)
        {
            temp += "  " + st + " = " + HttpContext.Current.Request.Form[st] + "<br />";
        }
        sb.Append(temp + "<br /><br />");


        Utility sendMail = new Utility();
        if (!blSuppressEmail)
        {
            sendMail.SendEmailMsg("Bob.Nelson@ccs.spokane.edu", "CCSWebApp@ccs.spokane.edu", "Apps.spokane.edu Error", true, sb.ToString(), "");
            sendMail.SendEmailMsg("Laura.Padden@ccs.spokane.edu", "CCSWebApp@ccs.spokane.edu", "Apps.spokane.edu Error", true, sb.ToString(), "");
            sendMail.SendEmailMsg("Brandy.Vaughn@ccs.spokane.edu", "CCSWebApp@ccs.spokane.edu", "Apps.spokane.edu Error", true, sb.ToString(), "");
        }
        // Attempt to redirect the user
        //Server.Transfer("/_error/Error500.htm"); //comment out to debug
        // }
        // catch (Exception err)
        //  {
        // Error occured while redirecting user to an error page. We could do nothing
        // now, but setting a status code and completing the request
        //     Response.StatusCode = 404;
        //     String errtext = err.Data.ToString();
        //     this.CompleteRequest();
        // }
    }
    
    void Application_AuthenticateRequest(Object sender, EventArgs e)
    {
        String cookieName = FormsAuthentication.FormsCookieName;
        HttpCookie authCookie = Context.Request.Cookies[cookieName];

        if (null == authCookie)
        {//There is no authentication cookie.
            return;
        }

        FormsAuthenticationTicket authTicket = null;

        try
        {
            authTicket = FormsAuthentication.Decrypt(authCookie.Value);
        }
        catch 
        {
            //Write the exception to the Event Log.
            return;
        }

        if (null == authTicket)
        {//Cookie failed to decrypt.
            return;
        }

        //When the ticket was created, the UserData property was assigned a
        //pipe-delimited string of group names.
        String[] groups = authTicket.UserData.Split(new char[] { '|' });

        //Create an Identity.
        GenericIdentity id = new GenericIdentity(authTicket.Name, "LdapAuthentication");

        //This principal flows throughout the request.
        GenericPrincipal principal = new GenericPrincipal(id, groups);

        Context.User = principal;

    }
}
