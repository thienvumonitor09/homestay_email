﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Homestay_Admin_MatchPreferences7 : System.Web.UI.Page
{
    Homestay hsInfo = new Homestay();
    Quarter_MetaData QMD = new Quarter_MetaData();
    private static Dictionary<string, EntityObject> stuHM;
    FamilyManager familyManager = new FamilyManager();
    StudentManager studentManager = new StudentManager();
    private static Dictionary<string, EntityObject> famHM;
    private static DataTable dtDisplay;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            /** Populate DL for students Using DataBind **/
            DataTable dtStudents = hsInfo.GetHomestayStudents("Active", 0);
            //DataTable dtStudents = hsInfo.GetHomestayStudents("Active", 0);
            dtStudents.Columns.Add("valueField", typeof(string));
            if (dtStudents != null)
            {
                if (dtStudents.Rows.Count > 0)
                {
                    foreach (DataRow dr in dtStudents.Rows)
                    {
                        string applicantIDStr = dr["applicantID"].ToString();
                        dr["valueField"] = dr["familyName"].ToString() + ", "
                                            + dr["firstName"].ToString() + " - " + Convert.ToDateTime(dr["DOB"]).ToShortDateString();
                    }
                }
            }
            ddlStudents.DataSource = dtStudents;
            ddlStudents.DataTextField = "valueField";
            ddlStudents.DataValueField = "applicantID";
            ddlStudents.DataBind();
            ddlStudents.Items.Insert(0, new ListItem("--Select a student--", "")); //Add default value
            /** End populating dropdow **/
           
        }
        //Response.Write(stuHM["2401"].Name.ToString());
        //litResultMsg.Text = stuHM["2401"].Name.ToString();
        //litResultMsg.Text = stuHM.Count() +"";
        /*
        string connectionStr = ConfigurationManager.ConnectionStrings["ConnectionString_CCSInternationalStudent"].ToString();
        SqlConnection conn = new SqlConnection(connectionStr);
        string sqlQuery = @"SELECT  [id]
                              ,[applicantID]
                          FROM[CCSInternationalStudent].[dbo].[HomestayStudentInfo]";


        DataTable students = new DataTable();
        SqlCommand cmd = new SqlCommand(sqlQuery, conn);

        try
        {
            conn.Open();

            SqlDataAdapter objDataAdapter = new SqlDataAdapter(cmd);

            objDataAdapter.Fill(students);
            ddlStudents.DataSource = students;
            ddlStudents.DataTextField = "applicantID";
            ddlStudents.DataValueField = "id";
            ddlStudents.DataBind();

        }
        catch (Exception ex)
        {
            Label1.Text += "Error RunGetQuery: " + ex.Message + "<br />SQL: " + sqlQuery + "<br />";
        }
        */

    }



    

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        insertDiv.Visible = false;
        gvStuFam.Visible = false;
        //Reset ddlFamilies
        ddlFamilies.SelectedIndex = -1;
        //Add effective quarter
        DataTable dtQuarters = QMD.GetQuarterStart(-3, 15);
        
        //DataTable dtQuarters = QMD.GetQuarterStart(-1, 1);
        ddlEffectiveQtr.DataSource = dtQuarters;
        ddlEffectiveQtr.DataTextField = "textField";
        ddlEffectiveQtr.DataValueField = "valueField";
        ddlEffectiveQtr.DataBind();
        ddlEffectiveQtr.Items.Insert(0, new ListItem("--Select a quarter--", "")); //Add default value

        ddlEndQtr.DataSource = dtQuarters;
        ddlEndQtr.DataTextField = "textField";
        ddlEndQtr.DataValueField = "valueField";
        ddlEndQtr.DataBind();
        ddlEndQtr.Items.Insert(0, new ListItem("--Select a quarter--", "")); //Add default value

        //Reset Placement, Room, EffectiveQtr
        ddlRmPlacement.SelectedIndex = -1;
        ddlRoom.SelectedIndex = -1;
        ddlEffectiveQtr.SelectedIndex = -1;
        //ddlRmPlacement.Enabled = false;
        //ddlRoom.Enabled = false;
        //ddlEffectiveQtr.Enabled = false;

        //Reset Family Info & Room status
        lblFamilyStatus.Text = "";
        litRm1Available.Text = "";
        litRm2Available.Text = "";
        litRm3Available.Text = "";
        litRm4Available.Text = "";
        //stuHM = MatchingUtility.LoadStudentHM();
        stuHM = studentManager.GetStuHM();
        //famHM = MatchingUtility.LoadFamilyHM();
        famHM = familyManager.GetFamilyHM();

       
        //litResultMsg.Text = "No of Families have open room: " + famHM.Count() + "<br/>";
        //litRoomWithOtherFamily.Text = "Number of Families have open room: " +  famHM.Count() +"<br/>";
        //litResultMsg.Text += "Gender: " + rdoGender.SelectedValue + "<br/>";
        //litResultMsg.Text = "HS Preference: " + rdoGender.SelectedValue + "<br/>";
        //litResultMsg.Text += "Smoking: " + rdoSmoking.SelectedValue + "<br/>";

        int selectedStudentID = Convert.ToInt32(ddlStudents.SelectedValue);
        string rdoGenderStr = rdoGender.SelectedValue.ToString();
        string rdoHomestayStr = rdoHomestay.SelectedValue.ToString();
        string rdoSmokingStr = rdoSmoking.SelectedValue.ToString();

        //itSelectedStuInfo.Text = "<strong>Selected Student's Info:</strong>" + "<br/>";
        //litResultMsg.Text += "ID: " + stuHM[selectedStudentID + ""].Name.ToString() + "<br/>";
        //litResultMsg.Text += "Smoker: " + stuHM[selectedStudentID + ""].Attributes["Smoker"][0];
        EntityObject stuObject = stuHM[selectedStudentID + ""];
        Dictionary<string, string[]> stuAttributes = stuObject.Attributes;

        //Display student's current status
        //DisplayCurrStudentStatus(ddlStudents.SelectedValue);
        DisplayGvCurrStudentStatus(ddlStudents.SelectedValue);
        
        //litSelectedStuInfo.Text += " entityObj:" + stuObject.ToString() + "</br>";
        string stuCol = stuObject.Attributes["studyWhere"][0].ToLower();
        //litResultMsg.Text += " College:" + stuCol + "<br/>";

       
        //Popudate Datatable dtDisplay to display "Comparision"
        dtDisplay = new DataTable();
        dtDisplay.Columns.AddRange(new DataColumn[4]
                    {
                        
                        new DataColumn("Field", typeof(string)),
                        new DataColumn("StudentValue", typeof(string)),
                        new DataColumn("FamilyValue", typeof(string)),
                        new DataColumn("DisplayText", typeof(string))
                    });
        dtDisplay.Rows.Add("applicantID", stuAttributes["applicantID"][0], "", "ID");
        dtDisplay.Rows.Add("Name", stuAttributes["familyName"][0]+ " ," + stuAttributes["firstName"][0], "", "Name");
        dtDisplay.Rows.Add("college", stuAttributes["college"][0], "", "College");
        dtDisplay.Rows.Add("smoke", stuAttributes["smoke"][0], "", "Do you smoke?");
        dtDisplay.Rows.Add("otherSmoke", stuAttributes["otherSmoke"][0], "", "Other smokers OK?");
        dtDisplay.Rows.Add("smokingHabits", stuAttributes["smokingHabits"][0], "", "Explain:");
        dtDisplay.Rows.Add("gender", stuAttributes["gender"][0], "", "Gender Preference");
        dtDisplay.Rows.Add("homestayOption", stuAttributes["homestayOption"][0], "", "Homestay Option");
        dtDisplay.Rows.Add("dietaryNeeds", string.Join(", ", stuAttributes["dietaryNeeds"]), "", "Dietary needs");
        dtDisplay.Rows.Add("hobbies", string.Join(", ", stuAttributes["hobbies"]), "", "Hobbies");
        dtDisplay.Rows.Add("activitiesEnjoyed", string.Join(", ", stuAttributes["activitiesEnjoyed"]), "", "Other hobbies/activities enjoyed");
        dtDisplay.Rows.Add("interaction", stuAttributes["interaction"][0], "", "Interaction and Conversation");
        dtDisplay.Rows.Add("homeEnvironment", stuAttributes["homeEnvironment"][0], "", "Home Environment");
        dtDisplay.Rows.Add("homeEnvironmentPreferences", stuAttributes["homeEnvironmentPreferences"][0], "", "Explain");
        dtDisplay.Rows.Add("childrenPref", stuAttributes["childrenPref"][0], "", "Children");
        dtDisplay.Rows.Add("traveledOutsideCountry", stuAttributes["traveledOutsideCountry"][0], "", "Traveled outside U.S. before?");
        string othersStr = String.Format("Allergies:{0}, Health conditions: {1}, Anything else:\n"
                                            , stuAttributes["allergies"][0], stuAttributes["healthConditions"][0], stuAttributes["anythingElse"][0]); 
        dtDisplay.Rows.Add("otherNotes", othersStr, "", "Others");
        //dtDisplay.Rows.Add("familyID", stuAttributes["familyID"][0], "");
        GridView1.DataSource = dtDisplay;
        GridView1.DataBind();




        //litResultMsg.Text = "--------- <br/>";
        //litResultMsg.Text += " " + famHM.Count();
        //litRoomWithOtherFamily.Text = selectedStudentID + " " + rdoGenderStr + " " + rdoHomestayStr+ " " + rdoSmokingStr;
        /** Populate DL for students Using DataBind **/
        //string strSQL = "SELECT id, homeName FROM [CCSInternationalStudent].[dbo].[HomestayFamilyInfo]";
        //DataTable dtFamilies = hsInfo.RunGetQuery(strSQL);

        //List of matching level
        var matchingList = new List<Dictionary<string, EntityObject>>();

        /** Filter by college **/
        Dictionary<string, EntityObject> famHMFilteredCollege = MatchingUtility.MatchStuFamCollege(stuCol, famHM);
        //litResultMsg.Text += "There are " + famHMFilteredCollege.Count() + " families match with student's college: <br/>";
        //litResultMsg.Text += MatchingUtility.PrintHM(famHMFilteredCollege);
        matchingList.Add(famHMFilteredCollege);

        famHM = famHMFilteredCollege;

        /** Filter by Smoking preferences **/
        if (rdoSmoking.SelectedValue.Equals("Include")  || rdoSmoking.SelectedValue.Equals(""))
        {
            Dictionary<string, EntityObject> famHMFilteredSmoking = MatchingUtility.MatchStuFamSmoking(stuObject, famHM);
            //litResultMsg.Text += "<br/>There are " + famHMFilteredSmoking.Count() + " families match with student's smoking:<br/>";
            //litResultMsg.Text += MatchingUtility.PrintHM(famHMFilteredSmoking);
            matchingList.Add(famHMFilteredSmoking);
        }

        /** Filter by Gender **/
        if (rdoGender.SelectedValue.Equals("Include") || rdoGender.SelectedValue.Equals(""))
        {
            Dictionary<string, EntityObject> famHMFilteredGender = MatchingUtility.MatchStuFamGender(stuObject, famHM);
            //litResultMsg.Text += "<br/>There are " + famHMFilteredGender.Count() + " families match with student's gender:<br/>";
            //litResultMsg.Text += MatchingUtility.PrintHM(famHMFilteredGender);
            matchingList.Add(famHMFilteredGender);
        }


        /** Filter by homestayOption **/
        if (rdoHomestay.SelectedValue.Equals("Include") || rdoHomestay.SelectedValue.Equals(""))
        {
            Dictionary<string, EntityObject> famHMFilteredHomestayOption = MatchingUtility.MatchStuFamHomestayOption(stuObject, famHM, litResultMsg.Text);
            //litResultMsg.Text += "<br/>There are " + famHMFilteredHomestayOption.Count() + " families match with student's hsOption:<br/>";
            //litResultMsg.Text += MatchingUtility.PrintHM(famHMFilteredHomestayOption);
            matchingList.Add(famHMFilteredHomestayOption);
        }


        /** Combine all dictionaries **/
        Dictionary<string, EntityObject> famHMFilteredFinal = MatchingUtility.FindIntersection(matchingList);
        //litResultMsg.Text += "<br/>There are " + famHMFilteredFinal.Count() + " families match with student:<br/>";
        //litResultMsg.Text += matchingList.Count() + "<br/>";

        //Proces final list to add score
        famHMFilteredFinal = MatchingUtility.AddScore(stuObject, famHMFilteredFinal);
        famHM = famHMFilteredFinal;

        //litResultMsg.Text += MatchingUtility.PrintHM2(stuObject, famHMFilteredFinal);


        /** Populate DL for students Using DataBind **/
        DataTable dtFamilies = new DataTable();
        dtFamilies.Columns.Add("id", typeof(string));
        dtFamilies.Columns.Add("homeName", typeof(string));
        dtFamilies.Columns.Add("score", typeof(string));
        dtFamilies.Columns.Add("textField", typeof(string));
        foreach (var f in famHMFilteredFinal)
        {
            
            Dictionary<string, string[]> attributes = f.Value.Attributes;
            string formatText = String.Format("{0}, score: {1}%", attributes["homeName"][0].ToString(), attributes["score"][0]);
            dtFamilies.Rows.Add(attributes["id"][0].ToString(), attributes["homeName"][0].ToString(), attributes["score"][0], formatText);
        }

        dtFamilies.DefaultView.Sort = "score desc";
        dtFamilies = dtFamilies.DefaultView.ToTable();

        litFoundFamily.Text = "<strong>There are " + famHMFilteredFinal.Count() + " families that match above criteria found:</strong>" 
                              + "(The result is ranked in order of overall match score)";
        //litResultMsg.Text += dtFamilies.Rows.Count + " families: <br/>";

        //Match with smoking preferences

        ddlFamilies.DataSource = dtFamilies;
        ddlFamilies.DataTextField = "textField";
        ddlFamilies.DataValueField = "id";
        ddlFamilies.DataBind();
        ddlFamilies.Items.Insert(0, new ListItem("--Select a family--", "")); //Add default value
                                                                              /** End populating dropdow **/

        //populate room
        DataTable dtRooms = new DataTable();
        dtRooms.Columns.Add("valueField", typeof(string));
        dtRooms.Columns.Add("textField", typeof(string));

        for (var i = 1; i <= 4; i++)
        {
            dtRooms.Rows.Add(i + "", "Room " + i);
        }

        ddlRoom.DataSource = dtRooms;
        ddlRoom.DataTextField = "textField";
        ddlRoom.DataValueField = "valueField";
        ddlRoom.DataBind();
        ddlRoom.Items.Insert(0, new ListItem("--Select a room--", "")); //Add default value
    }

    private void DisplayGvCurrStudentStatus(string selectedValue)
    {
        string sqlQueryCurrentStatus = @"SELECT *
                                      FROM [CCSInternationalStudent].[dbo].[HomestayPlacement]
                                      INNER JOIN [CCSInternationalStudent].[dbo].[HomestayFamilyInfo]
                                      ON HomestayPlacement.familyID = HomestayFamilyInfo.id
                                      WHERE applicantID =" + ddlStudents.SelectedValue;

        DataTable dtCurrentStatus = hsInfo.RunGetQuery(sqlQueryCurrentStatus);
        if (dtCurrentStatus == null || dtCurrentStatus.Rows.Count == 0)
        {
            litCurrStudentStatus.Text = "The student has not been placed!";
        }
        else
        {
            litCurrStudentStatus.Text = "";

        }

        GvStudentStatus.DataSource = dtCurrentStatus;
        GvStudentStatus.DataBind();
    }

    protected void btnCompare_Click(object sender, EventArgs e)
    {
        insertDiv.Visible = true;
        gvStuFam.Visible = true;
        DisplayPanel2();
    }

    protected void btnReset_Click(object sender, EventArgs e)
    {
        Response.Redirect("MatchPreferences7.aspx");
    }


   protected void DisplayPanel2()
    {
        

        StringBuilder sb = new StringBuilder();
        //sb.Append("<strong>Family Info</strong>:<br/>");

        string selectedStudentIDStr = ddlStudents.SelectedValue;
        EntityObject stuObject = stuHM[selectedStudentIDStr];
        Dictionary<string, string[]> stuAttributes = stuObject.Attributes;

       

        
        
        string familyIDStr = ddlFamilies.SelectedValue;
        if (familyIDStr.Equals(""))
        {
            return;
        }
        EntityObject famObject = famHM[familyIDStr];
        var famAttributes = famObject.Attributes;

        //Hide/Unhide insertDiv
        string sqlQuery2 = @"SELECT *
                              FROM [CCSInternationalStudent].[dbo].[HomestayPlacement]
                              WHERE applicantID =" + selectedStudentIDStr + " AND familyID= " + familyIDStr;
        DataTable dtsqlQuery1 = hsInfo.RunGetQuery(sqlQuery2);
        if (dtsqlQuery1 == null || dtsqlQuery1.Rows.Count == 0)
        {
            //insertDiv.Visible = true;
        }
        else
        {
            //insertDiv.Visible = false;
        }



        //Display student's current status
        //DisplayCurrStudentStatus(selectedStudentIDStr);
        DisplayGvCurrStudentStatus(selectedStudentIDStr);
        //sb.Append(MatchingUtility.PrintEnityObject(famObject) + "<br/><br/>");
        //PrintEnityObject(famObject);
        litFamilyInfo.Text = sb.ToString();

        string[] rmStatusArr = new string[4];

        //string rm1Status = famAttributes["room1Occupancy"][0];
        //string rm2Status = famAttributes["room2Occupancy"][0];
        //string rm3Status = famAttributes["room3Occupancy"][0];
        //string rm4Status = famAttributes["room4Occupancy"][0];
        for (var i = 1; i <=4; i++)
        {
           
            if (famAttributes["room"+i].Length > 0)
            {
                /*
                rmStatusArr[i - 1] += String.Format(", {0}, {1} - {2}, {3}, effectiveQuarter: {4}",
                                                    stuAttributes["familyName"][0], stuAttributes["firstName"][0],
                                                    famAttributes["room" + i][0], //applicantID 
                                                    famAttributes["room" + i][1], //placementType
                                                    famAttributes["room" + i][2] // effectiveQuarter
                                                    );
                                                    */
            }

        }
        for (var i = 1; i <= 4; i++)
        {
            string sqlQuery = @"SELECT *
                              FROM [CCSInternationalStudent].[dbo].[HomestayPlacement]
                              WHERE roomNumber =" + i + " AND familyID= " + familyIDStr;
            DataTable dtPlacements = hsInfo.RunGetQuery(sqlQuery);
            foreach (DataRow rowPlacement in dtPlacements.Rows)
            {
                string studentIDStr = rowPlacement["applicantID"].ToString();
                string studentName = stuHM[studentIDStr].Attributes["familyName"][0] +" ,"
                                    + stuHM[studentIDStr].Attributes["firstName"][0];
                rmStatusArr[i-1] += studentName 
                                + " ,effectiveQuarter: " + rowPlacement["effectiveQuarter"] + " endQuarter: " + rowPlacement["endQuarter"] 
                                + "<br/>";
                
            }
            /*
            Button btn = new Button();
            btn.ID = "Button" + i.ToString();
            btn.Text = "Test button" + i.ToString();
            btn.Click += new EventHandler(btn_Click);
            rm1Occupancy.Controls.Add(btn);
            */
        }

        
        litRm1Available.Text = famAttributes["room1Occupancy"][0];
        litRm2Available.Text = famAttributes["room2Occupancy"][0];
        litRm3Available.Text = famAttributes["room3Occupancy"][0];
        litRm4Available.Text = famAttributes["room4Occupancy"][0];

        litRm1Occupancy.Text = rmStatusArr[0];
        litRm2Occupancy.Text = rmStatusArr[1];
        litRm3Occupancy.Text = rmStatusArr[2];
        litRm4Occupancy.Text = rmStatusArr[3];

        //display all rooms
        DataTable dtAllRoom = new DataTable();
        dtAllRoom.Columns.AddRange(new DataColumn[3]
                    {

                        new DataColumn("room", typeof(string)),
                        new DataColumn("availableStarting", typeof(string)),
                        new DataColumn("occupancy", typeof(string))
                    });
        for (var i = 1; i <= 4; i++)
        {

            dtAllRoom.Rows.Add("Room " + i, famAttributes["room"+i+"Occupancy"][0], rmStatusArr[i-1]);
        }

        GvAllRoom.DataSource = dtAllRoom;
        GvAllRoom.DataBind();


        string sqlQuery1 = @"SELECT *
                              FROM [CCSInternationalStudent].[dbo].[HomestayPlacement]
                              WHERE applicantID = " + selectedStudentIDStr
                                    + " AND familyID = " + familyIDStr;
       

        DataTable dtPlacements1 = hsInfo.RunGetQuery(sqlQuery1);
        
        GridView3.DataSource = dtPlacements1;
        GridView3.DataBind();

        gvStuFam.DataSource = dtPlacements1;
        gvStuFam.DataBind();

        string stuFamilyID = stuAttributes["familyID"][0];
        /*
        //populate room
        DataTable dtRooms = new DataTable();
        dtRooms.Columns.Add("valueField", typeof(string));
        dtRooms.Columns.Add("textField", typeof(string));

        for (var i = 1; i <= 4; i++)
        {
            //Only add room available
            string roomOccupancyStr = famAttributes["room" + i + "Occupancy"][0];
            if (roomOccupancyStr.Equals("No Info") || roomOccupancyStr.Contains("CCS")
                || !GetIsAvailableNow(roomOccupancyStr))
            {

            }
            else
            {
                if (famAttributes["room"+i].Length == 0) //Only add room available and not occupied
                {
                    
                }
                
            }
            dtRooms.Rows.Add(i + "", "Room " + i);

        }

        ddlRoom.DataSource = dtRooms;
        ddlRoom.DataTextField = "textField";
        ddlRoom.DataValueField = "valueField";
        ddlRoom.DataBind();
        ddlRoom.Items.Insert(0, new ListItem("--Select a room--", "")); //Add default value
        */


        //disable room when stuAttributes["familyID"] = "NA" || not "NA" but familyID not match selected family
        if (stuFamilyID.Equals("NA")) // student has not been placed with any family
          //  || (!stuFamilyID.Equals("NA") && !stuFamilyID.Equals(familyIDStr)))
        {
            ddlRmPlacement.Enabled = true;
            ddlRoom.Enabled = true;
            ddlEffectiveQtr.Enabled = true;

            ddlRmPlacement.SelectedIndex = -1;
            ddlRoom.SelectedIndex = -1;
            ddlEffectiveQtr.SelectedIndex = -1;


            for (var i = 1; i <= 4; i++)
            {
                string roomOccupancy = famAttributes["room" + i + "Occupancy"][0];
                if (roomOccupancy.Contains("No Info") || roomOccupancy.Contains("Non-CCS"))
                {
                    ddlRoom.Items.Remove(ddlRoom.Items.FindByValue(i + ""));
                }
                
                /*
                if (roomOccupancy.Contains("CCS") || roomOccupancy.Equals("No Info") || !GetIsAvailableNow(roomOccupancy))
                {
                   
                }
                */
            }

        }
        else // student has  been placed with a family
        {
            //ddlRoom.Enabled = false;//because you must withdraw before change room
            //If stuFamilyID = familyIDStr, display and populate preselected value
            if (stuFamilyID.Equals(familyIDStr))
            {
                //litRm1Status.Text = "second";
               
                string roomNumberStr = stuAttributes["roomNumber"][0];
                ddlRoom.SelectedValue = roomNumberStr;
                //ddlRmPlacement.SelectedValue = famAttributes["room" + roomNumberStr][1];
                //ddlEffectiveQtr.SelectedValue = famAttributes["room" + roomNumberStr][2];

               
                ddlRmPlacement.Enabled = true;
                ddlEffectiveQtr.Enabled = true;
            }
            else
            {
                ddlRmPlacement.SelectedIndex = -1;
                ddlRoom.SelectedIndex = -1;
                ddlEffectiveQtr.SelectedIndex = -1;
                //litRm1Status.Text = "last";
                //ddlRmPlacement.Enabled = false;
                //ddlRoom.Enabled = false;
                //ddlEffectiveQtr.Enabled = false;
            }
            

        }
        //litRm1Status.Text = stuFamilyID + " " + familyIDStr + stuFamilyID.Equals(familyIDStr);
        //dtDisplay.Rows[0]["FamilyValue"] = "123";
        
        foreach (DataRow dr in dtDisplay.Rows)
        {
            string attr = dr["Field"].ToString();
            try
            {
                dr["FamilyValue"] = String.Join(", ", famAttributes[attr]);
            }
            catch (Exception ex)
            {
                dr["FamilyValue"] = "Key not found";
                switch (attr)
                {
                    case "applicantID":
                        dr["FamilyValue"] = String.Join(", ", famAttributes["id"]);
                        break;
                    case "childrenPref":
                        dr["FamilyValue"] = String.Join(", ", famAttributes["children"]);
                        break;
                    case "smokingHabits":
                        dr["FamilyValue"] = String.Join(", ", famAttributes["smokingGuidelines"]);
                        break;
                    case "homeEnvironmentPreferences":
                        dr["FamilyValue"] = String.Join(", ", famAttributes["homeEnvironment"]);
                        break;
                    case "activitiesEnjoyed":
                        dr["FamilyValue"] = String.Join(", ", famAttributes["otherHobbies"]);
                        break;
                    case "traveledOutsideCountry":
                        dr["FamilyValue"] = String.Join(", ", famAttributes["travelOutsideUS"]);
                        break;
                    case "otherNotes":
                        string familyOtherNotes = String.Format("Pets in home: {0}, Additional Expectations: {1}"
                                                , famAttributes["petsInHome"][0], famAttributes["additionalExpectations"][0]) ;
                        dr["FamilyValue"] = familyOtherNotes;
                        break;
                    case "Name":
                        dr["FamilyValue"] = String.Join(", ", famAttributes["homeName"]);
                        break;
                }
            }
            
            //string columnName = column.ColumnName;
            //string columnData = row[column].ToString();
            //attributes.Add(columnName, new string[] { columnData });
        }
        GridView1.DataSource = dtDisplay;
        GridView1.DataBind();


        DataTable dtMatching = new DataTable();
        dtMatching.Columns.AddRange(new DataColumn[2]
                    {

                        new DataColumn("Field", typeof(string)),
                        new DataColumn("ScoreMatch", typeof(string))
                    });
        dtMatching.Rows.Add("% children match", famAttributes["childrenScore"][0]);
        dtMatching.Rows.Add("% hobbies match", famAttributes["hobbiesScore"][0]);
        dtMatching.Rows.Add("% dietaryNeeds match", famAttributes["dietaryScore"][0]);
        dtMatching.Rows.Add("% interaction match", famAttributes["interactionScore"][0]);
        dtMatching.Rows.Add("% homeEnvironment match", famAttributes["homeEnvironmentScore"][0]);
        dtMatching.Rows.Add("% overall match", famAttributes["score"][0]);

        GridView2.DataSource = dtMatching;
        GridView2.DataBind();

        lblFamilyStatus.Text = famAttributes["familyStatus"][0];

        ddlRmPlacement.SelectedIndex = -1;
        ddlRoom.SelectedIndex = -1;
        ddlEffectiveQtr.SelectedIndex = -1;
        ddlEndQtr.SelectedIndex = -1;

       

    }

    void btn_Click(object sender, EventArgs e)
    {
        //lit.Text = string.Format("Button {0} was pressed.", ((Button)sender).ID);
    }

    private bool GetIsAvailableNow(string roomStatus)
    {
        DataTable dtQuarters = QMD.GetQuarterStart(-3, 15);
        foreach (DataRow r in dtQuarters.Rows)
        {
            if (r["valueField"].ToString().Equals(roomStatus))
            {
                string availableDate = r["textField"].ToString();
                availableDate = availableDate.Substring(availableDate.IndexOf(" ")).Trim();
                DateTime effectiveDate = Convert.ToDateTime(availableDate);
                bool availableNow = DateTime.Compare(effectiveDate, DateTime.Now) <= 0;
                //litRm1Status.Text = availableDate + availableNow;
                return availableNow;
            }
        }
        return false;
    }

    private void DisplayCurrStudentStatus(string selectedStudentIDStr)
    {
        string currStudentStatus = "";
       /*
        EntityObject stuObject = stuHM[selectedStudentIDStr];
        var stuAttributes = stuObject.Attributes;
        string currStudentStatus = stuAttributes["homestayStatus"][0] + "<br/> ";
        if (!stuAttributes["familyID"][0].Equals("NA"))
        {
            string familyIDStr = stuAttributes["familyID"][0];
            EntityObject famObject = famHM[familyIDStr];
            var famAttributes = famObject.Attributes;
            string roomNumberStr = stuAttributes["roomNumber"][0];
            string effectiveQuarterStr = "";
            string placementTypeStr = "";
            string endQuarterStr = "";
            string sqlQuery = @"SELECT *
                              FROM [CCSInternationalStudent].[dbo].[HomestayPlacement]
                              WHERE roomNumber =" + roomNumberStr + " AND familyID= " + familyIDStr + " AND applicantID = " + selectedStudentIDStr;
            DataTable dtPlacements = hsInfo.RunGetQuery(sqlQuery);
            if(dtPlacements!=null && dtPlacements.Rows.Count > 0)
            {
                endQuarterStr = dtPlacements.Rows[0]["endQuarter"].ToString();
            }
            try
            {
                effectiveQuarterStr = famAttributes["room" + roomNumberStr][2];
                placementTypeStr = famAttributes["room" + roomNumberStr][1];
            }
            catch (Exception e)
            {

            }
            currStudentStatus += "Room number: " + roomNumberStr + "<br/>"
                                + "Effective Quarter: " + effectiveQuarterStr +"<br/>"
                                + "End Quarter: " + endQuarterStr + "<br/>"
                                + "Home name: " + stuAttributes["homeName"][0] + "<br/>";
            // Student Placed in room number: 2; home name: UserName, TEST
        }
        */
        string sqlQuery = @"SELECT *
                              FROM [CCSInternationalStudent].[dbo].[HomestayPlacement]
                              WHERE applicantID =" + selectedStudentIDStr;
        
        DataTable dtPlacements = hsInfo.RunGetQuery(sqlQuery);
        if (dtPlacements == null || dtPlacements.Rows.Count == 0)
        {
            currStudentStatus = "From HSStudentInfo";
        }
        else
        {
            
            //From placement table
            foreach (DataRow rowPlacement in dtPlacements.Rows)
            {
                //string 
                currStudentStatus += " familyID: " + rowPlacement["familyID"]
                                    + " ,placementType: " + rowPlacement["placementType"]
                                    + " ,homeName: " + "homeName"
                                    + " ,roomNumber" + rowPlacement["roomNumber"]
                                    + " ,effectiveQuarter: " + rowPlacement["effectiveQuarter"]
                                    + " ,endQuarter: " + rowPlacement["endQuarter"]
                                    + "<br/>";
            }
                
        }
        //foreach (DataRow rowPlacement in dtPlacements.Rows)
        //{
           // rmStatusArr[i - 1] += " studentID:" + rowPlacement["applicantID"] + ", familyID: " + rowPlacement["familyID"]
              //              + " effectiveQuarter: " + rowPlacement["effectiveQuarter"] + " endQuarter: " + rowPlacement["endQuarter"] + "  ";
       // }

        litCurrStudentStatus.Text = currStudentStatus;

        

    }

    protected void btnPlace_Click(object sender, EventArgs e)
    {
        int selectedFamilyID = Convert.ToInt32(ddlFamilies.SelectedValue);
        EntityObject famObject = famHM[selectedFamilyID + ""];
        Dictionary<string, string[]> famAttributes = famObject.Attributes;

        string selectedStudentStr = ddlStudents.SelectedValue;
        string selectedFamilyStr = ddlFamilies.SelectedValue;

        string selectedPlacement = ddlRmPlacement.SelectedValue;
        string selectedRoomNumber = ddlRoom.SelectedValue;
        string selectedEffectiveQuater = ddlEffectiveQtr.SelectedValue;
        string selectedEndQuarter = ddlEndQtr.SelectedValue;

        

        //applicantID, familyID, roomNumber, effectiveQuarter, endQuarter
        string sqlQuery = @"SELECT *
                              FROM [CCSInternationalStudent].[dbo].[HomestayPlacement]
                              WHERE applicantID = " + selectedStudentStr
                                    + " AND familyID = " + selectedFamilyStr
                                    + " AND roomNumber = " + selectedRoomNumber
                                    + " AND effectiveQuarter = '" + selectedEffectiveQuater + "'"
		                            + " AND endQuarter = '" + selectedEndQuarter + "'";
        DataTable dtPlacements = hsInfo.RunGetQuery(sqlQuery);
        

        //If "Place"
        if (dtPlacements == null || dtPlacements.Rows.Count == 0)
        {
            //If placement does not exist, insert into HomestayPlacement
            lbMsg.Text = "Insert";
            //Check for insert 

            string homeName = famAttributes["homeName"][0];
            string sqlInsert = @"INSERT INTO [dbo].[HomestayPlacement]
                                           (
                                            [placementName]
                                           ,[applicantID]
                                           ,[familyID]
                                            ,[roomNumber]
                                            ,[placementType]
                                            ,[effectiveQuarter]
                                            ,[endQuarter]
                                           )
                                     VALUES( '" + homeName + "'"
                                                + ", " + selectedStudentStr
                                                + ", " + selectedFamilyStr
                                                + ", " + selectedRoomNumber
                                                + ", '" + selectedPlacement + "'"
                                                + ", '" + selectedEffectiveQuater + "'"
                                                + ", '" + selectedEndQuarter + "'"
                                                + ")";
            hsInfo.runSPQuery(sqlInsert, true);
            litPlace.Text = "New record has been inserted!";
        }
        else
        {
            //If exist, update
            lbMsg.Text = "The record exists.";
            ddlRoom.Focus();
            return;
        }
       


        DisplayPanel2();  
    }
    protected void ShowData()
    {
       
        string selectedStudentIDStr = ddlStudents.SelectedValue;
        string familyIDStr = ddlFamilies.SelectedValue;
        string sqlQuery1 = @"SELECT *
                              FROM [CCSInternationalStudent].[dbo].[HomestayPlacement]
                              WHERE applicantID = " + selectedStudentIDStr
                                    + " AND familyID = " + familyIDStr;


        DataTable dtPlacements = hsInfo.RunGetQuery(sqlQuery1);
        gvStuFam.DataSource = dtPlacements;
        
        gvStuFam.DataBind();
        
    }

    protected void gvStuFam_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvStuFam.EditIndex = e.NewEditIndex;
        ShowData();
        


    }
    protected void gvStuFam_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        

        
        //Finding the controls from Gridview for the row which is going to update  
        Label id = (Label)gvStuFam.Rows[e.RowIndex].FindControl("lbl_ID");
        string idStr = id.Text;
        DropDownList ddlPlacementType = (DropDownList)gvStuFam.Rows[e.RowIndex].FindControl("ddlPlacementType");
        DropDownList ddlRoomGv = (DropDownList)gvStuFam.Rows[e.RowIndex].FindControl("ddlRoomGv");
        DropDownList ddlEffectiveQuarter = (DropDownList)gvStuFam.Rows[e.RowIndex].FindControl("ddlEffectiveQuarter");
        DropDownList ddlEndQuarter = (DropDownList)gvStuFam.Rows[e.RowIndex].FindControl("ddlEndQuarter");
        int selectedFamilyID = Convert.ToInt32(ddlFamilies.SelectedValue);
        EntityObject famObject = famHM[selectedFamilyID + ""];
        Dictionary<string, string[]> famAttributes = famObject.Attributes;

        string selectedStudentStr = ddlStudents.SelectedValue;
        string selectedFamilyStr = ddlFamilies.SelectedValue;

        string selectedPlacement = ddlPlacementType.SelectedValue;
        string selectedRoomNumber = ddlRoom.SelectedValue;
        string selectedEffectiveQuater = ddlEffectiveQtr.SelectedValue;
        string selectedEndQuarter = ddlEndQtr.SelectedValue;
        //TextBox city = GridView1.Rows[e.RowIndex].FindControl("txt_City") as TextBox;
        //If "Withdrew"
        if (!selectedPlacement.Contains("Place"))
        {
            string sqlDelete = @"DELETE FROM [CCSInternationalStudent].[dbo].[HomestayPlacement]"
                               + " WHERE id = " + idStr;
            hsInfo.runSPQuery(sqlDelete, false);
            litPlace.Text = "Withdrew!";
            
        }
        else
        {
            //Update
            string sqlUpdate = @"UPDATE [CCSInternationalStudent].[dbo].[HomestayPlacement]
                                SET placementType = '" + ddlPlacementType.SelectedValue + "'"
                                + "  ,roomNumber = '" + ddlRoomGv.SelectedValue + "'"
                                + "  ,effectiveQuarter= '" + ddlEffectiveQuarter.SelectedValue + "'"
                                + "  ,endQuarter = '" + ddlEndQuarter.SelectedValue + "'"
                                + " WHERE id = " + idStr;
            hsInfo.runSPQuery(sqlUpdate, false);
            //update famHM 

            litPlace.Text = "Student has been updated!";
        }

        
        //Setting the EditIndex property to -1 to cancel the Edit mode in Gridview  
        gvStuFam.EditIndex = -1;
        //Call ShowData method for displaying updated data  
        ShowData();
        DisplayPanel2();
    }
    protected void gvStuFam_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvStuFam.EditIndex = -1;
        ShowData();
    }

    protected void gvStuFam_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        gvStuFam.Columns[1].Visible = false;
        //DropDownList ddlPlacementType = (DropDownList)e.Row.FindControl("ddlPlacementType");
        //ddlPlacementType.SelectedValue = "Withdrew";
        //ShowData();
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if ((e.Row.RowState & DataControlRowState.Edit) > 0)
            {
                //placementType
                DropDownList ddlPlacementType = (DropDownList)e.Row.FindControl("ddlPlacementType");

                //return DataTable havinf department data
                //DataTable dt = getDepartment();
                //ddList.DataSource = dt;
                //ddList.DataTextField = "dpt_Name";
                //ddList.DataValueField = "dp_Id";
                //ddList.DataBind();

                DataRowView dr = e.Row.DataItem as DataRowView;
                ddlPlacementType.SelectedValue = dr["placementType"].ToString();

                //roomNumber
                DropDownList ddlRoomGv = (DropDownList)e.Row.FindControl("ddlRoomGv");
                ddlRoomGv.SelectedValue = dr["roomNumber"].ToString();

                //effectiveQuarter
                DropDownList ddlEffectiveQuarter = (DropDownList)e.Row.FindControl("ddlEffectiveQuarter");
                DataTable dtQuarters = QMD.GetQuarterStart(-3, 15);
                //DataTable dtQuarters = QMD.GetQuarterStart(-1, 1);
                ddlEffectiveQuarter.DataSource = dtQuarters;
                ddlEffectiveQuarter.DataTextField = "textField";
                ddlEffectiveQuarter.DataValueField = "valueField";
                ddlEffectiveQuarter.DataBind();
                ddlEffectiveQuarter.Items.Insert(0, new ListItem("--Select a quarter--", "")); //Add default value


                ddlEffectiveQuarter.SelectedValue = dr["effectiveQuarter"].ToString();

                //endQuarter
                DropDownList ddlEndQuarter = (DropDownList)e.Row.FindControl("ddlEndQuarter");

                //DataTable dtQuarters = QMD.GetQuarterStart(-1, 1);
                ddlEndQuarter.DataSource = dtQuarters;
                ddlEndQuarter.DataTextField = "textField";
                ddlEndQuarter.DataValueField = "valueField";
                ddlEndQuarter.DataBind();
                ddlEndQuarter.Items.Insert(0, new ListItem("--Select a quarter--", "")); //Add default value


                ddlEndQuarter.SelectedValue = dr["endQuarter"].ToString();
            }
        }
    }

    
}