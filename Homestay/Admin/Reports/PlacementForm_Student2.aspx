﻿<%@ Page Language="C#" Title="Homestay Family" EnableEventValidation="false" EnableViewState="true"  AutoEventWireup="true" MasterPageFile="~/CCSApps.master" CodeFile="PlacementForm_Student2.aspx.cs" Inherits="Homestay_Admin_Reports_PlacementForm_Student2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
<style type="text/css">
    p
    {
        padding:2px;
        margin:0px;
    }
    .editClass{
        width:80%;
    }
   
    tr td:first-child{
        font-weight:bold;
    }

  
</style>
    <link href="/Homestay/_css/Print.css" rel="stylesheet" type="text/css" media="print" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="bannerH1Text" Runat="Server">
    Homestay Administration
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">
<div style="width:100%;text-align:center;margin-bottom:50px;">
    <div style="width:20%;float:left">
        <img src="/Homestay/images/ccs-logo.jpg" style="height: 104px; width: 211px" />
    </div>
    <div style="width:80%">
        <p style="font-size:30px;font-weight:bold">Information about your International Homestay Family</p>       
    </div>
    
   <br /> 

</div>
    
<div id="divHome" class="noPrint" runat="server" >
    
    <asp:Button  ID="btnHome" Text="Return to Admin Home Page" runat="server" class="ButtonAdmin" OnClick="linkBtnHome_Click"/>&nbsp;&nbsp;
    <asp:Button  ID="btnReset" Text="Reset" runat="server" class="ButtonAdmin" OnClick="btnReset_Click"/>
     
    <asp:Button ID="btnExport" runat="server" class="noPrint" Text="Export to PDF" OnClick="btnExport_Click" />
    <input type="button" name="btnPrint" class="noPrint" onclick="javascript:window.print();" value="Print" />
</div>
    <asp:Literal ID="litResutlMsg" runat="server"></asp:Literal>
    
    

    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <p aria-autocomplete="both">
                <asp:DetailsView ID="dvPlacement" runat="server" 
                    AutoGenerateRows="False" CellPadding="10" Width="100%" Font-Size="Large" 
                    OnItemUpdating="DetailsViewExample_ItemUpdating"
                    OnModeChanging="DetailsViewExample_ModeChanging"
                    OnItemCommand="DetailsViewExample_ItemCommand1" OnDataBound="DetailsViewExample_DataBound">
         <FieldHeaderStyle BackColor="#E4E4E4"  Font-Bold="True" Height="50px" Width="30%" />
         <RowStyle BorderStyle="Solid" BorderWidth="2px" />
        <Fields>
            <asp:TemplateField HeaderText="Student">
                <ItemTemplate>
                    <asp:Label ID="lblCampus" Text='<%# Bind("Student") %>' runat="server" ></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Date Of Birth">
                <ItemTemplate>
                    <asp:Label ID="lblQuarterOfArrival" Text='<%# Bind("DOB") %>' runat="server" ></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Country of Citizenship">
                <ItemTemplate>
                    <asp:Label ID="lblOrientationStartDate" Text='<%# Bind("Country") %>' runat="server"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="Homestay Family and Children">
                <ItemTemplate>
                    <asp:Label ID="lblQuarterStartDate" Text='<%# Bind("HomestayFamily") %>' runat="server" ></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Address">
                <ItemTemplate>
                    <asp:Label ID="lblStudentName" Text='<%# Bind("Address") %>' runat="server" ></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Phone numbers">
                <ItemTemplate>
                    <asp:Label ID="lblStudentGender" Text='<%# Bind("PhoneNumber") %>' runat="server" ></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Emails <p><i>Please email your homestay family to say hello before you arrive!</i></p>">
                <ItemTemplate>
                    <asp:Label ID="lblDOB" Text='<%# Bind("Email") %>' runat="server" ></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Transportation">
                <ItemTemplate>
                    <asp:Label ID="lblCountry" Text='<%# Bind("Transportation") %>' runat="server" ></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Hobbies and Interests">
                <ItemTemplate>
                    <asp:Label ID="lblHobbies" Text='<%# Bind("Hobbies") %>' runat="server" ></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txtHobbies" runat="server" Text='<%# Bind("Hobbies") %>' TextMode="MultiLine" Rows="2" Width="80%"></asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Smoking policy <p><i>(note that students in Washington, under age 18, are not allowed to smoke or possess tobacco products)</i></p>">
                <ItemTemplate>
                    <asp:Label ID="lblSmokingPolicy" Text='<%# Bind("SmokingPolicy") %>' runat="server" ></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:RadioButtonList  ID="rblSmokingPolicy" runat="server">
                        <asp:ListItem Text="Smoking permitted at this home"  ></asp:ListItem>
                        <asp:ListItem Text="No smoking while a resident of this home"  ></asp:ListItem>
                        <asp:ListItem Text="Smoking outside or away from home only"  ></asp:ListItem>
                    </asp:RadioButtonList>             
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Notes">
                <ItemTemplate>
                    <asp:Label ID="lblNotes" Text='<%# Bind("Notes") %>' runat="server" ></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txtNotes" runat="server" Text='<%# Bind("Notes") %>' TextMode="MultiLine" Rows="2" Width="80%"></asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Airport pickup <p><i>If student’s arrival at the Spokane airport (GEG) is after 9 pm, the student is expected to stay in one of our suggested hotels until the next day, at which time, the homestay family will pick the student up. If a student under the age of 18 has no other choice but to arrive at the Spokane airport after 9pm, special arrangements must be made.</i></p>">
                <ItemTemplate>
                    <asp:Label ID="lblEmail" Text='<%# Bind("AirportPickup") %>' runat="server" ></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
           <asp:CommandField ShowEditButton="true"  ItemStyle-CssClass="noPrint" /> 
        </Fields>
        
    </asp:DetailsView>
            </p>
            <div class="noPrint">
                 From: <br /><asp:TextBox ID="txtFrom" runat="server" Text="Internationalhomestay@ccs.spokane.edu" Width="500px" Enabled="false"/> <br />
                 To: <br /><asp:TextBox ID="txtTo" runat="server" Width="500px"/> <br />
                 CC: <br /><asp:TextBox ID="txtCC" runat="server" Width="500px"/> <br />
            </div>
           
             <asp:button id="btnSendMailClient" Cssclass="noPrint" runat="server" Onclick="SendMail_Click"  
                  Text="Send Mail"/>
            <br />
            <asp:Literal ID="litResultMsg" runat="server"/>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" DynamicLayout="true" AssociatedUpdatePanelID="UpdatePanel2" DisplayAfter="500" >

       <ProgressTemplate>
          <img src="/Homestay/images/demo_wait.gif" />
           Please Wait...
       </ProgressTemplate>
   
    </asp:UpdateProgress>
    
    <asp:Panel ID="PDFExprt" runat="server" Visible="false">
        
        <div style="width:100%;text-align:center;margin-bottom:50px;">
           
            <div style="width:80%;text-align:center;margin-bottom:50px;float:right">
                <p style="font-size:25px;font-weight:bold">Information about your International Homestay Family</p>       
            </div>
           <br /> 

        </div>
        <table cellpadding="10" cellspacing="0" rules="all" border="1" id="MainContent_dvPlacement" style="width:100%;border-collapse:collapse;">
			<tr style="border-width:2px;border-style:Solid;">
				<td style="background-color:#E4E4E4;font-weight:bold;height:50px;width:30%;">Student</td><td>
                     <asp:Label ID="lblPdfStudent"  runat="server" ></asp:Label>
                </td>
			</tr><tr style="border-width:2px;border-style:Solid;">
				<td style="background-color:#E4E4E4;font-weight:bold;height:50px;width:30%;">Date Of Birth</td><td>
                    <asp:Label ID="lblPdfDOB"  runat="server" ></asp:Label>
                </td>
			</tr><tr style="border-width:2px;border-style:Solid;">
				<td style="background-color:#E4E4E4;font-weight:bold;height:50px;width:30%;">Country of Citizenship</td><td>
                    <asp:Label ID="lblPdfCountry"  runat="server" ></asp:Label>
                </td>
			</tr>
            <tr style="border-width:2px;border-style:Solid;">
				<td style="background-color:#E4E4E4;font-weight:bold;height:50px;width:30%;">Homestay Family and Children</td><td>
                    <asp:Label ID="lblPdfHomestayFamily"  runat="server" ></asp:Label>
                </td>
			</tr><tr style="border-width:2px;border-style:Solid;">
				<td style="background-color:#E4E4E4;font-weight:bold;height:50px;width:30%;">Address</td><td>
                    <asp:Label ID="lblPdfAddress"  runat="server" ></asp:Label>
                </td>
			</tr><tr style="border-width:2px;border-style:Solid;">
				<td style="background-color:#E4E4E4;font-weight:bold;height:50px;width:30%;">Phone numbers</td><td>
                    <asp:Label ID="lblPdfPhoneNumber"  runat="server" ></asp:Label>
                </td>
			</tr><tr style="border-width:2px;border-style:Solid;">
				<td style="background-color:#E4E4E4;height:50px;width:30%;"><b>Emails</b> <br />
                     <span style="font-style:italic;font-size:smaller">Please email your homestay family to say hello before you arrive!</span></td>
                <td>
                    <asp:Label ID="lblPdfEmail"  runat="server" ></asp:Label>
                </td>
			</tr><tr style="border-width:2px;border-style:Solid;">
				<td style="background-color:#E4E4E4;font-weight:bold;height:50px;width:30%;">Transportation</td>
                <td>
                    <asp:Label ID="lblPdfTransportation"  runat="server" ></asp:Label>
                </td>
			</tr><tr style="border-width:2px;border-style:Solid;">
				<td style="background-color:#E4E4E4;font-weight:bold;height:50px;width:30%;">Hobbies and Interests</td>
                <td>
                    <asp:Label ID="lblPdfHobbies"  runat="server" ></asp:Label>
                </td>
			</tr>
               <tr style="border-width:2px;border-style:Solid;">
				<td style="background-color:#E4E4E4;height:50px;width:30%;"><b>Smoking policy</b> <br />
                    <span style="font-style:italic;font-size:smaller">(note that students in Washington, under age 18, are not allowed to smoke or possess tobacco products)</span>

				</td>
                <td>
                    <asp:Label ID="lblPdfSmokingPolicy"  runat="server" ></asp:Label>
                </td>
			</tr><tr style="border-width:2px;border-style:Solid;">
				<td style="background-color:#E4E4E4;font-weight:bold;height:50px;width:30%;">Notes</td>
                <td>
                    <asp:Label ID="lblPdfNotes"  runat="server" ></asp:Label>
                </td>
			</tr><tr style="border-width:2px;border-style:Solid;">
				<td style="background-color:#E4E4E4;height:50px;width:30%;"><strong>Airport pickup</strong><br />
                    <span style="font-style:italic;font-size:smaller">
                        If student’s arrival at the Spokane airport (GEG) is after 9 pm, the student is expected to stay in one of our suggested hotels until the next day, at which time, the homestay family will pick the student up. 
                        If a student under the age of 18 has no other choice but to arrive at the Spokane airport after 9pm, special arrangements must be made.
                    </span></td>
                <td>
                    <span>The family will pick you up at the airport. 
                                        Meet the family in the baggage claim area. Be sure your flight terminates at the GEG-Spokane Airport. 
                                        It is helpful to check with your family about travel dates before you buy your ticket. 
                                        <b>Please send your travel itinerary to: InternationalHomestay@ccs.spokane.edu</b> and to your family. 
                                        Please be sure to carry your family's phone number and email address with you on your flight. 
                                        If you arrive and cannot find your family at the airport, call: Ashley at 208-821-7626 or Amy at 509-844-5150 or Heather at 509-389-7368.</span>
                </td>
			</tr>
                
        </table>
       
    
</asp:Panel>
    <!-- Cannot use detailsview because it cannot generate html format. All will be converted to string -->

   
</asp:Content>
