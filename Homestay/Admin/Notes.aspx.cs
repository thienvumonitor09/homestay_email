﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Homestay_Admin_Notes : System.Web.UI.Page
{
    String strSQL = "";
    String strResultMsg = "";
    String selectedStudentSearch = "";
    String selectedFamilySearch = "";
    String selectedPlacementSearch = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        String selectedStudent = "0";
        String selectedFamily = "0";
        String selectedPlacement = "0";
        String selectedSearch = "";
        String strResultMsg = "";
        String strQueryType = "";
        String selectedNote = "0";
        String strButtonFace = "";
        String strNote = "";
        String strFunction = "Add a Note";
        String strUserName = "";
        String subject = "";
        String actionNeeded = "";
        String actionCompleted = "";
        String noteType = "";
        Boolean blAddMode = true;   //ADD OR EDIT A NOTE
        Boolean blAddView = true;   //NOTE VIEW OR GRID VIEW
        Int32 intResult = 0;
        DateTime startDate = System.DateTime.Today.AddDays(-15);
        DateTime endDate = System.DateTime.Today;
        Homestay hsInfo = new Homestay();
        DataTable dtStudents = hsInfo.GetHomestayStudents("Active", 0);
        DataTable dtFamilies = hsInfo.GetHomestayFamilyInfo(0, "", "DDLactive");
        DataTable dtNotes = hsInfo.GetHomestayNotes("All", "0", "0", "0", "0", "", "", "");
        String selectedView = rdoNoteView.SelectedValue;

        litResultMsg.Text = "";
        fldNewNote.Visible = true;
        if (selectedView == "Grid View") { blAddView = false; }
     //   Response.Write("blAddView?" + blAddView.ToString() + "<br />");
        if (blAddView)
        {
            //Set visibility of view; default is add/edit notes
            divGridView.Visible = false;
            divNote.Visible = true;
            divActivateButtons.Visible = true;
            divHome.Visible = true;
            divHomeGrid.Visible = false;
        }
        else
        {
            divGridView.Visible = true;
            divNote.Visible = false;
            divActivateButtons.Visible = false;
            divHomeGrid.Visible = true;
            divHome.Visible = false;
        }
        //Get postback values
        if (IsPostBack)
        {
            if (blAddView)
            {
                //capture selections
                selectedStudent = ddlStudents.SelectedValue;
                selectedFamily = ddlFamilies.SelectedValue;
                selectedNote = ddlNotes.SelectedValue;
                selectedPlacement = ddlPlacements.SelectedValue;
                if (selectedStudent == "") { selectedStudent = "0"; }
                if (selectedFamily == "") { selectedFamily = "0"; }
                if (selectedNote == "") { selectedNote = "0"; }
                if (selectedPlacement == "") { selectedPlacement = "0"; }
            }
            else
            {
                selectedFamilySearch = ddlFamilySearch.SelectedValue;
                selectedStudentSearch = ddlStudentSearch.SelectedValue;
                selectedPlacementSearch = ddlPlacementSearch.SelectedValue;
                noteType = ddlNoteType.SelectedValue;
                //Keep track of search type throughout load, edit and update events for correct data display
                if (Request.Form["ctl00$MainContent$btnDateSearch"] != null || (hdnSearchType.Value == "DateSearch" && hdnKeepSearch.Value == "true"))
                {
                    selectedSearch = "DateSearch";
                    hdnSearchType.Value = selectedSearch;
                    hdnKeepSearch.Value = "true";
                    SelectSQL(selectedSearch);
                }
                else if (Request.Form["ctl00$MainContent$btnStudentSearch"] != null || (hdnSearchType.Value == "StudentSearch" && hdnKeepSearch.Value == "true"))
                {
                    selectedSearch = "StudentSearch";
                    hdnSearchType.Value = selectedSearch;
                    SelectSQL(selectedSearch);
                    hdnKeepSearch.Value = "true";
                }
                else if (Request.Form["ctl00$MainContent$btnFamilySearch"] != null || (hdnSearchType.Value == "FamilySearch" && hdnKeepSearch.Value == "true"))
                {
                    selectedSearch = "FamilySearch";
                    hdnSearchType.Value = selectedSearch;
                    SelectSQL(selectedSearch);
                }
                else if (Request.Form["ctl00$MainContent$btnPlacementSearch"] != null || (hdnSearchType.Value == "PlacementSearch" && hdnKeepSearch.Value == "true"))
                {
                    selectedSearch = "PlacementSearch";
                    hdnSearchType.Value = selectedSearch;
                    SelectSQL(selectedSearch);
                }
                else if (Request.Form["ctl00$MainContent$btnCancel"] != null)
                {
                    hdnKeepSearch.Value = "false";
                }
            }
        }

        //Process Add/Edit functions
        if (blAddView)
        {
            //strResultMsg += "Selected Student: " + selectedStudent + "<br />";
            //strResultMsg += "Selected Family: " + selectedFamily + "<br />";
            if (selectedNote != "" && selectedNote != null && selectedNote != "0")
            {
                blAddMode = false;
            }
            //strResultMsg += "Add Mode? " + blAddMode + "<br />";
            //Process submission
            if (Request.Form["btnSubmit"] != null)
            {
                strButtonFace = Request.Form["btnSubmit"].ToString();
            }
//            strResultMsg += "Button Face: " + strButtonFace + "<br />";
 //           Response.Write("is add? " + blAddMode.ToString() + ", button face: " + strButtonFace + "<br />");
            if (strButtonFace != "")
            {
                strNote = Note.Text.Trim().Replace("'", "''");
                subject = txtSubject.Text.Trim().Replace("'", "''");
                noteType = ddlNoteType.SelectedValue;
                actionNeeded = rdoActionRequired.SelectedValue;
                actionCompleted = rdoActionCompleted.SelectedValue;
                switch (strButtonFace)
                {
                    case "Return to Admin Home Page":
                       Response.Redirect("/Homestay/Admin/Default.aspx");
                        break;
                    case "Save":
                        //insert
                        intResult = hsInfo.InsertHomestayNote(Convert.ToInt32(selectedStudent), Convert.ToInt32(selectedFamily), Convert.ToInt32(selectedPlacement), strNote, System.DateTime.Today, strUserName, actionNeeded, subject, noteType);
                        break;
                    case "Save Changes":
                        //update
                        intResult = hsInfo.UpdateHomestayNote(Convert.ToInt32(selectedNote), Convert.ToInt32(selectedStudent), Convert.ToInt32(selectedFamily), Convert.ToInt32(selectedPlacement), strNote, System.DateTime.Today, strUserName, actionNeeded, actionCompleted, subject, noteType);
                        break;
                    case "Update Status":
                        Response.Redirect("StatusMaintenance.aspx");
                        break;
                    case "Activate/De-activate":
                        Response.Redirect("Activation.aspx");
                        break;
                }
                if (intResult != 0) { strResultMsg += "Your submission processed successfully!<br />"; }
                ResetFields();
            }
            strUserName = Request.ServerVariables["LOGON_USER"];
            StaffUserName.Text = strUserName;
            //Load form
            switch (blAddMode)
            {
                case true:
                    //set visibility
                    divActionCompleted.Visible = false;
                    divSave.Visible = true;
                    divSaveChanges.Visible = false;
                    divEditDate.Visible = false;
                    //set default values
                    noteDate.Text = System.DateTime.Today.ToShortDateString();
                    break;
                case false:
                    //set visibility
                    divActionCompleted.Visible = true;
                    divSave.Visible = false;
                    divSaveChanges.Visible = true;
                    divEditDate.Visible = true;
                    //set default values
                    strFunction = "Edit a Note";
                    break;
            }
            //Set the page header
            litFunction.Text = strFunction;
            //Determine what type of data we want

            //Load the DDLs
            ddlStudents.Items.Clear();
            ListItem LI;
            LI = new ListItem();
            LI.Value = "0";
            LI.Text = "-- Select One --";
            ddlStudents.Items.Add(LI);
            if (dtStudents != null)
            {
                if (dtStudents.Rows.Count > 0)
                {
                    foreach (DataRow dr in dtStudents.Rows)
                    {
                        LI = new ListItem();
                        LI.Value = dr["applicantID"].ToString();
                        LI.Text = dr["familyName"].ToString() + ", " + dr["firstName"].ToString() + " - " + Convert.ToDateTime(dr["DOB"]).ToShortDateString();
                        ddlStudents.Items.Add(LI);
                    }
                }
                else { strResultMsg += "Error: dtStudents has no rows.<br />"; }
            }
            else { strResultMsg += "Error: dtStudents is null.<br />"; }



            ddlFamilies.Items.Clear();
            LI = new ListItem();
            LI.Value = "0";
            LI.Text = "-- Select One --";
            ddlFamilies.Items.Add(LI);
            if (dtFamilies != null)
            {
                if (dtFamilies.Rows.Count > 0)
                {
                    foreach (DataRow dr in dtFamilies.Rows)
                    {
                        LI = new ListItem();
                        LI.Value = dr["id"].ToString();
                        LI.Text = dr["homeName"].ToString();
                        ddlFamilies.Items.Add(LI);
                    }
                }
                else { strResultMsg += "Error: dtFamilies has no rows.<br />"; }
            }
            else { strResultMsg += "Error: dtFamilies is null.<br />"; }

            strQueryType = "DateRange";
            ddlNotes.Items.Clear();
            LI = new ListItem();
            LI.Value = "0";
            LI.Text = "-- Add New Note --";
            ddlNotes.Items.Add(LI);
 //           DataTable dtNotes = hsInfo.GetHomestayNotes(strQueryType, selectedStudent, selectedFamily, selectedPlacement, selectedNote, startDate.ToShortDateString(), endDate.ToShortDateString(), noteType);
            if (dtNotes != null)
            {
                if (dtNotes.Rows.Count > 0)
                {
                    foreach (DataRow dr in dtNotes.Rows)
                    {
                        LI = new ListItem();
                        LI.Value = dr["id"].ToString();
                        LI.Text = dr["subject"].ToString();
                        ddlNotes.Items.Add(LI);
                    }
                    if (selectedNote != "") { ddlNotes.SelectedValue = selectedNote; }
                }
                else { strResultMsg += "Error: dtNotes has no rows.<br />"; }
            }
            else { strResultMsg += "Error: dtNotes is null.<br />"; }

            ddlPlacements.Items.Clear();
            LI = new ListItem();
            LI.Value = "0";
            LI.Text = "-- Select One --";
            ddlPlacements.Items.Add(LI);
            //TO DO:  LOAD PLACEMENT DDL


            //Fetch the note
            if (!blAddMode)
            {
                //get one note
                DataTable dtNote = hsInfo.GetHomestayNotes("Edit", "0", "0", "0", selectedNote, endDate.ToShortDateString(), endDate.ToShortDateString(), noteType);
                if (dtNote != null)
                {
                    if (dtNote.Rows.Count > 0)
                    {
                        txtSubject.Text = dtNote.Rows[0]["subject"].ToString();
                        //override add mode default selections
                        try
                        {
                            ddlStudents.SelectedValue = dtNote.Rows[0]["applicantID"].ToString();
                        }
                        catch { ddlStudents.SelectedValue = "0"; strResultMsg += "Selected student id: " + dtNote.Rows[0]["applicantID"].ToString() + "<br />"; }
                        try
                        {
                            ddlFamilies.SelectedValue = dtNote.Rows[0]["familyID"].ToString();
                        }
                        catch { ddlFamilies.SelectedValue = "0"; strResultMsg += "Selected family id: " + dtNote.Rows[0]["familyID"].ToString() + "<br />"; }
                        ddlPlacements.SelectedValue = dtNote.Rows[0]["placementID"].ToString();
                        if (dtNote.Rows[0]["noteType"].ToString() == "")
                            ddlNoteType.SelectedIndex = -1;
                        else
                            ddlNoteType.SelectedValue = dtNote.Rows[0]["noteType"].ToString();
                        Note.Text = dtNote.Rows[0]["Note"].ToString();
                        if (!DBNull.Value.Equals(dtNote.Rows[0]["createDate"]))
                            noteDate.Text = Convert.ToDateTime(dtNote.Rows[0]["createDate"]).ToShortDateString();
                        else
                            noteDate.Text = "";
                        Response.Write("last modified date: " + DBNull.Value.Equals(dtNote.Rows[0]["lastModifiedDate"]).ToString() + "<br />");
                        if (!DBNull.Value.Equals(dtNote.Rows[0]["lastModifiedDate"]))
                            editDate.Text = Convert.ToDateTime(dtNote.Rows[0]["lastModifiedDate"]).ToShortDateString();
                        else
                            editDate.Text = "";
                        Boolean blActionNeeded = Convert.ToBoolean(dtNote.Rows[0]["actionNeeded"]);
                        if (blActionNeeded) { actionNeeded = "1"; } else { actionNeeded = "0"; }
                        rdoActionRequired.SelectedValue = actionNeeded;
                        Boolean blActionCompleted = Convert.ToBoolean(dtNote.Rows[0]["actionCompleted"]);
                        if (blActionCompleted) { actionCompleted = "1"; } else { actionCompleted = "0"; }
                        rdoActionCompleted.SelectedValue = actionCompleted;
                    }
                }
                /*
                 * Think this through in terms of add or edit - create date/lsat modified date and employee name/ID
                if (!blAddMode) { 
                    StaffUserName.Text = "";
                    // get the 
                */
            }
        }
        else
        {
            //Load grid view
            //Load the DDLs
            ddlStudentSearch.Items.Clear();
            ListItem LI;
            LI = new ListItem();
            LI.Value = "0";
            LI.Text = "-- Select One --";
            ddlStudentSearch.Items.Add(LI);
            if (dtStudents != null)
            {
                if (dtStudents.Rows.Count > 0)
                {
                    foreach (DataRow dr in dtStudents.Rows)
                    {
                        LI = new ListItem();
                        LI.Value = dr["applicantID"].ToString();
                        LI.Text = dr["familyName"].ToString() + ", " + dr["firstName"].ToString() + " - " + Convert.ToDateTime(dr["DOB"]).ToShortDateString();
                        ddlStudentSearch.Items.Add(LI);
                    }
                    //if (selectedStudent != "") { ddlStudents.SelectedValue = selectedStudent; }
                }
                else { strResultMsg += "Error: dtStudents has no rows.<br />"; }
            }
            else { strResultMsg += "Error: dtStudents is null.<br />"; }

            ddlFamilySearch.Items.Clear();
            LI = new ListItem();
            LI.Value = "0";
            LI.Text = "-- Select One --";
            ddlFamilySearch.Items.Add(LI);
            if (dtFamilies != null)
            {
                if (dtFamilies.Rows.Count > 0)
                {
                    foreach (DataRow dr in dtFamilies.Rows)
                    {
                        LI = new ListItem();
                        LI.Value = dr["id"].ToString();
                        LI.Text = dr["homeName"].ToString();
                        ddlFamilySearch.Items.Add(LI);
                    }
                    //if (selectedFamily != "") { ddlFamilies.SelectedValue = selectedFamily; }
                }
                else { strResultMsg += "Error: dtFamilies has no rows.<br />"; }
            }
            else { strResultMsg += "Error: dtFamilies is null.<br />"; }

            ddlPlacementSearch.Items.Clear();
            LI = new ListItem();
            LI.Value = "0";
            LI.Text = "-- Select One --";
            ddlPlacementSearch.Items.Add(LI);
            //TO DO:  LOAD PLACEMENT DDL
        }
        //Grab any messages from page or class and display
        litResultMsg.Text = "<p>" + hsInfo.strResultMsg + strResultMsg + "</p>";
    }

    private void ResetFields()
    {
        txtSubject.Text = "";
        Note.Text = "";
        StaffUserName.Text = "";
        rdoActionRequired.ClearSelection();
        rdoActionCompleted.ClearSelection();
        noteDate.Text = "";
        editDate.Text = "";
        ddlNoteType.SelectedValue = "0";
        ddlStudents.SelectedValue = "0";
        ddlFamilies.SelectedValue = "0";
        ddlPlacements.SelectedValue = "0";
        ddlNotes.SelectedValue = "0";
    }

    public void btnDateSearch_Click(object sender, EventArgs e)
    {
        SelectSQL("DateSearch");
        AppsSqlDataSource.SelectCommand = strSQL;
        try
        {
            gvNotes.DataBind();
            gvNotes.AllowPaging = false;
        }
        catch (Exception ex)
        {
            litResultMsg.Text += "Error: " + ex.Message + "<br />";
        }
        hdnKeepSearch.Value = "true";
        hdnSearchType.Value = "DateSearch";
    }
    public void btnFamilySearch_Click(object sender, EventArgs e)
    {
        SelectSQL("FamilySearch");
        AppsSqlDataSource.SelectCommand = strSQL;
        try
        {
            gvNotes.DataBind();
            gvNotes.AllowPaging = false;
        }
        catch (Exception ex)
        {
            litResultMsg.Text += "Error: " + ex.Message + "<br />";
        }
        hdnKeepSearch.Value = "true";
        hdnSearchType.Value = "FamilySearch";
        ddlFamilySearch.SelectedValue = selectedFamilySearch;
        litResultMsg.Text += "SQL: " + strSQL + "<br />";
    }
    public void btnStudentSearch_Click(object sender, EventArgs e)
    {
        SelectSQL("StudentSearch");
        AppsSqlDataSource.SelectCommand = strSQL;
        try
        {
            gvNotes.DataBind();
            gvNotes.AllowPaging = false;
        }
        catch (Exception ex)
        {
            litResultMsg.Text += "Error: " + ex.Message + "<br />";
        }
        hdnKeepSearch.Value = "true";
        hdnSearchType.Value = "StudentSearch";
        ddlStudentSearch.SelectedValue = selectedStudentSearch;
        //litResultMsg.Text += "SQL: " + strSQL + "<br />";
    }
    public void btnPlacementSearch_Click(object sender, EventArgs e)
    {
        SelectSQL("PlacementSearch");
        AppsSqlDataSource.SelectCommand = strSQL;
        try
        {
            gvNotes.DataBind();
            gvNotes.AllowPaging = false;
        }
        catch (Exception ex)
        {
            litResultMsg.Text += "Error: " + ex.Message + "<br />";
        }
        hdnKeepSearch.Value = "true";
        hdnSearchType.Value = "PlacementSearch";
        ddlPlacementSearch.SelectedValue = selectedPlacementSearch;
    }
    public void btnCancel_Click(object sender, EventArgs e)
    {
        ResetDateFields();
        string strSQL = "SELECT [familyID],[homeName],[applicantID],[studentName],[placementName],[noteID],[Note],[Date],[actionNeeded],[actionCompleted],[subject] FROM [CCSInternationalStudent].[dbo].[vw_HomestayNotesExtended] ORDER BY [Date] DESC";
        AppsSqlDataSource.SelectCommand = strSQL;
        try
        {
            gvNotes.DataBind();
        }
        catch (Exception ex)
        {
            litResultMsg.Text += "Error: " + ex.Message + "<br />";
        }
        hdnKeepSearch.Value = "false";
    }
    protected void OnRowEditing_Click(object sender, GridViewEditEventArgs e)
    {
        if (hdnKeepSearch.Value == "true")
        {
            SelectSQL(hdnSearchType.Value);
            AppsSqlDataSource.SelectCommand = strSQL;
            gvNotes.DataBind();
        }
    }

    protected void OnRowUpdated_Click(object sender, GridViewUpdatedEventArgs e)
    {
        if (hdnKeepSearch.Value == "true")
        {
            SelectSQL(hdnSearchType.Value);
            AppsSqlDataSource.SelectCommand = strSQL;
            try
            {
                gvNotes.DataBind();
            }
            catch
            {
                litResultMsg.Text += strResultMsg;
            }
        }
    }
    private void SelectSQL(string strType)
    {
        switch (strType)
        {
            case "DateSearch":
                string strEndDate = Convert.ToDateTime(dpEndDate.SelectedDate).AddDays(1).ToString();
                strSQL = "SELECT [familyID],[homeName],[applicantID],[studentName],[placementName],[id],[Note],[createDate],[actionNeeded],[actionCompleted],[subject],[lastModifiedDate] FROM [CCSInternationalStudent].[dbo].[vw_HomestayNotesExtended] WHERE (([createDate] >= '" + dpStartDate.SelectedDate + "' AND [createDate] <= '" + dpEndDate.SelectedDate + "') OR ([lastModifiedDate] >= '" + dpStartDate.SelectedDate + "' AND [lastModifiedDate] <= '" + dpEndDate.SelectedDate + "')) ORDER BY [createDate] DESC";
                break;
            case "FamilySearch":
                strSQL = "SELECT [familyID],[homeName],[applicantID],[studentName],[placementName],[id],[Note],[createDate],[actionNeeded],[actionCompleted],[subject],[lastModifiedDate] FROM [CCSInternationalStudent].[dbo].[vw_HomestayNotesExtended] WHERE ([familyID] = " + selectedFamilySearch + ")  ORDER BY [createDate] DESC";
                break;
            case "StudentSearch":
                strSQL = "SELECT [familyID],[homeName],[applicantID],[studentName],[placementName],[id],[Note],[createDate],[actionNeeded],[actionCompleted],[subject],[lastModifiedDate] FROM [CCSInternationalStudent].[dbo].[vw_HomestayNotesExtended] WHERE ([applicantID] = " + selectedStudentSearch + ")  ORDER BY [createDate] DESC";
                break;
            case "PlacementSearch":
                strSQL = "SELECT [familyID],[homeName],[applicantID],[studentName],[placementName],[id],[Note],[createDate],[actionNeeded],[actionCompleted],[subject],[lastModifiedDate] FROM [CCSInternationalStudent].[dbo].[vw_HomestayNotesExtended] WHERE ([placementID] = " + selectedPlacementSearch + ")  ORDER BY [createDate] DESC";
                break;
        }
    }
    private void ResetDateFields()
    {
        dpEndDate.SelectedDate = System.DateTime.Today;
        dpStartDate.SelectedDate = System.DateTime.Today;

    }


    protected void btnCmdReturn_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Homestay/Admin/Default.aspx");
    }
}