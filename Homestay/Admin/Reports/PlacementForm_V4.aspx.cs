﻿using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Homestay_Admin_Reports_PlacementForm_V4 : System.Web.UI.Page
{
    string strResultMsg = "";
    String selectedStudent = "0";
    DateTime dob;
    String arrivalDate;
    String shortDate = "";
    Homestay hsInfo = new Homestay();
    //DataTable dtDisplay = new DataTable();
    HomestayUtility hsUtility = new HomestayUtility();
    Utility utility = new Utility();
    Quarter_MetaData QMD = new Quarter_MetaData();
    protected void Page_Load(object sender, EventArgs e)
    {

        lblInfo.Text = @"<h3>Before your student arrives:</h3>
                        <ul>
                            <li>Review the Culturegram about the student's country and culture.       
                            <li>Be sure the room has all required items, working smoke detectors, C0<sub>2</sub> monitors, fire ladders, etc.</li>
                            <li>Attend the quarterly Family Meeting for Homestay Families.</li>
                        </ul>
            <h3>After your student arrives:</h3>
            <ol>
                <li>Meet your student at the airport near Baggage Claim. Bring a sign with <b><i>all</i></b> of their names on it.</li>
                <li>Bring the student to school for their Orientation and/or testing date(s).</li>
                <li>Ride the bus with them, at least once, to and from school. Inform them to carry cash for the bus fee until their ID card is active.
                    <p>Note:  They should <i>always</i> carry cash for the bus, just in case the system is ever down. Note: exact amount is needed - bus drivers can't make change; and, if they take 2 buses from your home to school, they can request a ""Transfer"" onto the second bus.  They can also buy a day pass for extended riding.
                    </p>
                </li>
                <li> Discuss the Homestay Student and Family Agreement and give the student a copy. Email a scanned copy to the Homestay Team at
                       <a href = ""mailto:InternationalHomestay@ccs.spokane.edu"" > InternationalHomestay@ccs.spokane.edu.</a> Be sure the student puts your
                              phone numbers in their own phone for emergencies, or if the need to contact you arises.</li>
          
                          <li> Help the student:
                            <ul>
                                <li> access your internet</ li >
   
                                <li> get a bank account</ li >
      
                                <li> purchase a cell phone plan</ li >
         
                            </ul>                         
                        </li>
         
                         <li> Be sure student attends the Homestay Success Meeting approximately one week after the quarter starts.</li>
            
             </ol>
             ";
        //Get studentID from querystring
        int intStudentID = GetQueryStringValue("studentID");
       
       // BindDetailView();
        //Populate ddlStudent
        if (!IsPostBack)
        {
            /*
            DataTable dtDisplay = GetViewState();
            dvPlacement.DataSource = dtDisplay;
            dvPlacement.DataBind();
            */
            BindDetailView();
            //
            //Populate emails
            int intFamilyID = GetQueryStringValue("familyID");
            DataTable dtRelatives = hsInfo.GetHomestayRelatives(0, intFamilyID);
            if (dtRelatives != null && dtRelatives.Rows.Count > 0)
            {
                foreach (DataRow drRelative in dtRelatives.Rows)
                {
                    if (drRelative["relationship"].ToString().Contains("Primary"))
                    {
                        txtTo.Text = drRelative["email"].ToString().Trim();
                    }
                }
            }
            //hdnText.Value = GetGridviewData(dvPlacement);

        }// NOT IsPostback
        
        //ddlStudent.SelectedValue = intStudentID+"";
        //GetDisplayDt();
        //BindDetailView();
        //selectedStudent = ddlStudent.SelectedValue;

        //DataTable dtStudentExtended2 = hsInfo.GetHomestayStudents("ID", Convert.ToInt32("0" + ddlStudent.SelectedValue));



    }

    private string GetShortDateString(string dateTimeStr)
    {
        DateTime arrivalDate;
        return DateTime.TryParse(dateTimeStr, out arrivalDate) ? arrivalDate.ToShortDateString() : "n/a";
    }
    protected void linkBtnHome_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Homestay/Admin/Default.aspx");
    }
   
    protected void DetailsViewExample_ItemUpdating(object sender, DetailsViewUpdateEventArgs e)
    {
        int intStudentID = GetQueryStringValue("studentID");
        DataTable dtDisplay = GetViewState();
        //txtOrientationStartDate
        //TextBox txtOrientationStartDate = (TextBox)dvPlacement.FindControl("txtOrientationStartDate");
        //lblOrientationStartDate
        Label lblOrientationStartDate = (Label)dvPlacement.FindControl("lblOrientationStartDate");
        //txtOrientationStartDate
        TextBox txtQuarterStartDate = (TextBox)dvPlacement.FindControl("txtQuarterStartDate");
        //txtUrgentCare
        TextBox txtUrgentCare = (TextBox)dvPlacement.FindControl("txtUrgentCare");
        //txtArrivalInfo
        TextBox txtArrivalInfo = (TextBox)dvPlacement.FindControl("txtArrivalInfo");
        //txtInterest
        TextBox txtInterest = (TextBox)dvPlacement.FindControl("txtInterest");
        //txtInterest
        TextBox txtNotes = (TextBox)dvPlacement.FindControl("txtNotes");
        //cblPgrogram
        CheckBoxList cblPgrogram = (CheckBoxList)dvPlacement.FindControl("cblPgrogram");

        var txtMajor = (TextBox)dvPlacement.FindControl("txtMajor");
        var interests = new List<string>();
        CheckBoxList cblProgram = (CheckBoxList)dvPlacement.FindControl("cblProgram");
        
        foreach (System.Web.UI.WebControls.ListItem item in cblProgram.Items)
        {
            if (item.Selected)
            {
                interests.Add(item.Value);
                
            }
        }
        //fill in Orientation start date
        string strOrientationStartDate = "";
        if (interests.Contains("IELP"))
        {
            strOrientationStartDate += "IELP";
        }
        if (interests.Contains("ACA") || interests.Contains("HSCP"))
        {
            strOrientationStartDate += "ACA";
        }

        //txtOrientationStartDate.Text = strOrientationStartDate;
        string strMajor = txtMajor.Text.Replace("'", "''");
        string updateQuery = @"UPDATE [dbo].[HomestayStudentInfo]
                                   SET Program ='" + String.Join("|", interests) + "'"
                                   + ", Major ='" + strMajor + "'"
                                 + " WHERE applicantID=" + intStudentID;
        hsInfo.runSPQuery(updateQuery, false);

        //Update dtDisplay

        dtDisplay.Rows[0]["OrientationStartDate"] = strOrientationStartDate;
        dtDisplay.Rows[0]["QuarterStartDate"] = txtQuarterStartDate.Text;
        dtDisplay.Rows[0]["Hobbies"] = txtInterest.Text;
        dtDisplay.Rows[0]["Program"] = String.Join("|", interests);
        dtDisplay.Rows[0]["Major"] = txtMajor.Text;
        dtDisplay.Rows[0]["Notes"] = txtNotes.Text;
        
        
        dtDisplay.Rows[0]["UrgentCare"] = txtUrgentCare.Text;
        dtDisplay.Rows[0]["ArrivalInfo"] = txtArrivalInfo.Text;
        ViewState["dtDisplay"] = dtDisplay;
        dvPlacement.ChangeMode(DetailsViewMode.ReadOnly);
        BindDetailView();
    }
    protected void DetailsViewExample_ItemCommand1(object sender, DetailsViewCommandEventArgs e)
    {
        //DataTable dtDisplay = GetViewState();
        switch (e.CommandName.ToString())
        {
            case "Edit":
                dvPlacement.ChangeMode(DetailsViewMode.Edit);
                BindDetailView();;
                break;
            case "Cancel":
                dvPlacement.ChangeMode(DetailsViewMode.ReadOnly);
                BindDetailView();
                break;
        }
    }
    protected void DetailsViewExample_ModeChanging(object sender, DetailsViewModeEventArgs e)
    {
    }
    protected void DetailsViewExample_DataBound(object sender, EventArgs e)
    {
        DataTable dtDisplay = GetViewState();
        if (((DetailsView)sender).CurrentMode == DetailsViewMode.Edit)
        {
            //DataTable dtDisplay = (DataTable)ViewState["dtDisplay"];
            
            DataRowView row = (DataRowView)((DetailsView)sender).DataItem;
            TextBox txtUrgentCare = (TextBox)dvPlacement.FindControl("txtUrgentCare");
            string[] interests = dtDisplay.Rows[0]["Program"].ToString().Trim().Split('|');
            CheckBoxList cblProgram = (CheckBoxList)dvPlacement.FindControl("cblProgram");
            //CheckBoxList checkBoxInterest = (CheckBoxList)dvStudent.Rows[2].Cells[0].Controls[2];

            foreach (System.Web.UI.WebControls.ListItem item in cblProgram.Items)
            {
                if (Array.Exists(interests, element => element == item.Value))
                {
                    item.Selected = true;
                }
            }
        }
    }
    private DataTable GetViewState()
    {

        DataTable dtDisplay = new DataTable();
        if (ViewState["dtDisplay"] != null)
        {
            dtDisplay = (DataTable)ViewState["dtDisplay"];
        }
        else
        {
            dtDisplay = GetDisplayDt();
        }
        return dtDisplay;
    }
    private DataTable GetDisplayDt()
    {
        DataTable dtDisplay = new DataTable();
        int intStudentID = GetQueryStringValue("studentID");
        //int intStudentID = hsUtility.ParseInt(ddlStudent.SelectedValue);
        //DataTable dtDisplay = new DataTable();
        dtDisplay.Columns.AddRange(new DataColumn[16]
                    {

                        new DataColumn("Campus", typeof(string)),
                        new DataColumn("QuarterOfArrival", typeof(string)),
                        new DataColumn("OrientationStartDate", typeof(string)),
                        new DataColumn("QuarterStartDate", typeof(string)),
                        new DataColumn("StudentName", typeof(string)),
                        new DataColumn("StudentGender", typeof(string)),
                        new DataColumn("DOB", typeof(string)),
                        new DataColumn("Country", typeof(string)),
                        new DataColumn("Program", typeof(string)),
                        new DataColumn("Major", typeof(string)),
                        new DataColumn("Email", typeof(string)),
                        new DataColumn("Hobbies", typeof(string)),
                        new DataColumn("Notes", typeof(string)),
                        new DataColumn("ArrivalInfo", typeof(string)),
                        new DataColumn("UrgentCare", typeof(string)),
                        new DataColumn("ApplicantID", typeof(string))
                    });
        DataRow displayRow = dtDisplay.NewRow();
        DataTable dtStudentExtended = hsInfo.GetHomestayStudents("ID", intStudentID); 
        //OrientationStartDate
        //displayRow["OrientationStartDate"] = "demo";
        if (dtStudentExtended != null && dtStudentExtended.Rows.Count > 0)
        {
            DataRow drStudent = dtStudentExtended.Rows[0];
            string sqlQuerySchoolInfo = @"SELECT *
                                  FROM [CCSInternationalStudent].[dbo].[SchoolInformation]
                                  where applicantID = " + intStudentID;
            DataTable dtSchoolInfo = hsInfo.RunGetQuery(sqlQuerySchoolInfo);
            //DataTable dtStudentMore = hsInfo.GetStudentStudyInfo(Convert.ToInt32( ddlStudent.SelectedValue));
            if (dtSchoolInfo != null && dtSchoolInfo.Rows.Count > 0)
            {
                DataRow drSchoolInfo = dtSchoolInfo.Rows[0];
                //Campus
                string campus = drSchoolInfo["studyWhere"].ToString();
                if (campus.Contains("SCC"))
                {
                    campus = "Spokane Community College - SCC";
                }
                else if (campus.Contains("SFCC"))
                {
                    campus = "Spokane Falls Community College - SFCC";
                }
                else if (campus.Contains("Pullman"))
                {
                    campus = "Pullman";
                }
                else
                {
                    campus = "Other";
                }
                displayRow["Campus"] = campus;

                string sqlQueryProgram = @"SELECT *
                                  FROM [CCSInternationalStudent].[dbo].[HomestayStudentInfo]
                                  where applicantID = " + intStudentID;
                DataTable dtHomestayStudentInfo = hsInfo.RunGetQuery(sqlQueryProgram);
                
                //Program
                displayRow["Program"] = dtHomestayStudentInfo.Rows[0]["Program"].ToString();
                //Major
                displayRow["Major"] = dtHomestayStudentInfo.Rows[0]["Major"].ToString();
                //OrientationStartDate
                string orientationStartDate = "";
                
                if (displayRow["Program"].ToString().Contains("IELP"))
                {
                    orientationStartDate = "IELP: March 20, 9:30am; Academic: March 28, 8:30am";
                }
                else
                {
                    orientationStartDate = "Academic: March 27, 8:30am";
                }
                displayRow["OrientationStartDate"] = orientationStartDate;
            }

            //QuarterOfArrival
            DataTable dtQ = QMD.GetQuarters();
            string strArrivalDate = GetShortDateString(drStudent["arrivalDate"].ToString());
           
            foreach (DataRow drQ in dtQ.Rows)
            {
                string strStartDate = GetShortDateString(drQ["StartDate"].ToString());
                string strEndDate = GetShortDateString(drQ["EndDate"].ToString());
                if (!strStartDate.Equals("n/a") && !strEndDate.Equals("n/a") && !strArrivalDate.Equals("n/a"))
                {
                    DateTime dtArrivalDate = Convert.ToDateTime(strArrivalDate);
                    DateTime dtStartDate = Convert.ToDateTime(strStartDate);
                    DateTime dtEndDate = Convert.ToDateTime(strEndDate);
                    if (DateTime.Compare(dtStartDate, dtArrivalDate) <= 0 && DateTime.Compare(dtEndDate, dtArrivalDate) >= 0)
                    {
                        string quarterName = drQ["Name"].ToString();
                        string yearS = Convert.ToDateTime(drQ["FirstDayOfClass"].ToString()).Year.ToString();
                        //displayRow["QuarterOfArrival"] = GetShortDateString(drStudent["arrivalDate"].ToString()) + " " + dtQ.Rows.Count;
                        displayRow["QuarterOfArrival"] = quarterName + " " + yearS;
                    }
                }
                else
                {
                    displayRow["QuarterOfArrival"] = "n/a";
                }
            }

            //QuarterOfArrival
            string strQuarterStartDate = GetShortDateString(drStudent["quarterStartDate"].ToString());
            displayRow["QuarterOfArrival"] = strQuarterStartDate;
            if (!string.IsNullOrEmpty(strQuarterStartDate))
            {
                foreach (DataRow drQ in dtQ.Rows)
                {
                    DateTime dtFirstDayOfClass = Convert.ToDateTime(drQ["FirstDayOfClass"].ToString());
                    DateTime dtQuarterStartDate = Convert.ToDateTime(strQuarterStartDate);
                    string quarterName = drQ["Name"].ToString();
                    if (DateTime.Compare(dtFirstDayOfClass, dtQuarterStartDate) == 0)
                    {
                        displayRow["QuarterOfArrival"] = quarterName + " " + dtFirstDayOfClass.Year.ToString();
                        break;
                    }
                }
            }
            
            

            //StudentName
            displayRow["StudentName"] = drStudent["firstName"].ToString().Trim().Replace("''", "'")
                                        + " " + drStudent["middleNames"].ToString().Trim().Replace("''", "'")
                                        + " " + drStudent["familyName"].ToString().Trim().Replace("''", "'");
            //Gender
            string gender = drStudent["Gender"].ToString();
            if (gender.Equals("M"))
            {
                gender = "Male";
            }
            else if (gender.Equals("F"))
            {
                gender = "Female";
            }
            else
            {
                gender = "Other";
            }
            displayRow["StudentGender"] = gender;

            
            //DOB
            displayRow["DOB"] = GetShortDateString(drStudent["DOB"].ToString());

            //Country
            displayRow["Country"] = drStudent["countryOfCitizenship"].ToString();

            //Email
            DataTable dtInfo = hsInfo.GetOneInternationalStudent("", intStudentID, "", "", "", "");
            if (dtInfo != null && dtInfo.Rows.Count > 0)
            {
                displayRow["Email"] = dtInfo.Rows[0]["Email"].ToString();
            }
            //Hobbies
            List<string> hobbiesList = hsUtility.GetPreferenceSelectionsByType("hobbies","student",intStudentID);
            displayRow["Hobbies"] = string.Join(", ", hobbiesList.ToArray());

            
            //UrgentCare
            displayRow["UrgentCare"] = "$20 co-pay";

            //ApplicantID
            displayRow["ApplicantID"] = intStudentID;

        }

        

        dtDisplay.Rows.Add(displayRow);
        return dtDisplay;
    }
    private void BindDetailView()
    {
        DataTable dtDisplay = GetViewState();
        //dtDisplay = GetDisplayDt();
        //DataTable dtDisplay = (DataTable)ViewState["dtDisplay"];
        //Bind dtDisplay to dvPlacement
        dvPlacement.DataSource = dtDisplay;
        dvPlacement.DataBind();
    }

    
    protected void Button1_Click(object sender, EventArgs e)
    {
        Process.Start(String.Format(
             "mailto:{0}?subject={1}&cc={2}&bcc={3}&body={4}",
              "", "", "", "", ""));
    }

    protected void SendMail_Click(object sender, EventArgs e)
    {
        //public string SendEmailMsg(string strTo, string strFrom, string strSubject, bool blIsHTML, string strBody, string strCCEmailAddresses)
        Utility utility = new Utility();
       
        string body = GetGridviewData(dvPlacement);
        body += lblInfo.Text;

        int intFamilyID = GetQueryStringValue("familyID");
        string result = "";
        
        /*
        DataTable dtRelatives = hsInfo.GetHomestayRelatives(0, intFamilyID);
        
        if (dtRelatives != null && dtRelatives.Rows.Count > 0)
        {
            foreach (DataRow drRelative in dtRelatives.Rows)
            {
                if (drRelative["relationship"].ToString().Contains("Primary"))
                {
                    string toEmail = drRelative["email"].ToString().Trim();
                    //"Internationalhomestay@ccs.spokane.edu"
                    result = utility.SendEmailMsg("vu.nguyen@ccs.spokane.edu", "Internationalhomestay@ccs.spokane.edu", "New Student Placement", true, body, "vu.nguyen@ccs.spokane.edu");
                }
            }
        }
        */
        string[] toEmailList = txtTo.Text.Split(';');
        foreach (string toEmail in toEmailList)
        {
            //txtTo.Text += toEmail;
            result = utility.SendEmailMsg(toEmail, "Internationalhomestay@ccs.spokane.edu", "New Student Placement", true, body, txtCC.Text);
        }
        
        //result = utility.SendEmailMsg(txtTo.Text, "Internationalhomestay@ccs.spokane.edu", "New Student Placement", true, body, txtCC.Text);
        if (result.Contains("Success"))
        {
            litResultMsg.Text = "An Email has been sent to Primary Contact of the Family.";
        }
        
    }

    // This Method is used to render gridview control
    public string GetGridviewData(DetailsView gv)
    {
        DetailsView gv2 = gv;

        StringBuilder strBuilder = new StringBuilder();
        StringWriter strWriter = new StringWriter(strBuilder);
        HtmlTextWriter htw = new HtmlTextWriter(strWriter);
        gv2.Rows[14].Visible = false; //Hide commandfield
        gv2.RenderControl(htw);
        gv2.Rows[14].Visible = true; //Unhide commandfield
        return strBuilder.ToString();
    }

    //This function to prevent exception when render page
    public override void VerifyRenderingInServerForm(Control control)
    {
        return;
    }

    public int GetQueryStringValue(string queryString)
    {
        int id = 0;
        if ((Request.QueryString[queryString] != null && Request.QueryString[queryString].ToString() != "0"))
        {
            id = hsUtility.ParseInt(Request.QueryString[queryString]);
        }
        return id;
    }

    [WebMethod]
    public static string GetString()
    {
        return "it worked";
    }
    protected void btnReset_Click(object sender, EventArgs e)
    {
        string s = HttpContext.Current.Request.Url.PathAndQuery;
        s = s.Substring(s.LastIndexOf('/'));
        Response.Redirect("." + s);
    }


}