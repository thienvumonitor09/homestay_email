﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;

/// <summary>
/// Summary description for WebService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
 [System.Web.Script.Services.ScriptService]
public class EmailToolWS : System.Web.Services.WebService
{
    private static String strUploadURL = @"\\ccs-internet\InternetContent\CCS\Homestay\EmailAttachments\";
    //private static readonly string strConnectionLocal = "Data Source=CCS-SQL-Dev;Workstation ID=internalEAN;Initial Catalog=CCSInternationalStudent;Integrated Security=true;";
    private static readonly string strConnectionLocal = ConfigurationManager.ConnectionStrings["ConnectionString_CCSInternationalStudent"].ToString();
    public EmailToolWS()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod()]
    public string HelloWorld()
    {
        return "Hello World";
    }

    [WebMethod()]
    public string GetAttachments()
    {
        //var filePath = HttpContext.Current.Server.MapPath("./UploadFiles/");
        var filePath = strUploadURL;
        string[] filePaths = Directory.GetFiles(filePath);
        ArrayList fileNameList = new ArrayList();
        foreach (string fullPath in filePaths)
        {
            fileNameList.Add(Path.GetFileName(fullPath));
        }
        //return string.Join("   ", fileNameList);
        JavaScriptSerializer js = new JavaScriptSerializer();
        js.MaxJsonLength = Int32.MaxValue;
        return js.Serialize(fileNameList);
    }

    [WebMethod()]
    public string GetEmailReports()
    {
        List<EmailReport> emailReports = new List<EmailReport>();
        DataSet ds = new DataSet();
        SqlConnection conn = new SqlConnection(strConnectionLocal);
        conn.Open();
        /*
        string query = @"SELECT  *
                        FROM[CCSInternationalStudent].[dbo].[EmailReports]
                        ORDER BY timestamp DESC";*/
        SqlCommand objCommand = new SqlCommand("usp_EmailTool_GetEmailReports", conn);
        objCommand.CommandType = CommandType.StoredProcedure;
        //SqlCommand cmd = new SqlCommand(query, conn);
        SqlDataAdapter da = new SqlDataAdapter(objCommand);
        da.Fill(ds);
        if (ds != null && ds.Tables.Count > 0)
        {
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                emailReports.Add(new EmailReport(dr["id"].ToString(), dr["fromEmail"].ToString(), dr["toEmail"].ToString(), 
                                                dr["ccEmail"].ToString(), dr["subject"].ToString(), 
                                                dr["emailContent"].ToString(), dr["attachments"].ToString(),
                                                dr["timestamp"].ToString()));
            }
        }
        conn.Close();

        JavaScriptSerializer js = new JavaScriptSerializer();
        js.MaxJsonLength = Int32.MaxValue;
        return js.Serialize(emailReports);
    }
    [WebMethod()]
    public string GetIndividualEmailReport(string reportID)
    {
        //List<EmailReport> emailReports = new List<EmailReport>();
        EmailReport emailReport = null;
        DataSet ds = new DataSet();
        SqlConnection conn = new SqlConnection(strConnectionLocal);
        conn.Open();
        /*
        string query = @"SELECT  *
                      FROM[CCSInternationalStudent].[dbo].[EmailReports] WHERE id="+reportID;
                      */
        SqlCommand objCommand = new SqlCommand("usp_EmailTool_GetIndividualEmailReport", conn);
        objCommand.CommandType = CommandType.StoredProcedure;
        // set values to parameters 
        objCommand.Parameters.AddWithValue("@reportID", reportID);
        //SqlCommand cmd = new SqlCommand(query, conn);
        SqlDataAdapter da = new SqlDataAdapter(objCommand);
        da.Fill(ds);
        
        if (ds != null && ds.Tables.Count > 0)
        {
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                emailReport = new EmailReport(dr["id"].ToString(), dr["fromEmail"].ToString(), dr["toEmail"].ToString(),
                                                dr["ccEmail"].ToString(), dr["subject"].ToString(),
                                                dr["emailContent"].ToString(), dr["attachments"].ToString(),
                                                dr["timestamp"].ToString());
            }
        }
        conn.Close();

        JavaScriptSerializer js = new JavaScriptSerializer();
        js.MaxJsonLength = Int32.MaxValue;
        return js.Serialize(emailReport);
    }

    [WebMethod()]
    public string GetTemplates()
    {
        List<Template> templates = new List<Template>();
        DataSet ds = new DataSet();
        SqlConnection conn = new SqlConnection(strConnectionLocal);
        conn.Open();
        /*
        string query = @"SELECT  [TemplateID]
                          ,[TemplateName]
                          ,[TemplateContent]
                      FROM[CCSInternationalStudent].[dbo].[Template]";
                      */
        SqlCommand cmd = new SqlCommand("usp_EmailTool_GetTemplates", conn);
        cmd.CommandType = CommandType.StoredProcedure;
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(ds);
        if (ds != null && ds.Tables.Count > 0)
        {
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                templates.Add(new Template(dr["TemplateID"].ToString(), dr["TemplateName"].ToString(), dr["TemplateContent"].ToString()));
            }
        }
        //templates.Add(new Template("0", "Custom...", "Custom.."));
        conn.Close();
        JavaScriptSerializer js = new JavaScriptSerializer();
        js.MaxJsonLength = Int32.MaxValue;
        return js.Serialize(templates);
    }

    [WebMethod()]
    public string SaveTemplate(string templateJS)
    {
        JavaScriptSerializer js = new JavaScriptSerializer();
        var deserializedResult = js.Deserialize<Template>(templateJS);
        //return deserializedResult.templateName;
        
        SqlConnection conn = new SqlConnection(strConnectionLocal);
        /*
        string sqlUpdate = @" UPDATE [CCSInternationalStudent].[dbo].[Template]
                                    SET [TemplateName] = @TemplateName , [TemplateContent] = @TemplateContent
                                    WHERE [TemplateID] = " + int.Parse(deserializedResult.templateID);
                                    */
        SqlCommand cmd = new SqlCommand("usp_EmailTool_UpdateTemplate", conn);
        // set values to parameters from textboxes
        cmd.Parameters.AddWithValue("@TemplateName", deserializedResult.templateName);
        cmd.Parameters.AddWithValue("@TemplateContent", deserializedResult.templateContent);
        cmd.Parameters.AddWithValue("@TemplateID", int.Parse(deserializedResult.templateID));
        cmd.CommandType = CommandType.StoredProcedure;
        try
        {
            conn.Open();
            cmd.ExecuteNonQuery();
        }
        catch (SqlException ex)
        {
            Console.WriteLine(ex.ToString());
        }
        finally
        {
            conn.Close();
        }
        
        return "ok";
    }
    [WebMethod()]
    public string SaveReports(string emailObj, string selectedTemplate, string emails, string ccEmails, string attachmentNames)
    {
        JavaScriptSerializer js = new JavaScriptSerializer();
        var deserializedEmailObj = js.Deserialize<Email>(emailObj);
        SqlConnection conn = new SqlConnection(strConnectionLocal);
        /*
        string sqlins = @"INSERT INTO [CCSInternationalStudent].[dbo].[EmailReports]  
                                (fromEmail
                                ,toEmail 
                                ,ccEmail
                                ,subject
                                ,emailContent
                                ,attachments
                                ,timestamp)
                            VALUES (
                                @FromEmail
                                ,@ToEmail 
                                ,@CcEmail
                                ,@Subject
                                ,@EmailContent
                                ,@Attachments
                                ,@Timestamp
                            );";
                            */
        SqlCommand cmd = new SqlCommand("usp_EmailTool_InsertEmailReport", conn);
        // set values to parameters from textboxes
        cmd.Parameters.AddWithValue("@FromEmail", deserializedEmailObj.FromEmail);
        cmd.Parameters.AddWithValue("@ToEmail", emails);
        cmd.Parameters.AddWithValue("@CcEmail", ccEmails);
        cmd.Parameters.AddWithValue("@Subject", deserializedEmailObj.Subject);
        cmd.Parameters.AddWithValue("@EmailContent", selectedTemplate);
        cmd.Parameters.AddWithValue("@Attachments", attachmentNames);
        cmd.Parameters.AddWithValue("@Timestamp", DateTime.Now);
        cmd.CommandType = CommandType.StoredProcedure;
        try
        {
            conn.Open();
            cmd.ExecuteNonQuery();
        }
        catch (SqlException ex)
        {
            Console.WriteLine(ex.ToString());
        }
        finally
        {
            conn.Close();
        }
        return "ok";
    }
    [WebMethod()]
    public string AddTemplate(string templateJS)
    {
        JavaScriptSerializer js = new JavaScriptSerializer();
        var deserializedResult = js.Deserialize<Template>(templateJS);
        //return deserializedResult.templateName;

        SqlConnection conn = new SqlConnection(strConnectionLocal);
        //string sqlins = @"INSERT INTO [CCSInternationalStudent].[dbo].[Template]  (TemplateName, TemplateContent) values (@TemplateName, @TemplateContent);";
        SqlCommand cmd = new SqlCommand("usp_EmailTool_InsertTemplate", conn);
        // set values to parameters from textboxes
        cmd.Parameters.AddWithValue("@TemplateName", deserializedResult.templateName);
        cmd.Parameters.AddWithValue("@TemplateContent", deserializedResult.templateContent);
        cmd.CommandType = CommandType.StoredProcedure;
        try
        {
            conn.Open();
            cmd.ExecuteNonQuery();
        }
        catch (SqlException ex)
        {
            Console.WriteLine(ex.ToString());
        }
        finally
        {
            conn.Close();
        }

        return "ok";
    }

    [WebMethod()]
    public string DeleteTemplate(string templateID)
    {
        //return deserializedResult.templateName;

        SqlConnection conn = new SqlConnection(strConnectionLocal);
        /*
        string sqlUpdate = @" DELETE FROM [CCSInternationalStudent].[dbo].[Template]
                              WHERE [TemplateID] = " + int.Parse(templateID);
                              */
        SqlCommand cmd = new SqlCommand("usp_EmailTool_DeleteTemplate", conn);
        cmd.Parameters.AddWithValue("@TemplateID", int.Parse(templateID));
        cmd.CommandType = CommandType.StoredProcedure;
        try
        {
            conn.Open();
            cmd.ExecuteNonQuery();
        }
        catch (SqlException ex)
        {
            Console.WriteLine(ex.ToString());
        }
        finally
        {
            conn.Close();
        }

        return "ok";
    }

    [WebMethod()]
    public string GetEmailGroup(string groupName)
    {
        ArrayList emails = new ArrayList();
        List<Person> persons = new List<Person>();
        DataSet ds = new DataSet();
        SqlConnection conn = new SqlConnection(strConnectionLocal);
        conn.Open();
        /*
        string query = @"SELECT DISTINCT [Email]
                          FROM [CCSInternationalStudent].[dbo].[ContactInformation]
                          WHERE Email = 'vu'";
                          */
        //string query = GetSQLStr(groupName);
        switch (groupName)
        {
            case "Testing 2":
                emails.Add("amy.cosgrove@ccs.spokane.edu");
                break;
            default:
                emails.Add("vu.nguyen@ccs.spokane.edu");
                break;
        }
        //string groupName = "All students";
        
        //emails.Add("vu.nguyen@ccs.spokane.edu");
        
        //string strConnection = "Data Source=CCS-SQL-Dev;Workstation ID=internalEAN;Initial Catalog=CCSEAN;Integrated Security=true;";
        
        
       
        SqlCommand cmd = new SqlCommand("usp_EmailTool_GetEmailGroup", conn);
        cmd.Parameters.AddWithValue("@GroupName", groupName);
        cmd.CommandType = CommandType.StoredProcedure;
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(ds);
        if (ds != null && ds.Tables.Count > 0)
        {
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                emails.Add(dr["Email"].ToString());
                //persons.Add(new Person(dr["fullName"].ToString(), dr["email"].ToString()));
            }
        }
        conn.Close();
        JavaScriptSerializer js = new JavaScriptSerializer();
        js.MaxJsonLength = Int32.MaxValue;
        return js.Serialize(emails);
        //return "ok";
    }
    [WebMethod()]
    public string GetEmailGroup2(string groupName)
    {
        List<Person> persons = new List<Person>();
        DataSet ds = new DataSet();
        SqlConnection conn = new SqlConnection(strConnectionLocal);
        conn.Open();
        //string query = GetSQLStr(groupName);
        if (!groupName.Equals("Individual Students") && !groupName.Equals("Individual Families"))
        {
            persons.Add(new Person("Vu Nguyen", "vu.nguyen@ccs.spokane.edu"));
        }
        
        SqlCommand cmd = new SqlCommand("usp_EmailTool_GetEmailGroup", conn);
        cmd.Parameters.AddWithValue("@GroupName", groupName);
        cmd.CommandType = CommandType.StoredProcedure;
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(ds);
        if (ds != null && ds.Tables.Count > 0)
        {
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                persons.Add(new Person(dr["fullName"].ToString(), dr["email"].ToString()));
            }
        }
        conn.Close();
        JavaScriptSerializer js = new JavaScriptSerializer();
        js.MaxJsonLength = Int32.MaxValue;
        return js.Serialize(persons);
    }

    [WebMethod()]
    public string SendEmailService(string emailObj, string selectedTemplate, string emails, string ccEmails, string attachmentNames)
    {
        JavaScriptSerializer js = new JavaScriptSerializer();
        var deserializedResult = js.Deserialize<Email>(emailObj);
        var attachmentList = js.Deserialize<List<string>>(attachmentNames);
        var result = "";
        //var filePath = HttpContext.Current.Server.MapPath("./UploadFiles/");
        var filePath = strUploadURL;
        result += filePath;
        ArrayList attachmentsList = new ArrayList();
        foreach (var attachmentEach in attachmentList)
        {
            //result = HttpContext.Current.Server.MapPath("./UploadFiles/" + attachmentEach);
            //attachmentsList.Add(HttpContext.Current.Server.MapPath("./UploadFiles/" + attachmentEach));
            attachmentsList.Add(strUploadURL + attachmentEach);
        }
        //var emailList = js.Deserialize<List<string>>(emails);
        /*
        var result = "";
        foreach (var emailEach in emailList)
        {
            result += " " + emailEach;
        }
        return result;*/
        //return emailObj;
        Utility uEmail = new Utility();
        //string sendTo = "vu.nguyen@ccs.spokane.edu";
        //string sendTo = deserializedResult.ToEmail;
        string sendTo = emails;
        //string sentFrom = "Internationalhomestay@ccs.spokane.edu";
        //string sentFrom = "vu.nguyen@ccs.spokane.edu";
        string sentFrom = deserializedResult.FromEmail;
        //string strSubject = "Problem sending homestay student applicaiton form submission email";
        string strSubject = deserializedResult.Subject;
        //string strBody = "<p><strong>Sent</strong </p>";
        string strBody = selectedTemplate;
        string strCCaddresses = ccEmails;
        js.MaxJsonLength = Int32.MaxValue;
        
        string strAttachments = js.Serialize(attachmentsList); // strAttachments is serialized has json format
        //string strCCaddresses = "Internationalhomestay@ccs.spokane.edu;laura.padden@ccs.spokane.edu";


        // string strEmailResult = uEmail.SendMultiEmailMsgWihAttachmentNoStoring(sendTo, sentFrom, strSubject, true, strBody, strCCaddresses,attachment);
        //string strEmailResult = uEmail.SendMultiEmailMsg(sendTo, sentFrom, strSubject, true, strBody, strCCaddresses);

        string strEmailResult = uEmail.SendMultiEmailMsgWihAttachmentStoring(sendTo, sentFrom, strSubject, true, strBody, strCCaddresses, strAttachments);
       
        return strEmailResult;
    }

    private static string GetSQLStr(string filter)
    {
        string query = "";
        switch (filter)
        {
            case "All students":
                query = @"WITH temp AS(
	                        SELECT applicantID, email
	                        ,ROW_NUMBER() OVER (
                                             PARTITION BY applicantID 
                                             ORDER BY applicantID DESC
         	                           ) AS [ROW NUMBER]
	                        FROM [CCSInternationalStudent].[dbo].[ContactInformation] 	
                        ),
                        temp2 AS(
	                        SELECT applicantID, email FROM temp
	                        WHERE temp.[ROW NUMBER] = 1
                        )

                        SELECT fullName, email
                        FROM temp2 
                        INNER JOIN ApplicantBasicInfo
                        ON applicantID = id
                        ORDER BY email ASC
                        ";
                break;
            case "All families":
                query = @"SELECT DISTINCT email
                          FROM [CCSInternationalStudent].[dbo].[HomestayRelatives]
                          WHERE relationship like '%Primary%'";
                break;
            case "Minor students":
                query = @"SELECT applicantID, Email, quarterStartDate, DOB
                        FROM (
	                        SELECT ContactInformation.id, ContactInformation.applicantID, quarterStartDate, Email
	                          FROM [CCSInternationalStudent].[dbo].[HomestayStudentInfo] 
	                          INNER JOIN [CCSInternationalStudent].[dbo].ContactInformation 
	                          ON HomestayStudentInfo.applicantID = ContactInformation.applicantID
		                         WHERE ContactInformation.id IN
		                         (
			                         SELECT MIN(ContactInformation.id) as minID
			                          FROM [CCSInternationalStudent].[dbo].[HomestayStudentInfo] 
			                          INNER JOIN [CCSInternationalStudent].[dbo].ContactInformation 
			                          ON HomestayStudentInfo.applicantID = ContactInformation.applicantID
			                        GROUP BY HomestayStudentInfo.applicantID
		                         )
	                         ) as tmp 
	                         INNER JOIN [CCSInternationalStudent].[dbo].[ApplicantBasicInfo] 
	                         ON  ApplicantBasicInfo.id = tmp.applicantID
	                         WHERE DATEADD(year,18, DOB) > quarterStartDate ";
                break;
            case "Adult students":
                query = @"SELECT applicantID, Email, quarterStartDate, DOB
                            FROM (
	                            SELECT ContactInformation.id, ContactInformation.applicantID, quarterStartDate, Email
	                              FROM [CCSInternationalStudent].[dbo].[HomestayStudentInfo] 
	                              INNER JOIN [CCSInternationalStudent].[dbo].ContactInformation 
	                              ON HomestayStudentInfo.applicantID = ContactInformation.applicantID
		                             WHERE ContactInformation.id IN
		                             (
			                             SELECT MIN(ContactInformation.id) as minID
			                              FROM [CCSInternationalStudent].[dbo].[HomestayStudentInfo] 
			                              INNER JOIN [CCSInternationalStudent].[dbo].ContactInformation 
			                              ON HomestayStudentInfo.applicantID = ContactInformation.applicantID
			                            GROUP BY HomestayStudentInfo.applicantID
		                             )
	                             ) as tmp 
	                             INNER JOIN [CCSInternationalStudent].[dbo].[ApplicantBasicInfo] 
	                             ON  ApplicantBasicInfo.id = tmp.applicantID
	                             WHERE DATEADD(year,18, DOB) <= quarterStartDate ";
                break;
            case "Homes with any open room":
                query = @"DECLARE  @currentDate datetime;
                        SET  @currentDate =GETDATE();
                        WITH HomeContact (familyID, email)
                        AS
                        (  
                            SELECT  familyID, email
                          FROM [CCSInternationalStudent].[dbo].[HomestayRelatives]
                          WHERE relationship like 'Primary Contact'
                        )  
                        SELECT id, username, homeName, email,room1StartDate, room1EndDate, room2StartDate, room2EndDate, room3StartDate, room3EndDate, room4StartDate, room4EndDate, familyStatus
                          FROM [CCSInternationalStudent].[dbo].[HomestayFamilyInfo]
                          INNER JOIN HomeContact
                          ON familyID = id
                        WHERE (room1EndDate < @currentDate OR room2EndDate < @currentDate 
                                OR room3EndDate < @currentDate OR room4EndDate < @currentDate)";
                break;
            case "Occupied homes":
                query = @"DECLARE  @currentDate datetime;
                        SET  @currentDate =GETDATE();
                        WITH HomeContact (familyID, email)
                        AS
                        (  
                            SELECT  familyID, email
                          FROM [CCSInternationalStudent].[dbo].[HomestayRelatives]
                          WHERE relationship like 'Primary Contact'
                        )  
                        SELECT id, username, homeName, email,room1StartDate, room1EndDate, room2StartDate, room2EndDate, room3StartDate, room3EndDate, room4StartDate, room4EndDate, familyStatus
                          FROM [CCSInternationalStudent].[dbo].[HomestayFamilyInfo]
                          INNER JOIN HomeContact
                          ON familyID = id
                        WHERE (room1EndDate > @currentDate OR room2EndDate > @currentDate 
                            OR room3EndDate > @currentDate OR room4EndDate > @currentDate)";
                break;
            case "Prospective families (applied but not yet approved)":
                query = @"DECLARE  @currentDate datetime;
                        SET  @currentDate =GETDATE();
                        WITH HomeContact (familyID, email)
                        AS
                        (  
                            SELECT  familyID, email
                          FROM [CCSInternationalStudent].[dbo].[HomestayRelatives]
                          WHERE relationship like 'Primary Contact'
                        )  
                        SELECT id, username, homeName, email,room1StartDate, room1EndDate, room2StartDate, room2EndDate, room3StartDate, room3EndDate, room4StartDate, room4EndDate, familyStatus
                          FROM [CCSInternationalStudent].[dbo].[HomestayFamilyInfo]
                          INNER JOIN HomeContact
                          ON familyID = id
                        WHERE familyStatus NOT IN ('Needs Placement', 'Student Placed' , 'Tentative Placement') OR familyStatus  IS NULL";
                break;
            case "Individual Students":
                query = @"WITH temp AS(
	                        SELECT applicantID, email
	                        ,ROW_NUMBER() OVER (
                                             PARTITION BY applicantID 
                                             ORDER BY applicantID DESC
         	                           ) AS [ROW NUMBER]
	                        FROM [CCSInternationalStudent].[dbo].[ContactInformation] 	
                        ),
                        temp2 AS(
	                        SELECT applicantID, email FROM temp
	                        WHERE temp.[ROW NUMBER] = 1
                        )

                        SELECT fullName, email
                        FROM temp2 
                        INNER JOIN ApplicantBasicInfo
                        ON applicantID = id
                        ORDER BY fullName ASC";
                break;
            case "Individual Families":
                query = @"SELECT applicantId, (firstname+ ' ' + familyName) as fullName, email, relationship
                          FROM [CCSInternationalStudent].[dbo].[HomestayRelatives]
                          WHERE relationship like '%Primary Contact%'
                          ORDER BY fullName ASC";
                break;
            default:
                query = @"SELECT DISTINCT [Email]
                          FROM [CCSInternationalStudent].[dbo].[ContactInformation]
                          WHERE Email = 'vu'";
                break;
        }
        return query;
    }
    public class Person
    {
        public string name;
        public string email;
        

        public Person() { }
        public Person(string name, string email)
        {
            this.name = name;
            this.email = email;
        }
    }

    public class Email
    {
        public string fromEmail;
        public string toEmail;
        public string subject;
        public string body;

        public string FromEmail{ get; set; }
        public string ToEmail { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }

        public Email() { }
        public Email(string fromEmail, string toEmail, string subject, string body)
        {
            this.FromEmail = fromEmail;
            this.toEmail = toEmail;
            this.subject = subject;
            this.body = body;
        }
    }

    public class Template
    {
        public string templateID;
        public string templateName;
        public string templateContent;


        public Template() { }

        public Template(string templateID, string templateName, string templateContent)
        {
                this.templateID = templateID;
                this.templateName = templateName;
                this.templateContent = templateContent;
        }

    }
    public class EmailReport
    {
        public string reportID;
        public string fromEmail;
        public string toEmail;
        public string ccEmail;
        public string subject;
        public string emailContent;
        public string attachments;
        public string timestamp;
        public EmailReport() { }

        public EmailReport(string reportID, string fromEmail, string toEmail, string ccEmail, string subject, string emailContent, 
                                string attachments,string timestamp)
        {
            this.reportID = reportID;
            this.fromEmail = fromEmail;
            this.toEmail = toEmail;
            this.ccEmail = ccEmail;
            this.subject = subject;
            this.emailContent = emailContent;
            this.attachments = attachments;
            this.timestamp = timestamp;
        }
    }

    public class EmailGroup
    {
        public string groupName;
        public ArrayList emails;

        public EmailGroup(string groupName, ArrayList emails)
        {
            this.groupName = groupName;
            this.emails = emails;
        }
    }

    [WebMethod()]
    public string UploadFile()
    {
        var postedFile = System.Web.HttpContext.Current.Request.Files["file"];
        //return postedFile.FileName;
        string sPath = "";
        if (postedFile != null)
        {

            var filePath = HttpContext.Current.Server.MapPath("./UploadFiles/" + postedFile.FileName);
            postedFile.SaveAs(filePath);
            return filePath;
        }
        else
        {
            return "Error";
        }



    }

}
