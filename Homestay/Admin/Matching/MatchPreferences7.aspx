﻿<%@ Page Title="Matching" Language="C#" MasterPageFile="~/CCSApps.master" AutoEventWireup="true" CodeFile="MatchPreferences7.aspx.cs" Inherits="Homestay_Admin_MatchPreferences7" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="bannerH1Text" Runat="Server">
    Match Preferenes
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">
     <asp:Button ID="btnReset" runat="server" Text="Reset" OnClick="btnReset_Click" />
    
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <br/><i>* required</i>
            <h3>Select a Student</h3>
            <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
            
            <asp:DropDownList ID="ddlStudents" runat="server" AppendDataBoundItems="true" 
                AutoPostBack="true">

            </asp:DropDownList> *
             
            <br />
            <h3>Level of Matching</h3>
            <div style="width:100%;float:left; margin-bottom:10px">
                <div style="float:left;width:200px;">
                    <label for="rdoGender"><strong>Gender</strong></label>
                    <asp:RadioButtonList ID="rdoGender" RepeatDirection="Horizontal" runat="server">
                        <asp:ListItem Value="Include" Selected="True"></asp:ListItem>
                        <asp:ListItem Value="Omit"></asp:ListItem>
                    </asp:RadioButtonList>
                </div>
                <div style="float:left;width:200px;">
                     <label for="rdoHomestay"><strong>Homestay Preference</strong> </label>
                    <asp:RadioButtonList ID="rdoHomestay" RepeatDirection="Horizontal" runat="server">
                        <asp:ListItem Value="Include" Selected="True"></asp:ListItem>
                        <asp:ListItem Value="Omit"></asp:ListItem>
                    </asp:RadioButtonList>
                </div>
                <div style="float:left;width:300px;">
                     <label for="rdoSmoking"><strong>Smoking Habits</strong></label>
                    <asp:RadioButtonList ID="rdoSmoking" RepeatDirection="Horizontal" runat="server">
                        <asp:ListItem Value="Include" Selected="True"></asp:ListItem>
                        <asp:ListItem Value="Omit"></asp:ListItem>
                    </asp:RadioButtonList>
                </div>
            </div>
           
           <br />
            
            <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" OnClientClick="return ValidateSelectStudent();"/>
        </ContentTemplate>

       

    </asp:UpdatePanel>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" DynamicLayout="true" AssociatedUpdatePanelID="UpdatePanel1" DisplayAfter="500" >

       <ProgressTemplate>
          <img src="../../images/demo_wait.gif" />
           Please Wait...
       </ProgressTemplate>
   
    </asp:UpdateProgress>
    <br />
    
    
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
           <h3>Current Status</h3>
            <asp:Literal ID="litCurrStudentStatus" runat="server" Text=""></asp:Literal>
             <asp:GridView ID="GvStudentStatus" AutoGenerateColumns="False" 
                       Width="100%" 
                        cellpadding="5"
                        cellspacing="5"
                       runat="server">
                    <HeaderStyle BackColor="#448BC1" Font-Bold="False" ForeColor="White" />
                    <AlternatingRowStyle BackColor="AliceBlue"></AlternatingRowStyle>
                           <Columns>
                               <asp:BoundField DataField="homeName" HeaderText="homeName"  ItemStyle-Width="20%"  />
                               <asp:BoundField DataField="placementType" HeaderText="placementType"  ItemStyle-Width="30%" />
                               <asp:BoundField DataField="roomNumber" HeaderText="roomNumber" ItemStyle-Width="10%"/>
                               <asp:BoundField DataField="effectiveQuarter" HeaderText="effectiveQuarter" ItemStyle-Width="20%"/>
                               <asp:BoundField DataField="endQuarter" HeaderText="endQuarter" ItemStyle-Width="20%"/>
                        </Columns>
                    </asp:GridView>
           <h3>Families that Match</h3>
            <asp:Label ID="litRoomWithOtherFamily" runat="server" Text=""></asp:Label>
            <asp:Literal ID="litFoundFamily" runat=server></asp:Literal>   
            <br />
             <div style="margin-bottom:10px;width:40%;float:left;"> 
                  <asp:DropDownList ID="ddlFamilies" runat="server" Width="200px" >
               
                    </asp:DropDownList>
                 <asp:Label ID="litFamilyPlaced" runat="server" Text=""></asp:Label>
                 <asp:Button ID="btnCompare" runat="server" Text="Compare"  OnClientClick="return ValidateSelectFamily();" OnClick="btnCompare_Click"/>
             </div> 
           
             <div id="divRoomPlacement" runat="server" style="margin-bottom:10px;width:100%;float:left;">
                 <b>Family status: </b> <asp:Label ID="lblFamilyStatus" runat="server" Text="" />

                 <asp:GridView ID="GvAllRoom" AutoGenerateColumns="False" 
                       emptydatatext="N/A"  Width="100%" 
                        cellpadding="5"
                        cellspacing="5"
                       runat="server" >
                    <HeaderStyle BackColor="#448BC1" Font-Bold="true" ForeColor="White" />
                    <AlternatingRowStyle BackColor="AliceBlue"></AlternatingRowStyle>
                           <Columns>
                               <asp:BoundField DataField="room" HeaderText="Room"  ItemStyle-Width="10%" />
                               <asp:BoundField DataField="availableStarting" HeaderText="Available starting"  ItemStyle-Width="40%" />
                               <asp:BoundField DataField="occupancy" HeaderText="Occupancy" ItemStyle-Width="50%" HtmlEncode="False" />
                               
                        </Columns>
                    </asp:GridView>


                <asp:Table ID="roomStatus" runat="server" Width="90%" BorderWidth="0px">
            <asp:TableHeaderRow>
                <asp:TableCell Width="10%"><b>Room</b></asp:TableCell>
                <asp:TableCell Width="20%"><b>Available starting</b></asp:TableCell>
               <asp:TableCell Width="20%"><b>Occupancy</b></asp:TableCell>
            </asp:TableHeaderRow>
            <asp:TableRow>
                <asp:TableCell Width="10%">Room 1</asp:TableCell>
                <asp:TableCell ID="rm1Available" Width="20%"><asp:Literal ID="litRm1Available" runat="server"></asp:Literal></asp:TableCell>
                
                <asp:TableCell ID="rm1Occupancy" Width="20%"><asp:Literal ID="litRm1Occupancy" runat="server"></asp:Literal>
                    <asp:PlaceHolder runat="server" ID="place" /><br />
                </asp:TableCell>
                
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell Width="10%">Room 2</asp:TableCell>
                <asp:TableCell ID="rm2Available" Width="20%"><asp:Literal ID="litRm2Available" runat="server"></asp:Literal></asp:TableCell>

               <asp:TableCell ID="rm2Occupancy" Width="20%"><asp:Literal ID="litRm2Occupancy" runat="server"></asp:Literal></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell Width="10%">Room 3</asp:TableCell>
                <asp:TableCell ID="rm3Srm1Availabletatus" Width="20%"><asp:Literal ID="litRm3Available" runat="server"></asp:Literal></asp:TableCell>
                <asp:TableCell ID="rm3Occupancy" Width="20%"><asp:Literal ID="litRm3Occupancy" runat="server"></asp:Literal></asp:TableCell>
                
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell Width="10%">Room 4</asp:TableCell>
                <asp:TableCell ID="rm4Available" Width="20%"><asp:Literal ID="litRm4Available" runat="server"></asp:Literal></asp:TableCell>
                <asp:TableCell ID="rm4Occupancy" Width="20%"><asp:Literal ID="litRm4Occupancy" runat="server"></asp:Literal></asp:TableCell>
                
            </asp:TableRow>
        </asp:Table>
<!--
        <asp:GridView ID="GridView3" runat="server" AutoGenerateColumns="False" CellPadding="6" >  
            <Columns>  
                <asp:TemplateField>  
                    <ItemTemplate>  
                        <asp:Button ID="btn_Edit" runat="server" Text="Edit" CommandName="Edit" />  
                    </ItemTemplate>  
                    <EditItemTemplate>  
                        <asp:Button ID="btn_Update" runat="server" Text="Update" CommandName="Update"/>  
                        <asp:Button ID="btn_Cancel" runat="server" Text="Cancel" CommandName="Cancel"/>  
                    </EditItemTemplate>  
                </asp:TemplateField>  
                <asp:TemplateField HeaderText="ID">  
                    <ItemTemplate>  
                        <asp:Label ID="lbl_ID" runat="server" Text='<%#Eval("id") %>'></asp:Label>  
                    </ItemTemplate>  
                </asp:TemplateField>  
                <asp:TemplateField HeaderText="applicantID">  
                    <ItemTemplate>  
                        <asp:Label ID="lbl_applicantID" runat="server" Text='<%#Eval("applicantID") %>'></asp:Label>  
                    </ItemTemplate>  
                    <EditItemTemplate>  
                        <asp:TextBox ID="txt_applicantID" runat="server" Text='<%#Eval("applicantID") %>'></asp:TextBox>  
                    </EditItemTemplate>  
                </asp:TemplateField>  
                <asp:TemplateField HeaderText="effectiveQuarter">  
                    <ItemTemplate>  
                        <asp:Label ID="lbl_effectiveQuarter" runat="server" Text='<%#Eval("effectiveQuarter") %>'></asp:Label>  
                    </ItemTemplate>  
                    <EditItemTemplate>  
                        <asp:TextBox ID="txt_effectiveQuarter" runat="server" Text='<%#Eval("effectiveQuarter") %>'></asp:TextBox>  
                    </EditItemTemplate>  
                </asp:TemplateField>  
            </Columns>  
            <HeaderStyle BackColor="#663300" ForeColor="#ffffff"/>  
            <RowStyle BackColor="#e7ceb6"/>  
        </asp:GridView>  
    -->
            </div>
            <!--
            <p>
                
                <b>Notes:</b> <br />
                <i>If the student has been placed:
                    <ul>
                        <li>You can change "Placement" and "Effective Quarter" of the student.</li>
                        <li> You must withdraw student from the room before moving the student to another home/room.</li>
                    </ul>
                </i>
            </p>
            -->
            <asp:GridView ID="gvStuFam" runat="server" AutoGenerateColumns="False" CellPadding="6" 
                OnRowCancelingEdit="gvStuFam_RowCancelingEdit"   
                OnRowDatabound="gvStuFam_RowDataBound"
                OnRowEditing="gvStuFam_RowEditing" 
                OnRowUpdating="gvStuFam_RowUpdating">  
            <Columns>  
                <asp:TemplateField>  
                    <ItemTemplate>  
                        <asp:Button ID="btn_Edit" runat="server" Text="Edit" CommandName="Edit" />  
                    </ItemTemplate>  
                    <EditItemTemplate>  
                        <asp:Button ID="btn_Update" runat="server" Text="Update" CommandName="Update"/>  
                        <asp:Button ID="btn_Cancel" runat="server" Text="Cancel" CommandName="Cancel"/>  
                    </EditItemTemplate>  
                </asp:TemplateField>  
                <asp:TemplateField HeaderText="id">
                    <ItemTemplate>
                        <asp:Label ID="lbl_id" runat="server"  Text='<%#Eval("id") %>'></asp:Label>
                        
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="placementType">  
                    <ItemTemplate>  
                        <asp:Label ID="lbl_placementType" runat="server" Text='<%#Eval("placementType") %>'></asp:Label>  
                    </ItemTemplate>  
                    <EditItemTemplate>
			            <asp:DropDownList ID="ddlPlacementType" runat="server">
                            <asp:ListItem Value="">--Select a placement type--</asp:ListItem>
                            <asp:ListItem Value="Tentative Placement"></asp:ListItem>
                            <asp:ListItem Value="Student Placed"></asp:ListItem>
                            <asp:ListItem Value="Withdrew"></asp:ListItem>
                            <asp:ListItem Value="Graduated"></asp:ListItem>
                            <asp:ListItem Value="Student Moved Out"></asp:ListItem>
			            </asp:DropDownList>
		            </EditItemTemplate>  
                </asp:TemplateField> 
                <asp:TemplateField HeaderText="roomNumber">  
                    <ItemTemplate>  
                        <asp:Label ID="lbl_roomNumber" runat="server" Text='<%#Eval("roomNumber") %>'></asp:Label>  
                    </ItemTemplate>  
                    <EditItemTemplate>  
                        <asp:DropDownList ID="ddlRoomGv" runat="server">
                            <asp:ListItem Value="">--Select a room--</asp:ListItem>
                            <asp:ListItem Value="1">Room 1</asp:ListItem>
                            <asp:ListItem Value="2">Room 2</asp:ListItem>
                            <asp:ListItem Value="3">Room 3</asp:ListItem>
                            <asp:ListItem Value="4">Room 4</asp:ListItem>
			            </asp:DropDownList>
                    </EditItemTemplate>  
                </asp:TemplateField>  
                <asp:TemplateField HeaderText="effectiveQuarter">  
                    <ItemTemplate>  
                        <asp:Label ID="lbl_effectiveQuarter" runat="server" Text='<%#Eval("effectiveQuarter") %>'></asp:Label>  
                    </ItemTemplate>  
                    <EditItemTemplate>  
                        <asp:DropDownList ID="ddlEffectiveQuarter" runat="server">

                        </asp:DropDownList>
                    </EditItemTemplate>  
                </asp:TemplateField>  
                <asp:TemplateField HeaderText="endQuarter">  
                    <ItemTemplate>  
                        <asp:Label ID="lbl_endQuarter" runat="server" Text='<%#Eval("endQuarter") %>'></asp:Label>  
                    </ItemTemplate>  
                    <EditItemTemplate>  
                        <asp:DropDownList ID="ddlEndQuarter" runat="server">

                        </asp:DropDownList>
                    </EditItemTemplate>  
                </asp:TemplateField>  
            </Columns>  
            <HeaderStyle BackColor="#663300" ForeColor="#ffffff"/>  
            <RowStyle BackColor="#e7ceb6"/>  
        </asp:GridView>  
            <div id="insertDiv" runat="server" Visible="false">

            <h4>Place student</h4>
            <i>You can place a student here:</i><br/>
                
            
            <div style="width:200px;float:left;">
                 <b>Room *</b> <br />
                <asp:DropDownList ID="ddlRoom" runat="server">
                   
                  </asp:DropDownList>
            </div>
             <div style="width:300px;float:left;">
                 <b>Effective Quarter *</b> <br />
                <asp:DropDownList ID="ddlEffectiveQtr" runat="server">
                        
                  </asp:DropDownList>
            </div>
            <div style="width:300px;float:left;">
                 <b>End Quarter *</b> <br />
                <asp:DropDownList ID="ddlEndQtr" runat="server">
                        
                  </asp:DropDownList>
            </div>
                <div style="width:300px;float:left;">

                 <b>Placement Type</b> <br />
                <asp:DropDownList ID="ddlRmPlacement" runat="server">
                        <asp:ListItem Value="">--Select a placement type--</asp:ListItem>
                        <asp:ListItem Value="Tentative Placement"></asp:ListItem>
                        <asp:ListItem Value="Student Placed"></asp:ListItem>
                  </asp:DropDownList>
            </div>
            <br />
             <div style="width:20%">
                <asp:Button ID="btnPlace" runat="server" Text="Place" OnClick="btnPlace_Click" OnClientClick="return ValidatePlacement();"/> <br />
                 <asp:Label ID="lbMsg" runat="server" ForeColor="Red"></asp:Label><br /> <br />
                  <asp:Label ID="litPlace" runat="server" Text=""></asp:Label>
                 
                  
            </div>

          </div> <!-- End insertDiv -->
           <br />
            <div style="width:100%;">
                <div style="width:70%;float:left;margin-right:10px">
                    <h4>Comparison</h4>
                    <asp:GridView ID="GridView1" AutoGenerateColumns="False" 
                       emptydatatext="N/A"  Width="100%" 
                        cellpadding="5"
                        cellspacing="5"
                       runat="server">
                    <HeaderStyle BackColor="#448BC1" Font-Bold="False" ForeColor="White" />
                    <AlternatingRowStyle BackColor="AliceBlue"></AlternatingRowStyle>
                           <Columns>
                               <asp:BoundField DataField="DisplayText" HeaderText="Field"  ItemStyle-Width="20%" ItemStyle-BackColor="#CCCCCC" />
                               <asp:BoundField DataField="StudentValue" HeaderText="Student"  ItemStyle-Width="40%" />
                               <asp:BoundField DataField="FamilyValue" HeaderText="Family" ItemStyle-Width="40%"/>
                               
                        </Columns>
                    </asp:GridView>
                </div>
                <div style="width:20%;float:left">
                    <h4>Matching score</h4>
                    <asp:GridView ID="GridView2" AutoGenerateColumns="False" 
                               emptydatatext="N/A"  Width="95%" 
                                cellpadding="5"
                                cellspacing="5"
                               runat="server">
                            <HeaderStyle BackColor="#448BC1" Font-Bold="False" ForeColor="White" />
                            <AlternatingRowStyle BackColor="AliceBlue"></AlternatingRowStyle>
                                   <Columns>
                                       <asp:BoundField DataField="Field" HeaderText="Field"  ItemStyle-Width="80%" ItemStyle-BackColor="#CCCCCC" />
                                       <asp:BoundField DataField="ScoreMatch" HeaderText="Percent"   />
                               
                               
                                </Columns>
                  </asp:GridView>
                </div>
            </div>
          
            
            
            
            <table style="width: 80%;">
                <tr>
                    <td style=" vertical-align:top">
                        <asp:Literal ID="litSelectedStuInfo" runat=server></asp:Literal>  
                    </td>
                    <td style="vertical-align:top">
                        <asp:Literal ID="litFamilyInfo" runat=server></asp:Literal>  
                    </td>
                </tr>
                
                
            </table>
            <asp:Literal ID="litResultMsg" runat=server></asp:Literal>   
        </ContentTemplate>

    </asp:UpdatePanel>

    

    
    <script type="text/javascript">
        function ValidateSelectStudent() {
            var ddl = $('#<%=ddlStudents.ClientID%>');
            var selectedValue = ddl.val();
  
            //alert(selectedValue); //SelVal is the selected Value
            if (selectedValue == 0) {
                 alert("Please select a student to search");
                return false;
            }
            return true;
        }

        function ValidateSelectFamily() {
            var ddl = $('#<%=ddlFamilies.ClientID%>');
            var selectedValue = ddl.val();
  
            //alert(selectedValue); //SelVal is the selected Value
            if (selectedValue == 0) {
                 alert("Please select a family to compare");
                return false;
            }
            return true;
        }

        function ValidatePlacement() {
            var ddlRoom = $('#<%=ddlRoom.ClientID%>');
            var ddlEffectiveQtr = $('#<%=ddlEffectiveQtr.ClientID%>');
            var ddlEndQtr = $('#<%=ddlEndQtr.ClientID%>');
           
            if (ddlRoom.val() == "") {
                alert("Please select a room to place");
                ddlRoom.focus();
                return false;
            }
            if (ddlEffectiveQtr.val() == "") {
                alert("Please select an effective quarter");
                ddlEffectiveQtr.focus();
                return false;
            }
            if (ddlEndQtr.val() == "") {
                alert("Please select an end quarter");
                ddlEndQtr.focus();
                return false;
            }
            return true;
         }

        
        $(function () {
            //alert("hello");
                //Get data as JSON type from REST API
            /*
            $.ajax({
                type: "POST",
                url: "MatchingWS.asmx/GetDemo",
                contentType: "application/json",
                data: {applicantID : "abc"},
                //dataType: "json"
            })
            .done(function (data) {
                //alert("success");

                console.log(data);
                //displayTable(data);
            })
            .fail(function (xhr) {
                console.log('error', xhr);
            });
            */
        });
        
        
    </script>
    </asp:Content>