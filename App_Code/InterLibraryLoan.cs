﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.DirectoryServices;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for InterLibraryLoan
/// </summary>
public class InterLibraryLoan
{
	public InterLibraryLoan()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public string[] GetStudentInfo(string strSID, string strColCode)
    {
        //Can use as lookup for student name or retrieve all data set to modular variables
        //Currently returns a one-dimensional array with 10 elements:
        //0 = Last Name
        //1 = First Name
        //2 = Middle Initial
        //3 = Name as it appears in the student records
        //4 = Email address
        //5 = Phone Number
        //6 = Address 
        //7 = City
        //8 = State
        //9 = ZIP Code
        string[] strReturnArray = new string[10];
        string strFullName = "";
        string strFName = "";
        string strLName = "";
        string strMInitial = "";
        string strEmail = "";
        string strPhone1 = "";
        string strAddr1 = "";
        string strCity = "";
        string strState = "";
        string strZIP = "";

        int intUpBound = 0;
        int i = 0;
        string strTemp1 = "";
        string strTemp2 = "";
        string strSQL = "usp_GetOneStu_D '" + strSID + "', '" + strColCode + "'";
        string strConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CcsSm"].ToString();
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCommand = new SqlCommand();
        SqlDataReader objReader;

        objConn.ConnectionString = strConnectionString;
        objConn.Open();
        objCommand.CommandText = strSQL;
        objCommand.CommandType = CommandType.Text;
        objCommand.Connection = objConn;
        objReader = objCommand.ExecuteReader();
        if (objReader.HasRows)
        {
            while (objReader.Read())
            {
                strFullName = objReader["STU_NAME"].ToString();
                strEmail = objReader["StuEmailAddr"].ToString();
                strPhone1 = "(" + objReader["DAY_AREA_CODE"].ToString() + ") " + objReader["DAY_PREFIX"].ToString() + "-" + objReader["DAY_SUFFIX"].ToString();
                strAddr1 = objReader["STU_STREET"].ToString();
                strCity = objReader["STU_CITY"].ToString();
                strState = objReader["STU_ST"].ToString();
                strZIP = objReader["STU_ZIP"].ToString();
            }
        }
        objConn.Close();
        objConn.Dispose();
        objReader.Dispose();
        objCommand.Dispose();
        string[] strArray = strFullName.Split(new Char[] { ' ' });
        intUpBound = strArray.GetUpperBound(0);
        for (i = 0; i < intUpBound; i++)
        {
            //strResultMsg =+ "i: " + i + "<br />";
            switch (i)
            {
                case 0:
                    strLName = strArray[0];
                    strTemp2 = strLName.Substring(1, strLName.Length - 1).ToLower();
                    strTemp1 = strLName.Substring(0, 1);
                    strReturnArray[0] = strTemp1 + strTemp2;
                    break;
                case 1:
                    strFName = strArray[1];
                    strTemp2 = strFName.Substring(1, strFName.Length - 1).ToLower();
                    strTemp1 = strFName.Substring(0, 1);
                    strReturnArray[1] = strTemp1 + strTemp2;
                    break;
                case 2:
                    strMInitial = strArray[2];
                    strReturnArray[2] = strMInitial;
                    break;
            }
        }
        strReturnArray[3] = strFullName;
        strReturnArray[4] = strEmail;
        strReturnArray[5] = strPhone1;
        strReturnArray[6] = strAddr1;
        strReturnArray[7] = strCity;
        strReturnArray[8] = strState;
        strReturnArray[9] = strZIP;

        return strReturnArray;
    }

    public string[] GetActiveDirectoryInfo(string strADUser, string strDomain)
    {
        //Returns a String Array with 8 elements:
        //0 = LAST NAME: sn
        //1 = FIRST NAME: givenName
        //2 = FULL NAME: cn
        //3 = EMAIL ADDRESS: mail 
        //4 = PHONE: telephoneNumber (e.g. (509) 533-3828)
        //5 = BUILDING/ROOM: physicalDeliveryOfficeName (e.g. 24-309)
        //6 = MAILSTOP:  extensionAttribute4 (e.g. 3240)
        //7 = SID: employeeID (e.g. 820172628)
        //8 = STREET ADDRESS:  streetAddress (e.g. N. 2000 Greene St.)
        //9 = CITY: l (that's the lower case L)(e.g. Spokane)
        //10 = STATE: st (e.g. WA)
        //11 = POSTAL CODE:  postalCode (e.g. 99217)
        //12 = INSTITUTION:  company (e.g. Spokane Falls Community College)
        string[] strReturnArray = new string[13];
        string strFullName = "";
        string strFName = "";
        string strLName = "";
        string strEmail = "";
        string strPhone = "";
        string strBldgRoom = "";
        string strMailStop = "";
        string strSID = "";
        string strStreet = "";
        string strCity = "";
        string strState = "";
        string strZIP = "";
        string strInstitution = "";
        string strDirectoryEntry = "";
        //string strADUser = "";
        //string strDomain = "";
        //Int32 intLength = 0;
        //Int32 intIndex = 0;
        //Logins may or may not have @ or \ symbol in them from IIS; from forms authentication, user name only
        //if (strLogonUser.IndexOf("@") <= 0)
        //{
        //    if (strLogonUser.IndexOf("\\") <= 0)
        //    {
        //        strADUser = strLogonUser;
        //    }
        //    else
        //    {
        //        intLength = strLogonUser.Length;
        //        intIndex = strLogonUser.IndexOf("\\");
        //        strDomain = strLogonUser.Substring(0, intIndex).ToLower();
        //        strADUser = strLogonUser.Substring(intIndex + 1, ((intLength - intIndex) - 1)).ToLower();
        //    }
        //}
        //else
        //{
        //    intLength = strLogonUser.Length;
        //    intIndex = strLogonUser.IndexOf("@");
        //    strADUser = strLogonUser.Substring(0, intIndex).ToLower();
        //    strDomain = strLogonUser.Substring(intIndex + 1, ((intLength - intIndex) - 1)).ToLower();
        //}

        //strDomain = "bigfoot.spokane.edu";
        switch (strDomain.ToLower())
        {
            case "student":
            case "bigfoot.spokane.edu":
                strDirectoryEntry = "LDAP://OU=Student Accounts,DC=student,DC=spokane,DC=cc,DC=wa,DC=us";
                break;
            case "ccs":
                strDirectoryEntry = "LDAP://DC=ccs,DC=spokane,DC=cc,DC=wa,DC=us";
                break;
            case "dist":
                strDirectoryEntry = "LDAP://DC=dist,DC=spokane,DC=cc,DC=wa,DC=us";
                break;
            case "iel":
                strDirectoryEntry = "LDAP://DC=iel,DC=spokane,DC=cc,DC=wa,DC=us";
                break;
            case "scc":
                strDirectoryEntry = "LDAP://DC=scc,DC=spokane,DC=cc,DC=wa,DC=us";
                break;
            case "sfcc":
                strDirectoryEntry = "LDAP://OU=Staff and Faculty Users,DC=sfcc,DC=spokane,DC=cc,DC=wa,DC=us";
                break;
            default:
                strDirectoryEntry = "GC://DC=spokane,DC=cc,DC=wa,DC=us";
                break;
        }
        try
        {
            //strADUser = "AAROND1837";
            DirectoryEntry de = new DirectoryEntry(strDirectoryEntry);
            DirectorySearcher deSearch = new DirectorySearcher(de);
            deSearch.Filter = "(&(sAMAccountName=" + strADUser + "))"; // search filter
            deSearch.SearchScope = SearchScope.Subtree;
            deSearch.PropertiesToLoad.Add("sn"); // last name
            deSearch.PropertiesToLoad.Add("givenName"); // first name
            deSearch.PropertiesToLoad.Add("cn"); // full name
            deSearch.PropertiesToLoad.Add("mail"); // email address
            deSearch.PropertiesToLoad.Add("telephoneNumber"); // Phone
            deSearch.PropertiesToLoad.Add("company"); // Institution
            if ((strDomain.ToLower() != "bigfoot.spokane.edu") && (strDomain.ToLower() != "student"))
            {
                deSearch.PropertiesToLoad.Add("physicalDeliveryOfficeName"); // Building/Room
                deSearch.PropertiesToLoad.Add("extensionAttribute4"); // Mail Stop
                deSearch.PropertiesToLoad.Add("streetAddress"); // Street
                deSearch.PropertiesToLoad.Add("l"); // City
                deSearch.PropertiesToLoad.Add("st"); // State
                deSearch.PropertiesToLoad.Add("postalCode"); // ZIP
            }
            deSearch.PropertiesToLoad.Add("employeeID"); // SID
            //Do the search
            SearchResult objResult = deSearch.FindOne();
            DirectoryEntry dirEntry = objResult.GetDirectoryEntry();
            //Fetch the properties
            strFullName = dirEntry.Properties["cn"].Value.ToString();
            strFName = dirEntry.Properties["givenName"].Value.ToString();
            strLName = dirEntry.Properties["sn"].Value.ToString();
            strEmail = dirEntry.Properties["mail"].Value.ToString();
            if ((strDomain != "bigfoot.spokane.edu") && (strDomain.ToLower() != "student"))
            {
                strPhone = dirEntry.Properties["telephoneNumber"].Value.ToString();
                strBldgRoom = dirEntry.Properties["physicalDeliveryOfficeName"].Value.ToString();
                strMailStop = dirEntry.Properties["extensionAttribute4"].Value.ToString();
                strStreet = dirEntry.Properties["streetAddress"].Value.ToString();
                strCity = dirEntry.Properties["l"].Value.ToString();
                strState = dirEntry.Properties["st"].Value.ToString();
                strZIP = dirEntry.Properties["postalCode"].Value.ToString();
            }
            strSID = dirEntry.Properties["employeeID"].Value.ToString();
            strInstitution = dirEntry.Properties["company"].Value.ToString();
            //Load the array
            strReturnArray[0] = strFullName;
            strReturnArray[1] = strFName;
            strReturnArray[2] = strLName;
            strReturnArray[3] = strEmail;
            strReturnArray[4] = strPhone;
            strReturnArray[5] = strBldgRoom;
            strReturnArray[6] = strMailStop;
            strReturnArray[7] = strSID;
            strReturnArray[8] = strStreet;
            strReturnArray[9] = strCity;
            strReturnArray[10] = strState;
            strReturnArray[11] = strZIP;
            strReturnArray[12] = strInstitution;
        }
        catch
        {
            //do nothing
        }
        return strReturnArray;
    }
}