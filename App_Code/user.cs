using System;
using System.Data;
using System.DirectoryServices;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Collections;

/// <summary>
/// Summary description for user
/// </summary>
public class user {

	public user(){
	}

    public string college() {
        string college = "";
        string domainuser = HttpContext.Current.User.Identity.Name;
        string[] temp = domainuser.Split("\\".ToCharArray());
        string searchdomain = temp[0];
        //if (searchdomain.ToLower() == "dist") college = "170";
        if (searchdomain.ToLower() == "scc") college = "171";
        if (searchdomain.ToLower() == "sfcc") college = "172";
        if (searchdomain.ToLower() == "iel") college = "172";
        return college;
    }

    public DirectoryEntry aduser(){
            string domainuser = HttpContext.Current.User.Identity.Name;
            string[] temp = domainuser.Split("\\".ToCharArray());
            string searchuser = temp[1];
            string searchdomain = temp[0];
            if (searchdomain.ToLower() == "dist") searchdomain = "dist.spokane.cc.wa.us";
            if (searchdomain.ToLower() == "scc") searchdomain = "scc.spokane.cc.wa.us";
            if (searchdomain.ToLower() == "sfcc") searchdomain = "sfcc.spokane.cc.wa.us";
            if (searchdomain.ToLower() == "iel") searchdomain = "iel.spokane.cc.wa.us";
            DirectoryEntry entry = new DirectoryEntry("LDAP://" + searchdomain);
            DirectorySearcher mySearcher = new DirectorySearcher(entry);
            string filter = string.Format("(anr= {0})", searchuser);
            mySearcher.Filter = (filter);
            SearchResult result = mySearcher.FindOne();
            DirectoryEntry directoryEntry = result.GetDirectoryEntry();
            mySearcher.Dispose();
            mySearcher = null;
            return directoryEntry;
    }

    /* New method added 1/5/14 */
    public ArrayList adUserInfo(string domainuser)
    {
        string searchuser;
        string searchdomain;
        string updomain;
        if (domainuser.Contains("@"))
        {
            string[] temp = domainuser.Split("@".ToCharArray());
            searchuser = temp[0];
            searchdomain = temp[1];
            updomain = searchdomain;
            if (searchdomain.ToLower() == "ccs") updomain = "ccs.spokane.edu";
            if (searchdomain.ToLower().Equals("scc.spokane.edu")) updomain = "scc.spokane.edu";
            if (searchdomain.ToLower().Equals("sfcc.spokane.edu")) updomain = "sfcc.spokane.edu";
        }
        else
        {
            string[] temp = domainuser.Split("\\".ToCharArray());
            searchuser = temp[1];
            searchdomain = temp[0];
            updomain = searchdomain;
            if (searchdomain.ToLower() == "district17_adm") updomain = "dist";
            if (searchdomain.ToLower() == "ccs") updomain = "ccs.spokane.edu";
        }
        if (searchdomain.ToLower() == "district17_adm" || searchdomain.ToLower() == "dist") searchdomain = "dist.spokane.cc.wa.us";
        if (searchdomain.ToLower() == "scc") searchdomain = "scc.spokane.cc.wa.us";
        if (searchdomain.ToLower() == "sfcc") searchdomain = "sfcc.spokane.cc.wa.us";
        if (searchdomain.ToLower() == "iel") searchdomain = "iel.spokane.cc.wa.us";
        if (searchdomain.ToLower() == "ccs.spokane.edu" || searchdomain.ToLower() == "scc.spokane.edu" || searchdomain.ToLower() == "sfcc.spokane.edu" || searchdomain.ToLower() == "ccs") searchdomain = "ccs.spokane.cc.wa.us";

        DirectoryEntry entry = new DirectoryEntry("LDAP://" + searchdomain);
        DirectorySearcher mySearcher = new DirectorySearcher(entry);
        string filter = string.Format("(userPrincipalName= {0})", searchuser + "@" + updomain);
        mySearcher.Filter = (filter);
        SearchResult result = mySearcher.FindOne();
        if ((result == null) && (updomain == "ccs.spokane.edu"))
        {
            updomain = updomain.Replace(".spokane.edu", "");
            filter = "(&((&(objectCategory=Person)(objectClass=User)))(samaccountname=" + searchuser + "))";
            mySearcher.Filter = (filter);
            result = mySearcher.FindOne();
        }
        DirectoryEntry directoryEntry = result.GetDirectoryEntry();
        string sid = directoryEntry.Properties["employeeID"][0].ToString();
        string phone = directoryEntry.Properties["telephoneNumber"][0].ToString();
        string mail = directoryEntry.Properties["mail"][0].ToString();
        string name = directoryEntry.Properties["displayName"][0].ToString();
        directoryEntry.Dispose();
        directoryEntry = null;
        mySearcher.Dispose();
        mySearcher = null;
        ArrayList mylist = new ArrayList();
        mylist.Add(sid);
        mylist.Add(phone);
        mylist.Add(mail);
        mylist.Add(name);
        return mylist;
    }

    /* New method added 1/5/14 */
    private bool sidExists(string domainuser)
    {
        try
        {
            string searchuser;
            string searchdomain;
            string updomain;
            if (domainuser.Contains("@"))
            {
                string[] temp = domainuser.Split("@".ToCharArray());
                searchuser = temp[0];
                searchdomain = temp[1];
                updomain = searchdomain;
                if (searchdomain.ToLower() == "ccs") updomain = "ccs.spokane.edu";
                if (searchdomain.ToLower().Equals("scc.spokane.edu")) updomain = "scc.spokane.edu";
                if (searchdomain.ToLower().Equals("sfcc.spokane.edu")) updomain = "sfcc.spokane.edu";
            }
            else
            {
                string[] temp = domainuser.Split("\\".ToCharArray());
                searchuser = temp[1];
                searchdomain = temp[0];
                updomain = searchdomain;
                if (searchdomain.ToLower() == "district17_adm") updomain = "dist";
                if (searchdomain.ToLower() == "ccs") updomain = "ccs.spokane.edu";
            }
            if (searchdomain.ToLower() == "district17_adm" || searchdomain.ToLower() == "dist") searchdomain = "dist.spokane.cc.wa.us";
            if (searchdomain.ToLower() == "scc") searchdomain = "scc.spokane.cc.wa.us";
            if (searchdomain.ToLower() == "sfcc") searchdomain = "sfcc.spokane.cc.wa.us";
            if (searchdomain.ToLower() == "iel") searchdomain = "iel.spokane.cc.wa.us";
            if (searchdomain.ToLower() == "ccs.spokane.edu" || searchdomain.ToLower() == "scc.spokane.edu" || searchdomain.ToLower() == "sfcc.spokane.edu" || searchdomain.ToLower() == "ccs") searchdomain = "ccs.spokane.cc.wa.us";

            DirectoryEntry entry = new DirectoryEntry("LDAP://" + searchdomain);
            DirectorySearcher mySearcher = new DirectorySearcher(entry);
            string filter = string.Format("(userPrincipalName= {0})", searchuser + "@" + updomain);
            mySearcher.Filter = (filter);
            SearchResult result = mySearcher.FindOne();
            if ((result == null) && (updomain == "ccs.spokane.edu"))
            {
                updomain = updomain.Replace(".spokane.edu", "");
                filter = "(&((&(objectCategory=Person)(objectClass=User)))(samaccountname=" + searchuser + "))";
                mySearcher.Filter = (filter);
                result = mySearcher.FindOne();
            }
            DirectoryEntry directoryEntry = result.GetDirectoryEntry();
            string sid = directoryEntry.Properties["employeeID"][0].ToString();
            directoryEntry.Dispose();
            directoryEntry = null;
            mySearcher.Dispose();
            mySearcher = null;
            if (sid.Substring(0, 2) == "82")
                return true;
            else
                return false;
        }
        catch
        {
            return false;
        }
    }

    private bool hasAccess(string domainuser)
    {
        /*change to use ad security groups
        XmlDocument xmlDoc = new XmlDocument();
        xmlDoc.Load(userfile);
        XmlNodeList nodeList = xmlDoc.SelectNodes("users/user[@login='" + domainuser.ToLower() + "']");
        try
        {
            if (nodeList.Count == 0)
                return false;
            else
                return true;
        }
        catch (Exception e)
        {
            return false;
        }
         */

        return true;
    }

    public bool isValid()
    {
        string domainuser = HttpContext.Current.User.Identity.Name;
        if (this.sidExists(domainuser))
            if (this.hasAccess(domainuser))
                return true;
            else
                return false;
        else
            return false;
    }

    /* old method
    public ArrayList adUserInfo(string domainuser)
    {
        string[] temp = domainuser.Split("\\".ToCharArray());
        string searchuser = temp[1];
        string searchdomain = temp[0];
        if (searchdomain.ToLower() == "dist") searchdomain = "dist.spokane.cc.wa.us";
        if (searchdomain.ToLower() == "scc") searchdomain = "scc.spokane.cc.wa.us";
        if (searchdomain.ToLower() == "sfcc") searchdomain = "sfcc.spokane.cc.wa.us";
        if (searchdomain.ToLower() == "iel") searchdomain = "iel.spokane.cc.wa.us";

        DirectoryEntry entry = new DirectoryEntry("LDAP://" + searchdomain);
        DirectorySearcher mySearcher = new DirectorySearcher(entry);
        string filter = string.Format("(anr= {0})", searchuser);
        mySearcher.Filter = (filter);
        SearchResult result = mySearcher.FindOne();
        DirectoryEntry directoryEntry = result.GetDirectoryEntry();
        string sid = directoryEntry.Properties["employeeID"][0].ToString();
        string phone = directoryEntry.Properties["telephoneNumber"][0].ToString();
        string mail = directoryEntry.Properties["mail"][0].ToString();
        directoryEntry.Dispose();
        directoryEntry = null;
        mySearcher.Dispose();
        mySearcher = null;
        ArrayList mylist = new ArrayList();
        mylist.Add(sid);
        mylist.Add(phone);
        mylist.Add(mail);
        return mylist;
    }
     */

    /* old method
    private bool sidExists(string domainuser)
    {
        try
        {
            string[] temp = domainuser.Split("\\".ToCharArray());
            string searchuser = temp[1];
            string searchdomain = temp[0];
            //			searchdomain = "campus.spokane.cc.wa.us";
            if (searchdomain.ToLower() == "dist") searchdomain = "dist.spokane.cc.wa.us";
            if (searchdomain.ToLower() == "scc") searchdomain = "scc.spokane.cc.wa.us";
            if (searchdomain.ToLower() == "sfcc") searchdomain = "sfcc.spokane.cc.wa.us";
            if (searchdomain.ToLower() == "iel") searchdomain = "iel.spokane.cc.wa.us";
            DirectoryEntry entry = new DirectoryEntry("LDAP://" + searchdomain);
            DirectorySearcher mySearcher = new DirectorySearcher(entry);
            string filter = string.Format("(anr= {0})", searchuser);
            mySearcher.Filter = (filter);
            SearchResult result = mySearcher.FindOne();
            DirectoryEntry directoryEntry = result.GetDirectoryEntry();
            string sid = directoryEntry.Properties["employeeID"][0].ToString();
            directoryEntry.Dispose();
            directoryEntry = null;
            mySearcher.Dispose();
            mySearcher = null;
            if (sid.Substring(0, 2) == "82")
                return true;
            else
                return false;
        }
        catch
        {
            return false;
        }
    }
    */

}
