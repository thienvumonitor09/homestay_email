using System;
using System.Data;
using System.Collections;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.DirectoryServices;
using System.Text;

/// <summary>
/// Summary description for EmployeeActiveDirectoryInfo
/// </summary>
public class EmployeeActiveDirectoryInfo
{
	public string strADLogin;
	public string strDepartment;
	public string strDivision;
	public string strFullName;
    public string strCompany;
	public string strFirstName;
	public string strLastName;
	public string strPhoneNumber;
	public string strLoginSID;
	public string strUserType;
	public string strEmail;
	public string strTitle;
	public string strPhysicalDeliveryOfficeName;
    public string strStreet;
    public string strMailStop;
    public string strCity;
    public string strState;
    public string strZIP;
	public string [] memberList;
	public int memberCount;
	private DirectoryEntry entry;
	private DirectorySearcher Dsearch;
	private SearchResultCollection results;
	private string ADLookupType;
	public Boolean boolIsStudent;
	public string strPwdLastSet;
	public string strAccountExpires;
	public string strAccountExpirationStatus;
    private string strConnectionString = "";

	public EmployeeActiveDirectoryInfo(String inputString, String LookupType, String strInstitution)
	{

        if (strInstitution == "Student")
        {
            strConnectionString = ConfigurationManager.ConnectionStrings["LDAP_Bigfoot"].ConnectionString;

        }
        else if (strInstitution == "CCS")
        {
            strConnectionString = ConfigurationManager.ConnectionStrings["LDAP_CCS"].ConnectionString;
        }
        else
        {
            strConnectionString = ConfigurationManager.ConnectionStrings["GC_CCS"].ConnectionString;

        }
        // Setting Resources
		if (LookupType == "ADNAME")
		{
			strADLogin = inputString;
			ADLookupType = "ADNAME";
			if (strADLogin.IndexOfAny(new char[] { '\\' }) > 0)
			{
				strADLogin = strADLogin.Substring(strADLogin.LastIndexOf("\\") + 1);
			}
			LDAPGetUserInfo(ADLookupType);
		}
        if (LookupType == "SID"){
            strLoginSID = inputString;
    		ADLookupType = "SID";
	    	LDAPGetUserInfo(ADLookupType);
        } 
        if (LookupType == "CN"){
            strFullName = inputString;
    		ADLookupType = "CN";
	    	LDAPGetUserInfo(ADLookupType);
        }
    }
	// Destructor
	~EmployeeActiveDirectoryInfo()
	{
		// Resource Clean Up
		entry.Close();
		entry.Dispose();
		Dsearch.Dispose();
	}
	// *****************************************
	//  BEGIN ACTIVE DIRECTORY LOOKUP CODE
	// *****************************************
	//  GET SID from Active Directory
    /// <summary>
    /// 
    /// </summary>
    /// <param name="LookupType"></param>
	private void LDAPGetUserInfo(String LookupType)
	{   // This will find Staff and Faculty Accounts
		entry = new DirectoryEntry(strConnectionString);
		Dsearch = new DirectorySearcher(entry);
        switch (LookupType)
        {
            case "ADNAME":
                Dsearch.Filter = "(sAMAccountName=" + strADLogin + ")";
                break;
            case "SID":
    			Dsearch.Filter = "(employeeID=" + strLoginSID + ")";
                break;
            case "CN":
    			Dsearch.Filter = "(cn=" + strFullName + ")";
            break;
        }
/*		if (LookupType == "ADNAME")
		{
			Dsearch.Filter = "(SAMAccountName=" + strADLogin + ")";
		}
		else 
		{
			Dsearch.Filter = "(employeeID=" + strLoginSID + ")";
		}*/
		Dsearch.SearchScope = SearchScope.Subtree;
		Dsearch.PropertiesToLoad.Add("sAMAccountName");
		Dsearch.PropertiesToLoad.Add("cn");
		Dsearch.PropertiesToLoad.Add("title");
		Dsearch.PropertiesToLoad.Add("physicalDeliveryOfficeName");
		Dsearch.PropertiesToLoad.Add("employeeID");
        Dsearch.PropertiesToLoad.Add("givenname");
        Dsearch.PropertiesToLoad.Add("company");
		Dsearch.PropertiesToLoad.Add("sn");
		Dsearch.PropertiesToLoad.Add("mail");
		Dsearch.PropertiesToLoad.Add("telephonenumber");
		Dsearch.PropertiesToLoad.Add("department");
		Dsearch.PropertiesToLoad.Add("description");
		Dsearch.PropertiesToLoad.Add("memberof");
		Dsearch.PropertiesToLoad.Add("pwdlastset");
		Dsearch.PropertiesToLoad.Add("accountexpires");
		Dsearch.PropertiesToLoad.Add("accountexpirationstatus");
        Dsearch.PropertiesToLoad.Add("extensionAttribute4"); // Mail Stop
        Dsearch.PropertiesToLoad.Add("streetAddress"); // Street
        Dsearch.PropertiesToLoad.Add("l"); // City
        Dsearch.PropertiesToLoad.Add("st"); // State
        Dsearch.PropertiesToLoad.Add("postalCode"); // ZIP

		try
		{
		    results = Dsearch.FindAll();
		    // 2B. Pull in results
		    if (results != null && results.Count > 0)
		    {
				foreach (SearchResult result in results)
				{
					//string searchpath = result.Path;
					ResultPropertyCollection rpc = result.Properties;
					foreach (string property in rpc.PropertyNames)
					{
						foreach (object value in rpc[property])
						{
							switch (property)
							{
								case "samaccountname":
									strADLogin = value.ToString();
									break;
								case "employeeid":
									strLoginSID = value.ToString();
									break;
								case "givenname":
									strFirstName = value.ToString();
									break;
								case "sn":
									strLastName = value.ToString();
									break;
								case "cn":
									strFullName = value.ToString();
                                    break;
                                case "company":
                                    strCompany = value.ToString();
                                    break;
								case "title":
									strTitle = value.ToString();
									break;
								case "physicalDeliveryOfficeName":
									strPhysicalDeliveryOfficeName = value.ToString();
									break;
								case "mail":
									strEmail = value.ToString();
									break;
								case "department":
									strDepartment = value.ToString();
									if (value.ToString() == "Student")
									{
										strUserType = value.ToString();
									}
									else
									{
										strUserType = "Staff";
									}
									break;
								case "description":
									strDivision = value.ToString();
									break;
								case "pwdlastset":
									strPwdLastSet = value.ToString();
									break;
								case "accountexpires":
									strAccountExpires = value.ToString();
									break;
								case "accountexpirationstatus":
									strAccountExpirationStatus = value.ToString();
									break;
								case "telephonenumber":
									strPhoneNumber = value.ToString();
									//strPhoneNumber = strPhoneNumber.Replace("(", "");
									//strPhoneNumber = strPhoneNumber.Replace(")", "");
									//strPhoneNumber = strPhoneNumber.Replace("-", "");
									//strPhoneNumber = strPhoneNumber.Replace(" ", "");
									break;
                                case "extensionattribute4":
                                    strMailStop = value.ToString();
                                    break;
                                case "streetaddress":
                                    strStreet = value.ToString();
                                    break;
                                case "l":
                                    strCity = value.ToString();
                                    break;
                                case "st":
                                    strState = value.ToString();
                                    break;
                                case "postalcode":
                                    strZIP = value.ToString();
                                    break;
                                case "memberof":
									if (result != null)
									{
										String memberHolder = "";
										memberCount = result.Properties["memberof"].Count;
										for (int counter = 0; counter < memberCount; counter++)
										{
											memberHolder = memberHolder + ((string)result.Properties["memberof"][counter]) + "|" ;
										}
										memberList = memberHolder.Split('|');
									}
									break;
							}
						}
					}
				}
			}
		}
		catch
		{
			strLoginSID = "NOT FOUND";
		}
		//catch (Exception ex)
		//{
		//	strLoginSID = ex.Message;
		//}
		finally
		{
			Dsearch.Dispose();
			entry.Close();
			entry.Dispose();
		}
	}

	// *****************************************
	//  END ACTIVE DIRECTORY LOOKUP CODE
	// *****************************************

}
