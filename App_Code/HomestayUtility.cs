﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for HomestayUtility
/// </summary>
public class HomestayUtility
{
    Homestay hsInfo = new Homestay();
    public HomestayUtility()
    {
        
    }

    public void ExportPdf()
    {
        try
        {
            Document pdfDoc = new Document(PageSize.A4, 25, 10, 25, 10);
            PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDoc, HttpContext.Current.Response.OutputStream);
            pdfDoc.Open();
            pdfWriter.CloseStream = false;
            Paragraph Text = new Paragraph("Family Profile");
            pdfDoc.Add(Text);

            pdfDoc.Close();
            HttpContext.Current.Response.Buffer = true;
            HttpContext.Current.Response.ContentType = "application/pdf";
            HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" + "demo" + ".pdf");
            HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            HttpContext.Current.Response.Write(pdfDoc);
            HttpContext.Current.Response.End();
        }
        catch (Exception ex)
        {
            HttpContext.Current.Response.Write("error" + ex.Message);
        }
        
    }

    //Get list of preferences by
    //preferenceType {Diet, Hobbies}
    //enity {Student, Family}
    public List<string> GetPreferenceSelectionsByType(string preferenceType, string entity,  int id)
    {
        //Get PrefererenceSelections
        List<string> result = new List<string>();
        DataTable dtPref = new DataTable();
        if (entity.ToLower().Equals("student"))
        {
            dtPref = hsInfo.GetHomestayPreferenceSelections(id, 0);
        }
        else if (entity.ToLower().Equals("family"))
        {
            dtPref = hsInfo.GetHomestayPreferenceSelections(0, id);
        }

        //Get PreferencesByType
        string[] preferences = GetPreferencesByType(preferenceType);

        if (dtPref != null && dtPref.Rows.Count > 0)
        {
            foreach (DataRow dtFamPrefRow in dtPref.Rows)
            {
                string fieldIDStr = dtFamPrefRow["fieldID"].ToString().Trim();
                string fieldValueStr = dtFamPrefRow["fieldValue"].ToString().Trim();
                if (Array.Exists(preferences, element => element == fieldValueStr))
                {
                    result.Add(dtFamPrefRow["Preference"].ToString().Trim());
                }
            }
        }

        //Add Optional
        if (entity.ToLower().Equals("family") && preferenceType.ToLower().Equals("hobbies"))
        {
            DataTable dtFamilyInfo = hsInfo.GetHomestayFamilyInfo(id, "", "ID");
            if (dtFamilyInfo != null && dtFamilyInfo.Rows.Count > 0)
            {
                string otherHobbies = dtFamilyInfo.Rows[0]["otherHobbies"].ToString().Trim();
                if (!String.IsNullOrEmpty(otherHobbies))
                {
                    result.Add(otherHobbies);
                }
            }
        }
        if (entity.ToLower().Equals("student") && preferenceType.ToLower().Equals("hobbies"))
        {
            DataTable dtInfo = hsInfo.GetOneHomestayStudent(id);
            if (dtInfo != null && dtInfo.Rows.Count > 0)
            {
                string otherHobbies = dtInfo.Rows[0]["activitiesEnjoyed"].ToString().Trim();
                if (!String.IsNullOrEmpty(otherHobbies))
                {
                    result.Add(otherHobbies);
                }
            }
        }
        
        return result;
    }

    //
    public string[] GetPreferencesByType(string type)
    {
        if (type.ToLower().Equals("diet"))
        {

            return new string[] { "Vegetarian", "GlutenFree", "DairyFree", "Halal", "OtherFoodAllergy", "Kosher", "NoSpecialDiet" };
        }else if (type.ToLower().Equals("hobbies"))
        {
            return new string[] { "MusicalInstrument", "Art" ,"TeamSports", "IndividualSports" ,"ListenMusic","Drama","WatchMovies",
                                "Singing","Shopping","ReadBooks","Outdoors","Cooking", "Photography", "Gaming"};
        }else if (type.ToLower().Equals("othersmoke"))
        {
            return new string[] { "Other smoker OK", "Other smoker not OK", "Other smoker no preference" };
        }
        return new string[] { };
    }
    public List<string> GetFamilyHobbies(int intFamilyID)
    {
        List<string> hobbiesList = new List<string>();
        DataTable dtPref = hsInfo.GetHomestayPreferenceSelections(0, intFamilyID);
        if (dtPref != null && dtPref.Rows.Count > 0)
        {
            string[] hobbies = { "MusicalInstrument", "Art" ,"TeamSports", "IndividualSports" ,"ListenMusic","Drama","WatchMovies",
                                "Singing","Shopping","ReadBooks","Outdoors","Cooking", "Photography", "Gaming"};
            foreach (DataRow dtFamPrefRow in dtPref.Rows)
            {
                string fieldIDStr = dtFamPrefRow["fieldID"].ToString().Trim();
                string fieldValueStr = dtFamPrefRow["fieldValue"].ToString().Trim();
                if (Array.Exists(hobbies, element => element == fieldValueStr))
                {
                    hobbiesList.Add(dtFamPrefRow["Preference"].ToString().Trim());
                }
            }

            DataTable dtFamilyInfo = hsInfo.GetHomestayFamilyInfo(intFamilyID, "", "ID");
            if (dtFamilyInfo != null && dtFamilyInfo.Rows.Count > 0)
            {
                string otherHobbies = dtFamilyInfo.Rows[0]["otherHobbies"].ToString().Trim();
                if (!String.IsNullOrEmpty(otherHobbies))
                {
                    hobbiesList.Add(otherHobbies);
                }
            }
        }
        return hobbiesList;
    }

    public List<string> GetDiet(int intFamilyID)
    {
        List<string> hobbiesList = new List<string>();
        DataTable dtPref = hsInfo.GetHomestayPreferenceSelections(0, intFamilyID);
        if (dtPref != null && dtPref.Rows.Count > 0)
        {
            string[] diets = new string[] { "Vegetarian", "GlutenFree", "DairyFree", "Halal", "OtherFoodAllergy", "Kosher", "NoSpecialDiet" };
            foreach (DataRow dtFamPrefRow in dtPref.Rows)
            {
                string fieldIDStr = dtFamPrefRow["fieldID"].ToString().Trim();
                string fieldValueStr = dtFamPrefRow["fieldValue"].ToString().Trim();
                if (Array.Exists(diets, element => element == fieldValueStr))
                {
                    hobbiesList.Add(dtFamPrefRow["Preference"].ToString().Trim());
                }
            }

            
        }
        return hobbiesList;
    }

    public int ParseInt(string value)
    {
        int result;
        return int.TryParse(value, out result) ? result : 0;
    }

    public string GetShortDateString(string dateTimeStr)
    {
        DateTime arrivalDate;
        return DateTime.TryParse(dateTimeStr, out arrivalDate) ? arrivalDate.ToShortDateString() : "n/a";
    }


}