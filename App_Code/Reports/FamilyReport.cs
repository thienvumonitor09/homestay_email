﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for FamilyReport
/// </summary>
public class FamilyReport
{
    public string Id { get; set; }
    public string HomeName { get; set; }
    public string GenderPreference { get; set; }
    public string FamilyStatus { get; set; }
    public string CollegeQualified { get; set; }
    public string TimeSCC { get; set; }
    public string TimeSFCC { get; set; }
    public string PetsInHome { get; set; }
    public string Address { get; set; }
    //public string City { get; set; }
    //public string State { get; set; }
    //public string ZIP { get; set; }
    public string LandlinePhone { get; set; }
    //public string CellPhone { get; set; }
    //public string Email { get; set; }
    public string NoOpenRoom { get; set; }
    public string AllowSmoking { get; set; }
    public string HSOption { get; set; }
    public string Student { get; set; }
}