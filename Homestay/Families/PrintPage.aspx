﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PrintPage.aspx.cs" Inherits="Homestay_Families_PrintPage" EnableEventValidation="false" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <div id="employeelistDiv" runat="server">  
    <form id="form1" runat="server">
        
            Homestay Program - Select all that apply<br />
        <asp:CheckBox ID="Full18" runat="server" Text="Full - 18+ Room and Board, $700" Checked="true"/><br />
        <asp:CheckBox ID="Shared18" runat="server" Text="Shared - 18+ Room Only, $400" /><br />
        <asp:CheckBox ID="FullUA" runat="server" Text="Full Minors - Ages 16/17, Room and Board, $850" /><br />
        <asp:CheckBox ID="NoPreference" runat="server" Text="No preference" /><br />

        <asp:RadioButtonList ID="rdoGenderPreference" runat="server" RepeatDirection="Horizontal">
             <asp:ListItem Value="Male" Text="Males Only" />
             <asp:ListItem Value="Female" Text="Females Only" />
             <asp:ListItem Value="Either" Text="Either" />
             <asp:ListItem Value="Combination" Text="Combination - separate bathrooms available" />
        </asp:RadioButtonList>
    <table border="1">  
        <tr>  
            <td colspan="2"> <b>Employee Detail</b> </td>  
        </tr>  
        <tr>  
            <td><b>EmployeeID:</b></td>  
            <td>  
                <asp:Label ID="lblEmployeeId" runat="server"></asp:Label>  
            </td>  
        </tr>  
        <tr>  
            <td><b>FirstName:</b></td>  
            <td>  
                <asp:Label ID="lblFirstName" runat="server"></asp:Label>  
            </td>  
        </tr>  
        <tr>  
            <td><b>LastName:</b></td>  
            <td>  
                <asp:Label ID="lblLastName" runat="server"></asp:Label>  
            </td>  
        </tr>  
        <tr>  
            <td><b>City:</b></td>  
            <td>  
                <asp:Label ID="lblCity" runat="server"></asp:Label>  
            </td>  
        </tr>  
        <tr>  
            <td><b>Region:</b></td>  
            <td>  
                <asp:Label ID="lblState" runat="server"></asp:Label>  
            </td>  
        </tr>  
        <tr>  
            <td><b>Postal Code:</b></td>  
            <td>  
                <asp:Label ID="lblPostalCode" runat="server"></asp:Label>  
            </td>  
        </tr>  
        <tr>  
            <td><b>Country:</b></td>  
            <td>  
                <asp:Label ID="lblCountry" runat="server"></asp:Label>  
            </td>  
        </tr>  
    </table>  

<asp:Button ID="btnExport" runat="server" Text="Export" OnClick="btnExport_Click" />  
    </form>
        </div>  
</body>
</html>
