﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for CCSUnits
/// </summary>
public class CCSUnits
{
    public CCSUnits()
	{
        //Constructor
	}
    public DataTable getCCSUnits(string strLookupType, string strIsActive, string strUnit, string strDivision, string strSMSDeptID)
    {
        // @CALLTYPE -- DISTDIV (distinct division lookup), DISTDEPT (distinct department lookup), ORIGDEPT (original department lookup)
        // @ACTIVE -- 0 or 1 (1 = active)
        // @HOMECAMPUS -- 171, 172 or ALL
        // @DIVISION -- DIVISION INPUT
        // @ORIGDEPT -- ORIGINAL SMS DEPTID

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCommand = new SqlCommand();
        SqlParameter param = new SqlParameter();
        SqlDataAdapter objDataAdapter;
        DataSet ds = new DataSet();
        try
        {
            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSGen"].ToString();
            objConn.Open();
            objCommand.CommandText = "usp_CCSUnits_LookUp";
            objCommand.CommandType = CommandType.StoredProcedure;
            objCommand.Connection = objConn;

            param.ParameterName = "@CALLTYPE";
            param.SqlDbType = SqlDbType.VarChar;
            param.Size = 10;
            param.Value = strLookupType;
            objCommand.Parameters.Add(param);

            param = new SqlParameter();
            param.ParameterName = "@ACTIVE";
            param.SqlDbType = SqlDbType.VarChar;
            param.Size = 1;
            param.Value = strIsActive;
            objCommand.Parameters.Add(param);

            param = new SqlParameter();
            param.ParameterName = "@HOMECAMPUS";
            param.SqlDbType = SqlDbType.VarChar;
            param.Size = 3;
            param.Value = strUnit;
            objCommand.Parameters.Add(param);

            param = new SqlParameter();
            param.ParameterName = "@DIVISION";
            param.SqlDbType = SqlDbType.VarChar;
            param.Size = 50;
            param.Value = strDivision;
            objCommand.Parameters.Add(param);

            param = new SqlParameter();
            param.ParameterName = "@ORIGDEPT";
            param.SqlDbType = SqlDbType.VarChar;
            param.Size = 4;
            param.Value = strSMSDeptID;
            objCommand.Parameters.Add(param);

            objDataAdapter = new SqlDataAdapter(objCommand);
            objDataAdapter.Fill(ds);
        }
        finally
        {
            objConn.Close();
            objConn.Dispose();
            objCommand.Dispose();
        }
        return ds.Tables[0];
    }
}