﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CCSApps.master" AutoEventWireup="true" CodeFile="Communication.aspx.cs" Inherits="Homestay_Admin_Communication" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="X-UA-Compatible" content="IE=11" />
     <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular-animate.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular-sanitize.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" /> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ng-ckeditor/0.2.1/ng-ckeditor.min.js"></script>
    <!--<script src="https://cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script> -->
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular-route.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular-cookies.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/open-iconic/1.1.1/font/css/open-iconic-bootstrap.min.css"  />
    <script type="text/javascript" src="Scripts/ckeditor/ckeditor.js"></script> 
    <script type="text/javascript" src="Scripts/app.js"></script> 
    <script type="text/javascript" src="Scripts/app.config.js"></script> 
    <script src="Scripts/directvies/ckEditor.directive.js"></script>

    <script type="text/javascript" src="Scripts/controllers/templateCtrl.js"></script> 
    <script type="text/javascript" src="Scripts/controllers/emailCtrl.js"></script> 
    <script type="text/javascript" src="Scripts/controllers/reportCtrl.js"></script> 
    <script type="text/javascript" src="Scripts/controllers/individualReportCtrl.js"></script> 
    <!-- css for icon -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> 

    <style>
        .modal-dialog,
        .modal-content {
            /* 80% of window height */
            height: 90%;
        }

        .modal-body {
            /* 100% = dialog height, 120px = header + footer */
            max-height: 80%;
            overflow-y: scroll;
        }

       
    </style>
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bannerH1Text" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">


    <div ng-app="myApp">
      
        <div class="container">
            <nav class="navbar navbar-primary">
              <div class="container-fluid">
                <ul class="nav navbar-nav">
                  
                  <li><a href="../Default.aspx">Admin page</a></li>
                  <li><a href="Communication.aspx">Main Email Tool</a></li>
                  <li><a href="#!templates">Templates</a></li>
                    <li><a href="#!reports">Reports</a></li>
                </ul>
              </div>
            </nav>
            <!--
            <div style="padding-bottom:10px;">
                <a href="../Default.aspx">Admin page</a> &nbsp; &nbsp;&nbsp;
                <a href="Communication.aspx">Main Email Tool</a> &nbsp; &nbsp;&nbsp;
                &nbsp; &nbsp;&nbsp;
                
            </div>
            
            -->
            <div ng-view></div>
        </div>

           
            
</div>

</asp:Content>

