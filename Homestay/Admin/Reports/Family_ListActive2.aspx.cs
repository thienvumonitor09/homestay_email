﻿using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Homestay_Admin_Reports_Family_ListActive2 : System.Web.UI.Page
{
    Homestay hsInfo = new Homestay();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            ShowData();
        }

    }
    protected void ShowData()
    {
        DataTable dt = GetDataTable();
        gvFamilies.DataSource = dt;
        gvFamilies.DataBind();
        ViewState["dirState"] = dt;
        ViewState["sortdr"] = "Asc";
    }
    protected void gvFamilies_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dtrslt = (DataTable)ViewState["dirState"];
        if (dtrslt.Rows.Count > 0)
        {
            if (Convert.ToString(ViewState["sortdr"]) == "Asc")
            {
                dtrslt.DefaultView.Sort = e.SortExpression + " Desc";
                ViewState["sortdr"] = "Desc";
            }
            else
            {
                dtrslt.DefaultView.Sort = e.SortExpression + " Asc";
                ViewState["sortdr"] = "Asc";
            }
            gvFamilies.DataSource = dtrslt;
            gvFamilies.DataBind();


        }

    }
    private DataTable GetDataTable()
    {
        DataTable dtDisplay = new DataTable();
        //Add Columns for DataTable dtDisplay
        dtDisplay.Columns.AddRange(new DataColumn[8]
                    {
                        new DataColumn("id", typeof(string)),
                        new DataColumn("FullName", typeof(string)),
                        new DataColumn("GenderPreference", typeof(string)),
                        new DataColumn("AllowSmoking", typeof(string)),
                        new DataColumn("CollegeQualified", typeof(string)),
                        new DataColumn("TimeSCC", typeof(string)),
                        new DataColumn("TimeSFCC", typeof(string)),
                        new DataColumn("HSOption", typeof(string))
                    });

        DataTable dtFamilies = hsInfo.GetHomestayFamilyInfo(0, "", "DDLactive");
        if (dtFamilies != null && dtFamilies.Rows.Count > 0)
        {
            foreach (DataRow drFamily in dtFamilies.Rows)
            {

                Int32 intFamilyID = Convert.ToInt32(drFamily["id"].ToString());
                
                DataTable dtRelatives = hsInfo.GetHomestayRelatives(0, intFamilyID);

                DataRow displayRow = dtDisplay.NewRow();
                DataTable dtFamilyInfo = hsInfo.GetHomestayFamilyInfo(intFamilyID, "", "ID");
                string collegeQualified = "N/A";
                int busMinutesSCC = 0;
                int busMinutesSFCC = 0;
                string strGenderPref = "";
                string hsOption = "N/A";
                if (dtFamilyInfo != null && dtFamilyInfo.Rows.Count > 0)
                {
                    DataRow drFamilyInfo = dtFamilyInfo.Rows[0];

                    collegeQualified = drFamilyInfo["collegeQualified"].ToString();
                    busMinutesSCC = drFamilyInfo.IsNull("busMinutesSCC") ? 0 : Convert.ToInt32(dtFamilyInfo.Rows[0]["busMinutesSCC"].ToString().Trim());
                    busMinutesSFCC = drFamilyInfo.IsNull("busMinutesSFCC") ? 0 : Convert.ToInt32(dtFamilyInfo.Rows[0]["busMinutesSFCC"].ToString().Trim());
                    //Get HSOption
                    hsOption = drFamilyInfo["homestayOption"].ToString();
                    strGenderPref = drFamilyInfo["genderPreference"].ToString();
                }

                displayRow["id"] = intFamilyID + "";
                displayRow["FullName"] = drFamily["homeName"].ToString();
                displayRow["GenderPreference"] = strGenderPref;
                displayRow["AllowSmoking"] = GetAllowSmoking(intFamilyID);
                displayRow["CollegeQualified"] = collegeQualified;
                displayRow["TimeSCC"] = busMinutesSCC.ToString();
                displayRow["TimeSFCC"] = busMinutesSFCC.ToString();
                displayRow["HSOption"] = hsOption;
                dtDisplay.Rows.Add(displayRow);

            }
        }
        
       
        return dtDisplay;
    }
    protected void OnSelectedIndexChanged(object sender, EventArgs e)
    {
        BindDetailView();
    }

    protected void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvFamilies.PageIndex = e.NewPageIndex;
        ShowData();
        
    }
    private void BindDetailView()
    {

        dvFamily.DataSource = GetDvFamily();
        dvFamily.DataBind();
    }
    private DataTable GetDvFamily()
    {
        //Bind dtDisplay to dvPlacement
        string strFamilyID = ((Label)gvFamilies.SelectedRow.FindControl("lblid")).Text;
        int intFamilyID = Convert.ToInt32(strFamilyID);
        return GetDvFamilyByID(intFamilyID);
        
    }
    private DataTable GetDvFamilyByID(int intFamilyID)
    {
        DataTable dt = new DataTable();
        dt.Columns.AddRange(new DataColumn[3]
            {
                new DataColumn("Address", typeof(string)),
                new DataColumn("LandlinePhone", typeof(string)),
                new DataColumn("Students", typeof(string))
            });
        DataRow displayRow = dt.NewRow();
        DataTable dtFamilyInfo = hsInfo.GetHomestayFamilyInfo(intFamilyID, "", "ID");

        if (dtFamilyInfo != null && dtFamilyInfo.Rows.Count > 0)
        {
            DataRow drFamilyInfo = dtFamilyInfo.Rows[0];
            string address = String.Format("{0}, {1}, {2}, {3}"
                                        , drFamilyInfo["StreetAddress"].ToString()
                                        , drFamilyInfo["City"].ToString()
                                        , drFamilyInfo["State"].ToString().Trim()
                                        , drFamilyInfo["ZIP"].ToString()
                                        );
            displayRow["Address"] = address;
            displayRow["LandlinePhone"] = drFamilyInfo["landlinePhone"].ToString();
            displayRow["Students"] = GetStudentsLivingWithFamily(intFamilyID);
        }



        dt.Rows.Add(displayRow);
        return dt;
    }
    private string GetStudentsLivingWithFamily(int intFamilyID)
    {
        string sqlQuery = @"SELECT *
                            FROM [CCSInternationalStudent].[dbo].[HomestayPlacement]
                            WHERE familyID =" + intFamilyID;
        Homestay hs = new Homestay();
        DataTable dtPlacements = hs.RunGetQuery(sqlQuery);
        string result = "";
        if (dtPlacements != null && dtPlacements.Rows.Count > 0) { }
        {
            foreach (DataRow drPlacement in dtPlacements.Rows)
            {
                int applicantID = Convert.ToInt32(drPlacement["applicantID"].ToString());
                DataTable dtInfo = hsInfo.GetOneInternationalStudent("", applicantID, "", "", "", "");
                DataRow drInfo = dtInfo.Rows[0];
                result += String.Format("Student: {0}, Room Number: {1}, Effective Quarter: {2}, End Quarter: {3} <br/>"
                                , drInfo["familyName"].ToString() + ", " + drInfo["firstName"].ToString()
                                , drPlacement["roomNumber"].ToString()
                                , drPlacement["effectiveQuarter"].ToString()
                                , drPlacement["endQuarter"].ToString()
                                );
            }


        }
        result = String.IsNullOrEmpty(result) ? "N/A" : result;

        return result;
    }
    private string GetAllowSmoking(int intFamilyID)
    {
        DataTable dtFamPref = hsInfo.GetHomestayPreferenceSelections(0, intFamilyID);
        var result = dtFamPref.AsEnumerable().FirstOrDefault(myRow => myRow.Field<String>("Preference").Contains("Other smoker"));
        if (result != null)
        {
            if (result["Preference"].ToString().Contains("not"))
            {
                return "No";
            }
            else
            {
                return "Yes";
            }
        }

        return "N/A";
    }

    protected void rptStudents_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HyperLink lnkStudents = e.Item.FindControl("lnkStudents") as HyperLink;
            DataRowView drv = e.Item.DataItem as DataRowView;
            string studentID = drv.Row["applicantID"].ToString();
            string familyID = drv.Row["familyID"].ToString();
            
            lnkStudents.Text = drv.Row["familyName"].ToString() + ", " + drv.Row["firstName"].ToString();
            lnkStudents.NavigateUrl = "/Homestay/Admin/Matching/editplacement.aspx?studentID=" + studentID + "&familyID=" + familyID;
        }
    }
    protected void gvFamilies_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        Repeater rptStudents = e.Row.FindControl("rptStudents") as Repeater;
        if (e.Row.RowIndex >= 0)
        {
            var lblid = e.Row.FindControl("lblid") as Label;
            int familyID = Convert.ToInt32(lblid.Text);
            DataTable dtPlacement = GetDtPlacementByFamilyID(familyID);
            rptStudents.DataSource = dtPlacement;
            rptStudents.DataBind();
        }
    }

    private DataTable GetDtPlacementByFamilyID(int familyID)
    {
        string sqlQuery = @"SELECT *
                               FROM [CCSInternationalStudent].[dbo].[HomestayPlacement] a
                              INNER JOIN ApplicantBasicInfo b
                              ON a.applicantID = b.id
                              where familyID=" + familyID;
        return hsInfo.RunGetQuery(sqlQuery);
    }
    protected void ExportToExcel(object sender, EventArgs e)
    {
        Response.Clear();
        Response.Buffer = true;

        Response.AddHeader("content-disposition", "attachment;filename=Active_Homestay_Students_List.xls");
        Response.Charset = "";
        Response.ContentType = "application/vnd.ms-excel";
        using (StringWriter sw = new StringWriter())
        {
            HtmlTextWriter hw = new HtmlTextWriter(sw);

            //To Export all pages
            GridView1.AllowPaging = false;
            BindGridViewExport();

            GridView1.HeaderRow.BackColor = System.Drawing.Color.White;
            foreach (TableCell cell in GridView1.HeaderRow.Cells)
            {
                cell.BackColor = GridView1.HeaderStyle.BackColor;
            }
            foreach (GridViewRow row in GridView1.Rows)
            {
                row.BackColor = System.Drawing.Color.White;
                foreach (TableCell cell in row.Cells)
                {
                    if (row.RowIndex % 2 == 0)
                    {
                        cell.BackColor = GridView1.AlternatingRowStyle.BackColor;
                    }
                    else
                    {
                        cell.BackColor = GridView1.RowStyle.BackColor;
                    }
                    cell.CssClass = "textmode";
                }
            }

            GridView1.RenderControl(hw);

            //style to format numbers to string
            string style = @"<style> .textmode { } </style>";
            Response.Write(style);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();
        }
    }
    protected void ExportToPDF(object sender, EventArgs e)
    {
        using (StringWriter sw = new StringWriter())
        {
            using (HtmlTextWriter hw = new HtmlTextWriter(sw))
            {
                //To Export all pages
                GridView1.AllowPaging = false;
                BindGridViewExport();

                GridView1.RenderControl(hw);
                StringReader sr = new StringReader(sw.ToString());
                Document pdfDoc = new Document(PageSize.A2, 10f, 10f, 10f, 0f);
                //HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
                PdfWriter writer = PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                pdfDoc.Open();
                XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, sr);
                pdfDoc.Close();

                Response.ContentType = "application/pdf";
                Response.AddHeader("content-disposition", "attachment;filename=List_Active_Families.pdf");
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.Write(pdfDoc);
                Response.End();

                
            }
        }
    }

    private void BindGridViewExport()
    {
        //Add DetailsView to GridView
        DataTable dt = GetDataTable();
        dt.Columns.AddRange(new DataColumn[3]
        {
                new DataColumn("Students", typeof(string)),
                 new DataColumn("Address", typeof(string)),
                 new DataColumn("LandlinePhone", typeof(string))
        });

        foreach (DataRow dr in dt.Rows)
        {
            int familyID = Convert.ToInt32(dr["id"].ToString());
            DataTable dtDv = GetDvFamilyByID(familyID);
            DataRow drDv = dtDv.Rows[0];
            DataTable dtPlacement = GetDtPlacementByFamilyID(familyID);
            List<string> students = new List<string>();
            if (dtPlacement != null && dtPlacement.Rows.Count > 0)
            {
                foreach (DataRow drPlacement in dtPlacement.Rows)
                {
                    students.Add(drPlacement["familyName"].ToString() + ", " + drPlacement["firstName"].ToString());
                }
            }
            dr["Students"] = string.Join("; ", students);
            dr["Address"] = drDv["Address"].ToString();
            dr["LandlinePhone"] = drDv["LandlinePhone"].ToString();

        }

        GridView1.DataSource = dt;
        GridView1.DataBind();
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }
    protected void btnAdmin_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Homestay/Admin/Default.aspx");
    }

    protected void btnMain_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Homestay/Admin/Reports/ReportsHome.aspx");
    }
}