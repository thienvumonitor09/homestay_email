﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web;

/// <summary>
/// Summary description for OnlineSurvey
/// </summary>
public class OnlineSurvey
{
    public string strResultMsg = "";   // Error messages are concatenated for each post-back for display on calling page
    public bool blSuccess = false;     // Used by calling page or class function to determine status of an operation 
    public Int32 intNewIdentity = 0;   // Used to retrieve the new identity created upon insert in database
    public string strFileName = "";    // Used by upload utility to pass file name of uploaded document
    public string strMaskScript = "";  // Used by calling page to show appropriate JQuery mask for form input per profile settings
    public string strCountdownScript = ""; // Used by calling page to show JQuery character countdown for text area input with limited length
    public bool blDisplayMaskScript = false;       
    public bool blDisplayCountdownScript = false;       
    string strSuccessMsg = "";
    bool blReturnIdentity = false;
    bool blIsSQL = true;
    bool blASCIIToHTML = false;
    bool blHTMLToASCII = false;

    //Survey variables for profile settings
    Int32 intSurveyID = 0;
    string strOwnerFName = ""; 
    string strOwnerLName = ""; 
    string strOwnerEmplID = ""; 
    string strCreateDate = ""; 
    string strSurveyTitle = ""; 
    string strSurveyPurpose = "";
    string strSurveyInstructions = ""; 
    Int32 intSurveyType = 0; 
    Int32 intQuestionCount = 0; 
    string strSurveyPassword = "";
    int bitAnonymous = 0;   //bit prefixes indicate value is either 1 or 0 for Transact SQL statements
    string strAnonymous = "0";
    string strOwnerEmail = "";
    int bitUserNotification = 0;
    string strUserNotification = "";
    int bitClone = 0; 
    string strClone = "0";
    string strEmail1 = "";
    string strEmail2 = "";
    string strEmail3 = "";
    int bitSupressEmail = 0;
    string strSupressEmail  = "0";
    int bitUseAltEmail = 0;
    int bitUseDefaultFrom = 0;
    string strUseDefaultFrom = "0";
    Int32 intFormType = 0;
    string strFormType = "";
    int bitOtherNotification = 0;
    string strOtherNotification = "";
    Int32 intOtherEmailCount = 0;
    string strOtherEmailCount = "";
    int bitStudentEmailOption = 0;
    string strStudentEmailOption = "";
    int bitDisplaySummaryOption = 0;
    string strDisplaySummaryOption = "";
    int bitSubmitOnce = 0;
    string strSubmitOnce = "";
    string strCustomMessage = "";
    int bitNumberFields = 0;
    string strNumberFields = "";
    Int32 intEmailResultsOption = 0;
    string strEmailResultsOption = "";
    Int32 bitRequireLogin = 0;
    Int32 bitDisplaySurveyResults = 0;
    string strTypeTag1 = "";
    string strTypeTag2 = "";
    string strTypeTag3 = "";
    string strTypeTag4 = "";
    int bitIsAdmin = 0;
    string strIsAdmin = "0";
    bool blResetFields = false;
    //WebSurvey_Questions fields
    Int32 intQID = 0;
    string strQuestion = "";
    string strCorrectText = "";
    string strIncorrectText = "";
    Int32 intOptionCount = 0;
    Int32 intQuestionType = 0;
    Int32 intQSequence = 0;
    string strRequiredField = "";
    int bitRequiredField = 0;
    Int32 intValidateType = 0;
    string strVertAlign = "";
    int bitVertAlign = 0;
    Int32 intMaskType;
    string strMaskType = "";
    Int32 intMaxLength = 0;
    string strMaxLength = "";
    string strDisplayCountdown = "";
    int bitDisplayCountdown = 0;
    int bitIsFieldGroup = 0;
    string strIsFieldGroup = "";
    //WebSurvey_Options fields
    Int32 intOptionID = 0;
    int bitFillIn = 0;
    string strDescription = "";
    int bitAnswer = 0;
    Int32 intOSequence = 0;
    Int32 intOptionGroupID = 0;
    int bitIsOptionGroup = 0;
    Int32 MaxOfOptionGroupID = 0;
    Int32 intOptionTemplate = 0;
    string strUploadURL = @"\\ccs-internet\InternetContent\CCS\OnlineSurvey\";    //Location of email attachment storage folder
    Int32 intDropDownListType = 0;

    //Survey Question Arrays
    //Renamed prefix Survey to Quiz
    string[] arrQuizCorrectText = new string[200];
    string[] arrQuizIncorrectText = new string[200];
    //NOTE:  arrSurveyQuestions (renamed) is modular in ASP version
    string[] arrQuestions = new string[200];
    int[] arrOptionCount = new int[200];
    int[] arrQuestionType = new int[200];
    int[] arrQID = new int[200];
    int[] arrValidateType = new int[200];
    int[] arrMaskType = new int[200];
    int[] arrMaxLength = new int[200];
    Boolean[] arrRequiredField = new Boolean[200];
    Boolean[] arrVertAlign = new Boolean[200];
    int[] arrDisplayCountdown = new int[200];
    Boolean[] arrFieldGroup = new Boolean[200];
    int[] arrDropDownListType = new int[200];

	public OnlineSurvey()
	{
		//
		// Constructor 
		//
	}
    /*
    '*******************************************************************
    'INDEX TO ALL FUNCTIONS
    '*******************************************************************
    'TEXT FUNCTIONS 
    '*******************************************************************
        HTMLtoASCII
        CreateYNDropdown - used to create a dropdown with Yes/No
        CreateRadioSet - used to create radio buttons.
    '*******************************************************************
    'PRIMARY SURVEY FUNCTIONS - define profile, questions and options
    '*******************************************************************
        CreateSurvey
        EditSurvey
        DeleteSurvey
        InsertOSOwner
        DeleteOSOwner
        LookupPrimaryOwner(intSurveyID)
        CreateQuestion
        EditQuestion
        DeleteSurveyQuestion
        UpdateQuestionOrder
        ChangeQuestionOrder
    	GetQuestionCount
    	GetQuestionType
    	IncrementQuestionCount
    	DecrementQuestionCount
        CreateOption
        EditOption
        DeleteOneOption
        GetMaxOptionGroupID
        LookupOptionGroupID
        LookupOptionTemplateID
        UpdateWebSurveyOptionGroupID
    	IncrementOptionCount
    	DecrementOptionCount
        CreateTally
        EditTally
    	DecrementTallyCount
        IncrementTallyUserCount
        CloneSurvey
        CountOptionsByOptionsGroupID
    '*************************************************************************
    'OPTION DROP-DOWN LIST FUNCTIONS - ALL RETURN HTML OPTION ELEMENTS
    '*************************************************************************
    	optSurveyTypes
        optSurveyTypesNoQuiz
    	optWebSurveys
    	optWebSurveys_Edit (omits surveys with open tallies; cannot be edited)
    	optWebSurveysAll - admins only
    	optWebSurveysAllSec - admins only
    	optOpenWebSurveys
    	optWebSurveysOnly - no forms
        optWebSurveysOnlyAdmin
    	optWebSurveyOwners
    	optWebSurveyQuestions
    	optQuestionTypes
    	optWebTallies
    	optWebQuizStudents
        optWebAdmins(strSelected)
    '*************************************************************************
    'GET FUNCTIONS - ALL RETURN DATATABLE
    '*************************************************************************
    	GetOneSurvey
    	GetOwnerSurveys
    	GetOneSurveyAndOwner
    	GetOneOwner_ByUID
    	GetOneOwner_ByID
    	GetOneQuestion
        GetAllSurveyQuestions
        GetSurveyQuestionOptions
    	GetOneTally
        GetOptionsByOptionGroupID(intOptionGroupID)
        GetOptionsByOptionTemplate(strSelSurvey, intOptionGroupID, strTestDescription)
    	GetSurveyTallyResults
        GetSelectSurveyResults
        GetSelectSurveyUsers
        GetOneWebAdmin(strAdminID) - COMBINED WITH GetOneWebAdminByUserID(strEmplID)
        GetOneSurveyUser
        GetTallyResponsesByQ
        GetTallyResponsesByUID
        GetSurveyUsers
        GetExportResults
    '*************************************************************************
    'USER MODULE FUNCTIONS - User interaction with survey/form/quiz
    '*************************************************************************
    	LoadQuestionArrays
        CalculateTallyResults
    	DisplaySurveyQuestions
       	ProcessEmailResults - MOVE THIS TO PAGE-LEVEL FUNCTION?
    	DisplayTallyResults
    	CreateUser
    	CreateResponse
    	AddUserComments
        ValidateSubmitOnceSID
    	ValidateOwner
        DisplayOwnerReport
    '*****************************************************
    ' ADMIN/PROCESS FUNCTIONS - Admin and processing forms
    '*****************************************************
    	DisplayFormResults
    	UpdateResponses
        DeleteTallyResults
    	DeleteOneUserRecord
    	DisplayQuizResults
        DeleteAdmin(intAdminID)
        InsertAdmin(strEmplID, strFName, strLName, strEmail, strPhone, intType, strLastAccess, intLastProfileID)
        UpdateAdmin(intWAdmin_ID, strEmplID, strFName, strLName, strEmail, strPhone, intType, strLastAccess, intLastProfileID)
    */

    #region "TEXT FUNCTIONS"
    /// <summary>
    /// Removes HTML line breaks, bolding tags and converts &#39; to apostrophe
    /// </summary>
    /// <param name="strText"></param>
    /// <returns>Converted text</returns>
    public string HTMLtoASCII(string strText)
    {
        string strResult = strText;
        strResult = strResult.Replace("<br />", System.Environment.NewLine);
        strResult = strResult.Replace("<br>", System.Environment.NewLine);
	    strResult = strResult.Replace("<strong>", "");
	    strResult = strResult.Replace("</strong>", "");
	    strResult = strResult.Replace("&#39;", "'");
        return strResult;
    }

    /// <summary>
    /// Pass parameters to create a Yes/No drop-down listbox
    /// </summary>
    /// <param name="strSetValue"></param>
    /// <param name="strControlName"></param>
    /// <param name="blReadOnly"></param>
    /// <returns>Returns a string with a drop-down listbox control</returns>
    public string CreateYNDropdown(string strSetValue, string strControlName, bool blReadOnly)
    {
        //strResultMsg += "Set Value: " + strSetValue + "<br />";
        string strResult = "";
	    if (blReadOnly)
        {
            if (strSetValue.ToLower() == "yes" || strSetValue == "1" || strSetValue.ToLower() == "true")
            {
			    strResult = "Yes";
            }
		    else
            {
			    strResult = "No";
            }
        }
	    else	
        {	
		    strResult = "<select name=\"" + strControlName + "\">";
		    strResult += "<option value=\"Yes\"";
		
		    if (strSetValue.ToLower() == "yes" || strSetValue == "1" || strSetValue.ToLower() == "true")
            {
			    strResult += " selected=\"selected\"";
		    }
		    strResult += ">Yes</option>";
		    strResult += "<option value=\"No\"";
            if (strSetValue.ToLower() == "no" || strSetValue == "0" || strSetValue.ToLower() == "false")
            {
                strResult += "  selected=\"selected\"";
		    }
		    strResult += ">No</option>";
		    strResult += "</select>";
        }
        return strResult;
    }

    /// <summary>
    /// Pass parameters to return a string for input type radio button
    /// </summary>
    /// <param name="strValue"></param>
    /// <param name="strControlName"></param>
    /// <param name="blChecked"></param>
    /// <param name="blReadOnly"></param>
    /// <returns>Returns a string for input type radio button</returns>
    public string CreateRadioButton(string strValue, string strControlName, bool blChecked, bool blReadOnly)
    {
        string strResult = "";
	    if (blReadOnly)
        {
		    if (blChecked)
            {
			    strResult = "Yes";
            }
		    else
            {
			    strResult = "No";
            }
        }
	    else
        {
		    strResult = "<input type=\"radio\" name=\"" + strControlName + "\"";
		    strResult += " value=\"" + strValue + "\""; 
		    if (blChecked)
            {
			    strResult += " CHECKED ";
            }
		    strResult += ">";
	    }

        return strResult;
    }
#endregion

#region "PROFILE, OWNERS, QUESTIONS, OPTIONS"

    /// <summary>
    /// Executes a stored procedure or TSQL statement as NonQuery or Scalar if blIsInsert=true
    /// </summary>
    /// <param name="strSQL"></param>
    /// <param name="blIsInsert"></param>
    /// <returns>Row(s) affected; or scalar new identity value</returns>
    public Int32 runOSSQLQuery(string strSQL, bool blIsInsert)
    {
        Int32 intResult = 0;
        blSuccess = true;
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCommand = new SqlCommand();
        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSOnlineSurvey"].ToString();
        try
        {
            objConn.Open();
            objCommand.CommandText = strSQL;
            objCommand.CommandType = CommandType.Text;
            objCommand.Connection = objConn;
            if(blIsInsert)
            {
                //Execute scalar
                intResult = (Int32)objCommand.ExecuteScalar();
            }
            else
            {
                //Execute NonQuery
                intResult = objCommand.ExecuteNonQuery();
            }
        }
        catch(Exception ex)
        {
            //capture error
            strResultMsg += "Error runOSSQLQuery: " + ex.Message + "<br />";
            strResultMsg += "SQL: " + strSQL + "<br />";
            blSuccess = false;
            return intResult;
        }
        finally
        {
            objConn.Close();
            objConn.Dispose();
            objCommand.Dispose();
        }
        return intResult;
    }
    /// <summary>
    /// Creates a basic profile with settings
    /// </summary>
    /// <param name="strCreateDate"></param>
    /// <param name="strSurveyTitle"></param>
    /// <param name="strSurveyPurpose"></param>
    /// <param name="strSurveyInstructions"></param>
    /// <param name="intSurveyType"></param>
    /// <param name="intQuestionCount"></param>
    /// <param name="strSurveyPassword"></param>
    /// <param name="blAnonymous"></param>
    /// <param name="blUserNotification"></param>
    /// <param name="blClone"></param>
    /// <param name="strEmail1"></param>
    /// <param name="strEmail2"></param>
    /// <param name="strEmail3"></param>
    /// <param name="blSupressEmail"></param>
    /// <param name="blUseAltEmail"></param>
    /// <param name="blUseDefaultFrom"></param>
    /// <param name="intFormType"></param>
    /// <param name="blOtherNotification"></param>
    /// <param name="intOtherEmailCount"></param>
    /// <param name="blStudentEmailOption"></param>
    /// <param name="blDisplaySummaryOption"></param>
    /// <param name="blSubmitOnce"></param>
    /// <param name="strCustomMessage"></param>
    /// <param name="blNumberFields"></param>
    /// <param name="intEmailResultsOption"></param>
    /// <returns>Result or error messages</returns>
    public Int32 CreateSurvey(string strCreateDate, string strSurveyTitle, string strSurveyPurpose, 
	    string strSurveyInstructions, string intSurveyType, Int32 intQuestionCount,
        string strSurveyPassword, string bitAnonymous, string bitUserNotification, string bitClone, string strEmail1, string strEmail2,
        string strEmail3, string bitSupressEmail, string bitUseAltEmail, string bitUseDefaultFrom, string intFormType, string bitOtherNotification,
        Int32 intOtherEmailCount, string bitStudentEmailOption, string bitDisplaySummaryOption, string bitSubmitOnce, string strCustomMessage,
        string bitNumberFields, string intEmailResultsOption, string bitRequireLogin, string bitDisplaySurveyResults)
    {
        string strSQL = "";
        intNewIdentity = 0;
        blSuccess = false;
        blReturnIdentity = true;
        blIsSQL = true;
        blASCIIToHTML = false;
        blHTMLToASCII = false; 
        Utility Fixer = new Utility();
        strSurveyTitle = Fixer.ConvertAndFixText(strSurveyTitle, blASCIIToHTML, blHTMLToASCII, blIsSQL);
        strSurveyInstructions = Fixer.ConvertAndFixText(strSurveyInstructions, blASCIIToHTML, blHTMLToASCII, blIsSQL);
        strSurveyPurpose = Fixer.ConvertAndFixText(strSurveyPurpose, blASCIIToHTML, blHTMLToASCII, blIsSQL);
        strCustomMessage = Fixer.ConvertAndFixText(strCustomMessage, blASCIIToHTML, blHTMLToASCII, blIsSQL);
        //Return values: 0 = Insert or Connection failed; other values = new ID
	    strSQL = "pr_InsertWebSurvey '";
	    strSQL += strCreateDate + "', '" + strSurveyTitle;
	    strSQL += "', '" + strSurveyPurpose + "', '" + strSurveyInstructions + "', " + intSurveyType + ", ";
	    strSQL += intQuestionCount + ", '" + strSurveyPassword + "', " + bitAnonymous;
        strSQL += ", " + bitUserNotification + ", " + bitClone + ", '" + strEmail1 + "', '";
        strSQL += strEmail2 + "', '" + strEmail3 + "', " + bitSupressEmail + ", " + bitUseAltEmail + ", " + bitUseDefaultFrom + ", " + intFormType;
        strSQL += ", " + bitOtherNotification + ", " + intOtherEmailCount + ", " + bitStudentEmailOption + ", " + bitDisplaySummaryOption + ", " + bitSubmitOnce;
        strSQL += ", '" + strCustomMessage + "', " + bitNumberFields + ", " + intEmailResultsOption + ", " + bitRequireLogin + ", " + bitDisplaySurveyResults;
        //Run the query, which captures any error message in strResultMsg
        //strResultMsg += "Create Survey SQL: " + strSQL + "<br />";
        intNewIdentity = runOSSQLQuery(strSQL, blReturnIdentity);
        if(intNewIdentity > 0)
        {
            //Reset variable
            strResultMsg += "Your survey was added successfully!<br />";
            blSuccess = true;
        }
        else
        {
            strResultMsg += "No Survey rows were affected.<br />";
            strResultMsg += "Create Survey SQL: " + strSQL + "<br />";
        }
        return intNewIdentity;
    }
    /// <summary>
    /// Update settings in basic profile
    /// </summary>
    /// <param name="intSurveyID"></param>
    /// <param name="strCreateDate"></param>
    /// <param name="strSurveyTitle"></param>
    /// <param name="strSurveyPurpose"></param>
    /// <param name="strSurveyInstructions"></param>
    /// <param name="intSurveyType"></param>
    /// <param name="intQuestionCount"></param>
    /// <param name="strSurveyPassword"></param>
    /// <param name="blAnonymous"></param>
    /// <param name="blUserNotification"></param>
    /// <param name="blClone"></param>
    /// <param name="strEmail1"></param>
    /// <param name="strEmail2"></param>
    /// <param name="strEmail3"></param>
    /// <param name="blSupressEmail"></param>
    /// <param name="blUseAltEmail"></param>
    /// <param name="blUseDefaultFrom"></param>
    /// <param name="intFormType"></param>
    /// <param name="blOtherNotification"></param>
    /// <param name="intOtherEmailCount"></param>
    /// <param name="blStudentEmailOption"></param>
    /// <param name="blDisplaySummaryOption"></param>
    /// <param name="blSubmitOnce"></param>
    /// <param name="strCustomMessage"></param>
    /// <param name="blNumberFields"></param>
    /// <param name="intEmailResultsOption"></param>
    /// <returns>Result or error messages</returns>
    public String EditSurvey(string strSurveyID, string strCreateDate, string strSurveyTitle,
        string strSurveyPurpose, string strSurveyInstructions, string intSurveyType, Int32 intQuestionCount,
        string strSurveyPassword, string bitAnonymous, string bitUserNotification, string bitClone, string strEmail1,
        string strEmail2, string strEmail3, string bitSupressEmail, string bitUseAltEmail, string bitUseDefaultFrom, string intFormType, string bitOtherNotification,
        Int32 intOtherEmailCount, string bitStudentEmailOption, string bitDisplaySummaryOption, string bitSubmitOnce, string strCustomMessage, string bitNumberFields,
        string intEmailResultsOption, string bitRequireLogin, string bitDisplaySurveyResults)
    {
	    string strSQL = "";
        blReturnIdentity = false;
        blIsSQL = true;
        blASCIIToHTML = false;
        blHTMLToASCII = false; 
        Utility Fixer = new Utility();
        strSurveyTitle = Fixer.ConvertAndFixText(strSurveyTitle, blASCIIToHTML, blHTMLToASCII, blIsSQL);
        strSurveyInstructions = Fixer.ConvertAndFixText(strSurveyInstructions, blASCIIToHTML, blHTMLToASCII, blIsSQL);
        strSurveyPurpose = Fixer.ConvertAndFixText(strSurveyPurpose, blASCIIToHTML, blHTMLToASCII, blIsSQL);
        strCustomMessage = Fixer.ConvertAndFixText(strCustomMessage, blASCIIToHTML, blHTMLToASCII, blIsSQL);
        //Return values: 0 = Update or Connection failed; 1 = Row affected
        Int32 intResult = 0;
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCommand = new SqlCommand();
        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSOnlineSurvey"].ToString();
        strSQL += "pr_UpdateWebSurvey " + strSurveyID;
	    strSQL +=", '" + strCreateDate + "', '" + strSurveyTitle;
	    strSQL +="', '" + strSurveyPurpose + "', '" + strSurveyInstructions + "', " + intSurveyType + ", ";
        strSQL += intQuestionCount + ", '" + strSurveyPassword + "', " + bitAnonymous;
        strSQL += ", " + bitUserNotification + ", " + bitClone + ", '" + strEmail1 + "', '";
        strSQL += strEmail2 + "', '" + strEmail3 + "', " + bitSupressEmail + ", " + bitUseAltEmail + ", " + bitUseDefaultFrom + ", " + intFormType;
        strSQL += ", " + bitOtherNotification + ", " + intOtherEmailCount + ", " + bitStudentEmailOption + ", " + bitDisplaySummaryOption + ", " + bitSubmitOnce;
        strSQL += ", '" + strCustomMessage + "', " + bitNumberFields + ", " + intEmailResultsOption + ", " + bitRequireLogin + ", " + bitDisplaySurveyResults;
        //Run the query, which captures any error message in strResultMsg
        intResult = runOSSQLQuery(strSQL, blReturnIdentity);
        if(intResult == 1)
        {
            //Reset variable
            strResultMsg += "Your Survey was updated successfully!<br />";
        }
        else
        {
            strResultMsg += "No Survey rows were affected.<br />";
            strResultMsg += "Edit Survey SQL: " + strSQL + "<br />";
        }
        return strResultMsg;
    }

    /// <summary>
    /// Pass Survey ID to delete profile, labels and fields, tally and submissions
    /// Pass Survey ID = 0 to delete ONLY the tally but not the profile
    /// Pass Tally ID = "All" to delete all tallies but not the profile
    /// blSuccess indicates RunOSSQLQuery ran without error
    /// </summary>
    /// <param name="intSurveyID"></param>
    /// <param name="intTallyID"></param>
    /// <param name="strSurveyTitle"></param>
    /// <returns>Result or error messages</returns>
    public String DeleteSurvey(string strSurveyID, string strTallyID)
    {
        //strResultMsg += "SurveyID: " + strSurveyID + "; TallyID: " + strTallyID + "<br />";
        bool blDeleteSurvey = false;
        bool blDeleteTally = false;
        bool blDeleteAllTallies = false;
        bool blIsError = false;
        Int32 intResult = 0;
        string strSQL = "";
        string strErrorMsg = "";
        blReturnIdentity = false;

        if (strTallyID != "" && strTallyID != "0" && strTallyID != "All" && strSurveyID == "0")
        {
            // DELETE SURVEY AND ALL TALLIES
            blDeleteTally = true;
        }
        else if (strSurveyID != "" && strSurveyID != "0" && strTallyID == "0")
        {
            // KEEP SURVEY; DELETE TALLY ONLY
            blDeleteSurvey = true;
        }
        else if (strSurveyID != "" && strSurveyID != "0" && strTallyID == "All")
        {
            // KEEP SURVEY; DELETE ALL TALLIES
            blDeleteAllTallies = true;
        }
        try
        {
            if (blDeleteSurvey)
            {
                // DELETE SURVEY AND ALL TALLIES
                //Delete tally responses
                strSQL = "pr_DeleteSurveyResponsesAll " + strSurveyID;
                //Run the query, which captures any error message in strResultMsg
                intResult = runOSSQLQuery(strSQL, blReturnIdentity);
                strResultMsg += "Responses deleted: " + intResult + "<br />";

                if (blSuccess)
                {
                    //Delete tally results
                    strSQL = "pr_DeleteSurveyTallyResultsAll " + strSurveyID;
                    //Run the query, which captures any error message in strResultMsg
                    intResult = runOSSQLQuery(strSQL, blReturnIdentity);
                    strResultMsg += "Tally Results deleted? " + intResult + "<br />";

                    if (blSuccess)
                    {
                        //Delete users
                        strSQL = "pr_DeleteSurveyUserAll " + strSurveyID;
                        //Run the query, which captures any error message in strResultMsg
                        intResult = runOSSQLQuery(strSQL, blReturnIdentity);
                        strResultMsg += "Users deleted: " + intResult + "<br />";

                        if (blSuccess)
                        {
                            //Delete Tally 
                            strSQL = "pr_DeleteSurveyTalliesAll " + strSurveyID;
                            //Run the query, which captures any error message in strResultMsg
                            intResult = runOSSQLQuery(strSQL, blReturnIdentity);
                            strResultMsg += "Tallies deleted: " + intResult + "<br />";
                            if (blSuccess)
                            {
                                //Delete Options
                                strSQL = "pr_DeleteSurveyQuestionOptionsAll " + strSurveyID;
                                //Run the query, which captures any error message in strResultMsg
                                intResult = runOSSQLQuery(strSQL, blReturnIdentity);
                                strResultMsg += "Options deleted: " + intResult + "<br />";

                                if (blSuccess)
                                {
                                    //Delete Questions
                                    strSQL = "pr_DeleteSurveyQuestionsAll " + strSurveyID;
                                    //Run the query, which captures any error message in strResultMsg
                                    intResult = runOSSQLQuery(strSQL, blReturnIdentity);
                                    strResultMsg += "Questions deleted: " + intResult + "<br />";

                                    if (blSuccess)
                                    {
                                        //Delete Survey
                                        strSQL = "pr_DeleteSurveyAll " + strSurveyID;
                                        //Run the query, which captures any error message in strResultMsg
                                        intResult = runOSSQLQuery(strSQL, blReturnIdentity);
                                        strResultMsg += "Surveys deleted: " + intResult + "<br />";
                                        if (!blSuccess)
                                        {
                                            blIsError = true;
                                        }
                                    }
                                    else
                                    {
                                        blIsError = true;
                                    }
                                }
                                else
                                {
                                    blIsError = true;
                                }
                            }
                            else
                            {
                                blIsError = true;
                            }
                        }
                        else
                        {
                            blIsError = true;
                        }
                    }
                    else
                    {
                        blIsError = true;
                    }
                }
                else
                {
                    blIsError = true;
                }

                if (blIsError)
                {
                    strResultMsg += "Problems were encountered while deleting your Profile.<br />";
                }
                else
                {
                    //Delete Other profile owners
                    strResultMsg += DeleteOSProfileOwners(strSurveyID);
                    strResultMsg += "Your Profile was successfully deleted!<br />";
                }

            }

            // KEEP SURVEY; DELETE TALLY ONLY
            if (blDeleteTally)
            {

                //Delete users
                strSQL = "pr_DeleteSurveyUser " + strTallyID;
                //Run the query, which captures any error message in strResultMsg
                intResult = runOSSQLQuery(strSQL, blReturnIdentity);

                if (blSuccess)
                {
                    //Delete Tally Results
                    strSQL = "pr_DeleteSurveyTallyResults " + strTallyID;
                    //Run the query, which captures any error message in strResultMsg
                    intResult = runOSSQLQuery(strSQL, blReturnIdentity);

                    if (blSuccess)
                    {
                        //Delete Responses
                        strSQL = "pr_DeleteSurveyResponses " + strTallyID;
                        //Run the query, which captures any error message in strResultMsg
                        intResult = runOSSQLQuery(strSQL, blReturnIdentity);

                        if (blSuccess)
                        {
                            //Delete Tallies
                            strSQL = "pr_DeleteSurveyTallies " + strTallyID;
                            //Run the query, which captures any error message in strResultMsg
                            intResult = runOSSQLQuery(strSQL, blReturnIdentity);
                            if (!blSuccess)
                            {
                                blIsError = true;
                            }
                        }
                        else
                        {
                            blIsError = true;
                        }
                    }
                    else
                    {
                        blIsError = true;
                    }
                }
                else
                {
                    blIsError = true;
                }

                if (blIsError)
                {
                    strResultMsg += "No Tally rows were affected.<br />";
                }
                else
                {
                    strResultMsg += "Your Tally was successfully deleted!<br />";
                }
            }

            if (blDeleteAllTallies)
            {
                //KEEP SURVEY; DELETE ALL TALLIES
                //Delete all tally responses
                strSQL = "pr_DeleteSurveyResponsesAll " + strSurveyID;
                //Run the query, which captures any error message in strResultMsg
                intResult = runOSSQLQuery(strSQL, blReturnIdentity);

                if (blSuccess)
                {
                    //Delete all tally results
                    strSQL = "pr_DeleteSurveyTallyResultsAll " + strSurveyID;
                    //Run the query, which captures any error message in strResultMsg
                    intResult = runOSSQLQuery(strSQL, blReturnIdentity);

                    if (blSuccess)
                    {
                        //Delete all users
                        strSQL = "pr_DeleteSurveyUserAll " + strSurveyID;
                        //Run the query, which captures any error message in strResultMsg
                        intResult = runOSSQLQuery(strSQL, blReturnIdentity);

                        if (blSuccess)
                        {
                            //Delete all Tallies
                            strSQL = "pr_DeleteSurveyTalliesAll " + strSurveyID;
                            //Run the query, which captures any error message in strResultMsg
                            intResult = runOSSQLQuery(strSQL, blReturnIdentity);
                            if (!blSuccess)
                            {
                                blIsError = true;
                            }
                        }
                        else
                        {
                            blIsError = true;
                        }
                    }
                    else
                    {
                        blIsError = true;
                    }
                }
                else
                {
                    blIsError = true;
                }
            }
        }
        catch (Exception ex)
        {
            strErrorMsg = ex.Message + "<br />";
        }
        //return strResultMsg;
        return strErrorMsg;
    }

    /// <summary>
    /// Insert an OnlineSurvey owner/authorized editor
    /// Currently pr_InsertWebOwnerAndProfile does NOT return a new identity; it inserts 2 new records. 
    /// Pass false for the last paramater
    /// </summary>
    /// <param name="strWO_EmplID"></param>
    /// <param name="strWO_FName"></param>
    /// <param name="strWO_LName"></param>
    /// <param name="strWO_Email"></param>
    /// <param name="strWO_Telephone"></param>
    /// <param name="strWO_Office"></param>
    /// <param name="intSurveyID"></param>
    /// <param name="bitWOP_Primary"></param>
    /// <param name="blReturnIdentity"></param>
    /// <returns>Result or error messages</returns>
    public String InsertOSOwner(string strWO_EmplID, string strWO_FName, string strWO_LName, string strWO_Email, string strWO_Telephone, string strWO_Office, Int32 intSurveyID, int bitWOP_Primary, bool blReturnIdentity)
    {
        //REMOVE THIS LINE OF CODE SHOULD IDENTITY BE RETURNED; CURRENTLY NOT ENABLED IN SP
        if (blReturnIdentity == true) { blReturnIdentity = false; } 

	    string  strSQL;
        blIsSQL = true;
        blASCIIToHTML = false;
        blHTMLToASCII = false;
        Utility Fixer = new Utility();
        strWO_FName = Fixer.ConvertAndFixText(strWO_FName, blASCIIToHTML, blHTMLToASCII, blIsSQL);
        strWO_LName = Fixer.ConvertAndFixText(strWO_LName, blASCIIToHTML, blHTMLToASCII, blIsSQL);
        strWO_Office = Fixer.ConvertAndFixText(strWO_Office, blASCIIToHTML, blHTMLToASCII, blIsSQL);
	    strSQL = "pr_InsertWebOwnerAndProfile '" + strWO_EmplID + "', '";
	    strSQL += strWO_FName + "', '" + strWO_LName + "', '" + strWO_Email + "', '";
	    strSQL += strWO_Telephone + "', '" + strWO_Office + "', ";
	    strSQL += intSurveyID + ", '" + bitWOP_Primary  + "'" ;
	
        //Run the query, which captures any error message in strResultMsg
        intNewIdentity = runOSSQLQuery(strSQL, blReturnIdentity);
        // Currently pr_InsertWebOwnerAndProfile does NOT return a new identity; it inserts 2 new records.
        if(intNewIdentity > 0)
        {
            //Reset variable
            strResultMsg += "Your Owner was added successfully!<br />";
        }
        else
        {
            strResultMsg += "No Owner rows were affected.<br />";
            strResultMsg += "Insert Owner SQL: " + strSQL + "<br />";
        }
        return strResultMsg;
    }

    /// <summary>
    /// Update contact information for owner
    /// </summary>
    /// <param name="strWO_EmplID"></param>
    /// <param name="strWO_FName"></param>
    /// <param name="strWO_LName"></param>
    /// <param name="strWO_Email"></param>
    /// <param name="strWO_Telephone"></param>
    /// <param name="strWO_Office"></param>
    /// <param name="intWOP_ID"></param>
    /// <param name="bitWOP_Primary"></param>
    /// <returns>Result or error messages</returns>
    public String UpdateOSOwner(string strWO_EmplID, string strWO_FName, string strWO_LName, string strWO_Email, string strWO_Telephone, string strWO_Office, Int32 intWOP_ID, int bitWOP_Primary)
	{
	    string  strSQL = "";
        blReturnIdentity = false;
        blIsSQL = true;
        blASCIIToHTML = false;
        blHTMLToASCII = false; 
        Utility Fixer = new Utility();
        strWO_LName = Fixer.ConvertAndFixText(strWO_LName, blASCIIToHTML, blHTMLToASCII, blIsSQL);
	    strSQL = "pr_UpdateWebOwnerAndProfile '" + strWO_EmplID + "', '";
	    strSQL += strWO_FName + "', '" + strWO_LName + "', '" + strWO_Email + "', '";
	    strSQL += strWO_Telephone + "', '" + strWO_Office + "', ";
	    strSQL += intWOP_ID + ", " + bitWOP_Primary;
        //strResultMsg += "UpdateOSOwner sql: " + strSQL + "<br />";
        //Run the query, which captures any error message in strResultMsg
        Int32 intResult = runOSSQLQuery(strSQL, blReturnIdentity);
        if (strResultMsg == "")
        {
            //Reset variable
            strResultMsg += "Your Owner was successfully updated!<br />";
        }
        else
        {
            strResultMsg += "No Owner rows were affected.<br />";
            strResultMsg += "Update Owner SQL: " + strSQL + "<br />";
        }

        return strResultMsg;
    }

    /// <summary>
    /// Deletes a survey owner plus the basic profile
    /// </summary>
    /// <param name="strEmplID"></param>
    /// <param name="intWOP_ID"></param>
    /// <returns>Result or error messages</returns>
    public String DeleteOSOwner(string strEmplID, Int32 intWOP_ID)
    {
        string strSQL = "pr_DeleteOneWebOwnerAndProfile '" + strEmplID + "', " + intWOP_ID;
        blReturnIdentity = false;
        //Run the query, which captures any error message in strResultMsg
        Int32 intResult = runOSSQLQuery(strSQL, blReturnIdentity);
        if(intResult == 1)
        {
            //Reset variable
            strResultMsg += "Your Owner was successfully deleted!<br />";
        }
        else
        {
            strResultMsg += "No Owner rows were affected.<br />";
            strResultMsg += "Delete Profile Owner SQL: " + strSQL + "<br />";
        }

        return strResultMsg;
    }

    /// <summary>
    /// Get WebOwner ID of primary Profile Owner
    /// </summary>
    /// <param name="intSurveyID"></param>
    /// <returns>Int32 rows affected</returns>
    public Int32 LookupPrimaryOwner(Int32 intSurveyID)
    {
        Int32 intResult = 0;
        string strSQL = "pr_LookupPrimaryOwner";
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCommand = new SqlCommand();
        SqlParameter objParam = new SqlParameter();
        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSOnlineSurvey"].ToString();
        try
        {
            objConn.Open();
            objCommand.CommandText = strSQL;
            objCommand.CommandType = CommandType.StoredProcedure;
            objCommand.Connection = objConn;
            objParam.ParameterName = "@ID";
            objParam.SqlDbType = SqlDbType.Int;
            objParam.Value = intSurveyID;
            objCommand.Parameters.Add(objParam);

            objParam = new SqlParameter();
            objParam.ParameterName = "@OwnerID";
            objParam.SqlDbType = SqlDbType.VarChar;
            objParam.Size = 100;
            objParam.Direction = ParameterDirection.Output;
            objCommand.Parameters.Add(objParam);
            objCommand.ExecuteReader();
            intResult = Convert.ToInt32(objCommand.Parameters["@OwnerID"].Value);
        }
        catch
        {
            return intResult;
        }
        finally
        {
            objConn.Close();
            objCommand.Dispose();
        }
        return intResult;
    }


    /// <summary>
    /// Delete all owners by Profile ID
    /// </summary>
    /// <param name="intProfileID"></param>
    /// <returns>Result or error messages</returns>
    public String DeleteOSProfileOwners(string intProfileID)
    {
        string strSQL = "pr_DeleteWebOwners_Profiles " + intProfileID;
        blReturnIdentity = false;
        //strResultMsg += "SQL: " + strSQL + "<br />"
        //Run the query, which captures any error message in strResultMsg
        Int32 intResult = runOSSQLQuery(strSQL, blReturnIdentity);
        if(intResult == 1)
        {
            //Reset variable
            strResultMsg += "Your Owner was successfully deleted!<br />";
        }
        else
        {
            strResultMsg += "No Owner rows were affected.<br />";
            strResultMsg += "Delete Profile Owner SQL: " + strSQL + "<br />";
       }
        return strResultMsg;
    }

    /// <summary>
    /// Create question and settings
    /// </summary>
    /// <param name="intSurveyID"></param>
    /// <param name="strQuestion"></param>
    /// <param name="strCorrectText"></param>
    /// <param name="strIncorrectText"></param>
    /// <param name="intOptionCount"></param>
    /// <param name="intQuestionType"></param>
    /// <param name="intQSequence"></param>
    /// <param name="intValidateType"></param>
    /// <param name="blRequiredField"></param>
    /// <param name="blVertAlign"></param>
    /// <param name="intMaskType"></param>
    /// <param name="intMaxLength"></param>
    /// <param name="blDisplayCountdown"></param>
    /// <param name="blIsFieldGroup"></param>
    /// <returns>Result or error messages</returns>
    public Int32 CreateQuestion(string intSurveyID, string strQuestion, string strCorrectText, string strIncorrectText, string intOptionCount, string intQuestionType, string intQSequence, string intValidateType, string bitRequiredField, string bitVertAlign, string intMaskType, string intMaxLength, string bitDisplayCountdown, string bitIsFieldGroup, string intDropDownListType)
    {
	    string strSQL = "";
        blReturnIdentity = true;
        blIsSQL = true;
        blASCIIToHTML = false;
        blHTMLToASCII = false; Utility Fixer = new Utility();
        strQuestion = Fixer.ConvertAndFixText(strQuestion, blASCIIToHTML, blHTMLToASCII, blIsSQL);
        strCorrectText = Fixer.ConvertAndFixText(strCorrectText, blASCIIToHTML, blHTMLToASCII, blIsSQL);
        strIncorrectText = Fixer.ConvertAndFixText(strIncorrectText, blASCIIToHTML, blHTMLToASCII, blIsSQL);
	    strSQL = "pr_InsertWebSurvey_Questions ";
	    strSQL += intSurveyID + ", '" + strQuestion + "', '" + strCorrectText + "', '" + strIncorrectText + "', " + intOptionCount + ", ";
        strSQL += intQuestionType + ", " + intQSequence + ", " + intValidateType + ", " + bitRequiredField + ", " + bitVertAlign + ", " + intMaskType + ", " + intMaxLength + ", " + bitDisplayCountdown + ", " + bitIsFieldGroup + ", " + intDropDownListType;
        //Run the query, which captures any error message in strResultMsg
        intNewIdentity = 0;
        intNewIdentity = runOSSQLQuery(strSQL, blReturnIdentity);
        if (intNewIdentity > 0)
        {
            //strResultMsg += "Your question was added successfully!<br />";
        }
        else
        {
            strResultMsg += "Error CreateQuestion:  " + strSQL + "<br />";

        }
        return intNewIdentity;
    }
        
    /// <summary>
    /// Update question and settings
    /// </summary>
    /// <param name="intSurveyID"></param>
    /// <param name="intQID"></param>
    /// <param name="strQuestion"></param>
    /// <param name="strCorrectText"></param>
    /// <param name="strIncorrectText"></param>
    /// <param name="intOptionCount"></param>
    /// <param name="intQuestionType"></param>
    /// <param name="intQSequence"></param>
    /// <param name="intValidateType"></param>
    /// <param name="blRequiredField"></param>
    /// <param name="blVertAlign"></param>
    /// <param name="intMaskType"></param>
    /// <param name="intMaxLength"></param>
    /// <param name="blDisplayCountdown"></param>
    /// <param name="blIsFieldGroup"></param>
    /// <returns>Result or error messages</returns>
    public String EditQuestion(string intSurveyID, Int32 intQID, string strQuestion, string strCorrectText, string strIncorrectText, Int32 intOptionCount, Int32 intQuestionType, Int32 intQSequence, Int32 intValidateType, string bitRequiredField, string bitVertAlign, string intMaskType, string intMaxLength, string bitDisplayCountdown, string bitIsFieldGroup, string intDropDownListType)
	{
        string strSQL = "";
        blReturnIdentity = false;
        blIsSQL = true;
        blASCIIToHTML = false;
        blHTMLToASCII = false; 
        Utility Fixer = new Utility();
        strQuestion = Fixer.ConvertAndFixText(strQuestion, blASCIIToHTML, blHTMLToASCII, blIsSQL);
        strCorrectText = Fixer.ConvertAndFixText(strCorrectText, blASCIIToHTML, blHTMLToASCII, blIsSQL);
        strIncorrectText = Fixer.ConvertAndFixText(strIncorrectText, blASCIIToHTML, blHTMLToASCII, blIsSQL);
	    strSQL = "pr_UpdateWebSurvey_Questions ";
	    strSQL += intSurveyID + ", " + intQID + ", '" + strQuestion + "', '" + strCorrectText + "', '" + strIncorrectText + "', " + intOptionCount + ", " ;
        strSQL += intQuestionType + ", " + intQSequence + ", " + intValidateType + ", " + bitRequiredField + ", " + bitVertAlign + ", " + intMaskType + ", " + intMaxLength + ", " + bitDisplayCountdown + ", " + bitIsFieldGroup + ", " + intDropDownListType;
        //Run the query, which captures any error message in strResultMsg
        Int32 intResult = runOSSQLQuery(strSQL, blReturnIdentity);
        if (intResult != 1)
        //{
            //Reset variable
        //    strResultMsg += "Your Question was updated successfully!<br />";
        //}
        //else
        {
            strResultMsg += "No Question rows were affected.<br />";
            strResultMsg += "Edit Question SQL: " + strSQL + "<br />";
        }
        return strResultMsg;
    }

    /// <summary>
    /// Delete a question and its options by Survey and Question ID.  Why queries need Survey ID is beyond me
    /// </summary>
    /// <param name="intSurveyID"></param>
    /// <param name="intQID"></param>
    /// <returns>Result or error messages</returns>
    public String DeleteSurveyQuestion(string intSurveyID, Int32 intQID)
    {
        //Delete dependent Options
        string strSQL = "pr_DeleteAllQuestionOptions " + intSurveyID + ", " + intQID;
        blReturnIdentity = false;
        //Run the query, which captures any error message in strResultMsg
        Int32 intResult = runOSSQLQuery(strSQL, blReturnIdentity);

        //Delete Question
        strSQL = "pr_DeleteOneSurveyQuestion " + intSurveyID + ", " + intQID;
        //Run the query, which captures any error message in strResultMsg
        intResult = runOSSQLQuery(strSQL, blReturnIdentity);
        if(intResult == 1)
        {
            //strResultMsg += "Your Question was deleted successfully!<br />";
        }
        else
        {
            strResultMsg += "No Question rows were affected.<br />";
        }
        return strResultMsg;
    }

    /// <summary>
    /// Update Sequence by Survey and Question ID
    /// </summary>
    /// <param name="intSurveyID"></param>
    /// <param name="intQID"></param>
    /// <param name="intQSequence"></param>
    /// <returns>Result or error messages</returns>
    public String UpdateQuestionOrder(Int32 intSurveyID, Int32 intQID, Int32 intQSequence)
    {
        string strSQL = "pr_UpdateSurveyQuestionOrder " + intSurveyID + ", " + intQID + ", " + intQSequence;
        blReturnIdentity = false;
        //Run the query, which captures any error message in strResultMsg
        Int32 intResult = runOSSQLQuery(strSQL, blReturnIdentity);
        if (intResult == 1)
        {
            //Reset variable
            strResultMsg = "Question " + intQSequence + " was re-ordered successfully!<br />";
        }
        else
        {
            strResultMsg += "No Question rows were affected.<br />";
            strResultMsg += "Update Question Order SQL: " + strSQL + "<br />";
        }
        return strResultMsg;
    }

    /// <summary>
    /// Create option and settings
    /// </summary>
    /// <param name="intSurveyID"></param>
    /// <param name="intQID"></param>
    /// <param name="blFillIn"></param>
    /// <param name="strDescription"></param>
    /// <param name="blAnswer"></param>
    /// <param name="intOSequence"></param>
    /// <param name="intOptionGroupID"></param>
    /// <param name="intOptionTemplate"></param>
    /// <returns>Result or error messages</returns>
    public String CreateOption(string intSurveyID, Int32 intQID, int bitFillIn, string strDescription, string bitAnswer, string intOSequence, Int32 intOptionGroupID, Int32 intOptionTemplate)
	{
        string strSQL = "";
        blReturnIdentity = true;
        blIsSQL = true;
        blASCIIToHTML = false;
        blHTMLToASCII = false; 
        Utility Fixer = new Utility();
        strDescription = Fixer.ConvertAndFixText(strDescription, blASCIIToHTML, blHTMLToASCII, blIsSQL);
	    strSQL = "pr_InsertWebSurvey_Options ";
        strSQL = strSQL + intSurveyID + ", " + intQID + ", " + bitFillIn + ", '";
        strSQL = strSQL + strDescription + "', '" + bitAnswer + "', " + intOSequence + ", " + intOptionGroupID + ", " + intOptionTemplate;
        //Run the query, which captures any error message in strResultMsg
        intNewIdentity = 0;
        intNewIdentity = runOSSQLQuery(strSQL, blReturnIdentity);
        if (intNewIdentity > 0)
        {
            //strResultMsg += "Your Option was added successfully!<br />";
        }
        else
        {
            strResultMsg += "Error CreateOption: " + strSQL + "<br />";
        }
        return strResultMsg;

    }

    /// <summary>
    /// Update option and settings. bitAnswer expects "yes" or "no"
    /// </summary>
    /// <param name="intSurveyID"></param>
    /// <param name="intQID"></param>
    /// <param name="intOptionID"></param>
    /// <param name="blFillIn"></param>
    /// <param name="strDescription"></param>
    /// <param name="bitAnswer"></param>
    /// <param name="intOSequence"></param>
    /// <returns>Result or error messages</returns>
    public String EditOption(Int32 intSurveyID, Int32 intQID, Int32 intOptionID, int bitFillIn, string strDescription, string bitAnswer, Int32 intOSequence)
    {
        string strSQL = "";
        blReturnIdentity = false;
        blIsSQL = true;
        blASCIIToHTML = false;
        blHTMLToASCII = false; Utility Fixer = new Utility();
        strDescription = Fixer.ConvertAndFixText(strDescription, blASCIIToHTML, blHTMLToASCII, blIsSQL);
	    strSQL = "pr_UpdateWebSurvey_Options ";
	    strSQL += intSurveyID + ", " + intQID + ", " + intOptionID + ", " + bitFillIn + ", '";
	    strSQL += strDescription + "', '" + bitAnswer + "', " + intOSequence;
        //Run the query, which captures any error message in strResultMsg
        Int32 intResult = runOSSQLQuery(strSQL, blReturnIdentity);
        if(intResult == 1)
        {
            //strResultMsg += "Your Option was updated successfully!<br />";
        }
        else
        {
            strResultMsg += "No Option rows were affected.<br />";
            strResultMsg += "Edit Option SQL: " + strSQL + "<br />";
       }
        return strResultMsg;
    }

    /// <summary>
    /// Delete one option by Survey ID, Question ID and Sequence 
    /// </summary>
    /// <param name="intSurveyID"></param>
    /// <param name="intQID"></param>
    /// <param name="intOSequence"></param>
    /// <returns>Result or error messages</returns>
    public String DeleteOneOption(Int32 intSurveyID, Int32 intQID, Int32 intOSequence)
    {
        string strSQL = "";
        blReturnIdentity = false;
        strSQL = "pr_DeleteOneSurveyQuestionOption " + intSurveyID + ", " + intQID + ", " + intOSequence;
        //Run the query, which captures any error message in strResultMsg
        Int32 intResult = runOSSQLQuery(strSQL, blReturnIdentity);
        if(intResult == 1)
        {
            //Reset variable
            strResultMsg += "Your Option was successfully deleted!<br />";
        }
        else
        {
            strResultMsg += "No Option rows were affected.<br />";
            strResultMsg += "Delete One Option SQL: " + strSQL + "<br />";
        }
        return strResultMsg;    
    }

    /// <summary>
    /// Update OptionGroupID field in Survey
    /// </summary>
    /// <param name="intSurveyID"></param>
    /// <param name="intOptionGroupID"></param>
    /// <returns>Result or error messages</returns>
    public String UpdateWebSurveyOptionGroupID(Int32 intSurveyID, Int32 intOptionGroupID)
    {
        string strSQL = "";
        blReturnIdentity = false;
        strSQL = "pr_UpdateWebSurveyOptionGroupID " + intSurveyID + ", " + intOptionGroupID;
        //Run the query, which captures any error message in strResultMsg
        Int32 intResult = runOSSQLQuery(strSQL, blReturnIdentity);
        if(intResult == 1)
        {
            //Reset variable
            strResultMsg += "Your Survey was added successfully!<br />";
        }
        else
        {
            strResultMsg += "No Survey rows were affected.<br />";
            strResultMsg += "Update Survey Option GroupID SQL: " + strSQL + "<br />";
        }
        return strResultMsg;
    }

    /// <summary>
    /// Open a recordset and retrieve the Max Option Group ID
    /// </summary>
    /// <returns>Max Option Group ID</returns>
    public Int32 GetMaxOptionGroupID()
    {
        Int32 intMaxID = 0;
        string strSQL = "pr_GetMaxOptionGroupID";
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCommand = new SqlCommand();
        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSOnlineSurvey"].ToString();
        SqlDataAdapter objDataAdapter;
        DataSet ds = new DataSet();
        try
        {
            objConn.Open();
            objCommand.CommandText = strSQL;
            objCommand.CommandType = CommandType.StoredProcedure;
            objCommand.Connection = objConn;
            objDataAdapter = new SqlDataAdapter(objCommand);
            objDataAdapter.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dtRow in ds.Tables[0].Rows)
                {
                    intMaxID = Convert.ToInt32(dtRow["MaxOfWS_OptionGroupID"]);
                }
            }
        }
        catch
        {
            return intMaxID;
        }
        finally
        {
            objConn.Close();
            objCommand.Dispose();
        }
        return intMaxID;
   }

    /// <summary>
    /// Get OptionGroupID for survey question option cloning
    /// </summary>
    /// <param name="intSurveyID"></param>
    /// <returns>Int32 Option Group ID</returns>
    public Int32 LookupOptionGroupID(Int32 intSurveyID)
    {
        Int32 intResult = 0;
        string strSQL = "pr_LookupOptionGroupID";
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCommand = new SqlCommand();
        SqlParameter objParam = new SqlParameter();
        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSOnlineSurvey"].ToString();
        try
        {
            objConn.Open();
            objCommand.CommandText = strSQL;
            objCommand.CommandType = CommandType.StoredProcedure;
            objCommand.Connection = objConn;
            objParam.ParameterName = "@SurveyID";
            objParam.SqlDbType = SqlDbType.Int;
            objParam.Value = intSurveyID;
            objCommand.Parameters.Add(objParam);

            objParam = new SqlParameter();
            objParam.ParameterName = "@OptionGroupID";
            objParam.SqlDbType = SqlDbType.Int;
            objParam.Direction = ParameterDirection.Output;
            objCommand.Parameters.Add(objParam);
            objCommand.ExecuteReader();
            intResult = Convert.ToInt32(objCommand.Parameters["@OptionGroupID"].Value);
        }
        catch
        {
            return intResult;
        }
        finally
        {
            objConn.Close();
            objCommand.Dispose();
        }
        return intResult;
    }


    /// <summary>
    /// Get OptionTemplateID for option editing
    /// </summary>
    /// <param name="intQuestionID"></param>
    /// <returns>Int32 Option Template ID</returns>
    public Int32 LookupOptionTemplateID(Int32 intQuestionID)
    {
        Int32 intResult = 0;
        string strSQL = "pr_LookupOptionTemplateID";
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCommand = new SqlCommand();
        SqlParameter objParam = new SqlParameter();
        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSOnlineSurvey"].ToString();
        try
        {
            objConn.Open();
            objCommand.CommandText = strSQL;
            objCommand.CommandType = CommandType.StoredProcedure;
            objCommand.Connection = objConn;
            objParam.ParameterName = "@QuestionID";
            objParam.SqlDbType = SqlDbType.Int;
            objParam.Value = intSurveyID;
            objCommand.Parameters.Add(objParam);

            objParam = new SqlParameter();
            objParam.ParameterName = "@OptionTemplateID";
            objParam.SqlDbType = SqlDbType.Int;
            objParam.Direction = ParameterDirection.Output;
            objCommand.Parameters.Add(objParam);
            objCommand.ExecuteReader();
            intResult = Convert.ToInt32(objCommand.Parameters["@OptionTemplateID"].Value);
        }
        catch
        {
            return intResult;
        }
        finally
        {
            objConn.Close();
            objCommand.Dispose();
        }
        return intResult;
    }


    /// <summary>
    /// Create a Tally or Schedule with settings
    /// </summary>
    /// <param name="intSurveyID"></param>
    /// <param name="strDateOpen"></param>
    /// <param name="strDateClosed"></param>
    /// <param name="intDisplayPage"></param>
    /// <param name="strDisplaySite"></param>
    /// <param name="blIsPerpetual"></param>
    /// <returns>Result or error messages</returns>
    public Int32 CreateTally(Int32 intSurveyID, string strDateOpen, string strDateClosed, Int32 intDisplayPage, string strDisplaySite, int bitIsPerpetual) 
    {
	    string strSQL = "";
        blReturnIdentity = true;
	    if (strDateOpen == "")
        {
		    strDateOpen = System.DateTime.Today.ToShortDateString();
	    }
	    if (strDateClosed == "" )
        {
            strDateClosed = System.DateTime.Today.ToShortDateString();
        }
	    //if intDisplayPage = "" then
		//    intDisplayPage = 0
	    //end if
	    strSQL = "pr_InsertWebSurvey_Tally ";
	    strSQL += intSurveyID + ", '" + strDateOpen + "', '" + strDateClosed + "', " ;
        strSQL += intDisplayPage + ", '" + strDisplaySite + "', " + bitIsPerpetual;
        //Run the query, which captures any error message in strResultMsg
        intNewIdentity = runOSSQLQuery(strSQL, blReturnIdentity);
        if(intNewIdentity > 0)
        {
            //Reset variable
            strResultMsg += "Your Schedule was added successfully!<br />";
        }
        else
        {
            strResultMsg += "No new Tally ID created.<br />";
            strResultMsg += "CreateTally SQL: " + strSQL + "<br />";
        }
        return intNewIdentity;
    }


    /// <summary>
    /// Changes dates and/or toggle perpetual setting
    /// </summary>
    /// <param name="intTallyID"></param>
    /// <param name="strDateOpen"></param>
    /// <param name="strDateClosed"></param>
    /// <param name="intDisplayPage"></param>
    /// <param name="strDisplaySite"></param>
    /// <param name="bitIsPerpetual"></param>
    /// <returns>String result message</returns>
    public String EditTally(Int32 intTallyID, string strDateOpen, string strDateClosed, Int32 intDisplayPage, string strDisplaySite, int bitIsPerpetual) 
    {
	    string strSQL = "";
        blReturnIdentity = false;
	    if (strDateOpen == "")
        {
		    strDateOpen = System.DateTime.Today.ToShortDateString();
	    }
	    if (strDateClosed == "" )
        {
            strDateClosed = System.DateTime.Today.ToShortDateString();
        }
        strSQL = "pr_UpdateWebSurvey_Tally ";
	    strSQL += intTallyID + ", '" + strDateOpen + "', '" + strDateClosed + "', " ;
	    strSQL += intDisplayPage + ", '" + strDisplaySite + "', " + bitIsPerpetual;
	
        //Run the query, which captures any error message in strResultMsg
        Int32 intResult = runOSSQLQuery(strSQL, blReturnIdentity);
        if(intResult == 1)
        {
            //Reset variable
            strResultMsg += "Your Schedule was changed successfully!<br />";
        }
        else
        {
            strResultMsg += "No Tally rows were affected.<br />";
            strResultMsg += "EditTally SQL: " + strSQL + "<br />";
        }
        return strResultMsg;
    }


    /// <summary>
    /// Copy a profile and its questions/fields ready for editing
    /// </summary>
    /// <param name="intSurveyID"></param>
    /// <param name="strWO_FName"></param>
    /// <param name="strWO_LName"></param>
    /// <param name="strWO_EmplID"></param>
    /// <param name="strWO_Email"></param>
    /// <param name="strWO_Telephone"></param>
    /// <param name="strWO_Office"></param>
    /// <returns>Copy of Profile, Questions, Options in database</returns>
    public Boolean CloneSurvey(Int32 intSurveyID, string strWO_FName, string strWO_LName, string strWO_EmplID, string strWO_Email, string strWO_Telephone, string strWO_Office)
    {
        Int32 intNewSurveyID = 0;
        Int32 intNewQID = 0;
        bool blReturn = false;
        string strMaxLength = "";
        string strOSequence = "";
        string strAnswer = "";
        string strFillIn = "";
        StringBuilder myMsg = new StringBuilder();

        //CloneSurvey = False
        try
        {
            DataTable dt = GetOneSurvey(intSurveyID.ToString());
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dtRow in dt.Rows)
                {
                    strCreateDate = System.DateTime.Today.ToShortDateString();
                    strSurveyTitle = dtRow["WS_SurveyTitle"].ToString() + " - COPY";
                    strSurveyPurpose = dtRow["WS_SurveyPurpose"].ToString();
                    strSurveyInstructions = dtRow["WS_SurveyInstructions"].ToString();
                    intSurveyType = Convert.ToInt32(dtRow["WS_SurveyType"]);
                    intQuestionCount = Convert.ToInt32(dtRow["WS_QuestionCount"]);
                    strSurveyPassword = dtRow["WS_SurveyPassword"].ToString();
                    bitAnonymous = Convert.ToInt32(dtRow["WS_Anonymous"]);
                    //if bitAnonymous = -1 then blAnonymous = 1;
                    //strOwnerEmail = dtRow["WS_OwnerEmail"];
                    bitUserNotification = Convert.ToInt32(dtRow["WS_UserNotification"]);
                    //if bitUserNotification = -1 then blUserNotification = 1;
                    bitClone = Convert.ToInt32(dtRow["WS_Clone"]);
                    //if blClone = -1 then blClone = 1;
                    strEmail1 = dtRow["WS_Email1"].ToString();
                    strEmail2 = dtRow["WS_Email2"].ToString();
                    strEmail3 = dtRow["WS_Email3"].ToString();
                    bitSupressEmail = Convert.ToInt32(dtRow["WS_SupressEmail"]);
                    //if bitSupressEmail = -1 then blSupressEmail = 1;
                    bitUseAltEmail = Convert.ToInt32(dtRow["WS_UseAltEmail"]);
                    //if bitUseAltEmail = -1 then blUseAltEmail = 1;
                    bitUseDefaultFrom = Convert.ToInt32(dtRow["WS_UseDefaultFrom"]);
                    //if bitUseDefaultFrom = -1 then bitUseAltEmail = 1;
                    intFormType = Convert.ToInt32(dtRow["WS_FormType"]);
                    bitOtherNotification = Convert.ToInt32(dtRow["WS_OtherNotification"]);
                    //if bitUserNotification = -1 then bitUserNotification = 1;
                    intOtherEmailCount = Convert.ToInt32(dtRow["WS_OtherEmailCount"]);
                    //bitStudentEmailOption = Request.Form["optStudentEmailOption"];
                    bitStudentEmailOption = Convert.ToInt32(dtRow["WS_StudentEmailOption"]);
                    //if bitStudentEmailOption = "" then bitStudentEmailOption = 0;
                    bitDisplaySummaryOption = Convert.ToInt32(dtRow["WS_DisplaySummaryOption"]);
                    //if bitDisplaySummaryOption = -1 then bitDisplaySummaryOption = 1;
                    bitSubmitOnce = Convert.ToInt32(dtRow["WS_SubmitOnce"]);
                    //if bitSubmitOnce = -1 then bitSubmitOnce = 1;
                    strCustomMessage = dtRow["WS_CustomMessage"].ToString();
                    bitNumberFields = Convert.ToInt32(dtRow["WS_NumberFields"]);
                    //if bitNumberFields = -1 then blNumberFields = 1;
                    intEmailResultsOption = Convert.ToInt32(dtRow["WS_EmailResultsOption"]);
                    bitRequireLogin = Convert.ToInt32(dtRow["WS_RequireLogin"]);
                    bitDisplaySurveyResults = Convert.ToInt32(dtRow["WS_DisplaySurveyResults"]);
                }

                //Reset variable
                intNewIdentity = 0;
                blASCIIToHTML = false;
                //myMsg.Append( "CreateSurvey ");
                intNewIdentity = CreateSurvey(strCreateDate, strSurveyTitle, strSurveyPurpose, strSurveyInstructions, intSurveyType.ToString(), intQuestionCount,
                    strSurveyPassword, bitAnonymous.ToString(), bitUserNotification.ToString(), bitClone.ToString(), strEmail1, strEmail2, strEmail3, bitSupressEmail.ToString(),
                    bitUseAltEmail.ToString(), bitUseDefaultFrom.ToString(), intFormType.ToString(), bitOtherNotification.ToString(), intOtherEmailCount, bitStudentEmailOption.ToString(), bitDisplaySummaryOption.ToString(), bitSubmitOnce.ToString(),
                    strCustomMessage, bitNumberFields.ToString(), intEmailResultsOption.ToString(), bitRequireLogin.ToString(), bitDisplaySurveyResults.ToString());
                //myMsg.Append(intNewIdentity.ToString() + "<br />");
                if (blSuccess)
                {
                    intNewSurveyID = intNewIdentity;
                    if (intNewSurveyID > 0)
                    {
                        //Save the new Survey ID 
                        HttpContext.Current.Session["SurveyID"] = intNewSurveyID;
                        //Reset variable
                        intNewIdentity = 0;
                        strResultMsg = InsertOSOwner(strWO_EmplID, strWO_FName, strWO_LName, strWO_Email, strWO_Telephone, strWO_Office, intNewSurveyID, 1, true);
                        //myMsg.Append(strResultMsg + "<br />");
                        // intNewIdentity set in InsertOSOwner 
                        if (intNewIdentity > 0)
                        {
                            //myMsg.Append("Your new Profile was added successfully!<br />");
                            DataTable dtQ = GetAllSurveyQuestions(intSurveyID);
                            if (dtQ.Rows.Count > 0)
                            {
                                foreach (DataRow dtRowQ in dtQ.Rows)
                                {
                                    intQID = Convert.ToInt32(dtRowQ["WSQ_QID"]);
                                    strQuestion = dtRowQ["WSQ_Question"].ToString();
                                    strCorrectText = dtRowQ["WSQ_CorrectText"].ToString();
                                    strIncorrectText = dtRowQ["WSQ_IncorrectText"].ToString();
                                    intOptionCount = Convert.ToInt32(dtRowQ["WSQ_OptionCount"]);
                                    intQuestionType = Convert.ToInt32(dtRowQ["WSQ_QuestionType"]);
                                    intQSequence = Convert.ToInt32(dtRowQ["WSQ_Sequence"]);
                                    intValidateType = Convert.ToInt32(dtRowQ["WSQ_ValidateType"]);
                                    bitRequiredField = Convert.ToInt32(dtRowQ["WSQ_RequiredField"]);
                                    bitVertAlign = Convert.ToInt32(dtRowQ["WSQ_VertAlign"]);
                                    intMaskType = Convert.ToInt32(dtRowQ["WSQ_MaskType"]);
                                    if (DBNull.Value.Equals(dtRowQ["WSQ_MaxLength"]))
                                    {
                                        strMaxLength = "0";
                                    }
                                    else
                                    {
                                        strMaxLength = dtRowQ["WSQ_MaxLength"].ToString();
                                    }
                                    bitDisplayCountdown = Convert.ToInt32(dtRowQ["WSQ_DisplayCountdown"]);
                                    bitIsFieldGroup = Convert.ToInt32(dtRowQ["WSQ_IsFieldGroup"]);
                                    intDropDownListType = Convert.ToInt32(dtRowQ["WSQ_DropDownListType"]);
                                    //Reset variable
                                    intNewIdentity = 0;
                                    blASCIIToHTML = false;
                                    //myMsg.Append(" CreateQuestion ");
                                    intNewQID = CreateQuestion(intNewSurveyID.ToString(), strQuestion, strCorrectText, strIncorrectText, intOptionCount.ToString(), intQuestionType.ToString(), intQSequence.ToString(), intValidateType.ToString(), bitRequiredField.ToString(), bitVertAlign.ToString(), intMaskType.ToString(), strMaxLength, bitDisplayCountdown.ToString(), bitIsFieldGroup.ToString(), intDropDownListType.ToString());
                                    //myMsg.Append(intNewQID.ToString() + "<br />");
                                    //intNewQID = intNewIdentity;
                                    //strResultMsg += "QID: " + intNewQID + ".<br />"
                                    if (intNewQID > 0)
                                    {
                                        DataTable dtO = GetSurveyQuestionOptions(intSurveyID, intQID);
                                        if (dtO.Rows.Count > 0)
                                        {
                                            foreach (DataRow dtRowO in dtO.Rows)
                                            {
                                                strDescription = "";
                                                intOSequence = 0;
                                                strFillIn = dtRowO["WSO_FillIn"].ToString();
                                                if (strFillIn == "True") { strFillIn = "1"; }
                                                if (strFillIn == "False") { strFillIn = "0"; }
                                                //strResultMsg += "data is numeric: " + blFillIn + "<br />"
                                                strDescription = dtRowO["WSO_Description"].ToString();
                                                strAnswer = dtRowO["WSO_Answer"].ToString();
                                                //SP parameter expects a "Yes" or "No" value from drop-down lists
                                                if (strAnswer == "1" || strAnswer == "True") { strAnswer = "Yes"; }
                                                if (strAnswer == "0" || strAnswer == "False") { strAnswer = "No"; }
                                                if (DBNull.Value.Equals(dtRowO["WSO_Sequence"]))
                                                {
                                                    strOSequence = "0";
                                                }
                                                else
                                                {
                                                    strOSequence = dtRowO["WSO_Sequence"].ToString();
                                                }
                                                //Cannot copy Option Group ID - unique to each survey
                                                intOptionGroupID = 0;
                                                intOptionTemplate = 0;
                                                //CreateOption(intSurveyID, intQID, blFillIn, strDescription, blAnswer, intOSequence, intOptionGroupID, intOptionTemplate)
                                                //myMsg.Append(" CreateOption ");
                                                strResultMsg = CreateOption(intNewSurveyID.ToString(), intNewQID, Convert.ToInt32(strFillIn), strDescription, strAnswer, strOSequence, intOptionGroupID, intOptionTemplate);
                                                //myMsg.Append(strResultMsg + "<br />");
                                                //strResultMsg += "Move Next<br />"	
                                            }
                                        }
                                        else
                                        {
                                            myMsg.Append("Problem retrieving Option records for QID " + intQID.ToString() + ".<br />");
                                        }   //Options
                                    }
                                    else
                                    {
                                        myMsg.Append("Problem inserting new Question for QID " + intQID.ToString() + ".<br />");
                                    }
                                }   //foreach Q
                                //If we get to this line, Success
                                blReturn = true;
                            }
                            else
                            {
                                myMsg.Append("Problem retrieving Question records.<br />");
                            }   //Questions
                        }
                        else
                        {
                            myMsg.Append("Unable to insert owner for SurveyID: " + Convert.ToString(intNewSurveyID) + ".  Please <a href=\"mailto:ITSupportCenter@ccs.spokane.edu\">contact the IT Support Center</a>.<br />");
                        }    //Owner
                    }   //Retrive new Profile ID
                }   //Profile
                else
                {
                    myMsg.Append("Problem inserting new Profile " + Convert.ToString(intSurveyID) + ".  Please <a href=\"mailto:ITSupportCenter@ccs.spokane.edu\">contact the IT Support Center</a>.<br />");
                }
            }
            else
            {
                myMsg.Append("Problem retrieving Profile records.<br />");
            }
        }
        catch (Exception ex)
        {
            myMsg.Append("Error: " + ex.Message);
        }
        strResultMsg = myMsg.ToString();
        return blReturn;
    }

    /// <summary>
    /// Get the number of options in an option group by ID
    /// </summary>
    /// <param name="intOptionGroupID"></param>
    /// <returns>Int32 count</returns>
    public Int32 CountOptionsByOptionGroupID(Int32 intOptionGroupID)
    {
        Int32 intResult = 0;
        string strSQL = "pr_CountOptionsByGroupID " + intOptionGroupID;
        //strResultMsg += "CountOptionsByOptionGroupID: " + strSQL + "<br />";
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCommand = new SqlCommand();
        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSOnlineSurvey"].ToString();
        SqlDataAdapter objDataAdapter;
        DataSet ds = new DataSet();
        try
        {
            objConn.Open();
            objCommand.CommandText = strSQL;
            objCommand.CommandType = CommandType.Text;
            objCommand.Connection = objConn;
            objDataAdapter = new SqlDataAdapter(objCommand);
            objDataAdapter.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                intResult = Convert.ToInt32(ds.Tables[0].Rows[0]["OptionGroupCount"]);
            }
        }
        catch (Exception ex)
        {
            intQuestionCount = 0;
            strResultMsg = ex.Message;
        }
        finally
        {
            objConn.Close();
            objCommand.Dispose();
        }
        return intResult;
    }


    /// <summary>
    /// Creates HTML rows to re-order sequencing of questions/fields
    /// </summary>
    /// <param name="intSurveyID"></param>
    /// <returns>String of rows for a table</returns>
    public String ChangeQuestionOrder(Int32 intSurveyID)
    {
        String strResult = "";
        Int32 i = 0;
        DataTable dt = GetAllSurveyQuestions(intSurveyID);
        if (dt.Rows.Count > 0)
        {
            foreach (DataRow dtRow in dt.Rows)
            {
                i += 1;
	            strResult += "<tr><td>" + dtRow["WSQ_Question"].ToString() + "</td>";
	            strResult += "<td><input type=\"text\" size=\"5\" name=\"txtSequence" + i + "\" value=\"" + dtRow["WSQ_Sequence"].ToString() + "\">";
                strResult += "<input type=\"hidden\" name=\"hdnQID" + i + "\" value=\"" + dtRow["WSQ_QID"].ToString() + "\"></td></tr>" + System.Environment.NewLine;
	        }
	        //strResult += "</table>"
        }
        return strResult;
    }

    /// <summary>
    /// Get the question count field data from the Profile by ID
    /// </summary>
    /// <param name="intSurveyID"></param>
    /// <returns>Integer count</returns>
    public Int32 GetQuestionCount(Int32 intSurveyID)
    {
        Int32 intQuestionCount = 0;
        string strSQL = "pr_GetQuestionCount " + intSurveyID;
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCommand = new SqlCommand();
        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSOnlineSurvey"].ToString();
        SqlDataAdapter objDataAdapter;
        DataSet ds = new DataSet();
        if (intSurveyID != 0)
        {
            try
            {
                objConn.Open();
                objCommand.CommandText = strSQL;
                objCommand.CommandType = CommandType.Text;
                objCommand.Connection = objConn;
                objDataAdapter = new SqlDataAdapter(objCommand);
                objDataAdapter.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    intQuestionCount = Convert.ToInt32(ds.Tables[0].Rows[0]["WS_QuestionCount"]);
                }
            }
            catch (Exception ex)
            {
                intQuestionCount = 0;
                strResultMsg += ex.Message;
            }
            finally
            {
                objConn.Close();
                objCommand.Dispose();
            }
        }
        return intQuestionCount;

    }

    /// <summary>
    /// Returns the selected Question Type Description
    /// </summary>
    /// <param name="strSelected"></param>
    /// <returns>String description</returns>
    public String GetQuestionType(int intQuestionType)
    {
        String strResult = "";
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCommand = new SqlCommand();
        SqlParameter param = new SqlParameter();
        SqlDataAdapter objDataAdapter;
        DataSet ds = new DataSet();
        try
        {
            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSOnlineSurvey"].ToString();
            objConn.Open();
            objCommand.CommandText = "pr_GetWebSurvey_QuestionTypes";
            objCommand.CommandType = CommandType.StoredProcedure;
            objCommand.Connection = objConn;

            param.ParameterName = "@QuizType";
            param.SqlDbType = SqlDbType.Int;
            param.Value = 0;
            objCommand.Parameters.Add(param);

            objDataAdapter = new SqlDataAdapter(objCommand);
            objDataAdapter.Fill(ds);
            foreach (DataRow dtRow in ds.Tables[0].Rows)
            {
                if (Convert.ToInt32(intQuestionType) == Convert.ToInt32(dtRow.ItemArray[0]))
                {
                    strResult = dtRow.ItemArray[1].ToString();
                }
            }
        }
        finally
        {
            objConn.Close();
            objConn.Dispose();
            objCommand.Dispose();
        }
        return strResult;
    }
    
    /// <summary>
    /// Update Option count in WebSurvey_Questions table + 1
    /// </summary>
    /// <param name="intSurveyID"></param>
    /// <param name="intQID"></param>
    /// <returns>String error messages</returns>
    public string IncrementOptionCount(Int32 intSurveyID, Int32 intQID)
    {
        string strSQL = "pr_IncrementOptionCount " + intSurveyID + ", " + intQID;
        blReturnIdentity = false;
        Int32 intResult = runOSSQLQuery(strSQL, blReturnIdentity);
        if (intResult == 0)
        {
            strResultMsg += "No Question rows were affected.<br />";
            strResultMsg += "IncrementOptionCount SQL: " + strSQL + "<br />";
        }
        return strResultMsg;
    }

    /// <summary>
    /// Update Option count in WebSurvey_Questions table - 1
    /// </summary>
    /// <param name="intSurveyID"></param>
    /// <param name="intQID"></param>
    /// <returns>String error messages</returns>
    public string DecrementOptionCount(Int32 intSurveyID, Int32 intQID)
    {

        string strSQL = "pr_DecrementOptionCount " + intSurveyID + ", " + intQID;
        blReturnIdentity = false;
        Int32 intResult = runOSSQLQuery(strSQL, blReturnIdentity);
        if (intResult == 0)
        {
            strResultMsg += "No Question rows were affected.<br />";
            strResultMsg += "DecrementOptionCount SQL: " + strSQL + "<br />";
        }
        return strResultMsg;
    }

    /// <summary>
    /// Update Question count in WebSurvey_Questions table + 1
    /// </summary>
    /// <param name="intSurveyID"></param>
    /// <returns>String error messages</returns>
    public string IncrementQuestionCount(string intSurveyID)
    {
        String strSQL = "pr_IncrementQuestionCount " + intSurveyID;
        blReturnIdentity = false;
        Int32 intResult = runOSSQLQuery(strSQL, blReturnIdentity);
        if (intResult == 0)
        {
            strResultMsg += "No WebSurvey rows were affected.<br />";
            strResultMsg += "IncrementQuestionCount SQL: " + strSQL + "<br />";
        }
        return strResultMsg;
    }

    public string DecrementQuestionCount(Int32 intSurveyID)
    {
        string strSQL = "pr_DecrementQuestionCount " + intSurveyID;
        blReturnIdentity = false;
        Int32 intResult = runOSSQLQuery(strSQL, blReturnIdentity);
        if (intResult == 0)
        {
            strResultMsg += "No WebSurvey rows were affected.<br />";
            strResultMsg += "DecrementQuestionCount SQL: " + strSQL + "<br />";
        }
        return strResultMsg;
    }

    /// <summary>
    /// Update WebSurvey_Tally User count - 1
    /// </summary>
    /// <param name="intTallyID"></param>
    /// <returns>String error/success message</returns>
    public string DecrementTallyCount(Int32 intTallyID)
    {
        String strSQL = "pr_DecrementTallyCount " + intTallyID;
        blReturnIdentity = false;
        Int32 intResult = runOSSQLQuery(strSQL, blReturnIdentity);
        //strResultMsg += "DecrementTallyCount SQL: " + strSQL + "<br />";
        if (intResult == 0)
        {
            strResultMsg += "No WebSurvey_Tally rows were affected. SQL: " + strSQL + "<br />";
        }
        return strResultMsg;
    }

    /// <summary>
    /// Update WebSurvey_Tally User count + 1
    /// </summary>
    /// <param name="strTallyID"></param>
    /// <returns>String error/success message</returns>
    public String IncrementTallyUserCount(String strTallyID)
    {
        String strSQL = "pr_IncrementTallyUserCount " + strTallyID;
        blReturnIdentity = false;
        Int32 intResult = runOSSQLQuery(strSQL, blReturnIdentity);
        //strResultMsg += "IncrementTallyUserCount SQL: " + strSQL + "<br />";
        if (intResult == 1)
        {
            strSuccessMsg = "Success";
        }
        else
        {
            strResultMsg += "Error IncrementTallyUserCount: No WebSurvey_Tally rows were affected.<br />";
        }
        return strSuccessMsg;
    }


#endregion


#region "DROP-DOWN LISTS"

    /// <summary>
    /// Creates the drop-down list options for calling functions
    /// </summary>
    /// <param name="intValueArrayPosition"></param>
    /// <param name="intDescrArrayPosition"></param>
    /// <param name="strQuery"></param>
    /// <param name="strSelected"></param>
    /// <param name="blSelectOne"></param>
    /// <param name="blAddNew"></param>
    /// <param name="strObject"></param>
    /// <returns>string of options</returns>
    String CreateDropDownList(Int32 intValueArrayPosition, Int32 intDescrArrayPosition, String strQuery, String strSelected, bool blSelectOne, bool blAddNew, string strObject)
    {
        String strDDL = "";
        String strSelectTag = "";
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCommand = new SqlCommand();
        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSOnlineSurvey"].ToString();
        SqlDataAdapter objDataAdapter;
        DataSet ds = new DataSet();
        if (blSelectOne)
        {
            if (strSelected == "") { strSelectTag = " SELECTED"; } else { strSelectTag = ""; }
            strDDL += "<option value=\"\"" + strSelectTag + ">-- Select One --</option>" + System.Environment.NewLine;
        }
        if (blAddNew)
        {
            if (strSelected == "0") { strSelectTag = " SELECTED"; } else { strSelectTag = ""; }
            strDDL += "<option value=\"\"" + strSelectTag + ">-- Add New" + strObject + " --</option>" + System.Environment.NewLine;
        }

        try
        {
            objConn.Open();
            objCommand.CommandText = strQuery;
            objCommand.CommandType = CommandType.StoredProcedure;
            objCommand.Connection = objConn;
            objDataAdapter = new SqlDataAdapter(objCommand);
            objDataAdapter.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dtRow in ds.Tables[0].Rows)
                {
                    //Reset tag after each iteration
                    strSelectTag = "";
                    if (strSelected != "" && strSelected != "0")
                    {
                        if (strSelected == dtRow.ItemArray[intValueArrayPosition].ToString())
                        {
                            strSelectTag = " SELECTED";
                        }
                    }
                    strDDL += "<option value=\"" + dtRow.ItemArray[intValueArrayPosition].ToString() + "\"" + strSelectTag + ">" + dtRow.ItemArray[intDescrArrayPosition].ToString() + "</option>" + System.Environment.NewLine;
                }
            }
        }
        catch
        {
            return strDDL;
        }
        finally
        {
            objConn.Close();
            objCommand.Dispose();
        }
        return strDDL;
    }

    /// <summary>
    /// Creates the survey/profile drop-down list options for calling functions
    /// </summary>
    /// <param name="strQuery"></param>
    /// <param name="strSelSurvey"></param>
    /// <param name="blSelectOne"></param>
    /// <param name="blAddNew"></param>
    /// <param name="strProfileType"></param>
    /// <param name="blIsAdmin"></param>
    /// <returns>String of options</returns>
    String CreateSurveyDropDownList(String strQuery, String strSelSurvey, bool blSelectOne, bool blAddNew, string strProfileType, bool blIsAdmin)
    {
        //strResultMsg += "strQuery: " + strQuery + "<br />";
        String strDDL = "";
        String strSelectTag = "";
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCommand = new SqlCommand();
        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSOnlineSurvey"].ToString();
        SqlDataAdapter objDataAdapter;
        DataSet ds = new DataSet();
        if (strSelSurvey == "" && blSelectOne)
        {
	        strSelSurvey = "0";
        }
        if (blSelectOne)
        {
            if (strSelSurvey == "") { strSelectTag = " SELECTED"; } else { strSelectTag = ""; }
            strDDL += "<option value=\"\"" + strSelectTag + ">-- Select One --</option>" + System.Environment.NewLine;
        }
        if (blAddNew)
        {
            if (strSelSurvey == "0") { strSelectTag = " SELECTED"; } else { strSelectTag = ""; }
            strDDL += "<option value=\"0\"" + strSelectTag + ">-- Add New" + strProfileType + " --</option>" + System.Environment.NewLine;
        }

        try
        {
            string strTestSurveyID = "";
            objConn.Open();
            objCommand.CommandText = strQuery;
            objCommand.CommandType = CommandType.Text;
            objCommand.Connection = objConn;
            objDataAdapter = new SqlDataAdapter(objCommand);
            objDataAdapter.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dtRow in ds.Tables[0].Rows)
                {
                    //Reset tag for new iteration
                    strSelectTag = "";
                    strTestSurveyID = dtRow.ItemArray[0].ToString();
                    if (strSelSurvey != "" && strSelSurvey != "0")
                    {
                        if (strSelSurvey == strTestSurveyID)
                        {
                            //strResultMsg += "SelSurvey: " + strSelSurvey + "; TestSurvey: " + strTestSurveyID + "<br />";
                            strSelectTag = " SELECTED";
                            //Save WS_SurveyType to session variable 
                            HttpContext.Current.Session["SurveyType"] = dtRow.ItemArray[2].ToString();
                        }
                    }
                    if (blIsAdmin)
                    {
                        strDDL += "<option value=\"" + dtRow.ItemArray[0].ToString() + "\"" + strSelectTag + ">" + dtRow.ItemArray[1].ToString() + " - " + dtRow.ItemArray[0].ToString() + "</option>" + System.Environment.NewLine;
                    }
                    else
                    {
                        strDDL += "<option value=\"" + dtRow.ItemArray[0].ToString() + "\"" + strSelectTag + ">" + dtRow.ItemArray[1].ToString() + "</option>" + System.Environment.NewLine;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            strResultMsg += "<p><strong>Error CreateSurveyDropDownList:</strong> " + ex.Message + "; SQL: " + strQuery + "</p>";
            return strDDL;
        }
        finally
        {
            objConn.Close();
            objCommand.Dispose();
        }
        return strDDL;
    }

    /// <summary>
    /// Create a drop-down list of survey types
    /// </summary>
    /// <param name="strSelected"></param>
    /// <returns>HTML options only</returns>
    public String optSurveyTypes(string strSelected)
    {
        string strSQL = "pr_GetWebSurvey_Types";
        string strResult = CreateDropDownList(0, 1, strSQL, strSelected, false, false, "");
        return strResult;
    }

    /// <summary>
    /// Create a drop-down list of survey types minus quizzes
    /// </summary>
    /// <param name="strSelected"></param>
    /// <returns>String of options</returns>
    public String optSurveyTypesNoQuiz(string strSelected)
    {
        string strSQL = "pr_GetWebSurvey_Types_NoQuiz";
        string strResult = CreateDropDownList(0, 1, strSQL, strSelected, false, false, "");
        return strResult;
    }


    /// <summary>
    /// Creates a drop-down list of profiles by owner; all or filtered by type
    /// </summary>
    /// <param name="strEmplID"></param>
    /// <param name="strSelType"></param>
    /// <param name="strSelSurvey"></param>
    /// <returns>string of options</returns>
    public String optWebSurveys(string strEmplID, string strSelType, string strSelSurvey, bool blSelectOne, bool blAddNew)
    {
        string strDDL = "";
        string strSQL = "";
        if (strSelType == "")
        {
	        strSelType = "0";
        }

		if (strSelType == "0")
        {
			strSQL = "pr_optWebSurvey '" + strEmplID + "'";
        }
		else 
        {
			strSQL = "pr_optFilterWebSurvey '" + strEmplID + "' ," + strSelType;
        }
        //strResultMsg += "optWebSurveys SQL: " + strSQL + "</br >";
        //WS_SurveyID and WS_SurveyTitle
        strDDL += CreateSurveyDropDownList(strSQL, strSelSurvey, blSelectOne, blAddNew, "", false);
        return strDDL;
    }

    /// <summary>
    /// Creates a drop-down list of profiles available for editing by type
    /// </summary>
    /// <param name="strEmplID"></param>
    /// <param name="strSelSurvey"></param>
    /// <param name="strObject"></param>
    /// <returns>string of options</returns>
    public String optWebSurveys_Edit(string strEmplID, string strSelSurvey, string strObject)
    {
        string strDDL = "";
        string strSQL = "";
        //Pass "ALL" as UserID to get admin list
        switch(strObject)
        {
	        case "Survey":
		        strSQL = "pr_optWebSurvey_SurveysOnly '" + strEmplID + "'";
                break;
	        case "Form":
		        strSQL = "pr_optWebSurvey_Forms '" + strEmplID + "'";
                break;
            case "Quiz":
		        strSQL = "pr_optWebSurvey_Quizzes '" + strEmplID + "'";
                break;
        }
        //WS_SurveyID and WS_SurveyTitle
        strDDL += CreateSurveyDropDownList(strSQL, strSelSurvey, false, true, strObject, false);
        return strDDL;
    }

    /// <summary>
    /// Creates a drop-down list of all profiles; or filtered by type
    /// </summary>
    /// <param name="strSelType"></param>
    /// <param name="strSelSurvey"></param>
    /// <param name="EmplID"></param>
    /// <returns>string of options</returns>
    public String optWebSurveysAll(string strSelType, string strSelSurvey, string EmplID)
    {
        string strDDL = "";
        string strSQL = "";

        if (strSelType == "")
        {
	        strSelType = "0";
        }

        if (strSelType == "0")
        {
            strSQL = "pr_optWebSurveysAll_Unsec '" + EmplID + "'";
        }
        else
        {
            strSQL = "pr_optFilterWebSurveyAll " + strSelType + ", '" + EmplID + "'";
        }
        //WS_SurveyID and WS_SurveyTitle
        strDDL = CreateSurveyDropDownList(strSQL, strSelSurvey, true, false, "", false);

        return strDDL;
    }

    /// <summary>
    /// Creates a drop-down list of profiles for admins only; all or filtered by type
    /// </summary>
    /// <param name="strSelType"></param>
    /// <param name="strSelSurvey"></param>
    /// <returns>string of options</returns>
    public String optWebSurveysAllSec(string strSelType, string strSelSurvey)
    {
        string strDDL = "";
        string strSQL = "";
        string strObject = "";

        if (strSelType == "")
        {
	        strSelType = "0";
        }
        switch (strSelType)
        {
	        case "1":
	        case "2":
		        strObject = " Survey";
                break;
	        case "3":
	        case "4":
		        strObject = " Form";
                break;
	        case "5":
		        strObject = " Quiz";
                break;
        }
		if (strSelType == "0")
        {
			strSQL = "pr_optWebSurveyAll";
        }
		else 
        {
			strSQL = "pr_optFilterWebSurveyAdmin " + strSelType;
        }
				    
        //WS_SurveyID and WS_SurveyTitle
        strDDL = CreateSurveyDropDownList(strSQL, strSelSurvey, false, true, strObject, true);
        //strResultMsg += "<p>SQL: " + strSQL + "</p>";
        return strDDL;
    }

    /// <summary>
    /// Creates a drop-down list of open profiles only; all or filtered by type
    /// </summary>
    /// <param name="strSelType"></param>
    /// <param name="strSelSurvey"></param>
    /// <returns>string of options</returns>
    public String optOpenWebSurveys(string strSelType, string strSelSurvey)
    {
        string strSQL = "";
        string strDDL = "";

        if (strSelType == "")
        {
	        strSelType = "0";
        }

		if (strSelType == "0")
        {
			strSQL = "pr_optOpenWebSurvey";
        }
		else
        {
			strSQL = "pr_optOpenFilterWebSurvey " +  strSelType;
        }

        //WS_SurveyID and WS_SurveyTitle
        strDDL += CreateSurveyDropDownList(strSQL, strSelSurvey, true, false, "", false);
        return strDDL;
    }

    /// <summary>
    /// Creates a drop-down list of survey profiles only
    /// </summary>
    /// <param name="strEmplID"></param>
    /// <param name="strSelType"></param>
    /// <param name="strSelSurvey"></param>
    /// <returns>string of options</returns>
    public String optWebSurveysOnly(string strEmplID, string strSelType, string strSelSurvey)
    {
    string strSQL = "";
        string strDDL = "";

        if (strSelType == "")
        {
	        strSelType = "0";
        }
		if (strSelType == "0")
        {
			strSQL = "pr_optWebSurvey_surveys '" + strEmplID + "'";
        }
		else
        {
			strSQL = "pr_optFilterWebSurvey_surveys  '" + strEmplID + "', " + strSelType ;
        }
        //WS_SurveyID and WS_SurveyTitle
        strDDL += CreateSurveyDropDownList(strSQL, strSelSurvey, true, false, "", false);
        return strDDL;
    }

    /// <summary>
    /// Creates a drop-down list of owners for users or for admins
    /// </summary>
    /// <param name="strSelected"></param>
    /// <param name="strSurveyID"></param>
    /// <param name="blShowAll"></param>
    /// <returns>string of options</returns>
    public String optWebSurveyOwners(string strSelected, string strSurveyID, bool blAddMode, bool blAddNew)
    {
        String strSQL = "";
        String strDDL = "";
        String strSelectTag = "";

        if (strSelected == "")
        {
            strSelected = "0";
        }
        if (blAddNew)
        {
            if (strSelected == "0") { strSelectTag = " SELECTED"; } else { strSelectTag = ""; }
            strDDL = "<option value=\"0\"" + strSelectTag + ">-- Add New Owner --</option>" + System.Environment.NewLine;
        }
        else
        {
            strDDL = "<option value=\"\">Select An Owner</option>" + System.Environment.NewLine;
        }
        if (blAddMode)
        {
			strSQL = "pr_GetAllWebOwners";
			//CANDIDATE FOR DELETE
			//strSQL = "pr_GetAllSurveyOwners";
        }
	    else
        {
		    strSQL = "pr_GetSelSurveyOwners " + strSurveyID;
	    }
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCommand = new SqlCommand();
        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSOnlineSurvey"].ToString();
        SqlDataAdapter objDataAdapter;
        DataSet ds = new DataSet();
        //strResultMsg += "SQL: " + strSQL + "<br />";
        try
        {
            objConn.Open();
            objCommand.CommandText = strSQL;
            objCommand.CommandType = CommandType.Text;
            objCommand.Connection = objConn;
            objDataAdapter = new SqlDataAdapter(objCommand);
            objDataAdapter.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dtRow in ds.Tables[0].Rows)
                {
                    // WO_EmplID in pr_GetAllWebOwners
                    if (strSQL == "pr_GetAllWebOwners")
                    {
                        if (Convert.ToInt32(strSelected) == Convert.ToInt32(dtRow["WO_EmplID"]))
                        {
                            strSelectTag = " SELECTED";
                        }
                        else
                        {
                            strSelectTag = "";
                        }
                        // 1 = WO_FName; 2 = WO_LName
                        strDDL += "<option value=\"" + dtRow["WO_EmplID"].ToString() + "\"" + strSelectTag + ">" + dtRow["WO_LName"].ToString() + ", " + dtRow["WO_FName"].ToString() + "</option>" + System.Environment.NewLine;
                    }
                    else
                    {
                        // WOP_ID in pr_GetSelSurveyOwners and pr_GetAllSurveyOwners join
                        if (Convert.ToInt32(strSelected) == Convert.ToInt32(dtRow.ItemArray[6]))
                        {
                            strSelectTag = " SELECTED";
                        }
                        else
                        {
                            strSelectTag = "";
                        }
                        // 1 = WO_FName; 2 = WO_LName
                        strDDL += "<option value=\"" + dtRow["WOP_ID"].ToString() + "\"" + strSelectTag + ">" + dtRow["WO_LName"].ToString() + ", " + dtRow["WO_FName"].ToString() + "</option>" + System.Environment.NewLine;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            return ex.Message + strDDL;
        }
        finally
        {
            objConn.Close();
            objCommand.Dispose();
        }
        return strDDL;
    }


    public String optQuestionTypes(string strSelected, bool blIsForm)
    {
        string strSQL = "pr_GetWebSurvey_QuestionTypes " + HttpContext.Current.Session["SurveyType"];
        String strDDL = "";
        String strSelectTag = "";
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCommand = new SqlCommand();
        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSOnlineSurvey"].ToString();
        SqlDataAdapter objDataAdapter;
        DataSet ds = new DataSet();
        if (strSelected == "") { strSelectTag = " SELECTED"; } else { strSelectTag = ""; }
        strDDL += "<option value=\"\"" + strSelectTag + ">-- Select One --</option>" + System.Environment.NewLine;
        //Reset tag
        strSelectTag = "";
        try
        {
            objConn.Open();
            objCommand.CommandText = strSQL;
            objCommand.CommandType = CommandType.Text;
            objCommand.Connection = objConn;
            objDataAdapter = new SqlDataAdapter(objCommand);
            objDataAdapter.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dtRow in ds.Tables[0].Rows)
                {
                    if (strSelected != "")
                    {
                        if (strSelected == dtRow.ItemArray[0].ToString())
                        {
                            strSelectTag = " SELECTED";
                        }
                        else
                        {
                            strSelectTag = "";
                        }
                    }
                    //Only forms can attach a file
                    if (blIsForm && dtRow.ItemArray[0].ToString() == "7")
                    {
                        strDDL += "<option value=\"" + dtRow.ItemArray[0].ToString() + "\"" + strSelectTag + ">" + dtRow.ItemArray[1].ToString() + "</option>" + System.Environment.NewLine;
                    }
                    else if (dtRow.ItemArray[0].ToString() != "7")
                    {
                        strDDL += "<option value=\"" + dtRow.ItemArray[0].ToString() + "\"" + strSelectTag + ">" + dtRow.ItemArray[1].ToString() + "</option>" + System.Environment.NewLine;
                    }
                }
            }
        }
        catch
        {
            return strDDL;
        }
        finally
        {
            objConn.Close();
            objCommand.Dispose();
        }
        return strDDL;
    }

    /// <summary>
    /// Creates drop-down list options of abbreviated Questions/Fields
    /// </summary>
    /// <param name="strSurveyID"></param>
    /// <param name="strSelQuestion"></param>
    /// <returns>String of options</returns>
    public String optWebSurveyQuestions(String strSurveyID, String strSelQuestion, Boolean blSelectOne, Boolean blAddNew)
    {
        if (strSelQuestion == "") { strSelQuestion = "0"; }
        string strSQL = "pr_optWebSurveyQuestion " + strSurveyID ;
        String strDDL = "";
        String strSelectTag = "";
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCommand = new SqlCommand();
        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSOnlineSurvey"].ToString();
        SqlDataAdapter objDataAdapter;
        DataSet ds = new DataSet();
        if (blSelectOne)
        {
            if (strSelQuestion == "") { strSelectTag = " SELECTED"; } else { strSelectTag = ""; }
            strDDL += "<option value=\"\"" + strSelectTag + ">-- Select One --</option>" + System.Environment.NewLine;
        }
        if (blAddNew)
        {
            if (strSelQuestion == "0") { strSelectTag = " SELECTED"; } else { strSelectTag = ""; }
            strDDL += "<option value=\"0\"" + strSelectTag + ">-- Add New Question --</option>" + System.Environment.NewLine;
        }

        try
        {
            objConn.Open();
            objCommand.CommandText = strSQL;
            objCommand.CommandType = CommandType.Text;
            objCommand.Connection = objConn;
            objDataAdapter = new SqlDataAdapter(objCommand);
            objDataAdapter.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dtRow in ds.Tables[0].Rows)
                {
                    if (strSelQuestion == dtRow.ItemArray[0].ToString())
                    {
                        strSelectTag = " SELECTED";
                    }
                    else
                    {
                        strSelectTag = "";
                    }
                    //Strip HTML markup or throws nasty errors
                    String strQuestion = dtRow.ItemArray[2].ToString().Replace("<", "&lt;").Replace(">", "&gt;");
                    if (strQuestion.Length > 100)
                    {
                        strQuestion = strQuestion.Substring(0, 100) + "...";
                    }
                    strDDL += "<option value=\"" + dtRow.ItemArray[0].ToString() + "\"" + strSelectTag + ">" + dtRow.ItemArray[1].ToString() + " - " + strQuestion + "</option>" + System.Environment.NewLine;
                }
            }
        }
        catch
        {
            return strDDL;
        }
        finally
        {
            objConn.Close();
            objCommand.Dispose();
        }
        return strDDL;
    }

    /// <summary>
    /// Creates drop-down list options for Tallies associated with a Profile
    /// </summary>
    /// <param name="strSelSurvey"></param>
    /// <param name="strSelTally"></param>
    /// <param name="intSurveyType"></param>
    /// <param name="blAddMode"></param>
    /// <returns>String of options</returns>
    public String optWebTallies(string strSelSurvey, string strSelTally, Int32 intSurveyType, bool blAddMode)
    {
        if (strSelSurvey == "") { strSelSurvey = "0"; }
        if (strSelTally == "") { strSelTally = "0"; }
        string strSQL = "pr_GetSelectWebTallies " + strSelSurvey;
        String strDDL = "";
        String strSelectTag = "";
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCommand = new SqlCommand();
        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSOnlineSurvey"].ToString();
        SqlDataAdapter objDataAdapter;
        DataSet ds = new DataSet();

        if (blAddMode)
        {
            strDDL = "<option value=\"0\">--Add New--</option>" + System.Environment.NewLine;
        }
        else
        {
            strDDL = "<option value=\"0\">--Select One--</option>" + System.Environment.NewLine;
        }
        try
        {
            objConn.Open();
            objCommand.CommandText = strSQL;
            objCommand.CommandType = CommandType.Text;
            objCommand.Connection = objConn;
            objDataAdapter = new SqlDataAdapter(objCommand);
            objDataAdapter.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dtRow in ds.Tables[0].Rows)
                {
                    if (strSelTally == dtRow["WST_TallyID"].ToString())
                    {
                        strSelectTag = " SELECTED";
                    }
                    else
                    {
                        strSelectTag = "";
                    }
                    //2 = WST_DateOpen; 3 = WST+DateClosed.  When equal, open perpetually
                    if (intSurveyType == 1 || intSurveyType == 2 || dtRow["WST_DateOpen"].ToString() != dtRow["WST_DateOpen"].ToString())
                    {
                        strDDL += "<option value=\"" + dtRow["WST_TallyID"].ToString() + "\"" + strSelectTag + ">" + Convert.ToDateTime(dtRow["WST_DateOpen"]).ToShortDateString() + " to " + Convert.ToDateTime(dtRow["WST_DateClosed"]).ToShortDateString() + "</option>" + System.Environment.NewLine;
                    }
                    else
                    {
                        //0 = WST_TallyID
                        strDDL += "<option value=\"" + dtRow["WST_TallyID"].ToString() + "\"" + strSelectTag + ">Tally ID: " + dtRow["WST_TallyID"].ToString() + "</option>" + System.Environment.NewLine;
                    }
                    
                }
            }
        }
        catch
        {
            return strDDL;
        }
        finally
        {
            objConn.Close();
            objCommand.Dispose();
        }
        return strDDL;
    }

    /// <summary>
    /// Creates drop-down list options for quiz takers associated with a Tally
    /// </summary>
    /// <param name="strSelTally"></param>
    /// <param name="strSelStudent"></param>
    /// <returns>String of options</returns>
    public String optWebQuizStudents(string strSelTally, string strSelStudent)
    {
        string strResult = "";
        string strSelectTag = "";
        string strSQL = "pr_GetSelectWebStudents " + strSelTally;
        string strName = "";
        string strDomain = "Student";
        string strID = "";
        if (strSelStudent == "") { strSelStudent = "0"; }
        if (strSelTally == "") { strSelTally = "0"; }
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCommand = new SqlCommand();
        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSOnlineSurvey"].ToString();
        SqlDataAdapter objDataAdapter;
        DataSet ds = new DataSet();

        if (strSelStudent == "1")
        {
	        strResult += "<option value=\"1\" selected>All Students</option>" + System.Environment.NewLine;
	        strResult += "<option value=\"2\">All Employees</option>" + System.Environment.NewLine;
        }
        else if (strSelStudent == "2")
        {
	        strResult += "<option value=\"1\">All Students</option>" + System.Environment.NewLine;
	        strResult += "<option value=\"2\" selected>All Employees</option>" + System.Environment.NewLine;
        }
        else
        {
	        strResult += "<option value=\"1\">All Students</option>" + System.Environment.NewLine;
	        strResult += "<option value=\"2\">All Employees</option>" + System.Environment.NewLine;
        }
        try
        {
            objConn.Open();
            objCommand.CommandText = strSQL;
            objCommand.CommandType = CommandType.Text;
            objCommand.Connection = objConn;
            objDataAdapter = new SqlDataAdapter(objCommand);
            objDataAdapter.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                //Default is 1 = Student
                if (strSelStudent == "2") //Employees
                {
                    strDomain = "CCS";
                }
                else if(strSelStudent == "0")   //Mix of staff/students
                {
                    strDomain = "GC";
                }
                foreach (DataRow dtRow in ds.Tables[0].Rows)
                {
                    strID = dtRow["WSU_StudentID"].ToString();
				    if (strSelStudent == strID)
                    {
					    strSelectTag = " SELECTED";
                    }
				    else
                    {
					    strSelectTag = "";
                    }
                    if(strID != "")
                    {
                        EmployeeActiveDirectoryInfo myInfo = new EmployeeActiveDirectoryInfo(strID,"SID", strDomain);
                        strName = myInfo.strFullName;
                    }
                    if (strName == "") 
                    { 
                        strName = strID; 
                    } 
                    else
                    {
                        strName = strID + " - " + strName;
                    }
				    strResult += "<option value=\"" + strID + strSelectTag + "\">" + strName + "</option>" + System.Environment.NewLine;
                    //Reset for next record
                    strName = "";
                }
            }
        }
        catch
        {
            return strResult;
        }
        finally
        {
            objConn.Close();
            objCommand.Dispose();
        }
        return strResult;
    }

    /// <summary>
    /// Creates drop-down list options of all admins
    /// </summary>
    /// <param name="strSelected"></param>
    /// <returns>String of options</returns>
    public String optWebAdmins(string strSelected)
    {
        string strSQL = "pr_optWebAdmins";
        String strResult = "<option value=\"\">Select One</option>" + System.Environment.NewLine;
        String strSelectTag = "";

        if (strSelected == "0")
        {
            strResult += "<option value=\"0\" SELECTED>Add New Admin</option>" + System.Environment.NewLine;
        }
        else
        {
            strResult += "<option value=\"0\">Add New Admin</option>" + System.Environment.NewLine;
        }

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCommand = new SqlCommand();
        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSOnlineSurvey"].ToString();
        SqlDataAdapter objDataAdapter;
        DataSet ds = new DataSet();

        try
        {
            objConn.Open();
            objCommand.CommandText = strSQL;
            objCommand.CommandType = CommandType.Text;
            objCommand.Connection = objConn;
            objDataAdapter = new SqlDataAdapter(objCommand);
            objDataAdapter.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dtRow in ds.Tables[0].Rows)
                {            
            
                    if (strSelected == dtRow["WAdmin_ID"].ToString())
                    {
					    strSelectTag = " SELECTED";
                    }
				    else
                    {
					    strSelectTag = "";
				    }
                    strResult += "<option value=" + dtRow["WAdmin_ID"] + strSelectTag + ">" + dtRow["WAdmin_LName"] + ", " + dtRow["WAdmin_FName"] + "</option>" + System.Environment.NewLine;
                }
            }
        }
        catch
        {
            return strResult;
        }
        finally
        {
            objConn.Close();
            objCommand.Dispose();
        }
        return strResult;
    }

    /// <summary>
    /// Pass True to use STRM (term code) as option value; False to use quarter display as option value
    /// </summary>
    /// <param name="blUseSTRMValue"></param>
    /// <param name="intBeforeCurrentQtr"></param>
    /// <param name="intAfterCurrentQtr"></param>
    /// <param name="strSelectedQtr"></param>
    /// <returns>Returns a drop-down list of quarters relative to current quarter</returns>
    public string optQuarters(bool blUseSTRMValue, Int32 intBeforeCurrentQtr, Int32 intAfterCurrentQtr, string strSelectedQtr)
    {
        string strResult = "";
        string strSelectTag = "";
        Quarter_MetaData myQMD = new Quarter_MetaData();
        DataTable dt = myQMD.GetSTRM_DisplayDescr_Quarter_MetaData_Base(intBeforeCurrentQtr, intAfterCurrentQtr);
        if (dt.Rows.Count > 0)
        {
            foreach (DataRow dtRow in dt.Rows)
            {
                if (blUseSTRMValue)
                {
                    if (strSelectedQtr == dtRow["STRM"].ToString())
                    {
                        strSelectTag = " SELECTED";
                    }
                    else
                    {
                        strSelectTag = "";
                    }
                    strResult += "<option value=\"" + dtRow["STRM"] + "\"" + strSelectTag + ">" + dtRow["DisplayDesc"] + "</option>" + System.Environment.NewLine;
                }
                else
                {
                    if (strSelectedQtr == dtRow["DisplayDesc"].ToString())
                    {
                        strSelectTag = " SELECTED";
                    }
                    else
                    {
                        strSelectTag = "";
                    }
                    strResult += "<option value=\"" + dtRow["DisplayDesc"] + "\"" + strSelectTag + ">" + dtRow["DisplayDesc"] + "</option>" + System.Environment.NewLine;
                }
            }
        }

        return strResult;
    }

    /// <summary>
    /// Uses iCatalog web services to retrieve clusters for drill-down drop-down list of programs.
    /// </summary>
    /// <returns>striong of options</returns>
    public string optProgramCategories(string strSelected)
    {
        //strResultMsg += "Selected: " + strSelected + "<br />";
        string strResult = "";
        string strSelectTag = "";
        iCatalogXML.iCatalogXML iCatalogWS = new iCatalogXML.iCatalogXML();
        DataSet dsCategory = iCatalogWS.GetProgramCategories();
        Int32 rowCount = dsCategory.Tables[0].Rows.Count;
        for (Int32 intDSRow = 0; intDSRow < rowCount; intDSRow++)
        {
            if (strSelected == dsCategory.Tables[0].Rows[intDSRow]["CategoryID"].ToString())
            {
                strSelectTag = " SELECTED";
            }
            else
            {
                strSelectTag = "";
            }
            strResult += "<option value=\"" + dsCategory.Tables[0].Rows[intDSRow]["CategoryID"].ToString() + "\"" + strSelectTag + ">" + dsCategory.Tables[0].Rows[intDSRow]["CategoryTitle"].ToString() + "</option>" + System.Environment.NewLine;
        }
        return strResult;
    }

    /// <summary>
    /// Assumes a cluster/category has been chosen; returns matching programs.  Pass blank to get a horrendously long list of all programs.
    /// </summary>
    /// <param name="strSelectedCategory"></param>
    /// <returns>string of options</returns>
    public string optProgramsByCategory(string strSelectedCategory)
    {
        string strResult = "";
        Utility myUtil = new Utility();
        iCatalogXML.iCatalogXML iCatalogWS = new iCatalogXML.iCatalogXML();
        String strRegTerm = iCatalogWS.GetRegistrationTerm();
        DataSet dsProgOptions = iCatalogWS.GetOptionsByCategory(strRegTerm, "", "", strSelectedCategory, "", "");
        Int32 intRowCount = dsProgOptions.Tables[0].Rows.Count;
        string[,] ar = new string[intRowCount, intRowCount];
        if (intRowCount > 0)
        {
            //iterate
            for (Int32 intDSRow = 0; intDSRow < dsProgOptions.Tables[0].Rows.Count; intDSRow++)
            {
                String strNewProgramTitle = myUtil.TitleCase(dsProgOptions.Tables[0].Rows[intDSRow]["ProgramTitle"].ToString());
                String strOptionTitle = myUtil.TitleCase(dsProgOptions.Tables[0].Rows[intDSRow]["OptionTitle"].ToString());
                String strCollegeShortTitle = dsProgOptions.Tables[0].Rows[intDSRow]["CollegeShortTitle"].ToString();
                String strDegreeShortTitle = dsProgOptions.Tables[0].Rows[intDSRow]["DegreeShortTitle"].ToString();
                //Determine location title
                String strDisplayLocation = strCollegeShortTitle;
                String strDisplayProgram = strNewProgramTitle;
                if (strOptionTitle != "")
                {
                    strDisplayProgram = strOptionTitle;
                }
                //Create drop-down options
                strResult += "<option value=\"" + strNewProgramTitle + "\">" + strDisplayProgram + " - " + strDegreeShortTitle + " - " + strDisplayLocation + "</option>" + System.Environment.NewLine;
            }
        }
        return strResult;
    }

    public string optDropDownListTypes(string strSelected)
    {
        string strSQL = "pr_GetWebSurvey_DropDownListTypes";
        string strResult = CreateDropDownList(0, 1, strSQL, strSelected, true, false, "");
        return strResult;
    }

#endregion

#region "GET FUNCTIONS"

    /// <summary>
    /// Fetches data per query sent to function
    /// </summary>
    /// <param name="strQuery"></param>
    /// <returns>DataTable</returns>
    DataTable RunGetQuery(string strQuery)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCommand = new SqlCommand();
        SqlDataAdapter objDataAdapter;
        DataSet ds = new DataSet();
        DataTable dt = new DataTable();
        try
        {
            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSOnlineSurvey"].ToString();
            objConn.Open();
            objCommand.CommandText = strQuery;
            objCommand.CommandType = CommandType.Text;
            objCommand.Connection = objConn;

            objDataAdapter = new SqlDataAdapter(objCommand);
            objDataAdapter.Fill(ds);
            dt = ds.Tables[0];
        }
        catch (Exception ex)
        {
            strResultMsg += "Error RunGetQuery: " + ex.Message + "<br />SQL: " + strQuery + "<br />";
        }
        finally
        {
            objConn.Close();
            objConn.Dispose();
            objCommand.Dispose();
        }
        return dt;
    }

    /// <summary>
    /// Get all settings for a Profile given a specific ID
    /// </summary>
    /// <param name="intSurveyID"></param>
    /// <returns>DataTable</returns>
    public DataTable GetOneSurvey(string strSurveyID)
    {
        string strSQL = "pr_GetOneWebSurvey " + strSurveyID;    
        DataTable dt = RunGetQuery(strSQL);
        return dt;
    }

    
    /// <summary>
    /// Gets all settings for all Profiles associated with an Owner
    /// </summary>
    /// <param name="strEmplID"></param>
    /// <returns>DataTable</returns>
    public DataTable GetOwnerSurveys(string strEmplID)
    {
        string strSQL = "pr_GetAllWebSurveys '" + strEmplID + "'";
        DataTable dt = RunGetQuery(strSQL);
        return dt;
    }

    /// <summary>
    /// Get Profile and Owner data by Profile ID
    /// </summary>
    /// <param name="intSurveyID"></param>
    /// <returns>DataTable</returns>
    public DataTable GetOneSurveyAndOwner(String strSurveyID)
    {
        string strSQL = "pr_GetOneWebSurveyAndOwner " + strSurveyID;
        DataTable dt = RunGetQuery(strSQL);
        return dt;
    }


    /// <summary>
    /// Get Owner data by WebOwner table ID
    /// </summary>
    /// <param name="intOwnerID"></param>
    /// <returns>DataTable</returns>
    public DataTable GetOneOwner_ByID(Int32 intOwnerID)
    {
        string strSQL = "pr_GetOneWebOwner_Profile " + intOwnerID.ToString();
        DataTable dt = RunGetQuery(strSQL);
        return dt;
    }



    /// <summary>
    /// Get one Question data
    /// </summary>
    /// <param name="intSurveyID"></param>
    /// <param name="intQID"></param>
    /// <returns>DataTable</returns>
    public DataTable GetOneQuestion(Int32 intQID)
    {
        //Fixed this to only require intQID as parameter
        string strSQL = "pr_GetOneWebSurveyQuestion " + intQID.ToString();
        DataTable dt = RunGetQuery(strSQL);
        return dt;
    }

    /// <summary>
    /// Get Owner data by Employee ID
    /// </summary>
    /// <param name="strEmplID"></param>
    /// <returns>DataTable</returns>
    public DataTable GetOneOwner_ByUID(string strEmplID)
    {
        string strSQL = "pr_GetOneWebOwner '" + strEmplID + "'";
        DataTable dt = RunGetQuery(strSQL);
        return dt;
    }

    /// <summary>
    /// Get all fields to all questions per Profile ID
    /// </summary>
    /// <param name="intSurveyID"></param>
    /// <returns>DataTable</returns>
   public DataTable GetAllSurveyQuestions(Int32 intSurveyID)
    {
        string strSQL = "pr_GetAllWebSurveyQuestions " + intSurveyID.ToString();
        DataTable dt = RunGetQuery(strSQL);
        return dt;
    }


    /// <summary>
    /// Get all options by Question ID
    /// </summary>
    /// <param name="intSurveyID"></param>
    /// <param name="intQID"></param>
    /// <returns>DataTable</returns>
    public DataTable GetSurveyQuestionOptions(Int32 intSurveyID, Int32 intQID)
    {
        //strResultMsg += "Survey ID: " + intSurveyID.ToString() + "; QID: " + intQID.ToString() + "<br />";
        string strSQL = "pr_GetAllWebSurveyOptions_byQ " + intSurveyID.ToString() + ", " + intQID.ToString();
        DataTable dt = RunGetQuery(strSQL);
        return dt;
    }

    /// <summary>
    /// Get one Tally by Tally ID or all Tallies by Profile ID
    /// </summary>
    /// <param name="strSelTally"></param>
    /// <param name="strSelSurvey"></param>
    /// <returns>DataTable</returns>
    public DataTable GetOneTally(string strSelTally, string strSelSurvey)
    {
        bool blContinue = false;
        string strSQL = "";
        DataTable dt = new DataTable();
	    if (strSelTally == "" || strSelTally == "0")
        {
		    strSQL = "pr_GetSelectWebTallies " + strSelSurvey;
            blContinue = true;
        }
	    else if (strSelSurvey != "")
        {
		    strSQL = "pr_GetOneWebSurveyTally " + strSelTally; 
            blContinue = true;
        }
	    else
        {
	        blContinue = false;
        }
        if (blContinue)
        {
            dt = RunGetQuery(strSQL);
        }
        return dt;
    }

    /// <summary>
    /// Get all options associated with an OptionGroupID
    /// </summary>
    /// <param name="intOptionGroupID"></param>
    /// <returns>DataTable</returns>
    public DataTable GetOptionsByOptionGroupID(Int32 intOptionGroupID)
    {
        string strSQL = "pr_GetOptionsByOptionGroupID " + intOptionGroupID.ToString();
        DataTable dt = RunGetQuery(strSQL);
        return dt;
    }

    /// <summary>
    /// Get similar options by template ID for editing
    /// </summary>
    /// <param name="intSurveyID"></param>
    /// <param name="intOptionTemplate"></param>
    /// <param name="strDescription"></param>
    /// <returns>DataTable</returns>
    public DataTable GetOptionsByOptionTemplate(Int32 intSurveyID, Int32 intOptionTemplate, string strDescription)
    {
        string strSQL = "pr_GetOptionsByOptionTemplate " + intSurveyID.ToString() + ", " + intOptionTemplate.ToString() + ", '" + strDescription.ToString() + "'";
        DataTable dt = RunGetQuery(strSQL);
        return dt;
    }

    /// <summary>
    /// Get Responses from WebSurvey_TallyResults by Tally ID
    /// </summary>
    /// <param name="intTallyID"></param>
    /// <returns>DataTable</returns>
    public DataTable GetSurveyTallyResults(Int32 intTallyID)
    {
        string strSQL = "pr_GetOneTallyResults " + intTallyID.ToString();
        DataTable dt = RunGetQuery(strSQL);
        return dt;
    }

    /// <summary>
    /// Get Responses from WebSurvey_User by Tally ID
    /// </summary>
    /// <param name="intTallyID"></param>
    /// <returns>DataTable</returns>
    public DataTable GetSelectSurveyResults(Int32 intTallyID)
    {
        string strSQL = "pr_GetSelectResults " + intTallyID.ToString(); 
        DataTable dt = RunGetQuery(strSQL);
        return dt;
    }

    /// <summary>
    /// Get Responses from WebSurvey_Responses by Tally ID and QID
    /// </summary>
    /// <param name="intTallyID"></param>
    /// <param name="intQID"></param>
    /// <returns>DataTable</returns>
    public DataTable GetTallyResponsesByQID(Int32 intTallyID, Int32 intQID)
    {
        string strSQL = "pr_GetWebSurveyResponses_byQ " + intTallyID + ", " + intQID;
        DataTable dt = RunGetQuery(strSQL);
        return dt;
    }

    /// <summary>
    /// Get Responses from WebSurvey_Responses by UserID
    /// </summary>
    /// <param name="intUserID"></param>
    /// <returns>DataTable</returns>
    public DataTable GetTallyResponsesByUID(Int32 intUserID)
    {
        string strSQL = "pr_GetWebSurveyResponses_byUser " + intUserID;
        DataTable dt = RunGetQuery(strSQL);
        return dt;
    }
      
    /// <summary>
    /// Get calculated quiz results per tally per user
    /// </summary>
    /// <param name="intTallyID"></param>
    /// <param name="strSID"></param>
    /// <returns>DataTable</returns>
    public DataTable GetQuizResults(Int32 intTallyID, string strSID)
    {
        string strSQL = "pr_GetMaxWebSurveyQuiz_Results " + intTallyID + ", '" + strSID + "'"; 
        DataTable dt = RunGetQuery(strSQL);
        return dt;
    }

    /// <summary>
    /// Get Users from WebSurvey_User by Tally ID
    /// </summary>
    /// <param name="intTallyID"></param>
    /// <returns>DataTable</returns>
    public DataTable GetSelectSurveyUsers(Int32 intTallyID)
    {
        //SP limits to 300 users
        string strSQL = "pr_GetSelSurveyUsers " + intTallyID.ToString(); 
        DataTable dt = RunGetQuery(strSQL);
        return dt;
    }

    /// <summary>
    /// Get Admin table info for one admin
    /// </summary>
    /// <param name="intAdminID"></param>
    /// <param name="strEmplID"></param>
    /// <returns>DataTable</returns>
    public DataTable GetOneWebAdmin(Int32 intAdminID, string strEmplID)
    {
        //consolidates GetOneWebAdminByUserID into one function
        string strSQL = "";
        if (strEmplID != "")
        {
            strSQL = "pr_GetOneWebAdminByUserID '" + strEmplID + "'";
        }
        else if (intAdminID != 0)
        {
            strSQL = "pr_GetOneWebAdmin " + intAdminID.ToString();
        }
        DataTable dt = RunGetQuery(strSQL);
        return dt;
    }

    /// <summary>
    /// Get User info by identity ID
    /// </summary>
    /// <param name="intUserID"></param>
    /// <returns>DataTable</returns>
    public DataTable GetOneSurveyUser(Int32 intUserID)
    {
        string strSQL = "pr_GetOneWebSurvey_User " + intUserID;
        DataTable dt = RunGetQuery(strSQL);
        return dt;
    }

    public DataTable GetSurveyUsers(Int32 intTallyID)
    {
        //SP limits to 300 users

        string strSQL = "pr_GetSurveyUsers " + intTallyID;
        DataTable dt = RunGetQuery(strSQL);
        return dt;
    }

    public DataTable GetExportResults(string strSelSurvey, string strSelTally, string strUserID)
    {
        string strSQL = "pr_SurveyResultsOnly_Export " + strSelSurvey + ", " + strSelTally + ", " + strUserID;
        DataTable dt = RunGetQuery(strSQL);
        return dt;
    }


#endregion


#region "USER MODULE FUNCTIONS"

    /// <summary>
    /// Loads all survey data into arrays for other uses
    /// </summary>
    /// <param name="intSurveyID"></param>
    /// <returns>Integer - Question Count</returns>
    private Int32 LoadQuestionArrays(Int32 intSurveyID)
    {
        Int32 i = 0;
        Int32 intQCount = 0;
        //LOAD THE ARRAYS
        DataTable dt = GetAllSurveyQuestions(intSurveyID);
        //strResultMsg += "intSurveyID: " + intSurveyID + "</br >";
        //strResultMsg += "Row count: " + dt.Rows.Count + "</br >";
        if (dt.Rows.Count > 0)
        {
            foreach (DataRow dtRow in dt.Rows)
            {

                arrQID[i] = Convert.ToInt32(dtRow["WSQ_QID"]);
                arrQuestions[i] = dtRow["WSQ_Question"].ToString();
                arrQuizCorrectText[i] = dtRow["WSQ_CorrectText"].ToString();
                arrQuizIncorrectText[i] = dtRow["WSQ_IncorrectText"].ToString();
                arrOptionCount[i] = Convert.ToInt32(dtRow["WSQ_OptionCount"]);
                arrQuestionType[i] = Convert.ToInt32(dtRow["WSQ_QuestionType"]);
                arrValidateType[i] = Convert.ToInt32(dtRow["WSQ_ValidateType"]);
                arrMaskType[i] = Convert.ToInt32(dtRow["WSQ_MaskType"]);
                if (! DBNull.Value.Equals (dtRow["WSQ_MaxLength"]))
                {
                    arrMaxLength[i] = Convert.ToInt32(dtRow["WSQ_MaxLength"]);
                }
                arrRequiredField[i] = Convert.ToBoolean(dtRow["WSQ_RequiredField"]);
                arrVertAlign[i] = Convert.ToBoolean(dtRow["WSQ_VertAlign"]);
                arrDisplayCountdown[i] = Convert.ToInt32(dtRow["WSQ_DisplayCountdown"]);
                arrFieldGroup[i] = Convert.ToBoolean(dtRow["WSQ_IsFieldGroup"]);
                arrDropDownListType[i] = Convert.ToInt32(dtRow["WSQ_DropDownListType"]);
                //strResultMsg += "Field Group " & i & ": " & arrFieldGroup[i] & "; ";
                i += 1;
            }
            intQCount = i;
        }
        return intQCount;
    }

    /// <summary>
    /// Load question arrays and render display code 
    /// </summary>
    /// <param name="intSurveyID"></param>
    /// <param name="intSurveyType"></param>
    /// <param name="blNumberFields"></param>
    /// <param name="blAnonymous"></param>
    /// <param name="blUserNotification"></param>
    /// <param name="blSubmitOnce"></param>
    /// <param name="intOtherEmailCount"></param>
    /// <returns>String of survey questions for display</returns>
    public String DisplaySurveyQuestions(Int32 intSurveyID, Int32 intSurveyType, bool blNumberFields, bool blAnonymous, bool blUserNotification, bool blSubmitOnce, Int32 intOtherEmailCount, string strSelectedCategory)
    {
        int i = 0; 
        int j = 0; 
        int k = 0; 
        int q = 0;
        int z = 0;
        int intBlanks = 0;
        string strRow = "";
        int intQCount = 0;
        bool blKCount = false;
        string strReqFieldTag = "";
        string strQuestion = "";
        bool blCloseFieldGroup = false;
        string strPlaceholderCell = "";
        string strColSpanTag = "";
        //string strColSpanTagMinusOne = "";    //MAY NO LONGER BE IN USE?
        string strOptionCell = "";
        int intTabIndex = 1;
        int intFieldNo = 0;
        string strCountdownSpan = "";
        StringBuilder objBuilder = new StringBuilder();

        //Set table colspan tags
        if (blNumberFields)
        {
            strColSpanTag = " colspan=\"3\"";
            //strColSpanTagMinusOne = " colspan=\"2\"";
        }
        else
        {
            strColSpanTag = " colspan=\"2\"";
            //strColSpanTagMinusOne = "";
        }
        if (!blAnonymous) 
        { 
            intTabIndex += 4;
            intFieldNo += 4;
        }
        else if(!blUserNotification || blSubmitOnce) 
        {
            intTabIndex += 1;
            intFieldNo += 1;
        }
        intTabIndex += intOtherEmailCount;
        intQCount = LoadQuestionArrays(intSurveyID);
        //HttpContext.Current.Response.Write("intQCount: " + intQCount);
        
        //BEGIN FOR LOOP
        //Preserve Question Count for processing
        objBuilder.Append("<tr><td" + strColSpanTag + "><input type=\"hidden\" name=\"hdnQCount\" value=\"" + intQCount + "\"></td></tr>" + System.Environment.NewLine);
        //Parse the questions based on survey/form type, then question type, alignment
        for (i = 0; i < intQCount; i++)
        {
            intFieldNo += 1;
            //strResultMsg += "blCloseFieldGroup BEFORE: " + blCloseFieldGroup + "<br />";
            if (arrFieldGroup[i])
            {
                if (! blCloseFieldGroup)
                {
                    //First one
                    blCloseFieldGroup = true;
                    //Start the hidden field/horizontal group row
                    strRow += "<tr><td" + strColSpanTag + ">";
                }
            }
            if (blCloseFieldGroup && ! arrFieldGroup[i])	//Complete the previous Q type 3 row when next field not part of field group
            {
		        objBuilder.Append("</td></tr>" + System.Environment.NewLine);
                blCloseFieldGroup = false;
            }
            //strResultMsg += "blCloseFieldGroup AFTER: " + blCloseFieldGroup + "<br />";
	        //Hidden field row: preserve Question data for processing 
            if (! blCloseFieldGroup)
            {
                strRow += "<tr><td" + strColSpanTag + ">";
            }
	        strRow += "<input type=\"hidden\" name=\"hdnQID\" value=\"" + arrQID[i] + "\">" + System.Environment.NewLine;
	        strRow += "<input type=\"hidden\" name=\"hdnOCount\" value=\"" + arrOptionCount[i] + "\">" + System.Environment.NewLine;
	        strRow += "<input type=\"hidden\" name=\"hdnQType\" value=\"" + arrQuestionType[i] + "\">" + System.Environment.NewLine;
	        strRow += "<input type=\"hidden\" name=\"hdnValidateType\" value=\"" + arrValidateType[i] + "\">" + System.Environment.NewLine;
	        strRow += "<input type=\"hidden\" name=\"hdnRequiredField\" value=\"" + arrRequiredField[i] + "\">" + System.Environment.NewLine;
            strRow += "<input type=\"hidden\" name=\"hdnFieldNo" + i + "\" value=\"" + (intFieldNo - intBlanks) + "\">" + System.Environment.NewLine;
            if(! blCloseFieldGroup)
            {
                strRow += "</td></tr>" + System.Environment.NewLine;
            }
            //Get label
            strQuestion = arrQuestions[i];
            //PUT IN FIELD NUMBERS IF DESIRED
	        if (blNumberFields)
            {
                //Set placeholder cell tag
                strPlaceholderCell = "<td>&nbsp;</td>";
                if (arrRequiredField[i])
                {
                    strReqFieldTag = "<span style=\"color:Red;\"><strong>*&nbsp;</strong></span>";
                }
                else
                {
                    strReqFieldTag = "";
                }
                if (intQCount == 1)
                {
                    if (blCloseFieldGroup)
                    {
			            strRow += strReqFieldTag + "<strong>Q.&nbsp;&nbsp;</strong>";
                    }
                    else
                    {
			            strRow += "<tr><td>" + strReqFieldTag + "<strong>Q.&nbsp;&nbsp;</strong></td>" + System.Environment.NewLine;
                    }
                }
		        else if (arrQuestionType[i] != 5) //Skip blank fields
                {
                    if (blCloseFieldGroup)
                    {
			            strRow += strReqFieldTag + "<strong>" + ((i + 1) - intBlanks) + ".&nbsp;&nbsp;</strong>";
                    }
                    else
                    {
            	        strRow += "<tr><td>" + strReqFieldTag + "<strong>" + ((i + 1) - intBlanks) + ".&nbsp;&nbsp;</strong></td>" + System.Environment.NewLine;
                    }
		        }
                //Reset strReqFieldTag to avoid duplication below
                strReqFieldTag = "";
            }
            else
            {
                if (! blCloseFieldGroup)
                {
                    strRow += "<tr>";
                }
                if (arrFieldGroup[i])
                {
                    strQuestion = strQuestion.Replace("<p>", "");
                    strQuestion = strQuestion.Replace("</p>", "");
                }
               if (arrRequiredField[i])
               {
                    strQuestion = strQuestion.Replace("<p>", "");
                    strQuestion = strQuestion.Replace("</p>", "");
                    strReqFieldTag = "<span style=\"color:Red;\"><strong>*&nbsp;</strong></span>";
               }
               else
               {
                    strReqFieldTag = "";
               }
            }

            //FIELD LABELS
            switch (arrQuestionType[i])
            {
		        case 1: //Q Type 1 Radio
			        if (arrVertAlign[i])
                    {
                        strRow += "<td colspan=\"2\">" + strReqFieldTag + "<label for=\"option" + i + "\" title=\"Question " + i + "\">" + strQuestion + "</label></td></tr>" + System.Environment.NewLine;
                    }
                    else
                    {
                        //strRow += "<td>" + strReqFieldTag + "<label for=\"option" + i + "\">" + i + ". " + strQuestion + "</label></td>" + System.Environment.NewLine;
                        strRow += "<td>" + strReqFieldTag + "<label for=\"option" + i + "\" title=\"Question " + i + "\">" + i + ". " + strQuestion + "</label></td>" + System.Environment.NewLine;
                        strRow += "<td>" + System.Environment.NewLine + "<table cellpadding=\"4\" cellspacing=\"0\" border=\"0\" bordercolor=\"lightgrey\">"  + System.Environment.NewLine + "<tr>";
                    }
                    break;
		        case 2: //Q Type 2, Checkbox
			        if (arrVertAlign[i])
                    {
                        strRow += "<td colspan=\"2\">" + strReqFieldTag + "<label for=\"checkbox" + i + "\" title=\"Question " + i + "\">" + strQuestion + "</label></td></tr>" + System.Environment.NewLine;
                    }
                    else
                    {
                        strRow += "<td>" + strReqFieldTag + "<label for=\"checkbox" + i + "\" title=\"Question " + i + "\">" + strQuestion + "</label></td>" + System.Environment.NewLine;
			            strRow += "<td>" +  System.Environment.NewLine + "<table cellpadding=\"4\" cellspacing=\"0\" border=\"0\" bordercolor=\"lightgrey\">"  + System.Environment.NewLine + "<tr>";
                    }
                    break;
		        case 3:  //fill in
			        if (strQuestion.Length > 80)
                    {
                        strRow += "<td colspan=\"2\">" + strReqFieldTag + "<label for=\"text" + i + "\" title=\"Question " + i + "\">" + strQuestion + "</label></td></tr>" + System.Environment.NewLine;
                    }
                    else
                    {
                        if (blCloseFieldGroup)
                        {
                            strRow += strReqFieldTag + "<label for=\"text" + i + "\" title=\"Question " + i + "\">" + strQuestion + "</label>&nbsp;&nbsp;";
                        }
                        else
                        {
                            strRow += "<td>" + strReqFieldTag + "<label for=\"text" + i + "\" title=\"Question " + i + "\">" + strQuestion + "</label></td>" + System.Environment.NewLine;
                        }
			        }
                    break;
		        case 4: //text area
                    strRow += "<td colspan=\"2\">" + strReqFieldTag + "<label for=\"text" + i + "\" title=\"Question " + i + "\">" + strQuestion + "</label></td></tr>" + System.Environment.NewLine;
                    break;
		        case 5: //Blank - no answer, no numbering
                    strRow += "<tr><td valign=\"top\"" + strColSpanTag + ">" + strQuestion + "</td></tr>" + System.Environment.NewLine;
			        intBlanks += 1;
                    break;
		        case 6: // Multiple Choice - drop-down
                    strRow += "<td>" + strReqFieldTag + "<label for=\"option" + i + "\" title=\"Question " + i + "\">" + strQuestion + "</label></td>";
                    strRow += "<td><select name=\"option" + i + "\" id=\"option" + i + "\"><option value=\"0\">Select One</option>";
                    break;
                case 7:  //attachment
                    if (strQuestion.Length > 80)
                    {
                        strRow += "<td colspan=\"2\">" + strReqFieldTag + "<label for=\"text" + i + "\" title=\"Question " + i + "\">" + strQuestion + "</label></td></tr>" + System.Environment.NewLine;
                    }
			        else
                    {
                        strRow += "<td><label for=\"text" + i + "\" title=\"Question " + i + "\">" + strQuestion + "</label></td>" + System.Environment.NewLine;
			        }
                    break;
                case 8: // Pre-populated DDLs
                    strRow += "<td>" + strReqFieldTag + "<label for=\"cluster\">" + strQuestion + "</label></td>";
                    if (arrDropDownListType[i] == 5)
                    {
                        if (strSelectedCategory == "") { strSelectedCategory = "2"; }
                        strRow += "<td><div style=\"margin-bottom:2px;\"><select name=\"cluster\" id=\"cluster\" onchange=\"javascript:popPrograms(this.selectedIndex,'option" + i + "');\">";
                        strRow += optProgramCategories(strSelectedCategory) + "</select></div>";
                        strRow += "<select name=\"option" + i + "\" id=\"option" + i + "\"><option value=\"0\">Select One</option>";
                    }
                    else
                    {
                        strRow += "<td><select name=\"option" + i + "\" id=\"option" + i + "\"><option value=\"0\">Select One</option>";
                    }
                    break;
            }
            //strResultMsg += "i: " + i + "; Blanks: " + intBlanks + "; Q: " + (i - intBlanks) + "<br />";

    	    //Now parse the answer options by question type, then survey type
            //strResultMsg += "arrQID " + i + ": " + arrQID[i] + "; ";
	        DataTable dt = GetSurveyQuestionOptions(intSurveyID, arrQID[i]);
            if (dt.Rows.Count > 0)
            {
                Int32 ziOffset = z - i, currentOptionNumber;
                foreach (DataRow dtRow in dt.Rows)
                {
                    z += 1;
                    currentOptionNumber = z - i - ziOffset;
                    //Forms with one-option fields remain on the same row
                    //strSelectTag = "";
                    //START THE ROW
                    if ((arrQuestionType[i] == 1 || arrQuestionType[i] == 2) && arrVertAlign[i])
                    {
                        strRow += "<tr>";
				    }
			        intOptionID = Convert.ToInt32(dtRow["WSO_OptionID"]);
			        strDescription = dtRow["WSO_Description"].ToString();
                    //strResultMsg += "QType " + i + ": " + arrQuestionType[i] + "; ";
                    //strResultMsg += "SurveyType: " + intSurveyType + "; ";
			        switch (arrQuestionType[i])
                    {
			            //Create cell innards
				        case 1:			//1 = "Multiple Choice - select 1 (radio)"
					        //if arrRequiredField[i] = true and strDescription = "N/A" then
					        if (arrVertAlign[i])	//vertical alignment
                            {
						        strOptionCell = strPlaceholderCell + "<td valign=\"top\" colspan=\"2\"><input id=\"button" + z + "\" type=\"radio\" tabindex=\"" + (i + intTabIndex) + "\" name=\"option" + i + "\" value=\"" + intOptionID + "\" title=\"Radio Button No. " + (i + intTabIndex) + "-" + currentOptionNumber + "\">";
                            }
					        else	//horizontal alignment
                            {
                                strOptionCell = strPlaceholderCell + "<td valign=\"top\"><input id=\"button" + z + "\" type=\"radio\" tabindex=\"" + (i + intTabIndex) + "\" name=\"option" + i + "\" value=\"" + intOptionID + "\" title=\"Radio Button No. " + (i + intTabIndex) + "-" + currentOptionNumber + "\">";
					        }
                            //<label for=\"option" + i + "\">" + strQuestion + "</label>
					        strOptionCell += "<label for=\"button" + z + "\" title=\"Radio Button No. " + (i + intTabIndex) + "-" + currentOptionNumber + "\">" + strDescription + "</label>";
					        //Reset tag 
					        //strSelectTag = "";
                            break;
				        case 2:			//2 = "Multiple Choice - Multiple Select (checkbox)"
					        if (arrVertAlign[i])		//vertical alignment
                            {
                                strOptionCell = strPlaceholderCell + "<td valign=\"top\" colspan=\"2\"><input id=\"box" + z + "\" type=\"checkbox\" tabindex=\"" + (i + intTabIndex) + "\" name=\"checkbox" + i + "\" value=\"" + intOptionID + "\" title=\"Checkbox No. " + (i + intTabIndex) + "-" + currentOptionNumber + "\">";
                                strOptionCell += "<label for=\"box" + z + "\" title=\"Checkbox No. " + (i + intTabIndex) + "-" + currentOptionNumber + "\">" + strDescription + "</label>";
                            }
					        else	//Create rows of 5 cells each - 	'horizontal alignment
                            {
						        k = k + 1;
						        if (k < 6)
                                {
							        if ((intSurveyType == 1 || intSurveyType == 2) || ! arrVertAlign[i]) //all surveys, or forms with horizontal alignment
                                    {
                                        strOptionCell = strPlaceholderCell + "<td valign=\"top\" ><input id=\"box" + z + "\" type=\"checkbox\" tabindex=\"" + (i + intTabIndex) + "\" name=\"checkbox" + i + "\" value=\"" + intOptionID + "\" title=\"Checkbox No. " + (i + intTabIndex) + "-" + currentOptionNumber + "\">";
							        }
                                    else if ((intSurveyType == 3 || intSurveyType == 4) && arrVertAlign[i])	//forms with vertical alignment
                                    {
                                        strOptionCell = strPlaceholderCell + "<td valign=\"top\" colspan=\"2\" ><input id=\"box" + z + "\" type=\"checkbox\" tabindex=\"" + (i + intTabIndex) + "\" name=\"checkbox" + i + "\" value=\"" + intOptionID + "\" title=\"Checkbox No. " + (i + intTabIndex) + "-" + currentOptionNumber + "\">";
							        }
                                    strOptionCell += "<label for=\"box" + z + "\" title=\"Checkbox No. " + (i + intTabIndex) + "-" + currentOptionNumber + "\">" + strDescription + "</label>";
                                }
						        else
                                {
							        if ((intSurveyType == 1 || intSurveyType == 2) || ! arrVertAlign[i])	//all surveys, or forms with horizontal alignment
                                    {
                                        strOptionCell = "</tr>" + System.Environment.NewLine + "<tr>" + strPlaceholderCell + "<td valign=\"top\"><input id=\"box" + z + "\" type=\"checkbox\" tabindex=\"" + (i + intTabIndex) + "\" name=\"checkbox" + i + "\" value=\"" + intOptionID + "\" title=\"Checkbox No. " + (i + intTabIndex) + "-" + currentOptionNumber + "\">";
							        }
                                    else if ((intSurveyType == 3 || intSurveyType == 4) && arrVertAlign[i]) //forms with vertical alignment
                                    {
                                        strOptionCell = "</tr>" + System.Environment.NewLine + "<tr>" + strPlaceholderCell + "<td valign=\"top\" colspan=\"2\"><inpu id=\"box" + z + "\"t type=\"checkbox\" tabindex=\"" + (i + intTabIndex) + "\" name=\"checkbox" + i + "\" value=\"" + intOptionID + "\" title=\"Checkbox No. " + (i + intTabIndex) + "-" + currentOptionNumber + "\">";
							        }
                                    strOptionCell += "<label for=\"box" + z + "\" title=\"Checkbox No. " + (i + intTabIndex) + "-" + currentOptionNumber + "\">" + strDescription + "</label>";
                                    k = 1;
                                }
						        blKCount = true;
                            }
                            break;
				        case 3:			//3 = "Fill In < 50 char"
                            if ((arrMaxLength[i] != null) && (arrMaxLength[i] != 0))
                            {
                                strMaxLength = " maxlength=\"" + arrMaxLength[i] + "\"";
                                intMaxLength = arrMaxLength[i];
                            }
                            else
                            {
                                strMaxLength = " maxlength=\"50\"" ;
                                intMaxLength = 50;
                            }
                            //Accumulate masks; inserts into script tags on display page.
                            if(arrMaskType[i] == 1) //DATE MASK
                            {
                                strMaskScript += "$(\"#text" + i + "\").mask(\"99/99/9999\");" + System.Environment.NewLine + "        ";
                                //MAY HAVE TO MAKE THIS A SESSION VARIABLE
                                blDisplayMaskScript = true;
                            }
                            else if (arrMaskType[i] == 2)    //PHONE MASK
                            {
                                strMaskScript += "$(\"#text" + i + "\").mask(\"(999) 999-9999\");" + System.Environment.NewLine + "        ";
                                //MAY HAVE TO MAKE THIS A SESSION VARIABLE
                                blDisplayMaskScript = true;
                            }
					        if (strQuestion.Length > 80)	//long question text
                            {
						        strOptionCell = "<tr>" + strPlaceholderCell + "<td colspan=\"2\"><input type=\"text\" tabindex=\"" + (i + intTabIndex)  + "\" id=\"text" + i + "\" name=\"text" + i + "\"" + strMaxLength + " size=\"" + intMaxLength + "\" title=\"Text Box No. " + (i + intTabIndex) + "-" + currentOptionNumber + "\">" + System.Environment.NewLine;
                            }
					        else	//short question text
                            {
                                if (blCloseFieldGroup)
                                {
						            strOptionCell = "<input type=\"text\" tabindex=\"" + (i + intTabIndex)  + "\" id=\"text" + i + "\" name=\"text" + i + "\"" + strMaxLength + " size=\"" + intMaxLength + "\" title=\"Text Box No. " + (i + intTabIndex) + "-" + currentOptionNumber + "\">&nbsp;&nbsp;&nbsp;&nbsp;";
                                }
                                else
                                {
                                    strOptionCell = "<td><input type=\"text\" tabindex=\"" + (i + intTabIndex) + "\" id=\"text" + i + "\" name=\"text" + i + "\"" + strMaxLength + " size=\"" + intMaxLength + "\" title=\"Text Box No. " + (i + intTabIndex) + "-" + currentOptionNumber + "\">";
                                }
					        }
					        strOptionCell += "<input type=\"hidden\" size=\"5\" name=\"hdnOptionID" + i + "\" value=\"" + intOptionID + "\" title=\"Text Box No. " + (i + intTabIndex) + "-" + currentOptionNumber + "\">";
                            break;
				        case 4:			//4 = "Fill In - Unlimited Text"
                            if ((arrMaxLength[i] != null) && (arrMaxLength[i] != 0))
                            {
                                strMaxLength = " maxlength=\"" + arrMaxLength[i] + "\"";
                                //MAY HAVE TO MAKE THIS A SESSION VARIABLE
                                blDisplayCountdownScript = true;
                                //Accumulate lines of script; inserts into script tags on display page.
                                strCountdownScript += "$('#" + "text" + i + "').limit('" + arrMaxLength[i] + "', '#" + "text" + i + "_charsLeft');" + System.Environment.NewLine;
                                strCountdownSpan = "(<span id=\"" + "text" + i + "_charsLeft\"></span> Characters Remaining)<br />";
                            }
                            else
                            {
                                strMaxLength = " maxlength=\"2000\"";
                                strCountdownSpan = "";
                            }
					        if (intSurveyType == 1 || intSurveyType == 2)	//all surveys
                            {
                                strOptionCell = "<tr><td" + strColSpanTag + ">" + strCountdownSpan + "<textarea tabindex=\"" + (i + intTabIndex) + "\"" + strMaxLength + " id=\"text" + i + "\" name=\"text" + i + "\" title=\"Text Box No. " + (i + intTabIndex) + "-" + currentOptionNumber + "\" cols=\"70\" rows=\"5\"></textarea>";
					        }
                            else if (intSurveyType == 3 || intSurveyType == 4)	//all forms
                            {
                                strOptionCell = "<tr>" + strPlaceholderCell + "<td colspan=\"2\">" + strCountdownSpan + "<textarea tabindex=\"" + (i + intTabIndex) + "\"" + strMaxLength + " id=\"text" + i + "\" name=\"text" + i + "\" title=\"Text Box No. " + (i + intTabIndex) + "-" + currentOptionNumber + "\" cols=\"70\" rows=\"5\"></textarea>";
					        }
					        strOptionCell += "<input type=\"hidden\" size=\"5\" name=\"hdnOptionID" + i + "\" value=\"" + intOptionID + "\" title=\"Text Box No. " + (i + intTabIndex) + "-" + currentOptionNumber + "\">" + System.Environment.NewLine;
                            break;
				        //case 5	Leave blank
				        case 6:			//1 = "Multiple Choice - select 1 (drop-down)"
					        strOptionCell = "<option label=\"" + strDescription + "\" value=\"" + intOptionID + "\" title=\"Drop-down Selection Box No. " + (i + intTabIndex) + "-" + currentOptionNumber + "\">" + strDescription + "</option>" + System.Environment.NewLine;
                            break;
                        case 7:          //7 = fill-in/attachment
                            if (intSurveyType == 3 || intSurveyType == 4)	//all forms
                            {
						        if (strQuestion.Length > 80)
                                {
                                    //long question text
                                    strOptionCell = "<tr>" + strPlaceholderCell + "<td colspan=\"2\"><input type=\"text\" tabindex=" + (i + intTabIndex) + " id=\"text" + i + "\" name=\"text" + i + "\" title=\"Text with Attachment No. " + (i + intTabIndex) + "-" + currentOptionNumber + "\" size=\"50\" maxlength=\"200\"> <input type=\"button\" name=\"btnPopUp\" value=\"Attach a File\" onclick=\"javascript:openNamedWindow('/_Upload/Default.aspx?ID=OSF&amp;Field=text" + i + "',960,800,'AttachFile');\">" + System.Environment.NewLine;
						        }
                                else	
                                {
                                    //short question text
                                    strOptionCell = "<td><input type=\"text\" tabindex=\"" + (i + intTabIndex) + "\" id=\"text" + i + "\" name=\"text" + i + "\" title=\"Text with Attachment No. " + (i + intTabIndex) + "-" + currentOptionNumber + "\" size=\"50\" maxlength=\"200\"> <input type=\"button\" name=\"btnPopUp\" value=\"Attach a File\" onclick=\"javascript:openNamedWindow('/_Upload/Default.aspx?ID=OSF&amp;Field=text" + i + "',960,800,'AttachFile');\">";
						        }
					        }
					        strOptionCell += "<input type=\"hidden\" size=\"5\" name=\"hdnOptionID" + i + "\" value=\"" + intOptionID + "\" title=\"Text with Attachment No. " + (i + intTabIndex) + "-" + currentOptionNumber + "\">";
                            break;
                        case 8:
                            try
                            {
                                CCSDropDownLists myDDL = new CCSDropDownLists();
                                ArrayList AL;
                                String strSelectTag = "";
                                switch (arrDropDownListType[i])
                                {
                                    case 1:
                                        //States
                                        // Returns a 51 element, zero-based array list of 50 states plus DC
                                        AL = myDDL.populateStates();
                                        for (Int32 idx = 0; idx < AL.Count; idx++)
                                        {
                                            if (AL[idx].ToString() == "Washington") { strSelectTag = " selected=\"selected\""; } else { strSelectTag = ""; }
                                            strOptionCell += "<option label=\"" + AL[idx].ToString() + "\" value=\"" + AL[idx].ToString() + "\"" + strSelectTag + ">" + AL[idx].ToString() + "</option>" + System.Environment.NewLine;
                                        }
                                        break;
                                    case 2:
                                        //Countries
                                        // Returns a zero-based ArrayList with alphabetical list of countries
                                        AL = myDDL.populateCountries();
                                        for (Int32 idx = 0; idx < AL.Count; idx++)
                                        {
                                            if (AL[idx].ToString() == "United States of America") { strSelectTag = " selected=\"selected\""; } else { strSelectTag = ""; }
                                            strOptionCell += "<option label=\"" + AL[idx].ToString() + "\" value=\"" + AL[idx].ToString() + "\"" + strSelectTag + ">" + AL[idx].ToString() + "</option>" + System.Environment.NewLine;
                                        }
                                        break;
                                    case 3:
                                        //Quarters using Term Code
                                        strOptionCell += optQuarters(true, 0, 4, "");
                                        break;
                                    case 4:
                                        //Quarters using Quarter Description
                                        strOptionCell += optQuarters(false, 0, 4, "");
                                        break;
                                    case 5:
                                        //Drill-down Programs
                                        strOptionCell += optProgramsByCategory(strSelectedCategory);
                                        break;
                                }
                                strOptionCell += "</select><input type=\"hidden\" size=\"5\" name=\"hdnOptionID" + i + "\" value=\"" + intOptionID + "\">";
                            }
                            catch (Exception ex)
                            {
                                strResultMsg += "Error populating drop-down list: " + ex.Message;
                            }
                            break;
                    }   //End switch
			        //Complete the cell and/or row
                    if (arrQuestionType[i] == 6 || arrQuestionType[i] == 8)
                    {
					    strRow += strOptionCell;
                    }
			        else if (arrVertAlign[i]  || arrQuestionType[i] == 4 || arrQuestionType[i] == 7)
                    {
                        //surveys with vertical alignment all Q types; plus 4, 7
					    strRow += strOptionCell + "</td></tr>" + System.Environment.NewLine;
                    }
			        else if (arrQuestionType[i] == 3) //Fill-ins 
                    {
                        if (arrFieldGroup[i])
                        {
					        strRow += strOptionCell;
                        }
                        else
                        {
					        strRow += strOptionCell + "</td></tr>" + System.Environment.NewLine;
                        }
                    }
                    else if (arrQuestionType[i] == 1 || arrQuestionType[i] == 2)	//questions with horizontal alignment; Q types 1 + 2 
                    {
				        strRow += strOptionCell + "</td>" + System.Environment.NewLine;
			        }
                    // reset string for next loop
                    strOptionCell = "";
                }   //End foreach
            }   //End row count > 0
            
            //COMPLETE ROWS
            if ((!arrVertAlign[i]) && (arrQuestionType[i] == 1 || arrQuestionType[i] == 2))
            {
			    if (blKCount)
                {
				    //fill in empty extra cells with &nbsp;
				    k = arrOptionCount[i] % 5;
				    if (k > 0 )
                    {
					    q = 5 - k;
                    }
				    else
                    {
					    q = 0;
                    }
				    if (q > 0)
                    {
					    for (j = 1; j < q; j++)
                        {
						    strRow += "<td valign=\"top\">&nbsp;</td>";
					    }
				    }
				    strRow += "</tr>" + System.Environment.NewLine + "</table>"  +  System.Environment.NewLine + "</td>" + System.Environment.NewLine + "</tr>" + System.Environment.NewLine;
                }
			    else
                {
				    strRow += "</tr>" + System.Environment.NewLine + "</table>"  +  System.Environment.NewLine + "</td>" + System.Environment.NewLine + "</tr>" + System.Environment.NewLine;
			    }
		    }
            if (arrQuestionType[i] == 6)	//Complete the select, cell and row
            {
                strRow += "</select></td></tr>" + System.Environment.NewLine;
            }
            if (arrQuestionType[i] == 8)	//Complete the cell and row
            {
                strRow += "</td></tr>" + System.Environment.NewLine;
            }
            //removed extra row spacing
	        //strRow += "<tr><td colspan=""2"">&nbsp;</td></tr>" + System.Environment.NewLine;
	        objBuilder.Append(strRow);
            //HttpContext.Current.Response.Write("Row: " + strRow);

            //Reset variables
	        blKCount = false;
	        k = 0;
	        strRow = "";
        }   //end FOR LOOP

        if (blCloseFieldGroup)	//Complete the previous Q type 3 row - last question
        {
	        objBuilder.Append("</td></tr>" + System.Environment.NewLine);
        }

        return objBuilder.ToString();
    }

    /// <summary>
    /// Increments tally count for each question option
    /// </summary>
    /// <param name="intTallyID"></param>
    /// <param name="intSurveyID"></param>
    /// <returns>String error/success message</returns>
    public String CalculateTallyResults(Int32 intTallyID, Int32 intSurveyID)
    {
        string strSQL1 = "pr_GetDistinctOptionIDs " + intTallyID.ToString();
        string strSQL2 = "";
        Int32 intResult = 0;
        string strSuccessMsg = "";
        string strOptionID = "";
        string strQID = "";
        //strResultMsg += "TallyID: " + intTallyID + " SurveyID: " + intSurveyID + "<br />"
        //if not isNumeric(intTallyID) then
        //    strResultMsg += "Cannot calculate results without a TallyID: " + intTallyID + "<br />"
        //    exit function
        //end if
        //strResultMsg += "CalculateTallyResults strSQL1: " + strSQL1 + "<br />";
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCommand = new SqlCommand();
        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSOnlineSurvey"].ToString();
        SqlDataAdapter objDataAdapter;
        DataSet ds = new DataSet();

        try
        {
            objConn.Open();
            objCommand.CommandText = strSQL1;
            objCommand.CommandType = CommandType.Text;
            objCommand.Connection = objConn;
            objDataAdapter = new SqlDataAdapter(objCommand);
            objDataAdapter.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dtRow in ds.Tables[0].Rows)
                {

                    strOptionID = dtRow["WSR_OptionID"].ToString();
                    strQID = dtRow["WSR_QID"].ToString();
                    //Run a stored procedure that calculates and inserts data into WebSurvey_TallyResults
                    strSQL2 = "pr_TallyResults " + intSurveyID.ToString() + ", " + intTallyID.ToString() + ", " + strQID + ", " + strOptionID;
                    //strResultMsg += "CalculateTallyResults strSQL2: " + strSQL2 + "<br />";
                    SqlCommand objCommand2 = new SqlCommand();
                    try
                    {
                        objCommand2.CommandText = strSQL2;
                        objCommand2.CommandType = CommandType.Text;
                        objCommand2.Connection = objConn;
                        //Execute NonQuery
                        intResult = objCommand2.ExecuteNonQuery();
                        strSuccessMsg += "Success";
                    }
                    catch (Exception ex)
                    {
                        //capture error
                        strResultMsg += "Error CalculateTallyResults: " + ex.Message + "<br />";
                    }
                    finally
                    {
                        objCommand2.Dispose();
                    }
                }
            }
        }
        catch (Exception ex)
        {
            //capture error
            strResultMsg += "Error CalculateTallyResults: " + ex.Message + "<br />";
        }
        finally
        {
            objConn.Close();
            objConn.Dispose();
            objCommand.Dispose();
        }
        return strSuccessMsg;
    }


    /// <summary>
    /// Insert a User record to identify responses
    /// </summary>
    /// <param name="intTallyID"></param>
    /// <param name="intSurveyID"></param>
    /// <param name="strUserLName"></param>
    /// <param name="strUserFName"></param>
    /// <param name="strUserEmail"></param>
    /// <param name="strLogonID"></param>
    /// <param name="strSystemID"></param>
    /// <returns>Error messages only</returns>
    public Int32 CreateUser(Int32 intTallyID, Int32 intSurveyID, string strUserLName, string strUserFName, string strUserEmail, string strLogonID, string strSystemID)
    {
        string strSQL = "";
        blReturnIdentity = true;
        blIsSQL = true;
        blASCIIToHTML = false;
        blHTMLToASCII = false; Utility Fixer = new Utility();
        strUserLName = Fixer.ConvertAndFixText(strUserLName, blASCIIToHTML, blHTMLToASCII, blIsSQL);
        strUserFName = Fixer.ConvertAndFixText(strUserFName, blASCIIToHTML, blHTMLToASCII, blIsSQL);
        strUserEmail = Fixer.ConvertAndFixText(strUserEmail, blASCIIToHTML, blHTMLToASCII, blIsSQL);

        strSQL = "pr_InsertWebSurvey_User ";
        strSQL += intTallyID + ", " + intSurveyID + ", '" + strUserLName + "', '";
        strSQL += strUserFName + "', '" + strUserEmail + "', '" + strLogonID + "', '" + strSystemID + "'";

        //Run the query, which captures any error message in strResultMsg
        intNewIdentity = runOSSQLQuery(strSQL, blReturnIdentity);
        if (intNewIdentity > 0)
        {
            //strResultMsg += "Your User was added successfully!<br />";
        }
        else
        {
            strResultMsg += "Error CreateUser: No WebSurvey_User rows were affected.<br />";
            strResultMsg += "CreateUser SQL: " + strSQL + "<br />";
       }
        return intNewIdentity;
    }

    /// <summary>
    /// Add comments submitted by user after taking survey
    /// </summary>
    /// <param name="intTallyID"></param>
    /// <param name="intUserID"></param>
    /// <param name="strComments"></param>
    /// <returns>Result or error messages</returns>
    public String AddUserComments(Int32 intTallyID, Int32 intUserID, string strComments)
    {
        string strSQL = "";
        blReturnIdentity = false;
        blIsSQL = true;
        blASCIIToHTML = false;
        blHTMLToASCII = false; Utility Fixer = new Utility();
        strComments = Fixer.ConvertAndFixText(strComments, blASCIIToHTML, blHTMLToASCII, blIsSQL);
        strSQL = "pr_UpdateWebSurvey_UserComments ";
        strSQL += intTallyID + ", " + intUserID + ", '";
        strSQL += strComments + "'";

        //Run the query, which captures any error message in strResultMsg
        Int32 intResult = runOSSQLQuery(strSQL, blReturnIdentity);
        //strResultMsg += "Add User Comments SQL: " + strSQL + "<br />";
        if (intResult == 0)
        {
            strResultMsg += "Error: Comments were not successfully added.<br />";
        }
        return strResultMsg;
    }

    /// <summary>
    /// Determine: has a user has made a submission? per tally ID by SID
    /// </summary>
    /// <param name="intTallyID"></param>
    /// <param name="strSID"></param>
    /// <returns>Int32 1 = true; 0 = false</returns>
    public Int32 ValidateSubmitOnceSID(Int32 intTallyID, string strSID)
    {
        Int32 intStatus = 0;
        string strSQL = "pr_ValidateSubmitBySID";
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCommand = new SqlCommand();
        SqlParameter objParam = new SqlParameter();
        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSOnlineSurvey"].ToString();
        try
        {
            objConn.Open();
            objCommand.CommandText = strSQL;
            objCommand.CommandType = CommandType.StoredProcedure;
            objCommand.Connection = objConn;

            objParam.ParameterName = "@TallyID";
            objParam.SqlDbType = SqlDbType.Int;
            objParam.Value = intTallyID;
            objCommand.Parameters.Add(objParam);

            objParam = new SqlParameter();
            objParam.ParameterName = "@SID";
            objParam.SqlDbType = SqlDbType.Char;
            objParam.Size = 9;
            objParam.Value = strSID;
            objCommand.Parameters.Add(objParam);

            objParam = new SqlParameter();
            objParam.ParameterName = "@Status";
            objParam.SqlDbType = SqlDbType.Int;
            objParam.Direction = ParameterDirection.Output;
            objCommand.Parameters.Add(objParam);
            objCommand.ExecuteReader();
            intStatus = Convert.ToInt32(objCommand.Parameters["@Status"].Value);
        }
        catch
        {
            return intStatus;
        }
        finally
        {
            objConn.Close();
            objCommand.Dispose();
        }
        return intStatus;
    }

    /// <summary>
    /// Verify an owner is authorized to access submission results by Profile ID
    /// </summary>
    /// <param name="strEmplID"></param>
    /// <param name="strSurveyID"></param>
    /// <returns>Boolean true = authorized; false = not authorized.</returns>
    public Boolean ValidateOwner(string strEmplID, string strSurveyID)
    {
        string strSQL = "pr_ValidateOwner '" + strEmplID + "', " + strSurveyID;

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCommand = new SqlCommand();
        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSOnlineSurvey"].ToString();
        SqlDataAdapter objDataAdapter;
        DataSet ds = new DataSet();

        try
        {
            objConn.Open();
            objCommand.CommandText = strSQL;
            objCommand.CommandType = CommandType.Text;
            objCommand.Connection = objConn;
            objDataAdapter = new SqlDataAdapter(objCommand);
            objDataAdapter.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                blSuccess = true;
		    }
        }
        catch (Exception ex)
        {
            //capture error
            strResultMsg += "Error: " + ex.Message + "<br />";
            return false;
        }
        finally
        {
            objConn.Close();
            objConn.Dispose();
            objCommand.Dispose();
        }

        return blSuccess;
       }

    public string DisplayOwnerReport(string strSelOwner)
    {
        string strTypeLabel = "";
        string strSQL = "";
        string strOpenDate = "";
        string strCloseDate = "";
        string strTallyID = "";
        Int32 intLength = 0;
        StringBuilder objBuilder = new StringBuilder();
        DataTable dtReport = GetOwnerSurveys(strSelOwner);
        if (dtReport.Rows.Count > 0) {
	        foreach (DataRow drReport in dtReport.Rows) {
		        intSurveyID = Convert.ToInt32(drReport["WS_SurveyID"]);
		        //strOwnerUserID = rsGetSurvey["WO_UserID"]
		        intQuestionCount = Convert.ToInt32(drReport["WS_QuestionCount"]);
		        //strOwnerFName = rsGetSurvey["WO_FName"]
		        //strOwnerLName = rsGetSurvey["WO_LName]
		        intSurveyType = Convert.ToInt32(drReport["WS_SurveyType"]);
		        strCreateDate = Convert.ToDateTime(drReport["WS_CreateDate"]).ToShortDateString();
                intLength = strCreateDate.Length;
                if (intLength > 12)
                {
                    strCreateDate = strCreateDate.Substring(0, intLength - 11);
                }
		        strSurveyTitle = drReport["WS_SurveyTitle"].ToString();
		        switch (intSurveyType) {
			        case 1:
				        strTypeLabel = "Secured Survey";
                        break;
			        case 2:
				        strTypeLabel = "Public Survey";
                        break;
			        case 3:
				        strTypeLabel = "Secured Form";
                        break;
			        case 4:
				        strTypeLabel = "Public Form";
                        break;
			        case 5:
				        strTypeLabel = "Quiz";
                        break;
		        }
	        //CREATE ROWS WITH THE SURVEYS BY NUMBER, TITLE, CREATE DATE
		        objBuilder.Append("<tr class=\"bgColorTint\"><td colspan=\"2\"><strong>" + strSurveyTitle + "</strong></td><td><strong>Type:</strong> " + strTypeLabel + "</td></tr>");
		        objBuilder.Append("<tr class=\"bgGreyTint\"><td>Survey ID: " + intSurveyID + "</td><td>Create Date: " + strCreateDate + "</td><td>Question Count: " + intQuestionCount + "</td></tr>");
		        strSQL = "pr_GetSelectWebTallies " + intSurveyID;
                DataTable dtTallies = RunGetQuery(strSQL);
                if (dtTallies.Rows.Count > 0) {
			        foreach (DataRow drTally in dtTallies.Rows) {
                        if (drTally["WST_DateOpen"] != null)
                        {
                            strOpenDate = Convert.ToDateTime(drTally["WST_DateOpen"]).ToShortDateString();
                            intLength = strOpenDate.Length;
                            if (intLength > 12)
                            {
                                strOpenDate = strOpenDate.Substring(0, intLength - 11);
                            }
                        }
                        if (drTally["WST_DateClosed"] != null)
                        {
                            strCloseDate = Convert.ToDateTime(drTally["WST_DateClosed"]).ToShortDateString();
                            intLength = strCloseDate.Length;
                            if (intLength > 12)
                            {
                                strCloseDate = strCloseDate.Substring(0, intLength - 11);
                            }
                        }
                        strTallyID = drTally["WST_TallyID"].ToString();
                        //CREATE TABLE ROWS CONTAINING THE TALLIES
                        objBuilder.Append("<tr><td>Tally ID: " + strTallyID + "</td><td>Open Date: " + strOpenDate + "</td><td>Close Date: " + strCloseDate + "</td></tr>" + System.Environment.NewLine);
				    }
		        //else
                    //strResultMsg = strResultMsg & "Error opening Tallies recordset: " & blSuccess & "<br />"
                }
            }   //foreach
        //else
        //strResultMsg = strResultMsg & "Error opening Surveys recordset: " & blSuccess & "<br />"
        }
        return objBuilder.ToString();
    }

    /// <summary>
    /// Insert a Response to a survey question
    /// </summary>
    /// <param name="intSurveyID"></param>
    /// <param name="intTallyID"></param>
    /// <param name="intQID"></param>
    /// <param name="intOptionID"></param>
    /// <param name="strResponseText"></param>
    /// <param name="intUserID"></param>
    /// <returns>String - Error messages only</returns>
    public String CreateResponse(Int32 intSurveyID, Int32 intTallyID, Int32 intQID, Int32 intOptionID, string strResponseText, Int32 intUserID)
    {
        //Assumes FixText has been applied to response text
        string strSQL = "";
        blReturnIdentity = true;
        strSQL = "pr_InsertWebSurvey_Responses ";
        strSQL += intSurveyID + ", " + intTallyID + ", " + intQID + ", " + intOptionID;
        strSQL += ", '" + strResponseText + "', " + intUserID;
        //if intOptionID = "" then exit function
        //Run the query, which captures any error message in strResultMsg
        intNewIdentity = runOSSQLQuery(strSQL, blReturnIdentity);
        if (intNewIdentity > 0)
        {
            //strResultMsg += "Your Response was created successfully!<br />";
        }
        else
        {
            strResultMsg += "Error CreateResponse: No WebSurvey_Responses were affected.<br />";
            strResultMsg += "CreateResponse SQL: " + strSQL + "<br />";
        }
        return strResultMsg;

    }


    /// <summary>
    /// Set up table tags; function creates rows with one cell each.  
    /// Users displayed in a nested table with rows of 3 cells each.
    /// Results displayed in a nested table with 5 cells each.
    /// </summary>
    /// <param name="intTallyID"></param>
    /// <param name="intSurveyID"></param>
    /// <param name="blAnonymous"></param>
    /// <returns>String of table rows</returns>
    public String DisplayTallyResults(Int32 intTallyID, Int32 intSurveyID, bool blAnonymous)
    {
        StringBuilder objBuilder = new StringBuilder();
        //strResultMsg += "DisplayTallyResults: " + intTallyID + ", " + intSurveyID + ", " + blAnonymous + ".<br />";

        //dim strSQL
        int intQCount = 0;
        int i = 0;
        bool blContinue = false;
        bool blTally = false;
        string strDescription = "";
        string strResponseText = "";
        string strWSTR_Count = "0";
        string strUserInfo = "";
        if (intTallyID == 0)
        {
            strResultMsg += "A valid Tally is required to display results.<br />";
	        return strResultMsg;
        }

        DataTable dt = GetAllSurveyQuestions(intSurveyID);
        intQCount = LoadQuestionArrays(intSurveyID);

        //Get rsGetTallyResults recordset
        DataTable dtTally = GetSurveyTallyResults(intTallyID);
        if (dtTally.Rows.Count > 0)
        {
	        blTally = true;
        }
        for (i = 0; i < intQCount; i++)
        {

	        if (arrQuestionType[i] == 5)
            {
		        objBuilder.Append("<tr class=\"BGColorTint\"><td>" + arrQuestions[i] + "</td></tr>" + System.Environment.NewLine);
            }
	        else
            {
		        objBuilder.Append("<tr class=\"BGColorTint\"><td><strong>" + arrQuestions[i] + "</strong></td></tr>" + System.Environment.NewLine);
	        }
            if (arrQuestionType[i] == 1 || arrQuestionType[i] == 2 || arrQuestionType[i] == 6)  //Multiple Choice 
            {
	            //Open rsGetOptions recordset
		        DataTable dtOpts = GetSurveyQuestionOptions(intSurveyID, arrQID[i]);
                if (dtOpts.Rows.Count > 0)
                {
			        blContinue = true;
                }
		        else
                {
			        blContinue = false;
                    strResultMsg += "Error retrieving Tally Results data: please contact the <a href=\"mailto:ITSupportCenter@ccs.spokane.edu\">IT Support Center</a>.<br />";
			        return strResultMsg;
		        }
		        objBuilder.Append("<tr><td><table class=\"ItemList\">");
		        //strResultMsg = strResultMsg & "Before: " & intOptionID & " count: " & rsGetTallyResults("WSTR_Count") & "<br />"
		        if (blContinue)
                {
                    foreach (DataRow dtOptsRow in dtOpts.Rows)
                    {
                        intOptionID = Convert.ToInt32(dtOptsRow["WSO_OptionID"]);
                        strDescription = dtOptsRow["WSO_Description"].ToString();
				        if (blTally)
                        {
                            foreach (DataRow dtTallyRow in dtTally.Rows)
                            {
					            //rsGetTallyResults.Find "WSTR_OptionID = " & intOptionID, , 1, 1
                                if (intOptionID == Convert.ToInt32(dtTallyRow["WSTR_OptionID"]))
                                {
                                    //do something!
                                    strWSTR_Count = dtTallyRow["WSTR_Count"].ToString();
                                }
                            }
				        }
					    objBuilder.Append("<tr><td>" + strDescription + "</td><td style=\"vertical-align:top;\">" + "Count: </td><td style=\"vertical-align:top;\">"); 
					    objBuilder.Append(strWSTR_Count + "&nbsp;</td></tr>" + System.Environment.NewLine);
				        //strResultMsg = strResultMsg & "After: " & intOptionID & " count: " & rsGetTallyResults("WSTR_Count") & "<br />"
                        //Reset default value
                        strWSTR_Count = "0";
                    }
		        }
		        objBuilder.Append("</table></td></tr>" + System.Environment.NewLine);
            }
	        else if (arrQuestionType[i] == 3 || arrQuestionType[i] == 4)	//Fill In   
            {
		       //Get Text Responses 
		       DataTable dtResponses = GetTallyResponsesByQID(intTallyID, arrQID[i]);
		       if(dtResponses.Rows.Count > 0)
               {
				    //process each response: create a row of five cells
				    foreach (DataRow dtRow in dtResponses.Rows)
                    {
					    strResponseText = dtRow["WSR_ResponseText"].ToString();
					    if (strResponseText != "" && strResponseText != "&nbsp;")
                        {
                            objBuilder.Append("<tr><td style=\"border:1px solid #D3D3D3;\"><p>" + strResponseText + "</p>");
						    //if Len(strResponseText) > 125 then
						    //    objBuilder.Append("<br /><hr width=""80%""></p>"
						    //end if
						    objBuilder.Append("</td></tr>" + System.Environment.NewLine);
					    }
                    }
                }
            }
        }   //for loop
        //Get User Comments and info, if not anonymous
        DataTable dtUsers = GetSurveyUsers(intTallyID);
        if (dtUsers.Rows.Count > 0)
        {
	        strUserInfo += "<tr class=\"bgGreyTint\"><td><strong>Comments</strong></td></tr>" + System.Environment.NewLine;
	        strUserInfo += "<tr><td><table class=\"ItemList\" style=\"width:650px;\">";
	        i = 0;
	        foreach (DataRow dtUsersRow in dtUsers.Rows)
            {
		        if (dtUsersRow["WSU_UserLName"].ToString() != "")
                {
			        strUserInfo += "<tr><td><strong>Name:  </strong></td><td>" ;
			        strUserInfo += dtUsersRow["WSU_UserFName"].ToString() + " " + dtUsersRow["WSU_UserLName"].ToString() + "&nbsp;&nbsp;&nbsp;</td>";
			        strUserInfo += "<td><strong>ctcLink ID:  </strong></td><td>"; 
			        strUserInfo += dtUsersRow["WSU_StudentID"].ToString() + "</td></tr>" + System.Environment.NewLine;
			        strUserInfo += "<tr><td colspan=\"2\"><strong>Email:  </strong></td><td colspan=\"2\" >" + dtUsersRow["WSU_UserEmail"].ToString() + "</td></tr>" + System.Environment.NewLine;
		        }
		        if (dtUsersRow["WSU_Comments"].ToString().Length > 6)
                {
			        i += 1;
			        strUserInfo += "<tr><td style=\"vertical-align:top;\"><strong>Comment&nbsp;" + i + ":</strong></td><td colspan=\"3\" >" + dtUsersRow["WSU_Comments"].ToString() + "</td></tr>" + System.Environment.NewLine;
		        }
            }
            //Spacer row - no longer used
	        //strUserInfo += "<tr><td colspan=\"4\" >&nbsp;</td></tr>";
	        strUserInfo += "</table></td></tr>" + System.Environment.NewLine;
	        objBuilder.Append(strUserInfo);
        }
        return objBuilder.ToString();
    }

#endregion

#region "PROCESS/ADMIN FUNCTIONS"



    /// <summary>
    /// Delete Tally Results only; ID remains for re-use
    /// </summary>
    /// <param name="intTallyID"></param>
    /// <returns>Result or error messages</returns>
    public String DeleteTallyResults(Int32 intTallyID)
    {
        string strSQL = "";
        Int32 intResult = 0;
        bool blIsError = false;
        blReturnIdentity = false;

	    //Delete existing results starting from responses, working up to users
        //Delete Responses
        strSQL = "pr_DeleteSurveyResponses " + intTallyID;
        //Run the query, which captures any error message in strResultMsg
        intResult = runOSSQLQuery(strSQL, blReturnIdentity);
        if(blSuccess)
        {
            //Delete Results
            strSQL = "pr_DeleteSurveyTallyResults " + intTallyID;
            //Run the query, which captures any error message in strResultMsg
            intResult = runOSSQLQuery(strSQL, blReturnIdentity);
            if (blSuccess)
            {

                //Delete Users
                strSQL = "pr_DeleteSurveyUser " + intTallyID;
                //Run the query, which captures any error message in strResultMsg
                intResult = runOSSQLQuery(strSQL, blReturnIdentity);
                if (blSuccess)
                {
                    //Reset Tally User Count
                    strSQL = "pr_ResetTallyUserCount " + intTallyID;
                    //Run the query, which captures any error message in strResultMsg
                    intResult = runOSSQLQuery(strSQL, blReturnIdentity);
                    if (!blSuccess)
                    {
                        blIsError = true;
                    }
                }
            }
            else
            {
                blIsError = true;
            }
        }
        else
        {
            blIsError = true;
        }

        if(blIsError)
        {
            strResultMsg += "Problems were encountered while deleting your Tally.<br />";
        }
        else
        {
            strResultMsg += "Your Tally Results were successfully deleted!<br />";
        }

        return strResultMsg;
    }

    /// <summary>
    /// Produces readable, tallied resultset formatted in paragraphs from survey/form
    /// </summary>
    /// <param name="intSurveyID"></param>
    /// <param name="intResultsOption"></param>
    /// <returns>String</returns>
    public String ProcessEmailResults(Int32 intSurveyID, Int32 intResultsOption)
    {
        StringBuilder objBuilder = new StringBuilder();
        int i = 0;
        int j = 0;
        int intQCount = 0;
        string strTemp = "";
        string strQType = "";   //Modular scope
        string strOptionBox = "";   //Modular scope
        int intCheckID = 0;
        int intOCount = 0;
        Int32 intOptionID = 0;
        string strCheckBox = "";
        string strTextBox = "";
        string strResponseText = "";
        //LOAD THE ARRAYS
        intQCount = LoadQuestionArrays(intSurveyID);
        //HttpContext.Current.Response.Write("Q Count: " + intQCount + "<br />");
        //HttpContext.Current.Response.Write(HttpContext.Current.Request.Form["hdnQType"]);
        //HttpContext.Current.Response.Write(HttpContext.Current.Request.Form["hdnOCount"]);
        string[] arrQType = HttpContext.Current.Request.Form["hdnQType"].Split(',');
        string[] arrOCount = HttpContext.Current.Request.Form["hdnOCount"].Split(',');

        if (intQCount < 1)
        {
            return objBuilder.ToString();
        }
        for (i = 0; i < intQCount; i++)
        {
            try
            {
                //Get labels
                //If option 2, do not return labels
                if (intResultsOption < 2)
                {
                    strTemp = arrQuestions[i];
                    if (strTemp.Contains("<p>"))
                    {
                        strTemp = strTemp.Replace("<p>", "").Replace("</p>", "");
                        //strTemp = strTemp.Replace("<p>", "");
                        //strTemp += strTemp.Replace("</p>", "");
                    }
                    if (arrQType[i] == "4") //fill in > 50: line break before response
                    {
                        objBuilder.Append("<strong>" + strTemp + "</strong><br />");
                    }
                    else
                    {
                        objBuilder.Append("<strong>" + strTemp + "</strong>&nbsp;&nbsp;");
                    }
                }
                //Get results by question type
                strQType = arrQType[i];
                DataTable dtOpt = GetSurveyQuestionOptions(intSurveyID, arrQID[i]);
                if (dtOpt.Rows.Count > 0)
                {

                    //intQID = HttpContext.Current.Request.Form["hdnQID"][i];
                    if (strQType == "1" || strQType == "6")	//Multiple Choice - radio or drop-down
                    {
                        strOptionBox = "option" + i;
                        if (HttpContext.Current.Request.Form[strOptionBox] == null)
                        {
                            intOptionID = 0;
                        }
                        else
                        {
                            intOptionID = Convert.ToInt32(HttpContext.Current.Request.Form[strOptionBox]);
                        }
                        //HttpContext.Current.Response.Write("Multiple Choice - radio/drop-down OptionID: " + intOptionID + "<br />");
                        if (intOptionID != 0)
                        {
                            foreach (DataRow dtRow in dtOpt.Rows)
                            {
                                if (intOptionID == Convert.ToInt32(dtRow["WSO_OptionID"]))
                                {
                                    strDescription = dtRow["WSO_Description"].ToString();
                                    objBuilder.Append(strDescription);
                                    //HttpContext.Current.Response.Write("Description: " + strDescription + "<br />");
                                }
                            }
                        }
                        //else
                        //{
                        //    HttpContext.Current.Response.Write("Unable to access QType " + strQType + " for " + strOptionBox + "<br />");
                        //} 
                        objBuilder.Append("<br />");
                    }
                    else if (strQType == "2") //Multiple Choice - checkbox
                    {
                        intOCount = Convert.ToInt32(arrOCount[i]);
                        intCheckID = 0;
                        strCheckBox = "checkbox" + i;
                        if (HttpContext.Current.Request.Form[strCheckBox] != null)
                        {
                            string strValue = HttpContext.Current.Request.Form[strCheckBox];
                            //strResultMsg += "Multiple Choice - checkbox value: " + strValue + "<br />";
                            if (strValue.Contains(","))
                            {
                                string[] arrCheckBox = strValue.Split(',');
                                int intUBound = arrCheckBox.GetUpperBound(0);
                                //0-based array in .Net
                                for (j = 0; j <= intUBound; j++)
                                {
                                    intOptionID = Convert.ToInt32(arrCheckBox[j]);
                                    //Prevent duplicate submissions
                                    if (intOptionID != intCheckID)
                                    {
                                        intCheckID = intOptionID;
                                        foreach (DataRow dtRow in dtOpt.Rows)
                                        {
                                            //strResultMsg = strResultMsg & " Descr: " & strDescription & "<br />"
                                            if (intOptionID == Convert.ToInt32(dtRow["WSO_OptionID"]))
                                            {
                                                strDescription = dtRow["WSO_Description"].ToString();
                                                if (j == 0)
                                                {
                                                    objBuilder.Append(strDescription);
                                                }
                                                else
                                                {
                                                    objBuilder.Append(", " + strDescription);
                                                }

                                            }
                                        }
                                    }
                                }
                            }
                            else if (strValue != "")
                            {
                                //Not an array; one response only
                                intOptionID = Convert.ToInt32(strValue);
                                foreach (DataRow dtRow in dtOpt.Rows)
                                {
                                    if (intOptionID == Convert.ToInt32(dtRow["WSO_OptionID"]))
                                    {
                                        strDescription = dtRow["WSO_Description"].ToString();
                                        objBuilder.Append(strDescription);
                                    }
                                }
                            }
                        }
                        //else nothing checked; move on
                        objBuilder.Append("<br />");
                    }
                    else if (strQType == "3" || strQType == "4")	//Fill In - short; Fill In - Essay
                    {
                        strResponseText = "";
                        strTextBox = "text" + i;
                        if(HttpContext.Current.Request.Form[strTextBox] != null)
                        {
                            strResponseText = HttpContext.Current.Request.Form[strTextBox].ToString();
                        }
                        objBuilder.Append(strResponseText + "<br />");
                    }
                    else if (strQType == "5")	//Blanks
                    {
                        objBuilder.Append("<br />");
                    }
                    else if (strQType == "7")
                    {
                        strTextBox = "text" + i;
                        if (HttpContext.Current.Request.Form[strTextBox] == null)
                        {
                            if (intResultsOption < 2 && strFileName == "")
                            {
                                objBuilder.Append("(No Attachment)<br />");
                            }
                        }
                        else if (HttpContext.Current.Request.Form[strTextBox] != "")
                        {
                            //strUploadURL is a constant set in declarations
                            //Some folks use this field to collect URLs - these are not attachments to emails.
                            if (HttpContext.Current.Request.Form[strTextBox].Substring(0, 4) != "http")
                            {
                                //File name for other type 7 questions may also have already been captured; hence +=
                                strFileName += strUploadURL + HttpContext.Current.Request.Form[strTextBox].ToString() + ";";
                            }
                            objBuilder.Append("Attachment: " + HttpContext.Current.Request.Form[strTextBox].ToString() + "<br />");
                        }
                    }
                    else if (strQType == "8") //pre-populated DDLs
                    {
                        strOptionBox = "option" + i;
                        strResponseText = HttpContext.Current.Request.Form[strOptionBox].ToString();
                        objBuilder.Append(strResponseText + "<br />");
                    }
                }
                //else
                //{
                //    HttpContext.Current.Response.Write("Error: No Options<br />");
                //}
            }
            catch (Exception ex)
            {
                HttpContext.Current.Response.Write("Error processing email results: " + ex.Message + "<br />");
            }
        }
        //strResultMsg += "File Before: " + strFileName + "<br />";
        //Remove final semicolon
        if (strFileName != "" && strFileName.Contains(";"))
        {
            strFileName = strFileName.Substring(0, strFileName.Length - 1);
        }
        //strResultMsg += "File After: " + strFileName + "<br />";
        return objBuilder.ToString();
    }

    /// <summary>
    /// Creates a display in form format to enable processing results
    /// </summary>
    /// <param name="intTallyID"></param>
    /// <param name="intSurveyID"></param>
    /// <param name="strViewAll"></param>
    /// <param name="mblPrintFormResults"></param>
    /// <param name="intPageBreak"></param>
    /// <returns>String of formatted results</returns>
    public String DisplayFormResults(Int32 intTallyID, Int32 intSurveyID, string strViewAll, bool mblPrintFormResults, Int32 intPageBreak, Int32 intSurveyType )
    {
        //ADDED TWO ADDITIONAL PARAMETERS - ADJUST CALLING FUNCTIONS
        Int32 i = 0;
        Int32 j = 0;
        StringBuilder objBuilder = new StringBuilder();
        StringBuilder objMultiOption = new StringBuilder();
        Int32 intQCount = 0;
        string strDescription = "";
        string strResponseText = "";
        Int32 intThisUserID = 0;
        string strUserInfo = "";
        bool blResponseSent = false;
        bool blSingleOption = false;
        Int32 intThisQID = 0;
        string strTestQuestion = "";
        //NOTE THAT A MODULAR VARIABLE mblPrintFormResults MUST BE SET TO TRUE
        //FROM THE CALLING PAGE TO REMOVE PROCESSING FIELDS AND SUBMIT BUTTON FOR 
        //PRINTING.  DEFAULT IS TO INCLUDE ALL THAT FROM THE VIEW FORM RESULTS PAGE.

        if (intTallyID == 0)
        {
	        strResultMsg += "A valid Tally is required to display results.<br />";
	        return strResultMsg;
        //else
	        //strResultMsg = strResultMsg & "Tally ID processed: " & intTallyID & ".<br />"
        }

	    intQCount = LoadQuestionArrays(intSurveyID);
        //strResultMsg += "Survey ID: " + intSurveyID + ".<br />";
        //strResultMsg += "Tally ID: " + intTallyID + ".<br />";
        //strResultMsg += "QUESTION count: " + intQCount + ".<br />";
        DataTable dtUsers = new DataTable();
        if (strViewAll == "All")
        {
	        dtUsers = GetSurveyUsers(intTallyID);
        }
        else
        {
	        dtUsers = GetSelectSurveyUsers(intTallyID);
        } 
        if (dtUsers.Rows.Count == 0)
        {
            strResultMsg += "<br />There are no User records to display.<br />";
	        return strResultMsg;
        }
        i = 1;
        j = 0;
        foreach (DataRow dtUsersRow in dtUsers.Rows)
        {
	        j += 1;
            //Reset variable
	        strUserInfo = "";
	        intThisUserID = Convert.ToInt32(dtUsersRow["WSU_UserID"]);
            strUserInfo += "<tr><td><strong>Timestamp:</strong></td><td>" + Convert.ToDateTime(dtUsersRow["WSU_Date"]).ToString() +"</td></tr>" + System.Environment.NewLine;
	        if (mblPrintFormResults)
            {
		        if (dtUsersRow["WSU_StudentID"].ToString() != "")
                {
                    strUserInfo += "<tr><td><strong>ctcLink ID:</strong></td><td>" + dtUsersRow["WSU_StudentID"].ToString() + "</td></tr>" + System.Environment.NewLine;
		        }	
		        if (dtUsersRow["WSU_UserFName"].ToString() != "" || dtUsersRow["WSU_UserLName"].ToString() != "")
                {
                    strUserInfo += "<tr><td><strong>Name:</strong></td><td>" + dtUsersRow["WSU_UserFName"].ToString() + " ";
			        strUserInfo += dtUsersRow["WSU_UserLName"] + "</td></tr>" + System.Environment.NewLine;
		        }
		        if (dtUsersRow["WSU_UserEmail"].ToString() != "")
                {
                    strUserInfo += "<tr><td><strong>User Email:</strong></td><td>" + dtUsersRow["WSU_UserEmail"].ToString() + "</td></tr>" + System.Environment.NewLine;
		        }	
            }
	        else
            {
		        //strResultMsg = strResultMsg + "i: " + i + "; j: "+ j + "; intThisUserID: " + intThisUserID + ".<br />"
			    strUserInfo += "<tr><td><strong>Record No.</strong></td><td>" + dtUsersRow["WSU_UserID"].ToString();
                strUserInfo += "<input type=\"hidden\" name=UserID" + j + " value=\"" + dtUsersRow["WSU_UserID"].ToString() + "\"></td></tr>" + System.Environment.NewLine;
		        if (dtUsersRow["WSU_StudentID"].ToString() != "")
                {
                    strUserInfo += "<tr><td><strong>ctcLink ID:</strong></td><td>" + dtUsersRow["WSU_StudentID"].ToString() + "</td></tr>" + System.Environment.NewLine;
                }	
		        if (dtUsersRow["WSU_UserFName"].ToString() != "")
                {
                    strUserInfo += "<tr><td><strong>Name:</strong></td><td>" + dtUsersRow["WSU_UserFName"].ToString() + " ";
			        strUserInfo += dtUsersRow["WSU_UserLName"] + "</td></tr>" + System.Environment.NewLine;
		        }
		        if (dtUsersRow["WSU_UserEmail"].ToString() != "")
                {
                    strUserInfo += "<tr><td><strong>User Email:</strong></td><td>" + dtUsersRow["WSU_UserEmail"].ToString() + "</td></tr>" + System.Environment.NewLine;
		        }
	            //strResultMsg = strResultMsg + "ResponseSent: " + dtRow["WSU_ResponseSent"] + "<br />"
		        blResponseSent = Convert.ToBoolean(dtUsersRow["WSU_ResponseSent"]);
		        if (blResponseSent)
                {
			        strUserInfo += "<tr><td><strong>Processed?</strong></td><td><input name=\"ResponseSent" + j + "\" type=\"radio\" value=\"true\" checked>Yes&nbsp;&nbsp;&nbsp;";
			        strUserInfo += "<input type=\"radio\" name=\"ResponseSent" + j + "\" value=\"false\">No</td></tr>" + System.Environment.NewLine;
                }
		        else
                {
			        strUserInfo += "<tr><td><strong>Processed?</strong></td><td><input name=ResponseSent" + j + " type=\"radio\" value=\"true\">Yes&nbsp;&nbsp;&nbsp;";
			        strUserInfo += "<input type=\"radio\" name=\"ResponseSent" + j + "\" value=\"false\" checked>No</td></tr>" + System.Environment.NewLine;
		        }
		        strUserInfo += "<tr><td><strong>Comment:</strong></td><td><textarea cols=\"50\" rows=\"5\" name=\"Comments" + j + "\">" + dtUsersRow["WSU_Comments"].ToString() + "</textarea></td></tr>" + System.Environment.NewLine;
	        }
	        objBuilder.Append(strUserInfo);
            //strResultMsg += "intThisUserID : " + intThisUserID + ".<br />";
		    DataTable dtResponses = GetTallyResponsesByUID(intThisUserID);
            if (dtResponses.Rows.Count == 0)
            {
                strResultMsg += "<p>Error retrieving responses for User ID " + intThisUserID.ToString() + ": no records returned.</p>";
                return strResultMsg;
            }
            //PRINT QUESTIONS AND RESPONSES
            for (i = 0; i < intQCount; i++)
            {
                //strResultMsg += "For count: " + i + ".<br />";
                //strResultMsg = strResultMsg + "Question Type: " + arrQuestionType[i] + ".<br />"
		        if (arrQuestionType[i] == 5)	//Text Only
                {
			        objBuilder.Append("<tr><td colspan=\"2\"><strong>" + arrQuestions[i] + "</strong></td></tr>" + System.Environment.NewLine);
                }
		        else 
                {
                    blSingleOption = true;
                    //Multiple responses possible with question type 2
			        //rsGetResponses.Find "WSR_QID = " + arrQID[i]
                    DataRow[] dtResponsesRows = dtResponses.Select("WSR_QID = " + arrQID[i]);
                    //strResultMsg += "Count: " + i + "; QID: " + arrQID[i] + ".<br />";
                    if (dtResponsesRows.Length > 0)
                    {
                        foreach (DataRow dtResponsesRow in dtResponsesRows)
                        {
			                //Process normally
				            if (arrQuestionType[i] == 1 || arrQuestionType[i] == 2 || arrQuestionType[i] == 6)		//Multiple Choice 
                            {
                                DataTable dtOptions = GetSurveyQuestionOptions(intSurveyID, arrQID[i]);
					            if (dtOptions.Rows.Count == 0)	//Not fatal, keep going
                                {
						            strResultMsg += "Could not open rsGetOptions recordset for Q: " + arrQID[i] + ".<br />";
                                }
					            else
                                {
						            intThisQID = Convert.ToInt32(dtResponsesRow["WSR_QID"]);
									//rsGetOptions.Find "WSO_OptionID = " & rsGetResponses("WSR_OptionID"), , 1, 1
                                    DataRow[] dtOptionsRows = dtOptions.Select("WSO_OptionID = " + Convert.ToInt32(dtResponsesRow["WSR_OptionID"]));
                                    if (dtOptionsRows.Length > 0)
                                    {
                                        intOptionID = Convert.ToInt32(dtOptionsRows[0]["WSO_OptionID"]);
                                        strDescription = dtOptionsRows[0]["WSO_Description"].ToString();
                                    }
                                    //strResultMsg += "QID: " + intThisQID + "; Q: " + strDescription + "<br />";
									objMultiOption.Append(strDescription);
									//if (idx > 1 && idx <= arrOptionCount[i])
                                    if(arrQuestionType[i] == 2) 
                                    {
                                        //strTestQuestion = arrQuestions[i];
                                        //objMultiOption.Append("; ");
                                        if (arrQuestions[i] == strTestQuestion)
                                        {
                                            //Set to multiple responses mode
                                            blSingleOption = false;
                                        }
                                        else 
                                        { 
                                            //Set to single response mode
                                            strTestQuestion = arrQuestions[i]; 
                                            blSingleOption = true; 
                                        }
                                    }
                                    //Reset variable
									strDescription = "";
						            if (objMultiOption.Length > 50)
                                    {
							            objBuilder.Append("<tr><td colspan=\"2\"><strong>");
                                        if (blSingleOption) 
                                        { 
                                            objBuilder.Append(arrQuestions[i]); 
                                        } else {
                                            objBuilder.Append("&nbsp;"); 
                                        }

                                        objBuilder.Append("</strong></td></tr>" + System.Environment.NewLine);
							            objBuilder.Append("<tr><td colspan=\"2\">" + objMultiOption.ToString() + "</td></tr>" + System.Environment.NewLine);
                                    }
						            else
                                    {
                                        objBuilder.Append("<tr><td><strong>");
                                        if (blSingleOption)
                                        {
                                            objBuilder.Append(arrQuestions[i]);
                                        }
                                        else
                                        {
                                            objBuilder.Append("&nbsp;");
                                        } 
                                        objBuilder.Append("</strong></td>" + System.Environment.NewLine);
                                        objBuilder.Append("<td valign=\"top\">" + objMultiOption.ToString() + "&nbsp;</td></tr>" + System.Environment.NewLine);
						            }
                                    blSingleOption = false;
                                    //Reset variable
                                    objMultiOption.Clear();
					            }
                            }
                            else if (arrQuestionType[i] == 3 || arrQuestionType[i] == 7 || arrQuestionType[i] == 8) //Fill In - Attach a file - propopulated DDLs
                            {
					            //Get Text Responses 
					            strResponseText = dtResponsesRow["WSR_ResponseText"].ToString();
					            objBuilder.Append("<tr><td><strong>" + arrQuestions[i] + "</strong></td>");
                                if ((arrQuestionType[i] == 7) && strResponseText != "" && strResponseText != "&nbsp;" && strResponseText != null)
                                {
                                    if (strResponseText.Length > 10)
                                    {
                                        if (strResponseText.Substring(0, 4) == "http")
                                        {
                                            objBuilder.Append("<td valign=\"top\">" + "<a href=\"" + strResponseText + "\" target=\"_blank\">" + strResponseText + "</a></td></tr>" + System.Environment.NewLine);
                                        }
                                        else 
                                        {
                                            objBuilder.Append("<td valign=\"top\">" + "<a href=\"/InternetContent/OnlineSurvey/" + strResponseText + "\" target=\"_blank\">" + strResponseText + "</a></td></tr>" + System.Environment.NewLine);
                                        }
                                    }
                                    else
                                    {
                                        objBuilder.Append("<td valign=\"top\">" + strResponseText + "&nbsp;</td></tr>" + System.Environment.NewLine);
                                    }
                                }
                                else
                                {
					                objBuilder.Append("<td valign=\"top\">" + strResponseText + "&nbsp;</td></tr>" + System.Environment.NewLine);
                                }
                            }
				            else if (arrQuestionType[i] == 4)	//Fill In - Long
                            {
					            //Get Text Responses - but beware of nulls
                                if (!DBNull.Value.Equals(dtResponsesRow["WSR_ResponseText"]))
                                {
                                    strResponseText = dtResponsesRow["WSR_ResponseText"].ToString();
					                if (strResponseText.Length > 50)
                                    {
						                objBuilder.Append("<tr><td colspan=\"2\"><strong>" + arrQuestions[i] + "</strong></td></tr>" + System.Environment.NewLine);
						                objBuilder.Append("<tr><td valign=\"top\" colspan=\"2\">" + strResponseText + "&nbsp;</td></tr>" + System.Environment.NewLine);
                                    }
                                    else	//Treat it like a fill-in
                                    {
                                        objBuilder.Append("<tr><td><strong>" + arrQuestions[i] + "</strong></td>" + System.Environment.NewLine);
                                        objBuilder.Append("<td valign=\"top\">" + strResponseText + "&nbsp;</td></tr>" + System.Environment.NewLine);
                                    }
                                }
					            else	//Treat it like a fill-in
                                {
						            objBuilder.Append("<tr><td><strong>" + arrQuestions[i] + "</strong></td>" + System.Environment.NewLine);
						            objBuilder.Append("<td valign=\"top\">&nbsp;</td></tr>" + System.Environment.NewLine);
					            }
                            }
                        }   // foreach dtResponsesRow
                    }
                    else
                    {
			            // No Response to question
				        objBuilder.Append("<tr><td><strong>" + arrQuestions[i] + "</strong></td>" + System.Environment.NewLine);
				        objBuilder.Append("<td valign=\"top\">&nbsp;</td></tr>" + System.Environment.NewLine);
                        //strResultMsg += "No data QID: " + arrQID[i] + "<br />";
                        //strResultMsg = strResultMsg & "Response Text: " & rsGetResponses("WSR_ResponseText"] + "<br />"
                    }
                    //Reset variables
		            strResponseText = "";
		            strUserInfo = "";
                }   
            }   //for loop PRINT
            //Opinion Poll uses same WSU_COmments field as Forms use to process forms.  Display ony if Opinion Poll for printing.
            //Should never hit this line but ya never know
		    if (dtUsersRow["WSU_Comments"].ToString() != "" && mblPrintFormResults && intSurveyType == 2)
            {
                objBuilder.Append("<tr><td><strong>Comment:</strong></td><td>" + dtUsersRow["WSU_Comments"].ToString() + "</td></tr>" + System.Environment.NewLine);
		    }
            // Place a visual break between each submission, or page break for printing if indicated
            if (intPageBreak == 1) 	// Defined at top of calling page
            {
                objBuilder.Append("<tr class=\"bgGreyTint\"><td colspan=\"2\"><p class=\"page\">&nbsp;</p></td></tr>" + System.Environment.NewLine);
                objBuilder.Append("<tr><td colspan=\"2\"><h3>" + strSurveyTitle + "</h3></td></tr>" + System.Environment.NewLine);
            }
            else
            {
                objBuilder.Append("<tr class=\"bgGreyTint\"><td colspan=\"2\">&nbsp;</td></tr>" + System.Environment.NewLine);
            }
        }   //foreach dtUsersRow
        if (!mblPrintFormResults)
        {
	        objBuilder.Append("<tr style=\"height:40px;\"><td colspan=\"2\" style=\"text-align:center;\"><input type=\"submit\" class=\"button\" value=\"Submit\" name=\"btnResponse\">&nbsp;&nbsp;");
	        objBuilder.Append("&nbsp;Record No. to delete: <input type=\"text\" size=\"5\" name=\"txtDeleteNo\">&nbsp;<input type=\"submit\" class=\"button\" value=\"Delete\" name=\"btnDelete\"");
	        objBuilder.Append(" onClick=\"javascript:return confirm('Are you sure you want to delete this record?')\">");
	        objBuilder.Append("<input type=\"hidden\" value=" + j + " name=\"RecordCount\"></td></tr>" + System.Environment.NewLine);
	        //strResult = strResult & "<input type=""hidden"" value=" & intTallyID & " name=TallyID>"
        } 
        return objBuilder.ToString();
    }

    /// <summary>
    /// Update one response with processing information
    /// </summary>
    /// <param name="intUserID"></param>
    /// <param name="blResponseSent"></param>
    /// <param name="strComments"></param>
    /// <returns>String result message</returns>
    public String UpdateResponses(Int32 intUserID, bool blResponseSent, string strComments)
    {
        blReturnIdentity = false;
        blIsSQL = true;
        blASCIIToHTML = false;
        blHTMLToASCII = false; Utility Fixer = new Utility();
        strComments = Fixer.ConvertAndFixText(strComments, blASCIIToHTML, blHTMLToASCII, blIsSQL);
        string strSQL = "pr_UpdateWebSurvey_User " + intUserID + ", " + blResponseSent + ", '" + strComments + "'";
        Int32 intResult = runOSSQLQuery(strSQL, blReturnIdentity);
        //strResultMsg = strResultMsg & "SQL string: " & strSQL & "<br />"
	    if (intResult == 0)
        {
			strResultMsg += "Error updating records: 0 rows affected.<br />";
        }
	    else
        {
		    strResultMsg += strResultMsg + "Your records were successfully updated!<br />";
        }
        return strResultMsg;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="intUserID"></param>
    /// <param name="intTallyID"></param>
    /// <returns></returns>
    public String DeleteOneUserRecord(Int32 intUserID, Int32 intTallyID)
    {
        //strResultMsg = strResultMsg & "intUserID: " & intUserID & "<br />"
        //strResultMsg = strResultMsg & "intTallyID: " & intTallyID & "<br />"
        blReturnIdentity = false;
        string strSQL1 = "pr_DeleteSurveyResponsesUser " + intUserID;
        string strSQL2 = "pr_DeleteOneSurveyUser " + intUserID;
        Int32 intResult1 = runOSSQLQuery(strSQL1, false);
        if (intResult1 == 0)
        {
            strResultMsg += "Error deleting User Responses: 0 rows affected.<br />";
            return strResultMsg;
        }
        else
        {
            //strResultMsg = strResultMsg & "SQL: " & strSQL1 & "<br />"
            Int32 intResult2 = runOSSQLQuery(strSQL2, blReturnIdentity);
            if (intResult2 == 0)
            {
                strResultMsg += "Error deleting User: : 0 rows affected.<br />";
                return strResultMsg;
            }
            else
            {
                strResultMsg += DecrementTallyCount(intTallyID);
                return strResultMsg;
            }
        }
    }

    /// <summary>
    /// Displays total correct per
    /// </summary>
    /// <param name="intTallyID"></param>
    /// <param name="strStudentID"></param>
    /// <returns>String results</returns>
    public String DisplayQuizResults(Int32 intTallyID, string strStudentID)
    {
        StringBuilder objBuilder = new StringBuilder();
	    Int32 intCorrect = 0;
	    Int32 intTotal = 0;

	    if (intTallyID == 0)
        {
		    strResultMsg += "A valid Tally is required to display results.<br />";
		    return strResultMsg;
        }
	    if (strStudentID == "" || strStudentID == "0" || strStudentID == "anonymous")
        {
		    strResultMsg += "A valid student ID must be selected to display results.<br />";
		    return strResultMsg;
        }

        DataTable dt = GetQuizResults(intTallyID, strStudentID); 
        //strResultMsg = strResultMsg & "strSQL: " & strSQL & "<br />"

	    if (dt.Rows.Count > 0)
        {
		    foreach(DataRow dtRow in dt.Rows)
            {
				intTotal += 1;
				objBuilder.Append("<strong>" + intTotal + ")</strong> " + dtRow["WSQ_Question"].ToString() + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"); 
				if (dtRow["WSR_ResponseText"].ToString() == "1")
                {
					intCorrect += 1;
					objBuilder.Append("<strong>Correct</strong><br />" + System.Environment.NewLine);
                }
				else
                {
					objBuilder.Append("<strong>Incorrect</strong><br />" + System.Environment.NewLine);
                }
                //strResultMsg = strResultMsg & rs("WSQ_Question") & "; "
		    }
	    }
	    objBuilder.Append("<br /><strong>Total: " + intCorrect + " correct out of " + intTotal + "</strong>");
        return objBuilder.ToString();
    }

    //::: ADMIN FUNCTIONS :::

    /// <summary>
    /// Basic insert all fields for admin
    /// </summary>
    /// <param name="strEmplID"></param>
    /// <param name="strFName"></param>
    /// <param name="strLName"></param>
    /// <param name="strEmail"></param>
    /// <param name="strPhone"></param>
    /// <param name="intType"></param>
    /// <param name="strLastAccess"></param>
    /// <param name="intLastProfileID"></param>
    /// <returns>Int32 result 1 or 0</returns>
    public Int32 InsertAdmin(string strEmplID, string strFName, string strLName, string strEmail, string strPhone, Int32 intType, string strLastAccess, Int32 intLastProfileID)
    {
        //strEmplID, strFName, strLName, strEmail, strPhone, intType, strLastAccess, intLastProfileID
        blReturnIdentity = false;
        string strSQL = "pr_InsertWebAdmin '" + strEmplID + "', '" + strFName + "', '" + strLName + "', '" + strEmail + "', '" +
            strPhone + "', " + intType + ", '" + strLastAccess + "', " + intLastProfileID;
        //strResultMsg = strResultMsg & "strSQL: " & strSQL & "<br />"
        //This is an insert, but the SP does not retrieve the new identity - not needed
        Int32 intResult = runOSSQLQuery(strSQL, blReturnIdentity);
        if (intResult > 0)
        {
            strResultMsg += "Your web admin was inserted successfully!<br />";
        }
        else
        {
            strResultMsg += "Error inserting web admin: 0 rows affected.<br />";
        }
        return intResult;
    }

    /// <summary>
    /// Edit admin information
    /// </summary>
    /// <param name="intWAdmin_ID"></param>
    /// <param name="strEmplID"></param>
    /// <param name="strFName"></param>
    /// <param name="strLName"></param>
    /// <param name="strEmail"></param>
    /// <param name="strPhone"></param>
    /// <param name="intType"></param>
    /// <param name="strLastAccess"></param>
    /// <param name="intLastProfileID"></param>
    /// <returns></returns>
    public String UpdateAdmin(Int32 intWAdmin_ID, string strEmplID, string strFName, string strLName, string strEmail, string strPhone, Int32 intType, string strLastAccess, Int32 intLastProfileID)
    {
        //strEmplID, strFName, strLName, strEmail, strPhone, intType, strLastAccess, intLastProfileID
        string strSQL = "pr_UpdateWebAdmin " + intWAdmin_ID + ", '" + strEmplID + "', '" + strFName + "', '" + strLName + "', '" + strEmail + "', '" +
           strPhone + "', " + intType + ", '" + strLastAccess + "', " + intLastProfileID;
        //strResultMsg = strResultMsg + "strSQL: " + strSQL + "<br />"
        blReturnIdentity = false;
        Int32 intResult = runOSSQLQuery(strSQL, blReturnIdentity);
        if (intResult > 0)
        {
            strResultMsg += "Your web admin was updated successfully!<br />";
        }
        else
        {
            strResultMsg += "Error updating web admin: 0 rows affected.<br />";
        }
        return strResultMsg;
    }

    /// <summary>
    /// Delete an admin
    /// </summary>
    /// <param name="intAdminID"></param>
    /// <returns></returns>
    public String DeleteAdmin(Int32 intAdminID)
    {
        string strSQL = "pr_DeleteOneWebAdmin " + intAdminID;
        blReturnIdentity = false;
        Int32 intResult = runOSSQLQuery(strSQL, blReturnIdentity);
        if (intResult > 0)
        {
            strResultMsg += "Your web admin was deleted successfully!<br />";
        }
        else
        {
            strResultMsg += "Error deleting web admin: 0 rows affected.<br />";
        }
        return strResultMsg;
    }

#endregion
}