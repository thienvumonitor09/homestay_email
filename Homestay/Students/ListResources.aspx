﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CCSApps.master" CodeFile="ListResources.aspx.cs" Inherits="Homestay_Students_ListResources" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">

    <h1>List of Resources</h1>
    <ul>
        <li><a href="/Homestay/Families/_PDF/MorneauShepell-studentCounseling.pdf" target="_blank">Student Counseling and Support Service-Morneau Shepell 2018</a></li>
                <li><a href="/Homestay/Families/_PDF/2018-19-Lewermark.pdf" target="_blank">Insurance Plan 2018-19</a></li>
    </ul>
     <h2>Tips for Success </h2>
            <ul>
                <li><a href="/Homestay/Families/_PDF/Guidelines/HouseGuidelines_English.pdf" target="_blank" title="House Guidelines">English</a></li>
                <li><a href="/Homestay/Families/_PDF/Guidelines/HouseGuidelines_Chinese.pdf" target="_blank" title="House Guidelines">Chinese</a></li>
                <li><a href="/Homestay/Families/_PDF/Guidelines/HouseGuidelines_Vietnamese.pdf" target="_blank" title="House Guidelines">Vietnamese</a></li>
                <li><a href="/Homestay/Families/_PDF/Guidelines/HouseGuidelines_Arabic.pdf" target="_blank" title="House Guidelines">Arabic</a></li>
                <li><a href="/Homestay/Families/_PDF/Guidelines/HouseGuidelines_Korean.pdf" target="_blank" title="House Guidelines">Korean</a></li>
                <li><a href="/Homestay/Families/_PDF/Guidelines/HouseGuidelines_Japanese.pdf" target="_blank" title="House Guidelines">Japanese</a></li>
            </ul>
    <h2>Orientation </h2>
    <ul>
        <li><a href="/Homestay/Families/_PDF/orientation/SCC ACA Spring 2019 Welcome Week.pdf" target="_blank">SCC Orientation</a></li>
        <li><a href="/Homestay/Families/_PDF/orientation/SFCC ACA Spring 2019 Welcome Week.pdf" target="_blank">SFCC Orientation for Academic Students</a></li>
        <li><a href="/Homestay/Families/_PDF/orientation/SFCC IELP Spring 2019 Welcome Week.pdf" target="_blank">SFCC Orientation for IELP Students</a></li>
    </ul>
    <h2>Tips for Success</h2>
        
    <ul>
        <li><a href="/Homestay/Students/_PDF/Tips for Success/English translation - tips for success.pdf" target="_blank" title="Tips for Success">English</a></li>
         <li><a href="/Homestay/Students/_PDF/Tips for Success/Chinese translation - tips for success.pdf" target="_blank" title="Tips for Success">Chinese</a></li>
         <li><a href="/Homestay/Students/_PDF/Tips for Success/Vietnamese translation - tips for success.pdf" target="_blank" title="Tips for Success">Vietnamese</a></li>
         <li><a href="/Homestay/Students/_PDF/Tips for Success/Hindi translation - tips for success.pdf" target="_blank" title="Tips for Success">Hindi</a></li>      
    </ul>
    
    <h2>Homestay Change Forms</h2>
    <ul>
         <li><a href="/Homestay/Students/_PDF/Homestay-Change-Form-ccs-40-332.pdf" target="_blank">Homestay Change Form 16-17(Form 40-332)</a></li>
        <li><a href="/Homestay/Students/_PDF/Homestay-Change-Form-ccs-40-333.pdf" target="_blank">Homestay Change Form 18+(Form 40-333)</a></li>
    </ul>
    
    <h2>Other Forms</h2>
    <ul>
        <li><a href="/Homestay/Families/_PDF/MovingOut-ccs 40-331.pdf" target="_blank">Moving Out Form (ccs 40-331)</a></li>
        <li><a href="/Homestay/Families/_PDF/UrgentCareLocations2018-2019.pdf" target="_blank">Urgent Care Locations</a></li>
        <li><a href="/Homestay/Families/_PDF/MentalHealth2017-2NOV.pdf" target="_blank">Mental Health Resources</a></li>
        <li><a href="/Homestay/Families/_PDF/TravelPermissionForm-ccs 40-334.pdf" target="_blank">Travel Consent Form for Minors (Form 40-334)</a></li>
    </ul>
 
    
</asp:Content>
