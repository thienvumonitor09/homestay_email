﻿
var app = angular.module('myApp', ['ngSanitize', 'ngCkeditor', 'ngRoute']);
/* Factory */
app.factory('CrudFactory', function () {
    var factory = {};
    factory.attachModal = function () {
        $('#attachFileModalID').modal('show');
    }

    return factory;
}); 
/*
A directive to enable two way binding of file field
*/

app.directive('ckEditor', function () {
    return {
        require: '?ngModel',
        link: function (scope, elm, attr, ngModel) {
            var ck = CKEDITOR.replace(elm[0]);
            if (!ngModel) return;
            ck.on('instanceReady', function () {
                ck.setData(ngModel.$viewValue);
            });
            function updateModel() {
                scope.$apply(function () {
                    ngModel.$setViewValue(ck.getData());
                });
            }
            ck.on('change', updateModel);
            ck.on('key', updateModel);
            ck.on('dataReady', updateModel);

            ngModel.$render = function (value) {
                ck.setData(ngModel.$viewValue);
            };
        }
    };
});

app.directive('demoFileModel', function ($parse) {
    return {
        restrict: 'A', //the directive can be used as an attribute only

        /*
         link is a function that defines functionality of directive
         scope: scope associated with the element
         element: element on which this directive used
         attrs: key value pair of element attributes
         */
        link: function (scope, element, attrs) {
            var model = $parse(attrs.demoFileModel),
                modelSetter = model.assign; //define a setter for demoFileModel

            //Bind change event on the element
            element.bind('change', function () {
                //Call apply on scope, it checks for value changes and reflect them on UI
                scope.$apply(function () {
                    //set the model value
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
});

app.service('fileUploadService', function ($http, $q) {
    this.uploadFileToUrl = function (file, uploadUrl) {
        //FormData, object of key/value pair for form fields and values
        var fileFormData = new FormData();
        fileFormData.append('file', file);

        var deffered = $q.defer();
        $http.post(uploadUrl, fileFormData, {
            transformRequest: angular.identity,
            headers: { 'Content-Type': undefined }
        }).then(function (response) {
            deffered.resolve(response);
        }, function (error) {
            deffered.reject(response);
        });
        return deffered.promise;
    }
});


app.config(function ($routeProvider) {
    $routeProvider
        .when("/", {
            templateUrl: "sendEmail.html",
            controller: 'myCtrl'
        })
        .when("/confirmation", {
            templateUrl: "confirmation.html",
            controller: 'myCtrl'
        })
        .when("/tomato", {
            template: "<h1>Tomato</h1><p>Tomatoes contain around 95% water.</p>"
        });
});
app.controller('myCtrl', function ($scope, $http, $location, fileUploadService, CrudFactory,$window) {
    /*
    $scope.editorOptions = {
        language: 'en',
        allowedContent: true,
        entities: false,
        uiColor: '#D3D3D3'
    };
    */
    $scope.backAdmin = function () {
        $window.location.href = "http://10.171.1.202:5175/Homestay/Admin/Default.aspx"; //You should have http here.
    };
   $('[data-toggle="tooltip"]').tooltip();   
    $scope.uploadFile = function () {
       
        var file = $scope.myFile;
        console.log('file is ');
        console.dir(file);
        var uploadUrl = "/WebService.asmx/UploadFile"; //Url of webservice/api/server

        var fileFormData = new FormData();
        fileFormData.append('file', file);
        $http({
            method: "POST",
            url: "WebService.asmx/UploadFile",
            //dataType: 'json',
            data: fileFormData,
            headers: { "Content-Type": undefined },
            transformRequest: angular.identity
        }).then(function (response) {
            //alert(JSON.stringify(response.data));
            //alert(response.data);
        });
    };

    $scope.content_two = "hello";
    $scope.emailObj = {};

    //$scope.emailObj.FromEmail = "vu.nguyen@ccs.spokane.edu";   
    //$scope.emailObj.ToEmail = "vu.nguyen@ccs.spokane.edu";  
    $scope.emailObj.ToEmail = "";  
    
    $scope.emailObj.Subject = "Test subject";  
    $scope.emailObj.Body = "content"; 

    //Load groups for email lists from json file
    $http.get("./Scripts/groups.json").then(function (response) {
        $scope.groups = response.data;
    });
   
    $scope.updateEmails = function () {
        $scope.emails = $scope.emails.split(",");
    };
    $scope.ccEmails = [];
    $scope.updateCCEmails = function () {
        $scope.ccEmails = $scope.ccEmails.split(",");
    };

    $scope.emailTemplates = {};

    
    $scope.getEmailsByGroup = function () {
        $scope.searchName = "";
        if ($scope.group == "Individual Students" || $scope.group == "Individual Families") {

            $scope.selectedEmail = [];
            /*
            angular.forEach($scope.emails, function (value, key) {
                $scope.selectedEmail.push({ name: value, selected: false });
            });
            */
            //console.log($scope.selectedEmail);
            $('#individualEmail').modal('show');
            $http({
                method: "POST",
                url: "WebService.asmx/GetEmailGroup2",
                dataType: 'json',
                data: { groupName: $scope.group },
                headers: { "Content-Type": "application/json" }
            }).then(function (response) {
                //alert(response.data);
                //console.log(JSON.stringify(response.data));
                //$scope.emailTemplates = JSON.parse(response.data.d);
                //console.log($scope.emailTemplates);
                //$scope.selectedEmail = JSON.parse(response.data.d);
                var res = JSON.parse(response.data.d);
                //console.log(res);
                angular.forEach(res, function (value, key) {
                    $scope.selectedEmail.push({ name: value.name,email:value.email, selected: false });
                });
                //console.log($scope.selectedEmail);
                //$scope.selectedEmail = JSON.parse(response.data.d);
            }, function (error) {
                alert("error");
            });


            if ($scope.group == "Individual Students") {
                $("#modalTitle").text("Students' Email");
            } else {
                $("#modalTitle").text("Families' Email");
            }

        } else {
            $http({
                method: "POST",
                url: "WebService.asmx/GetEmailGroup",
                dataType: 'json',
                data: { groupName: $scope.group },
                headers: { "Content-Type": "application/json" }
            }).then(function (response) {
                //alert(response.data);
                console.log(response.data);
                //$scope.emailTemplates = JSON.parse(response.data.d);
                //console.log($scope.emailTemplates);
                $scope.emails = JSON.parse(response.data.d);

            }, function (error) {
                alert("error");
            });
        //$scope.emails = $scope.group;
        }  
    };
    $scope.saveSelectedEmails = function () {
        $scope.emails = [];
        angular.forEach($scope.selectedEmail, function (value, key) {
            if (value.selected) {
                console.log(value.name);
                $scope.emails.push(value.email);
            }

            //$scope.attached.push({ name: value, selected: false });
        });
        //$('#attachFileModalID').modal('hide');
    }

    $scope.addEmail = function () {
        $scope.emailObj.ToEmail.split(";").forEach(function (element) {
            if (element != "" && $scope.emails.indexOf(element) == -1) {
                $scope.emails.push(element);
            }
            console.log($scope.emails);
        });
        
    }
    
    $scope.emailSignature = '<p><em>Community Colleges of Spokane</em>'
                            + '<em> Global Education Homestay Program </em><br/>'
                            + "<em><a href='mailto: internationalhomestay@ccs.spokane.edu'>internationalhomestay@ccs.spokane.edu</a></em><br /"
                            + "<em>509-533-4113</em></p>";
    
    $scope.getEmailTemplates = function () {
        $http({
            method: "POST",
            url: "WebService.asmx/GetTemplates",
            dataType: 'json',
            data: {},
            headers: { "Content-Type": "application/json" }
        }).then(function (response) {
            //alert(response.data);
            //console.log(response.data);
            $scope.emailTemplates = JSON.parse(response.data.d);
            console.log($scope.emailTemplates);
        }, function (error) {
            alert("error");
        });
    };

    $scope.getEmailTemplates();
    $scope.template2 = "template2";
    //console.log($scope.emailObj);

    $scope.addSignature = function(){
        $scope.selectedTemplate.templateContent += ($scope.senderName + $scope.emailSignature);
    }
    //$scope.attachments = ["Homestay-Change-Form.pdf", "Moving-Out-Form.pdf"];
    $scope.attachments = [];
    $scope.attachFileModal = function () {
        $scope.getAllFiles();
        $('#attachFileModalID').modal('show');
        //CrudFactory.attachModal();
    };
    
    $scope.getAllFiles = function () {
        //alert("retrieving files");
        $http({
            method: "POST",
            url: "WebService.asmx/GetAttachments",
            dataType: 'json',
            data: {},
            headers: { "Content-Type": "application/json" }
        }).then(function (response) {
            //alert(JSON.stringify(response.data.d));
            var attachmentsObj = JSON.parse(response.data.d);
            //$scope.attachments = JSON.parse(response.data.d);
            $scope.attached = [];
            angular.forEach(attachmentsObj, function (value, key) {
                //console.log(value);
                $scope.attached.push({name:value, selected:false});
            });
        }, function (error) {
            alert("error");
        });
    };
    $scope.saveAttachment = function () {
        //console.log("ok");
        $scope.attachments = [];
        angular.forEach($scope.attached, function (value, key) {
            if (value.selected){
                console.log(value.name);
                $scope.attachments.push(value.name);
            }
            
            //$scope.attached.push({ name: value, selected: false });
        });
        $('#attachFileModalID').modal('hide');
    }
    $scope.attachFile = function () {
        $scope.attachments.push("Homestay-Change-Form.pdf");
    };
    $scope.send = function () {
        //alert($scope.selectedTemplate.templateContent);
        //var file = $scope.myFile;
        //var fileFormData = new FormData();
        //fileFormData.append('file', file);
        //console.dir(file);
        alert("sending");
        $http({
            method: "POST",
            url: "WebService.asmx/SendEmailService",
            dataType: 'json',
            data: {
                emailObj: JSON.stringify($scope.emailObj),
                selectedTemplate: $scope.selectedTemplate.templateContent,
                emails: JSON.stringify($scope.emails),
                ccEmails: JSON.stringify($scope.ccEmails),
                attachmentNames: JSON.stringify($scope.attachments)
            },
            //transformRequest: angular.identity,
            headers: { "Content-Type": "application/json" }
        }).then(function (response) {
            //alert(JSON.stringify(response.data));
            //alert(response.data.d);
            //console.log("success");
            alert(response.data.d);
            $location.path('/confirmation');
        }, function (error) {
            alert("error");
        });
    };
    
    $scope.getLogonUser = function () {
        //alert($scope.selectedTemplate.templateContent);
        //alert("sending");
        $http({
            method: "POST",
            url: "Communication.aspx/GetLogonUser",
            dataType: 'json',
            data: { },
            headers: { "Content-Type": "application/json" }
        }).then(function (response) {
            var strLogonUser = (response.data.d);
            console.log(strLogonUser);
            //alert(response.data);
            var n = strLogonUser.lastIndexOf("\\"); //find index of \ because logon user has format ccs\vu.nguyen
            console.log(strLogonUser.substr(n + 1) + "@ccs.spokane.edu"); 

            //Get log in email from username
            var userName = strLogonUser.substr(n + 1);
            $scope.emailObj.FromEmail = userName + "@ccs.spokane.edu";

            //Get Sender full name
            var senderName = "";
            userName.split(".").forEach(function (element) { senderName += element.charAt(0).toUpperCase() + element.substr(1) +" " ; });
            $scope.senderName = "<p><strong>" + senderName + "</strong></p>";
        }, function (error) {
            alert("error");
        });
    };
    $scope.getLogonUser();
    /*
    $scope.getTemplates = function () {
        $http({
            method: "GET",
            url: "./Scripts/template.json",
            dataType: 'json',
            headers: { "Content-Type": "application/json" }
        }).then(function (response) {
            console.log(response.data)
        }, function (error) {
            alert("error");
        });
    };
    */
    
    //$scope.emailObj.Body = "hello";

});

