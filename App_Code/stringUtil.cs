using System;
using System.Web;
using System.Text.RegularExpressions;

namespace sccwww
{
	/// <summary>
	/// Summary description for stringUtil.
	/// </summary>
	public class stringUtil
	{
		public stringUtil()
		{
			//
			// TODO: Add constructor logic here
			//
		}
    public static String PCase(String strParam)
    {
      String strProper = strParam.Substring(0, 1).ToUpper();
      strParam = strParam.Substring(1).ToLower();
      String strPrev = "";

      for (int iIndex = 0; iIndex < strParam.Length; iIndex++)
      {
        if (iIndex > 1)
        {
          strPrev = strParam.Substring(iIndex - 1, 1);
        }
        if (strPrev.Equals(" ") ||
            strPrev.Equals("/") ||
            strPrev.Equals("\t") ||
            strPrev.Equals("\n") ||
            strPrev.Equals("."))
        {
          strProper += strParam.Substring(iIndex, 1).ToUpper();
        }
        else
        {
          strProper += strParam.Substring(iIndex, 1);
        }
      }
      strProper = strProper.Replace(" The ", " the ");
      strProper = strProper.Replace(" In ", " in ");
      strProper = strProper.Replace(" Of ", " of ");
      strProper = strProper.Replace(" And ", " and ");
      strProper = strProper.Replace(" For ", " for ");
      strProper = strProper.Replace(" Ix", " IX");
      strProper = strProper.Replace(" Viii", " VIII");
      strProper = strProper.Replace(" Vii", " VII");
      strProper = strProper.Replace(" Vi", " VI");
      strProper = strProper.Replace(" Iv", " IV");
      strProper = strProper.Replace(" Iii", " III");
      strProper = strProper.Replace(" Ii", " II");
      strProper = strProper.Replace("Emt ", "EMT ");
      strProper = strProper.Replace("Iec ", "IEC ");
      strProper = strProper.Replace("Iv ", "IV ");
      strProper = strProper.Replace("Nec ", "NEC ");
      strProper = strProper.Replace(":nec ", ":NEC ");
      strProper = strProper.Replace("Rcw ", "RCW ");
      strProper = strProper.Replace("Wac ", "WAC ");
      return strProper;
    }
    public static String TitleCase(String strParam)
    {
      Regex regNotChar = new Regex("[^:a-zA-Z0-9_,'-]");
      MatchCollection matches = regNotChar.Matches(strParam);
      string[] strArr = regNotChar.Split(strParam);
      String output = "";
      for (int i = 0; i < strArr.Length; i++)
      {
        if (matches.Count == i)
        {
          if (strArr[i].Length >= 1)
          {
            output += strArr[i].Substring(0, 1) + strArr[i].ToLower().Substring(1);
          }
          else
          {
            output += strArr[i].ToLower();
          }
        }
        else
        {
          if (strArr[i].Length >= 1)
          {
            output += strArr[i].Substring(0, 1) + strArr[i].ToLower().Substring(1) + matches[i].Value;
          }
          else
          {
            output += strArr[i].ToLower() + matches[i].Value;
          }
        }
      }

      output = output.Trim();
      output = output.Replace(" The ", " the ");
      output = output.Replace(" In ", " in ");
      output = output.Replace(" Of ", " of ");
      output = Regex.Replace(output, "(\\W)(And)(\\W)", "$1and$3");
      output = Regex.Replace(output, "(\\W)(For)(\\W)", "$1for$3");
      output = Regex.Replace(output, "(\\W)(Ix)(\\W)", "$1IX$3");
      output = Regex.Replace(output, "(\\W)(Viii)(\\W)", "$1VIII$3");
      output = Regex.Replace(output, "(\\W)(Vii)(\\W)", "$1VII$3");
      output = Regex.Replace(output, "(\\W)(Vi)(\\W)", "$1VI$3");
      output = Regex.Replace(output, "(\\W)(Iv)(\\W)", "$1IV$3");
      output = Regex.Replace(output, "(\\W)(Iii)(\\W)", "$1III$3");
      output = Regex.Replace(output, "(\\W)(Ii)(\\W)", "$1II$3");
      output = Regex.Replace(output, "(\\W)(Emt)(\\W)", "$1EMT$3");
      output = Regex.Replace(output, "(\\W)(Iec)(\\W)", "$1IEC$3");
      output = output.Replace("Iec ", "IEC ");
      output = output.Replace("Iv ", "IV ");
      output = output.Replace("Nec ", "NEC ");
      output = output.Replace(":nec ", ":NEC ");
      output = output.Replace("Rcw ", "RCW ");
      output = output.Replace("Wac ", "WAC ");
      output = Regex.Replace(output, "(Aas-t)(\\W)", "AAS-T$2");
      output = Regex.Replace(output, "(aas-t)(\\W)", "AAS-T$2");
      output = Regex.Replace(output, "(Rotc)(\\W)", "ROTC$2");
      output = Regex.Replace(output, "(Cnc)(\\W)", "CNC$2");
      output = Regex.Replace(output, "(Rn)(\\W)", "RN$2");
      output = Regex.Replace(output, "(Lpn)(\\W)", "LPN$2");
      return output;
    }
    public static String formatTime(String start, String end)
    {
      //trim leading 0's
      if (start.Trim().Equals("ARR") || end.Trim().Equals("ARR"))
        return "Arranged";
      start = start.Trim().Insert(2, ":");
      end = end.Trim().Insert(2, ":");
      start = Regex.Replace(start, "\\b0", "").Replace(":0", ":00").Replace("P", " p.m.").Replace("A", " a.m.");
      end = Regex.Replace(end, "\\b0", "").Replace(":0", ":00").Replace("P", " p.m.").Replace("A", " a.m.");
      if (start.IndexOf("p.m.") > 0 && end.IndexOf("p.m.") > 0)
        start = start.Replace(" p.m.", "");
      if (start.IndexOf("a.m.") > 0 && end.IndexOf("a.m.") > 0)
        start = start.Replace(" a.m.", "");
      return start + "-" + end;
    }
    public static String SQLarg(String input)
    {
      string temp;
      temp = input.Replace("'", "''");
      temp = HttpContext.Current.Server.HtmlEncode(temp);
      return temp;
    }
    public static String SQLarg(String input, bool doublequote)
    {
      string temp;
      temp = input.Replace("'", "''");
      temp = input.Replace("\"", "");
      temp = HttpContext.Current.Server.HtmlEncode(temp);
      return temp;
    }
	}
}
