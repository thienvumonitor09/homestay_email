﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Homestay_Admin_Reports_Family_ListActive : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void linkBtnHome_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Homestay/Admin/Default.aspx");
    }

    protected void linkBtnReport_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Homestay/Admin/Reports/ReportsHome.aspx");
    }
}