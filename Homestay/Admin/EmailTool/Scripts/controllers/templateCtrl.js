﻿app.controller('templateCtrl', function ($scope, $http, $window) {
    $scope.getEmailTemplates = function () {
        $http({
            method: "POST",
            url: "EmailToolWS.asmx/GetTemplates",
            dataType: 'json',
            data: {},
            headers: { "Content-Type": "application/json" }
        }).then(function (response) {
            //alert(response.data);
            console.log(response.data);
            $scope.emailTemplates = JSON.parse(response.data.d);
            console.log($scope.emailTemplates);
        }, function (error) {
            alert("error");
        });
    };

    $scope.getEmailTemplates();
    //$scope.IsVisible = false;
    $scope.selectedTemplate = {};
    $scope.isAdd = false;
    $scope.isEdit = false;

    $scope.ShowHideEdit = function (templateID) {
        $scope.isAdd = false;
        $scope.isEdit = true;
        $('#myModal').modal('show');
        $("#modalTitle").text("Edit Template");
        //CrudFactory.editTemplate();
        //alert("hello" + templateID);
        $scope.selectedTemplate = $scope.emailTemplates[templateID];
        //$scope.IsVisible = true;
    };

    $scope.deleteTemplate = function (templateIndex) {
        //alert("remove" + templateIndex);
        var r = confirm("Are you sure to delete the template " + $scope.emailTemplates[templateIndex].templateName + "?");
        if (r == false) {
            return;
        } else {
            $http({
                method: "POST",
                url: "EmailToolWS.asmx/DeleteTemplate",
                dataType: 'json',
                data: { templateID: $scope.emailTemplates[templateIndex].templateID },
                headers: { "Content-Type": "application/json" }
            }).then(function (response) {
                //alert(JSON.stringify(response.data.d));
                //console.log($scope.emailTemplates);
                $window.location.reload();
            }, function (error) {
                alert("error");
            });
        }
    };
    $scope.saveTemplate = function () {
        alert("saving");
        $http({
            method: "POST",
            url: "EmailToolWS.asmx/SaveTemplate",
            dataType: 'json',
            data: { templateJS: JSON.stringify($scope.selectedTemplate) },
            headers: { "Content-Type": "application/json" }
        }).then(function (response) {
            //alert(JSON.stringify(response.data.d));
            //console.log($scope.emailTemplates);
            alert("SAVED!");

        }, function (error) {
            alert("error");
        });
    };

    $scope.resetTemplate = function () {
        //alert("reset");
        //MathService.multiply();
    };
    $scope.addTemplateForm = function () {
        $scope.isAdd = true;
        $scope.isEdit = false;
        $scope.selectedTemplate = {};
        $('#myModal').modal('show');
        $("#modalTitle").text("Add Template");
        //CrudFactory.addTemplateModal();
    }
    $scope.addTemplate = function () {
        //alert("addTemplate");
        $http({
            method: "POST",
            url: "EmailToolWS.asmx/AddTemplate",
            dataType: 'json',
            data: { templateJS: JSON.stringify($scope.selectedTemplate) },
            headers: { "Content-Type": "application/json" }
        }).then(function (response) {
            //alert(JSON.stringify(response.data.d));
            //console.log($scope.emailTemplates);
            alert("New Template was added");
            $scope.isSave = false;
            $window.location.reload();
        }, function (error) {
            alert("error");
        });
    };
});