﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for PlacingUtility
/// </summary>
public class PlacingUtility
{
    Homestay hsInfo = new Homestay();
    Quarter_MetaData QMD = new Quarter_MetaData();
    public PlacingUtility()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public bool ValidateOverlap(string selectedFamilyStr, string selectedRoomNumber, string effectiveQuarter, string endQuarter)
    {

        //display list date
        List<Stay> stays = GetStaysByFamilyRoom(selectedFamilyStr, selectedRoomNumber);
        stays.Add(new Stay()
        {
            Start = GetClassDate("FirstDayOfClass", effectiveQuarter),
            End = GetClassDate("LastDayOfClass", endQuarter),
        }
                );


        if (DoesNotOverlap(stays))
        {
            return true;
        }
        return false;

    }

    public bool ValidateOverlapByDate(string selectedFamilyStr, string selectedRoomNumber, string effectiveDate, string endDate)
    {

        //display list date
        List<Stay> stays = GetStaysByFamilyRoom(selectedFamilyStr, selectedRoomNumber);
        //DateTime endDateTime = Convert.ToDateTime(endDate);
        stays.Add(new Stay()
            {
                Start = GetDateTimeByType("effectiveDate", effectiveDate),
                End = GetDateTimeByType("endDate", endDate)
        }
        );


        if (DoesNotOverlap(stays))
        {
            return true;
        }
        return false;

    }

    public DateTime GetDateTimeByType(string type,string dateStr) {
        DateTime result = DateTime.MaxValue;
        if (String.IsNullOrEmpty(dateStr))
        {
            return result;
        }
        else
        {
            result = Convert.ToDateTime(dateStr);
            if (type.Equals("endDate"))
            {
                result = new DateTime(result.Year, result.Month, result.Day, result.Hour + 23, result.Minute + 59, result.Second);
            }
        }
        return result;
    }


    /*
private bool GetValidQuarterRange(string dateStr1, string dateStr2)
{

   if (!validQuarterRange(dateStr1, dateStr2))
   {
       lblMsg.Text = "End Quarter cannot be before Effective Quarter";

       return false;
   }
   return true;
}
*/
    public bool DoesNotOverlap(IEnumerable<Stay> stays)
    {
        DateTime endPrior = DateTime.MinValue;
        foreach (Stay stay in stays.OrderBy(x => x.Start))
        {
            if (stay.Start > stay.End)
                return false;
            if (stay.Start < endPrior)
                return false;
            endPrior = stay.End;
        }
        return true;
    }

    public DateTime GetDateWithOpenEndDate(string studentIDStr, string familyIDStr, string roomNumberStr)
    {
        string sqlQuery = @"SELECT *
                              FROM [CCSInternationalStudent].[dbo].[HomestayPlacement]
                              WHERE applicantID = " + studentIDStr
                                   + " AND familyID = " + familyIDStr
                                   + " AND roomNumber = " + roomNumberStr;
        DataTable dtPlacements = hsInfo.RunGetQuery(sqlQuery);
        if (dtPlacements != null && dtPlacements.Rows.Count > 0)
        {

        }
        DateTime result = DateTime.Now;
        return result;
    }
    public bool validQuarterRange(string effectiveDateStr, string endDateStr)
    {
        //Assumption: effectiveDate is always valid because client-side force user to select an effectiveDate
        try
        {

            DateTime effectiveDate = GetDateTime(effectiveDateStr);
            DateTime endDate = GetDateTime(endDateStr);
            if (DateTime.Compare(effectiveDate, endDate) > 0)
            {
                return false;
            }
            return true;
        }
        catch (FormatException e) //this case happens when endDate = "--Select an end date--", return true because only effectiveDate is required
        {
            return true;
        }
    }
    public bool validDateRange(string effectiveDateStr, string endDateStr)
    {
        //Assumption: effectiveDate is always valid because client-side force user to select an effectiveDate
        try
        {

            DateTime effectiveDate = GetDateTimeByType("effectiveDate", effectiveDateStr);

            DateTime endDate = GetDateTimeByType("endDate", endDateStr);
            if (DateTime.Compare(effectiveDate, endDate) > 0)
            {
                return false;
            }
            return true;
        }
        catch (FormatException e) //this case happens when endDate = "--Select an end date--", return true because only effectiveDate is required
        {
            return true;
        }
    }

    public DateTime GetDateTime(string dateStr)
    {
        return DateTime.Parse(dateStr.Substring(dateStr.IndexOf(" ")));
    }
    public List<Stay> GetStaysByFamilyRoom(string familyIDStr, string rooNumberStr)
    {
        var result = new List<Stay>();
        string sqlQuery = @"SELECT *
                              FROM [CCSInternationalStudent].[dbo].[HomestayPlacement]
                              WHERE  familyID = " + familyIDStr
                                       + " AND roomNumber = " + rooNumberStr;
        DataTable dtPlacements = hsInfo.RunGetQuery(sqlQuery);
        if (dtPlacements != null && dtPlacements.Rows.Count > 0)
        {
            foreach (DataRow dr in dtPlacements.Rows)
            {
                //DateTime start = GetClassDate("FirstDayOfClass", dr["effectiveQuarter"].ToString());
                //DateTime end = GetClassDate("LastDayOfClass", dr["endQuarter"].ToString());
                DateTime start = GetDateTimeByType("effectiveDate", dr["effectiveQuarter"].ToString());
                DateTime end = GetDateTimeByType("endDate", dr["endQuarter"].ToString());
                result.Add(new Stay(start, end));
            }
        }

        return result;

       
    }

    public List<Stay> GetStaysByFamilyRoomExceptID(string familyIDStr, string rooNumberStr, string idStr)
    {
        var result = new List<Stay>();
        string sqlQuery = @"SELECT *
                              FROM [CCSInternationalStudent].[dbo].[HomestayPlacement]
                              WHERE  familyID = " + familyIDStr
                                       + " AND roomNumber = " + rooNumberStr
                                       + " AND id <> " + idStr;
        DataTable dtPlacements = hsInfo.RunGetQuery(sqlQuery);
        if (dtPlacements != null && dtPlacements.Rows.Count > 0)
        {
            foreach (DataRow dr in dtPlacements.Rows)
            {
                DateTime start = GetClassDate("FirstDayOfClass", dr["effectiveQuarter"].ToString());
                DateTime end = GetClassDate("LastDayOfClass", dr["endQuarter"].ToString());
                result.Add(new Stay(start, end));
            }
        }

        return result;
    }
    public List<Stay> GetStaysByFamilyRoomExcludeID(string familyIDStr, string rooNumberStr, string idStr)
    {
        var result = new List<Stay>();
        string sqlQuery = @"SELECT *
                              FROM [CCSInternationalStudent].[dbo].[HomestayPlacement]
                              WHERE  familyID = " + familyIDStr
                                       + " AND roomNumber = " + rooNumberStr
                                       + " AND id <> " + idStr;
        DataTable dtPlacements = hsInfo.RunGetQuery(sqlQuery);
        if (dtPlacements != null && dtPlacements.Rows.Count > 0)
        {
            foreach (DataRow dr in dtPlacements.Rows)
            {
                DateTime start = GetDateTimeByType("effectiveDate", dr["effectiveQuarter"].ToString());
                DateTime end = GetDateTimeByType("endDate", dr["endQuarter"].ToString());
                //DateTime start = GetClassDate("FirstDayOfClass", dr["effectiveQuarter"].ToString());
                //DateTime end = GetClassDate("LastDayOfClass", dr["endQuarter"].ToString());
                result.Add(new Stay(start, end));
            }
        }

        return result;
    }
    public DateTime GetClassDate(string dateType, string quarter)
    {
        DataTable dtQuarters = QMD.GetRangeRows_Quarter_MetaData_Base(-3, 20);
        DateTime result = DateTime.MaxValue;
        if (dtQuarters != null && dtQuarters.Rows.Count > 0)
        {
            foreach (DataRow dr in dtQuarters.Rows)
            {
                string rowQuarter = dr["Name"].ToString() + " " + Convert.ToDateTime(dr["FirstDayOfClass"]).Year.ToString();
                if (quarter.Equals(rowQuarter))
                {
                    result = DateTime.Parse(dr[dateType].ToString());
                }
            }
        }
        return result;
    }
    public bool IsOverlappedPeriods(DateTime startA, DateTime endA, DateTime startB, DateTime endB)
    {
        return DateTime.Compare(startA, endB) <= 0 && DateTime.Compare(startB, endA) <= 0;
        //return tStartA < tEndB && tStartB < tEndA;
    }
}