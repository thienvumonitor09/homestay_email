﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Class to manage homestay families
/// </summary>
public class FamilyManager
{
    private Dictionary<string, EntityObject> famHM;
    public FamilyManager()
    {
        famHM = LoadFamilyHM();
    }

    public Dictionary<string, EntityObject> GetFamilyHM()
    {
        return famHM;
    }

    public void SetFamilyHM(Dictionary<string, EntityObject> famHM)
    {
        this.famHM = famHM;
    }
    private Dictionary<string, EntityObject> LoadFamilyHM()
    {
        Dictionary<string, EntityObject> famHM = new Dictionary<string, EntityObject>();
        Homestay hsInfo = new Homestay();

       

        string connectionStr = ConfigurationManager.ConnectionStrings["ConnectionString_CCSInternationalStudent"].ToString();
        SqlConnection conn = new SqlConnection(connectionStr);
        string sqlQuery = @"SELECT *
                          FROM[CCSInternationalStudent].[dbo].[HomestayFamilyInfo]";


        DataTable dtFamilies = hsInfo.RunGetQuery(sqlQuery);
        foreach (DataRow row in dtFamilies.Rows)
        {
            string idStr = row["id"].ToString();
            Dictionary<string, string[]> attributes = new Dictionary<string, string[]>();
            foreach (DataColumn column in dtFamilies.Columns)
            {
                string columnName = column.ColumnName.Trim();
                string columnData = row[column].ToString().Trim();
                //To replace empty/null value for room occupancy with "No Info"
                if (columnName.ToLower().Contains("occupancy"))
                {
                    if (columnData.Length == 0)
                    {
                        columnData = "No Info";
                    }
                }
                //attributes.Add(columnName, new string[] { columnData });
                attributes[columnName] = new string[] { columnData };//Using this way instead of Add() to avoid exception if exist
            }
            //Add College : to match collegeQualified to college
            attributes["college"] = attributes["collegeQualified"];

            //replace homestayOption with yes/no
            if (attributes["homestayOption"][0].Equals("") || attributes["homestayOption"][0].Contains("Full"))
            {
                attributes["homestayOption1"] = new string[] { "yes" };
            }
            else
            {
                attributes["homestayOption1"] = new string[] { "no" };
            }
            //Add pref
            sqlQuery = @"SELECT  *
                        FROM [CCSInternationalStudent].[dbo].[HomestayPreferenceSelections]
                        WHERE familyID =" + idStr;
            DataTable dtPref = hsInfo.RunGetQuery(sqlQuery);
            foreach (DataRow rowPref in dtPref.Rows)
            {
                attributes[rowPref["preferenceID"].ToString()] = new string[] { rowPref["preferenceID"].ToString() };
            }
            


            //Add relatives
            sqlQuery = @"SELECT DOB
                        FROM [CCSInternationalStudent].[dbo].[HomestayRelatives]
                        WHERE (relationship LIKE '%child%' OR relationship LIKE '%adult%') AND familyID =" + idStr;
            DataTable dtRevalitves = hsInfo.RunGetQuery(sqlQuery);
            List<string> childList = new List<string>();
            foreach (DataRow rowRelatives in dtRevalitves.Rows)
            {
                DateTime today = DateTime.Today;
                DateTime dob = Convert.ToDateTime(rowRelatives["DOB"]);
                int age = today.Year - dob.Year;

                if (dob > today.AddYears(-age))
                {
                    age--;
                }
                if (age < 5)
                {
                    childList.Add("young");
                }
                else
                {
                    childList.Add("old");
                }

            }
            //attributes["children"] = dtRevalitves.AsEnumerable().Select(s => s.Field<DateTime>("DOB").ToString()).ToArray<string>();
            attributes["children"] = childList.ToArray();

            //Add dietary needs
            attributes = MatchingUtility.AddDiertaryNeedsAttributes(attributes);


            //Add attribute "smoker", "othersmoker"
            attributes = MatchingUtility.AddAttributes(attributes);

            //Add Gender
            attributes["gender"] = attributes["genderPreference"];

            //Add hobbies
            attributes = MatchingUtility.AddHobbiesAttributes(attributes);

            //Add interaction
            attributes = MatchingUtility.AddInteractionAttributes(attributes);

            //Add homeEnvironment
            attributes = MatchingUtility.AddHomeEnvironmentAttributes(attributes);

            //Add rooms
            string roomType = "Occupancy";
            for (var i = 1; i <= 4; i++)
            {
                var keyStr = "room" + i + roomType;
                var keyStrM = keyStr + "M";
                if (attributes[keyStr].Length == 0 || attributes[keyStr][0].Trim().Equals(""))
                {
                    attributes[keyStrM] = new string[] { "N/A" };
                }
                else
                {
                    attributes[keyStrM] = attributes[keyStr];
                }
            }

            //Add placed student info
            //to access values for room1, access by index by this order "studentID" , "placementType"
            //Create empty attributes for 4 rooms
            for (var i = 1; i <= 4; i++)
            {
                attributes["room" + i] = new string[] { };
            }
            sqlQuery = @"SELECT *
                       FROM [CCSInternationalStudent].[dbo].[HomestayPlacement]
                       WHERE familyID =" + idStr;
            DataTable dtPlacements = hsInfo.RunGetQuery(sqlQuery);
            foreach (DataRow rowPlacement in dtPlacements.Rows)
            {
                string roomNoStr = "room" + rowPlacement["roomNumber"].ToString().Trim();
                attributes[roomNoStr] = new string[]
                                        {
                                            rowPlacement["applicantID"].ToString().Trim()
                                            ,rowPlacement["placementType"].ToString().Trim()
                                            ,rowPlacement["effectiveQuarter"].ToString().Trim()
                                        };
            }

            //attributes["room1"] = new string[] {"studentID" , "placementType" };
            //Add familyStatus
            int countOccupiedRoom = 0;
            string updatedFamilyStatus = "";
            for (var i = 1; i <= 4; i++)
            {
                if (attributes["room" + i].Length > 0)
                {
                    countOccupiedRoom++;
                }
            }
            if (countOccupiedRoom == 0)
            {
                updatedFamilyStatus = "Open room(s)";
            }
            else if (countOccupiedRoom == 4)
            {
                updatedFamilyStatus = "Rooms Full";
            }
            else
            {
                updatedFamilyStatus = "Open Room-Student Placed";
            }
            //update famHM
            attributes["familyStatus"] = new string[] { updatedFamilyStatus };

            EntityObject entityObject = new EntityObject() { Name = idStr, Attributes = attributes };
            famHM.Add(idStr, entityObject);
        }

        //Detect smoker/nonsmoker and Add new attributes 


        //Filter based on open rooms. If all 4 rooms are occupied, exlcude that room
        Dictionary<string, EntityObject> filteredFamilies = new Dictionary<string, EntityObject>();
        string[] rooms = { "room1Occupancy", "room2Occupancy", "room3Occupancy", "room4Occupancy" };
        foreach (var f in famHM)
        {
            Dictionary<string, string[]> attributes = f.Value.Attributes;
            int count = 0;
            foreach (var r in rooms)
            {
                if (attributes[r][0].ToLower().Contains("ccs")) //for ccs and non-ccs
                {
                    count++;
                }

            }

            if (count < 4)
            {
                filteredFamilies.Add(f.Key, new EntityObject { Name = f.Key, Attributes = attributes });
            }

        }
        //return filteredFamilies;
        return famHM;



    }
}