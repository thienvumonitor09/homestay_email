﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CCSApps.master" CodeFile="ReportsHome.aspx.cs" Inherits="Homestay_Admin_Reports_ReportsHome" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
<style type="text/css">
    p
    {
        padding:2px;
        margin:0px;
    }
</style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="bannerH1Text" Runat="Server">
    Homestay Administration
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:Button ID="btnAdminPage" runat="server" Text="Return to Admin Page" OnClick="btnAdminPage_Click"/>
<h2>Admin Reports</h2>
    

    <ul>
        <li>List all <a href="Student_ListActive.aspx">active Homestay Students</a></li>
        <li>List all <a href="Family_ListActive2.aspx">active Homestay Families</a></li>
        <!--
        <li><a href="PlacementForm_V.aspx" target="_blank">New Student Placement Form</a></li>
        <li><a href="PlacementForm_Student.aspx" target="_blank">Homestay Family</a></li>
        -->
    </ul>

</asp:Content>
