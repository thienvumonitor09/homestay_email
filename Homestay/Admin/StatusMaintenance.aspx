﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CCSApps.master" AutoEventWireup="true" CodeFile="StatusMaintenance.aspx.cs" Inherits="Homestay_Admin_StatusMaintenance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bannerH1Text" Runat="Server">
    Status Maintenance
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:Literal ID="litResultMsg" runat="server" ></asp:Literal>
    <input type="submit" name="btnSubmit" id="btnHome" value="Return to Admin Home Page" style="float: right;"/>&nbsp;&nbsp;
    
   
    <fieldset id="fldStatusMaintenance">
    <legend>Status Maintenance</legend>
        <div style="clear:both;float:left;width:30%;margin-bottom:10px;">
            <h3>Students</h3>
            <p>
                <asp:DropDownList ID="ddlStudents" AutoPostBack="true" runat="server"></asp:DropDownList><br />
                Current Status: <br />
                <asp:TextBox ID="studentStatus" runat="server" Width="150px" ReadOnly="true" MaxLength="50" style="margin-top:5px;"></asp:TextBox><br />
                <label for="ddlStudentStatus">Update Student Status to:</label><br />
                <asp:DropDownList ID="ddlStudentStatus" runat="server" style="margin-top:5px;"></asp:DropDownList><br />
                <input type="submit" name="btnSubmit" value="Update Student Status" style="margin-top:5px;" />
            </p>
        </div>
        <div style="float:left;width:30%;margin-bottom:10px;">
            <h3>Families</h3>
            <p>
                <asp:DropDownList ID="ddlFamilies" AutoPostBack="true" runat="server"></asp:DropDownList><br />
                Current Status: <br />
                <asp:TextBox ID="familyStatus" runat="server" Width="150px" ReadOnly="true" MaxLength="50" style="margin-top:5px;"></asp:TextBox><br />
                <label for="ddlFamilyStatus">Update Family Status to:</label><br />
                <asp:DropDownList ID="ddlFamilyStatus" runat="server" style="margin-top:5px;"></asp:DropDownList>
                <input type="submit" name="btnSubmit" value="Update Family Status" style="margin-top:5px;" />
            </p>
        </div>

    </fieldset>
</asp:Content>

