﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CCSApps.master" AutoEventWireup="true" CodeFile="Activation.aspx.cs" Inherits="Homestay_Admin_TEST_Activation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bannerH1Text" Runat="Server">
    Activate/De-activate
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">
    <div style="float:left;width:70%;">
        <p>Set the student or family to inactive when they will no longer participate in the Homestay program.  This removes their name
        from most drop-down lists.  They can be reactivated using this form as well.</p>
        <asp:Literal ID="litResultMsg" runat="server"></asp:Literal>
    </div>
    <div id="divHome" runat="server" style="float:right;margin-right:30px;">
        <input type="submit" name="btnSubmit" id="btnHome" value="Return to Admin Home Page"/>&nbsp;&nbsp;
    </div>
    <fieldset id="fldActivation" style="clear:both;">
    <legend>Activate/De-activate</legend>
        <div style="clear:both;float:left;width:450%;margin-bottom:10px;">
            <p>Select a Student<br />
                <asp:DropDownList ID="ddlAllStudents" AutoPostBack="true" runat="server"></asp:DropDownList>
            </p>
            <div id="eAllStudents" class="divError" runat="server"></div>
            <p><label for="rdoIsStudentActive">Is Student Active?</label><br />
                <asp:RadioButtonList ID="rdoIsStudentActive" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
                    <asp:ListItem Value="1">Yes</asp:ListItem>
                    <asp:ListItem Value="0">No</asp:ListItem>
                </asp:RadioButtonList>
            </p>
            <div id="eStudentActive" class =" divError" runat="server"></div>
            <p><label for="ddlStudentStatus">Current Student Status</label><br />
                <asp:TextBox ID="homestayStatus" ReadOnly="true" MaxLength="50" runat="server"></asp:TextBox><br />
                <label for="ddlStudentStatus">Change Status</label><br />
                <asp:DropDownList ID="ddlStudentStatus" runat="server" style="margin-top:5px;"></asp:DropDownList></p>
            <div id="eStudentStatus" class="divError" runat="server"></div>
            <p><label for="dateInactive">Date Deactivated:</label><br />Note: when re-activating a student, delete the date below<br />
                <asp:TextBox ID="dateInactive" runat="server"></asp:TextBox><br />
            </p>
            <div id="eDateInactive" class="divError" runat="server"></div>
            <p>
              <!--  <input type="submit" name="btnSubmit" value="Update Student" style="margin-top:5px;" /> -->
                <asp:Button ID="btnCmdUpdateStudent" runat="server" Text="Update Student" OnClick="btnCmdUpdateStudent_Click" />
            </p>
        </div>
        <div style="clear:both;float:left;width:45%;margin-bottom:10px;border-top:1px solid grey;">
            <p>
                <label for="rdoIsFamilyActive">Select a Family</label><br />
                <asp:DropDownList ID="ddlAllFamilies" AutoPostBack="true" runat="server"></asp:DropDownList>
            </p>
            <div id="eAllFamilies" class="divError" runat="server"></div>
            <p>
                <label for="rdoIsFamilyActive">Is Family Active?</label><br />
                <asp:RadioButtonList ID="rdoIsFamilyActive" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
                    <asp:ListItem Value="1">Yes</asp:ListItem>
                    <asp:ListItem Value="0">No</asp:ListItem>
                </asp:RadioButtonList>
            </p>
            <div id="eFamilyActive" class="divError" runat="server"></div>
            <p>
                <label for="ddlFamilyStatus">Current Family Status</label><br />
                <asp:TextBox ID="familyStatus" ReadOnly="true" MaxLength="50" runat="server"></asp:TextBox><br />
                <label for="ddlFamilyStatus">Change Status</label><br />
                <asp:DropDownList ID="ddlFamilyStatus" runat="server" style="margin-top:5px;"></asp:DropDownList><br />
            </p>
            <div id="eFamilyStatus" class="divError" runat="server"></div>
            <p>
                <label for="dateTerminated">Date Terminated</label><br />Note: if re-activating a family, delete the date below<br />
                <asp:TextBox ID="dateTerminated" runat="server"></asp:TextBox>
            </p>
            <div id="eDateTerminated" class="divError" runat="server"></div>
            <p>
              <!--  <input type="submit" name="btnSubmit" value="Update Family" style="margin-top:5px;" /> -->
                <asp:Button ID="btnCmdUpdateFamily" runat="server" Text="Update Family" OnClick="btnCmdUpdateFamily_Click" />
            </p>
        </div>
    </fieldset>

</asp:Content>

