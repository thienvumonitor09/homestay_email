﻿<%@ Page Language="C#" Title="New Student Placement" validateRequest="false" AutoEventWireup="true" MasterPageFile="~/CCSApps.master" CodeFile="PlacementForm_V3.aspx.cs" Inherits="Homestay_Admin_Reports_PlacementForm_V3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
<style type="text/css">
   textarea { height: auto; }
    p
    {
        padding:2px;
        margin:0px;
    }
    .editClass{
        width:80%;
    }
</style>
    <link href="/Homestay/_css/Print.css" rel="stylesheet" type="text/css" media="print" />
    <script src="/Homestay/_js/ckeditor/ckeditor.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="bannerH1Text" Runat="Server">
    Homestay Administration
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">
     
<div style="width:100%;text-align:center;margin-bottom:50px;">
    <div style="width:30%;float:left">
        <img src="/Homestay/images/ccs-logo.jpg" style="height: 104px; width: 211px" />
    </div>
    <div style="width:70%">
        <h1><b>Global Education Homestay Program</b></h1>
        <h2>New Student Placement</h2>
    </div>
    
    

</div>
    
<div id="divHome" class="noPrint" runat="server" style="float:right;margin-right:30px;">
    <asp:Button  ID="btnHome" Text="Return to Admin Home Page" runat="server" class="ButtonAdmin" OnClick="linkBtnHome_Click"/>&nbsp;&nbsp;
</div>
    <asp:Literal ID="litResutlMsg" runat="server"></asp:Literal>
    
    <br /><br /><br />
    <div style="height:800px;">
        <textarea name="editor1" id="editor1" rows="5000" cols="80"  style="height:100%;">
             
    </textarea>
    </div>
   
<asp:button id="btnSendMailClient" Cssclass="noPrint" runat="server" Onclick="SendMail_Click" Text="Send Mail" UseSubmitBehavior="false" data-dismiss="modal"/> 

<button id="btnClick" onclick="clickme()">Click me</button> 
    <p><input type="button" name="btnPrint" class="noPrint" onclick="javascript:window.print();" value="Print" /></p>
   
    <script>

                // Replace the <textarea id="editor1"> with a CKEditor
                // instance, using default configuration.
        $(function () { 
                    var editor = CKEDITOR.instances['editor1'];
                    if (editor) { editor.destroy(true); }
                    CKEDITOR.replace('editor1');
	          //CKEDITOR.replace( 'editor1' );  
           // $('#editor1').append("123"); 
            $.ajax({
                type: "POST",
                url: "PlacementForm_V3.aspx/GetString",
                //data: dataValue,
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert("Request: " + XMLHttpRequest.toString() + "\n\nStatus: " + textStatus + "\n\nError: " + errorThrown);
                },
                success: function (result) {
                    //alert("We returned: " + result.d);
                    console.log(result.d);
                    var s = result.d;
                    $('#editor1').html(s);
                }
                });
            })
        function pageLoad(){

             //CKEDITOR.replace('editor1');

        }
        function clickme() {
            alert("email")
        };
            </script>

</asp:Content>
