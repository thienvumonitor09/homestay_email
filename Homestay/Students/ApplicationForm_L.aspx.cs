﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Security.Principal;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;


public partial class Homestay_ApplicationForm_L : System.Web.UI.Page
{
    

    /* FIELD LIST from vw_StudentInfoExtended
        hsGrad, hsGradDate, studyWhat, studyWhere, 
        studyMajor, studyMajorOther, trSchools, studiesBegin, 
        id, familyName, firstName, middleNames, 
        fullName, DOB, Gender, studentStatus, 
        countryOfBirth, countryOfCitizenship, englishAbility, visaType, 
        numberDependents, Spouse, Children, inUS, 
        useAgency, sendI_20, releaseInformation, allTrue, 
        howHearCCS, dateSubmitted, SID, maritalStatus, 
        addressCode, addresseeName, agencyName, Addr, 
        City, StateProv, Zip, Country, Phone, 
        CellPhone, Email, Relationship, healthIns
     */

    /************************************************************************************
    *  address codes:  1:  student's permanent address
    *                  2:  student's parent's address (if different)
    *                  3:  student's agency address (if any)
    *                  4:  student's relative address (if in US)
    *                  5:  student's emergency contact address
    *  Use applicant ID to match to student
    * ********************************************************************************/

    /* FIELD LIST from ContactInformation
        applicantID, addressCode, addresseeName, agencyName
		Addr, City, StateProv, Zip, Country
		Phone, CellPhone, Email, Relationship, speakEnglish
     */
    /*
     * Student Status:
     * App Submitted
     * App Complete
     * Placed
     * Withdrew
     * Graduated
     * Visa Denied
     * Deferred
     * Moved Out
     */
    Boolean blIsAdmin = false;
    String strDOB = "";
    String strFirstName = "";
    String strLastName = "";
    Int32 intApplicantID = 0;
    String strSID = "";
    Int32 intAgencyID = 0;
    String strGender = "";
    String strCountry = "";
    String strStudentEmail = "";
    String strArrivalDate = "";
    String strAgency = "";
    String strAgentName = "";
    String strAgentEmail = "";
    String strAgencyAddress = "";
    String strAgencyCity = "";
    String strAgencyState = "";
    String strAgencyCountry = "";
    String strAgencyZIP = "";
    String strAgencyPhone = "";
    Boolean blUseAgency = false;
    String strCollege = "";
    String strHomestayOption = "";
    String strPayMethod = "";
    Boolean blNoRecords = false;
    Int32 intIsActive = 1;
    String strResultMsg = "";
    String strButtonFace = "";
    DateTime dtDOB = System.DateTime.Today;
    String strYear = System.DateTime.Today.Year.ToString();
    DateTime dtFirstDayOfClass;
    DateTime dtLatestDOB;
    Boolean blUnderAge = false;
    String strUploadURL = @"\\ccs-internet\InternetContent\CCS\Homestay\";
    String strUploadServer = "";
    String strFileName = "";
    String strQuarterStartDate = "";
    String strSelectedQuarter = "";
    Boolean blLetterSubmitted = false;
    Boolean blPhotoSubmitted = false;
    String studentStatus = "App Submitted";
    Int32 intNewIdentity = 0;
    Int32 intResult = 0;
    String strSubmitMsg = "";
    String strBlank = "";
    char speakEnglish = '0';
    Boolean blDuplicateBasicInfoRecords = false;
    Boolean blAddMode = true;
    String strAddMode = "";
    String selectedStudent = "";
    String applicantID = "";

    

    DateTime outDate;               // used in try/catch to validate dob on form
    Boolean dateSuccess = false;    // used in try/catch to validate dob on form
    Boolean validateError = false;

    String postBackControlName = "";

    Homestay hsInfo = new Homestay();
    Quarter_MetaData QMD = new Quarter_MetaData();
    HomestayUtility hsUtility = new HomestayUtility();
   
    protected void Page_Load(object sender, EventArgs e)
    {
        String strLogonUser = Request.ServerVariables["LOGON_USER"];
        litResultMsg.Text = "";
        //For testing: get security context
        //WindowsIdentity winID = WindowsIdentity.GetCurrent();
        //strResultMsg = "<p><b>Security context:</b> " + winID.Name + "</p>";

        if (strLogonUser != "" && strLogonUser != null) 
        { 
            //If user has not logged out of Family Portal, forms authentication considers the person logged in
            if (strLogonUser.Contains("ccs\\"))
            {
                blIsAdmin = true; 
            }
        }

       if (IsPostBack)
        {
            applicantID = hdnApplicantID.Value;
            
        }
        if (Request.QueryString["ID"] != null)
        {
            //page load
            applicantID = Request.QueryString["ID"].ToString();
        }
        if (applicantID != "")
        {
            //returning from Profile page
            selectedStudent = applicantID;
        }
        //strResultMsg += "applicantID: " + applicantID + "<br />";
        if (applicantID != "0" && applicantID != "" && applicantID != null)
        {
            intApplicantID = Convert.ToInt32(applicantID);
            hdnApplicantID.Value = applicantID;
        }
        
        
        /*      
              //Not an ASP.Net control
              if (Request.Form["btnSubmit"] != null) { strButtonFace = Request.Form["btnSubmit"].ToString(); }
              //Redirect without processing
              if (strButtonFace == "Continue") { Response.Redirect("PlacementProfile.aspx?ID=" + hdnApplicantID.Value); }
              //Admin function only
              if (strButtonFace == "Reset Fields") { resetFields(); ddlStudents.SelectedValue = "0"; }
              if (strButtonFace == "Return to Admin Home Page" && blIsAdmin){ Response.Redirect("/Homestay/Admin/Default.aspx"); }
       */

        //strResultMsg += "Button: " + strButtonFace + "<br />";
        strSelectedQuarter = rdoQuarterStart.SelectedValue;

//        Response.Write("postback? " + IsPostBack.ToString() + "<br />");

        if (!IsPostBack)
        {
                litErr.Text = "";

                //Visibility for initial page load
                divMorePersonalInfo.Visible = false;
                recruitingAgent.Visible = false;
                personalInformation.Visible = true;
                divSearchButton.Visible = true;
                homestay18.Visible = false;
                homestay1617.Visible = false;
                PhotoLetter.Visible = false;
                divSave.Visible = false;
                divNextPage.Visible = false;

                if (blIsAdmin)
                {
                    strResultMsg = "";
                    //strResultMsg = "<p><b>strLogonUser:</b> " + strLogonUser + "; Is admin? " + blIsAdmin + "</p>";
                    //Admin acess and tools
                    if (ddlStudents.SelectedValue == "")
                    {
                        selectedStudent = applicantID;
                    }
                    else
                    {
                        //override any query string value
                        selectedStudent = ddlStudents.SelectedValue;
                    }
                    if (selectedStudent != "0" && selectedStudent != "" && selectedStudent != null)
                    {
                        applicantID = selectedStudent;
                        intApplicantID = Convert.ToInt32(applicantID);
                        hdnApplicantID.Value = selectedStudent;
                    }
                    //Set visibility
                    divAdmin.Visible = true;
                    divHome.Visible = true;
                    divNextPage.Visible = true;
                    //Fill student drop-down
                    DataTable dtStudents = hsInfo.GetHomestayStudents("Active", 0);
                    if (dtStudents != null)
                    {
                        if (dtStudents.Rows.Count > 0)
                        {
                            ddlStudents.Items.Clear();
                        System.Web.UI.WebControls.ListItem LI;
                            LI = new System.Web.UI.WebControls.ListItem();
                            LI.Value = "0";
                            LI.Text = "-- Add New Student --";
                            ddlStudents.Items.Add(LI);
                            foreach (DataRow dr in dtStudents.Rows)
                            {
                                LI = new System.Web.UI.WebControls.ListItem();
                                LI.Value = dr["applicantID"].ToString();
                                LI.Text = dr["familyName"].ToString() + ", " + dr["firstName"].ToString() + " - " + Convert.ToDateTime(dr["DOB"]).ToShortDateString();
                                ddlStudents.Items.Add(LI);
                            }
                            if (selectedStudent != "") { ddlStudents.SelectedValue = selectedStudent; }
                        }
                        else if (blIsAdmin) { strResultMsg += "Error: dtStudents has no rows.<br />"; }
                    }
                    else if (blIsAdmin) { strResultMsg += "Error: dtStudents is null.<br />"; }

                }
                else
                {
                    //Student view
                    divAdmin.Visible = false;
                    divHome.Visible = false;
                }
                DataTable dtQuarters = QMD.GetRangeRows_Quarter_MetaData_Base(0, 3);
            System.Web.UI.WebControls.ListItem LI2;
                if (dtQuarters != null)
                {
                    if (dtQuarters.Rows.Count > 0)
                    {
                        rdoQuarterStart.Items.Clear();
                        LI2 = new System.Web.UI.WebControls.ListItem();
                    /*
                    LI2.Value = "Current student";
                    LI2.Text = "Current student";
                    rdoQuarterStart.Items.Add(LI2);
                    */
                    foreach (DataRow dr in dtQuarters.Rows)
                        {
                            LI2 = new System.Web.UI.WebControls.ListItem();
                            LI2.Value = Convert.ToDateTime(dr["FirstDayOfClass"]).ToShortDateString();
                            LI2.Text = dr["Name"].ToString() + " - " 
                                        + String.Format("{0:MM/dd/yyyy}", Convert.ToDateTime(dr["FirstDayOfClass"]));
                            rdoQuarterStart.Items.Add(LI2);
                            //strResultMsg += "Item: " + dr["Name"].ToString() + " - " + Convert.ToDateTime(dr["FirstDayOfClass"]).ToShortDateString() + "<br />";
                        }
                    
                        if (strSelectedQuarter != "") { rdoQuarterStart.SelectedValue = strSelectedQuarter; }
                    }
                    else if (blIsAdmin) { strResultMsg += "QMD has no rows.<br />"; }
                }
                else if (blIsAdmin) { strResultMsg += "QMD is null.<br />"; }
            postBackControlName = "";
        } else                  // not a postback
        {
            postBackControlName = Page.Request.Params["__EVENTTARGET"];
//            Response.Write("control name: " + postBackControlName + "<br />");
        }
       // if (IsPostBack || postBackControlName.Trim() == "ddlStudents")
        if((IsPostBack && validateError) || Request.QueryString["UA"] != null || (IsPostBack && blIsAdmin && postBackControlName.Trim() == "ddlStudents"))
       // if (postBackControlName.Trim() == "ddlStudents" || Request.QueryString["UA"] != null)
        {
            if (blIsAdmin && postBackControlName.Trim() == "ddlStudents")
            {
                selectedStudent = ddlStudents.SelectedValue;
 //               Response.Write("control: " + selectedStudent + "<br />");
                //set visibility of admin info for admins
                if (blIsAdmin)
                {
                    divAdmin.Visible = true;
                    divHome.Visible = true;
                }
                intApplicantID = Convert.ToInt32(selectedStudent);
            }
            hdnApplicantID.Value = intApplicantID.ToString();
            //do something
            DataTable dtStudent = hsInfo.GetOneInternationalStudent("", intApplicantID, "", "", "", "");
            if (dtStudent != null)
            {
                if (dtStudent.Rows.Count > 0)
                {
                    //clear any values from previous selection
                    resetFields();
                    if (dtStudent.Rows.Count > 1) { blDuplicateBasicInfoRecords = true; }
                    

                    strFirstName = dtStudent.Rows[0]["firstName"].ToString();
                    txtFirstName.Text = strFirstName;
                    strLastName = dtStudent.Rows[0]["familyName"].ToString();
                    txtFamilyName.Text = strLastName;
                    strGender = dtStudent.Rows[0]["Gender"].ToString();
                    strSID = dtStudent.Rows[0]["SID"].ToString();
                    hdnSID.Value = strSID;
                    txtSID.Text = strSID;
                    dtDOB = Convert.ToDateTime(dtStudent.Rows[0]["DOB"]);
                    txtDOB.Text = dtDOB.ToShortDateString();
                    if (strQuarterStartDate != "")
                    {
                        dtFirstDayOfClass = Convert.ToDateTime(strQuarterStartDate);
                    }
                    else
                    {
                        strQuarterStartDate = QMD.NextQtrFirstClass.ToShortDateString();
                        dtFirstDayOfClass = QMD.NextQtrFirstClass;
                    }
                    dtLatestDOB = dtFirstDayOfClass.AddYears(-18);
                    if (dtLatestDOB < dtDOB)
                    {
                        blUnderAge = true;
                        hdnAgeLevel.Value = "true";
                        divNextPage.Visible = true;
                    }
                    else
                    {
                        blUnderAge = false;
                        hdnAgeLevel.Value = "false";
                    }
                    //strResultMsg += "DOB threshold: " + dtLatestDOB.ToShortDateString() + "; underage? " + blUnderAge + "<br />";
                    strCountry = dtStudent.Rows[0]["Country"].ToString();
                    strStudentEmail = dtStudent.Rows[0]["Email"].ToString();
                    blUseAgency = Convert.ToBoolean(dtStudent.Rows[0]["useAgency"]);
                    //Check with Homestay if they want this behavior
                    if (blUseAgency)
                    {
                        recruitingAgent.Visible = true;
                        DataTable dtContact = hsInfo.GetOneContact(3, intApplicantID);
                        if (dtContact != null)
                        {
                            if (dtContact.Rows.Count > 0)
                            {
                                strAgency = dtContact.Rows[0]["agencyName"].ToString();
                                txtAgency.Text = strAgency;
                                strAgentName = dtContact.Rows[0]["addresseeName"].ToString();
                                txtAgentName.Text = strAgentName;
                                strAgentEmail = dtContact.Rows[0]["Email"].ToString();
                                txtAgentEmail.Text = strAgentEmail;
                                strAgencyAddress = dtContact.Rows[0]["Addr"].ToString();
                                txtAgencyAddress.Text = strAgencyAddress;
                                strAgencyCity = dtContact.Rows[0]["City"].ToString();
                                txtAgencyCity.Text = strAgencyCity;
                                strAgencyState = dtContact.Rows[0]["StateProv"].ToString();
                                txtAgencyState.Text = strAgencyState;
                                strAgencyZIP = dtContact.Rows[0]["Zip"].ToString();
                                txtAgencyZip.Text = strAgencyZIP;
                                strAgencyCountry = dtContact.Rows[0]["Country"].ToString();
                                txtAgencyCountry.Text = strAgencyCountry;
                                strAgencyPhone = dtContact.Rows[0]["Phone"].ToString();
                                txtAgencyPhone.Text = strAgencyPhone;
                                hdnAgencyID.Value = dtContact.Rows[0]["id"].ToString();
                            }
                        }
                    }
                    else
                    {
                        recruitingAgent.Visible = false;
                    }
                    strCollege = dtStudent.Rows[0]["studyWhere"].ToString();
                    if (strCollege == "Pullman") { strCollege = "SFCC"; }
                    if (blIsAdmin)
                    {
                        //radio buttons only available to staff
                        rdoWhereStudy.SelectedValue = strCollege;
                    }
                    rdoGender.SelectedValue = strGender;
                    txtCountry.Text = strCountry;
                    txtPermEmail.Text = strStudentEmail;
                }
            }// end IsPostback generated from the student drop-down
            
        }

        //Set mode: insert or update
        strAddMode = hdnAddMode.Value;
        if (strAddMode.ToLower() == "false") { blAddMode = false; }
        if (Request.QueryString["UA"] != null)
        {
            if (Request.QueryString["UA"] == "f")
                blUnderAge = false;
            else
                blUnderAge = true;
        }
     
        if (!IsPostBack)
        {
            bool blstring = false;
            bool blnull = false;
            if(hdnApplicantID.Value != "")
                blstring = true;
            else
                blstring = false;
            if(hdnApplicantID.Value != null)
                blnull = true;
            else
                blnull = false;

         //   Response.Write("blstring: " + blstring.ToString() + "; blnull: " + blnull.ToString() + "<br />"); 

           // for use when Back button is clicked on profile page
            if (hdnApplicantID.Value != "" && hdnApplicantID.Value != null)
            {
                // get the data from the tables and populate the text boxes
                //Find the student and populate the info
                DataTable dtInfo = hsInfo.GetOneInternationalStudent("", Convert.ToInt32(hdnApplicantID.Value), "01-01-1900", "", "", "");
                if (dtInfo != null)
                {
                    txtFamilyName.Text = dtInfo.Rows[0]["familyName"].ToString();
                    txtFirstName.Text = dtInfo.Rows[0]["firstName"].ToString();
                    txtDOB.Text = String.Format("{0:MM/dd/yyyy}", Convert.ToDateTime(dtInfo.Rows[0]["DOB"]));
                    txtCountry.Text = dtInfo.Rows[0]["Country"].ToString();
                    txtPermEmail.Text = dtInfo.Rows[0]["Email"].ToString();
                    rdoGender.SelectedValue = dtInfo.Rows[0]["Gender"].ToString();
                }

            }
            
        }

        if ((IsPostBack && blIsAdmin && postBackControlName.Trim() != "ddlStudents") || Request.QueryString["UA"] != null)
        {

            //Collect data for insert/update 
            strFirstName = txtFirstName.Text;
            strLastName = txtFamilyName.Text;
            strDOB = txtDOB.Text;
            strSID = txtSID.Text;
            //            Response.Write("ispostback NOT ddlStudents: " + strSID + "<br />");
            
            strAgency = txtAgency.Text.Trim().Replace("'", "''");
            strAgentName = txtAgentName.Text.Trim().Replace("'", "''");
            strAgentEmail = txtAgentEmail.Text.Trim().Replace("'", "''");
            strAgencyAddress = txtAgencyAddress.Text.Trim().Replace("'", "''");
            strAgencyCity = txtAgencyCity.Text.Trim().Replace("'", "''");
            strAgencyState = txtAgencyState.Text.Trim().Replace("'", "''");
            strAgencyZIP = txtAgencyZip.Text.Trim().Replace("'", "''");
            strAgencyCountry = txtAgencyCountry.Text.Trim().Replace("'", "''");
            strAgencyPhone = txtAgencyPhone.Text.Trim().Replace("'", "''");
            strHomestayOption = rdoHomestayOption.SelectedValue;
            strArrivalDate = txtArrivalDate.Text.Trim().Replace("'", "''");
            //Response.Write("arrival date 443: " + strArrivalDate + "<br />");
//            Response.Write("Homestay: " + strHomestayOption + "<br />");

            strCollege = rdoWhereStudy.SelectedValue;
//            Response.Write("college: " + strCollege + "<br />");

            strGender = rdoGender.SelectedValue;
            strCountry = txtCountry.Text.Trim().Replace("'", "''");
            strStudentEmail = txtPermEmail.Text.Trim().Replace("'", "''");

        }

        strQuarterStartDate = rdoQuarterStart.SelectedValue;

       // Response.Write("arrive: " + strArrivalDate + "<br />");
        if (strArrivalDate != "" && strArrivalDate != "1900-01-01")
            txtArrivalDate.Text = strArrivalDate;
        if (strHomestayOption != "")
        {
            rdoHomestayOption.SelectedValue = strHomestayOption;
        }

        if (hdnLetterSubmitted.Value.ToLower() == "true")
        {
            
        }
        if (hdnPhotoSubmitted.Value.ToLower() == "true")
        {

        }

        if (hdnAgeLevel.Value == "true")
        {
            blUnderAge = true;
            homestay1617.Visible = false;
            homestay1617.Visible = true;
        }
        else if (hdnAgeLevel.Value == "false")
        {
            homestay1617.Visible = false;
            homestay18.Visible = true;
        }
        else
        {
            homestay18.Visible = false;
            homestay1617.Visible = false;
        }

        if (intApplicantID > 0 && !blDuplicateBasicInfoRecords)
        {
            //Set visibility
            personalInformation.Visible = true;
            divMorePersonalInfo.Visible = true;
            divSearchButton.Visible = false;
            if (blUnderAge)
            {
                homestay18.Visible = false;
                homestay1617.Visible = true;
            }
            else
            {
                homestay18.Visible = true;
                homestay1617.Visible = false;
            }
            PhotoLetter.Visible = true;
            divSave.Visible = true;
            if (blIsAdmin)
            {
                divNextPage.Visible = true;
            }

            //Check for existing HomestayStudentInfo record
            DataTable dtHSStudent = hsInfo.GetOneHomestayStudent(intApplicantID);
            if (dtHSStudent != null)
            {
                if (dtHSStudent.Rows.Count > 0)
                {
                    blAddMode = false;
                    hdnAddMode.Value = blAddMode.ToString();
                    //strResultMsg += "blAddMode 1? " + blAddMode + "<br />";
                    Configuration config = WebConfigurationManager.OpenWebConfiguration(Request.ApplicationPath);
  //                  Response.Write("config: " + config.ToString() + "<br />");
                    AppSettingsSection appSettings = (AppSettingsSection)config.GetSection("appSettings");
                    strUploadServer = "/InternetContent" + appSettings.Settings["HomestayShare"].Value;
                    foreach (DataRow drHSStudent in dtHSStudent.Rows)
                    {
                        blLetterSubmitted = Convert.ToBoolean(drHSStudent["letterSubmitted"]);
                        hdnLetterSubmitted.Value = blLetterSubmitted.ToString();
                        blPhotoSubmitted = Convert.ToBoolean(drHSStudent["photoSubmitted"]);
                        hdnPhotoSubmitted.Value = blPhotoSubmitted.ToString();
                        if (blLetterSubmitted || blPhotoSubmitted)
                        {
                            litLetterFile.Text = "";
                            litPhotoFile.Text = "";
                            DataTable dt = hsInfo.GetUploadedFiles(intApplicantID);
                            if (dt != null)
                            {
                                if (dt.Rows.Count > 0)
                                {
                                    foreach (DataRow dr in dt.Rows)
                                    {
                                        strFileName = dr["fileName"].ToString();
                                        string filePath = dr["filePath"].ToString();
                                        string uploadServer = "";
                                        if (filePath.Contains("ccs-internet"))
                                        {
                                            uploadServer += @"http:\\apps.spokane.edu\InternetContent\Homestay\";
                                        }
                                        else
                                        {
                                            uploadServer += @"http:\\portal.ccs.spokane.edu\InternetContent\Homestay\";
                                        }
                                        if (strFileName.Contains("Letter"))
                                        {
                                            //get the file name
                                            litLetterFile.Text += "<br /><b>Letter submitted:</b> " + "<a href=\"" + uploadServer + strFileName + "\" target=\"_blank\">" + strFileName + "</a>";
                                        }
                                        if (strFileName.Contains("Photo"))
                                        {
                                            //Get the file name
                                            litPhotoFile.Text += "<br /><b>Photo submitted:</b> " + "<a href=\"" + uploadServer + strFileName + "\" target=\"_blank\">" + strFileName + "</a>";
                                        }
                                    }
                                }
                            }
                        }
                        //strResultMsg += "Letter submitted: " + Convert.ToBoolean(drHSStudent["letterSubmitted"]) + "<br />";
                        //strResultMsg += "Start Date: " + Convert.ToDateTime(drHSStudent["quarterStartDate"]).ToShortDateString() + "<br />";
                        if (Convert.ToDateTime(drHSStudent["quarterStartDate"]).ToShortDateString() != "1/1/1900")
                        {
                            try
                            {
                                rdoQuarterStart.SelectedValue = Convert.ToDateTime(drHSStudent["quarterStartDate"]).ToShortDateString();
                            }
                            catch
                            {
                                //do nothing
                            }
                        }
                        txtArrivalDate.Text = Convert.ToDateTime(drHSStudent["arrivalDate"]).ToShortDateString();
                        if (blIsAdmin)
                        {
                            txtDateApplied.Text = Convert.ToDateTime(drHSStudent["dateApplied"]).ToShortDateString();
                        }
                        //Response.Write(intApplicantID);
                        DataTable dtNotes = hsInfo.GetHomestayNotesExtended("Student", ""+intApplicantID, "0", "0", "0", "", "", "");
                        if (dtNotes != null)
                        {

                            if (dtNotes.Rows.Count > 0)
                            {
                                gridView.DataSource = dtNotes;
                                gridView.DataBind();
                                //gridView.Columns[dtNotes.Columns["familyID"].Ordinal].Visible = false;
                                DataRow dr = dtNotes.Rows[0];
                                subjectID.Text = dr["Subject"].ToString();
                                noteID.Text = dr["Note"].ToString();
                                if (dr["actionNeeded"].ToString() == "True")
                                {
                                    actionNeededID.Text = "Yes";
                                }
                                
                                dateCreatedID.Text = dr["createDate"].ToString();
                                noteTypeID.Text = dr["noteType"].ToString();
                            }

                        }
                        if (!blUnderAge)
                        {
                            rdoHomestayOption.SelectedValue = drHSStudent["homestayOption"].ToString().Trim();
                        }
                    }
                    if (blIsAdmin)
                    {
                        strResultMsg += "A Homestay student application exists.  You may review the information here.<br />";
                    }
                    else
                    {
                        //setVisibilityNoAccess();
                        if (strButtonFace != "Save")
                        {
                            strResultMsg += "<span style='color:#DD0000;'>Your Homestay application has already been submitted. <br />You may view the information and upload more files</span>.</p>";
                            litAllRequired.Text = "";
                            PhotoLetter.Visible = true;
                            divUpload.Visible = true;
                            divSave.Visible = false;
                            divNextPage.Visible = true;
                        }
                        else
                        {
                            divNextPage.Visible = false;
                        }
                        
                    }
                }
                else { 
                    strResultMsg = "Please complete the application form below.  The application consists of multiple pages.<br />";
                    litAllRequired.Text = "";
                }
            }
            else { 
                strResultMsg = "Please complete the application below.  The application consists of multiple pages.<br />";
                litAllRequired.Text = "";
            }
        }
        if (blNoRecords)
        {
            setVisibilityNoAccess();
            if(!strResultMsg.Contains("Return")) {
                strResultMsg += "<span style=\"color:Red;font-weight:bold\">No records were returned.  You must complete the <a href=\"https://portal.ccs.spokane.edu/_netapps/internationalsa/InternationalStudents/Standardized/applicationForm.aspx?site=ccs\">International Student application</a> before applying for Homestay.</span><br />";
                strResultMsg += "<p style=\"font-size:larger; font-weight:bold;\"><a href=\"/Homestay/Default.aspx\">Return to the Homestay Homepage</a><br />";
            }
        }
        if (blDuplicateBasicInfoRecords) 
        {
            setVisibilityNoAccess();
            if (!strResultMsg.Contains("Return"))
            {
                strResultMsg += "<span style=\"color:Red;font-weight:bold\">Warning: multiple records exist with this last name, first name and date of birth.</span>  Please contact the Homestay staff to resolve this issue before continuing with the application process.<br />";
                strResultMsg += "<p style=\"font-size:larger; font-weight:bold;\"><a href=\"/Homestay/Default.aspx\">Return to the Homestay Homepage</a><br />";
            }    
        }

        litResultMsg.Text = "<p>" + hsInfo.strResultMsg + strResultMsg +  "</p>";
    }

    void setVisibilityNoAccess()
    {
        //VISIBILITY when not redirecting
        if (!blIsAdmin)
        {
            personalInformation.Visible = false;
            divMorePersonalInfo.Visible = false;
            divSearchButton.Visible = false;
            recruitingAgent.Visible = false;
            homestay18.Visible = false;
            homestay1617.Visible = false;
            PhotoLetter.Visible = false;
            divSave.Visible = false;
            divNextPage.Visible = false;
        }// end is NOT admin
        else
        {
            personalInformation.Visible = true;
            divMorePersonalInfo.Visible = true;
            divSearchButton.Visible = false;
            if (hdnAgencyID.Value == null || hdnAgencyID.Value == "")
            recruitingAgent.Visible = false;
            else
                recruitingAgent.Visible = true;
            if (blUnderAge)
            {
                homestay18.Visible = false;
                homestay1617.Visible = true;
            }
            else
            {
                homestay1617.Visible = false;
                homestay18.Visible = true;
            }
            PhotoLetter.Visible = true;
            divSave.Visible = true;
            divNextPage.Visible = true;
        }// end is Admin
    }

    void resetFields()
    {
        //Stupid viewstate...
        txtSID.Text = "";
        txtFamilyName.Text = "";
        txtFirstName.Text = "";
        txtDOB.Text = "";
        txtDateApplied.Text = "";
        txtArrivalDate.Text = "";
        rdoHomestayOption.ClearSelection();
        rdoQuarterStart.ClearSelection();
        rdoGender.ClearSelection();
        txtCountry.Text = "";
        txtPermEmail.Text = "";
        if (blIsAdmin) { rdoWhereStudy.ClearSelection(); }
        txtAgencyAddress.Text = "";
        txtAgencyCity.Text = "";
        txtAgencyCountry.Text = "";
        txtAgencyPhone.Text = "";
        txtAgencyState.Text = "";
        txtAgencyZip.Text = "";
        txtAgentEmail.Text = "";
        txtAgentName.Text = "";
        txtAgency.Text = "";
        litLetterFile.Text = "";
        litPhotoFile.Text = "";
        litResultMsg.Text = "";

        subjectID.Text = "";
        noteID.Text = "";
        actionNeededID.Text = "";
        dateCreatedID.Text = "";
        noteTypeID.Text = "";


        gridView.DataSource = null;
        gridView.DataBind();
    }
    protected void btnCmdSave_Click(object sender, EventArgs e)
    {
        blUnderAge = Convert.ToBoolean(hdnAgeLevel.Value);
        //validate the required fields
        if (!string.IsNullOrEmpty(strSID.Trim()) && blIsAdmin)
        {
            if (strSID.Length != 9)
            {
                eSID.InnerHtml = "ctcLink ID must be 9 digits long";
                txtSID.Focus();
                return;
            }
            else
            {
                eSID.InnerHtml = "";
              //  strSID = txtSID.Text.Trim();
                txtSID.Text = strSID;
            }
            int goodSIDnum;
            bool goodSID = false;
            goodSID = Int32.TryParse(strSID, out goodSIDnum);
            if (!goodSID)
            {
                eSID.InnerHtml = "ctcLink ID must contain only digits";
                txtSID.Focus();
                return;
            }
            else
            {
                eSID.InnerHtml = "";
              //  strSID = txtSID.Text.Trim();
                txtSID.Text = strSID;
            }
        }// end txtSID contains a value
        try
        {
            // Response.Write("arrival in Save TEXT value: " + txtArrivalDate.Text.Trim() + "<br />");

            if (txtArrivalDate.Text.Trim() == "")
                strArrivalDate = "01/01/1900";
            else
                strArrivalDate = txtArrivalDate.Text.Trim();

            DateTime arriveDte;
            dateSuccess = DateTime.TryParse(strArrivalDate, out arriveDte);
            if (!dateSuccess)
            {
                eArrivalDate.InnerHtml = "Please enter your arrival date in a valid date format";
                txtArrivalDate.Focus();
                validateError = true;
                return;
            }
            else
            {
                eArrivalDate.InnerHtml = "";
                validateError = false;
               // strArrivalDate = txtArrivalDate.Text.Trim().Replace("'", "''");
                txtArrivalDate.Text = strArrivalDate;
            }
            if (!blIsAdmin)
                strHomestayOption = rdoHomestayOption.SelectedValue;
            if(!blUnderAge && strHomestayOption == "")
            {
                eHomeStayOption.InnerHtml = "Please select a Homestay Option";
                rdoHomestayOption.Focus();
                validateError = true;
                return;
            }
            if(!blUnderAge)
            {
                eHomeStayOption.InnerHtml = "";
             //   strHomestayOption = rdoHomestayOption.SelectedValue;
                rdoHomestayOption.SelectedValue = strHomestayOption;
                validateError = false;
            }
        }
        finally
        {

            if (eHomeStayOption.InnerHtml != "" || eArrivalDate.InnerHtml != "")
            {
                if (blUnderAge)
                {
                    homestay1617.Visible = true;
                    homestay18.Visible = false;
                }
                else
                {
                    homestay18.Visible = true;
                    homestay1617.Visible = false;
                }
            }
        }

        //Start the updates
        if (hdnApplicantID.Value != null && hdnApplicantID.Value != "") { intApplicantID = Convert.ToInt32(hdnApplicantID.Value); }
        //1 = Update contct information for agent
        InternationalStudent myIStudent = new InternationalStudent();
        if (strCollege.Trim() != "")
        {
            //Response.Write(strCollege);
            myIStudent.UpdateSchool(intApplicantID, strCollege);
        }
        if (hdnAgencyID.Value != null && hdnAgencyID.Value != "") { intAgencyID = Convert.ToInt32(hdnAgencyID.Value); }
        if (intAgencyID == 0 && strAgentName.Trim() != "")
        {
            //Insert new agent record - address code 3
            strSubmitMsg = myIStudent.InsertContactInfo(intApplicantID, 3, strAgentName, strAgency, strAgencyAddress, strAgencyCity, strAgencyState,
                strAgencyZIP, strAgencyCountry, strAgencyPhone, strBlank, strAgentEmail, strBlank, speakEnglish);
        }
        else if (strAgentName.Trim() != "")
        {
            //Update existing agency address - address code 3
            strSubmitMsg = myIStudent.UpdateContactInfo(intAgencyID, strAgentName, strAgency, strAgencyAddress, strAgencyCity, strAgencyState,
                strAgencyZIP, strAgencyCountry, strAgencyPhone, strBlank, strAgentEmail, strBlank, speakEnglish);
        }
        if (strSubmitMsg == "OK contact info") { strResultMsg += "Your agent contact information was updated successfully!<br />"; }

        if (strHomestayOption == "")
        {
            if (blUnderAge)
            {
                //16/17 year olds forced choice
                strHomestayOption = "With food";
            }
            else
            {
                strHomestayOption = "Either";
            }
        }
        //Upload the letter and photo   
        if(strLastName == "" || strFileName == "") {
                strLastName = txtFamilyName.Text.Trim().Replace("'", "''");
                strFirstName = txtFirstName.Text.Trim().Replace("'", "''");
            }
        //Reset error message
        eLetter.InnerText = "";
        ePhoto.InnerText = "";
        if (uplLetterFile.HasFile)
        {
            if (Utility.ContainsNonEnglishChars(uplLetterFile.FileName))
            {
                //Response.Write(uplLetterFile.FileName);
                eLetter.InnerText = "You have selected a file containing invalid characters. Please rename the file with only English characters!";
                btnCmdUpload.Focus();
                return;
            }
            //Response.Write("upLetter names string: " + strLastName + ", " + strFileName + "  maybe txt: " + txtFamilyName.Text + ", " + txtFirstName + "<br />");
            strFileName = strLastName + "-" + intApplicantID + "-Letter-" + uplLetterFile.FileName;
            uplLetterFile.SaveAs(strUploadURL + strFileName);
            strResultMsg += "Your letter, <span style=\"color:red;\">" + strFileName + "</span>, was successfully uploaded!<br />";
            //Insert record in UploadedFilesInformation table
            intResult = hsInfo.InsertUploadedFilesInformation(intApplicantID, strFirstName + " " + strLastName, strFileName, strUploadURL);
            blLetterSubmitted = true;
            intResult = hsInfo.UpdateAgreementSubmittedHomestayStudentInfo(intApplicantID, 1, "letterSubmitted");
        }
        else if (hdnLetterSubmitted.Value.ToLower() == "true")
        {
            //previous submission exists
            blLetterSubmitted = true;
        }
        //strResultMsg += "Letter submitted? " + hdnLetterSubmitted.Value + "<br />";
        if (uplPhotoFile.HasFile)
        {
            if (Utility.ContainsNonEnglishChars(uplPhotoFile.FileName))
            {
                Response.Write(uplPhotoFile.FileName);
                ePhoto.InnerText = "You have selected a file containing invalid characters. Please rename the file with only English characters!";
                btnCmdUpload.Focus();
                return;
            }
            strFileName = strLastName + "-" + intApplicantID + "-Photo-" + uplPhotoFile.FileName;
            uplPhotoFile.SaveAs(strUploadURL + strFileName);
            strResultMsg += "Your photo, <span style=\"color:red;\">" + strFileName + "</span>, was successfully uploaded!<br />";
            //Insert record in UploadedFilesInformation table
            intResult = hsInfo.InsertUploadedFilesInformation(intApplicantID, strFirstName + " " + strLastName, strFileName, strUploadURL);
            blPhotoSubmitted = true;
            intResult = hsInfo.UpdateAgreementSubmittedHomestayStudentInfo(intApplicantID, 1, "photoSubmitted");
        }
        else if (hdnPhotoSubmitted.Value.ToLower() == "true")
        {
            //previous submission exists
            blPhotoSubmitted = true;
        }
        intApplicantID = Convert.ToInt32(hdnApplicantID.Value);

        //Response.Write("For Insert/Update: <br />" + strSelectedQuarter + "<br />" + strArrivalDate + "<br />" + strHomestayOption + "<br />" + strPayMethod + "<br />" + blLetterSubmitted.ToString() + "<br />" + blPhotoSubmitted.ToString() + "<br />" + intIsActive.ToString() + "<br />" + blAddMode.ToString() + "<Br />" + strCollege + "<br />" + strStudentEmail + "<br />");
        intNewIdentity = hsInfo.InsertUpdateP1HomestayStudentInfo(intApplicantID, strSelectedQuarter, strArrivalDate, strHomestayOption, strPayMethod, blLetterSubmitted, blPhotoSubmitted, intIsActive, blAddMode);
        // for Admins to change the SID, college attending, and student email
        if (blIsAdmin && intNewIdentity == -1)
        {
            myIStudent.UpdateSID(intApplicantID, strSID);
        }
        strResultMsg += "intNewIdentity: " + intNewIdentity + "<br />";
        // If not automatically taken to next page, display result messages
        if (intNewIdentity > 0)
        {
            strResultMsg += "Your student information was inserted successfully!<br />";
        }
        else if (intNewIdentity == -1)
        {
            strResultMsg += "Your student information was updated successfully!<br />";
        }
        if (blAddMode && intNewIdentity > 0)
        {
            //2 = set IsHomestay bitflag in ApplicantBasicInfo table; use applicant ID
            intResult = hsInfo.UpdateHomestayBitflagApplicantBasicInfo(intApplicantID, 1);
            //3 = Update studentStatus; use homestay ID
            intResult = hsInfo.UpdateHomestayStudentStatus(intNewIdentity, 0, studentStatus);
            //4 = Update SID, if provided
            if (hdnSID.Value != null && hdnSID.Value != "")
            {
                //international student DB; use applicant ID
                myIStudent.UpdateSID(intApplicantID, strSID);
            }
            //if (hsInfo.strResultMsg == "")
            //{
            // Go to part 2
            Response.Redirect("PlacementProfile_L.aspx?ID=" + intApplicantID);
            //}
        }
        else if (!blAddMode && intApplicantID > 0)
        {
            //2 = Update SID, if field was empty to start with
            if (strSID != "" && strSID != null && (hdnSID.Value == null || hdnSID.Value == ""))
            {
                myIStudent.UpdateSID(intApplicantID, strSID);
            }
        }
        //Display any messages
        setVisibilityNoAccess();
        divNextPage.Visible = true;
    }// end btnSave_Click

    #region Export PDF
    protected void btnExport2_Click(object sender, EventArgs e)
    {
        ExportPdfUtility pdfUtility = new ExportPdfUtility();
        try
        {
            Document pdfDoc = new Document(PageSize.A4, 25, 10, 25, 10);
            PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            BaseFont bf = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
            Font font1 = new Font(bf, 16, Font.NORMAL);
            Font font3 = new Font(bf, 15, Font.BOLD);
            Paragraph Text = new Paragraph("Application Form - Students", font1);
            pdfDoc.Add(Text);
            string familyNameStr = "";
            string firstNameStr = "";
            DataTable dtStudent = hsInfo.GetOneInternationalStudent("", intApplicantID, "", "", "", "");
            if (dtStudent != null && dtStudent.Rows.Count > 0)
            {
                DataRow drStudent = dtStudent.Rows[0];
                familyNameStr = drStudent["familyName"].ToString();
                firstNameStr = drStudent["firstName"].ToString();
                pdfDoc.Add(pdfUtility.GetPhrase("Student: ", String.Format("{0}, {1}     {2}",
                                                                            familyNameStr
                                                                            ,firstNameStr
                                                                            ,drStudent["Country"].ToString()
                                                                          ),false));
                //Get arrival date and options
                DataTable dtHSStudent = hsInfo.GetOneHomestayStudent(intApplicantID);
                string anythingElse = "";
                string optionStr = "";
                DataRow drHSStudent = dtHSStudent.Rows[0];
                if (dtHSStudent != null && dtHSStudent.Rows.Count > 0)
                {
                    //DataRow drHSStudent = dtHSStudent.Rows[0];
                    pdfDoc.Add(pdfUtility.GetPhrase("Application date: ", Convert.ToDateTime(drHSStudent["dateApplied"]).ToShortDateString(), false));

                    //Quarter start date
                    DataTable dtQ = QMD.GetQuarters();
                    string strQuarterStartDate = "n/a";
                    foreach (DataRow drQ in dtQ.Rows)
                    {
                        string strStartDate = hsUtility.GetShortDateString(drQ["StartDate"].ToString());
                        string strEndDate = hsUtility.GetShortDateString(drQ["EndDate"].ToString());
                        if (!strStartDate.Equals("n/a") && !strEndDate.Equals("n/a") && !strArrivalDate.Equals("n/a"))
                        {
                            DateTime dtArrivalDate = Convert.ToDateTime(strArrivalDate);
                            DateTime dtStartDate = Convert.ToDateTime(strStartDate);
                            DateTime dtEndDate = Convert.ToDateTime(strEndDate);
                            if (DateTime.Compare(dtStartDate, dtArrivalDate) <= 0 && DateTime.Compare(dtEndDate, dtArrivalDate) >= 0)
                            {
                                string quarterName = drQ["Name"].ToString();
                                string yearS = Convert.ToDateTime(drQ["FirstDayOfClass"].ToString()).Year.ToString();
                                strQuarterStartDate = quarterName + " " + yearS;
                            }
                        }
                        
                        
                    }
                    pdfDoc.Add(pdfUtility.GetPhrase("Quarter start date: ", strQuarterStartDate, true));

                    anythingElse = drHSStudent["quarterStartDate"].ToString();
                    //Add homestay options
                    if (blUnderAge)
                    {
                        optionStr = "Full-minor.";
                    }
                    else
                    {
                        if (drHSStudent["homestayOption"].ToString().Trim().Equals("With food"))
                        {
                            optionStr = "Full";
                        }
                        else if (drHSStudent["homestayOption"].ToString().Trim().Equals("Without food"))
                        {
                            optionStr = "Shared";
                        }
                        else
                        {
                            optionStr = "Either";
                        }
                    }

                    
                }
                pdfDoc.Add(pdfUtility.GetPhrase("College attend: ", drStudent["studyWhere"].ToString(), false));
                pdfDoc.Add(pdfUtility.GetPhrase("CTC: ", drStudent["SID"].ToString(), false));
                pdfDoc.Add(pdfUtility.GetPhrase("Birthdate: ", Convert.ToDateTime(drStudent["DOB"].ToString()).ToShortDateString(), false));
                string genderStr = drStudent["Gender"].ToString();
                if (genderStr.Equals("M"))
                {
                    genderStr = "Male";
                }
                else if(genderStr.Equals("F"))
                {
                    genderStr = "Female";
                }
                pdfDoc.Add(pdfUtility.GetPhrase("Gender: ", genderStr, true));

                pdfDoc.Add(pdfUtility.GetPhrase("Student email: ", drStudent["Email"].ToString(), true));
                //pdfDoc.Add(pdfUtility.GetPhrase("Notes: ", anythingElse,true));
               
                //Notes
                string notes = "";
                pdfDoc.Add(pdfUtility.GetPhrase("Notes: ", "", true));
                DataTable dtNotes = hsInfo.GetHomestayNotesExtended("Student", "" + applicantID, "0", "0", "0", "", "", "");
                if (dtNotes != null && dtNotes.Rows.Count > 0)
                {
                    
                    foreach (DataRow drNotes in dtNotes.Rows)
                    {
                        string detailNote = String.Format("{0})"
                                                    + " Note: {1},"
                                                    + " Date Created: {2},"
                                                    + " Family: {3},"
                                                    + " Action Needed: {4},"
                                                    + " Action Done: {5}"
                                                    , dtNotes.Rows.IndexOf(drNotes) + 1
                                                    , drNotes["Note"].ToString()
                                                    , Convert.ToDateTime(drNotes["createDate"].ToString().Trim()).ToShortDateString()
                                                    , drNotes["homeName"].ToString()
                                                    , drNotes["actionNeeded"].ToString()
                                                    , drNotes["actionCompleted"].ToString()
                                                    );
                        pdfDoc.Add(pdfUtility.GetSpace());
                        pdfDoc.Add(pdfUtility.GetPhrase("", detailNote, true));
                        notes += detailNote;

                    }
                    
                }
                
                //Add agent
                blUseAgency = Convert.ToBoolean(dtStudent.Rows[0]["useAgency"]);
                if (blUseAgency)
                {
                    DataTable dtContact = hsInfo.GetOneContact(3, intApplicantID);
                    if (dtContact != null && dtContact.Rows.Count > 0)
                    {
                       
                        string agentInfo = String.Format    
                                                ( "{0}   {1}   {2}   {3}"
                                                ,dtContact.Rows[0]["agencyName"].ToString()
                                                , dtContact.Rows[0]["addresseeName"].ToString()
                                                , dtContact.Rows[0]["Email"].ToString()
                                                , dtContact.Rows[0]["Phone"].ToString()
                                                );
                        pdfDoc.Add(pdfUtility.GetPhrase("Agent: ", agentInfo, true));
                        
                    }
                }
                //End add agent

                //Add Photo and Letter

                pdfDoc.Add(pdfUtility.GetPhrase("Photo and Letter: ", "", true));
                string letterNames = "";
                string photoNames = "";
                
                if (blLetterSubmitted || blPhotoSubmitted)
                {
                    DataTable dt = hsInfo.GetUploadedFiles(intApplicantID);
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        
                        foreach (DataRow dr in dt.Rows)
                        {
                            if (dr["fileName"].ToString().Contains("Letter"))
                            {
                                letterNames += dr["fileName"].ToString() + ", ";
                            }
                            else if (dr["fileName"].ToString().Contains("Photo"))
                            {
                                photoNames += dr["fileName"].ToString() + ", ";
                            }
                        }
                        
                    }

                    if (!string.IsNullOrEmpty(letterNames))
                    {
                      
                        pdfDoc.Add(pdfUtility.GetSpace());
                        pdfDoc.Add(pdfUtility.GetPhrase("Letter: ", letterNames, true));
                    }
                    if (!string.IsNullOrEmpty(photoNames))
                    {
                        pdfDoc.Add(pdfUtility.GetSpace());
                        pdfDoc.Add(pdfUtility.GetPhrase("Photo: ", photoNames, true));
                    }
                }

                //End Photo and Letter
                //Signature boxes check?
                string signatureAllCheck = "";
                //UA
                if (!Convert.ToBoolean(drHSStudent["agreeWaiveLiabilityParentUA"]))
                {
                    signatureAllCheck += " ,WaiveLiabilityParentUA";
                }
                if (!Convert.ToBoolean(drHSStudent["agreeCCSConductUA"]))
                {
                    signatureAllCheck += " ,CCSConductUA";
                }
                if (!Convert.ToBoolean(drHSStudent["agreeNoticesWaiverParentUA"]))
                {
                    signatureAllCheck += " ,NoticesWaiverParentUA";
                }
                if (string.IsNullOrEmpty(signatureAllCheck))
                {
                    signatureAllCheck = "Yes";
                }
                else
                {
                    signatureAllCheck = "Incomplete " + signatureAllCheck;
                }
                
               
                //18+
                if (Convert.ToBoolean(drHSStudent["agreeCCSConduct18"]))
                {
                    signatureAllCheck = "Yes";
                }
                else
                {
                    signatureAllCheck = "Imcomplete: CCSConduct18";
                }

               

                pdfDoc.Add(pdfUtility.GetPhrase("Signature boxes check?: ", signatureAllCheck, true));
                //Homestay Option
                pdfDoc.Add(pdfUtility.GetPhrase("Homestay Option: ", optionStr, true));

                //Home Environment
                pdfDoc.Add(pdfUtility.GetPhrase("Home Environment: ", "", true));
                

                Dictionary<string, string> hm = new Dictionary<string, string>();
                DataTable dtPref = hsInfo.GetHomestayPreferenceSelections(intApplicantID, 0);
                if (dtPref != null && dtPref.Rows.Count > 0)
                {
                    foreach (DataRow dtFamPrefRow in dtPref.Rows)
                    {
                        hm.Add(dtFamPrefRow["fieldID"].ToString().Trim(), dtFamPrefRow["fieldValue"].ToString().Trim());
                    }
                }
                   

                if (dtPref != null && dtPref.Rows.Count > 0)
                {
                    //rdoSmallChildren
                    pdfDoc.Add(pdfUtility.GetSpace());
                    pdfDoc.Add(pdfUtility.GetPhrase("Children in home: ", pdfUtility.GetPreferenceSelection(hm, "rdoSmallChildren"), true));

                    //rdoSmoker
                    pdfDoc.Add(pdfUtility.GetSpace());
                    pdfDoc.Add(pdfUtility.GetPhrase("Smoking: ", pdfUtility.GetPreferenceSelection(hm, "rdoSmoker"), true));

                    //rdoTraveledOutsideCountry
                    pdfDoc.Add(pdfUtility.GetSpace());
                    pdfDoc.Add(pdfUtility.GetPhrase("Traveled out of country: ", pdfUtility.GetPreferenceSelection(hm, "rdoTraveledOutsideCountry"), true));

                    //rdoInteraction
                    pdfDoc.Add(pdfUtility.GetSpace());
                    pdfDoc.Add(pdfUtility.GetPhrase("Interaction: ", pdfUtility.GetPreferenceSelection(hm, "rdoInteraction"), true));

                    //rdoHomeEnvironment
                    pdfDoc.Add(pdfUtility.GetSpace());
                    pdfDoc.Add(pdfUtility.GetPhrase("Home Environement: ", pdfUtility.GetPreferenceSelection(hm, "rdoHomeEnvironment")
                                                    + " " + dtHSStudent.Rows[0]["homeEnvironmentPreferences"].ToString(), true));

                    
                    pdfDoc.Add(pdfUtility.GetSpace());
                    pdfDoc.Add(pdfUtility.GetPhrase("Diet: ", pdfUtility.GetContent(hm, "diets"), true));

                    pdfDoc.Add(pdfUtility.GetSpace());
                    pdfDoc.Add(pdfUtility.GetPhrase("Hobbies: ", pdfUtility.GetContent(hm, "hobbies"), true));
                }

                //Allergies or Health Conditions: 
                pdfDoc.Add(pdfUtility.GetSpace());
                pdfDoc.Add(pdfUtility.GetPhrase("Allergies or Health Conditions: ", drHSStudent["allergies"].ToString() + " " + drHSStudent["healthConditions"].ToString(), true));
                pdfDoc.Add(pdfUtility.GetSpace());
                pdfDoc.Add(pdfUtility.GetPhrase("", drHSStudent["healthStatus"].ToString(), true));
                //Car: 
                string driveCar = (drHSStudent["driveCar"].ToString().Equals("True")) ? "Yes" : "No";
                pdfDoc.Add(pdfUtility.GetSpace());
                pdfDoc.Add(pdfUtility.GetPhrase("Car: ", driveCar, true));
                //Comments: 
                pdfDoc.Add(pdfUtility.GetSpace());
                pdfDoc.Add(pdfUtility.GetPhrase("Comments: ", drHSStudent["anythingElse"].ToString(), true));

                //Family
                pdfDoc.Add(pdfUtility.GetPhrase("Family: ", "", true));
                
               

                DataTable dtRelatives = hsInfo.GetHomestayRelatives(intApplicantID, 0);
                if (dtRelatives != null && dtRelatives.Rows.Count > 0)
                {
                    foreach (DataRow drRelatvies in dtRelatives.Rows)
                    {
                        string relationship = drRelatvies["relationship"].ToString();
                        if (relationship.Equals("Father") || relationship.Equals("Mother"))
                        {
                            
                            string info = String.Format("Name: {0}, {1}, Occupation: {2}"
                                                , drRelatvies["familyName"].ToString()
                                                , drRelatvies["firstName"].ToString()
                                                , drRelatvies["occupation"].ToString());
                            pdfDoc.Add(pdfUtility.GetSpace());
                            pdfDoc.Add(pdfUtility.GetPhrase("Guardian: ", info, true));
                        }
                        else
                        {
                            
                            string info = String.Format("Age: {0}, Gender: {1}"
                                                , drRelatvies["age"].ToString()
                                                , drRelatvies["gender"].ToString());
                            pdfDoc.Add(pdfUtility.GetSpace());
                            pdfDoc.Add(pdfUtility.GetPhrase("Sibling: ", info, true));
                        }
                    }
                }
                
            }

            string fileNameExport = familyNameStr + "-" + firstNameStr + "-" + "Application-Form-Students";
            pdfWriter.CloseStream = false;
            pdfDoc.Close();
            Response.Buffer = true;
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=" + fileNameExport + ".pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Write(pdfDoc);
            Response.End();
        }
        catch (Exception ex)
        {
            Response.Write("error" + ex.Message);
        }
    }
    #endregion

    #region Export PDF
    protected void btnExport_Click(object sender, EventArgs e)
    {
        try
        {
            Document pdfDoc = new Document(PageSize.A4, 25, 10, 25, 10);
            PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            BaseFont bf = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
            //Font font1 = new Font(bf, 16, Font.NORMAL, Color.BLUE);
            Font font3 = new Font(bf, 15, Font.BOLD);
            Paragraph Text = new Paragraph("Application Form -Students", font3);
            pdfDoc.Add(Text);
            string familyNameStr = "";
            string firstNameStr = "";
            DataTable dtStudent = hsInfo.GetOneInternationalStudent("", intApplicantID, "", "", "", "");
            if (dtStudent != null)
            {
                //strCollege = dtStudent.Rows[0]["studyWhere"].ToString();
                if (dtStudent.Rows.Count > 0)
                {
                    foreach (DataRow row in dtStudent.Rows)
                    {
                        familyNameStr = row["familyName"].ToString();
                        Text = new Paragraph("Family name: " + row["familyName"].ToString());
                        pdfDoc.Add(Text);

                        firstNameStr = row["firstName"].ToString();
                        Text = new Paragraph("First Name: " + row["firstName"].ToString());
                        pdfDoc.Add(Text);

                        Text = new Paragraph("College student plans to attend: " + row["studyWhere"].ToString());
                        pdfDoc.Add(Text);

                        Text = new Paragraph("Date of Birth: " + Convert.ToDateTime(row["DOB"].ToString()).ToShortDateString() );
                        pdfDoc.Add(Text);

                        Text = new Paragraph("Gender: " + row["Gender"].ToString());
                        pdfDoc.Add(Text);

                        Text = new Paragraph("Home Country: " + row["Country"].ToString());
                        pdfDoc.Add(Text);

                        Text = new Paragraph("Student Email: " + row["Email"].ToString());
                        pdfDoc.Add(Text);
                        
                        //Get arrival date and options
                        DataTable dtHSStudent = hsInfo.GetOneHomestayStudent(intApplicantID);
                        if (dtHSStudent != null)
                        {
                            if (dtHSStudent.Rows.Count > 0)
                            {
                                foreach (DataRow drHSStudent in dtHSStudent.Rows)
                                {
                                    Text = new Paragraph("Application Date: "
                                        + Convert.ToDateTime(drHSStudent["dateApplied"]).ToShortDateString());
                                    pdfDoc.Add(Text);
                                    
                                    Text = new Paragraph("Estimated date of arrival: "
                                        + Convert.ToDateTime(drHSStudent["arrivalDate"]).ToShortDateString());
                                    pdfDoc.Add(Text);

                                    //Add homestay options
                                    string optionStr = "";
                                    if (blUnderAge)
                                    {
                                        optionStr = "Full Homestay is $850 per month, including meals.";
                                    }
                                    else
                                    {
                                        if (drHSStudent["homestayOption"].ToString().Trim().Equals("With food"))
                                        {
                                            optionStr = "$700.00 per month for Homestay with food ";
                                        }
                                        else if(drHSStudent["homestayOption"].ToString().Trim().Equals("Without food"))
                                        {
                                            optionStr = "$400.00 per month for Homestay without food";
                                        }
                                        else
                                        {
                                            optionStr = "Either";
                                        }
                                    }
                                    
                                    Text = new Paragraph("Homestay Options", font3);
                                    pdfDoc.Add(Text);
                                    
                                    Text = new Paragraph(optionStr);
                                    pdfDoc.Add(Text);
                                    //End homestay options

                                }
                            }
                        }
                        
                        //Add agent
                        blUseAgency = Convert.ToBoolean(dtStudent.Rows[0]["useAgency"]);
                        if (blUseAgency)
                        {
                            DataTable dtContact = hsInfo.GetOneContact(3, intApplicantID);
                            if (dtContact != null)
                            {
                                if (dtContact.Rows.Count > 0)
                                {
                                    Text = new Paragraph("Recruiting Agent",font3);
                                    pdfDoc.Add(Text);
                                    Text = new Paragraph("Agency: " + dtContact.Rows[0]["agencyName"].ToString());
                                    pdfDoc.Add(Text);
                                    Text = new Paragraph("Agent Name: " + dtContact.Rows[0]["addresseeName"].ToString());
                                    pdfDoc.Add(Text);
                                    Text = new Paragraph("Agent Email: " + dtContact.Rows[0]["Email"].ToString());
                                    pdfDoc.Add(Text);
                                    Text = new Paragraph("Agency Street Address: " + dtContact.Rows[0]["Addr"].ToString());
                                    pdfDoc.Add(Text);
                                    Text = new Paragraph("Agency City: " + dtContact.Rows[0]["City"].ToString());
                                    pdfDoc.Add(Text);
                                    Text = new Paragraph("Agency State/Province: " + dtContact.Rows[0]["StateProv"].ToString());
                                    pdfDoc.Add(Text);
                                    Text = new Paragraph("Agency Zip/Postal Code: " + dtContact.Rows[0]["Zip"].ToString());
                                    pdfDoc.Add(Text);
                                    Text = new Paragraph("Agency Country: " + dtContact.Rows[0]["Country"].ToString());
                                    pdfDoc.Add(Text);
                                    Text = new Paragraph("Agency Phone: " + dtContact.Rows[0]["Phone"].ToString());
                                    pdfDoc.Add(Text);
                                }
                            }
                        }
                        //End add agent

                        //Add Photo and Letter
                        Text = new Paragraph("Photo and Letter", font3);
                        pdfDoc.Add(Text);
                        string letterNames = "";
                        string photoNames = "";
                        if (blLetterSubmitted || blPhotoSubmitted)
                        {
                            DataTable dt = hsInfo.GetUploadedFiles(intApplicantID);
                            if (dt != null)
                            {
                                if (dt.Rows.Count > 0)
                                {
                                    foreach (DataRow dr in dt.Rows)
                                    {
                                        if (dr["fileName"].ToString().Contains("Letter"))
                                        {
                                            letterNames += dr["fileName"].ToString() + "\n";
                                        }
                                        else if (dr["fileName"].ToString().Contains("Photo"))
                                        {
                                            photoNames += dr["fileName"].ToString() + "\n";
                                        }
                                    }
                                }
                            }

                            if (!string.IsNullOrEmpty(letterNames))
                            {
                                Text = new Paragraph("Letter", font3);
                                pdfDoc.Add(Text);
                                Text = new Paragraph(letterNames);
                                pdfDoc.Add(Text);
                            }
                            if (!string.IsNullOrEmpty(photoNames))
                            {
                                Text = new Paragraph("Photo", font3);
                                pdfDoc.Add(Text);
                                Text = new Paragraph(photoNames);
                                pdfDoc.Add(Text);
                            }
                        }
                            
                        //End Photo and Letter

                    }

                }
            }

            string fileNameExport = familyNameStr + "-" + firstNameStr + "-" + "Application-Form-Students";
            pdfWriter.CloseStream = false;
            pdfDoc.Close();
            Response.Buffer = true;
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=" + fileNameExport + ".pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Write(pdfDoc);
            Response.End();
        }
        catch (Exception ex)
        {
            Response.Write("error" + ex.Message);
        }
    }
        #endregion
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        litErr.Text = "";
        //Get records by student search
        //validate the required fields
        if (txtFamilyName.Text.Trim() == "")
        {
            eFamName.InnerText = "Please enter your family name";
            txtFamilyName.Focus();
            return;
        }
        else
        {
            eFamName.InnerText = "";
            strLastName = txtFamilyName.Text.Trim().Replace("'", "''");
        }
        if (txtFirstName.Text.Trim() == "")
        {
            eFirstName.InnerText = "Please enter your first name.";
            txtFirstName.Focus();
            return;
        }
        else
        {
            eFirstName.InnerText = "";
            strFirstName = txtFirstName.Text.Trim().Replace("'", "''");
        }
        // test if valid date
        dateSuccess = DateTime.TryParse(txtDOB.Text.Trim(), out outDate);
        if (!dateSuccess)
        {
            eDOB.InnerText = "Please enter a valid date of birth";
            txtDOB.Focus();
            return;
        }
        else
        {
            eDOB.InnerText = "";
            strDOB = outDate.ToShortDateString();
        }

        if (rdoQuarterStart.SelectedIndex == -1)
        {
            eQtrStart.InnerText = "Please select a quarter start date.";
            rdoQuarterStart.Focus();
            return;
        }
        else
        {
            strQuarterStartDate = rdoQuarterStart.SelectedValue;
            eQtrStart.InnerText = "";
        }
        
        //Do some data calculation to determine age       
        if (strDOB != "" && dateSuccess)
        {
            dtDOB = Convert.ToDateTime(strDOB);
            if (strQuarterStartDate != "")
            {
                dtFirstDayOfClass = Convert.ToDateTime(strQuarterStartDate);
            }
            else
            {
                strQuarterStartDate = QMD.NextQtrFirstClass.ToShortDateString();
                dtFirstDayOfClass = QMD.NextQtrFirstClass;
            }
            dtLatestDOB = dtFirstDayOfClass.AddYears(-18);//calculate dtLatestDOB from selected or by default
            //Initial set of mode: true at this point
            hdnAddMode.Value = blAddMode.ToString();
            
            //Find the student and populate the info
            DataTable dtInfo = hsInfo.GetOneInternationalStudent("DOB", 0, strDOB, strFirstName, strLastName, "");
            if (dtInfo != null)
            {
                blNoRecords = false;
                if (dtInfo.Rows.Count > 0)
                {
                   
                    if (dtInfo.Rows.Count > 1) //check if duplicate record
                    {
                        blDuplicateBasicInfoRecords = true;
                    }
                    selectedStudent = dtInfo.Rows[0]["id"].ToString();
                    intApplicantID = Convert.ToInt32(dtInfo.Rows[0]["id"]);
                    hdnApplicantID.Value = intApplicantID.ToString();
                    strGender = dtInfo.Rows[0]["Gender"].ToString();
                    strCountry = dtInfo.Rows[0]["Country"].ToString();
                    strStudentEmail = dtInfo.Rows[0]["Email"].ToString();
                    blUseAgency = Convert.ToBoolean(dtInfo.Rows[0]["useAgency"]);
                    //Check with Homestay if they want this behavior
                    if (blUseAgency)
                    {
                        recruitingAgent.Visible = true;
                        DataTable dtContact = hsInfo.GetOneContact(3, intApplicantID);
                        if (dtContact != null)
                        {
                            if (dtContact.Rows.Count > 0)
                            {
                                strAgency = dtContact.Rows[0]["agencyName"].ToString();
                                txtAgency.Text = strAgency;
                                strAgentName = dtContact.Rows[0]["addresseeName"].ToString();
                                txtAgentName.Text = strAgentName;
                                strAgentEmail = dtContact.Rows[0]["Email"].ToString();
                                txtAgentEmail.Text = strAgentEmail;
                                strAgencyAddress = dtContact.Rows[0]["Addr"].ToString();
                                txtAgencyAddress.Text = strAgencyAddress;
                                strAgencyCity = dtContact.Rows[0]["City"].ToString();
                                txtAgencyCity.Text = strAgencyCity;
                                strAgencyState = dtContact.Rows[0]["StateProv"].ToString();
                                txtAgencyState.Text = strAgencyState;
                                strAgencyZIP = dtContact.Rows[0]["Zip"].ToString();
                                txtAgencyZip.Text = strAgencyZIP;
                                strAgencyCountry = dtContact.Rows[0]["Country"].ToString();
                                txtAgencyCountry.Text = strAgencyCountry;
                                strAgencyPhone = dtContact.Rows[0]["Phone"].ToString();
                                txtAgencyPhone.Text = strAgencyPhone;
                                hdnAgencyID.Value = dtContact.Rows[0]["id"].ToString();
                            }
                        }
                    }
                    else
                    {
                        recruitingAgent.Visible = false;
                    }
                    /*End checking show homestay recruiting agent*/
                    strCollege = dtInfo.Rows[0]["studyWhere"].ToString();
                    if (strCollege == "Pullman") { strCollege = "SFCC"; }
                    if (blIsAdmin)
                    {
                        rdoWhereStudy.SelectedValue = strCollege;
                    }
                    rdoGender.SelectedValue = strGender;
                    txtCountry.Text = strCountry;
                    txtPermEmail.Text = strStudentEmail;
                }
                else
                {
                    blNoRecords = true;
                }
            }//end check if dtInfo != null
            else
            {
                blNoRecords = true;
            }
        }

        if (blNoRecords)
        {
            //no student application records found
            strResultMsg = "<span style=\"color:Red;font-weight:bold\">No records were returned.  You must complete the <a href=\"https://portal.ccs.spokane.edu/_netapps/internationalsa/InternationalStudents/Standardized/applicationForm.aspx?site=ccs\" target='_blank'>International Student application</a> before applying for Homestay.</span><br />";
            strResultMsg += "<p >If you have previously submitted an International Student application, please correct either the family name, first name or date-of-birth in the form below or ";
            strResultMsg += "<a href=\"/Homestay/Default.aspx\">Return to the Homestay Homepage</a><p>";
            litErr.Text = strResultMsg;
            divErr.Visible = true;
            return;
        }
        else
        {
            if(blDuplicateBasicInfoRecords)
            {
                strResultMsg += "<span style=\"color:Red;font-weight:bold\">Warning: multiple records exist with this last name, first name and date of birth.</span> <br />We are using the most recent submission for the following information.<br />If this is not the information you want used, please contact the Homestay staff to resolve this issue.<br />";
                litDuplicateRecords.Text = strResultMsg;
            }
            // student records returned so populate fields and show
            divMorePersonalInfo.Visible = true;
           
            PhotoLetter.Visible = true;
            divSave.Visible = true;
            divSearchButton.Visible = false;
            litAllRequired.Visible = false;

            //Check for existing HomestayStudentInfo record
            DataTable dtHSStudent = hsInfo.GetOneHomestayStudent(intApplicantID);
            if (dtHSStudent != null)
            {
                if (dtHSStudent.Rows.Count > 0)
                {
                    //if  record is found in HomeStayStudentInfo, used quarter start date from db, instead of using selected.
                    dtFirstDayOfClass = Convert.ToDateTime(dtHSStudent.Rows[0]["quarterStartDate"]);
                    dtLatestDOB = dtFirstDayOfClass.AddYears(-18); //recalculate dtLatestDOB from db

                    blAddMode = false;
                    hdnAddMode.Value = blAddMode.ToString();
                    //strResultMsg += "blAddMode 1? " + blAddMode + "<br />";
                    Configuration config = WebConfigurationManager.OpenWebConfiguration(Request.ApplicationPath);
                    AppSettingsSection appSettings = (AppSettingsSection)config.GetSection("appSettings");
                    strUploadServer = "/InternetContent" + appSettings.Settings["HomestayShare"].Value;
                    foreach (DataRow drHSStudent in dtHSStudent.Rows)
                    {
                        blLetterSubmitted = Convert.ToBoolean(drHSStudent["letterSubmitted"]);
                        hdnLetterSubmitted.Value = blLetterSubmitted.ToString();
                        blPhotoSubmitted = Convert.ToBoolean(drHSStudent["photoSubmitted"]);
                        hdnPhotoSubmitted.Value = blPhotoSubmitted.ToString();
                        if (blLetterSubmitted || blPhotoSubmitted)
                        {
                            litLetterFile.Text = "";
                            litPhotoFile.Text = "";
                            DataTable dt = hsInfo.GetUploadedFiles(intApplicantID);
                            if (dt != null)
                            {
                                if (dt.Rows.Count > 0)
                                {
                                    foreach (DataRow dr in dt.Rows)
                                    {
                                        strFileName = dr["fileName"].ToString();
                                        string filePath = dr["filePath"].ToString();
                                        string uploadServer = "";
                                        if (filePath.Contains("ccs-internet"))
                                        {
                                            uploadServer += @"http:\\apps.spokane.edu\InternetContent\Homestay\";
                                        }
                                        else
                                        {
                                            uploadServer += @"http:\\portal.ccs.spokane.edu\InternetContent\Homestay\";
                                        }
                                        if (strFileName.Contains("Letter"))
                                        {
                                            //get the file name
                                            litLetterFile.Text += "<br /><b>Letter submitted:</b> " + "<a href=\"" + strUploadServer + strFileName + "\" target=\"_blank\">" + strFileName + "</a>";
                                        }
                                        if (strFileName.Contains("Photo"))
                                        {
                                            //Get the file name
                                            litPhotoFile.Text += "<br /><b>Photo submitted:</b> " + "<a href=\"" + strUploadServer + strFileName + "\" target=\"_blank\">" + strFileName + "</a>";
                                        }
                                    }
                                }
                            }
                        }
                        //strResultMsg += "Letter submitted: " + Convert.ToBoolean(drHSStudent["letterSubmitted"]) + "<br />";
                        //strResultMsg += "Start Date: " + Convert.ToDateTime(drHSStudent["quarterStartDate"]).ToShortDateString() + "<br />";
                        if (Convert.ToDateTime(drHSStudent["quarterStartDate"]).ToShortDateString() != "1/1/1900")
                        {
                            try
                            {
                                rdoQuarterStart.SelectedValue = Convert.ToDateTime(drHSStudent["quarterStartDate"]).ToShortDateString();
                            }
                            catch
                            {
                                //do nothing
                            }
                        }
                        txtArrivalDate.Text = Convert.ToDateTime(drHSStudent["arrivalDate"]).ToShortDateString();
                        if (blIsAdmin)
                        {
                            txtDateApplied.Text = Convert.ToDateTime(drHSStudent["dateApplied"]).ToShortDateString();
                        }
                        if (!blUnderAge)
                        {
                            rdoHomestayOption.SelectedValue = drHSStudent["homestayOption"].ToString().Trim();
                        }
                    }
                    if (blIsAdmin)
                    {
                        strResultMsg += "A Homestay student application exists.  You may review the information here.<br />";
                    }
                    else
                    {
                        //setVisibilityNoAccess();
                        if (strButtonFace != "Save")
                        {
                            strResultMsg += "<span style='color:#DD0000;'>Your Homestay application has already been submitted. <br />You may view the information and upload more files</span>.</p>";
                            litAllRequired.Text = "";
                            PhotoLetter.Visible = true;
                            divUpload.Visible = true;
                            divSave.Visible = false;
                            divNextPage.Visible = true;
                        }
                        else
                        {
                            divNextPage.Visible = false;
                        }
                    }
                }
                else
                {
                    strResultMsg = "Please complete the application form below.  The application consists of multiple pages.<br />";
                    litAllRequired.Text = "";
                }
                
                if (dtLatestDOB < dtDOB)
                {
                    blUnderAge = true;
                    hdnAgeLevel.Value = "true";
                    //divNextPage.Visible = true;
                }
                else
                {
                    blUnderAge = false;
                    hdnAgeLevel.Value = "false";
                }

                if (blUnderAge)
                {
                    homestay1617.Visible = true;
                    homestay18.Visible = false;
                }
                else
                {
                    homestay18.Visible = true;
                    homestay1617.Visible = false;
                }
            } //end if dtHSStudent != null
            else
            {
                strResultMsg = "Please complete the application below.  The application consists of multiple pages.<br />";
                litAllRequired.Text = "";
            }
            litResultMsg.Text = "<p>" + hsInfo.strResultMsg + strResultMsg + "</p>";
        }//end if blNoRecord
    }// end btnSearch_Click
    protected void btnCmdContinue_Click(object sender, EventArgs e)
    {
        if(blUnderAge)
            Response.Redirect("PlacementProfile_L.aspx?ID=" + hdnApplicantID.Value + "&UA=t");
        else
            Response.Redirect("PlacementProfile_L.aspx?ID=" + hdnApplicantID.Value);
    }
    protected void btnCmdReturnAdminHome_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Homestay/Admin");
    }

    protected void btnCmdResetFields_Click(object sender, EventArgs e)
    {
        resetFields();
        ddlStudents.SelectedValue = "0";
        //return;
        Response.Redirect("ApplicationForm_L.aspx");
    }
    protected void btnCmdUpload_Click(object sender, EventArgs e)
    {
        //Reset error message
        eLetter.InnerText = "";
        ePhoto.InnerText = "";
        Response.Write("here" + uplLetterFile.FileName +"another");
        //Upload the letter and photo        
        if (uplLetterFile.HasFile)
        {
            if (Utility.ContainsNonEnglishChars(uplLetterFile.FileName))
            {
                Response.Write(uplLetterFile.FileName);
                eLetter.InnerText = "You have selected a file containing invalid characters. Please rename the file with only English characters!";
                btnCmdUpload.Focus();
                return;
            }
            strFileName = txtFamilyName.Text.Trim().Replace("'", "''") + "-" + intApplicantID + "-Letter-" + uplLetterFile.FileName;
            uplLetterFile.SaveAs(strUploadURL + strFileName);
            strResultMsg += "Your letter, <span style=\"color:red;\">" + strFileName + "</span>, was successfully uploaded!<br />";
            //Insert record in UploadedFilesInformation table
            intResult = hsInfo.InsertUploadedFilesInformation(intApplicantID, txtFirstName.Text.Trim().Replace("'", "''") + " " + txtFamilyName.Text.Trim().Replace("'", "''"), strFileName, strUploadURL);
            blLetterSubmitted = true;
            hdnLetterSubmitted.Value = blLetterSubmitted.ToString();
            //Insert bit value into HomestayStudentInfo table
            intResult = hsInfo.UpdateAgreementSubmittedHomestayStudentInfo(intApplicantID, 1, "letterSubmitted");
        }
        else if (hdnLetterSubmitted.Value.ToLower() == "true")
        {
            //previous submission exists
            blLetterSubmitted = true;
        }
        //strResultMsg += "Letter submitted? " + hdnLetterSubmitted.Value + "<br />";
        if (uplPhotoFile.HasFile)
        {
            if (Utility.ContainsNonEnglishChars(uplPhotoFile.FileName))
            {
                Response.Write(uplPhotoFile.FileName);
                ePhoto.InnerText = "You have selected a file containing invalid characters. Please rename the file with only English characters!";
                btnCmdUpload.Focus();
                return;
            }

            strFileName = txtFamilyName.Text.Trim().Replace("'", "''") + "-" + intApplicantID + "-Photo-" + uplPhotoFile.FileName;
            uplPhotoFile.SaveAs(strUploadURL + strFileName);
            strResultMsg += "Your photo, <span style=\"color:red;\">" + strFileName + "</span>, was successfully uploaded!<br />";
            //Insert record in UploadedFilesInformation table
            intResult = hsInfo.InsertUploadedFilesInformation(intApplicantID, txtFirstName.Text.Trim().Replace("'", "''") + " " + txtFamilyName.Text.Trim().Replace("'", "''"), strFileName, strUploadURL);
            blPhotoSubmitted = true;
            hdnPhotoSubmitted.Value = blPhotoSubmitted.ToString();
            //Insert bit value into HomestayStudentInfo table
            intResult = hsInfo.UpdateAgreementSubmittedHomestayStudentInfo(intApplicantID, 1, "photoSubmitted");
        }
        else if (hdnPhotoSubmitted.Value.ToLower() == "true")
        {
            //previous submission exists
            blPhotoSubmitted = true;
        }
        litLetterFile.Text = "";
        litPhotoFile.Text = "";
        DataTable dt = hsInfo.GetUploadedFiles(intApplicantID);
        if (dt != null)
        {
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    strFileName = dr["fileName"].ToString();
                    if (strFileName.Contains("Letter"))
                    {
                        //get the file name
                        litLetterFile.Text += "<br /><b>Letter submitted:</b> " + "<a href=\"" + strUploadServer + strFileName + "\" target=\"_blank\">" + strFileName + "</a>";
                    }
                    if (strFileName.Contains("Photo"))
                    {
                        //Get the file name
                        litPhotoFile.Text += "<br /><b>Photo submitted:</b> " + "<a href=\"" + strUploadServer + strFileName + "\" target=\"_blank\">" + strFileName + "</a>";
                    }
                }
            }
        }
        btnCmdUpload.Focus();
    }// end btnCmdUpload

    protected void BackPreviousPage()
    {
        // get the ID and populate the page
        //Set visibility
        personalInformation.Visible = true;
        divMorePersonalInfo.Visible = true;
        divSearchButton.Visible = false;
        if (blUnderAge)
        {
            homestay18.Visible = false;
            homestay1617.Visible = true;
        }
        else
        {
            homestay18.Visible = true;
            homestay1617.Visible = false;
        }
        PhotoLetter.Visible = true;
        divSave.Visible = true;
        if (blIsAdmin)
        {
            divNextPage.Visible = true;
        }

        //Check for existing HomestayStudentInfo record
        DataTable dtHSStudent = hsInfo.GetOneHomestayStudent(intApplicantID);
        if (dtHSStudent != null)
        {
            if (dtHSStudent.Rows.Count > 0)
            {
                blAddMode = false;
                hdnAddMode.Value = blAddMode.ToString();
                //strResultMsg += "blAddMode 1? " + blAddMode + "<br />";
                Configuration config = WebConfigurationManager.OpenWebConfiguration(Request.ApplicationPath);
                AppSettingsSection appSettings = (AppSettingsSection)config.GetSection("appSettings");
                strUploadServer = "/InternetContent" + appSettings.Settings["HomestayShare"].Value;
                foreach (DataRow drHSStudent in dtHSStudent.Rows)
                {
                    blLetterSubmitted = Convert.ToBoolean(drHSStudent["letterSubmitted"]);
                    hdnLetterSubmitted.Value = blLetterSubmitted.ToString();
                    blPhotoSubmitted = Convert.ToBoolean(drHSStudent["photoSubmitted"]);
                    hdnPhotoSubmitted.Value = blPhotoSubmitted.ToString();
                    if (blLetterSubmitted || blPhotoSubmitted)
                    {
                        litLetterFile.Text = "";
                        litPhotoFile.Text = "";
                        DataTable dt = hsInfo.GetUploadedFiles(intApplicantID);
                        if (dt != null)
                        {
                            if (dt.Rows.Count > 0)
                            {
                                foreach (DataRow dr in dt.Rows)
                                {
                                    strFileName = dr["fileName"].ToString();
                                    string filePath = dr["filePath"].ToString();
                                    string uploadServer = "";
                                    if (filePath.Contains("ccs-internet"))
                                    {
                                        uploadServer += @"apps.spokane.edu/InternetContent/";
                                    }
                                    else
                                    {
                                        uploadServer += @"portal.ccs.spokane.edu/InternetContent/";
                                    }
                                    if (strFileName.Contains("Letter"))
                                    {
                                        //get the file name
                                        litLetterFile.Text += "<br /><b>Letter submitted:</b> " + "<a href=\"" + strUploadServer + strFileName + "\" target=\"_blank\">" + strFileName + "</a>";
                                    }
                                    if (strFileName.Contains("Photo"))
                                    {
                                        //Get the file name
                                        litPhotoFile.Text += "<br /><b>Photo submitted:</b> " + "<a href=\"" + uploadServer + strFileName + "\" target=\"_blank\">" + strFileName + "</a>";
                                    }
                                }
                            }
                        }
                    }
                    //strResultMsg += "Letter submitted: " + Convert.ToBoolean(drHSStudent["letterSubmitted"]) + "<br />";
                    //strResultMsg += "Start Date: " + Convert.ToDateTime(drHSStudent["quarterStartDate"]).ToShortDateString() + "<br />";
                    if (Convert.ToDateTime(drHSStudent["quarterStartDate"]).ToShortDateString() != "1/1/1900")
                    {
                        try
                        {
                            rdoQuarterStart.SelectedValue = Convert.ToDateTime(drHSStudent["quarterStartDate"]).ToShortDateString();
                        }
                        catch
                        {
                            //do nothing
                        }
                    }
                    txtArrivalDate.Text = Convert.ToDateTime(drHSStudent["arrivalDate"]).ToShortDateString();
                    if (blIsAdmin)
                    {
                        txtDateApplied.Text = Convert.ToDateTime(drHSStudent["dateApplied"]).ToShortDateString();
                    }
                    if (!blUnderAge)
                    {
                        rdoHomestayOption.SelectedValue = drHSStudent["homestayOption"].ToString().Trim();
                    }
                }
                if (blIsAdmin)
                {
                    strResultMsg += "A Homestay student application exists.  You may review the information here.<br />";
                }
                else
                {
                    //setVisibilityNoAccess();
                    if (strButtonFace != "Save")
                    {
                        strResultMsg += "<span style='color:#DD0000;'>Your Homestay application has already been submitted. <br />You may view the information and upload more files</span>.</p>";
                        litAllRequired.Text = "";
                        PhotoLetter.Visible = true;
                        divUpload.Visible = true;
                        divSave.Visible = false;
                        divNextPage.Visible = true;
                    }
                    else
                    {
                        divNextPage.Visible = false;
                    }
                    
                }
            }
            else
            {
                strResultMsg = "Please complete the application form below.  The application consists of multiple pages.<br />";
                litAllRequired.Text = "";
            }
        }
        else
        {
            strResultMsg = "Please complete the application below.  The application consists of multiple pages.<br />";
            litAllRequired.Text = "";
        }
    }// end BackPreviousPage
}