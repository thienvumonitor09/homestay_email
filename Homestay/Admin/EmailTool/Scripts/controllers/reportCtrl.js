﻿app.controller('reportCtrl', function ($scope, $http, $window, $routeParams) {
    $scope.editorOptions = {
        language: 'en',
        allowedContent: true,
        entities: false,
        uiColor: '#D3D3D3'
    };
    $scope.demo = "here";
    $scope.emailReports = [];
    $scope.getEmailReports = function () {
        $http({
            method: "POST",
            url: "EmailToolWS.asmx/GetEmailReports",
            dataType: 'json',
            data: {},
            headers: { "Content-Type": "application/json" }
        }).then(function (response) {
            //alert(response.data);
            //console.log(response.data.d);
            $scope.emailReports = JSON.parse(response.data.d);

            angular.forEach($scope.emailReports, function (value, key) {
                var toEmailStr = value.toEmail;
                value.countTo = toEmailStr.split(",").length;
                var ccEmailStr = value.ccEmail;
                value.countCC = ccEmailStr.split(",").length;
            });
            console.log($scope.emailReports);
            //alert(response.data.d);
        }, function (error) {
            alert("error");
        });
    };

    $scope.getEmailReports();

});