﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Homestay_Admin_Reports_PlacementForm_V2 : System.Web.UI.Page
{
    string strResultMsg = "";
    String selectedStudent = "0";
    DateTime dob;
    String arrivalDate;
    String shortDate = "";
    Homestay hsInfo = new Homestay();
    DataTable dtDisplay;
    HomestayUtility hsUtility = new HomestayUtility();
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {

        lblInfo.Text = @"<h3>Before your student arrives:</h3>
                        <ul>
                            <li>Review the Culturegram about the student's country and culture.       
                            <li>Be sure the room has all required items, working smoke detectors, C0<sub>2</sub> monitors, fire ladders, etc.</li>
                            <li>Attend the quarterly Family Meeting for Homestay Families.</li>
                        </ul>
            <h3>After your student arrives:</h3>
            <ol>
                <li>Meet your student at the airport near Baggage Claim. Bring a sign with <b><i>all</i></b> of their names on it.</li>
                <li>Bring the student to school for their Orientation and/or testing date(s).</li>
                <li>Ride the bus with them, at least once, to and from school. Inform them to carry cash for the bus fee until their ID card is active.
                    <p>Note:  They should <i>always</i> carry cash for the bus, just in case the system is ever down. Note: exact amount is needed - bus drivers can't make change; and, if they take 2 buses from your home to school, they can request a ""Transfer"" onto the second bus.  They can also buy a day pass for extended riding.""
                    </p>
                </li>
                <li> Discuss the Homestay Student and Family Agreement and give the student a copy. Email a scanned copy to the Homestay Team at
                       <a href = ""mailto:InternationalHomestay@ccs.spokane.edu"" > InternationalHomestay@ccs.spokane.edu.</a> Be sure the student puts your
                              phone numbers in their own phone for emergencies, or if the need to contact you arises.</li>
          
                          <li> Help the student:
                            <ul>
                                <li> access your internet</ li >
   
                                <li> get a bank account</ li >
      
                                <li> purchase a cell phone plan</ li >
         
                            </ul>                         
                        </li>
         
                         <li> Be sure student attends the Homestay Success Meeting approximately one week after the quarter starts.</li>
            
             </ol>
             ";
        //Get studentID from querystring
        int intStudentID = GetQueryStringValue("studentID");
        dtDisplay = GetDisplayDt();
        //Populate ddlStudent
        if (!IsPostBack)
        {
            DataTable dtStudents = hsInfo.GetHomestayStudents("Active", 0);
            dtStudents.Columns.Add("valueField", typeof(string));
            if (dtStudents != null &&  dtStudents.Rows.Count > 0)
            {
                foreach (DataRow dr in dtStudents.Rows)
                {
                    string applicantIDStr = dr["applicantID"].ToString();
                    dr["valueField"] = dr["familyName"].ToString() + ", "
                                        + dr["firstName"].ToString() + " - " + Convert.ToDateTime(dr["DOB"]).ToShortDateString();
                }
            }
            ddlStudent.DataSource = dtStudents;
            ddlStudent.DataTextField = "valueField";
            ddlStudent.DataValueField = "applicantID";
            ddlStudent.DataBind();
            ddlStudent.Items.Insert(0, new ListItem("--Select a student--", "0")); //Add default value
            litResutlMsg.Text = strResultMsg;
            BindDetailView();

            
            ddlStudent.SelectedValue = intStudentID + "";


            //Populate emails
            int intFamilyID = GetQueryStringValue("familyID");
            DataTable dtRelatives = hsInfo.GetHomestayRelatives(0, intFamilyID);
            if (dtRelatives != null && dtRelatives.Rows.Count > 0)
            {
                foreach (DataRow drRelative in dtRelatives.Rows)
                {
                    if (drRelative["relationship"].ToString().Contains("Primary"))
                    {
                        txtTo.Text = drRelative["email"].ToString().Trim();
                    }
                }
            }
            
            //litArea.Text = "<b>something</b>";
            //litArea.Text = "here";

        }// NOT IsPostback
        hdnArea.Value= @"<b>123</b>";
        //litArea.Text = GetGridviewData(dvPlacement);
        //ddlStudent.SelectedValue = intStudentID+"";
        //GetDisplayDt();
        //BindDetailView();
        //selectedStudent = ddlStudent.SelectedValue;

        //DataTable dtStudentExtended2 = hsInfo.GetHomestayStudents("ID", Convert.ToInt32("0" + ddlStudent.SelectedValue));



    }

    private string GetShortDateString(string dateTimeStr)
    {
        DateTime arrivalDate;
        return DateTime.TryParse(dateTimeStr, out arrivalDate) ? arrivalDate.ToShortDateString() : "n/a";
    }
    protected void linkBtnHome_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Homestay/Admin/Default.aspx");
    }

    protected void DetailsViewExample_ItemUpdating(object sender, DetailsViewUpdateEventArgs e)
    {
        //txtOrientationStartDate
        TextBox txtOrientationStartDate = (TextBox)dvPlacement.Rows[2].Cells[1].Controls[1];
        dtDisplay.Rows[0]["OrientationStartDate"] = txtOrientationStartDate.Text;
        //txtUrgentCare
        TextBox txtUrgentCare = (TextBox)dvPlacement.Rows[12].Cells[1].Controls[1];
        //txtArrivalInfo
        TextBox txtArrivalInfo = (TextBox)dvPlacement.Rows[11].Cells[1].Controls[1];
        //Update dtDisplay
        dtDisplay.Rows[0]["OrientationStartDate"] = txtOrientationStartDate.Text;
        dtDisplay.Rows[0]["UrgentCare"] = txtUrgentCare.Text;
        dtDisplay.Rows[0]["ArrivalInfo"] = txtArrivalInfo.Text;
        dvPlacement.ChangeMode(DetailsViewMode.ReadOnly);
        BindDetailView();
    }
    protected void DetailsViewExample_ItemCommand1(object sender, DetailsViewCommandEventArgs e)
    {
        switch (e.CommandName.ToString())
        {
            case "Edit":
                dvPlacement.ChangeMode(DetailsViewMode.Edit);
                BindDetailView();
                break;
            case "Cancel":
                dvPlacement.ChangeMode(DetailsViewMode.ReadOnly);
                BindDetailView();
                break;
        }
    }
    protected void DetailsViewExample_ModeChanging(object sender, DetailsViewModeEventArgs e)
    {
    }
    protected void DetailsViewExample_DataBound(object sender, EventArgs e)
    {
        if (((DetailsView)sender).CurrentMode == DetailsViewMode.Edit)
        {
            DataRowView row = (DataRowView)((DetailsView)sender).DataItem;
            TextBox txtUrgentCare = (TextBox)dvPlacement.FindControl("txtUrgentCare");
            /*
            RadioButtonList rblGender =
                (RadioButtonList)((DetailsView)sender).FindControl("rbGender");
            DropDownList ddlQualification =
                (DropDownList)((DetailsView)sender).FindControl("ddlQualification");
                */

           
            //rblGender.SelectedValue = row[2].ToString().Trim();
            //ddlQualification.SelectedValue = row[3].ToString().Trim();

        }
    }
    private DataTable GetDisplayDt()
    {
        int intStudentID = GetQueryStringValue("studentID");
        //int intStudentID = hsUtility.ParseInt(ddlStudent.SelectedValue);
        DataTable dtDisplay = new DataTable();
        dtDisplay.Columns.AddRange(new DataColumn[13]
                    {

                        new DataColumn("Campus", typeof(string)),
                        new DataColumn("QuarterOfArrival", typeof(string)),
                        new DataColumn("OrientationStartDate", typeof(string)),
                        new DataColumn("QuarterStartDate", typeof(string)),
                        new DataColumn("StudentName", typeof(string)),
                        new DataColumn("StudentGender", typeof(string)),
                        new DataColumn("DOB", typeof(string)),
                        new DataColumn("Country", typeof(string)),
                        new DataColumn("ProgramOfStudy", typeof(string)),
                        new DataColumn("Email", typeof(string)),
                        new DataColumn("Hobbies", typeof(string)),
                        new DataColumn("ArrivalInfo", typeof(string)),
                        new DataColumn("UrgentCare", typeof(string))
                    });
        DataRow displayRow = dtDisplay.NewRow();
        DataTable dtStudentExtended = hsInfo.GetHomestayStudents("ID", intStudentID); 
        //OrientationStartDate
        //displayRow["OrientationStartDate"] = "demo";
        if (dtStudentExtended != null && dtStudentExtended.Rows.Count > 0)
        {
            DataRow drStudent = dtStudentExtended.Rows[0];
            string sqlQuerySchoolInfo = @"SELECT *
                                  FROM [CCSInternationalStudent].[dbo].[SchoolInformation]
                                  where applicantID = " + intStudentID;
            DataTable dtSchoolInfo = hsInfo.RunGetQuery(sqlQuerySchoolInfo);
            //DataTable dtStudentMore = hsInfo.GetStudentStudyInfo(Convert.ToInt32( ddlStudent.SelectedValue));
            if (dtSchoolInfo != null && dtSchoolInfo.Rows.Count > 0)
            {
                DataRow drSchoolInfo = dtSchoolInfo.Rows[0];
                //Campus
                string campus = drSchoolInfo["studyWhere"].ToString();
                if (campus.Contains("SCC"))
                {
                    campus = "Spokane Community College - SCC";
                }
                else if (campus.Contains("SFCC"))
                {
                    campus = "Spokane Falls Community College - SFCC";
                }
                else if (campus.Contains("Pullman"))
                {
                    campus = "Pullman";
                }
                else
                {
                    campus = "Other";
                }
                displayRow["Campus"] = campus;

                //ProgramOfStudy
                displayRow["ProgramOfStudy"] = drSchoolInfo["studyWhat"].ToString().Trim().Replace("''", "'")
                                            + " - " + drSchoolInfo["studyMajor"].ToString().Trim().Replace("''", "'")
                                            + drSchoolInfo["studyMajorOther"].ToString().Trim().Replace("''", "'");
            }

            //QuarterOfArrival
            displayRow["QuarterOfArrival"] = GetShortDateString(drStudent["arrivalDate"].ToString());

            //StudentName
            displayRow["StudentName"] = drStudent["firstName"].ToString().Trim().Replace("''", "'")
                                        + " " + drStudent["middleNames"].ToString().Trim().Replace("''", "'")
                                        + " " + drStudent["familyName"].ToString().Trim().Replace("''", "'");
            //Gender
            string gender = drStudent["Gender"].ToString();
            if (gender.Equals("M"))
            {
                gender = "Male";
            }
            else if (gender.Equals("F"))
            {
                gender = "Female";
            }
            else
            {
                gender = "Other";
            }
            displayRow["StudentGender"] = gender;

            
            //DOB
            displayRow["DOB"] = GetShortDateString(drStudent["DOB"].ToString());

            //Country
            displayRow["Country"] = drStudent["countryOfCitizenship"].ToString();

            //Email
            DataTable dtInfo = hsInfo.GetOneInternationalStudent("", intStudentID, "", "", "", "");
            if (dtInfo != null && dtInfo.Rows.Count > 0)
            {
                displayRow["Email"] = dtInfo.Rows[0]["Email"].ToString();
            }
            //Hobbies
            
            List<string> hobbiesList = hsUtility.GetPreferenceSelectionsByType("hobbies","student",intStudentID);
            displayRow["Hobbies"] = string.Join(", ", hobbiesList.ToArray());

            //UrgentCare
            displayRow["UrgentCare"] = "$20 co-pay";

        }

        

        dtDisplay.Rows.Add(displayRow);
        return dtDisplay;
    }
    private void BindDetailView()
    {


        //Bind dtDisplay to dvPlacement
        dvPlacement.DataSource = dtDisplay;
        dvPlacement.DataBind();
        //litArea.Text = GetGridviewData(dvPlacement);
    }

   
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        Response.Redirect("./PlacementForm_V.aspx?studentID="+ddlStudent.SelectedValue);
        GetDisplayDt();
        BindDetailView();
    }

    /*
    private string GetHobbies()
    {
        string strHobbies = "";
        DataTable dtPref = hsInfo.GetHomestayPreferenceSelections(Convert.ToInt32(ddlStudent.SelectedValue), 0);

        string[] hobbies = { "MusicalInstrument", "Art" ,"TeamSports", "IndividualSports" ,"ListenMusic","Drama","WatchMovies",
                                "Singing","Shopping","ReadBooks","Outdoors","Cooking", "Photography", "Gaming"};
        foreach (DataRow dtFamPrefRow in dtPref.Rows)
        {
            //Text = new Paragraph(dtFamPrefRow["fieldID"].ToString().Trim() +" " + dtFamPrefRow["fieldValue"].ToString().Trim());
            //pdfDoc.Add(Text);
            string fieldIDStr = dtFamPrefRow["fieldID"].ToString().Trim();
            string fieldValueStr = dtFamPrefRow["fieldValue"].ToString().Trim();
            if (Array.Exists(hobbies, element => element == fieldValueStr))
            {
                strHobbies += dtFamPrefRow["Preference"].ToString().Trim() + ", ";
            }
        }

        DataTable dtOneHomestayStudent = hsInfo.GetOneHomestayStudent(Convert.ToInt32(ddlStudent.SelectedValue));
        if (dtOneHomestayStudent != null && dtOneHomestayStudent.Rows.Count > 0)
        {
            if (!String.IsNullOrEmpty(dtOneHomestayStudent.Rows[0]["activitiesEnjoyed"].ToString()))
            {
                strHobbies += "; " + dtOneHomestayStudent.Rows[0]["activitiesEnjoyed"].ToString().Trim();
            }
        }
        strHobbies = strHobbies.Trim();
        if (!String.IsNullOrEmpty(strHobbies) && strHobbies[strHobbies.Length - 1] == ',')
        {
            strHobbies = strHobbies.Substring(0, strHobbies.Length - 1);
        }
        return strHobbies;
    }
    */

    protected void Button1_Click(object sender, EventArgs e)
    {
        Process.Start(String.Format(
             "mailto:{0}?subject={1}&cc={2}&bcc={3}&body={4}",
              "", "", "", "", ""));
    }

    protected void SendMail_Click(object sender, EventArgs e)
    {
        //public string SendEmailMsg(string strTo, string strFrom, string strSubject, bool blIsHTML, string strBody, string strCCEmailAddresses)
       
       
        //string body = GetGridviewData(dvPlacement);
        //body += lblInfo.Text;

        int intFamilyID = GetQueryStringValue("familyID");
        string result = "";
        
        /*
        DataTable dtRelatives = hsInfo.GetHomestayRelatives(0, intFamilyID);
        
        if (dtRelatives != null && dtRelatives.Rows.Count > 0)
        {
            foreach (DataRow drRelative in dtRelatives.Rows)
            {
                if (drRelative["relationship"].ToString().Contains("Primary"))
                {
                    string toEmail = drRelative["email"].ToString().Trim();
                    //"Internationalhomestay@ccs.spokane.edu"
                    result = utility.SendEmailMsg("vu.nguyen@ccs.spokane.edu", "Internationalhomestay@ccs.spokane.edu", "New Student Placement", true, body, "vu.nguyen@ccs.spokane.edu");
                }
            }
        }
        */
        string[] toEmailList = txtTo.Text.Split(';');
        foreach (string toEmail in toEmailList)
        {
            //txtTo.Text += toEmail;
            //result = utility.SendEmailMsg(toEmail, "Internationalhomestay@ccs.spokane.edu", "New Student Placement", true, body, txtCC.Text);
        }
        
        result = utility.SendEmailMsg("vu.nguyen@ccs.spokane.edu", "Internationalhomestay@ccs.spokane.edu", "New Student Placement", true, "here", txtCC.Text);
        if (result.Contains("Success"))
        {
            litResultMsg.Text = "An Email has been sent to Primary Contact of the Family.";
        }

        
    }

    /*
    // This Method is used to render gridview control
    public string GetGridviewData(DetailsView gv)
    {
        DetailsView gv2 = gv;

        StringBuilder strBuilder = new StringBuilder();
        StringWriter strWriter = new StringWriter(strBuilder);
        HtmlTextWriter htw = new HtmlTextWriter(strWriter);
        gv2.Rows[13].Visible = false; //Hide commandfield
        gv2.RenderControl(htw);
        gv2.Rows[13].Visible = true; //Unhide commandfield
        return strBuilder.ToString();
    }

    //This function to prevent exception when render page
    public override void VerifyRenderingInServerForm(Control control)
    {
        return;
    }
    */
    public int GetQueryStringValue(string queryString)
    {
        int id = 0;
        if ((Request.QueryString[queryString] != null && Request.QueryString[queryString].ToString() != "0"))
        {
            id = hsUtility.ParseInt(Request.QueryString[queryString]);
        }
        return id;
    }

   
    public static string ConvertDataTableToHTML(DataTable dt)
    {
        string html = @"<table cellspacing=""0"" cellpadding =""10"" rules =""all"" border =""1"" id =""MainContent_dvPlacement"" style =""font - size:Large; width: 100 %; border - collapse:collapse; "" > ";
        //add header row
        html += "<tbody>";
        for (int i = 0; i < dt.Columns.Count; i++)
            html += "<td>" + dt.Columns[i].ColumnName + "</td>";
        html += "</tr>";
        //add rows
        for (int i = 0; i < dt.Columns.Count; i++)
        {
            html += "<tr>";
            html += @"<td style=""background - color:#E4E4E4;font-weight:bold;height:50px;width:30%;"">" + dt.Columns[i].ColumnName + "</td>";
            html += "<td>" + dt.Rows[0][i].ToString() + "</td>";

            html += "</tr>";
        }
        html += "</tbody>";
        html += "</table>";
        return html;
    }
}