﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Homestay_Admin_StatusMaintenance : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        String selectedStudent = "0";
        String selectedFamily = "0";
        String strStudentStatus = "";
        String strFamilyStatus = "";
        String selectedStudentStatus = "";
        String selectedFamilyStatus = "";
        String strResultMsg = "";
        Int32 intResult = 0;

        Homestay hsInfo = new Homestay();
        DataTable dtStudents = hsInfo.GetHomestayStudents("Active", 0);
        DataTable dtFamilies = hsInfo.GetHomestayFamilyInfo(0, "", "DDLactive");
        String strButtonFace = "";

        if (IsPostBack)
        {
            selectedStudent = ddlStudents.SelectedValue;
            selectedFamily = ddlFamilies.SelectedValue;
            selectedStudentStatus = ddlStudentStatus.SelectedValue;
            selectedFamilyStatus = ddlFamilyStatus.SelectedValue;
        }
        if (Request.Form["btnSubmit"] != null)
        {
            strButtonFace = Request.Form["btnSubmit"].ToString();
        }
        //strResultMsg += "Button Face: " + strButtonFace + "<br />";
        if (strButtonFace != "")
        {

            switch (strButtonFace)
            {
                case "Return to Admin Home Page":
                    Response.Redirect("/Homestay/Admin/Default.aspx");
                    break;
                case "Update Student Status":
                    //strResultMsg += "Update Student Status value: " + ddlStudentStatus.SelectedItem.Text + "; Student: " + selectedStudent + "<br />";
                    intResult = hsInfo.UpdateHomestayStudentStatus(0, Convert.ToInt32(selectedStudent), ddlStudentStatus.SelectedItem.Text);
                    break;
                case "Update Family Status":
                    intResult = hsInfo.UpdateFamilyStatus(Convert.ToInt32(selectedFamily), ddlFamilyStatus.SelectedItem.Text);
                    break;
            }
            if (intResult != 0) { strResultMsg += "Your submission was successful!<br />"; }
        }

        //Load DDLs
        //Load the DDLs
        ddlStudents.Items.Clear();
        ListItem LI;
        LI = new ListItem();
        LI.Value = "0";
        LI.Text = "-- Select One --";
        ddlStudents.Items.Add(LI);
        if (dtStudents != null)
        {
            if (dtStudents.Rows.Count > 0)
            {
                foreach (DataRow dr in dtStudents.Rows)
                {
                    LI = new ListItem();
                    LI.Value = dr["applicantID"].ToString();
                    LI.Text = dr["familyName"].ToString() + ", " + dr["firstName"].ToString() + " - " + Convert.ToDateTime(dr["DOB"]).ToShortDateString();
                    ddlStudents.Items.Add(LI);
                }
            }
            else { strResultMsg += "Error: dtStudents has no rows.<br />"; }
        }
        else { strResultMsg += "Error: dtStudents is null.<br />"; }

        ddlFamilies.Items.Clear();
        LI = new ListItem();
        LI.Value = "0";
        LI.Text = "-- Select One --";
        ddlFamilies.Items.Add(LI);
        if (dtFamilies != null)
        {
            if (dtFamilies.Rows.Count > 0)
            {
                foreach (DataRow dr in dtFamilies.Rows)
                {
                    LI = new ListItem();
                    LI.Value = dr["id"].ToString();
                    LI.Text = dr["homeName"].ToString();
                    ddlFamilies.Items.Add(LI);
                }
            }
            else { strResultMsg += "Error: dtFamilies has no rows.<br />"; }
        }
        else { strResultMsg += "Error: dtFamilies is null.<br />"; }

        DataTable dtStudentStatus = hsInfo.GetHomestayStatusValues("Student");
        ddlStudentStatus.Items.Clear();
        LI = new ListItem();
        LI.Value = "0";
        LI.Text = "-- Select One --";
        ddlStudentStatus.Items.Add(LI);
        if (dtStudentStatus != null)
        {
            if (dtStudentStatus.Rows.Count > 0)
            {
                foreach (DataRow dr in dtStudentStatus.Rows)
                {
                    LI = new ListItem();
                    LI.Value = dr["id"].ToString();
                    LI.Text = dr["statusDescription"].ToString().Trim();
                    ddlStudentStatus.Items.Add(LI);
                }
            }
            else { strResultMsg += "Error: dtStudentStatus has no rows.<br />"; }
        }
        else { strResultMsg += "Error: dtStudentStatus is null.<br />"; }

        DataTable dtFamilyStatus = hsInfo.GetHomestayStatusValues("Family");
        ddlFamilyStatus.Items.Clear();
        LI = new ListItem();
        LI.Value = "0";
        LI.Text = "-- Select One --";
        ddlFamilyStatus.Items.Add(LI);
        if (dtFamilyStatus != null)
        {
            if (dtFamilyStatus.Rows.Count > 0)
            {
                foreach (DataRow dr in dtFamilyStatus.Rows)
                {
                    LI = new ListItem();
                    LI.Value = dr["id"].ToString();
                    LI.Text = dr["statusDescription"].ToString().Trim();
                    ddlFamilyStatus.Items.Add(LI);
                }
            }
            else { strResultMsg += "Error: dtFamilyStatus has no rows.<br />"; }
        }
        else { strResultMsg += "Error: dtFamilyStatus is null.<br />"; }

        //Set current status
        if (selectedStudent != "" && selectedStudent != null && selectedStudent != "0")
        {
            ddlStudents.SelectedValue = selectedStudent;
            DataTable dtStudentInfo = hsInfo.GetOneHomestayStudent(Convert.ToInt32(selectedStudent));
            if (dtStudentInfo != null)
            {
                if (dtStudentInfo.Rows.Count > 0)
                {
                    strStudentStatus = dtStudentInfo.Rows[0]["homestayStatus"].ToString().Trim();
                    studentStatus.Text = strStudentStatus;
                }
            }
        }
        if (selectedFamily != "" && selectedFamily != null && selectedFamily != "0")
        {
            ddlFamilies.SelectedValue = selectedFamily;
            DataTable dtFamilyInfo = hsInfo.GetHomestayFamilyInfo(Convert.ToInt32(selectedFamily), "", "ID");
            if (dtFamilyInfo != null)
            {
                if (dtFamilyInfo.Rows.Count > 0)
                {
                    strFamilyStatus = dtFamilyInfo.Rows[0]["familyStatus"].ToString().Trim();
                    familyStatus.Text = strFamilyStatus;
                }
            }
        }

        //Grab any messages from page or class and display
        litResultMsg.Text = "<p>" + hsInfo.strResultMsg + strResultMsg + "</p>";
    }
}