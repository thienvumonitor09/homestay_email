﻿using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Homestay_Admin_Reports_Student_ListActive : System.Web.UI.Page
{
    Homestay hsInfo = new Homestay();
    protected void Page_Load(object sender, EventArgs e)
    {
        lblError.Text = "";
        litStudentCount.Text = "";
        if (!Page.IsPostBack)
        { 
            ShowData();
        }
    }

    protected void ShowData()
    {
        DataTable dt = GetDataTable();
        gvStudents.DataSource = dt;
        gvStudents.DataBind();
        ViewState["dirState"] = dt;
        ViewState["sortdr"] = "Asc";
    }

    private DataTable GetDataTable()
    {
        

        int queryApplicantID = 0;
        if ((Request.QueryString["applicantID"] != null && Request.QueryString["applicantID"].ToString() != "0"))
        {
            queryApplicantID = Convert.ToInt32(Request.QueryString["applicantID"]);
        }
        DataTable dtDisplay = new DataTable();
        dtDisplay.Columns.AddRange(new DataColumn[17]
                    {
                        new DataColumn("id", typeof(string)),
                        new DataColumn("FullName", typeof(string)),
                        new DataColumn("Gender", typeof(string)),
                        new DataColumn("Campus", typeof(string)),
                        new DataColumn("Age", typeof(string)),
                        new DataColumn("DOB", typeof(string)),
                        new DataColumn("Country", typeof(string)),
                        new DataColumn("PlacementStatus", typeof(string)),
                        new DataColumn("HostFamily", typeof(string)),
                        new DataColumn("Meeting", typeof(string)),
                        new DataColumn("Agreement", typeof(string)),
                        new DataColumn("PhoneNumber", typeof(string)),
                        new DataColumn("Email", typeof(string)),
                        new DataColumn("Hobbies", typeof(string)),
                        new DataColumn("Transportation", typeof(string)),
                        new DataColumn("SmokingPolicy", typeof(string)),
                        new DataColumn("AirportPickup", typeof(string))
                    });
        
        //id
        DataTable dtStudents = hsInfo.GetHomestayStudents("ReportActive", queryApplicantID);
        if (dtStudents != null && dtStudents.Rows.Count > 0)
        {
            foreach (DataRow drStudent in dtStudents.Rows)
            {
                DataRow displayRow = dtDisplay.NewRow();
                var applicantID = Convert.ToInt32(drStudent["applicantID"].ToString().Trim());
                //id
                displayRow["id"] = drStudent["applicantID"].ToString().Trim();
                
                //FullName
                string fullName = String.Format("{0}, {1} {2}", drStudent["familyName"].ToString(), drStudent["firstName"].ToString(), drStudent["middleNames"].ToString());
                displayRow["FullName"] = fullName;
                

                //Gender
                string gender = drStudent["Gender"].ToString();
                if (gender.Equals("M"))
                {
                    gender = "Male";
                }else if (gender.Equals("F"))
                {
                    gender = "Female";
                }
                else
                {
                    gender = "Other";
                }
                displayRow["Gender"] = gender;

                //Campus
                DataTable dtInfo = hsInfo.GetOneInternationalStudent("", applicantID, "", "", "", "");
                var campus = "";

                if (dtInfo != null && dtInfo.Rows.Count > 0)
                {
                    campus = dtInfo.Rows[0]["studyWhere"].ToString();
                    
                }
                displayRow["Campus"] = campus;

                //Age, DOB
                string age = "";
                string dob = "";
                try
                {
                    DateTime bDate = Convert.ToDateTime(drStudent["DOB"].ToString().Trim());
                    age = GetAge(bDate).ToString();
                    dob = bDate.ToShortDateString();
                }
                catch
                {
                    
                }
                displayRow["Age"] = age;
                displayRow["DOB"] = dob;


                //Country
                displayRow["Country"] = drStudent["countryOfCitizenship"].ToString();
                //Status
                //HostFamily
                displayRow["HostFamily"] = "";
                DataTable dtPlacement = GetDtPlacementByStudentID(applicantID);
                string familyNames = "";
                string placementStatus = drStudent["homestayStatus"].ToString().Trim();
                var familyNameList = new List<string>();
                if (dtPlacement != null && dtPlacement.Rows.Count > 0)
                {
                    foreach (DataRow drPlacement in dtPlacement.Rows)
                    {
                        /*
                        string placementInfo = String.Format("{0}, From: {1} To:{2} <br/>",
                                                                drPlacement["placementName"].ToString()
                                                                , drPlacement["effectiveQuarter"].ToString()
                                                                , drPlacement["endQuarter"].ToString());
                                                                */
                        familyNameList.Add(drPlacement["placementName"].ToString());
                                               // familyNames += placementInfo;
                    }
                    placementStatus = "Placed";
                }
                
                //displayRow["HostFamily"] = string.Join("; ",familyNameList);
                //PlacementStatus
                displayRow["PlacementStatus"] = placementStatus;
                DataTable dtStudentInfo = GetDtStudentInfo(applicantID);
                //Meeting       
                displayRow["Meeting"] = dtStudentInfo.Rows[0]["Meeting"].ToString();
                //Agreement
                displayRow["Agreement"] = dtStudentInfo.Rows[0]["Agreement"].ToString();
                //Add row
                dtDisplay.Rows.Add(displayRow);
            }
        }
        
        return dtDisplay;
    }

    protected void OnSelectedIndexChanged(object sender, EventArgs e)
    {
        BindDetailView();
    }

    protected void AgeChanged(object sender, EventArgs e)
    {
        //DropDownList ddlAge = (DropDownList)sender;

        string age = ddlAge.SelectedValue;

        DataTable dt = GetDataTable();
        DataTable newDt = dt;
        if (age.Equals("Under"))
        {
            newDt = dt.AsEnumerable()
                            .Where(r => Convert.ToInt32(r.Field<string>("Age")) < 18)
                            .CopyToDataTable();
        }
        else if (age.Equals("18"))
        {
            newDt = dt.AsEnumerable()
                            .Where(r => Convert.ToInt32(r.Field<string>("Age")) >= 18)
                            .CopyToDataTable();
        }
        
        gvStudents.DataSource = newDt;
        gvStudents.DataBind();

        //To pre-populate selected age after postback
        //DropDownList ddlAge2 = (DropDownList)gvStudents.HeaderRow.FindControl("ddlAge");
        //ddlAge2.Items.FindByValue(age).Selected = true;
        ViewState["dirState"] = newDt;
        ViewState["sortdr"] = "Asc";
        //BindDetailView();

    }

    protected void DetailsViewExample_ItemUpdating(object sender, DetailsViewUpdateEventArgs e)
    {
        var id = ((Label)dvStudent.FindControl("lblid")).Text;
        var txtMajor = (TextBox)dvStudent.FindControl("txtMajor");
        var rblMeeting = (RadioButtonList)dvStudent.FindControl("rblMeeting");
        var ddlQuarter = (DropDownList)dvStudent.FindControl("ddlQuarter");
        var txtYear = (TextBox)dvStudent.FindControl("txtYear");

        var interests = new List<string>();
        CheckBoxList cblProgram = (CheckBoxList)dvStudent.FindControl("cblProgram");
        foreach (System.Web.UI.WebControls.ListItem item in cblProgram.Items)
        {
            if (item.Selected)
            {
                interests.Add(item.Value);
            }
        }

        //Validation for Quarter-Year
        //Validation rule: both blank or both entered; Quarter's value != 0 && Year.Lentgh == 4 && Year only contains digits
        if (!ddlQuarter.SelectedValue.Equals("0"))
        {
            if (string.IsNullOrEmpty(txtYear.Text))
            {
                lblError.Text = "Agreement year cannot be blank.";
                return;
            }
            else
            {
                if (txtYear.Text.Length != 4)
                {
                    lblError.Text = "Year can only contains 4 digits.";
                    return;
                }
                else
                {
                    if (!txtYear.Text.All(char.IsDigit)) //check year only contains digit
                    {
                        lblError.Text = "Year can only contains digits.";
                        return;
                    }
                }
                
                
                
                
            }
        }
        else
        {
            if (!string.IsNullOrEmpty(txtYear.Text))
            {
                lblError.Text = "Agreement Quarter must be selected.";
                return;
            }else if (!txtYear.Text.All(char.IsDigit))
            {
                if (txtYear.Text.Length != 4)
                {
                    lblError.Text = "Year can only contains 4 digits.";
                    return;
                }
                else
                {
                    lblError.Text = "Year can only contains digits.";
                    return;
                }   
            }  
        }
        
        
        string strAgreement = ddlQuarter.SelectedValue + " " + txtYear.Text;
        string updateQuery = @"UPDATE [dbo].[HomestayStudentInfo]
                                   SET Program ='" + String.Join("|", interests) + "'"
                                   + ", Major ='" + txtMajor.Text.Replace("'","''") + "'"
                                   + ", Meeting ='" + rblMeeting.SelectedValue + "'"
                                   + ", Agreement ='" + strAgreement + "'"
                                 + " WHERE applicantID=" + id;
        hsInfo.runSPQuery(updateQuery, false);
        dvStudent.ChangeMode(DetailsViewMode.ReadOnly);
        BindDetailView();
        ShowData();//Update GridView gvStudents
    }
    protected void gvStudents_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dtrslt = (DataTable)ViewState["dirState"];
        if (dtrslt.Rows.Count > 0)
        {
            if (Convert.ToString(ViewState["sortdr"]) == "Asc")
            {
                dtrslt.DefaultView.Sort = e.SortExpression + " Desc";
                ViewState["sortdr"] = "Desc";
            }
            else
            {
                dtrslt.DefaultView.Sort = e.SortExpression + " Asc";
                ViewState["sortdr"] = "Asc";
            }
            gvStudents.DataSource = dtrslt;
            gvStudents.DataBind();
        }

    }
    protected void DetailsViewExample_ItemCommand1(object sender, DetailsViewCommandEventArgs e)
    {
        switch (e.CommandName.ToString())
        {
            case "Edit":
                dvStudent.ChangeMode(DetailsViewMode.Edit);
                BindDetailView();
                break;
            case "Cancel":
                dvStudent.ChangeMode(DetailsViewMode.ReadOnly);
                BindDetailView();
                break;
        }
    }


    protected void DetailsViewExample_ModeChanging(object sender, DetailsViewModeEventArgs e)
    {
    }
    protected void DetailsViewExample_DataBound(object sender, EventArgs e)
    {
        if (((DetailsView)sender).CurrentMode == DetailsViewMode.Edit)
        {
            DataRowView row = (DataRowView)((DetailsView)sender).DataItem;
            DataTable dtDvStudent = GetDvStudent();
            DataRow drDvStudent = dtDvStudent.Rows[0];
            //Populate cblProgram
            string[] interests = dtDvStudent.Rows[0]["Program"].ToString().Trim().Split('|');
            CheckBoxList cblProgram = (CheckBoxList)dvStudent.FindControl("cblProgram");
            foreach (System.Web.UI.WebControls.ListItem item in cblProgram.Items)
            {
                if(Array.Exists(interests, element => element == item.Value))
                {
                    item.Selected = true;
                }
            }

            
            int intStudentID = Convert.ToInt32(drDvStudent["id"].ToString());
            DataTable dtStudentInfo = GetDtStudentInfo(intStudentID);
            RadioButtonList rblMeeting = (RadioButtonList)dvStudent.FindControl("rblMeeting");
            DataRow drStudentInfo = dtStudentInfo.Rows[0];
            //Populate success meeting
            rblMeeting.SelectedValue = drStudentInfo["Meeting"].ToString();
            //Populate Agreement
            DropDownList ddlQuarter = (DropDownList)dvStudent.FindControl("ddlQuarter");
            TextBox txtYear = (TextBox)dvStudent.FindControl("txtYear");
            string agreement = drStudentInfo["Agreement"].ToString();
            string[] arr = agreement.Split(' ');
            if (arr.Length == 2)
            {
                ddlQuarter.SelectedValue = arr[0];
                txtYear.Text = arr[1];
            }
            else
            {
                //ddlQuarter.SelectedValue = "Summer";
                //txtYear.Text = "123";
            }
            
        }
    }
    private DataTable GetDvStudent() {
        //Bind dtDisplay to dvPlacement
        string id = (gvStudents.SelectedRow.FindControl("lblid") as Label).Text;
        int applicantID = Convert.ToInt32(id);
        return GetDvStudentByID(applicantID);
    }
    private DataTable GetDvStudentByID(int id)
    {
        //Bind dtDisplay to dvPlacement
        DataTable dt = new DataTable();
        DataRow displayRow = dt.NewRow();
        dt.Columns.AddRange(new DataColumn[12]
            {
                new DataColumn("id", typeof(string)),
                new DataColumn("ctcID", typeof(string)),
                new DataColumn("Email", typeof(string)),
                new DataColumn("CellPhone", typeof(string)),
                new DataColumn("MailingAddress", typeof(string)),
                new DataColumn("studyWhat", typeof(string)),
                new DataColumn("studyMajor", typeof(string)),
                new DataColumn("Program", typeof(string)),
                new DataColumn("Major",typeof(string)),
                new DataColumn("Notes",typeof(string)),
                new DataColumn("Meeting",typeof(string)),
                new DataColumn("Agreement",typeof(string))
            });

        int applicantID = id;
        DataTable dtInfo = hsInfo.GetOneInternationalStudent("", Convert.ToInt32(id), "", "", "", "");
        string studyWhat = "";
        string studyMajor = "";
        string strCtcID = "";

        if (dtInfo != null && dtInfo.Rows.Count > 0)
        {
            DataRow drInfo = dtInfo.Rows[0];
            studyWhat = drInfo["studyWhat"].ToString();
            studyMajor = drInfo["studyMajor"].ToString();
            //ctcID
            displayRow["ctcID"] = drInfo["SID"].ToString();
            //Email
            displayRow["Email"] = drInfo["Email"].ToString();
            //CellPhone
            displayRow["CellPhone"] = drInfo["LocalPhone"].ToString();
            //MailingAddress
            if (!String.IsNullOrEmpty(drInfo["LocalAddress"].ToString()))
            {
                displayRow["MailingAddress"] = drInfo["LocalAddress"].ToString() + ", " 
                        + drInfo["LocalCity"].ToString() 
                        + ", " + drInfo["LocalState"].ToString() + ", " + drInfo["LocalZip"].ToString();
            }

        }

       
        //id
        displayRow["id"] = id;
        
       
        
        //CellPhone
        
        displayRow["studyWhat"] = studyWhat;
        displayRow["studyMajor"] = studyMajor;
        string sqlQuery = @"SELECT * FROM [CCSInternationalStudent].[dbo].[HomestayStudentInfo] WHERE applicantID=" + id;
        DataTable dt1 = hsInfo.RunGetQuery(sqlQuery);

        displayRow["Program"] = dt1.Rows[0]["Program"].ToString();
        displayRow["Major"] = dt1.Rows[0]["Major"].ToString();

        //Notes
        string notes = "";
        DataTable dtNotes = hsInfo.GetHomestayNotesExtended("Student", "" + applicantID, "0", "0", "0", "", "", "");
        if (dtNotes != null && dtNotes.Rows.Count > 0)
        {
            foreach (DataRow drNotes in dtNotes.Rows)
            {
                string detailNote = String.Format("{0})"
                                            + " <b>Note:</b> {1},"
                                            + " <b>Date Created:</b> {2},"
                                            + " <b>Family:</b> {3},"
                                            + " <b>Action Needed:</b> {4},"
                                            + " <b>Action Done:</b> {5} <br/>"
                                            , dtNotes.Rows.IndexOf(drNotes) + 1
                                            , drNotes["Note"].ToString()
                                            , Convert.ToDateTime(drNotes["createDate"].ToString().Trim()).ToShortDateString()
                                            , drNotes["homeName"].ToString()
                                            , drNotes["actionNeeded"].ToString()
                                            , drNotes["actionCompleted"].ToString()
                                            );
                notes += detailNote;
            }
        }

        displayRow["Notes"] = notes;

        //Meeting
        displayRow["Meeting"] = dt1.Rows[0]["Meeting"].ToString();
        //Agreement
        displayRow["Agreement"] = dt1.Rows[0]["Agreement"].ToString();
        dt.Rows.Add(displayRow);
        return dt;
    }
    private void BindDetailView()
    {
        
        dvStudent.DataSource = GetDvStudent();
        dvStudent.DataBind();
    }

    private int GetAge(DateTime dateOfBirth)
    {
        int age = 0;
        age = DateTime.Now.Year - dateOfBirth.Year;
        if (DateTime.Now.DayOfYear < dateOfBirth.DayOfYear)
            age = age - 1;

        return age;
    }

    private DataTable GetDtStudentInfo(int id)
    {
        string sqlQuery = @"SELECT * FROM [CCSInternationalStudent].[dbo].[HomestayStudentInfo] WHERE applicantID=" + id;
        return hsInfo.RunGetQuery(sqlQuery);
    }

    private DataTable GetDtPlacementByStudentID(int studentID)
    {
        
        string sqlQuery = @"SELECT *
                              FROM [CCSInternationalStudent].[dbo].[HomestayPlacement]
                              where applicantID=" + studentID;
        return hsInfo.RunGetQuery(sqlQuery);
    }

    protected void ExportToExcel(object sender, EventArgs e)
    {
        Response.Clear();
        Response.Buffer = true;
        
        Response.AddHeader("content-disposition", "attachment;filename=Active_Homestay_Students_List.xls");
        Response.Charset = "";
        Response.ContentType = "application/vnd.ms-excel";
        using (StringWriter sw = new StringWriter())
        {
            HtmlTextWriter hw = new HtmlTextWriter(sw);

            //To Export all pages
            GridView1.AllowPaging = false;
            //BindGridViewExport();

            //Add DetailsView to GridView
            DataTable dt = GetDataTable();
            dt.Columns.AddRange(new DataColumn[3]
            {
                 new DataColumn("Program", typeof(string)),
                 new DataColumn("Major", typeof(string)),
                 new DataColumn("Notes", typeof(string))
            });

            foreach (DataRow dr in dt.Rows)
            {
                int studentID = Convert.ToInt32(dr["id"].ToString());
                DataTable dtDv = GetDvStudentByID(studentID);
                DataRow drDv = dtDv.Rows[0];

                dr["Program"] = drDv["Program"].ToString();
                dr["Major"] = drDv["Major"].ToString();
                dr["Notes"] = drDv["Notes"].ToString();
            }

            GridView1.DataSource = dt;
            GridView1.DataBind();

            GridView1.HeaderRow.BackColor = System.Drawing.Color.White;
            foreach (TableCell cell in GridView1.HeaderRow.Cells)
            {
                cell.BackColor = GridView1.HeaderStyle.BackColor;
            }
            foreach (GridViewRow row in GridView1.Rows)
            {
                row.BackColor = System.Drawing.Color.White;
                foreach (TableCell cell in row.Cells)
                {
                    if (row.RowIndex % 2 == 0)
                    {
                        cell.BackColor = GridView1.AlternatingRowStyle.BackColor;
                    }
                    else
                    {
                        cell.BackColor = GridView1.RowStyle.BackColor;
                    }
                    cell.CssClass = "textmode";
                }
            }

            GridView1.RenderControl(hw);

            //style to format numbers to string
            string style = @"<style> .textmode { } </style>";
            Response.Write(style);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();
        }
    }
    protected void ExportToPDF(object sender, EventArgs e)
    {
        using (StringWriter sw = new StringWriter())
        {
            using (HtmlTextWriter hw = new HtmlTextWriter(sw))
            {
                //To Export all pages
                GridView1.AllowPaging = false;
                BindGridViewExport();

                GridView1.RenderControl(hw);
                StringReader sr = new StringReader(sw.ToString());
                Document pdfDoc = new Document(PageSize.A2, 10f, 10f, 10f, 0f);
                HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
                PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                pdfDoc.Open();
                htmlparser.Parse(sr);
                pdfDoc.Close();

                Response.ContentType = "application/pdf";
                Response.AddHeader("content-disposition", "attachment;filename=List_Active_Students.pdf");
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.Write(pdfDoc);
                Response.End();
            }
        }
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }

    protected void btnAdmin_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Homestay/Admin/Default.aspx");
    }

    protected void btnMain_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Homestay/Admin/Reports/ReportsHome.aspx");
    }

    private void BindGridViewExport()
    {
        //Add DetailsView to GridView
        DataTable dt = GetDataTable();
        dt.Columns.AddRange(new DataColumn[2]
        {
                 new DataColumn("Program", typeof(string)),
                 new DataColumn("Major", typeof(string))
        });

        foreach (DataRow dr in dt.Rows)
        {
            int studentID = Convert.ToInt32(dr["id"].ToString());
            DataTable dtDv = GetDvStudentByID(studentID);
            DataRow drDv = dtDv.Rows[0];

            dr["Program"] = drDv["Program"].ToString();
            dr["Major"] = drDv["Major"].ToString();

        }

        GridView1.DataSource = dt;
        GridView1.DataBind();
    }
    protected void rptHostFamily_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HyperLink lnkHostFamily = e.Item.FindControl("lnkHostFamily") as HyperLink;
            DataRowView drv = e.Item.DataItem as DataRowView;
            string studentID = drv.Row["applicantID"].ToString();
            string familyID = drv.Row["familyID"].ToString();
            lnkHostFamily.Text = drv.Row["placementName"].ToString();
            lnkHostFamily.NavigateUrl = "/Homestay/Admin/Matching/editplacement.aspx?studentID=" + studentID + "&familyID="+ familyID;
        }
    }
    protected void gvStudents_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        Repeater rptHostFamily = e.Row.FindControl("rptHostFamily") as Repeater;
        if (e.Row.RowIndex >= 0)
        {
            var lblid = e.Row.FindControl("lblid") as Label;
            int applicantID = Convert.ToInt32(lblid.Text);
            DataTable dtPlacement = GetDtPlacementByStudentID(applicantID);
            rptHostFamily.DataSource = dtPlacement;
            rptHostFamily.DataBind();
        }
    }
}