﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

/// <summary>
/// Summary description for CFEApplications
/// </summary>
public class CFEApplications
{
	public CFEApplications()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public DataSet GetCFEApplications(Int32 intID)
    {
        DataSet ds = new DataSet();
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCommand = new SqlCommand();
        SqlParameter param = new SqlParameter();
        SqlDataAdapter objDataAdapter;
        try
        {
            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CFEApplications"].ToString();
            objConn.Open();
            objCommand.CommandText = "usp_CFEApplications_Select";
            objCommand.CommandType = CommandType.StoredProcedure;
            objCommand.Connection = objConn;

            param.ParameterName = "@ID";
            param.SqlDbType = SqlDbType.Int;
            param.Value = intID;
            objCommand.Parameters.Add(param);

            objDataAdapter = new SqlDataAdapter(objCommand);
            objDataAdapter.Fill(ds);
        }
        finally
        {
            objConn.Close();
            objConn.Dispose();
            objCommand.Dispose();
        }
        return ds;
    }

    public string DeactivateApplication(Int32 intID)
    {
        string strSuccess = "Success";
        //usp_CFEApplications_Deactivate
        string strSQL = "usp_CFEApplications_Deactivate";
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCommand = new SqlCommand();
        SqlParameter param = new SqlParameter();

        param.ParameterName = "@ID";
        param.SqlDbType = SqlDbType.Int;
        param.Value = intID;
        objCommand.Parameters.Add(param);

        try
        {
            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CFEApplications"].ToString();
            objConn.Open();
            objCommand.Connection = objConn;
            objCommand.CommandText = strSQL;
            objCommand.CommandType = CommandType.StoredProcedure;
            objCommand.ExecuteNonQuery();
            //int intRows = objCommand.ExecuteNonQuery();
            //if (intRows > 0) { strSuccess = "Success"; }
        }
        catch (Exception ex)
        {
            //do nothing
            strSuccess = ex.Message + "<br /> SQL: " + strSQL + "<br />";
        }
        finally
        {
            objConn.Close();
            objConn.Dispose();
            objCommand.Dispose();
        }
        return strSuccess;
    }

    public string InsertCFEApplication(string Name, string Address, string City, string State, string Zip, string Email, string PhoneArea, 
        string PhonePrefix, string PhoneSuffix, string HighSchool, string College, string Certificates, string Certifications, string Degrees, string ProfAwards, 
        string CommInvolvement, string CivicHonors, string Activities, string EmploymentHistory, string BusinessIdea, string TwoYearProjection, 
        string TenYearOutlook, string Utilization, string SID, string ReferredBy)
    {

        string strSuccess = "Success";
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCommand = new SqlCommand();
        SqlParameter param = new SqlParameter();

        param.ParameterName = "@Name";
        param.SqlDbType = SqlDbType.VarChar;
        param.Size = 50;
        param.Value = Name;
        objCommand.Parameters.Add(param);

        param = new SqlParameter();
        param.ParameterName = "@Address";
        param.SqlDbType = SqlDbType.VarChar;
        param.Size = 50;
        param.Value = Address;
        objCommand.Parameters.Add(param);

        param = new SqlParameter();
        param.ParameterName = "@City";
        param.SqlDbType = SqlDbType.VarChar;
        param.Size = 25;
        param.Value = City;
        objCommand.Parameters.Add(param);

        param = new SqlParameter();
        param.ParameterName = "@State";
        param.SqlDbType = SqlDbType.VarChar;
        param.Size = 15;
        param.Value = State;
        objCommand.Parameters.Add(param);

        param = new SqlParameter();
        param.ParameterName = "@Zip";
        param.SqlDbType = SqlDbType.VarChar;
        param.Size = 10;
        param.Value = Zip;
        objCommand.Parameters.Add(param);

        param = new SqlParameter();
        param.ParameterName = "@Email";
        param.SqlDbType = SqlDbType.VarChar;
        param.Size = 50;
        param.Value = Email;
        objCommand.Parameters.Add(param);

        param = new SqlParameter();
        param.ParameterName = "@PhoneArea";
        param.SqlDbType = SqlDbType.VarChar;
        param.Size = 3;
        param.Value = PhoneArea;
        objCommand.Parameters.Add(param);

        param = new SqlParameter();
        param.ParameterName = "@PhonePrefix";
        param.SqlDbType = SqlDbType.VarChar;
        param.Size = 3;
        param.Value = PhonePrefix;
        objCommand.Parameters.Add(param);

        param = new SqlParameter();
        param.ParameterName = "@PhoneSuffix";
        param.SqlDbType = SqlDbType.VarChar;
        param.Size = 4;
        param.Value = PhoneSuffix;
        objCommand.Parameters.Add(param);

        param = new SqlParameter();
        param.ParameterName = "@HighSchool";
        param.SqlDbType = SqlDbType.VarChar;
        param.Size = 100;
        param.Value = HighSchool;
        objCommand.Parameters.Add(param);

        param = new SqlParameter();
        param.ParameterName = "@College";
        param.SqlDbType = SqlDbType.VarChar;
        param.Size = 150;
        param.Value = College;
        objCommand.Parameters.Add(param);

        param = new SqlParameter();
        param.ParameterName = "@Certificates";
        param.SqlDbType = SqlDbType.VarChar;
        param.Size = 225;
        param.Value = Certificates;
        objCommand.Parameters.Add(param);

        param = new SqlParameter();
        param.ParameterName = "@Certifications";
        param.SqlDbType = SqlDbType.VarChar;
        param.Size = 420;
        param.Value = Certifications;
        objCommand.Parameters.Add(param);

        param = new SqlParameter();
        param.ParameterName = "@Degrees";
        param.SqlDbType = SqlDbType.VarChar;
        param.Size = 100;
        param.Value = Degrees;
        objCommand.Parameters.Add(param);

        param = new SqlParameter();
        param.ParameterName = "@ProfAwards";
        param.SqlDbType = SqlDbType.VarChar;
        param.Size = 5000;
        param.Value = ProfAwards;
        objCommand.Parameters.Add(param);

        param = new SqlParameter();
        param.ParameterName = "@CommInvolvement";
        param.SqlDbType = SqlDbType.VarChar;
        param.Size = 5000;
        param.Value = CommInvolvement;
        objCommand.Parameters.Add(param);

        param = new SqlParameter();
        param.ParameterName = "@CivicHonors";
        param.SqlDbType = SqlDbType.VarChar;
        param.Size = 5000;
        param.Value = CivicHonors;
        objCommand.Parameters.Add(param);

        param = new SqlParameter();
        param.ParameterName = "@Activities";
        param.SqlDbType = SqlDbType.Text;
        param.Value = Activities;
        objCommand.Parameters.Add(param);

        param = new SqlParameter();
        param.ParameterName = "@EmploymentHistory";
        param.SqlDbType = SqlDbType.Text;
        param.Value = EmploymentHistory;
        objCommand.Parameters.Add(param);

        param = new SqlParameter();
        param.ParameterName = "@BusinessIdea";
        param.SqlDbType = SqlDbType.Text;
        param.Value = BusinessIdea;
        objCommand.Parameters.Add(param);

        param = new SqlParameter();
        param.ParameterName = "@TwoYearProjection";
        param.SqlDbType = SqlDbType.Text;
        param.Value = TwoYearProjection;
        objCommand.Parameters.Add(param);

        param = new SqlParameter();
        param.ParameterName = "@TenYearOutlook";
        param.SqlDbType = SqlDbType.Text;
        param.Value = TenYearOutlook;
        objCommand.Parameters.Add(param);

        param = new SqlParameter();
        param.ParameterName = "@Utilization";
        param.SqlDbType = SqlDbType.Text;
        param.Value = Utilization;
        objCommand.Parameters.Add(param);
        
        param = new SqlParameter();
        param.ParameterName = "@Deleted";
        param.SqlDbType = SqlDbType.Bit;
        param.Value = 0;
        objCommand.Parameters.Add(param);

        param = new SqlParameter();
        param.ParameterName = "@SID";
        param.SqlDbType = SqlDbType.VarChar;
        param.Size = 11;
        param.Value = SID;
        objCommand.Parameters.Add(param);

        param = new SqlParameter();
        param.ParameterName = "@ReferredBy";
        param.SqlDbType = SqlDbType.VarChar;
        param.Size = 1000;
        param.Value = ReferredBy;
        objCommand.Parameters.Add(param);

        try
        {
            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CFEApplications"].ToString();
            objConn.Open();
            objCommand.Connection = objConn;
            objCommand.CommandText = "usp_CFEAppilcations_Insert";
            objCommand.CommandType = CommandType.StoredProcedure;
            objCommand.ExecuteNonQuery();
        }
        catch (Exception ex)
        {
            strSuccess = ex.Message;
        }
        finally
        {
            objConn.Close();
            objConn.Dispose();
            objCommand.Dispose();
        }


        return strSuccess;
    }
}