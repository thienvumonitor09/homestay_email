﻿<%@ Page Language="C#" Title="New Student Placement" EnableEventValidation="false" AutoEventWireup="true" MasterPageFile="~/CCSApps.master" CodeFile="PlacementForm_V.aspx.cs" Inherits="Homestay_Admin_Reports_PlacementForm_V" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
<style type="text/css">
    p
    {
        padding:2px;
        margin:0px;
    }
    .editClass{
        width:80%;
    }
</style>
    <link href="/Homestay/_css/Print.css" rel="stylesheet" type="text/css" media="print" />
    <script src="/Homestay/_js/ckeditor/ckeditor.js"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="bannerH1Text" Runat="Server">
    Homestay Administration
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">
     
<div style="width:100%;text-align:center;margin-bottom:50px;">
    <div style="width:30%;float:left">
        <img src="/Homestay/images/ccs-logo.jpg" style="height: 104px; width: 211px" />
    </div>
    <div style="width:70%">
        <h1><b>Global Education Homestay Program</b></h1>
        <h2>New Student Placement</h2>
    </div>
    
    

</div>
    
<div id="divHome" class="noPrint" runat="server" style="float:right;margin-right:30px;">
    <asp:Button  ID="btnHome" Text="Return to Admin Home Page" runat="server" class="ButtonAdmin" OnClick="linkBtnHome_Click"/>&nbsp;&nbsp;
</div>
    
   



    <asp:Literal ID="litResutlMsg" runat="server"></asp:Literal>
    
    <div id="divStudentdd" class="noPrint" runat="server" visible="true">
        <h2>Select a Student</h2>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:DropDownList ID="ddlStudent" runat="server"  style="margin-top:5px;"></asp:DropDownList>
                <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" CssClass="noPrint" />
             </ContentTemplate>
            
        </asp:UpdatePanel>
       
        
    </div>

    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
             <p aria-autocomplete="both">
    <asp:DetailsView ID="dvPlacement" runat="server" 
        AutoGenerateRows="False" CellPadding="10" Width="100%" Font-Size="Large"
        OnItemUpdating="DetailsViewExample_ItemUpdating"
        OnModeChanging="DetailsViewExample_ModeChanging"
        OnItemCommand="DetailsViewExample_ItemCommand1"
        OnDataBound="DetailsViewExample_DataBound" Visible="true">
         <FieldHeaderStyle BackColor="#E4E4E4"  Font-Bold="True" Height="50px" Width="30%" />
         <RowStyle BorderStyle="Solid" BorderWidth="2px" />
        <Fields>
            <asp:TemplateField HeaderText="Campus">
                <ItemTemplate>
                    <asp:Label ID="lblCampus" Text='<%# Bind("Campus") %>' runat="server" ></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Quarter Of Arrival">
                <ItemTemplate>
                    <asp:Label ID="lblQuarterOfArrival" Text='<%# Bind("QuarterOfArrival") %>' runat="server" ></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Orientation Start Date">
                <ItemTemplate>
                    <asp:Label ID="lblOrientationStartDate" Text='<%# Bind("OrientationStartDate") %>' runat="server"></asp:Label>
                </ItemTemplate>
                 <EditItemTemplate>
                    <asp:TextBox ID="txtOrientationStartDate" Text='<%# Bind("OrientationStartDate") %>' runat="server" Cssclass="editClass"></asp:TextBox>
                 </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Quarter Start Date">
                <ItemTemplate>
                    <asp:Label ID="lblQuarterStartDate" Text='<%# Bind("QuarterStartDate") %>' runat="server" ></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txtQuarterStartDate" Text='<%# Bind("QuarterStartDate") %>' runat="server" Cssclass="editClass"></asp:TextBox>
                 </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Student Name">
                <ItemTemplate>
                    <asp:Label ID="lblStudentName" Text='<%# Bind("StudentName") %>' runat="server" ></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Student Gender">
                <ItemTemplate>
                    <asp:Label ID="lblStudentGender" Text='<%# Bind("StudentGender") %>' runat="server" ></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Date of Birth">
                <ItemTemplate>
                    <asp:Label ID="lblDOB" Text='<%# Bind("DOB") %>' runat="server" ></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Country">
                <ItemTemplate>
                    <asp:Label ID="lblCountry" Text='<%# Bind("Country") %>' runat="server" ></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Program Of Study">
                <ItemTemplate>
                    <asp:Label ID="lblProgramOfStudy" Text='<%# Bind("ProgramOfStudy") %>' runat="server" ></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Student Email Address">
                <ItemTemplate>
                    <asp:Label ID="lblEmail" Text='<%# Bind("Email") %>' runat="server" ></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Hobbies and Interests">
                <ItemTemplate>
                    <asp:Label ID="lblOther" Text='<%# Bind("Hobbies") %>' runat="server" ></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
           <asp:TemplateField HeaderText="Airport Arrival Info">
                <ItemTemplate>
                    <asp:Label ID="lblArrivalInfo" Text='<%# Bind("ArrivalInfo") %>' runat="server" ></asp:Label>
                </ItemTemplate>
               <EditItemTemplate>
                    <asp:TextBox ID="txtArrivalInfo" Text='<%# Bind("ArrivalInfo") %>' runat="server"  Cssclass="editClass"></asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Urgent Care: covered by Student Insurance (Lewermark)">
                <ItemTemplate>
                    <asp:Label ID="lblUrgentCare" Text='<%# Bind("UrgentCare") %>' runat="server" ></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txtUrgentCare" Text='<%# Bind("UrgentCare") %>' runat="server"  Cssclass="editClass"></asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField>
           
            
            <asp:CommandField ShowEditButton="true"  ItemStyle-CssClass="noPrint" />
        </Fields>
        
    </asp:DetailsView>
                  <asp:Label ID="lblInfo" runat="server" Text=""/>
             
</p>  
             From: <br /><asp:TextBox ID="txtFrom" runat="server" Text="Internationalhomestay@ccs.spokane.edu" Width="500px" Enabled="false"/> <br />
                 To: <br /><asp:TextBox ID="txtTo" runat="server" Width="500px"/> <br />
                 CC: <br /><asp:TextBox ID="txtCC" runat="server" Width="500px"/> <br />
            
           
           
          
            <!-- onclientclick="return OpenMailWindow();"  xmlns:asp="#unknown"
                onClientClick="javascript:return confirm('Are you sure you want to delete this user?');"
                -->
            
          
            
            
            <br />
              
            <asp:Literal ID="litResultMsg" runat="server"/>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" DynamicLayout="true" AssociatedUpdatePanelID="UpdatePanel2" DisplayAfter="500" >

       <ProgressTemplate>
          <img src="/Homestay/images/demo_wait.gif" />
           Please Wait...
       </ProgressTemplate>
   
    </asp:UpdateProgress>
    <asp:button id="btnExport" Cssclass="noPrint" runat="server" Onclick="ExportGridToPDF" Text="Export to PDF" />
   
   
    <p><input type="button" name="btnPrint" class="noPrint" onclick="javascript:window.print();" value="Print" /></p>
    <script>
        $.ajax({
            type: "POST",
            url: "PlacementForm_V.aspx/GetString",
            //data: dataValue,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert("Request: " + XMLHttpRequest.toString() + "\n\nStatus: " + textStatus + "\n\nError: " + errorThrown);
            },
            success: function (result) {
                //alert("We returned: " + result.d);
            }
        });
    </script>
    <script>
        function OpenMailWindow()
        {
            //var b=document.getElementById("dvPlacement");
            
            var detailsview = document.getElementById('<%= dvPlacement.ClientID %>');
            //var c = detailsview.getElementByTagName("Campus");
            var toEmail = "to@gmail.com";
            var fromEmail = "from@gmail.com"
            var bodyTemplate = "bodyTemplate";
            var subject = "subject";
            window.open("mailto:" +toEmail +"?cc="+ fromEmail +"&bcc=user@example.com&subject=" + subject + "&body=" + bodyTemplate);
            return false;
        }
    </script>
    <script>
       
                // Replace the <textarea id="editor1"> with a CKEditor
                // instance, using default configuration.
                
            </script>
</asp:Content>
