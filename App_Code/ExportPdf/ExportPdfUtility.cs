﻿using iTextSharp.text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ExportPdfUtility
/// </summary>
public class ExportPdfUtility
{
    public ExportPdfUtility()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    //Create space between 2 phrase
    public Phrase GetSpace()
    {
        Phrase p = new Phrase();
        p.Add(new Chunk("           "));
        return p;
    }

    public Phrase GetPhrase(string title, string content, bool lastPhrase)
    {
        Font bold = FontFactory.GetFont("Arial", 10, Font.BOLD);
        Font normal = FontFactory.GetFont("Arial", 10);

        Phrase p = new Phrase();
        //title = (title.Equals("")) ? title : title + ": ";
        p.Add(new Chunk(title, bold));
        content = (lastPhrase) ? content + "\n" : content + "            ";
        p.Add(new Chunk(content, normal));

        return p;
    }

    public string[] GetDiets()
    {
        return new string[] { "Vegetarian", "GlutenFree", "DairyFree", "Halal", "OtherFoodAllergy", "Kosher", "NoSpecialDiet" };
    }

    public string[] GetHobbies()
    {
        return new string[] { "MusicalInstrument", "Art","TeamSports", "IndividualSports", "ListenMusic", "Drama"
                                                    ,"WatchMovies","Singing" ,"Shopping","ReadBooks","Outdoors" ,"Cooking", "Photography"
                                                    , "Gaming"};
    }

    public string GetContent(Dictionary<string, string> hm, string contentType)
    {
        string[] contentList = null;
        if (contentType.Equals("diets"))
        {
            contentList = GetDiets();
        }else if (contentType.Equals("hobbies"))
        {
            contentList = GetHobbies();
        }
        string content = "";
        foreach (string s in contentList)
        {
            string selection = GetPreferenceSelection(hm, s);

            if (!String.IsNullOrEmpty(selection))
            {
                content += selection + ", ";
            }
        }
        return content;
    }

    public string GetPreferenceSelection(Dictionary<string, string> hm, string selection)
    {
        string result = "";
        if (hm.TryGetValue(selection, out result))
        {
            if (result.Contains("Interaction") || result.Contains("Home")) //remove "Interaction" or "Home"
            {
                result = result.Substring(result.IndexOf(" ")).Trim();
            }
            return result;
        }
        
        return result;

    }
    public string GetShortDateString(string dateTimeString)
    {
        if (String.IsNullOrEmpty(dateTimeString))
        {
            return dateTimeString;
        }
        else
        {
            return Convert.ToDateTime(dateTimeString).ToShortDateString();
        }
    }
}