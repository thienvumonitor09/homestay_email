﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
public partial class Homestay_Families_ApplicationForm_V : System.Web.UI.Page
{
    string strResultMsg = "";
    Int32 intResult = 0;
    Int32 intFamilyID = 0;
    Boolean blLicenseSubmitted = false;
    Boolean blIsAdmin = true;
    Int32 intMainLevelRooms = 0;
    Int32 intUpstairsRooms = 0;
    Int32 intBasementRooms = 0;
    String strRelationship = "";
    String strUploadURL = @"\\ccs-internet\InternetContent\CCS\Homestay\";
    String strUploadServer = "";
    Homestay hsInfo = new Homestay();
    Quarter_MetaData QMD = new Quarter_MetaData();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["ID"] != null)
        {
            ViewState["intFamilyID"] = Request.QueryString["ID"].ToString();
            intFamilyID = Int32.Parse(Request.QueryString["ID"].ToString());
        }

        ddlQuarterStart1.Visible = (rdoRoom1Occupancy.SelectedIndex == 1) ? true : false;
        ddlQuarterStart2.Visible = (rdoRoom2Occupancy.SelectedIndex == 1) ? true : false;
        ddlQuarterStart3.Visible = (rdoRoom3Occupancy.SelectedIndex == 1) ? true : false;
        ddlQuarterStart4.Visible = (rdoRoom4Occupancy.SelectedIndex == 1) ? true : false;
        if (!IsPostBack)
        {
            DataTable dtQuarters = QMD.GetRangeRows_Quarter_MetaData_Base(-3, 15);
            //System.Web.UI.WebControls.ListItem LI2 = null;
            if (dtQuarters != null)
            {
                if (dtQuarters.Rows.Count > 0)
                {

                    //LI2 = new System.Web.UI.WebControls.ListItem();
                    //LI2.Value = "";
                    //LI2.Text = "--Select avaialable start date--";
                    ddlQuarterStart1.Items.Add(new System.Web.UI.WebControls.ListItem("--Select avaialable start date--", ""));
                    ddlQuarterStart2.Items.Add(new System.Web.UI.WebControls.ListItem("--Select avaialable start date--", ""));
                    ddlQuarterStart3.Items.Add(new System.Web.UI.WebControls.ListItem("--Select avaialable start date--", ""));
                    ddlQuarterStart4.Items.Add(new System.Web.UI.WebControls.ListItem("--Select avaialable start date--", ""));
                    foreach (DataRow dr in dtQuarters.Rows)
                    {
                        //LI2 = new System.Web.UI.WebControls.ListItem();
                        string ddlQuarterValue = dr["Name"].ToString() + " " + Convert.ToDateTime(dr["FirstDayOfClass"]).Year.ToString();
                        string ddlQuarterText = dr["Name"].ToString() + " " + String.Format("{0:MM/dd/yyyy}", Convert.ToDateTime(dr["FirstDayOfClass"]));

                        //LI2.Value = dr["Name"].ToString() + " " + Convert.ToDateTime(dr["FirstDayOfClass"]).Year.ToString();
                        //LI2.Text = dr["Name"].ToString() + " " Convert.ToDateTime(dr["FirstDayOfClass"]).Year.ToString();
                        ddlQuarterStart1.Items.Add(new System.Web.UI.WebControls.ListItem(ddlQuarterText, ddlQuarterValue));
                        ddlQuarterStart2.Items.Add(new System.Web.UI.WebControls.ListItem(ddlQuarterText, ddlQuarterValue));
                        ddlQuarterStart3.Items.Add(new System.Web.UI.WebControls.ListItem(ddlQuarterText, ddlQuarterValue));
                        ddlQuarterStart4.Items.Add(new System.Web.UI.WebControls.ListItem(ddlQuarterText, ddlQuarterValue));
                    }
                }
            }


            // populate the family drop-down
            DataTable dtFamilies = hsInfo.GetHomestayFamilyInfo(0, "", "DDLactive");
            if (dtFamilies != null)
            {
                if (dtFamilies.Rows.Count > 0)
                {
                    //ddlFamilies.Items.Clear();
                    System.Web.UI.WebControls.ListItem LI;
                    LI = new System.Web.UI.WebControls.ListItem();
                    LI.Value = "0";
                    LI.Text = "-- Add New Family --";
                    ddlFamilies.Items.Add(LI);
                    foreach (DataRow dr in dtFamilies.Rows)
                    {
                        LI = new System.Web.UI.WebControls.ListItem();
                        LI.Value = dr["id"].ToString();
                        LI.Text = dr["homeName"].ToString();
                        ddlFamilies.Items.Add(LI);
                    }
                }
                else { strResultMsg += "Error: dtFamilies has no rows.<br />"; }
                if (Request.QueryString["ID"] != null && Request.QueryString["ID"].ToString() != "0")
                {
                    btnGenerate.Visible = true;
                    ddlFamilies.SelectedValue = Request.QueryString["ID"].ToString();
                }
            }

            DataTable dtFamily = hsInfo.GetHomestayFamilyInfo(intFamilyID, "", "ID");
            if (dtFamily != null)
            {
                if (dtFamily.Rows.Count > 0)
                {
                    userName.Text = dtFamily.Rows[0]["userName"].ToString().Trim();
                    homeName.Text = dtFamily.Rows[0]["homeName"].ToString().Trim();
                    streetAddress.Text = dtFamily.Rows[0]["streetAddress"].ToString().Trim();
                    city.Text = dtFamily.Rows[0]["city"].ToString().Trim();
                    state.Text = dtFamily.Rows[0]["state"].ToString().Trim();
                    ZIP.Text = dtFamily.Rows[0]["ZIP"].ToString().Trim();
                    walkingBusStopMinutes.Text = dtFamily.Rows[0]["walkingBusStopMinutes"].ToString().Trim();
                    walkingBusStopMiles.Text = dtFamily.Rows[0]["walkingBusStopMiles"].ToString().Trim();
                    walkingBusStopBlocks.Text = dtFamily.Rows[0]["walkingBusStopBlocks"].ToString().Trim();
                    busMinutesSCC.Text = dtFamily.Rows[0]["busMinutesSCC"].ToString().Trim();
                    busMinutesSFCC.Text = dtFamily.Rows[0]["busMinutesSFCC"].ToString().Trim();
                    if (dtFamily.Rows[0]["collegeQualified"].ToString().Trim() == "")
                    {
                        collegeQualified.SelectedIndex = -1;
                    }
                    else
                    {
                        collegeQualified.SelectedValue = dtFamily.Rows[0]["collegeQualified"].ToString().Trim();
                    }
                    landLinePhone.Text = dtFamily.Rows[0]["landlinePhone"].ToString().Trim();
                    insuranceCompanyName.Text = dtFamily.Rows[0]["insuranceCompanyName"].ToString().Trim();
                    insurancePolicyNumber.Text = dtFamily.Rows[0]["insurancePolicyNumber"].ToString().Trim();
                    insuranceAgentName.Text = dtFamily.Rows[0]["insuranceAgentName"].ToString().Trim();
                    insuranceAgentPhone.Text = dtFamily.Rows[0]["insuranceAgentPhone"].ToString().Trim();
                    whyHost.Text = dtFamily.Rows[0]["whyHost"].ToString().Trim();
                    hostExperience.Text = dtFamily.Rows[0]["hostExperience"].ToString().Trim();
                    primaryLanguage.Text = dtFamily.Rows[0]["primaryLanguage"].ToString().Trim();
                    otherLanguages.Text = dtFamily.Rows[0]["otherLanguages"].ToString().Trim();
                    religiousActivities.Text = dtFamily.Rows[0]["religiousActivities"].ToString().Trim();
                    smokingGuidelines.Text = dtFamily.Rows[0]["smokingGuidelines"].ToString().Trim();
                    petsInHome.Text = dtFamily.Rows[0]["petsInHome"].ToString().Trim();
                    generalLifestyle.Text = dtFamily.Rows[0]["generalLifestyle"].ToString().Trim();
                    otherSchool.Text = dtFamily.Rows[0]["otherSchool"].ToString().Trim();
                    additionalExpectations.Text = dtFamily.Rows[0]["additionalExpectations"].ToString().Trim();
                    Boolean blIsActive = Convert.ToBoolean(dtFamily.Rows[0]["IsActive"]);
                    if (blIsActive) { rdoIsActive.SelectedValue = "1"; } else { rdoIsActive.SelectedValue = "0"; }
                    familyStatus.Text = dtFamily.Rows[0]["familyStatus"].ToString().Trim();
                    blLicenseSubmitted = Convert.ToBoolean(dtFamily.Rows[0]["licenseCopiesSubmitted"]);
                    howReferred.Text = dtFamily.Rows[0]["howHearHomestay"].ToString();
                    litLicenseFile.Text = "";
                    //if (blLicenseSubmitted)
                    //{

                    Configuration config = WebConfigurationManager.OpenWebConfiguration(Request.ApplicationPath);
                    AppSettingsSection appSettings = (AppSettingsSection)config.GetSection("appSettings");
                    strUploadServer = "/InternetContent" + appSettings.Settings["HomestayShare"].Value;
                    litLicenseFile.Text = "";
                    DataTable dt = hsInfo.GetUploadedFiles(intFamilyID);
                    if (dt != null)
                    {
                        if (dt.Rows.Count > 0)
                        {
                            foreach (DataRow dr in dt.Rows)
                            {
                                string strFileName = dr["fileName"].ToString();
                                if (strFileName.Contains("License"))
                                {
                                    //get the file name
                                    litLicenseFile.Text += "<br /><b>License copies submitted:</b> " + "<a href=\"" + strUploadServer + strFileName + "\" target=\"_blank\">" + strFileName + "</a>";
                                }
                            }
                        }
                    }
                    /*
                    ViewState["userName"] = userName.Text.Replace("'", "''");
                    ViewState["homeName"] = homeName.Text.Replace("'", "''");
                    ViewState["streetAddress"] = streetAddress.Text.Replace("'", "''");
                    ViewState["city"] = city.Text.Replace("'", "''");
                    ViewState["state"] = state.Text.Replace("'", "''");
                    ViewState["ZIP"] = ZIP.Text.Replace("'", "''");
                    ViewState["walkingBusStopMinutes"] = walkingBusStopMinutes.Text.Replace("'", "''");
                    ViewState["walkingBusStopMiles"] = walkingBusStopMiles.Text.Replace("'", "''");
                    ViewState["walkingBusStopBlocks"] = walkingBusStopBlocks.Text.Replace("'", "''");
                    ViewState["busMinutesSCC"] = busMinutesSCC.Text.Replace("'", "''");
                    ViewState["busMinutesSFCC"] = busMinutesSFCC.Text.Replace("'", "''");
                    */
                    //collegeQualified.SelectedValue = dtFamily.Rows[0]["collegeQualified"].ToString().Trim();
                    rdoRoom1.SelectedIndex = -1;
                    rdoRoom1.SelectedValue = dtFamily.Rows[0]["room1Level"].ToString();
                    room1DateOpen.Text = "";
                    if (!DBNull.Value.Equals(dtFamily.Rows[0]["room1StartDate"]))
                    {
                        room1DateOpen.Text = Convert.ToDateTime(dtFamily.Rows[0]["room1StartDate"]).ToShortDateString();
                        if (room1DateOpen.Text == "1/1/1900")
                            room1DateOpen.Text = "";
                    }
                    room1DateClosed.Text = "";
                    if (!DBNull.Value.Equals(dtFamily.Rows[0]["room1EndDate"]))
                    {
                        room1DateClosed.Text = Convert.ToDateTime(dtFamily.Rows[0]["room1EndDate"]).ToShortDateString();
                        if (room1DateClosed.Text == "1/1/1900")
                            room1DateClosed.Text = "";
                    }
                    rdoRoom1Bathroom.SelectedIndex = -1;
                    rdoRoom1Occupancy.SelectedIndex = -1;
                    rdoRoom1Bathroom.SelectedValue = dtFamily.Rows[0]["room1Bath"].ToString();
                    rdoRoom1Occupancy.Text = dtFamily.Rows[0]["room1Occupancy"].ToString();
                    if (dtFamily.Rows[0]["room1Occupancy"].ToString().Any(char.IsDigit))
                    {
                        rdoRoom1Occupancy.Text = "Planning";
                        ddlQuarterStart1.Visible = true;
                        ddlQuarterStart1.Text = dtFamily.Rows[0]["room1Occupancy"].ToString(); ;
                    }


                    rdoRoom2.SelectedIndex = -1;
                    rdoRoom2.SelectedValue = dtFamily.Rows[0]["room2Level"].ToString();
                    room2DateOpen.Text = "";
                    if (!DBNull.Value.Equals(dtFamily.Rows[0]["room2StartDate"]))
                    {
                        room2DateOpen.Text = Convert.ToDateTime(dtFamily.Rows[0]["room2StartDate"]).ToShortDateString();
                        if (room2DateOpen.Text == "1/1/1900")
                            room2DateOpen.Text = "";
                    }
                    room2DateClosed.Text = "";
                    if (!DBNull.Value.Equals(dtFamily.Rows[0]["room2EndDate"]))
                    {
                        room2DateClosed.Text = Convert.ToDateTime(dtFamily.Rows[0]["room2EndDate"]).ToShortDateString();
                        if (room2DateClosed.Text == "1/1/1900")
                            room2DateClosed.Text = "";
                    }
                    rdoRoom2Bathroom.SelectedIndex = -1;
                    rdoRoom2Bathroom.SelectedValue = dtFamily.Rows[0]["room2Bath"].ToString();
                    rdoRoom2Occupancy.SelectedIndex = -1;
                    rdoRoom2Occupancy.Text = dtFamily.Rows[0]["room2Occupancy"].ToString();
                    if (dtFamily.Rows[0]["room2Occupancy"].ToString().Any(char.IsDigit))
                    {
                        rdoRoom2Occupancy.Text = "Planning";
                        ddlQuarterStart2.Visible = true;
                        ddlQuarterStart2.Text = dtFamily.Rows[0]["room2Occupancy"].ToString();
                    }

                    rdoRoom3.SelectedIndex = -1;
                    rdoRoom3.SelectedValue = dtFamily.Rows[0]["room3Level"].ToString();
                    room3DateOpen.Text = "";
                    if (!DBNull.Value.Equals(dtFamily.Rows[0]["room3StartDate"]))
                    {
                        room3DateOpen.Text = Convert.ToDateTime(dtFamily.Rows[0]["room3StartDate"]).ToShortDateString();
                        if (room3DateOpen.Text == "1/1/1900")
                            room3DateOpen.Text = "";
                    }
                    room3DateClosed.Text = "";
                    if (!DBNull.Value.Equals(dtFamily.Rows[0]["room3EndDate"]))
                    {
                        room3DateClosed.Text = Convert.ToDateTime(dtFamily.Rows[0]["room3EndDate"]).ToShortDateString();
                        if (room3DateClosed.Text == "1/1/1900")
                            room3DateClosed.Text = "";
                    }
                    rdoRoom3Bathroom.SelectedIndex = -1;
                    rdoRoom3Bathroom.SelectedValue = dtFamily.Rows[0]["room3Bath"].ToString();
                    rdoRoom3Occupancy.SelectedIndex = -1;
                    rdoRoom3Occupancy.Text = dtFamily.Rows[0]["room3Occupancy"].ToString();
                    if (dtFamily.Rows[0]["room3Occupancy"].ToString().Any(char.IsDigit))
                    {
                        rdoRoom3Occupancy.Text = "Planning";
                        ddlQuarterStart3.Visible = true;
                        ddlQuarterStart3.Text = dtFamily.Rows[0]["room3Occupancy"].ToString();
                    }

                    rdoRoom4.SelectedIndex = -1;
                    rdoRoom4.SelectedValue = dtFamily.Rows[0]["room4Level"].ToString();
                    room4DateOpen.Text = "";
                    if (!DBNull.Value.Equals(dtFamily.Rows[0]["room4StartDate"]))
                    {
                        room4DateOpen.Text = Convert.ToDateTime(dtFamily.Rows[0]["room4StartDate"]).ToShortDateString();
                        if (room4DateOpen.Text == "1/1/1900")
                            room4DateOpen.Text = "";
                    }
                    room4DateClosed.Text = "";
                    if (!DBNull.Value.Equals(dtFamily.Rows[0]["room4EndDate"]))
                    {
                        room4DateClosed.Text = Convert.ToDateTime(dtFamily.Rows[0]["room4EndDate"]).ToShortDateString();
                        if (room4DateClosed.Text == "1/1/1900")
                            room4DateClosed.Text = "";
                    }
                    rdoRoom4Bathroom.SelectedIndex = -1;
                    rdoRoom4Occupancy.SelectedIndex = -1;
                    rdoRoom4Bathroom.SelectedValue = dtFamily.Rows[0]["room4Bath"].ToString();
                    rdoRoom4Occupancy.Text = dtFamily.Rows[0]["room4Occupancy"].ToString();
                    if (dtFamily.Rows[0]["room4Occupancy"].ToString().Any(char.IsDigit))
                    {
                        rdoRoom4Occupancy.Text = "Planning";
                        ddlQuarterStart4.Visible = true;
                        ddlQuarterStart4.Text = dtFamily.Rows[0]["room4Occupancy"].ToString();
                    }
                }
                
            }
            DataTable dtRelatives = hsInfo.GetHomestayRelatives(0, intFamilyID);
            if (dtRelatives != null)
            {
                if (dtRelatives.Rows.Count > 0)
                {
                    foreach (DataRow dtRow in dtRelatives.Rows)
                    {
                        strRelationship = dtRow["relationship"].ToString().Trim();
                        //strResultMsg += "Relationship: " + strRelationship + "<br />";
                        switch (strRelationship)
                        {
                            case "Primary Contact":
                                familyName.Text = dtRow["familyName"].ToString().Trim();
                                firstName.Text = dtRow["firstName"].ToString().Trim();
                                middleName.Text = dtRow["middleName"].ToString().Trim();
                                rdoGender.SelectedValue = dtRow["gender"].ToString().Trim();
                                DOB.Text = Convert.ToDateTime(dtRow["DOB"]).ToShortDateString();
                                driversLicenseNumber.Text = dtRow["driversLicenseNumber"].ToString().Trim();
                                occupation.Text = dtRow["occupation"].ToString().Trim();
                                cellPhone.Text = dtRow["cellPhone"].ToString().Trim();
                                workPhone.Text = dtRow["workPhone"].ToString().Trim();
                                email.Text = dtRow["email"].ToString().Trim();
                                hdnPrimaryID.Value = dtRow["id"].ToString();
                                break;
                            case "Secondary Contact":
                                familyName2.Text = dtRow["familyName"].ToString().Trim();
                                firstName2.Text = dtRow["firstName"].ToString().Trim();
                                middleName2.Text = dtRow["middleName"].ToString().Trim();
                                rdoGender2.SelectedValue = dtRow["gender"].ToString().Trim();
                                DOB2.Text = Convert.ToDateTime(dtRow["DOB"]).ToShortDateString();
                                driversLicenseNumber2.Text = dtRow["driversLicenseNumber"].ToString().Trim();
                                occupation2.Text = dtRow["occupation"].ToString().Trim();
                                cellPhone2.Text = dtRow["cellPhone"].ToString().Trim();
                                workPhone2.Text = dtRow["workPhone"].ToString().Trim();
                                email2.Text = dtRow["email"].ToString().Trim();
                                hdnSecondaryID.Value = dtRow["id"].ToString();
                                break;
                            case "Other Adult 1":
                                familyName3.Text = dtRow["familyName"].ToString().Trim();
                                firstName3.Text = dtRow["firstName"].ToString().Trim();
                                middleName3.Text = dtRow["middleName"].ToString().Trim();
                                rdoGender3.SelectedValue = dtRow["gender"].ToString().Trim();
                                DOB3.Text = Convert.ToDateTime(dtRow["DOB"]).ToShortDateString();
                                driversLicenseNumber3.Text = dtRow["driversLicenseNumber"].ToString().Trim();
                                occupation3.Text = dtRow["occupation"].ToString().Trim();
                                cellPhone3.Text = dtRow["cellPhone"].ToString().Trim();
                                workPhone3.Text = dtRow["workPhone"].ToString().Trim();
                                email3.Text = dtRow["email"].ToString().Trim();
                                hdnAdult1ID.Value = dtRow["id"].ToString();
                                break;
                            case "Other Adult 2":
                                familyName4.Text = dtRow["familyName"].ToString().Trim();
                                firstName4.Text = dtRow["firstName"].ToString().Trim();
                                middleName4.Text = dtRow["middleName"].ToString().Trim();
                                rdoGender4.SelectedValue = dtRow["gender"].ToString().Trim();
                                DOB4.Text = Convert.ToDateTime(dtRow["DOB"]).ToShortDateString();
                                driversLicenseNumber4.Text = dtRow["driversLicenseNumber"].ToString().Trim();
                                occupation4.Text = dtRow["occupation"].ToString().Trim();
                                cellPhone4.Text = dtRow["cellPhone"].ToString().Trim();
                                workPhone4.Text = dtRow["workPhone"].ToString().Trim();
                                email4.Text = dtRow["email"].ToString().Trim();
                                hdnAdult2ID.Value = dtRow["id"].ToString();
                                break;
                            case "Minor Child 1":
                                familyName5.Text = dtRow["familyName"].ToString().Trim();
                                firstName5.Text = dtRow["firstName"].ToString().Trim();
                                DOB5.Text = Convert.ToDateTime(dtRow["DOB"]).ToShortDateString();
                                rdoGender5.Text = dtRow["gender"].ToString().Trim();
                                hdnChild1ID.Value = dtRow["id"].ToString();
                                break;
                            case "Minor Child 2":
                                familyName6.Text = dtRow["familyName"].ToString().Trim();
                                firstName6.Text = dtRow["firstName"].ToString().Trim();
                                DOB6.Text = Convert.ToDateTime(dtRow["DOB"]).ToShortDateString();
                                rdoGender6.Text = dtRow["gender"].ToString().Trim();
                                hdnChild2ID.Value = dtRow["id"].ToString();
                                break;
                            case "Minor Child 3":
                                familyName7.Text = dtRow["familyName"].ToString().Trim();
                                firstName7.Text = dtRow["firstName"].ToString().Trim();
                                DOB7.Text = Convert.ToDateTime(dtRow["DOB"]).ToShortDateString();
                                rdoGender7.Text = dtRow["gender"].ToString().Trim();
                                hdnChild3ID.Value = dtRow["id"].ToString();
                                break;
                            case "Minor Child 4":
                                familyName8.Text = dtRow["familyName"].ToString().Trim();
                                firstName8.Text = dtRow["firstName"].ToString().Trim();
                                DOB8.Text = Convert.ToDateTime(dtRow["DOB"]).ToShortDateString();
                                rdoGender8.Text = dtRow["gender"].ToString().Trim();
                                hdnChild4ID.Value = dtRow["id"].ToString();
                                break;
                        }
                    }
                }
            }


        }
    }

    protected void ddlFamilies_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (blIsAdmin)
        {
            Response.Redirect("ApplicationForm_V.aspx?ID=" + ddlFamilies.SelectedValue);
        }
        else
        {
            //Response.Redirect("/Homestay/Families/ApplicationForm.aspx?ID=" + ddlFamilies.SelectedValue + "&UID=" + strUserName + "&admin=" + blIsAdmin);
        }

    }

    protected void btnCmdUpdateTravel_Click(object sender, EventArgs e)
    {
        Int32 intFamilyID = Int32.Parse(Request.QueryString["ID"].ToString());
        if (walkingBusStopMiles.Text.Trim() == "")
            walkingBusStopMiles.Text = "0";
        decimal decMiles = Convert.ToDecimal(walkingBusStopMiles.Text.Trim().Replace("'", "''"));
        /**********************************************************/
        /*  CONVERT REST TO INTS!!! */
        string sResult = "";
        Response.Write("familyID: " + intFamilyID.ToString() + "; walking miles: " + walkingBusStopMiles.Text + "; SCC bus minutes: " + busMinutesSCC.Text + "<br />");
        if (walkingBusStopMinutes.Text.Trim() == "")
            walkingBusStopMinutes.Text = "0";
        if (walkingBusStopBlocks.Text.Trim() == "")
            walkingBusStopBlocks.Text = "0";
        if (busMinutesSCC.Text.Trim() == "")
            busMinutesSCC.Text = "0";
        if (busMinutesSFCC.Text.Trim() == "")
            busMinutesSFCC.Text = "0";
        sResult = hsInfo.UpdateTravelTimeHomestayFamilyInfo(intFamilyID, 
            walkingBusStopMinutes.Text.Trim().Replace("'", "''"), 
            decMiles, walkingBusStopBlocks.Text.Trim().Replace("'", "''"), 
            busMinutesSCC.Text.Trim().Replace("'", "''"), busMinutesSFCC.Text.Trim().Replace("'", "''"), collegeQualified.SelectedValue);
        Response.Write("<br />btnUpdateTravel result: " + sResult + "<br />");
    }

    protected void btnCmdUpdateAndContinue_Click(object sender, EventArgs e)
    {
        Int32 intFamilyID = Int32.Parse(Request.QueryString["ID"].ToString());

        string strRoom1Occupancy = (rdoRoom1Occupancy.Text == "Planning") ? ddlQuarterStart1.SelectedValue : rdoRoom1Occupancy.Text.Replace("'", "''");
        string strRoom2Occupancy = (rdoRoom2Occupancy.Text == "Planning") ? ddlQuarterStart2.SelectedValue : rdoRoom2Occupancy.Text.Replace("'", "''");
        string strRoom3Occupancy = (rdoRoom3Occupancy.Text == "Planning") ? ddlQuarterStart3.SelectedValue : rdoRoom3Occupancy.Text.Replace("'", "''");
        string strRoom4Occupancy = (rdoRoom4Occupancy.Text == "Planning") ? ddlQuarterStart4.SelectedValue : rdoRoom4Occupancy.Text.Replace("'", "''");

        
        //Upload drivers license copies
        if (uplLicenseFile.HasFile)
        {
            string strFileName = homeName.Text + "-" + intFamilyID + "-License-" + uplLicenseFile.FileName;
            uplLicenseFile.SaveAs(strUploadURL + strFileName);
            strResultMsg += "Your file, <span style=\"color:red;\">" + strFileName + "</span>, was successfully uploaded!<br />";
            //Insert record in UploadedFilesInformation table
            //if (!IsPostBack)
            //{
            intResult = hsInfo.InsertUploadedFilesInformation(intFamilyID, homeName.Text, strFileName, strUploadURL);
            //}
            blLicenseSubmitted = true;
        }
        else if (hdnLicenseSubmitted.Value.ToLower() == "true")
        {
            //previous submission exists
            blLicenseSubmitted = true;
        }
        intResult = hsInfo.UpdateP1HomestayFamilyInfo(intFamilyID, userName.Text.Trim()
            , familyName.Text + ", " + firstName.Text, streetAddress.Text.Replace("'", "''"),
            city.Text.Replace("'", "''"),
            state.Text, ZIP.Text, landLinePhone.Text, wspBackgroundCheck.Text,
            insuranceCompanyName.Text.Replace("'", "''"), insurancePolicyNumber.Text.Replace("'", "''"),
            insuranceAgentName.Text.Replace("'", "''"), insuranceAgentPhone.Text.Replace("'", "''"),
             whyHost.Text.Replace("'", "''"), hostExperience.Text.Replace("'", "''"), primaryLanguage.Text, otherLanguages.Text.Replace("'", "''"),
            religiousActivities.Text.Replace("'", "''"), smokingGuidelines.Text.Replace("'", "''"), petsInHome.Text.Replace("'", "''"),
            generalLifestyle.Text.Replace("'", "''"), otherSchool.Text.Replace("'", "''"),
            additionalExpectations.Text.Replace("'", "''"), blLicenseSubmitted, 
            howReferred.Text.Replace("'", "''"),
            rdoRoom1.SelectedValue, "1900-01-01", "1900-01-01", rdoRoom1Bathroom.SelectedValue, strRoom1Occupancy,
            rdoRoom2.SelectedValue, "1900-01-01", "1900-01-01", rdoRoom2Bathroom.SelectedValue, strRoom2Occupancy,
            rdoRoom3.SelectedValue, "1900-01-01", "1900-01-01", rdoRoom3Bathroom.SelectedValue, strRoom3Occupancy,
            rdoRoom4.SelectedValue, "1900-01-01", "1900-01-01", rdoRoom4Bathroom.SelectedValue, strRoom4Occupancy,
            intMainLevelRooms, intUpstairsRooms, intBasementRooms);
        if (blIsAdmin)
        {
            //update admin only fields
            decimal decMiles = (!string.IsNullOrEmpty(walkingBusStopMiles.Text.Trim())) ?
                                 Convert.ToDecimal(walkingBusStopMiles.Text.Trim().Replace("'", "''")) : 0;
            string sResult = hsInfo.UpdateTravelTimeHomestayFamilyInfo(intFamilyID,
                                    walkingBusStopMinutes.Text.Replace("'", "''"), 
                                    decMiles,
                                    walkingBusStopBlocks.Text.Replace("'", "''"),
                                    busMinutesSCC.Text.Replace("'", "''").ToString() ,
                                    busMinutesSCC.Text.Replace("'", "''").ToString(), 
                                    collegeQualified.SelectedValue);
            Response.Write(Int32.Parse(ViewState["intFamilyID"].ToString()));
        }
        Response.Redirect("ApplicationForm_V.aspx?ID=" + ddlFamilies.SelectedValue);
    }

    protected void btnCmdSaveAndContinue_Click(object sender, EventArgs e)
    {
        string strRoom1Occupancy = (rdoRoom1Occupancy.Text == "Planning") ? ddlQuarterStart1.SelectedValue : rdoRoom1Occupancy.Text.Replace("'", "''");
        string strRoom2Occupancy = (rdoRoom2Occupancy.Text == "Planning") ? ddlQuarterStart2.SelectedValue : rdoRoom2Occupancy.Text.Replace("'", "''");
        string strRoom3Occupancy = (rdoRoom3Occupancy.Text == "Planning") ? ddlQuarterStart3.SelectedValue : rdoRoom3Occupancy.Text.Replace("'", "''");
        string strRoom4Occupancy = (rdoRoom4Occupancy.Text == "Planning") ? ddlQuarterStart4.SelectedValue : rdoRoom4Occupancy.Text.Replace("'", "''");


        intFamilyID = hsInfo.InsertHomestayFamilyInfo(userName.Text.Trim()
            , familyName.Text + ", " + firstName.Text, streetAddress.Text.Replace("'", "''"),
            city.Text.Replace("'", "''"),
            state.Text, ZIP.Text, landLinePhone.Text, wspBackgroundCheck.Text,
            insuranceCompanyName.Text.Replace("'", "''"), insurancePolicyNumber.Text.Replace("'", "''"),
            insuranceAgentName.Text.Replace("'", "''"), insuranceAgentPhone.Text.Replace("'", "''"),
             whyHost.Text.Replace("'", "''"), hostExperience.Text.Replace("'", "''"), primaryLanguage.Text, otherLanguages.Text.Replace("'", "''"),
            religiousActivities.Text.Replace("'", "''"), smokingGuidelines.Text.Replace("'", "''"), petsInHome.Text.Replace("'", "''"),
            generalLifestyle.Text.Replace("'", "''"), otherSchool.Text.Replace("'", "''"),
            additionalExpectations.Text.Replace("'", "''"), blLicenseSubmitted,
            howReferred.Text.Replace("'", "''"),
            rdoRoom1.SelectedValue, "1900-01-01", "1900-01-01", rdoRoom1Bathroom.SelectedValue, strRoom1Occupancy,
            rdoRoom2.SelectedValue, "1900-01-01", "1900-01-01", rdoRoom2Bathroom.SelectedValue, strRoom2Occupancy,
            rdoRoom3.SelectedValue, "1900-01-01", "1900-01-01", rdoRoom3Bathroom.SelectedValue, strRoom3Occupancy,
            rdoRoom4.SelectedValue, "1900-01-01", "1900-01-01", rdoRoom4Bathroom.SelectedValue, strRoom4Occupancy,
            intMainLevelRooms, intUpstairsRooms, intBasementRooms);

        intResult = hsInfo.InsertHomestayRelatives(0, intFamilyID, familyName.Text, firstName.Text, occupation.Text,
                            DOB.Text, 0, rdoGender.SelectedValue, "Primary Contact", email.Text,
                            cellPhone.Text, workPhone.Text, driversLicenseNumber.Text, middleName.Text);
        //Insert Primary Contact into HomestayRelatives table
        if (blIsAdmin)
        {
            //update admin only fields
            // Response.Write("travel: " + intFamilyID.ToString() + "; " + strWalkingBusStopMinutes + "; " + strWalkingBusStopMiles + "; " + strWalkingBusStopBlocks + "; " + strBusMinutesSCC + "; " + strBusMinutesSFCC + "; " + strCollegeQualified + "<br />"); 
            // intResult = hsInfo.UpdateTravelTimeHomestayFamilyInfo(intFamilyID, strWalkingBusStopMinutes, strWalkingBusStopMiles, strWalkingBusStopBlocks, strBusMinutesSCC, strBusMinutesSFCC, strCollegeQualified);
            decimal decMiles = (!string.IsNullOrEmpty(walkingBusStopMiles.Text.Trim())) ?
                                 Convert.ToDecimal(walkingBusStopMiles.Text.Trim().Replace("'", "''")) : 0;
            string sResult = "";
            sResult = hsInfo.UpdateTravelTimeHomestayFamilyInfo(intFamilyID, walkingBusStopMinutes.Text.Replace("'", "''"),
                                    decMiles,
                                    walkingBusStopBlocks.Text.Replace("'", "''"),
                                    busMinutesSCC.Text.Replace("'", "''").ToString(),
                                    busMinutesSCC.Text.Replace("'", "''").ToString(),
                                    collegeQualified.SelectedValue);
        }

        Response.Redirect("PlacementProfile.aspx?ID=" + intFamilyID);
    }
    protected void rdoRoom1Occupancy_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlQuarterStart1.Visible = (rdoRoom1Occupancy.SelectedIndex == 1) ? true : false;
    }
    protected void rdoRoom2Occupancy_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlQuarterStart2.Visible = (rdoRoom2Occupancy.SelectedIndex == 1) ? true : false;
    }
    protected void rdoRoom3Occupancy_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlQuarterStart3.Visible = (rdoRoom3Occupancy.SelectedIndex == 1) ? true : false;
    }
    protected void rdoRoom4Occupancy_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlQuarterStart4.Visible = (rdoRoom4Occupancy.SelectedIndex == 1) ? true : false;
    }
}